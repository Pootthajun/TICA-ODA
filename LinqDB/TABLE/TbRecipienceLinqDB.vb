Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for TB_Recipience table LinqDB.
    '[Create by  on January, 9 2017]
    Public Class TbRecipienceLinqDB
        Public sub TbRecipienceLinqDB()

        End Sub 
        ' TB_Recipience
        Const _tableName As String = "TB_Recipience"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _ID As Long = 0
        Dim _ACTIVITY_ID As  System.Nullable(Of Long) 
        Dim _PERSON_ID As  String  = ""
        Dim _PSH_SEX As  String  = ""
        Dim _PSH_CITY As  String  = ""
        Dim _PSH_NATIONALITY As  String  = ""
        Dim _PSH_MARITAL_STATUS As  String  = ""
        Dim _PSH_RELIGION As  String  = ""
        Dim _PSH_WORK_ADDRESS As  String  = ""
        Dim _PSH_WORK_TELEPHONE As  String  = ""
        Dim _PSH_WORK_FAX As  String  = ""
        Dim _PSH_WORK_EMAIL As  String  = ""
        Dim _PSH_HOME_ADDRESS As  String  = ""
        Dim _PSH_HOME_TELEPHONE As  String  = ""
        Dim _PSH_CITY_DEPARTTURE As  String  = ""
        Dim _PSH_EMERGENCY_CONTACT As  String  = ""
        Dim _PSH_EMERGENCY_TELEPHONE As  String  = ""
        Dim _PSH_EMERGENCY_RELATIONSHIP As  String  = ""
        Dim _EDU_TOEFLSCORE As  String  = ""
        Dim _EDU_IELTSSCORE As  String  = ""
        Dim _EDU_OTHERSCORE As  String  = ""
        Dim _EDU_TRAINED_DETAIL As  String  = ""
        Dim _EDU_PUBLICATIONS As  String  = ""
        Dim _EXP_EXPECTATION_DESCRIPTION As  String  = ""
        Dim _EXP_SIGNATURE() As Byte
        Dim _EXP_PRINT_NAME As  String  = ""
        Dim _EXP_PRINT_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _GA_TITLE_POST As  String  = ""
        Dim _GA_DUTIES As  String  = ""
        Dim _GA_STAMP As  String  = ""
        Dim _GA_TITLE As  String  = ""
        Dim _GA_ORG As  String  = ""
        Dim _GA_ADDRESS As  String  = ""
        Dim _GA_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _PROFILE_IMAGE() As Byte
        Dim _CREATED_BY As  String  = ""
        Dim _CREATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _ACTIVE_STATUS As Char = "Y"

        'Generate Field Property 
        <Column(Storage:="_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property ID() As Long
            Get
                Return _ID
            End Get
            Set(ByVal value As Long)
               _ID = value
            End Set
        End Property 
        <Column(Storage:="_ACTIVITY_ID", DbType:="BigInt")>  _
        Public Property ACTIVITY_ID() As  System.Nullable(Of Long) 
            Get
                Return _ACTIVITY_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _ACTIVITY_ID = value
            End Set
        End Property 
        <Column(Storage:="_PERSON_ID", DbType:="VarChar(50)")>  _
        Public Property PERSON_ID() As  String 
            Get
                Return _PERSON_ID
            End Get
            Set(ByVal value As  String )
               _PERSON_ID = value
            End Set
        End Property 
        <Column(Storage:="_PSH_SEX", DbType:="VarChar(1)")>  _
        Public Property PSH_SEX() As  String 
            Get
                Return _PSH_SEX
            End Get
            Set(ByVal value As  String )
               _PSH_SEX = value
            End Set
        End Property 
        <Column(Storage:="_PSH_CITY", DbType:="VarChar(250)")>  _
        Public Property PSH_CITY() As  String 
            Get
                Return _PSH_CITY
            End Get
            Set(ByVal value As  String )
               _PSH_CITY = value
            End Set
        End Property 
        <Column(Storage:="_PSH_NATIONALITY", DbType:="VarChar(100)")>  _
        Public Property PSH_NATIONALITY() As  String 
            Get
                Return _PSH_NATIONALITY
            End Get
            Set(ByVal value As  String )
               _PSH_NATIONALITY = value
            End Set
        End Property 
        <Column(Storage:="_PSH_MARITAL_STATUS", DbType:="VarChar(1)")>  _
        Public Property PSH_MARITAL_STATUS() As  String 
            Get
                Return _PSH_MARITAL_STATUS
            End Get
            Set(ByVal value As  String )
               _PSH_MARITAL_STATUS = value
            End Set
        End Property 
        <Column(Storage:="_PSH_RELIGION", DbType:="VarChar(100)")>  _
        Public Property PSH_RELIGION() As  String 
            Get
                Return _PSH_RELIGION
            End Get
            Set(ByVal value As  String )
               _PSH_RELIGION = value
            End Set
        End Property 
        <Column(Storage:="_PSH_WORK_ADDRESS", DbType:="VarChar(500)")>  _
        Public Property PSH_WORK_ADDRESS() As  String 
            Get
                Return _PSH_WORK_ADDRESS
            End Get
            Set(ByVal value As  String )
               _PSH_WORK_ADDRESS = value
            End Set
        End Property 
        <Column(Storage:="_PSH_WORK_TELEPHONE", DbType:="VarChar(50)")>  _
        Public Property PSH_WORK_TELEPHONE() As  String 
            Get
                Return _PSH_WORK_TELEPHONE
            End Get
            Set(ByVal value As  String )
               _PSH_WORK_TELEPHONE = value
            End Set
        End Property 
        <Column(Storage:="_PSH_WORK_FAX", DbType:="VarChar(50)")>  _
        Public Property PSH_WORK_FAX() As  String 
            Get
                Return _PSH_WORK_FAX
            End Get
            Set(ByVal value As  String )
               _PSH_WORK_FAX = value
            End Set
        End Property 
        <Column(Storage:="_PSH_WORK_EMAIL", DbType:="VarChar(250)")>  _
        Public Property PSH_WORK_EMAIL() As  String 
            Get
                Return _PSH_WORK_EMAIL
            End Get
            Set(ByVal value As  String )
               _PSH_WORK_EMAIL = value
            End Set
        End Property 
        <Column(Storage:="_PSH_HOME_ADDRESS", DbType:="VarChar(500)")>  _
        Public Property PSH_HOME_ADDRESS() As  String 
            Get
                Return _PSH_HOME_ADDRESS
            End Get
            Set(ByVal value As  String )
               _PSH_HOME_ADDRESS = value
            End Set
        End Property 
        <Column(Storage:="_PSH_HOME_TELEPHONE", DbType:="VarChar(50)")>  _
        Public Property PSH_HOME_TELEPHONE() As  String 
            Get
                Return _PSH_HOME_TELEPHONE
            End Get
            Set(ByVal value As  String )
               _PSH_HOME_TELEPHONE = value
            End Set
        End Property 
        <Column(Storage:="_PSH_CITY_DEPARTTURE", DbType:="VarChar(500)")>  _
        Public Property PSH_CITY_DEPARTTURE() As  String 
            Get
                Return _PSH_CITY_DEPARTTURE
            End Get
            Set(ByVal value As  String )
               _PSH_CITY_DEPARTTURE = value
            End Set
        End Property 
        <Column(Storage:="_PSH_EMERGENCY_CONTACT", DbType:="VarChar(500)")>  _
        Public Property PSH_EMERGENCY_CONTACT() As  String 
            Get
                Return _PSH_EMERGENCY_CONTACT
            End Get
            Set(ByVal value As  String )
               _PSH_EMERGENCY_CONTACT = value
            End Set
        End Property 
        <Column(Storage:="_PSH_EMERGENCY_TELEPHONE", DbType:="VarChar(50)")>  _
        Public Property PSH_EMERGENCY_TELEPHONE() As  String 
            Get
                Return _PSH_EMERGENCY_TELEPHONE
            End Get
            Set(ByVal value As  String )
               _PSH_EMERGENCY_TELEPHONE = value
            End Set
        End Property 
        <Column(Storage:="_PSH_EMERGENCY_RELATIONSHIP", DbType:="VarChar(100)")>  _
        Public Property PSH_EMERGENCY_RELATIONSHIP() As  String 
            Get
                Return _PSH_EMERGENCY_RELATIONSHIP
            End Get
            Set(ByVal value As  String )
               _PSH_EMERGENCY_RELATIONSHIP = value
            End Set
        End Property 
        <Column(Storage:="_EDU_TOEFLSCORE", DbType:="VarChar(5)")>  _
        Public Property EDU_TOEFLSCORE() As  String 
            Get
                Return _EDU_TOEFLSCORE
            End Get
            Set(ByVal value As  String )
               _EDU_TOEFLSCORE = value
            End Set
        End Property 
        <Column(Storage:="_EDU_IELTSSCORE", DbType:="VarChar(5)")>  _
        Public Property EDU_IELTSSCORE() As  String 
            Get
                Return _EDU_IELTSSCORE
            End Get
            Set(ByVal value As  String )
               _EDU_IELTSSCORE = value
            End Set
        End Property 
        <Column(Storage:="_EDU_OTHERSCORE", DbType:="VarChar(250)")>  _
        Public Property EDU_OTHERSCORE() As  String 
            Get
                Return _EDU_OTHERSCORE
            End Get
            Set(ByVal value As  String )
               _EDU_OTHERSCORE = value
            End Set
        End Property 
        <Column(Storage:="_EDU_TRAINED_DETAIL", DbType:="VarChar(500)")>  _
        Public Property EDU_TRAINED_DETAIL() As  String 
            Get
                Return _EDU_TRAINED_DETAIL
            End Get
            Set(ByVal value As  String )
               _EDU_TRAINED_DETAIL = value
            End Set
        End Property 
        <Column(Storage:="_EDU_PUBLICATIONS", DbType:="VarChar(500)")>  _
        Public Property EDU_PUBLICATIONS() As  String 
            Get
                Return _EDU_PUBLICATIONS
            End Get
            Set(ByVal value As  String )
               _EDU_PUBLICATIONS = value
            End Set
        End Property 
        <Column(Storage:="_EXP_EXPECTATION_DESCRIPTION", DbType:="VarChar(500)")>  _
        Public Property EXP_EXPECTATION_DESCRIPTION() As  String 
            Get
                Return _EXP_EXPECTATION_DESCRIPTION
            End Get
            Set(ByVal value As  String )
               _EXP_EXPECTATION_DESCRIPTION = value
            End Set
        End Property 
        <Column(Storage:="_EXP_SIGNATURE", DbType:="IMAGE")>  _
        Public Property EXP_SIGNATURE() As  Byte() 
            Get
                Return _EXP_SIGNATURE
            End Get
            Set(ByVal value() As Byte)
               _EXP_SIGNATURE = value
            End Set
        End Property 
        <Column(Storage:="_EXP_PRINT_NAME", DbType:="VarChar(250)")>  _
        Public Property EXP_PRINT_NAME() As  String 
            Get
                Return _EXP_PRINT_NAME
            End Get
            Set(ByVal value As  String )
               _EXP_PRINT_NAME = value
            End Set
        End Property 
        <Column(Storage:="_EXP_PRINT_DATE", DbType:="DateTime")>  _
        Public Property EXP_PRINT_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _EXP_PRINT_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _EXP_PRINT_DATE = value
            End Set
        End Property 
        <Column(Storage:="_GA_TITLE_POST", DbType:="VarChar(250)")>  _
        Public Property GA_TITLE_POST() As  String 
            Get
                Return _GA_TITLE_POST
            End Get
            Set(ByVal value As  String )
               _GA_TITLE_POST = value
            End Set
        End Property 
        <Column(Storage:="_GA_DUTIES", DbType:="VarChar(250)")>  _
        Public Property GA_DUTIES() As  String 
            Get
                Return _GA_DUTIES
            End Get
            Set(ByVal value As  String )
               _GA_DUTIES = value
            End Set
        End Property 
        <Column(Storage:="_GA_STAMP", DbType:="VarChar(250)")>  _
        Public Property GA_STAMP() As  String 
            Get
                Return _GA_STAMP
            End Get
            Set(ByVal value As  String )
               _GA_STAMP = value
            End Set
        End Property 
        <Column(Storage:="_GA_TITLE", DbType:="VarChar(250)")>  _
        Public Property GA_TITLE() As  String 
            Get
                Return _GA_TITLE
            End Get
            Set(ByVal value As  String )
               _GA_TITLE = value
            End Set
        End Property 
        <Column(Storage:="_GA_ORG", DbType:="VarChar(250)")>  _
        Public Property GA_ORG() As  String 
            Get
                Return _GA_ORG
            End Get
            Set(ByVal value As  String )
               _GA_ORG = value
            End Set
        End Property 
        <Column(Storage:="_GA_ADDRESS", DbType:="VarChar(250)")>  _
        Public Property GA_ADDRESS() As  String 
            Get
                Return _GA_ADDRESS
            End Get
            Set(ByVal value As  String )
               _GA_ADDRESS = value
            End Set
        End Property 
        <Column(Storage:="_GA_DATE", DbType:="DateTime")>  _
        Public Property GA_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _GA_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _GA_DATE = value
            End Set
        End Property 
        <Column(Storage:="_PROFILE_IMAGE", DbType:="IMAGE")>  _
        Public Property PROFILE_IMAGE() As  Byte() 
            Get
                Return _PROFILE_IMAGE
            End Get
            Set(ByVal value() As Byte)
               _PROFILE_IMAGE = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100)")>  _
        Public Property CREATED_BY() As  String 
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As  String )
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime")>  _
        Public Property CREATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_ACTIVE_STATUS", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ACTIVE_STATUS() As Char
            Get
                Return _ACTIVE_STATUS
            End Get
            Set(ByVal value As Char)
               _ACTIVE_STATUS = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _ID = 0
            _ACTIVITY_ID = Nothing
            _PERSON_ID = ""
            _PSH_SEX = ""
            _PSH_CITY = ""
            _PSH_NATIONALITY = ""
            _PSH_MARITAL_STATUS = ""
            _PSH_RELIGION = ""
            _PSH_WORK_ADDRESS = ""
            _PSH_WORK_TELEPHONE = ""
            _PSH_WORK_FAX = ""
            _PSH_WORK_EMAIL = ""
            _PSH_HOME_ADDRESS = ""
            _PSH_HOME_TELEPHONE = ""
            _PSH_CITY_DEPARTTURE = ""
            _PSH_EMERGENCY_CONTACT = ""
            _PSH_EMERGENCY_TELEPHONE = ""
            _PSH_EMERGENCY_RELATIONSHIP = ""
            _EDU_TOEFLSCORE = ""
            _EDU_IELTSSCORE = ""
            _EDU_OTHERSCORE = ""
            _EDU_TRAINED_DETAIL = ""
            _EDU_PUBLICATIONS = ""
            _EXP_EXPECTATION_DESCRIPTION = ""
             _EXP_SIGNATURE = Nothing
            _EXP_PRINT_NAME = ""
            _EXP_PRINT_DATE = New DateTime(1,1,1)
            _GA_TITLE_POST = ""
            _GA_DUTIES = ""
            _GA_STAMP = ""
            _GA_TITLE = ""
            _GA_ORG = ""
            _GA_ADDRESS = ""
            _GA_DATE = New DateTime(1,1,1)
             _PROFILE_IMAGE = Nothing
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
            _ACTIVE_STATUS = "Y"
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into TB_Recipience table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_Recipience table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _id > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("ID = @_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_Recipience table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from TB_Recipience table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_ID", cID)
                Return doDelete("ID = @_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of TB_Recipience by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doChkData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of TB_Recipience by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cID As Long, trans As SQLTransaction) As TbRecipienceLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doGetData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of TB_Recipience by specified PERSON_ID key is retrieved successfully.
        '/// <param name=cPERSON_ID>The PERSON_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPERSON_ID(cPERSON_ID As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_PERSON_ID", cPERSON_ID) 
            Return doChkData("PERSON_ID = @_PERSON_ID", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of TB_Recipience by specified PERSON_ID key is retrieved successfully.
        '/// <param name=cPERSON_ID>The PERSON_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByPERSON_ID(cPERSON_ID As String, cID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_PERSON_ID", cPERSON_ID) 
            cmdPara(1) = DB.SetBigInt("@_ID", cID) 
            Return doChkData("PERSON_ID = @_PERSON_ID And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of TB_Recipience by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into TB_Recipience table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _ID = dt.Rows(0)("ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to TB_Recipience table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from TB_Recipience table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(39) As SqlParameter
            cmbParam(0) = New SqlParameter("@_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _ID

            cmbParam(1) = New SqlParameter("@_ACTIVITY_ID", SqlDbType.BigInt)
            If _ACTIVITY_ID IsNot Nothing Then 
                cmbParam(1).Value = _ACTIVITY_ID.Value
            Else
                cmbParam(1).Value = DBNull.value
            End IF

            cmbParam(2) = New SqlParameter("@_PERSON_ID", SqlDbType.VarChar)
            If _PERSON_ID.Trim <> "" Then 
                cmbParam(2).Value = _PERSON_ID.Trim
            Else
                cmbParam(2).Value = DBNull.value
            End If

            cmbParam(3) = New SqlParameter("@_PSH_SEX", SqlDbType.VarChar)
            If _PSH_SEX.Trim <> "" Then 
                cmbParam(3).Value = _PSH_SEX.Trim
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_PSH_CITY", SqlDbType.VarChar)
            If _PSH_CITY.Trim <> "" Then 
                cmbParam(4).Value = _PSH_CITY.Trim
            Else
                cmbParam(4).Value = DBNull.value
            End If

            cmbParam(5) = New SqlParameter("@_PSH_NATIONALITY", SqlDbType.VarChar)
            If _PSH_NATIONALITY.Trim <> "" Then 
                cmbParam(5).Value = _PSH_NATIONALITY.Trim
            Else
                cmbParam(5).Value = DBNull.value
            End If

            cmbParam(6) = New SqlParameter("@_PSH_MARITAL_STATUS", SqlDbType.VarChar)
            If _PSH_MARITAL_STATUS.Trim <> "" Then 
                cmbParam(6).Value = _PSH_MARITAL_STATUS.Trim
            Else
                cmbParam(6).Value = DBNull.value
            End If

            cmbParam(7) = New SqlParameter("@_PSH_RELIGION", SqlDbType.VarChar)
            If _PSH_RELIGION.Trim <> "" Then 
                cmbParam(7).Value = _PSH_RELIGION.Trim
            Else
                cmbParam(7).Value = DBNull.value
            End If

            cmbParam(8) = New SqlParameter("@_PSH_WORK_ADDRESS", SqlDbType.VarChar)
            If _PSH_WORK_ADDRESS.Trim <> "" Then 
                cmbParam(8).Value = _PSH_WORK_ADDRESS.Trim
            Else
                cmbParam(8).Value = DBNull.value
            End If

            cmbParam(9) = New SqlParameter("@_PSH_WORK_TELEPHONE", SqlDbType.VarChar)
            If _PSH_WORK_TELEPHONE.Trim <> "" Then 
                cmbParam(9).Value = _PSH_WORK_TELEPHONE.Trim
            Else
                cmbParam(9).Value = DBNull.value
            End If

            cmbParam(10) = New SqlParameter("@_PSH_WORK_FAX", SqlDbType.VarChar)
            If _PSH_WORK_FAX.Trim <> "" Then 
                cmbParam(10).Value = _PSH_WORK_FAX.Trim
            Else
                cmbParam(10).Value = DBNull.value
            End If

            cmbParam(11) = New SqlParameter("@_PSH_WORK_EMAIL", SqlDbType.VarChar)
            If _PSH_WORK_EMAIL.Trim <> "" Then 
                cmbParam(11).Value = _PSH_WORK_EMAIL.Trim
            Else
                cmbParam(11).Value = DBNull.value
            End If

            cmbParam(12) = New SqlParameter("@_PSH_HOME_ADDRESS", SqlDbType.VarChar)
            If _PSH_HOME_ADDRESS.Trim <> "" Then 
                cmbParam(12).Value = _PSH_HOME_ADDRESS.Trim
            Else
                cmbParam(12).Value = DBNull.value
            End If

            cmbParam(13) = New SqlParameter("@_PSH_HOME_TELEPHONE", SqlDbType.VarChar)
            If _PSH_HOME_TELEPHONE.Trim <> "" Then 
                cmbParam(13).Value = _PSH_HOME_TELEPHONE.Trim
            Else
                cmbParam(13).Value = DBNull.value
            End If

            cmbParam(14) = New SqlParameter("@_PSH_CITY_DEPARTTURE", SqlDbType.VarChar)
            If _PSH_CITY_DEPARTTURE.Trim <> "" Then 
                cmbParam(14).Value = _PSH_CITY_DEPARTTURE.Trim
            Else
                cmbParam(14).Value = DBNull.value
            End If

            cmbParam(15) = New SqlParameter("@_PSH_EMERGENCY_CONTACT", SqlDbType.VarChar)
            If _PSH_EMERGENCY_CONTACT.Trim <> "" Then 
                cmbParam(15).Value = _PSH_EMERGENCY_CONTACT.Trim
            Else
                cmbParam(15).Value = DBNull.value
            End If

            cmbParam(16) = New SqlParameter("@_PSH_EMERGENCY_TELEPHONE", SqlDbType.VarChar)
            If _PSH_EMERGENCY_TELEPHONE.Trim <> "" Then 
                cmbParam(16).Value = _PSH_EMERGENCY_TELEPHONE.Trim
            Else
                cmbParam(16).Value = DBNull.value
            End If

            cmbParam(17) = New SqlParameter("@_PSH_EMERGENCY_RELATIONSHIP", SqlDbType.VarChar)
            If _PSH_EMERGENCY_RELATIONSHIP.Trim <> "" Then 
                cmbParam(17).Value = _PSH_EMERGENCY_RELATIONSHIP.Trim
            Else
                cmbParam(17).Value = DBNull.value
            End If

            cmbParam(18) = New SqlParameter("@_EDU_TOEFLSCORE", SqlDbType.VarChar)
            If _EDU_TOEFLSCORE.Trim <> "" Then 
                cmbParam(18).Value = _EDU_TOEFLSCORE.Trim
            Else
                cmbParam(18).Value = DBNull.value
            End If

            cmbParam(19) = New SqlParameter("@_EDU_IELTSSCORE", SqlDbType.VarChar)
            If _EDU_IELTSSCORE.Trim <> "" Then 
                cmbParam(19).Value = _EDU_IELTSSCORE.Trim
            Else
                cmbParam(19).Value = DBNull.value
            End If

            cmbParam(20) = New SqlParameter("@_EDU_OTHERSCORE", SqlDbType.VarChar)
            If _EDU_OTHERSCORE.Trim <> "" Then 
                cmbParam(20).Value = _EDU_OTHERSCORE.Trim
            Else
                cmbParam(20).Value = DBNull.value
            End If

            cmbParam(21) = New SqlParameter("@_EDU_TRAINED_DETAIL", SqlDbType.VarChar)
            If _EDU_TRAINED_DETAIL.Trim <> "" Then 
                cmbParam(21).Value = _EDU_TRAINED_DETAIL.Trim
            Else
                cmbParam(21).Value = DBNull.value
            End If

            cmbParam(22) = New SqlParameter("@_EDU_PUBLICATIONS", SqlDbType.VarChar)
            If _EDU_PUBLICATIONS.Trim <> "" Then 
                cmbParam(22).Value = _EDU_PUBLICATIONS.Trim
            Else
                cmbParam(22).Value = DBNull.value
            End If

            cmbParam(23) = New SqlParameter("@_EXP_EXPECTATION_DESCRIPTION", SqlDbType.VarChar)
            If _EXP_EXPECTATION_DESCRIPTION.Trim <> "" Then 
                cmbParam(23).Value = _EXP_EXPECTATION_DESCRIPTION.Trim
            Else
                cmbParam(23).Value = DBNull.value
            End If

            If _EXP_SIGNATURE IsNot Nothing Then 
                cmbParam(24) = New SqlParameter("@_EXP_SIGNATURE",SqlDbType.Image, _EXP_SIGNATURE.Length)
                cmbParam(24).Value = _EXP_SIGNATURE
            Else
                cmbParam(24) = New SqlParameter("@_EXP_SIGNATURE", SqlDbType.Image)
                cmbParam(24).Value = DBNull.value
            End If

            cmbParam(25) = New SqlParameter("@_EXP_PRINT_NAME", SqlDbType.VarChar)
            If _EXP_PRINT_NAME.Trim <> "" Then 
                cmbParam(25).Value = _EXP_PRINT_NAME.Trim
            Else
                cmbParam(25).Value = DBNull.value
            End If

            cmbParam(26) = New SqlParameter("@_EXP_PRINT_DATE", SqlDbType.DateTime)
            If _EXP_PRINT_DATE.Value.Year > 1 Then 
                cmbParam(26).Value = _EXP_PRINT_DATE.Value
            Else
                cmbParam(26).Value = DBNull.value
            End If

            cmbParam(27) = New SqlParameter("@_GA_TITLE_POST", SqlDbType.VarChar)
            If _GA_TITLE_POST.Trim <> "" Then 
                cmbParam(27).Value = _GA_TITLE_POST.Trim
            Else
                cmbParam(27).Value = DBNull.value
            End If

            cmbParam(28) = New SqlParameter("@_GA_DUTIES", SqlDbType.VarChar)
            If _GA_DUTIES.Trim <> "" Then 
                cmbParam(28).Value = _GA_DUTIES.Trim
            Else
                cmbParam(28).Value = DBNull.value
            End If

            cmbParam(29) = New SqlParameter("@_GA_STAMP", SqlDbType.VarChar)
            If _GA_STAMP.Trim <> "" Then 
                cmbParam(29).Value = _GA_STAMP.Trim
            Else
                cmbParam(29).Value = DBNull.value
            End If

            cmbParam(30) = New SqlParameter("@_GA_TITLE", SqlDbType.VarChar)
            If _GA_TITLE.Trim <> "" Then 
                cmbParam(30).Value = _GA_TITLE.Trim
            Else
                cmbParam(30).Value = DBNull.value
            End If

            cmbParam(31) = New SqlParameter("@_GA_ORG", SqlDbType.VarChar)
            If _GA_ORG.Trim <> "" Then 
                cmbParam(31).Value = _GA_ORG.Trim
            Else
                cmbParam(31).Value = DBNull.value
            End If

            cmbParam(32) = New SqlParameter("@_GA_ADDRESS", SqlDbType.VarChar)
            If _GA_ADDRESS.Trim <> "" Then 
                cmbParam(32).Value = _GA_ADDRESS.Trim
            Else
                cmbParam(32).Value = DBNull.value
            End If

            cmbParam(33) = New SqlParameter("@_GA_DATE", SqlDbType.DateTime)
            If _GA_DATE.Value.Year > 1 Then 
                cmbParam(33).Value = _GA_DATE.Value
            Else
                cmbParam(33).Value = DBNull.value
            End If

            If _PROFILE_IMAGE IsNot Nothing Then 
                cmbParam(34) = New SqlParameter("@_PROFILE_IMAGE",SqlDbType.Image, _PROFILE_IMAGE.Length)
                cmbParam(34).Value = _PROFILE_IMAGE
            Else
                cmbParam(34) = New SqlParameter("@_PROFILE_IMAGE", SqlDbType.Image)
                cmbParam(34).Value = DBNull.value
            End If

            cmbParam(35) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            If _CREATED_BY.Trim <> "" Then 
                cmbParam(35).Value = _CREATED_BY.Trim
            Else
                cmbParam(35).Value = DBNull.value
            End If

            cmbParam(36) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            If _CREATED_DATE.Value.Year > 1 Then 
                cmbParam(36).Value = _CREATED_DATE.Value
            Else
                cmbParam(36).Value = DBNull.value
            End If

            cmbParam(37) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(37).Value = _UPDATED_BY.Trim
            Else
                cmbParam(37).Value = DBNull.value
            End If

            cmbParam(38) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(38).Value = _UPDATED_DATE.Value
            Else
                cmbParam(38).Value = DBNull.value
            End If

            cmbParam(39) = New SqlParameter("@_ACTIVE_STATUS", SqlDbType.Char)
            cmbParam(39).Value = _ACTIVE_STATUS

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of TB_Recipience by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("activity_id")) = False Then _activity_id = Convert.ToInt64(Rdr("activity_id"))
                        If Convert.IsDBNull(Rdr("person_id")) = False Then _person_id = Rdr("person_id").ToString()
                        If Convert.IsDBNull(Rdr("psh_sex")) = False Then _psh_sex = Rdr("psh_sex").ToString()
                        If Convert.IsDBNull(Rdr("psh_city")) = False Then _psh_city = Rdr("psh_city").ToString()
                        If Convert.IsDBNull(Rdr("psh_nationality")) = False Then _psh_nationality = Rdr("psh_nationality").ToString()
                        If Convert.IsDBNull(Rdr("psh_marital_status")) = False Then _psh_marital_status = Rdr("psh_marital_status").ToString()
                        If Convert.IsDBNull(Rdr("psh_religion")) = False Then _psh_religion = Rdr("psh_religion").ToString()
                        If Convert.IsDBNull(Rdr("psh_work_address")) = False Then _psh_work_address = Rdr("psh_work_address").ToString()
                        If Convert.IsDBNull(Rdr("psh_work_telephone")) = False Then _psh_work_telephone = Rdr("psh_work_telephone").ToString()
                        If Convert.IsDBNull(Rdr("psh_work_fax")) = False Then _psh_work_fax = Rdr("psh_work_fax").ToString()
                        If Convert.IsDBNull(Rdr("psh_work_email")) = False Then _psh_work_email = Rdr("psh_work_email").ToString()
                        If Convert.IsDBNull(Rdr("psh_home_address")) = False Then _psh_home_address = Rdr("psh_home_address").ToString()
                        If Convert.IsDBNull(Rdr("psh_home_telephone")) = False Then _psh_home_telephone = Rdr("psh_home_telephone").ToString()
                        If Convert.IsDBNull(Rdr("psh_city_departture")) = False Then _psh_city_departture = Rdr("psh_city_departture").ToString()
                        If Convert.IsDBNull(Rdr("psh_emergency_contact")) = False Then _psh_emergency_contact = Rdr("psh_emergency_contact").ToString()
                        If Convert.IsDBNull(Rdr("psh_emergency_telephone")) = False Then _psh_emergency_telephone = Rdr("psh_emergency_telephone").ToString()
                        If Convert.IsDBNull(Rdr("psh_emergency_relationship")) = False Then _psh_emergency_relationship = Rdr("psh_emergency_relationship").ToString()
                        If Convert.IsDBNull(Rdr("edu_TOEFLScore")) = False Then _edu_TOEFLScore = Rdr("edu_TOEFLScore").ToString()
                        If Convert.IsDBNull(Rdr("edu_IELTsScore")) = False Then _edu_IELTsScore = Rdr("edu_IELTsScore").ToString()
                        If Convert.IsDBNull(Rdr("edu_OtherScore")) = False Then _edu_OtherScore = Rdr("edu_OtherScore").ToString()
                        If Convert.IsDBNull(Rdr("edu_trained_detail")) = False Then _edu_trained_detail = Rdr("edu_trained_detail").ToString()
                        If Convert.IsDBNull(Rdr("edu_publications")) = False Then _edu_publications = Rdr("edu_publications").ToString()
                        If Convert.IsDBNull(Rdr("exp_expectation_description")) = False Then _exp_expectation_description = Rdr("exp_expectation_description").ToString()
                        If Convert.IsDBNull(Rdr("exp_signature")) = False Then _exp_signature = CType(Rdr("exp_signature"), Byte())
                        If Convert.IsDBNull(Rdr("exp_print_name")) = False Then _exp_print_name = Rdr("exp_print_name").ToString()
                        If Convert.IsDBNull(Rdr("exp_print_date")) = False Then _exp_print_date = Convert.ToDateTime(Rdr("exp_print_date"))
                        If Convert.IsDBNull(Rdr("ga_title_post")) = False Then _ga_title_post = Rdr("ga_title_post").ToString()
                        If Convert.IsDBNull(Rdr("ga_duties")) = False Then _ga_duties = Rdr("ga_duties").ToString()
                        If Convert.IsDBNull(Rdr("ga_stamp")) = False Then _ga_stamp = Rdr("ga_stamp").ToString()
                        If Convert.IsDBNull(Rdr("ga_title")) = False Then _ga_title = Rdr("ga_title").ToString()
                        If Convert.IsDBNull(Rdr("ga_org")) = False Then _ga_org = Rdr("ga_org").ToString()
                        If Convert.IsDBNull(Rdr("ga_address")) = False Then _ga_address = Rdr("ga_address").ToString()
                        If Convert.IsDBNull(Rdr("ga_date")) = False Then _ga_date = Convert.ToDateTime(Rdr("ga_date"))
                        If Convert.IsDBNull(Rdr("profile_image")) = False Then _profile_image = CType(Rdr("profile_image"), Byte())
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                        If Convert.IsDBNull(Rdr("active_status")) = False Then _active_status = Rdr("active_status").ToString()
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of TB_Recipience by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As TbRecipienceLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("activity_id")) = False Then _activity_id = Convert.ToInt64(Rdr("activity_id"))
                        If Convert.IsDBNull(Rdr("person_id")) = False Then _person_id = Rdr("person_id").ToString()
                        If Convert.IsDBNull(Rdr("psh_sex")) = False Then _psh_sex = Rdr("psh_sex").ToString()
                        If Convert.IsDBNull(Rdr("psh_city")) = False Then _psh_city = Rdr("psh_city").ToString()
                        If Convert.IsDBNull(Rdr("psh_nationality")) = False Then _psh_nationality = Rdr("psh_nationality").ToString()
                        If Convert.IsDBNull(Rdr("psh_marital_status")) = False Then _psh_marital_status = Rdr("psh_marital_status").ToString()
                        If Convert.IsDBNull(Rdr("psh_religion")) = False Then _psh_religion = Rdr("psh_religion").ToString()
                        If Convert.IsDBNull(Rdr("psh_work_address")) = False Then _psh_work_address = Rdr("psh_work_address").ToString()
                        If Convert.IsDBNull(Rdr("psh_work_telephone")) = False Then _psh_work_telephone = Rdr("psh_work_telephone").ToString()
                        If Convert.IsDBNull(Rdr("psh_work_fax")) = False Then _psh_work_fax = Rdr("psh_work_fax").ToString()
                        If Convert.IsDBNull(Rdr("psh_work_email")) = False Then _psh_work_email = Rdr("psh_work_email").ToString()
                        If Convert.IsDBNull(Rdr("psh_home_address")) = False Then _psh_home_address = Rdr("psh_home_address").ToString()
                        If Convert.IsDBNull(Rdr("psh_home_telephone")) = False Then _psh_home_telephone = Rdr("psh_home_telephone").ToString()
                        If Convert.IsDBNull(Rdr("psh_city_departture")) = False Then _psh_city_departture = Rdr("psh_city_departture").ToString()
                        If Convert.IsDBNull(Rdr("psh_emergency_contact")) = False Then _psh_emergency_contact = Rdr("psh_emergency_contact").ToString()
                        If Convert.IsDBNull(Rdr("psh_emergency_telephone")) = False Then _psh_emergency_telephone = Rdr("psh_emergency_telephone").ToString()
                        If Convert.IsDBNull(Rdr("psh_emergency_relationship")) = False Then _psh_emergency_relationship = Rdr("psh_emergency_relationship").ToString()
                        If Convert.IsDBNull(Rdr("edu_TOEFLScore")) = False Then _edu_TOEFLScore = Rdr("edu_TOEFLScore").ToString()
                        If Convert.IsDBNull(Rdr("edu_IELTsScore")) = False Then _edu_IELTsScore = Rdr("edu_IELTsScore").ToString()
                        If Convert.IsDBNull(Rdr("edu_OtherScore")) = False Then _edu_OtherScore = Rdr("edu_OtherScore").ToString()
                        If Convert.IsDBNull(Rdr("edu_trained_detail")) = False Then _edu_trained_detail = Rdr("edu_trained_detail").ToString()
                        If Convert.IsDBNull(Rdr("edu_publications")) = False Then _edu_publications = Rdr("edu_publications").ToString()
                        If Convert.IsDBNull(Rdr("exp_expectation_description")) = False Then _exp_expectation_description = Rdr("exp_expectation_description").ToString()
                        If Convert.IsDBNull(Rdr("exp_signature")) = False Then _exp_signature = CType(Rdr("exp_signature"), Byte())
                        If Convert.IsDBNull(Rdr("exp_print_name")) = False Then _exp_print_name = Rdr("exp_print_name").ToString()
                        If Convert.IsDBNull(Rdr("exp_print_date")) = False Then _exp_print_date = Convert.ToDateTime(Rdr("exp_print_date"))
                        If Convert.IsDBNull(Rdr("ga_title_post")) = False Then _ga_title_post = Rdr("ga_title_post").ToString()
                        If Convert.IsDBNull(Rdr("ga_duties")) = False Then _ga_duties = Rdr("ga_duties").ToString()
                        If Convert.IsDBNull(Rdr("ga_stamp")) = False Then _ga_stamp = Rdr("ga_stamp").ToString()
                        If Convert.IsDBNull(Rdr("ga_title")) = False Then _ga_title = Rdr("ga_title").ToString()
                        If Convert.IsDBNull(Rdr("ga_org")) = False Then _ga_org = Rdr("ga_org").ToString()
                        If Convert.IsDBNull(Rdr("ga_address")) = False Then _ga_address = Rdr("ga_address").ToString()
                        If Convert.IsDBNull(Rdr("ga_date")) = False Then _ga_date = Convert.ToDateTime(Rdr("ga_date"))
                        If Convert.IsDBNull(Rdr("profile_image")) = False Then _profile_image = CType(Rdr("profile_image"), Byte())
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                        If Convert.IsDBNull(Rdr("active_status")) = False Then _active_status = Rdr("active_status").ToString()
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table TB_Recipience
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (ACTIVITY_ID, PERSON_ID, PSH_SEX, PSH_CITY, PSH_NATIONALITY, PSH_MARITAL_STATUS, PSH_RELIGION, PSH_WORK_ADDRESS, PSH_WORK_TELEPHONE, PSH_WORK_FAX, PSH_WORK_EMAIL, PSH_HOME_ADDRESS, PSH_HOME_TELEPHONE, PSH_CITY_DEPARTTURE, PSH_EMERGENCY_CONTACT, PSH_EMERGENCY_TELEPHONE, PSH_EMERGENCY_RELATIONSHIP, EDU_TOEFLSCORE, EDU_IELTSSCORE, EDU_OTHERSCORE, EDU_TRAINED_DETAIL, EDU_PUBLICATIONS, EXP_EXPECTATION_DESCRIPTION, EXP_SIGNATURE, EXP_PRINT_NAME, EXP_PRINT_DATE, GA_TITLE_POST, GA_DUTIES, GA_STAMP, GA_TITLE, GA_ORG, GA_ADDRESS, GA_DATE, PROFILE_IMAGE, CREATED_BY, CREATED_DATE, ACTIVE_STATUS)"
                Sql += " OUTPUT INSERTED.ID, INSERTED.ACTIVITY_ID, INSERTED.PERSON_ID, INSERTED.PSH_SEX, INSERTED.PSH_CITY, INSERTED.PSH_NATIONALITY, INSERTED.PSH_MARITAL_STATUS, INSERTED.PSH_RELIGION, INSERTED.PSH_WORK_ADDRESS, INSERTED.PSH_WORK_TELEPHONE, INSERTED.PSH_WORK_FAX, INSERTED.PSH_WORK_EMAIL, INSERTED.PSH_HOME_ADDRESS, INSERTED.PSH_HOME_TELEPHONE, INSERTED.PSH_CITY_DEPARTTURE, INSERTED.PSH_EMERGENCY_CONTACT, INSERTED.PSH_EMERGENCY_TELEPHONE, INSERTED.PSH_EMERGENCY_RELATIONSHIP, INSERTED.EDU_TOEFLSCORE, INSERTED.EDU_IELTSSCORE, INSERTED.EDU_OTHERSCORE, INSERTED.EDU_TRAINED_DETAIL, INSERTED.EDU_PUBLICATIONS, INSERTED.EXP_EXPECTATION_DESCRIPTION, INSERTED.EXP_SIGNATURE, INSERTED.EXP_PRINT_NAME, INSERTED.EXP_PRINT_DATE, INSERTED.GA_TITLE_POST, INSERTED.GA_DUTIES, INSERTED.GA_STAMP, INSERTED.GA_TITLE, INSERTED.GA_ORG, INSERTED.GA_ADDRESS, INSERTED.GA_DATE, INSERTED.PROFILE_IMAGE, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE, INSERTED.ACTIVE_STATUS"
                Sql += " VALUES("
                sql += "@_ACTIVITY_ID" & ", "
                sql += "@_PERSON_ID" & ", "
                sql += "@_PSH_SEX" & ", "
                sql += "@_PSH_CITY" & ", "
                sql += "@_PSH_NATIONALITY" & ", "
                sql += "@_PSH_MARITAL_STATUS" & ", "
                sql += "@_PSH_RELIGION" & ", "
                sql += "@_PSH_WORK_ADDRESS" & ", "
                sql += "@_PSH_WORK_TELEPHONE" & ", "
                sql += "@_PSH_WORK_FAX" & ", "
                sql += "@_PSH_WORK_EMAIL" & ", "
                sql += "@_PSH_HOME_ADDRESS" & ", "
                sql += "@_PSH_HOME_TELEPHONE" & ", "
                sql += "@_PSH_CITY_DEPARTTURE" & ", "
                sql += "@_PSH_EMERGENCY_CONTACT" & ", "
                sql += "@_PSH_EMERGENCY_TELEPHONE" & ", "
                sql += "@_PSH_EMERGENCY_RELATIONSHIP" & ", "
                sql += "@_EDU_TOEFLSCORE" & ", "
                sql += "@_EDU_IELTSSCORE" & ", "
                sql += "@_EDU_OTHERSCORE" & ", "
                sql += "@_EDU_TRAINED_DETAIL" & ", "
                sql += "@_EDU_PUBLICATIONS" & ", "
                sql += "@_EXP_EXPECTATION_DESCRIPTION" & ", "
                sql += "@_EXP_SIGNATURE" & ", "
                sql += "@_EXP_PRINT_NAME" & ", "
                sql += "@_EXP_PRINT_DATE" & ", "
                sql += "@_GA_TITLE_POST" & ", "
                sql += "@_GA_DUTIES" & ", "
                sql += "@_GA_STAMP" & ", "
                sql += "@_GA_TITLE" & ", "
                sql += "@_GA_ORG" & ", "
                sql += "@_GA_ADDRESS" & ", "
                sql += "@_GA_DATE" & ", "
                sql += "@_PROFILE_IMAGE" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE" & ", "
                sql += "@_ACTIVE_STATUS"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table TB_Recipience
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "ACTIVITY_ID = " & "@_ACTIVITY_ID" & ", "
                Sql += "PERSON_ID = " & "@_PERSON_ID" & ", "
                Sql += "PSH_SEX = " & "@_PSH_SEX" & ", "
                Sql += "PSH_CITY = " & "@_PSH_CITY" & ", "
                Sql += "PSH_NATIONALITY = " & "@_PSH_NATIONALITY" & ", "
                Sql += "PSH_MARITAL_STATUS = " & "@_PSH_MARITAL_STATUS" & ", "
                Sql += "PSH_RELIGION = " & "@_PSH_RELIGION" & ", "
                Sql += "PSH_WORK_ADDRESS = " & "@_PSH_WORK_ADDRESS" & ", "
                Sql += "PSH_WORK_TELEPHONE = " & "@_PSH_WORK_TELEPHONE" & ", "
                Sql += "PSH_WORK_FAX = " & "@_PSH_WORK_FAX" & ", "
                Sql += "PSH_WORK_EMAIL = " & "@_PSH_WORK_EMAIL" & ", "
                Sql += "PSH_HOME_ADDRESS = " & "@_PSH_HOME_ADDRESS" & ", "
                Sql += "PSH_HOME_TELEPHONE = " & "@_PSH_HOME_TELEPHONE" & ", "
                Sql += "PSH_CITY_DEPARTTURE = " & "@_PSH_CITY_DEPARTTURE" & ", "
                Sql += "PSH_EMERGENCY_CONTACT = " & "@_PSH_EMERGENCY_CONTACT" & ", "
                Sql += "PSH_EMERGENCY_TELEPHONE = " & "@_PSH_EMERGENCY_TELEPHONE" & ", "
                Sql += "PSH_EMERGENCY_RELATIONSHIP = " & "@_PSH_EMERGENCY_RELATIONSHIP" & ", "
                Sql += "EDU_TOEFLSCORE = " & "@_EDU_TOEFLSCORE" & ", "
                Sql += "EDU_IELTSSCORE = " & "@_EDU_IELTSSCORE" & ", "
                Sql += "EDU_OTHERSCORE = " & "@_EDU_OTHERSCORE" & ", "
                Sql += "EDU_TRAINED_DETAIL = " & "@_EDU_TRAINED_DETAIL" & ", "
                Sql += "EDU_PUBLICATIONS = " & "@_EDU_PUBLICATIONS" & ", "
                Sql += "EXP_EXPECTATION_DESCRIPTION = " & "@_EXP_EXPECTATION_DESCRIPTION" & ", "
                Sql += "EXP_SIGNATURE = " & "@_EXP_SIGNATURE" & ", "
                Sql += "EXP_PRINT_NAME = " & "@_EXP_PRINT_NAME" & ", "
                Sql += "EXP_PRINT_DATE = " & "@_EXP_PRINT_DATE" & ", "
                Sql += "GA_TITLE_POST = " & "@_GA_TITLE_POST" & ", "
                Sql += "GA_DUTIES = " & "@_GA_DUTIES" & ", "
                Sql += "GA_STAMP = " & "@_GA_STAMP" & ", "
                Sql += "GA_TITLE = " & "@_GA_TITLE" & ", "
                Sql += "GA_ORG = " & "@_GA_ORG" & ", "
                Sql += "GA_ADDRESS = " & "@_GA_ADDRESS" & ", "
                Sql += "GA_DATE = " & "@_GA_DATE" & ", "
                Sql += "PROFILE_IMAGE = " & "@_PROFILE_IMAGE" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" & ", "
                Sql += "ACTIVE_STATUS = " & "@_ACTIVE_STATUS" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table TB_Recipience
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table TB_Recipience
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT ID, ACTIVITY_ID, PERSON_ID, PSH_SEX, PSH_CITY, PSH_NATIONALITY, PSH_MARITAL_STATUS, PSH_RELIGION, PSH_WORK_ADDRESS, PSH_WORK_TELEPHONE, PSH_WORK_FAX, PSH_WORK_EMAIL, PSH_HOME_ADDRESS, PSH_HOME_TELEPHONE, PSH_CITY_DEPARTTURE, PSH_EMERGENCY_CONTACT, PSH_EMERGENCY_TELEPHONE, PSH_EMERGENCY_RELATIONSHIP, EDU_TOEFLSCORE, EDU_IELTSSCORE, EDU_OTHERSCORE, EDU_TRAINED_DETAIL, EDU_PUBLICATIONS, EXP_EXPECTATION_DESCRIPTION, EXP_SIGNATURE, EXP_PRINT_NAME, EXP_PRINT_DATE, GA_TITLE_POST, GA_DUTIES, GA_STAMP, GA_TITLE, GA_ORG, GA_ADDRESS, GA_DATE, PROFILE_IMAGE, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE, ACTIVE_STATUS FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
