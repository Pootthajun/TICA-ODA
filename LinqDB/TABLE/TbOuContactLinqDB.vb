Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for TB_OU_Contact table LinqDB.
    '[Create by  on November, 15 2016]
    Public Class TbOuContactLinqDB
        Public sub TbOuContactLinqDB()

        End Sub 
        ' TB_OU_Contact
        Const _tableName As String = "TB_OU_Contact"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _ID As Long = 0
        Dim _PARENT_TYPE As  String  = ""
        Dim _PARENT_ID As  String  = ""
        Dim _CONTACT_NAME As  String  = ""
        Dim _ADDRESS As  String  = ""
        Dim _TELEPHONE As  String  = ""
        Dim _FAX As  String  = ""
        Dim _WEBSITE As  String  = ""
        Dim _DESCRIPTION As  String  = ""
        Dim _EMAIL As  String  = ""
        Dim _POSITION As  String  = ""

        'Generate Field Property 
        <Column(Storage:="_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property ID() As Long
            Get
                Return _ID
            End Get
            Set(ByVal value As Long)
               _ID = value
            End Set
        End Property 
        <Column(Storage:="_PARENT_TYPE", DbType:="VarChar(50)")>  _
        Public Property PARENT_TYPE() As  String 
            Get
                Return _PARENT_TYPE
            End Get
            Set(ByVal value As  String )
               _PARENT_TYPE = value
            End Set
        End Property 
        <Column(Storage:="_PARENT_ID", DbType:="VarChar(50)")>  _
        Public Property PARENT_ID() As  String 
            Get
                Return _PARENT_ID
            End Get
            Set(ByVal value As  String )
               _PARENT_ID = value
            End Set
        End Property 
        <Column(Storage:="_CONTACT_NAME", DbType:="VarChar(250)")>  _
        Public Property CONTACT_NAME() As  String 
            Get
                Return _CONTACT_NAME
            End Get
            Set(ByVal value As  String )
               _CONTACT_NAME = value
            End Set
        End Property 
        <Column(Storage:="_ADDRESS", DbType:="VarChar(500)")>  _
        Public Property ADDRESS() As  String 
            Get
                Return _ADDRESS
            End Get
            Set(ByVal value As  String )
               _ADDRESS = value
            End Set
        End Property 
        <Column(Storage:="_TELEPHONE", DbType:="VarChar(50)")>  _
        Public Property TELEPHONE() As  String 
            Get
                Return _TELEPHONE
            End Get
            Set(ByVal value As  String )
               _TELEPHONE = value
            End Set
        End Property 
        <Column(Storage:="_FAX", DbType:="VarChar(50)")>  _
        Public Property FAX() As  String 
            Get
                Return _FAX
            End Get
            Set(ByVal value As  String )
               _FAX = value
            End Set
        End Property 
        <Column(Storage:="_WEBSITE", DbType:="VarChar(250)")>  _
        Public Property WEBSITE() As  String 
            Get
                Return _WEBSITE
            End Get
            Set(ByVal value As  String )
               _WEBSITE = value
            End Set
        End Property 
        <Column(Storage:="_DESCRIPTION", DbType:="VarChar(500)")>  _
        Public Property DESCRIPTION() As  String 
            Get
                Return _DESCRIPTION
            End Get
            Set(ByVal value As  String )
               _DESCRIPTION = value
            End Set
        End Property 
        <Column(Storage:="_EMAIL", DbType:="VarChar(250)")>  _
        Public Property EMAIL() As  String 
            Get
                Return _EMAIL
            End Get
            Set(ByVal value As  String )
               _EMAIL = value
            End Set
        End Property 
        <Column(Storage:="_POSITION", DbType:="VarChar(250)")>  _
        Public Property POSITION() As  String 
            Get
                Return _POSITION
            End Get
            Set(ByVal value As  String )
               _POSITION = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _ID = 0
            _PARENT_TYPE = ""
            _PARENT_ID = ""
            _CONTACT_NAME = ""
            _ADDRESS = ""
            _TELEPHONE = ""
            _FAX = ""
            _WEBSITE = ""
            _DESCRIPTION = ""
            _EMAIL = ""
            _POSITION = ""
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into TB_OU_Contact table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_OU_Contact table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _id > 0 Then 

                    Return doUpdate("ID = @_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_OU_Contact table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from TB_OU_Contact table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_ID", cID)
                Return doDelete("ID = @_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of TB_OU_Contact by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doChkData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of TB_OU_Contact by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cID As Long, trans As SQLTransaction) As TbOuContactLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doGetData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of TB_OU_Contact by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into TB_OU_Contact table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _ID = dt.Rows(0)("ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to TB_OU_Contact table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from TB_OU_Contact table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(10) As SqlParameter
            cmbParam(0) = New SqlParameter("@_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _ID

            cmbParam(1) = New SqlParameter("@_PARENT_TYPE", SqlDbType.VarChar)
            If _PARENT_TYPE.Trim <> "" Then 
                cmbParam(1).Value = _PARENT_TYPE.Trim
            Else
                cmbParam(1).Value = DBNull.value
            End If

            cmbParam(2) = New SqlParameter("@_PARENT_ID", SqlDbType.VarChar)
            If _PARENT_ID.Trim <> "" Then 
                cmbParam(2).Value = _PARENT_ID.Trim
            Else
                cmbParam(2).Value = DBNull.value
            End If

            cmbParam(3) = New SqlParameter("@_CONTACT_NAME", SqlDbType.VarChar)
            If _CONTACT_NAME.Trim <> "" Then 
                cmbParam(3).Value = _CONTACT_NAME.Trim
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_ADDRESS", SqlDbType.VarChar)
            If _ADDRESS.Trim <> "" Then 
                cmbParam(4).Value = _ADDRESS.Trim
            Else
                cmbParam(4).Value = DBNull.value
            End If

            cmbParam(5) = New SqlParameter("@_TELEPHONE", SqlDbType.VarChar)
            If _TELEPHONE.Trim <> "" Then 
                cmbParam(5).Value = _TELEPHONE.Trim
            Else
                cmbParam(5).Value = DBNull.value
            End If

            cmbParam(6) = New SqlParameter("@_FAX", SqlDbType.VarChar)
            If _FAX.Trim <> "" Then 
                cmbParam(6).Value = _FAX.Trim
            Else
                cmbParam(6).Value = DBNull.value
            End If

            cmbParam(7) = New SqlParameter("@_WEBSITE", SqlDbType.VarChar)
            If _WEBSITE.Trim <> "" Then 
                cmbParam(7).Value = _WEBSITE.Trim
            Else
                cmbParam(7).Value = DBNull.value
            End If

            cmbParam(8) = New SqlParameter("@_DESCRIPTION", SqlDbType.VarChar)
            If _DESCRIPTION.Trim <> "" Then 
                cmbParam(8).Value = _DESCRIPTION.Trim
            Else
                cmbParam(8).Value = DBNull.value
            End If

            cmbParam(9) = New SqlParameter("@_EMAIL", SqlDbType.VarChar)
            If _EMAIL.Trim <> "" Then 
                cmbParam(9).Value = _EMAIL.Trim
            Else
                cmbParam(9).Value = DBNull.value
            End If

            cmbParam(10) = New SqlParameter("@_POSITION", SqlDbType.VarChar)
            If _POSITION.Trim <> "" Then 
                cmbParam(10).Value = _POSITION.Trim
            Else
                cmbParam(10).Value = DBNull.value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of TB_OU_Contact by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("parent_type")) = False Then _parent_type = Rdr("parent_type").ToString()
                        If Convert.IsDBNull(Rdr("parent_id")) = False Then _parent_id = Rdr("parent_id").ToString()
                        If Convert.IsDBNull(Rdr("contact_name")) = False Then _contact_name = Rdr("contact_name").ToString()
                        If Convert.IsDBNull(Rdr("address")) = False Then _address = Rdr("address").ToString()
                        If Convert.IsDBNull(Rdr("telephone")) = False Then _telephone = Rdr("telephone").ToString()
                        If Convert.IsDBNull(Rdr("fax")) = False Then _fax = Rdr("fax").ToString()
                        If Convert.IsDBNull(Rdr("website")) = False Then _website = Rdr("website").ToString()
                        If Convert.IsDBNull(Rdr("description")) = False Then _description = Rdr("description").ToString()
                        If Convert.IsDBNull(Rdr("email")) = False Then _email = Rdr("email").ToString()
                        If Convert.IsDBNull(Rdr("position")) = False Then _position = Rdr("position").ToString()
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of TB_OU_Contact by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As TbOuContactLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("parent_type")) = False Then _parent_type = Rdr("parent_type").ToString()
                        If Convert.IsDBNull(Rdr("parent_id")) = False Then _parent_id = Rdr("parent_id").ToString()
                        If Convert.IsDBNull(Rdr("contact_name")) = False Then _contact_name = Rdr("contact_name").ToString()
                        If Convert.IsDBNull(Rdr("address")) = False Then _address = Rdr("address").ToString()
                        If Convert.IsDBNull(Rdr("telephone")) = False Then _telephone = Rdr("telephone").ToString()
                        If Convert.IsDBNull(Rdr("fax")) = False Then _fax = Rdr("fax").ToString()
                        If Convert.IsDBNull(Rdr("website")) = False Then _website = Rdr("website").ToString()
                        If Convert.IsDBNull(Rdr("description")) = False Then _description = Rdr("description").ToString()
                        If Convert.IsDBNull(Rdr("email")) = False Then _email = Rdr("email").ToString()
                        If Convert.IsDBNull(Rdr("position")) = False Then _position = Rdr("position").ToString()
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table TB_OU_Contact
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (PARENT_TYPE, PARENT_ID, CONTACT_NAME, ADDRESS, TELEPHONE, FAX, WEBSITE, DESCRIPTION, EMAIL, POSITION)"
                Sql += " OUTPUT INSERTED.ID, INSERTED.PARENT_TYPE, INSERTED.PARENT_ID, INSERTED.CONTACT_NAME, INSERTED.ADDRESS, INSERTED.TELEPHONE, INSERTED.FAX, INSERTED.WEBSITE, INSERTED.DESCRIPTION, INSERTED.EMAIL, INSERTED.POSITION"
                Sql += " VALUES("
                sql += "@_PARENT_TYPE" & ", "
                sql += "@_PARENT_ID" & ", "
                sql += "@_CONTACT_NAME" & ", "
                sql += "@_ADDRESS" & ", "
                sql += "@_TELEPHONE" & ", "
                sql += "@_FAX" & ", "
                sql += "@_WEBSITE" & ", "
                sql += "@_DESCRIPTION" & ", "
                sql += "@_EMAIL" & ", "
                sql += "@_POSITION"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table TB_OU_Contact
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "PARENT_TYPE = " & "@_PARENT_TYPE" & ", "
                Sql += "PARENT_ID = " & "@_PARENT_ID" & ", "
                Sql += "CONTACT_NAME = " & "@_CONTACT_NAME" & ", "
                Sql += "ADDRESS = " & "@_ADDRESS" & ", "
                Sql += "TELEPHONE = " & "@_TELEPHONE" & ", "
                Sql += "FAX = " & "@_FAX" & ", "
                Sql += "WEBSITE = " & "@_WEBSITE" & ", "
                Sql += "DESCRIPTION = " & "@_DESCRIPTION" & ", "
                Sql += "EMAIL = " & "@_EMAIL" & ", "
                Sql += "POSITION = " & "@_POSITION" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table TB_OU_Contact
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table TB_OU_Contact
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT ID, PARENT_TYPE, PARENT_ID, CONTACT_NAME, ADDRESS, TELEPHONE, FAX, WEBSITE, DESCRIPTION, EMAIL, POSITION FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
