Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for TB_OU_Organize table LinqDB.
    '[Create by  on July, 19 2017]
    Public Class TbOuOrganizeLinqDB
        Public sub TbOuOrganizeLinqDB()

        End Sub 
        ' TB_OU_Organize
        Const _tableName As String = "TB_OU_Organize"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _ID As Long = 0
        Dim _NODE_ID As String = ""
        Dim _NAME_TH As  String  = ""
        Dim _NAME_EN As  String  = ""
        Dim _ABBR_NAME_TH As  String  = ""
        Dim _ABBR_NAME_EN As  String  = ""
        Dim _PARENT_ID As  String  = ""
        Dim _URL As  String  = ""
        Dim _FUNDING_AGENCY_TYPE As Char = "N"
        Dim _EXECUTING_AGENCY_TYPE As Char = "N"
        Dim _IMPLEMENTING_AGENCY_TYPE As Char = "N"
        Dim _ACTIVE_STATUS As  System.Nullable(Of Char)  = ""
        Dim _CREATED_BY As  String  = ""
        Dim _CREATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)

        'Generate Field Property 
        <Column(Storage:="_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property ID() As Long
            Get
                Return _ID
            End Get
            Set(ByVal value As Long)
               _ID = value
            End Set
        End Property 
        <Column(Storage:="_NODE_ID", DbType:="VarChar(50) NOT NULL ",CanBeNull:=false)>  _
        Public Property NODE_ID() As String
            Get
                Return _NODE_ID
            End Get
            Set(ByVal value As String)
               _NODE_ID = value
            End Set
        End Property 
        <Column(Storage:="_NAME_TH", DbType:="VarChar(100)")>  _
        Public Property NAME_TH() As  String 
            Get
                Return _NAME_TH
            End Get
            Set(ByVal value As  String )
               _NAME_TH = value
            End Set
        End Property 
        <Column(Storage:="_NAME_EN", DbType:="VarChar(100)")>  _
        Public Property NAME_EN() As  String 
            Get
                Return _NAME_EN
            End Get
            Set(ByVal value As  String )
               _NAME_EN = value
            End Set
        End Property 
        <Column(Storage:="_ABBR_NAME_TH", DbType:="VarChar(100)")>  _
        Public Property ABBR_NAME_TH() As  String 
            Get
                Return _ABBR_NAME_TH
            End Get
            Set(ByVal value As  String )
               _ABBR_NAME_TH = value
            End Set
        End Property 
        <Column(Storage:="_ABBR_NAME_EN", DbType:="VarChar(100)")>  _
        Public Property ABBR_NAME_EN() As  String 
            Get
                Return _ABBR_NAME_EN
            End Get
            Set(ByVal value As  String )
               _ABBR_NAME_EN = value
            End Set
        End Property 
        <Column(Storage:="_PARENT_ID", DbType:="VarChar(50)")>  _
        Public Property PARENT_ID() As  String 
            Get
                Return _PARENT_ID
            End Get
            Set(ByVal value As  String )
               _PARENT_ID = value
            End Set
        End Property 
        <Column(Storage:="_URL", DbType:="VarChar(500)")>  _
        Public Property URL() As  String 
            Get
                Return _URL
            End Get
            Set(ByVal value As  String )
               _URL = value
            End Set
        End Property 
        <Column(Storage:="_FUNDING_AGENCY_TYPE", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property FUNDING_AGENCY_TYPE() As Char
            Get
                Return _FUNDING_AGENCY_TYPE
            End Get
            Set(ByVal value As Char)
               _FUNDING_AGENCY_TYPE = value
            End Set
        End Property 
        <Column(Storage:="_EXECUTING_AGENCY_TYPE", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property EXECUTING_AGENCY_TYPE() As Char
            Get
                Return _EXECUTING_AGENCY_TYPE
            End Get
            Set(ByVal value As Char)
               _EXECUTING_AGENCY_TYPE = value
            End Set
        End Property 
        <Column(Storage:="_IMPLEMENTING_AGENCY_TYPE", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property IMPLEMENTING_AGENCY_TYPE() As Char
            Get
                Return _IMPLEMENTING_AGENCY_TYPE
            End Get
            Set(ByVal value As Char)
               _IMPLEMENTING_AGENCY_TYPE = value
            End Set
        End Property 
        <Column(Storage:="_ACTIVE_STATUS", DbType:="Char(1)")>  _
        Public Property ACTIVE_STATUS() As  System.Nullable(Of Char) 
            Get
                Return _ACTIVE_STATUS
            End Get
            Set(ByVal value As  System.Nullable(Of Char) )
               _ACTIVE_STATUS = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100)")>  _
        Public Property CREATED_BY() As  String 
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As  String )
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime")>  _
        Public Property CREATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _ID = 0
            _NODE_ID = ""
            _NAME_TH = ""
            _NAME_EN = ""
            _ABBR_NAME_TH = ""
            _ABBR_NAME_EN = ""
            _PARENT_ID = ""
            _URL = ""
            _FUNDING_AGENCY_TYPE = "N"
            _EXECUTING_AGENCY_TYPE = "N"
            _IMPLEMENTING_AGENCY_TYPE = "N"
            _ACTIVE_STATUS = ""
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into TB_OU_Organize table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_OU_Organize table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _id > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("ID = @_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_OU_Organize table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from TB_OU_Organize table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_ID", cID)
                Return doDelete("ID = @_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of TB_OU_Organize by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doChkData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of TB_OU_Organize by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cID As Long, trans As SQLTransaction) As TbOuOrganizeLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doGetData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of TB_OU_Organize by specified NODE_ID key is retrieved successfully.
        '/// <param name=cNODE_ID>The NODE_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByNODE_ID(cNODE_ID As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_NODE_ID", cNODE_ID) 
            Return doChkData("NODE_ID = @_NODE_ID", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of TB_OU_Organize by specified NODE_ID key is retrieved successfully.
        '/// <param name=cNODE_ID>The NODE_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByNODE_ID(cNODE_ID As String, cID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_NODE_ID", cNODE_ID) 
            cmdPara(1) = DB.SetBigInt("@_ID", cID) 
            Return doChkData("NODE_ID = @_NODE_ID And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of TB_OU_Organize by specified PARENT_ID key is retrieved successfully.
        '/// <param name=cPARENT_ID>The PARENT_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPARENT_ID(cPARENT_ID As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_PARENT_ID", cPARENT_ID) 
            Return doChkData("PARENT_ID = @_PARENT_ID", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of TB_OU_Organize by specified PARENT_ID key is retrieved successfully.
        '/// <param name=cPARENT_ID>The PARENT_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByPARENT_ID(cPARENT_ID As String, cID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_PARENT_ID", cPARENT_ID) 
            cmdPara(1) = DB.SetBigInt("@_ID", cID) 
            Return doChkData("PARENT_ID = @_PARENT_ID And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of TB_OU_Organize by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into TB_OU_Organize table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _ID = dt.Rows(0)("ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to TB_OU_Organize table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from TB_OU_Organize table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(15) As SqlParameter
            cmbParam(0) = New SqlParameter("@_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _ID

            cmbParam(1) = New SqlParameter("@_NODE_ID", SqlDbType.VarChar)
            cmbParam(1).Value = _NODE_ID.Trim

            cmbParam(2) = New SqlParameter("@_NAME_TH", SqlDbType.VarChar)
            If _NAME_TH.Trim <> "" Then 
                cmbParam(2).Value = _NAME_TH.Trim
            Else
                cmbParam(2).Value = DBNull.value
            End If

            cmbParam(3) = New SqlParameter("@_NAME_EN", SqlDbType.VarChar)
            If _NAME_EN.Trim <> "" Then 
                cmbParam(3).Value = _NAME_EN.Trim
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_ABBR_NAME_TH", SqlDbType.VarChar)
            If _ABBR_NAME_TH.Trim <> "" Then 
                cmbParam(4).Value = _ABBR_NAME_TH.Trim
            Else
                cmbParam(4).Value = DBNull.value
            End If

            cmbParam(5) = New SqlParameter("@_ABBR_NAME_EN", SqlDbType.VarChar)
            If _ABBR_NAME_EN.Trim <> "" Then 
                cmbParam(5).Value = _ABBR_NAME_EN.Trim
            Else
                cmbParam(5).Value = DBNull.value
            End If

            cmbParam(6) = New SqlParameter("@_PARENT_ID", SqlDbType.VarChar)
            If _PARENT_ID.Trim <> "" Then 
                cmbParam(6).Value = _PARENT_ID.Trim
            Else
                cmbParam(6).Value = DBNull.value
            End If

            cmbParam(7) = New SqlParameter("@_URL", SqlDbType.VarChar)
            If _URL.Trim <> "" Then 
                cmbParam(7).Value = _URL.Trim
            Else
                cmbParam(7).Value = DBNull.value
            End If

            cmbParam(8) = New SqlParameter("@_FUNDING_AGENCY_TYPE", SqlDbType.Char)
            cmbParam(8).Value = _FUNDING_AGENCY_TYPE

            cmbParam(9) = New SqlParameter("@_EXECUTING_AGENCY_TYPE", SqlDbType.Char)
            cmbParam(9).Value = _EXECUTING_AGENCY_TYPE

            cmbParam(10) = New SqlParameter("@_IMPLEMENTING_AGENCY_TYPE", SqlDbType.Char)
            cmbParam(10).Value = _IMPLEMENTING_AGENCY_TYPE

            cmbParam(11) = New SqlParameter("@_ACTIVE_STATUS", SqlDbType.Char)
            If _ACTIVE_STATUS.Value <> "" Then 
                cmbParam(11).Value = _ACTIVE_STATUS.Value
            Else
                cmbParam(11).Value = DBNull.value
            End IF

            cmbParam(12) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            If _CREATED_BY.Trim <> "" Then 
                cmbParam(12).Value = _CREATED_BY.Trim
            Else
                cmbParam(12).Value = DBNull.value
            End If

            cmbParam(13) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            If _CREATED_DATE.Value.Year > 1 Then 
                cmbParam(13).Value = _CREATED_DATE.Value
            Else
                cmbParam(13).Value = DBNull.value
            End If

            cmbParam(14) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(14).Value = _UPDATED_BY.Trim
            Else
                cmbParam(14).Value = DBNull.value
            End If

            cmbParam(15) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(15).Value = _UPDATED_DATE.Value
            Else
                cmbParam(15).Value = DBNull.value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of TB_OU_Organize by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("node_id")) = False Then _node_id = Rdr("node_id").ToString()
                        If Convert.IsDBNull(Rdr("name_th")) = False Then _name_th = Rdr("name_th").ToString()
                        If Convert.IsDBNull(Rdr("name_en")) = False Then _name_en = Rdr("name_en").ToString()
                        If Convert.IsDBNull(Rdr("abbr_name_th")) = False Then _abbr_name_th = Rdr("abbr_name_th").ToString()
                        If Convert.IsDBNull(Rdr("abbr_name_en")) = False Then _abbr_name_en = Rdr("abbr_name_en").ToString()
                        If Convert.IsDBNull(Rdr("parent_id")) = False Then _parent_id = Rdr("parent_id").ToString()
                        If Convert.IsDBNull(Rdr("url")) = False Then _url = Rdr("url").ToString()
                        If Convert.IsDBNull(Rdr("funding_agency_type")) = False Then _funding_agency_type = Rdr("funding_agency_type").ToString()
                        If Convert.IsDBNull(Rdr("executing_agency_type")) = False Then _executing_agency_type = Rdr("executing_agency_type").ToString()
                        If Convert.IsDBNull(Rdr("implementing_agency_type")) = False Then _implementing_agency_type = Rdr("implementing_agency_type").ToString()
                        If Convert.IsDBNull(Rdr("active_status")) = False Then _active_status = Rdr("active_status").ToString()
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of TB_OU_Organize by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As TbOuOrganizeLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("node_id")) = False Then _node_id = Rdr("node_id").ToString()
                        If Convert.IsDBNull(Rdr("name_th")) = False Then _name_th = Rdr("name_th").ToString()
                        If Convert.IsDBNull(Rdr("name_en")) = False Then _name_en = Rdr("name_en").ToString()
                        If Convert.IsDBNull(Rdr("abbr_name_th")) = False Then _abbr_name_th = Rdr("abbr_name_th").ToString()
                        If Convert.IsDBNull(Rdr("abbr_name_en")) = False Then _abbr_name_en = Rdr("abbr_name_en").ToString()
                        If Convert.IsDBNull(Rdr("parent_id")) = False Then _parent_id = Rdr("parent_id").ToString()
                        If Convert.IsDBNull(Rdr("url")) = False Then _url = Rdr("url").ToString()
                        If Convert.IsDBNull(Rdr("funding_agency_type")) = False Then _funding_agency_type = Rdr("funding_agency_type").ToString()
                        If Convert.IsDBNull(Rdr("executing_agency_type")) = False Then _executing_agency_type = Rdr("executing_agency_type").ToString()
                        If Convert.IsDBNull(Rdr("implementing_agency_type")) = False Then _implementing_agency_type = Rdr("implementing_agency_type").ToString()
                        If Convert.IsDBNull(Rdr("active_status")) = False Then _active_status = Rdr("active_status").ToString()
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table TB_OU_Organize
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (NODE_ID, NAME_TH, NAME_EN, ABBR_NAME_TH, ABBR_NAME_EN, PARENT_ID, URL, FUNDING_AGENCY_TYPE, EXECUTING_AGENCY_TYPE, IMPLEMENTING_AGENCY_TYPE, ACTIVE_STATUS, CREATED_BY, CREATED_DATE)"
                Sql += " OUTPUT INSERTED.ID, INSERTED.NODE_ID, INSERTED.NAME_TH, INSERTED.NAME_EN, INSERTED.ABBR_NAME_TH, INSERTED.ABBR_NAME_EN, INSERTED.PARENT_ID, INSERTED.URL, INSERTED.FUNDING_AGENCY_TYPE, INSERTED.EXECUTING_AGENCY_TYPE, INSERTED.IMPLEMENTING_AGENCY_TYPE, INSERTED.ACTIVE_STATUS, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE"
                Sql += " VALUES("
                sql += "@_NODE_ID" & ", "
                sql += "@_NAME_TH" & ", "
                sql += "@_NAME_EN" & ", "
                sql += "@_ABBR_NAME_TH" & ", "
                sql += "@_ABBR_NAME_EN" & ", "
                sql += "@_PARENT_ID" & ", "
                sql += "@_URL" & ", "
                sql += "@_FUNDING_AGENCY_TYPE" & ", "
                sql += "@_EXECUTING_AGENCY_TYPE" & ", "
                sql += "@_IMPLEMENTING_AGENCY_TYPE" & ", "
                sql += "@_ACTIVE_STATUS" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table TB_OU_Organize
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "NODE_ID = " & "@_NODE_ID" & ", "
                Sql += "NAME_TH = " & "@_NAME_TH" & ", "
                Sql += "NAME_EN = " & "@_NAME_EN" & ", "
                Sql += "ABBR_NAME_TH = " & "@_ABBR_NAME_TH" & ", "
                Sql += "ABBR_NAME_EN = " & "@_ABBR_NAME_EN" & ", "
                Sql += "PARENT_ID = " & "@_PARENT_ID" & ", "
                Sql += "URL = " & "@_URL" & ", "
                Sql += "FUNDING_AGENCY_TYPE = " & "@_FUNDING_AGENCY_TYPE" & ", "
                Sql += "EXECUTING_AGENCY_TYPE = " & "@_EXECUTING_AGENCY_TYPE" & ", "
                Sql += "IMPLEMENTING_AGENCY_TYPE = " & "@_IMPLEMENTING_AGENCY_TYPE" & ", "
                Sql += "ACTIVE_STATUS = " & "@_ACTIVE_STATUS" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table TB_OU_Organize
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table TB_OU_Organize
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT ID, NODE_ID, NAME_TH, NAME_EN, ABBR_NAME_TH, ABBR_NAME_EN, PARENT_ID, URL, FUNDING_AGENCY_TYPE, EXECUTING_AGENCY_TYPE, IMPLEMENTING_AGENCY_TYPE, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
