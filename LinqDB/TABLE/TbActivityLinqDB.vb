Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for TB_Activity table LinqDB.
    '[Create by  on July, 4 2017]
    Public Class TbActivityLinqDB
        Public sub TbActivityLinqDB()

        End Sub 
        ' TB_Activity
        Const _tableName As String = "TB_Activity"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _ID As Long = 0
        Dim _ACTIVITY_NAME As  String  = ""
        Dim _DESCRIPTION As  String  = ""
        Dim _SECTOR_ID As  System.Nullable(Of Long) 
        Dim _SUB_SECTOR_ID As  System.Nullable(Of Long) 
        Dim _PLAN_START As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _PLAN_END As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _ACTUAL_START As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _ACTUAL_END As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _COMMITMENT_BUDGET As  System.Nullable(Of Double) 
        Dim _DISBURSEMENT As  System.Nullable(Of Double) 
        Dim _ADMINISTRATIVE As  System.Nullable(Of Double) 
        Dim _ACTIVITY_STATUS As  String  = ""
        Dim _ASSISTANCE_TYPE As  String  = ""
        Dim _BENEFICIARY As  String  = ""
        Dim _NOTIFY As Char = "Y"
        Dim _PROJECT_ID As  System.Nullable(Of Long) 
        Dim _PARENT_ID As  System.Nullable(Of Long) 
        Dim _COMMITMENT_BUDGET_LOAN As  System.Nullable(Of Double) 
        Dim _DISBURSEMENT_LOAN As  System.Nullable(Of Double) 
        Dim _PAYMENT_DATE_GANT As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _PAYMENT_DATE_LOAN As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _PAY_AS As  String  = ""
        Dim _PAYMENT_DATE_CONTRIBUTION As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _CREATED_BY As  String  = ""
        Dim _CREATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _ACTIVE_STATUS As  System.Nullable(Of Char)  = ""
        Dim _DISBURSEMENT_TYPE As  String  = ""
        Dim _RECIPIENT_TYPE As  String  = ""

        'Generate Field Property 
        <Column(Storage:="_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property ID() As Long
            Get
                Return _ID
            End Get
            Set(ByVal value As Long)
               _ID = value
            End Set
        End Property 
        <Column(Storage:="_ACTIVITY_NAME", DbType:="VarChar(250)")>  _
        Public Property ACTIVITY_NAME() As  String 
            Get
                Return _ACTIVITY_NAME
            End Get
            Set(ByVal value As  String )
               _ACTIVITY_NAME = value
            End Set
        End Property 
        <Column(Storage:="_DESCRIPTION", DbType:="Text")>  _
        Public Property DESCRIPTION() As  String 
            Get
                Return _DESCRIPTION
            End Get
            Set(ByVal value As  String )
               _DESCRIPTION = value
            End Set
        End Property 
        <Column(Storage:="_SECTOR_ID", DbType:="BigInt")>  _
        Public Property SECTOR_ID() As  System.Nullable(Of Long) 
            Get
                Return _SECTOR_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _SECTOR_ID = value
            End Set
        End Property 
        <Column(Storage:="_SUB_SECTOR_ID", DbType:="BigInt")>  _
        Public Property SUB_SECTOR_ID() As  System.Nullable(Of Long) 
            Get
                Return _SUB_SECTOR_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _SUB_SECTOR_ID = value
            End Set
        End Property 
        <Column(Storage:="_PLAN_START", DbType:="DateTime")>  _
        Public Property PLAN_START() As  System.Nullable(Of DateTime) 
            Get
                Return _PLAN_START
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _PLAN_START = value
            End Set
        End Property 
        <Column(Storage:="_PLAN_END", DbType:="DateTime")>  _
        Public Property PLAN_END() As  System.Nullable(Of DateTime) 
            Get
                Return _PLAN_END
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _PLAN_END = value
            End Set
        End Property 
        <Column(Storage:="_ACTUAL_START", DbType:="DateTime")>  _
        Public Property ACTUAL_START() As  System.Nullable(Of DateTime) 
            Get
                Return _ACTUAL_START
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _ACTUAL_START = value
            End Set
        End Property 
        <Column(Storage:="_ACTUAL_END", DbType:="DateTime")>  _
        Public Property ACTUAL_END() As  System.Nullable(Of DateTime) 
            Get
                Return _ACTUAL_END
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _ACTUAL_END = value
            End Set
        End Property 
        <Column(Storage:="_COMMITMENT_BUDGET", DbType:="Double")>  _
        Public Property COMMITMENT_BUDGET() As  System.Nullable(Of Double) 
            Get
                Return _COMMITMENT_BUDGET
            End Get
            Set(ByVal value As  System.Nullable(Of Double) )
               _COMMITMENT_BUDGET = value
            End Set
        End Property 
        <Column(Storage:="_DISBURSEMENT", DbType:="Double")>  _
        Public Property DISBURSEMENT() As  System.Nullable(Of Double) 
            Get
                Return _DISBURSEMENT
            End Get
            Set(ByVal value As  System.Nullable(Of Double) )
               _DISBURSEMENT = value
            End Set
        End Property 
        <Column(Storage:="_ADMINISTRATIVE", DbType:="Double")>  _
        Public Property ADMINISTRATIVE() As  System.Nullable(Of Double) 
            Get
                Return _ADMINISTRATIVE
            End Get
            Set(ByVal value As  System.Nullable(Of Double) )
               _ADMINISTRATIVE = value
            End Set
        End Property 
        <Column(Storage:="_ACTIVITY_STATUS", DbType:="VarChar(20)")>  _
        Public Property ACTIVITY_STATUS() As  String 
            Get
                Return _ACTIVITY_STATUS
            End Get
            Set(ByVal value As  String )
               _ACTIVITY_STATUS = value
            End Set
        End Property 
        <Column(Storage:="_ASSISTANCE_TYPE", DbType:="VarChar(1)")>  _
        Public Property ASSISTANCE_TYPE() As  String 
            Get
                Return _ASSISTANCE_TYPE
            End Get
            Set(ByVal value As  String )
               _ASSISTANCE_TYPE = value
            End Set
        End Property 
        <Column(Storage:="_BENEFICIARY", DbType:="Text")>  _
        Public Property BENEFICIARY() As  String 
            Get
                Return _BENEFICIARY
            End Get
            Set(ByVal value As  String )
               _BENEFICIARY = value
            End Set
        End Property 
        <Column(Storage:="_NOTIFY", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property NOTIFY() As Char
            Get
                Return _NOTIFY
            End Get
            Set(ByVal value As Char)
               _NOTIFY = value
            End Set
        End Property 
        <Column(Storage:="_PROJECT_ID", DbType:="BigInt")>  _
        Public Property PROJECT_ID() As  System.Nullable(Of Long) 
            Get
                Return _PROJECT_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _PROJECT_ID = value
            End Set
        End Property 
        <Column(Storage:="_PARENT_ID", DbType:="BigInt")>  _
        Public Property PARENT_ID() As  System.Nullable(Of Long) 
            Get
                Return _PARENT_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _PARENT_ID = value
            End Set
        End Property 
        <Column(Storage:="_COMMITMENT_BUDGET_LOAN", DbType:="Double")>  _
        Public Property COMMITMENT_BUDGET_LOAN() As  System.Nullable(Of Double) 
            Get
                Return _COMMITMENT_BUDGET_LOAN
            End Get
            Set(ByVal value As  System.Nullable(Of Double) )
               _COMMITMENT_BUDGET_LOAN = value
            End Set
        End Property 
        <Column(Storage:="_DISBURSEMENT_LOAN", DbType:="Double")>  _
        Public Property DISBURSEMENT_LOAN() As  System.Nullable(Of Double) 
            Get
                Return _DISBURSEMENT_LOAN
            End Get
            Set(ByVal value As  System.Nullable(Of Double) )
               _DISBURSEMENT_LOAN = value
            End Set
        End Property 
        <Column(Storage:="_PAYMENT_DATE_GANT", DbType:="DateTime")>  _
        Public Property PAYMENT_DATE_GANT() As  System.Nullable(Of DateTime) 
            Get
                Return _PAYMENT_DATE_GANT
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _PAYMENT_DATE_GANT = value
            End Set
        End Property 
        <Column(Storage:="_PAYMENT_DATE_LOAN", DbType:="DateTime")>  _
        Public Property PAYMENT_DATE_LOAN() As  System.Nullable(Of DateTime) 
            Get
                Return _PAYMENT_DATE_LOAN
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _PAYMENT_DATE_LOAN = value
            End Set
        End Property 
        <Column(Storage:="_PAY_AS", DbType:="VarChar(1)")>  _
        Public Property PAY_AS() As  String 
            Get
                Return _PAY_AS
            End Get
            Set(ByVal value As  String )
               _PAY_AS = value
            End Set
        End Property 
        <Column(Storage:="_PAYMENT_DATE_CONTRIBUTION", DbType:="DateTime")>  _
        Public Property PAYMENT_DATE_CONTRIBUTION() As  System.Nullable(Of DateTime) 
            Get
                Return _PAYMENT_DATE_CONTRIBUTION
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _PAYMENT_DATE_CONTRIBUTION = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100)")>  _
        Public Property CREATED_BY() As  String 
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As  String )
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime")>  _
        Public Property CREATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_ACTIVE_STATUS", DbType:="Char(1)")>  _
        Public Property ACTIVE_STATUS() As  System.Nullable(Of Char) 
            Get
                Return _ACTIVE_STATUS
            End Get
            Set(ByVal value As  System.Nullable(Of Char) )
               _ACTIVE_STATUS = value
            End Set
        End Property 
        <Column(Storage:="_DISBURSEMENT_TYPE", DbType:="VarChar(1)")>  _
        Public Property DISBURSEMENT_TYPE() As  String 
            Get
                Return _DISBURSEMENT_TYPE
            End Get
            Set(ByVal value As  String )
               _DISBURSEMENT_TYPE = value
            End Set
        End Property 
        <Column(Storage:="_RECIPIENT_TYPE", DbType:="VarChar(1)")>  _
        Public Property RECIPIENT_TYPE() As  String 
            Get
                Return _RECIPIENT_TYPE
            End Get
            Set(ByVal value As  String )
               _RECIPIENT_TYPE = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _ID = 0
            _ACTIVITY_NAME = ""
            _DESCRIPTION = ""
            _SECTOR_ID = Nothing
            _SUB_SECTOR_ID = Nothing
            _PLAN_START = New DateTime(1,1,1)
            _PLAN_END = New DateTime(1,1,1)
            _ACTUAL_START = New DateTime(1,1,1)
            _ACTUAL_END = New DateTime(1,1,1)
            _COMMITMENT_BUDGET = Nothing
            _DISBURSEMENT = Nothing
            _ADMINISTRATIVE = Nothing
            _ACTIVITY_STATUS = ""
            _ASSISTANCE_TYPE = ""
            _BENEFICIARY = ""
            _NOTIFY = "Y"
            _PROJECT_ID = Nothing
            _PARENT_ID = Nothing
            _COMMITMENT_BUDGET_LOAN = Nothing
            _DISBURSEMENT_LOAN = Nothing
            _PAYMENT_DATE_GANT = New DateTime(1,1,1)
            _PAYMENT_DATE_LOAN = New DateTime(1,1,1)
            _PAY_AS = ""
            _PAYMENT_DATE_CONTRIBUTION = New DateTime(1,1,1)
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
            _ACTIVE_STATUS = ""
            _DISBURSEMENT_TYPE = ""
            _RECIPIENT_TYPE = ""
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into TB_Activity table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_Activity table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _id > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("ID = @_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_Activity table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from TB_Activity table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_ID", cID)
                Return doDelete("ID = @_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of TB_Activity by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doChkData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of TB_Activity by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cID As Long, trans As SQLTransaction) As TbActivityLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doGetData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of TB_Activity by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into TB_Activity table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _ID = dt.Rows(0)("ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to TB_Activity table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from TB_Activity table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(30) As SqlParameter
            cmbParam(0) = New SqlParameter("@_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _ID

            cmbParam(1) = New SqlParameter("@_ACTIVITY_NAME", SqlDbType.VarChar)
            If _ACTIVITY_NAME.Trim <> "" Then 
                cmbParam(1).Value = _ACTIVITY_NAME.Trim
            Else
                cmbParam(1).Value = DBNull.value
            End If

            cmbParam(2) = New SqlParameter("@_DESCRIPTION", SqlDbType.Text)
            If _DESCRIPTION IsNot Nothing Then 
                cmbParam(2).Value = _DESCRIPTION.Trim
            Else
                cmbParam(2).Value = DBNull.value
            End IF

            cmbParam(3) = New SqlParameter("@_SECTOR_ID", SqlDbType.BigInt)
            If _SECTOR_ID IsNot Nothing Then 
                cmbParam(3).Value = _SECTOR_ID.Value
            Else
                cmbParam(3).Value = DBNull.value
            End IF

            cmbParam(4) = New SqlParameter("@_SUB_SECTOR_ID", SqlDbType.BigInt)
            If _SUB_SECTOR_ID IsNot Nothing Then 
                cmbParam(4).Value = _SUB_SECTOR_ID.Value
            Else
                cmbParam(4).Value = DBNull.value
            End IF

            cmbParam(5) = New SqlParameter("@_PLAN_START", SqlDbType.DateTime)
            If _PLAN_START.Value.Year > 1 Then 
                cmbParam(5).Value = _PLAN_START.Value
            Else
                cmbParam(5).Value = DBNull.value
            End If

            cmbParam(6) = New SqlParameter("@_PLAN_END", SqlDbType.DateTime)
            If _PLAN_END.Value.Year > 1 Then 
                cmbParam(6).Value = _PLAN_END.Value
            Else
                cmbParam(6).Value = DBNull.value
            End If

            cmbParam(7) = New SqlParameter("@_ACTUAL_START", SqlDbType.DateTime)
            If _ACTUAL_START.Value.Year > 1 Then 
                cmbParam(7).Value = _ACTUAL_START.Value
            Else
                cmbParam(7).Value = DBNull.value
            End If

            cmbParam(8) = New SqlParameter("@_ACTUAL_END", SqlDbType.DateTime)
            If _ACTUAL_END.Value.Year > 1 Then 
                cmbParam(8).Value = _ACTUAL_END.Value
            Else
                cmbParam(8).Value = DBNull.value
            End If

            cmbParam(9) = New SqlParameter("@_COMMITMENT_BUDGET", SqlDbType.Decimal)
            If _COMMITMENT_BUDGET IsNot Nothing Then 
                cmbParam(9).Value = _COMMITMENT_BUDGET.Value
            Else
                cmbParam(9).Value = DBNull.value
            End IF

            cmbParam(10) = New SqlParameter("@_DISBURSEMENT", SqlDbType.Decimal)
            If _DISBURSEMENT IsNot Nothing Then 
                cmbParam(10).Value = _DISBURSEMENT.Value
            Else
                cmbParam(10).Value = DBNull.value
            End IF

            cmbParam(11) = New SqlParameter("@_ADMINISTRATIVE", SqlDbType.Decimal)
            If _ADMINISTRATIVE IsNot Nothing Then 
                cmbParam(11).Value = _ADMINISTRATIVE.Value
            Else
                cmbParam(11).Value = DBNull.value
            End IF

            cmbParam(12) = New SqlParameter("@_ACTIVITY_STATUS", SqlDbType.VarChar)
            If _ACTIVITY_STATUS.Trim <> "" Then 
                cmbParam(12).Value = _ACTIVITY_STATUS.Trim
            Else
                cmbParam(12).Value = DBNull.value
            End If

            cmbParam(13) = New SqlParameter("@_ASSISTANCE_TYPE", SqlDbType.VarChar)
            If _ASSISTANCE_TYPE.Trim <> "" Then 
                cmbParam(13).Value = _ASSISTANCE_TYPE.Trim
            Else
                cmbParam(13).Value = DBNull.value
            End If

            cmbParam(14) = New SqlParameter("@_BENEFICIARY", SqlDbType.Text)
            If _BENEFICIARY IsNot Nothing Then 
                cmbParam(14).Value = _BENEFICIARY.Trim
            Else
                cmbParam(14).Value = DBNull.value
            End IF

            cmbParam(15) = New SqlParameter("@_NOTIFY", SqlDbType.Char)
            cmbParam(15).Value = _NOTIFY

            cmbParam(16) = New SqlParameter("@_PROJECT_ID", SqlDbType.BigInt)
            If _PROJECT_ID IsNot Nothing Then 
                cmbParam(16).Value = _PROJECT_ID.Value
            Else
                cmbParam(16).Value = DBNull.value
            End IF

            cmbParam(17) = New SqlParameter("@_PARENT_ID", SqlDbType.BigInt)
            If _PARENT_ID IsNot Nothing Then 
                cmbParam(17).Value = _PARENT_ID.Value
            Else
                cmbParam(17).Value = DBNull.value
            End IF

            cmbParam(18) = New SqlParameter("@_COMMITMENT_BUDGET_LOAN", SqlDbType.Decimal)
            If _COMMITMENT_BUDGET_LOAN IsNot Nothing Then 
                cmbParam(18).Value = _COMMITMENT_BUDGET_LOAN.Value
            Else
                cmbParam(18).Value = DBNull.value
            End IF

            cmbParam(19) = New SqlParameter("@_DISBURSEMENT_LOAN", SqlDbType.Decimal)
            If _DISBURSEMENT_LOAN IsNot Nothing Then 
                cmbParam(19).Value = _DISBURSEMENT_LOAN.Value
            Else
                cmbParam(19).Value = DBNull.value
            End IF

            cmbParam(20) = New SqlParameter("@_PAYMENT_DATE_GANT", SqlDbType.DateTime)
            If _PAYMENT_DATE_GANT.Value.Year > 1 Then 
                cmbParam(20).Value = _PAYMENT_DATE_GANT.Value
            Else
                cmbParam(20).Value = DBNull.value
            End If

            cmbParam(21) = New SqlParameter("@_PAYMENT_DATE_LOAN", SqlDbType.DateTime)
            If _PAYMENT_DATE_LOAN.Value.Year > 1 Then 
                cmbParam(21).Value = _PAYMENT_DATE_LOAN.Value
            Else
                cmbParam(21).Value = DBNull.value
            End If

            cmbParam(22) = New SqlParameter("@_PAY_AS", SqlDbType.VarChar)
            If _PAY_AS.Trim <> "" Then 
                cmbParam(22).Value = _PAY_AS.Trim
            Else
                cmbParam(22).Value = DBNull.value
            End If

            cmbParam(23) = New SqlParameter("@_PAYMENT_DATE_CONTRIBUTION", SqlDbType.DateTime)
            If _PAYMENT_DATE_CONTRIBUTION.Value.Year > 1 Then 
                cmbParam(23).Value = _PAYMENT_DATE_CONTRIBUTION.Value
            Else
                cmbParam(23).Value = DBNull.value
            End If

            cmbParam(24) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            If _CREATED_BY.Trim <> "" Then 
                cmbParam(24).Value = _CREATED_BY.Trim
            Else
                cmbParam(24).Value = DBNull.value
            End If

            cmbParam(25) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            If _CREATED_DATE.Value.Year > 1 Then 
                cmbParam(25).Value = _CREATED_DATE.Value
            Else
                cmbParam(25).Value = DBNull.value
            End If

            cmbParam(26) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(26).Value = _UPDATED_BY.Trim
            Else
                cmbParam(26).Value = DBNull.value
            End If

            cmbParam(27) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(27).Value = _UPDATED_DATE.Value
            Else
                cmbParam(27).Value = DBNull.value
            End If

            cmbParam(28) = New SqlParameter("@_ACTIVE_STATUS", SqlDbType.Char)
            If _ACTIVE_STATUS.Value <> "" Then 
                cmbParam(28).Value = _ACTIVE_STATUS.Value
            Else
                cmbParam(28).Value = DBNull.value
            End IF

            cmbParam(29) = New SqlParameter("@_DISBURSEMENT_TYPE", SqlDbType.VarChar)
            If _DISBURSEMENT_TYPE.Trim <> "" Then 
                cmbParam(29).Value = _DISBURSEMENT_TYPE.Trim
            Else
                cmbParam(29).Value = DBNull.value
            End If

            cmbParam(30) = New SqlParameter("@_RECIPIENT_TYPE", SqlDbType.VarChar)
            If _RECIPIENT_TYPE.Trim <> "" Then 
                cmbParam(30).Value = _RECIPIENT_TYPE.Trim
            Else
                cmbParam(30).Value = DBNull.value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of TB_Activity by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("activity_name")) = False Then _activity_name = Rdr("activity_name").ToString()
                        If Convert.IsDBNull(Rdr("description")) = False Then _description = Rdr("description").ToString()
                        If Convert.IsDBNull(Rdr("sector_id")) = False Then _sector_id = Convert.ToInt64(Rdr("sector_id"))
                        If Convert.IsDBNull(Rdr("sub_sector_id")) = False Then _sub_sector_id = Convert.ToInt64(Rdr("sub_sector_id"))
                        If Convert.IsDBNull(Rdr("plan_start")) = False Then _plan_start = Convert.ToDateTime(Rdr("plan_start"))
                        If Convert.IsDBNull(Rdr("plan_end")) = False Then _plan_end = Convert.ToDateTime(Rdr("plan_end"))
                        If Convert.IsDBNull(Rdr("actual_start")) = False Then _actual_start = Convert.ToDateTime(Rdr("actual_start"))
                        If Convert.IsDBNull(Rdr("actual_end")) = False Then _actual_end = Convert.ToDateTime(Rdr("actual_end"))
                        If Convert.IsDBNull(Rdr("commitment_budget")) = False Then _commitment_budget = Convert.ToDouble(Rdr("commitment_budget"))
                        If Convert.IsDBNull(Rdr("disbursement")) = False Then _disbursement = Convert.ToDouble(Rdr("disbursement"))
                        If Convert.IsDBNull(Rdr("administrative")) = False Then _administrative = Convert.ToDouble(Rdr("administrative"))
                        If Convert.IsDBNull(Rdr("activity_status")) = False Then _activity_status = Rdr("activity_status").ToString()
                        If Convert.IsDBNull(Rdr("assistance_type")) = False Then _assistance_type = Rdr("assistance_type").ToString()
                        If Convert.IsDBNull(Rdr("beneficiary")) = False Then _beneficiary = Rdr("beneficiary").ToString()
                        If Convert.IsDBNull(Rdr("notify")) = False Then _notify = Rdr("notify").ToString()
                        If Convert.IsDBNull(Rdr("project_id")) = False Then _project_id = Convert.ToInt64(Rdr("project_id"))
                        If Convert.IsDBNull(Rdr("parent_id")) = False Then _parent_id = Convert.ToInt64(Rdr("parent_id"))
                        If Convert.IsDBNull(Rdr("commitment_budget_loan")) = False Then _commitment_budget_loan = Convert.ToDouble(Rdr("commitment_budget_loan"))
                        If Convert.IsDBNull(Rdr("disbursement_loan")) = False Then _disbursement_loan = Convert.ToDouble(Rdr("disbursement_loan"))
                        If Convert.IsDBNull(Rdr("payment_date_gant")) = False Then _payment_date_gant = Convert.ToDateTime(Rdr("payment_date_gant"))
                        If Convert.IsDBNull(Rdr("payment_date_loan")) = False Then _payment_date_loan = Convert.ToDateTime(Rdr("payment_date_loan"))
                        If Convert.IsDBNull(Rdr("pay_as")) = False Then _pay_as = Rdr("pay_as").ToString()
                        If Convert.IsDBNull(Rdr("payment_date_contribution")) = False Then _payment_date_contribution = Convert.ToDateTime(Rdr("payment_date_contribution"))
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                        If Convert.IsDBNull(Rdr("active_status")) = False Then _active_status = Rdr("active_status").ToString()
                        If Convert.IsDBNull(Rdr("disbursement_type")) = False Then _disbursement_type = Rdr("disbursement_type").ToString()
                        If Convert.IsDBNull(Rdr("Recipient_Type")) = False Then _Recipient_Type = Rdr("Recipient_Type").ToString()
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of TB_Activity by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As TbActivityLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("activity_name")) = False Then _activity_name = Rdr("activity_name").ToString()
                        If Convert.IsDBNull(Rdr("description")) = False Then _description = Rdr("description").ToString()
                        If Convert.IsDBNull(Rdr("sector_id")) = False Then _sector_id = Convert.ToInt64(Rdr("sector_id"))
                        If Convert.IsDBNull(Rdr("sub_sector_id")) = False Then _sub_sector_id = Convert.ToInt64(Rdr("sub_sector_id"))
                        If Convert.IsDBNull(Rdr("plan_start")) = False Then _plan_start = Convert.ToDateTime(Rdr("plan_start"))
                        If Convert.IsDBNull(Rdr("plan_end")) = False Then _plan_end = Convert.ToDateTime(Rdr("plan_end"))
                        If Convert.IsDBNull(Rdr("actual_start")) = False Then _actual_start = Convert.ToDateTime(Rdr("actual_start"))
                        If Convert.IsDBNull(Rdr("actual_end")) = False Then _actual_end = Convert.ToDateTime(Rdr("actual_end"))
                        If Convert.IsDBNull(Rdr("commitment_budget")) = False Then _commitment_budget = Convert.ToDouble(Rdr("commitment_budget"))
                        If Convert.IsDBNull(Rdr("disbursement")) = False Then _disbursement = Convert.ToDouble(Rdr("disbursement"))
                        If Convert.IsDBNull(Rdr("administrative")) = False Then _administrative = Convert.ToDouble(Rdr("administrative"))
                        If Convert.IsDBNull(Rdr("activity_status")) = False Then _activity_status = Rdr("activity_status").ToString()
                        If Convert.IsDBNull(Rdr("assistance_type")) = False Then _assistance_type = Rdr("assistance_type").ToString()
                        If Convert.IsDBNull(Rdr("beneficiary")) = False Then _beneficiary = Rdr("beneficiary").ToString()
                        If Convert.IsDBNull(Rdr("notify")) = False Then _notify = Rdr("notify").ToString()
                        If Convert.IsDBNull(Rdr("project_id")) = False Then _project_id = Convert.ToInt64(Rdr("project_id"))
                        If Convert.IsDBNull(Rdr("parent_id")) = False Then _parent_id = Convert.ToInt64(Rdr("parent_id"))
                        If Convert.IsDBNull(Rdr("commitment_budget_loan")) = False Then _commitment_budget_loan = Convert.ToDouble(Rdr("commitment_budget_loan"))
                        If Convert.IsDBNull(Rdr("disbursement_loan")) = False Then _disbursement_loan = Convert.ToDouble(Rdr("disbursement_loan"))
                        If Convert.IsDBNull(Rdr("payment_date_gant")) = False Then _payment_date_gant = Convert.ToDateTime(Rdr("payment_date_gant"))
                        If Convert.IsDBNull(Rdr("payment_date_loan")) = False Then _payment_date_loan = Convert.ToDateTime(Rdr("payment_date_loan"))
                        If Convert.IsDBNull(Rdr("pay_as")) = False Then _pay_as = Rdr("pay_as").ToString()
                        If Convert.IsDBNull(Rdr("payment_date_contribution")) = False Then _payment_date_contribution = Convert.ToDateTime(Rdr("payment_date_contribution"))
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                        If Convert.IsDBNull(Rdr("active_status")) = False Then _active_status = Rdr("active_status").ToString()
                        If Convert.IsDBNull(Rdr("disbursement_type")) = False Then _disbursement_type = Rdr("disbursement_type").ToString()
                        If Convert.IsDBNull(Rdr("Recipient_Type")) = False Then _Recipient_Type = Rdr("Recipient_Type").ToString()
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table TB_Activity
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (ACTIVITY_NAME, DESCRIPTION, SECTOR_ID, SUB_SECTOR_ID, PLAN_START, PLAN_END, ACTUAL_START, ACTUAL_END, COMMITMENT_BUDGET, DISBURSEMENT, ADMINISTRATIVE, ACTIVITY_STATUS, ASSISTANCE_TYPE, BENEFICIARY, NOTIFY, PROJECT_ID, PARENT_ID, COMMITMENT_BUDGET_LOAN, DISBURSEMENT_LOAN, PAYMENT_DATE_GANT, PAYMENT_DATE_LOAN, PAY_AS, PAYMENT_DATE_CONTRIBUTION, CREATED_BY, CREATED_DATE, ACTIVE_STATUS, DISBURSEMENT_TYPE, RECIPIENT_TYPE)"
                Sql += " OUTPUT INSERTED.ID, INSERTED.ACTIVITY_NAME, INSERTED.DESCRIPTION, INSERTED.SECTOR_ID, INSERTED.SUB_SECTOR_ID, INSERTED.PLAN_START, INSERTED.PLAN_END, INSERTED.ACTUAL_START, INSERTED.ACTUAL_END, INSERTED.COMMITMENT_BUDGET, INSERTED.DISBURSEMENT, INSERTED.ADMINISTRATIVE, INSERTED.ACTIVITY_STATUS, INSERTED.ASSISTANCE_TYPE, INSERTED.BENEFICIARY, INSERTED.NOTIFY, INSERTED.PROJECT_ID, INSERTED.PARENT_ID, INSERTED.COMMITMENT_BUDGET_LOAN, INSERTED.DISBURSEMENT_LOAN, INSERTED.PAYMENT_DATE_GANT, INSERTED.PAYMENT_DATE_LOAN, INSERTED.PAY_AS, INSERTED.PAYMENT_DATE_CONTRIBUTION, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE, INSERTED.ACTIVE_STATUS, INSERTED.DISBURSEMENT_TYPE, INSERTED.RECIPIENT_TYPE"
                Sql += " VALUES("
                sql += "@_ACTIVITY_NAME" & ", "
                sql += "@_DESCRIPTION" & ", "
                sql += "@_SECTOR_ID" & ", "
                sql += "@_SUB_SECTOR_ID" & ", "
                sql += "@_PLAN_START" & ", "
                sql += "@_PLAN_END" & ", "
                sql += "@_ACTUAL_START" & ", "
                sql += "@_ACTUAL_END" & ", "
                sql += "@_COMMITMENT_BUDGET" & ", "
                sql += "@_DISBURSEMENT" & ", "
                sql += "@_ADMINISTRATIVE" & ", "
                sql += "@_ACTIVITY_STATUS" & ", "
                sql += "@_ASSISTANCE_TYPE" & ", "
                sql += "@_BENEFICIARY" & ", "
                sql += "@_NOTIFY" & ", "
                sql += "@_PROJECT_ID" & ", "
                sql += "@_PARENT_ID" & ", "
                sql += "@_COMMITMENT_BUDGET_LOAN" & ", "
                sql += "@_DISBURSEMENT_LOAN" & ", "
                sql += "@_PAYMENT_DATE_GANT" & ", "
                sql += "@_PAYMENT_DATE_LOAN" & ", "
                sql += "@_PAY_AS" & ", "
                sql += "@_PAYMENT_DATE_CONTRIBUTION" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE" & ", "
                sql += "@_ACTIVE_STATUS" & ", "
                sql += "@_DISBURSEMENT_TYPE" & ", "
                sql += "@_RECIPIENT_TYPE"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table TB_Activity
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "ACTIVITY_NAME = " & "@_ACTIVITY_NAME" & ", "
                Sql += "DESCRIPTION = " & "@_DESCRIPTION" & ", "
                Sql += "SECTOR_ID = " & "@_SECTOR_ID" & ", "
                Sql += "SUB_SECTOR_ID = " & "@_SUB_SECTOR_ID" & ", "
                Sql += "PLAN_START = " & "@_PLAN_START" & ", "
                Sql += "PLAN_END = " & "@_PLAN_END" & ", "
                Sql += "ACTUAL_START = " & "@_ACTUAL_START" & ", "
                Sql += "ACTUAL_END = " & "@_ACTUAL_END" & ", "
                Sql += "COMMITMENT_BUDGET = " & "@_COMMITMENT_BUDGET" & ", "
                Sql += "DISBURSEMENT = " & "@_DISBURSEMENT" & ", "
                Sql += "ADMINISTRATIVE = " & "@_ADMINISTRATIVE" & ", "
                Sql += "ACTIVITY_STATUS = " & "@_ACTIVITY_STATUS" & ", "
                Sql += "ASSISTANCE_TYPE = " & "@_ASSISTANCE_TYPE" & ", "
                Sql += "BENEFICIARY = " & "@_BENEFICIARY" & ", "
                Sql += "NOTIFY = " & "@_NOTIFY" & ", "
                Sql += "PROJECT_ID = " & "@_PROJECT_ID" & ", "
                Sql += "PARENT_ID = " & "@_PARENT_ID" & ", "
                Sql += "COMMITMENT_BUDGET_LOAN = " & "@_COMMITMENT_BUDGET_LOAN" & ", "
                Sql += "DISBURSEMENT_LOAN = " & "@_DISBURSEMENT_LOAN" & ", "
                Sql += "PAYMENT_DATE_GANT = " & "@_PAYMENT_DATE_GANT" & ", "
                Sql += "PAYMENT_DATE_LOAN = " & "@_PAYMENT_DATE_LOAN" & ", "
                Sql += "PAY_AS = " & "@_PAY_AS" & ", "
                Sql += "PAYMENT_DATE_CONTRIBUTION = " & "@_PAYMENT_DATE_CONTRIBUTION" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" & ", "
                Sql += "ACTIVE_STATUS = " & "@_ACTIVE_STATUS" & ", "
                Sql += "DISBURSEMENT_TYPE = " & "@_DISBURSEMENT_TYPE" & ", "
                Sql += "RECIPIENT_TYPE = " & "@_RECIPIENT_TYPE" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table TB_Activity
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table TB_Activity
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT ID, ACTIVITY_NAME, DESCRIPTION, SECTOR_ID, SUB_SECTOR_ID, PLAN_START, PLAN_END, ACTUAL_START, ACTUAL_END, COMMITMENT_BUDGET, DISBURSEMENT, ADMINISTRATIVE, ACTIVITY_STATUS, ASSISTANCE_TYPE, BENEFICIARY, NOTIFY, PROJECT_ID, PARENT_ID, COMMITMENT_BUDGET_LOAN, DISBURSEMENT_LOAN, PAYMENT_DATE_GANT, PAYMENT_DATE_LOAN, PAY_AS, PAYMENT_DATE_CONTRIBUTION, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE, ACTIVE_STATUS, DISBURSEMENT_TYPE, RECIPIENT_TYPE FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
