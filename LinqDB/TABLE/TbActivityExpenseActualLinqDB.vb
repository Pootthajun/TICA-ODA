Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for TB_Activity_Expense_Actual table LinqDB.
    '[Create by  on June, 28 2017]
    Public Class TbActivityExpenseActualLinqDB
        Public sub TbActivityExpenseActualLinqDB()

        End Sub 
        ' TB_Activity_Expense_Actual
        Const _tableName As String = "TB_Activity_Expense_Actual"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _ID As Long = 0
        Dim _HEADER_ID As  System.Nullable(Of Long) 
        Dim _PAYMENT_DATE_ACTUAL As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _PAY_ACTUAL_DETAIL As  String  = ""
        Dim _PAYMENT_ACTUAL_DETAIL As  String  = ""
        Dim _RECIPIENCE_ID As  System.Nullable(Of Long) 

        'Generate Field Property 
        <Column(Storage:="_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property ID() As Long
            Get
                Return _ID
            End Get
            Set(ByVal value As Long)
               _ID = value
            End Set
        End Property 
        <Column(Storage:="_HEADER_ID", DbType:="BigInt")>  _
        Public Property HEADER_ID() As  System.Nullable(Of Long) 
            Get
                Return _HEADER_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _HEADER_ID = value
            End Set
        End Property 
        <Column(Storage:="_PAYMENT_DATE_ACTUAL", DbType:="DateTime")>  _
        Public Property PAYMENT_DATE_ACTUAL() As  System.Nullable(Of DateTime) 
            Get
                Return _PAYMENT_DATE_ACTUAL
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _PAYMENT_DATE_ACTUAL = value
            End Set
        End Property 
        <Column(Storage:="_PAY_ACTUAL_DETAIL", DbType:="VarChar(100)")>  _
        Public Property PAY_ACTUAL_DETAIL() As  String 
            Get
                Return _PAY_ACTUAL_DETAIL
            End Get
            Set(ByVal value As  String )
               _PAY_ACTUAL_DETAIL = value
            End Set
        End Property 
        <Column(Storage:="_PAYMENT_ACTUAL_DETAIL", DbType:="VarChar(100)")>  _
        Public Property PAYMENT_ACTUAL_DETAIL() As  String 
            Get
                Return _PAYMENT_ACTUAL_DETAIL
            End Get
            Set(ByVal value As  String )
               _PAYMENT_ACTUAL_DETAIL = value
            End Set
        End Property 
        <Column(Storage:="_RECIPIENCE_ID", DbType:="BigInt")>  _
        Public Property RECIPIENCE_ID() As  System.Nullable(Of Long) 
            Get
                Return _RECIPIENCE_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _RECIPIENCE_ID = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _ID = 0
            _HEADER_ID = Nothing
            _PAYMENT_DATE_ACTUAL = New DateTime(1,1,1)
            _PAY_ACTUAL_DETAIL = ""
            _PAYMENT_ACTUAL_DETAIL = ""
            _RECIPIENCE_ID = Nothing
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into TB_Activity_Expense_Actual table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _ID = DB.GetNextID("ID",tableName, trans)
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_Activity_Expense_Actual table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _id > 0 Then 

                    Return doUpdate("ID = @_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_Activity_Expense_Actual table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from TB_Activity_Expense_Actual table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_ID", cID)
                Return doDelete("ID = @_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of TB_Activity_Expense_Actual by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doChkData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of TB_Activity_Expense_Actual by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cID As Long, trans As SQLTransaction) As TbActivityExpenseActualLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doGetData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of TB_Activity_Expense_Actual by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into TB_Activity_Expense_Actual table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to TB_Activity_Expense_Actual table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from TB_Activity_Expense_Actual table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(5) As SqlParameter
            cmbParam(0) = New SqlParameter("@_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _ID

            cmbParam(1) = New SqlParameter("@_HEADER_ID", SqlDbType.BigInt)
            If _HEADER_ID IsNot Nothing Then 
                cmbParam(1).Value = _HEADER_ID.Value
            Else
                cmbParam(1).Value = DBNull.value
            End IF

            cmbParam(2) = New SqlParameter("@_PAYMENT_DATE_ACTUAL", SqlDbType.DateTime)
            If _PAYMENT_DATE_ACTUAL.Value.Year > 1 Then 
                cmbParam(2).Value = _PAYMENT_DATE_ACTUAL.Value
            Else
                cmbParam(2).Value = DBNull.value
            End If

            cmbParam(3) = New SqlParameter("@_PAY_ACTUAL_DETAIL", SqlDbType.VarChar)
            If _PAY_ACTUAL_DETAIL.Trim <> "" Then 
                cmbParam(3).Value = _PAY_ACTUAL_DETAIL.Trim
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_PAYMENT_ACTUAL_DETAIL", SqlDbType.VarChar)
            If _PAYMENT_ACTUAL_DETAIL.Trim <> "" Then 
                cmbParam(4).Value = _PAYMENT_ACTUAL_DETAIL.Trim
            Else
                cmbParam(4).Value = DBNull.value
            End If

            cmbParam(5) = New SqlParameter("@_RECIPIENCE_ID", SqlDbType.BigInt)
            If _RECIPIENCE_ID IsNot Nothing Then 
                cmbParam(5).Value = _RECIPIENCE_ID.Value
            Else
                cmbParam(5).Value = DBNull.value
            End IF

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of TB_Activity_Expense_Actual by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("Header_id")) = False Then _Header_id = Convert.ToInt64(Rdr("Header_id"))
                        If Convert.IsDBNull(Rdr("Payment_Date_Actual")) = False Then _Payment_Date_Actual = Convert.ToDateTime(Rdr("Payment_Date_Actual"))
                        If Convert.IsDBNull(Rdr("Pay_Actual_Detail")) = False Then _Pay_Actual_Detail = Rdr("Pay_Actual_Detail").ToString()
                        If Convert.IsDBNull(Rdr("Payment_Actual_Detail")) = False Then _Payment_Actual_Detail = Rdr("Payment_Actual_Detail").ToString()
                        If Convert.IsDBNull(Rdr("Recipience_id")) = False Then _Recipience_id = Convert.ToInt64(Rdr("Recipience_id"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of TB_Activity_Expense_Actual by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As TbActivityExpenseActualLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("Header_id")) = False Then _Header_id = Convert.ToInt64(Rdr("Header_id"))
                        If Convert.IsDBNull(Rdr("Payment_Date_Actual")) = False Then _Payment_Date_Actual = Convert.ToDateTime(Rdr("Payment_Date_Actual"))
                        If Convert.IsDBNull(Rdr("Pay_Actual_Detail")) = False Then _Pay_Actual_Detail = Rdr("Pay_Actual_Detail").ToString()
                        If Convert.IsDBNull(Rdr("Payment_Actual_Detail")) = False Then _Payment_Actual_Detail = Rdr("Payment_Actual_Detail").ToString()
                        If Convert.IsDBNull(Rdr("Recipience_id")) = False Then _Recipience_id = Convert.ToInt64(Rdr("Recipience_id"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table TB_Activity_Expense_Actual
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (ID, HEADER_ID, PAYMENT_DATE_ACTUAL, PAY_ACTUAL_DETAIL, PAYMENT_ACTUAL_DETAIL, RECIPIENCE_ID)"
                Sql += " OUTPUT INSERTED.ID, INSERTED.HEADER_ID, INSERTED.PAYMENT_DATE_ACTUAL, INSERTED.PAY_ACTUAL_DETAIL, INSERTED.PAYMENT_ACTUAL_DETAIL, INSERTED.RECIPIENCE_ID"
                Sql += " VALUES("
                sql += "@_ID" & ", "
                sql += "@_HEADER_ID" & ", "
                sql += "@_PAYMENT_DATE_ACTUAL" & ", "
                sql += "@_PAY_ACTUAL_DETAIL" & ", "
                sql += "@_PAYMENT_ACTUAL_DETAIL" & ", "
                sql += "@_RECIPIENCE_ID"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table TB_Activity_Expense_Actual
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "HEADER_ID = " & "@_HEADER_ID" & ", "
                Sql += "PAYMENT_DATE_ACTUAL = " & "@_PAYMENT_DATE_ACTUAL" & ", "
                Sql += "PAY_ACTUAL_DETAIL = " & "@_PAY_ACTUAL_DETAIL" & ", "
                Sql += "PAYMENT_ACTUAL_DETAIL = " & "@_PAYMENT_ACTUAL_DETAIL" & ", "
                Sql += "RECIPIENCE_ID = " & "@_RECIPIENCE_ID" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table TB_Activity_Expense_Actual
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table TB_Activity_Expense_Actual
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT ID, HEADER_ID, PAYMENT_DATE_ACTUAL, PAY_ACTUAL_DETAIL, PAYMENT_ACTUAL_DETAIL, RECIPIENCE_ID FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
