Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for TB_Project table LinqDB.
    '[Create by  on November, 30 2016]
    Public Class TbProjectLinqDB
        Public sub TbProjectLinqDB()

        End Sub 
        ' TB_Project
        Const _tableName As String = "TB_Project"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _ID As Long = 0
        Dim _PROJECT_ID As  String  = ""
        Dim _PROJECT_NAME As  String  = ""
        Dim _PROJECT_TYPE As  String  = ""
        Dim _OBJECTIVE As  String  = ""
        Dim _DESCRIPTION As  String  = ""
        Dim _START_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _END_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _COOPERATION_FRAMEWORK_ID As  System.Nullable(Of Long) 
        Dim _COOPERATION_TYPE_ID As  System.Nullable(Of Long) 
        Dim _REGION_OECD_ID As  System.Nullable(Of Long) 
        Dim _OECD_ID As  System.Nullable(Of Long) 
        Dim _LOCATION As  String  = ""
        Dim _FUNDING_AGENCY_ID As  System.Nullable(Of Long) 
        Dim _EXECUTING_AGENCY_ID As  System.Nullable(Of Long) 
        Dim _IMPLEMENTING_AGENCY_ID As  System.Nullable(Of Long) 
        Dim _ASSISTANT As  String  = ""
        Dim _REMARK As  String  = ""
        Dim _TRANSFER_PROJECT_TO As  String  = ""
        Dim _MULTILATERAL_ID As  System.Nullable(Of Long) 
        Dim _ALLOCATE_BUDGET As  System.Nullable(Of Double) 
        Dim _OECD_AID_TYPE_ID As  System.Nullable(Of Long) 
        Dim _GRANCE_PERIOD As  System.Nullable(Of Long) 
        Dim _MATURITY_PERIOD As  System.Nullable(Of Long) 
        Dim _INTEREST_RATE As  System.Nullable(Of Double) 
        Dim _IMPLEMENTING_AGENCY_LOAN As  String  = ""
        Dim _IMPLEMENTING_AGENCY_CONTRIBUTION As  String  = ""
        Dim _CREATED_BY As  String  = ""
        Dim _CREATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _ACTIVE_STATUS As Char = "Y"

        'Generate Field Property 
        <Column(Storage:="_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property ID() As Long
            Get
                Return _ID
            End Get
            Set(ByVal value As Long)
               _ID = value
            End Set
        End Property 
        <Column(Storage:="_PROJECT_ID", DbType:="VarChar(20)")>  _
        Public Property PROJECT_ID() As  String 
            Get
                Return _PROJECT_ID
            End Get
            Set(ByVal value As  String )
               _PROJECT_ID = value
            End Set
        End Property 
        <Column(Storage:="_PROJECT_NAME", DbType:="VarChar(500)")>  _
        Public Property PROJECT_NAME() As  String 
            Get
                Return _PROJECT_NAME
            End Get
            Set(ByVal value As  String )
               _PROJECT_NAME = value
            End Set
        End Property 
        <Column(Storage:="_PROJECT_TYPE", DbType:="VarChar(1)")>  _
        Public Property PROJECT_TYPE() As  String 
            Get
                Return _PROJECT_TYPE
            End Get
            Set(ByVal value As  String )
               _PROJECT_TYPE = value
            End Set
        End Property 
        <Column(Storage:="_OBJECTIVE", DbType:="Text")>  _
        Public Property OBJECTIVE() As  String 
            Get
                Return _OBJECTIVE
            End Get
            Set(ByVal value As  String )
               _OBJECTIVE = value
            End Set
        End Property 
        <Column(Storage:="_DESCRIPTION", DbType:="Text")>  _
        Public Property DESCRIPTION() As  String 
            Get
                Return _DESCRIPTION
            End Get
            Set(ByVal value As  String )
               _DESCRIPTION = value
            End Set
        End Property 
        <Column(Storage:="_START_DATE", DbType:="DateTime")>  _
        Public Property START_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _START_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _START_DATE = value
            End Set
        End Property 
        <Column(Storage:="_END_DATE", DbType:="DateTime")>  _
        Public Property END_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _END_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _END_DATE = value
            End Set
        End Property 
        <Column(Storage:="_COOPERATION_FRAMEWORK_ID", DbType:="BigInt")>  _
        Public Property COOPERATION_FRAMEWORK_ID() As  System.Nullable(Of Long) 
            Get
                Return _COOPERATION_FRAMEWORK_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _COOPERATION_FRAMEWORK_ID = value
            End Set
        End Property 
        <Column(Storage:="_COOPERATION_TYPE_ID", DbType:="BigInt")>  _
        Public Property COOPERATION_TYPE_ID() As  System.Nullable(Of Long) 
            Get
                Return _COOPERATION_TYPE_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _COOPERATION_TYPE_ID = value
            End Set
        End Property 
        <Column(Storage:="_REGION_OECD_ID", DbType:="BigInt")>  _
        Public Property REGION_OECD_ID() As  System.Nullable(Of Long) 
            Get
                Return _REGION_OECD_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _REGION_OECD_ID = value
            End Set
        End Property 
        <Column(Storage:="_OECD_ID", DbType:="BigInt")>  _
        Public Property OECD_ID() As  System.Nullable(Of Long) 
            Get
                Return _OECD_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _OECD_ID = value
            End Set
        End Property 
        <Column(Storage:="_LOCATION", DbType:="VarChar(250)")>  _
        Public Property LOCATION() As  String 
            Get
                Return _LOCATION
            End Get
            Set(ByVal value As  String )
               _LOCATION = value
            End Set
        End Property 
        <Column(Storage:="_FUNDING_AGENCY_ID", DbType:="BigInt")>  _
        Public Property FUNDING_AGENCY_ID() As  System.Nullable(Of Long) 
            Get
                Return _FUNDING_AGENCY_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _FUNDING_AGENCY_ID = value
            End Set
        End Property 
        <Column(Storage:="_EXECUTING_AGENCY_ID", DbType:="BigInt")>  _
        Public Property EXECUTING_AGENCY_ID() As  System.Nullable(Of Long) 
            Get
                Return _EXECUTING_AGENCY_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _EXECUTING_AGENCY_ID = value
            End Set
        End Property 
        <Column(Storage:="_IMPLEMENTING_AGENCY_ID", DbType:="BigInt")>  _
        Public Property IMPLEMENTING_AGENCY_ID() As  System.Nullable(Of Long) 
            Get
                Return _IMPLEMENTING_AGENCY_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _IMPLEMENTING_AGENCY_ID = value
            End Set
        End Property 
        <Column(Storage:="_ASSISTANT", DbType:="VarChar(250)")>  _
        Public Property ASSISTANT() As  String 
            Get
                Return _ASSISTANT
            End Get
            Set(ByVal value As  String )
               _ASSISTANT = value
            End Set
        End Property 
        <Column(Storage:="_REMARK", DbType:="VarChar(500)")>  _
        Public Property REMARK() As  String 
            Get
                Return _REMARK
            End Get
            Set(ByVal value As  String )
               _REMARK = value
            End Set
        End Property 
        <Column(Storage:="_TRANSFER_PROJECT_TO", DbType:="VarChar(50)")>  _
        Public Property TRANSFER_PROJECT_TO() As  String 
            Get
                Return _TRANSFER_PROJECT_TO
            End Get
            Set(ByVal value As  String )
               _TRANSFER_PROJECT_TO = value
            End Set
        End Property 
        <Column(Storage:="_MULTILATERAL_ID", DbType:="BigInt")>  _
        Public Property MULTILATERAL_ID() As  System.Nullable(Of Long) 
            Get
                Return _MULTILATERAL_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _MULTILATERAL_ID = value
            End Set
        End Property 
        <Column(Storage:="_ALLOCATE_BUDGET", DbType:="Double")>  _
        Public Property ALLOCATE_BUDGET() As  System.Nullable(Of Double) 
            Get
                Return _ALLOCATE_BUDGET
            End Get
            Set(ByVal value As  System.Nullable(Of Double) )
               _ALLOCATE_BUDGET = value
            End Set
        End Property 
        <Column(Storage:="_OECD_AID_TYPE_ID", DbType:="BigInt")>  _
        Public Property OECD_AID_TYPE_ID() As  System.Nullable(Of Long) 
            Get
                Return _OECD_AID_TYPE_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _OECD_AID_TYPE_ID = value
            End Set
        End Property 
        <Column(Storage:="_GRANCE_PERIOD", DbType:="Int")>  _
        Public Property GRANCE_PERIOD() As  System.Nullable(Of Long) 
            Get
                Return _GRANCE_PERIOD
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _GRANCE_PERIOD = value
            End Set
        End Property 
        <Column(Storage:="_MATURITY_PERIOD", DbType:="Int")>  _
        Public Property MATURITY_PERIOD() As  System.Nullable(Of Long) 
            Get
                Return _MATURITY_PERIOD
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _MATURITY_PERIOD = value
            End Set
        End Property 
        <Column(Storage:="_INTEREST_RATE", DbType:="Double")>  _
        Public Property INTEREST_RATE() As  System.Nullable(Of Double) 
            Get
                Return _INTEREST_RATE
            End Get
            Set(ByVal value As  System.Nullable(Of Double) )
               _INTEREST_RATE = value
            End Set
        End Property 
        <Column(Storage:="_IMPLEMENTING_AGENCY_LOAN", DbType:="VarChar(500)")>  _
        Public Property IMPLEMENTING_AGENCY_LOAN() As  String 
            Get
                Return _IMPLEMENTING_AGENCY_LOAN
            End Get
            Set(ByVal value As  String )
               _IMPLEMENTING_AGENCY_LOAN = value
            End Set
        End Property 
        <Column(Storage:="_IMPLEMENTING_AGENCY_CONTRIBUTION", DbType:="VarChar(50)")>  _
        Public Property IMPLEMENTING_AGENCY_CONTRIBUTION() As  String 
            Get
                Return _IMPLEMENTING_AGENCY_CONTRIBUTION
            End Get
            Set(ByVal value As  String )
               _IMPLEMENTING_AGENCY_CONTRIBUTION = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100)")>  _
        Public Property CREATED_BY() As  String 
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As  String )
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime")>  _
        Public Property CREATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_ACTIVE_STATUS", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ACTIVE_STATUS() As Char
            Get
                Return _ACTIVE_STATUS
            End Get
            Set(ByVal value As Char)
               _ACTIVE_STATUS = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _ID = 0
            _PROJECT_ID = ""
            _PROJECT_NAME = ""
            _PROJECT_TYPE = ""
            _OBJECTIVE = ""
            _DESCRIPTION = ""
            _START_DATE = New DateTime(1,1,1)
            _END_DATE = New DateTime(1,1,1)
            _COOPERATION_FRAMEWORK_ID = Nothing
            _COOPERATION_TYPE_ID = Nothing
            _REGION_OECD_ID = Nothing
            _OECD_ID = Nothing
            _LOCATION = ""
            _FUNDING_AGENCY_ID = Nothing
            _EXECUTING_AGENCY_ID = Nothing
            _IMPLEMENTING_AGENCY_ID = Nothing
            _ASSISTANT = ""
            _REMARK = ""
            _TRANSFER_PROJECT_TO = ""
            _MULTILATERAL_ID = Nothing
            _ALLOCATE_BUDGET = Nothing
            _OECD_AID_TYPE_ID = Nothing
            _GRANCE_PERIOD = Nothing
            _MATURITY_PERIOD = Nothing
            _INTEREST_RATE = Nothing
            _IMPLEMENTING_AGENCY_LOAN = ""
            _IMPLEMENTING_AGENCY_CONTRIBUTION = ""
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
            _ACTIVE_STATUS = "Y"
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into TB_Project table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_Project table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _id > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("ID = @_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_Project table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from TB_Project table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_ID", cID)
                Return doDelete("ID = @_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of TB_Project by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doChkData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of TB_Project by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cID As Long, trans As SQLTransaction) As TbProjectLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doGetData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of TB_Project by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into TB_Project table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _ID = dt.Rows(0)("ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to TB_Project table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from TB_Project table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(31) As SqlParameter
            cmbParam(0) = New SqlParameter("@_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _ID

            cmbParam(1) = New SqlParameter("@_PROJECT_ID", SqlDbType.VarChar)
            If _PROJECT_ID.Trim <> "" Then 
                cmbParam(1).Value = _PROJECT_ID.Trim
            Else
                cmbParam(1).Value = DBNull.value
            End If

            cmbParam(2) = New SqlParameter("@_PROJECT_NAME", SqlDbType.VarChar)
            If _PROJECT_NAME.Trim <> "" Then 
                cmbParam(2).Value = _PROJECT_NAME.Trim
            Else
                cmbParam(2).Value = DBNull.value
            End If

            cmbParam(3) = New SqlParameter("@_PROJECT_TYPE", SqlDbType.VarChar)
            If _PROJECT_TYPE.Trim <> "" Then 
                cmbParam(3).Value = _PROJECT_TYPE.Trim
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_OBJECTIVE", SqlDbType.Text)
            If _OBJECTIVE IsNot Nothing Then 
                cmbParam(4).Value = _OBJECTIVE.Trim
            Else
                cmbParam(4).Value = DBNull.value
            End IF

            cmbParam(5) = New SqlParameter("@_DESCRIPTION", SqlDbType.Text)
            If _DESCRIPTION IsNot Nothing Then 
                cmbParam(5).Value = _DESCRIPTION.Trim
            Else
                cmbParam(5).Value = DBNull.value
            End IF

            cmbParam(6) = New SqlParameter("@_START_DATE", SqlDbType.DateTime)
            If _START_DATE.Value.Year > 1 Then 
                cmbParam(6).Value = _START_DATE.Value
            Else
                cmbParam(6).Value = DBNull.value
            End If

            cmbParam(7) = New SqlParameter("@_END_DATE", SqlDbType.DateTime)
            If _END_DATE.Value.Year > 1 Then 
                cmbParam(7).Value = _END_DATE.Value
            Else
                cmbParam(7).Value = DBNull.value
            End If

            cmbParam(8) = New SqlParameter("@_COOPERATION_FRAMEWORK_ID", SqlDbType.BigInt)
            If _COOPERATION_FRAMEWORK_ID IsNot Nothing Then 
                cmbParam(8).Value = _COOPERATION_FRAMEWORK_ID.Value
            Else
                cmbParam(8).Value = DBNull.value
            End IF

            cmbParam(9) = New SqlParameter("@_COOPERATION_TYPE_ID", SqlDbType.BigInt)
            If _COOPERATION_TYPE_ID IsNot Nothing Then 
                cmbParam(9).Value = _COOPERATION_TYPE_ID.Value
            Else
                cmbParam(9).Value = DBNull.value
            End IF

            cmbParam(10) = New SqlParameter("@_REGION_OECD_ID", SqlDbType.BigInt)
            If _REGION_OECD_ID IsNot Nothing Then 
                cmbParam(10).Value = _REGION_OECD_ID.Value
            Else
                cmbParam(10).Value = DBNull.value
            End IF

            cmbParam(11) = New SqlParameter("@_OECD_ID", SqlDbType.BigInt)
            If _OECD_ID IsNot Nothing Then 
                cmbParam(11).Value = _OECD_ID.Value
            Else
                cmbParam(11).Value = DBNull.value
            End IF

            cmbParam(12) = New SqlParameter("@_LOCATION", SqlDbType.VarChar)
            If _LOCATION.Trim <> "" Then 
                cmbParam(12).Value = _LOCATION.Trim
            Else
                cmbParam(12).Value = DBNull.value
            End If

            cmbParam(13) = New SqlParameter("@_FUNDING_AGENCY_ID", SqlDbType.BigInt)
            If _FUNDING_AGENCY_ID IsNot Nothing Then 
                cmbParam(13).Value = _FUNDING_AGENCY_ID.Value
            Else
                cmbParam(13).Value = DBNull.value
            End IF

            cmbParam(14) = New SqlParameter("@_EXECUTING_AGENCY_ID", SqlDbType.BigInt)
            If _EXECUTING_AGENCY_ID IsNot Nothing Then 
                cmbParam(14).Value = _EXECUTING_AGENCY_ID.Value
            Else
                cmbParam(14).Value = DBNull.value
            End IF

            cmbParam(15) = New SqlParameter("@_IMPLEMENTING_AGENCY_ID", SqlDbType.BigInt)
            If _IMPLEMENTING_AGENCY_ID IsNot Nothing Then 
                cmbParam(15).Value = _IMPLEMENTING_AGENCY_ID.Value
            Else
                cmbParam(15).Value = DBNull.value
            End IF

            cmbParam(16) = New SqlParameter("@_ASSISTANT", SqlDbType.VarChar)
            If _ASSISTANT.Trim <> "" Then 
                cmbParam(16).Value = _ASSISTANT.Trim
            Else
                cmbParam(16).Value = DBNull.value
            End If

            cmbParam(17) = New SqlParameter("@_REMARK", SqlDbType.VarChar)
            If _REMARK.Trim <> "" Then 
                cmbParam(17).Value = _REMARK.Trim
            Else
                cmbParam(17).Value = DBNull.value
            End If

            cmbParam(18) = New SqlParameter("@_TRANSFER_PROJECT_TO", SqlDbType.VarChar)
            If _TRANSFER_PROJECT_TO.Trim <> "" Then 
                cmbParam(18).Value = _TRANSFER_PROJECT_TO.Trim
            Else
                cmbParam(18).Value = DBNull.value
            End If

            cmbParam(19) = New SqlParameter("@_MULTILATERAL_ID", SqlDbType.BigInt)
            If _MULTILATERAL_ID IsNot Nothing Then 
                cmbParam(19).Value = _MULTILATERAL_ID.Value
            Else
                cmbParam(19).Value = DBNull.value
            End IF

            cmbParam(20) = New SqlParameter("@_ALLOCATE_BUDGET", SqlDbType.Decimal)
            If _ALLOCATE_BUDGET IsNot Nothing Then 
                cmbParam(20).Value = _ALLOCATE_BUDGET.Value
            Else
                cmbParam(20).Value = DBNull.value
            End IF

            cmbParam(21) = New SqlParameter("@_OECD_AID_TYPE_ID", SqlDbType.BigInt)
            If _OECD_AID_TYPE_ID IsNot Nothing Then 
                cmbParam(21).Value = _OECD_AID_TYPE_ID.Value
            Else
                cmbParam(21).Value = DBNull.value
            End IF

            cmbParam(22) = New SqlParameter("@_GRANCE_PERIOD", SqlDbType.Int)
            If _GRANCE_PERIOD IsNot Nothing Then 
                cmbParam(22).Value = _GRANCE_PERIOD.Value
            Else
                cmbParam(22).Value = DBNull.value
            End IF

            cmbParam(23) = New SqlParameter("@_MATURITY_PERIOD", SqlDbType.Int)
            If _MATURITY_PERIOD IsNot Nothing Then 
                cmbParam(23).Value = _MATURITY_PERIOD.Value
            Else
                cmbParam(23).Value = DBNull.value
            End IF

            cmbParam(24) = New SqlParameter("@_INTEREST_RATE", SqlDbType.Decimal)
            If _INTEREST_RATE IsNot Nothing Then 
                cmbParam(24).Value = _INTEREST_RATE.Value
            Else
                cmbParam(24).Value = DBNull.value
            End IF

            cmbParam(25) = New SqlParameter("@_IMPLEMENTING_AGENCY_LOAN", SqlDbType.VarChar)
            If _IMPLEMENTING_AGENCY_LOAN.Trim <> "" Then 
                cmbParam(25).Value = _IMPLEMENTING_AGENCY_LOAN.Trim
            Else
                cmbParam(25).Value = DBNull.value
            End If

            cmbParam(26) = New SqlParameter("@_IMPLEMENTING_AGENCY_CONTRIBUTION", SqlDbType.VarChar)
            If _IMPLEMENTING_AGENCY_CONTRIBUTION.Trim <> "" Then 
                cmbParam(26).Value = _IMPLEMENTING_AGENCY_CONTRIBUTION.Trim
            Else
                cmbParam(26).Value = DBNull.value
            End If

            cmbParam(27) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            If _CREATED_BY.Trim <> "" Then 
                cmbParam(27).Value = _CREATED_BY.Trim
            Else
                cmbParam(27).Value = DBNull.value
            End If

            cmbParam(28) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            If _CREATED_DATE.Value.Year > 1 Then 
                cmbParam(28).Value = _CREATED_DATE.Value
            Else
                cmbParam(28).Value = DBNull.value
            End If

            cmbParam(29) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(29).Value = _UPDATED_BY.Trim
            Else
                cmbParam(29).Value = DBNull.value
            End If

            cmbParam(30) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(30).Value = _UPDATED_DATE.Value
            Else
                cmbParam(30).Value = DBNull.value
            End If

            cmbParam(31) = New SqlParameter("@_ACTIVE_STATUS", SqlDbType.Char)
            cmbParam(31).Value = _ACTIVE_STATUS

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of TB_Project by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("project_id")) = False Then _project_id = Rdr("project_id").ToString()
                        If Convert.IsDBNull(Rdr("project_name")) = False Then _project_name = Rdr("project_name").ToString()
                        If Convert.IsDBNull(Rdr("project_type")) = False Then _project_type = Rdr("project_type").ToString()
                        If Convert.IsDBNull(Rdr("objective")) = False Then _objective = Rdr("objective").ToString()
                        If Convert.IsDBNull(Rdr("description")) = False Then _description = Rdr("description").ToString()
                        If Convert.IsDBNull(Rdr("start_date")) = False Then _start_date = Convert.ToDateTime(Rdr("start_date"))
                        If Convert.IsDBNull(Rdr("end_date")) = False Then _end_date = Convert.ToDateTime(Rdr("end_date"))
                        If Convert.IsDBNull(Rdr("cooperation_framework_id")) = False Then _cooperation_framework_id = Convert.ToInt64(Rdr("cooperation_framework_id"))
                        If Convert.IsDBNull(Rdr("cooperation_type_id")) = False Then _cooperation_type_id = Convert.ToInt64(Rdr("cooperation_type_id"))
                        If Convert.IsDBNull(Rdr("region_oecd_id")) = False Then _region_oecd_id = Convert.ToInt64(Rdr("region_oecd_id"))
                        If Convert.IsDBNull(Rdr("oecd_id")) = False Then _oecd_id = Convert.ToInt64(Rdr("oecd_id"))
                        If Convert.IsDBNull(Rdr("location")) = False Then _location = Rdr("location").ToString()
                        If Convert.IsDBNull(Rdr("funding_agency_id")) = False Then _funding_agency_id = Convert.ToInt64(Rdr("funding_agency_id"))
                        If Convert.IsDBNull(Rdr("executing_agency_id")) = False Then _executing_agency_id = Convert.ToInt64(Rdr("executing_agency_id"))
                        If Convert.IsDBNull(Rdr("implementing_agency_id")) = False Then _implementing_agency_id = Convert.ToInt64(Rdr("implementing_agency_id"))
                        If Convert.IsDBNull(Rdr("assistant")) = False Then _assistant = Rdr("assistant").ToString()
                        If Convert.IsDBNull(Rdr("remark")) = False Then _remark = Rdr("remark").ToString()
                        If Convert.IsDBNull(Rdr("transfer_project_to")) = False Then _transfer_project_to = Rdr("transfer_project_to").ToString()
                        If Convert.IsDBNull(Rdr("multilateral_id")) = False Then _multilateral_id = Convert.ToInt64(Rdr("multilateral_id"))
                        If Convert.IsDBNull(Rdr("allocate_budget")) = False Then _allocate_budget = Convert.ToDouble(Rdr("allocate_budget"))
                        If Convert.IsDBNull(Rdr("oecd_aid_type_id")) = False Then _oecd_aid_type_id = Convert.ToInt64(Rdr("oecd_aid_type_id"))
                        If Convert.IsDBNull(Rdr("grance_period")) = False Then _grance_period = Convert.ToInt32(Rdr("grance_period"))
                        If Convert.IsDBNull(Rdr("maturity_period")) = False Then _maturity_period = Convert.ToInt32(Rdr("maturity_period"))
                        If Convert.IsDBNull(Rdr("interest_rate")) = False Then _interest_rate = Convert.ToDouble(Rdr("interest_rate"))
                        If Convert.IsDBNull(Rdr("implementing_agency_loan")) = False Then _implementing_agency_loan = Rdr("implementing_agency_loan").ToString()
                        If Convert.IsDBNull(Rdr("implementing_agency_contribution")) = False Then _implementing_agency_contribution = Rdr("implementing_agency_contribution").ToString()
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                        If Convert.IsDBNull(Rdr("active_status")) = False Then _active_status = Rdr("active_status").ToString()
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of TB_Project by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As TbProjectLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("project_id")) = False Then _project_id = Rdr("project_id").ToString()
                        If Convert.IsDBNull(Rdr("project_name")) = False Then _project_name = Rdr("project_name").ToString()
                        If Convert.IsDBNull(Rdr("project_type")) = False Then _project_type = Rdr("project_type").ToString()
                        If Convert.IsDBNull(Rdr("objective")) = False Then _objective = Rdr("objective").ToString()
                        If Convert.IsDBNull(Rdr("description")) = False Then _description = Rdr("description").ToString()
                        If Convert.IsDBNull(Rdr("start_date")) = False Then _start_date = Convert.ToDateTime(Rdr("start_date"))
                        If Convert.IsDBNull(Rdr("end_date")) = False Then _end_date = Convert.ToDateTime(Rdr("end_date"))
                        If Convert.IsDBNull(Rdr("cooperation_framework_id")) = False Then _cooperation_framework_id = Convert.ToInt64(Rdr("cooperation_framework_id"))
                        If Convert.IsDBNull(Rdr("cooperation_type_id")) = False Then _cooperation_type_id = Convert.ToInt64(Rdr("cooperation_type_id"))
                        If Convert.IsDBNull(Rdr("region_oecd_id")) = False Then _region_oecd_id = Convert.ToInt64(Rdr("region_oecd_id"))
                        If Convert.IsDBNull(Rdr("oecd_id")) = False Then _oecd_id = Convert.ToInt64(Rdr("oecd_id"))
                        If Convert.IsDBNull(Rdr("location")) = False Then _location = Rdr("location").ToString()
                        If Convert.IsDBNull(Rdr("funding_agency_id")) = False Then _funding_agency_id = Convert.ToInt64(Rdr("funding_agency_id"))
                        If Convert.IsDBNull(Rdr("executing_agency_id")) = False Then _executing_agency_id = Convert.ToInt64(Rdr("executing_agency_id"))
                        If Convert.IsDBNull(Rdr("implementing_agency_id")) = False Then _implementing_agency_id = Convert.ToInt64(Rdr("implementing_agency_id"))
                        If Convert.IsDBNull(Rdr("assistant")) = False Then _assistant = Rdr("assistant").ToString()
                        If Convert.IsDBNull(Rdr("remark")) = False Then _remark = Rdr("remark").ToString()
                        If Convert.IsDBNull(Rdr("transfer_project_to")) = False Then _transfer_project_to = Rdr("transfer_project_to").ToString()
                        If Convert.IsDBNull(Rdr("multilateral_id")) = False Then _multilateral_id = Convert.ToInt64(Rdr("multilateral_id"))
                        If Convert.IsDBNull(Rdr("allocate_budget")) = False Then _allocate_budget = Convert.ToDouble(Rdr("allocate_budget"))
                        If Convert.IsDBNull(Rdr("oecd_aid_type_id")) = False Then _oecd_aid_type_id = Convert.ToInt64(Rdr("oecd_aid_type_id"))
                        If Convert.IsDBNull(Rdr("grance_period")) = False Then _grance_period = Convert.ToInt32(Rdr("grance_period"))
                        If Convert.IsDBNull(Rdr("maturity_period")) = False Then _maturity_period = Convert.ToInt32(Rdr("maturity_period"))
                        If Convert.IsDBNull(Rdr("interest_rate")) = False Then _interest_rate = Convert.ToDouble(Rdr("interest_rate"))
                        If Convert.IsDBNull(Rdr("implementing_agency_loan")) = False Then _implementing_agency_loan = Rdr("implementing_agency_loan").ToString()
                        If Convert.IsDBNull(Rdr("implementing_agency_contribution")) = False Then _implementing_agency_contribution = Rdr("implementing_agency_contribution").ToString()
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                        If Convert.IsDBNull(Rdr("active_status")) = False Then _active_status = Rdr("active_status").ToString()
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table TB_Project
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (PROJECT_ID, PROJECT_NAME, PROJECT_TYPE, OBJECTIVE, DESCRIPTION, START_DATE, END_DATE, COOPERATION_FRAMEWORK_ID, COOPERATION_TYPE_ID, REGION_OECD_ID, OECD_ID, LOCATION, FUNDING_AGENCY_ID, EXECUTING_AGENCY_ID, IMPLEMENTING_AGENCY_ID, ASSISTANT, REMARK, TRANSFER_PROJECT_TO, MULTILATERAL_ID, ALLOCATE_BUDGET, OECD_AID_TYPE_ID, GRANCE_PERIOD, MATURITY_PERIOD, INTEREST_RATE, IMPLEMENTING_AGENCY_LOAN, IMPLEMENTING_AGENCY_CONTRIBUTION, CREATED_BY, CREATED_DATE, ACTIVE_STATUS)"
                Sql += " OUTPUT INSERTED.ID, INSERTED.PROJECT_ID, INSERTED.PROJECT_NAME, INSERTED.PROJECT_TYPE, INSERTED.OBJECTIVE, INSERTED.DESCRIPTION, INSERTED.START_DATE, INSERTED.END_DATE, INSERTED.COOPERATION_FRAMEWORK_ID, INSERTED.COOPERATION_TYPE_ID, INSERTED.REGION_OECD_ID, INSERTED.OECD_ID, INSERTED.LOCATION, INSERTED.FUNDING_AGENCY_ID, INSERTED.EXECUTING_AGENCY_ID, INSERTED.IMPLEMENTING_AGENCY_ID, INSERTED.ASSISTANT, INSERTED.REMARK, INSERTED.TRANSFER_PROJECT_TO, INSERTED.MULTILATERAL_ID, INSERTED.ALLOCATE_BUDGET, INSERTED.OECD_AID_TYPE_ID, INSERTED.GRANCE_PERIOD, INSERTED.MATURITY_PERIOD, INSERTED.INTEREST_RATE, INSERTED.IMPLEMENTING_AGENCY_LOAN, INSERTED.IMPLEMENTING_AGENCY_CONTRIBUTION, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE, INSERTED.ACTIVE_STATUS"
                Sql += " VALUES("
                sql += "@_PROJECT_ID" & ", "
                sql += "@_PROJECT_NAME" & ", "
                sql += "@_PROJECT_TYPE" & ", "
                sql += "@_OBJECTIVE" & ", "
                sql += "@_DESCRIPTION" & ", "
                sql += "@_START_DATE" & ", "
                sql += "@_END_DATE" & ", "
                sql += "@_COOPERATION_FRAMEWORK_ID" & ", "
                sql += "@_COOPERATION_TYPE_ID" & ", "
                sql += "@_REGION_OECD_ID" & ", "
                sql += "@_OECD_ID" & ", "
                sql += "@_LOCATION" & ", "
                sql += "@_FUNDING_AGENCY_ID" & ", "
                sql += "@_EXECUTING_AGENCY_ID" & ", "
                sql += "@_IMPLEMENTING_AGENCY_ID" & ", "
                sql += "@_ASSISTANT" & ", "
                sql += "@_REMARK" & ", "
                sql += "@_TRANSFER_PROJECT_TO" & ", "
                sql += "@_MULTILATERAL_ID" & ", "
                sql += "@_ALLOCATE_BUDGET" & ", "
                sql += "@_OECD_AID_TYPE_ID" & ", "
                sql += "@_GRANCE_PERIOD" & ", "
                sql += "@_MATURITY_PERIOD" & ", "
                sql += "@_INTEREST_RATE" & ", "
                sql += "@_IMPLEMENTING_AGENCY_LOAN" & ", "
                sql += "@_IMPLEMENTING_AGENCY_CONTRIBUTION" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE" & ", "
                sql += "@_ACTIVE_STATUS"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table TB_Project
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "PROJECT_ID = " & "@_PROJECT_ID" & ", "
                Sql += "PROJECT_NAME = " & "@_PROJECT_NAME" & ", "
                Sql += "PROJECT_TYPE = " & "@_PROJECT_TYPE" & ", "
                Sql += "OBJECTIVE = " & "@_OBJECTIVE" & ", "
                Sql += "DESCRIPTION = " & "@_DESCRIPTION" & ", "
                Sql += "START_DATE = " & "@_START_DATE" & ", "
                Sql += "END_DATE = " & "@_END_DATE" & ", "
                Sql += "COOPERATION_FRAMEWORK_ID = " & "@_COOPERATION_FRAMEWORK_ID" & ", "
                Sql += "COOPERATION_TYPE_ID = " & "@_COOPERATION_TYPE_ID" & ", "
                Sql += "REGION_OECD_ID = " & "@_REGION_OECD_ID" & ", "
                Sql += "OECD_ID = " & "@_OECD_ID" & ", "
                Sql += "LOCATION = " & "@_LOCATION" & ", "
                Sql += "FUNDING_AGENCY_ID = " & "@_FUNDING_AGENCY_ID" & ", "
                Sql += "EXECUTING_AGENCY_ID = " & "@_EXECUTING_AGENCY_ID" & ", "
                Sql += "IMPLEMENTING_AGENCY_ID = " & "@_IMPLEMENTING_AGENCY_ID" & ", "
                Sql += "ASSISTANT = " & "@_ASSISTANT" & ", "
                Sql += "REMARK = " & "@_REMARK" & ", "
                Sql += "TRANSFER_PROJECT_TO = " & "@_TRANSFER_PROJECT_TO" & ", "
                Sql += "MULTILATERAL_ID = " & "@_MULTILATERAL_ID" & ", "
                Sql += "ALLOCATE_BUDGET = " & "@_ALLOCATE_BUDGET" & ", "
                Sql += "OECD_AID_TYPE_ID = " & "@_OECD_AID_TYPE_ID" & ", "
                Sql += "GRANCE_PERIOD = " & "@_GRANCE_PERIOD" & ", "
                Sql += "MATURITY_PERIOD = " & "@_MATURITY_PERIOD" & ", "
                Sql += "INTEREST_RATE = " & "@_INTEREST_RATE" & ", "
                Sql += "IMPLEMENTING_AGENCY_LOAN = " & "@_IMPLEMENTING_AGENCY_LOAN" & ", "
                Sql += "IMPLEMENTING_AGENCY_CONTRIBUTION = " & "@_IMPLEMENTING_AGENCY_CONTRIBUTION" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" & ", "
                Sql += "ACTIVE_STATUS = " & "@_ACTIVE_STATUS" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table TB_Project
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table TB_Project
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT ID, PROJECT_ID, PROJECT_NAME, PROJECT_TYPE, OBJECTIVE, DESCRIPTION, START_DATE, END_DATE, COOPERATION_FRAMEWORK_ID, COOPERATION_TYPE_ID, REGION_OECD_ID, OECD_ID, LOCATION, FUNDING_AGENCY_ID, EXECUTING_AGENCY_ID, IMPLEMENTING_AGENCY_ID, ASSISTANT, REMARK, TRANSFER_PROJECT_TO, MULTILATERAL_ID, ALLOCATE_BUDGET, OECD_AID_TYPE_ID, GRANCE_PERIOD, MATURITY_PERIOD, INTEREST_RATE, IMPLEMENTING_AGENCY_LOAN, IMPLEMENTING_AGENCY_CONTRIBUTION, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE, ACTIVE_STATUS FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
