Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for TB_Expense_Detail table LinqDB.
    '[Create by  on December, 14 2016]
    Public Class TbExpenseDetailLinqDB
        Public sub TbExpenseDetailLinqDB()

        End Sub 
        ' TB_Expense_Detail
        Const _tableName As String = "TB_Expense_Detail"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _ID As Long = 0
        Dim _ACTIVITY_ID As  System.Nullable(Of Long) 
        Dim _RECIPINCE_ID As  String  = ""
        Dim _RECIPINCE_TYPE As  String  = ""
        Dim _EXPENSE_SUB_ID As  System.Nullable(Of Long) 
        Dim _AMOUNT As  System.Nullable(Of Double) 
        Dim _EXPENSE_ID As  System.Nullable(Of Long) 

        'Generate Field Property 
        <Column(Storage:="_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property ID() As Long
            Get
                Return _ID
            End Get
            Set(ByVal value As Long)
               _ID = value
            End Set
        End Property 
        <Column(Storage:="_ACTIVITY_ID", DbType:="BigInt")>  _
        Public Property ACTIVITY_ID() As  System.Nullable(Of Long) 
            Get
                Return _ACTIVITY_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _ACTIVITY_ID = value
            End Set
        End Property 
        <Column(Storage:="_RECIPINCE_ID", DbType:="VarChar(50)")>  _
        Public Property RECIPINCE_ID() As  String 
            Get
                Return _RECIPINCE_ID
            End Get
            Set(ByVal value As  String )
               _RECIPINCE_ID = value
            End Set
        End Property 
        <Column(Storage:="_RECIPINCE_TYPE", DbType:="VarChar(50)")>  _
        Public Property RECIPINCE_TYPE() As  String 
            Get
                Return _RECIPINCE_TYPE
            End Get
            Set(ByVal value As  String )
               _RECIPINCE_TYPE = value
            End Set
        End Property 
        <Column(Storage:="_EXPENSE_SUB_ID", DbType:="BigInt")>  _
        Public Property EXPENSE_SUB_ID() As  System.Nullable(Of Long) 
            Get
                Return _EXPENSE_SUB_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _EXPENSE_SUB_ID = value
            End Set
        End Property 
        <Column(Storage:="_AMOUNT", DbType:="Double")>  _
        Public Property AMOUNT() As  System.Nullable(Of Double) 
            Get
                Return _AMOUNT
            End Get
            Set(ByVal value As  System.Nullable(Of Double) )
               _AMOUNT = value
            End Set
        End Property 
        <Column(Storage:="_EXPENSE_ID", DbType:="BigInt")>  _
        Public Property EXPENSE_ID() As  System.Nullable(Of Long) 
            Get
                Return _EXPENSE_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _EXPENSE_ID = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _ID = 0
            _ACTIVITY_ID = Nothing
            _RECIPINCE_ID = ""
            _RECIPINCE_TYPE = ""
            _EXPENSE_SUB_ID = Nothing
            _AMOUNT = Nothing
            _EXPENSE_ID = Nothing
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into TB_Expense_Detail table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_Expense_Detail table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _id > 0 Then 

                    Return doUpdate("ID = @_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_Expense_Detail table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from TB_Expense_Detail table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_ID", cID)
                Return doDelete("ID = @_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of TB_Expense_Detail by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doChkData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of TB_Expense_Detail by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cID As Long, trans As SQLTransaction) As TbExpenseDetailLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doGetData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of TB_Expense_Detail by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into TB_Expense_Detail table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _ID = dt.Rows(0)("ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to TB_Expense_Detail table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from TB_Expense_Detail table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(6) As SqlParameter
            cmbParam(0) = New SqlParameter("@_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _ID

            cmbParam(1) = New SqlParameter("@_ACTIVITY_ID", SqlDbType.BigInt)
            If _ACTIVITY_ID IsNot Nothing Then 
                cmbParam(1).Value = _ACTIVITY_ID.Value
            Else
                cmbParam(1).Value = DBNull.value
            End IF

            cmbParam(2) = New SqlParameter("@_RECIPINCE_ID", SqlDbType.VarChar)
            If _RECIPINCE_ID.Trim <> "" Then 
                cmbParam(2).Value = _RECIPINCE_ID.Trim
            Else
                cmbParam(2).Value = DBNull.value
            End If

            cmbParam(3) = New SqlParameter("@_RECIPINCE_TYPE", SqlDbType.VarChar)
            If _RECIPINCE_TYPE.Trim <> "" Then 
                cmbParam(3).Value = _RECIPINCE_TYPE.Trim
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_EXPENSE_SUB_ID", SqlDbType.BigInt)
            If _EXPENSE_SUB_ID IsNot Nothing Then 
                cmbParam(4).Value = _EXPENSE_SUB_ID.Value
            Else
                cmbParam(4).Value = DBNull.value
            End IF

            cmbParam(5) = New SqlParameter("@_AMOUNT", SqlDbType.Decimal)
            If _AMOUNT IsNot Nothing Then 
                cmbParam(5).Value = _AMOUNT.Value
            Else
                cmbParam(5).Value = DBNull.value
            End IF

            cmbParam(6) = New SqlParameter("@_EXPENSE_ID", SqlDbType.BigInt)
            If _EXPENSE_ID IsNot Nothing Then 
                cmbParam(6).Value = _EXPENSE_ID.Value
            Else
                cmbParam(6).Value = DBNull.value
            End IF

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of TB_Expense_Detail by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("activity_id")) = False Then _activity_id = Convert.ToInt64(Rdr("activity_id"))
                        If Convert.IsDBNull(Rdr("recipince_id")) = False Then _recipince_id = Rdr("recipince_id").ToString()
                        If Convert.IsDBNull(Rdr("recipince_type")) = False Then _recipince_type = Rdr("recipince_type").ToString()
                        If Convert.IsDBNull(Rdr("expense_sub_id")) = False Then _expense_sub_id = Convert.ToInt64(Rdr("expense_sub_id"))
                        If Convert.IsDBNull(Rdr("amount")) = False Then _amount = Convert.ToDouble(Rdr("amount"))
                        If Convert.IsDBNull(Rdr("expense_id")) = False Then _expense_id = Convert.ToInt64(Rdr("expense_id"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of TB_Expense_Detail by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As TbExpenseDetailLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("activity_id")) = False Then _activity_id = Convert.ToInt64(Rdr("activity_id"))
                        If Convert.IsDBNull(Rdr("recipince_id")) = False Then _recipince_id = Rdr("recipince_id").ToString()
                        If Convert.IsDBNull(Rdr("recipince_type")) = False Then _recipince_type = Rdr("recipince_type").ToString()
                        If Convert.IsDBNull(Rdr("expense_sub_id")) = False Then _expense_sub_id = Convert.ToInt64(Rdr("expense_sub_id"))
                        If Convert.IsDBNull(Rdr("amount")) = False Then _amount = Convert.ToDouble(Rdr("amount"))
                        If Convert.IsDBNull(Rdr("expense_id")) = False Then _expense_id = Convert.ToInt64(Rdr("expense_id"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table TB_Expense_Detail
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (ACTIVITY_ID, RECIPINCE_ID, RECIPINCE_TYPE, EXPENSE_SUB_ID, AMOUNT, EXPENSE_ID)"
                Sql += " OUTPUT INSERTED.ID, INSERTED.ACTIVITY_ID, INSERTED.RECIPINCE_ID, INSERTED.RECIPINCE_TYPE, INSERTED.EXPENSE_SUB_ID, INSERTED.AMOUNT, INSERTED.EXPENSE_ID"
                Sql += " VALUES("
                sql += "@_ACTIVITY_ID" & ", "
                sql += "@_RECIPINCE_ID" & ", "
                sql += "@_RECIPINCE_TYPE" & ", "
                sql += "@_EXPENSE_SUB_ID" & ", "
                sql += "@_AMOUNT" & ", "
                sql += "@_EXPENSE_ID"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table TB_Expense_Detail
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "ACTIVITY_ID = " & "@_ACTIVITY_ID" & ", "
                Sql += "RECIPINCE_ID = " & "@_RECIPINCE_ID" & ", "
                Sql += "RECIPINCE_TYPE = " & "@_RECIPINCE_TYPE" & ", "
                Sql += "EXPENSE_SUB_ID = " & "@_EXPENSE_SUB_ID" & ", "
                Sql += "AMOUNT = " & "@_AMOUNT" & ", "
                Sql += "EXPENSE_ID = " & "@_EXPENSE_ID" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table TB_Expense_Detail
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table TB_Expense_Detail
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT ID, ACTIVITY_ID, RECIPINCE_ID, RECIPINCE_TYPE, EXPENSE_SUB_ID, AMOUNT, EXPENSE_ID FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
