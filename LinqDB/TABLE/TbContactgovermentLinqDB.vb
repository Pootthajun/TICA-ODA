Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for TB_contactgoverment table LinqDB.
    '[Create by  on December, 9 2016]
    Public Class TbContactgovermentLinqDB
        Public sub TbContactgovermentLinqDB()

        End Sub 
        ' TB_contactgoverment
        Const _tableName As String = "TB_contactgoverment"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _ID As Long = 0
        Dim _MEMID As Long = 0
        Dim _GOVNAMETHAI As String = ""
        Dim _GOVNAMEENG As String = ""
        Dim _KONG As String = ""
        Dim _GOVNUMBER As String = ""
        Dim _GOVROAD As String = ""
        Dim _TAMBOL As String = ""
        Dim _AMPHER As String = ""
        Dim _PROVINCE As String = ""
        Dim _POSTCODE As String = ""
        Dim _TELEPHONE As String = ""
        Dim _FAX As String = ""
        Dim _WEBSITE As String = ""
        Dim _DETAIL As String = ""
        Dim _MINISTRY As  String  = ""
        Dim _EMAIL As  String  = ""
        Dim _CREATED_BY As  String  = ""
        Dim _CREATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _ACTIVE_STATUS As  String  = ""

        'Generate Field Property 
        <Column(Storage:="_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property ID() As Long
            Get
                Return _ID
            End Get
            Set(ByVal value As Long)
               _ID = value
            End Set
        End Property 
        <Column(Storage:="_MEMID", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property MEMID() As Long
            Get
                Return _MEMID
            End Get
            Set(ByVal value As Long)
               _MEMID = value
            End Set
        End Property 
        <Column(Storage:="_GOVNAMETHAI", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property GOVNAMETHAI() As String
            Get
                Return _GOVNAMETHAI
            End Get
            Set(ByVal value As String)
               _GOVNAMETHAI = value
            End Set
        End Property 
        <Column(Storage:="_GOVNAMEENG", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property GOVNAMEENG() As String
            Get
                Return _GOVNAMEENG
            End Get
            Set(ByVal value As String)
               _GOVNAMEENG = value
            End Set
        End Property 
        <Column(Storage:="_KONG", DbType:="VarChar(255) NOT NULL ",CanBeNull:=false)>  _
        Public Property KONG() As String
            Get
                Return _KONG
            End Get
            Set(ByVal value As String)
               _KONG = value
            End Set
        End Property 
        <Column(Storage:="_GOVNUMBER", DbType:="VarChar(50) NOT NULL ",CanBeNull:=false)>  _
        Public Property GOVNUMBER() As String
            Get
                Return _GOVNUMBER
            End Get
            Set(ByVal value As String)
               _GOVNUMBER = value
            End Set
        End Property 
        <Column(Storage:="_GOVROAD", DbType:="VarChar(50) NOT NULL ",CanBeNull:=false)>  _
        Public Property GOVROAD() As String
            Get
                Return _GOVROAD
            End Get
            Set(ByVal value As String)
               _GOVROAD = value
            End Set
        End Property 
        <Column(Storage:="_TAMBOL", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property TAMBOL() As String
            Get
                Return _TAMBOL
            End Get
            Set(ByVal value As String)
               _TAMBOL = value
            End Set
        End Property 
        <Column(Storage:="_AMPHER", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property AMPHER() As String
            Get
                Return _AMPHER
            End Get
            Set(ByVal value As String)
               _AMPHER = value
            End Set
        End Property 
        <Column(Storage:="_PROVINCE", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property PROVINCE() As String
            Get
                Return _PROVINCE
            End Get
            Set(ByVal value As String)
               _PROVINCE = value
            End Set
        End Property 
        <Column(Storage:="_POSTCODE", DbType:="VarChar(20) NOT NULL ",CanBeNull:=false)>  _
        Public Property POSTCODE() As String
            Get
                Return _POSTCODE
            End Get
            Set(ByVal value As String)
               _POSTCODE = value
            End Set
        End Property 
        <Column(Storage:="_TELEPHONE", DbType:="VarChar(40) NOT NULL ",CanBeNull:=false)>  _
        Public Property TELEPHONE() As String
            Get
                Return _TELEPHONE
            End Get
            Set(ByVal value As String)
               _TELEPHONE = value
            End Set
        End Property 
        <Column(Storage:="_FAX", DbType:="VarChar(40) NOT NULL ",CanBeNull:=false)>  _
        Public Property FAX() As String
            Get
                Return _FAX
            End Get
            Set(ByVal value As String)
               _FAX = value
            End Set
        End Property 
        <Column(Storage:="_WEBSITE", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property WEBSITE() As String
            Get
                Return _WEBSITE
            End Get
            Set(ByVal value As String)
               _WEBSITE = value
            End Set
        End Property 
        <Column(Storage:="_DETAIL", DbType:="Text NOT NULL ",CanBeNull:=false)>  _
        Public Property DETAIL() As String
            Get
                Return _DETAIL
            End Get
            Set(ByVal value As String)
               _DETAIL = value
            End Set
        End Property 
        <Column(Storage:="_MINISTRY", DbType:="VarChar(100)")>  _
        Public Property MINISTRY() As  String 
            Get
                Return _MINISTRY
            End Get
            Set(ByVal value As  String )
               _MINISTRY = value
            End Set
        End Property 
        <Column(Storage:="_EMAIL", DbType:="VarChar(100)")>  _
        Public Property EMAIL() As  String 
            Get
                Return _EMAIL
            End Get
            Set(ByVal value As  String )
               _EMAIL = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100)")>  _
        Public Property CREATED_BY() As  String 
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As  String )
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime")>  _
        Public Property CREATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_ACTIVE_STATUS", DbType:="VarChar(50)")>  _
        Public Property ACTIVE_STATUS() As  String 
            Get
                Return _ACTIVE_STATUS
            End Get
            Set(ByVal value As  String )
               _ACTIVE_STATUS = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _ID = 0
            _MEMID = 0
            _GOVNAMETHAI = ""
            _GOVNAMEENG = ""
            _KONG = ""
            _GOVNUMBER = ""
            _GOVROAD = ""
            _TAMBOL = ""
            _AMPHER = ""
            _PROVINCE = ""
            _POSTCODE = ""
            _TELEPHONE = ""
            _FAX = ""
            _WEBSITE = ""
            _DETAIL = ""
            _MINISTRY = ""
            _EMAIL = ""
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
            _ACTIVE_STATUS = ""
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into TB_contactgoverment table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _ID = DB.GetNextID("ID",tableName, trans)
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_contactgoverment table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _id > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("ID = @_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_contactgoverment table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from TB_contactgoverment table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_ID", cID)
                Return doDelete("ID = @_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of TB_contactgoverment by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doChkData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of TB_contactgoverment by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cID As Long, trans As SQLTransaction) As TbContactgovermentLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doGetData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of TB_contactgoverment by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into TB_contactgoverment table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to TB_contactgoverment table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from TB_contactgoverment table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(21) As SqlParameter
            cmbParam(0) = New SqlParameter("@_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _ID

            cmbParam(1) = New SqlParameter("@_MEMID", SqlDbType.Int)
            cmbParam(1).Value = _MEMID

            cmbParam(2) = New SqlParameter("@_GOVNAMETHAI", SqlDbType.VarChar)
            cmbParam(2).Value = _GOVNAMETHAI.Trim

            cmbParam(3) = New SqlParameter("@_GOVNAMEENG", SqlDbType.VarChar)
            cmbParam(3).Value = _GOVNAMEENG.Trim

            cmbParam(4) = New SqlParameter("@_KONG", SqlDbType.VarChar)
            cmbParam(4).Value = _KONG.Trim

            cmbParam(5) = New SqlParameter("@_GOVNUMBER", SqlDbType.VarChar)
            cmbParam(5).Value = _GOVNUMBER.Trim

            cmbParam(6) = New SqlParameter("@_GOVROAD", SqlDbType.VarChar)
            cmbParam(6).Value = _GOVROAD.Trim

            cmbParam(7) = New SqlParameter("@_TAMBOL", SqlDbType.VarChar)
            cmbParam(7).Value = _TAMBOL.Trim

            cmbParam(8) = New SqlParameter("@_AMPHER", SqlDbType.VarChar)
            cmbParam(8).Value = _AMPHER.Trim

            cmbParam(9) = New SqlParameter("@_PROVINCE", SqlDbType.VarChar)
            cmbParam(9).Value = _PROVINCE.Trim

            cmbParam(10) = New SqlParameter("@_POSTCODE", SqlDbType.VarChar)
            cmbParam(10).Value = _POSTCODE.Trim

            cmbParam(11) = New SqlParameter("@_TELEPHONE", SqlDbType.VarChar)
            cmbParam(11).Value = _TELEPHONE.Trim

            cmbParam(12) = New SqlParameter("@_FAX", SqlDbType.VarChar)
            cmbParam(12).Value = _FAX.Trim

            cmbParam(13) = New SqlParameter("@_WEBSITE", SqlDbType.VarChar)
            cmbParam(13).Value = _WEBSITE.Trim

            cmbParam(14) = New SqlParameter("@_DETAIL", SqlDbType.Text)
            cmbParam(14).Value = _DETAIL.Trim

            cmbParam(15) = New SqlParameter("@_MINISTRY", SqlDbType.VarChar)
            If _MINISTRY.Trim <> "" Then 
                cmbParam(15).Value = _MINISTRY.Trim
            Else
                cmbParam(15).Value = DBNull.value
            End If

            cmbParam(16) = New SqlParameter("@_EMAIL", SqlDbType.VarChar)
            If _EMAIL.Trim <> "" Then 
                cmbParam(16).Value = _EMAIL.Trim
            Else
                cmbParam(16).Value = DBNull.value
            End If

            cmbParam(17) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            If _CREATED_BY.Trim <> "" Then 
                cmbParam(17).Value = _CREATED_BY.Trim
            Else
                cmbParam(17).Value = DBNull.value
            End If

            cmbParam(18) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            If _CREATED_DATE.Value.Year > 1 Then 
                cmbParam(18).Value = _CREATED_DATE.Value
            Else
                cmbParam(18).Value = DBNull.value
            End If

            cmbParam(19) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(19).Value = _UPDATED_BY.Trim
            Else
                cmbParam(19).Value = DBNull.value
            End If

            cmbParam(20) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(20).Value = _UPDATED_DATE.Value
            Else
                cmbParam(20).Value = DBNull.value
            End If

            cmbParam(21) = New SqlParameter("@_ACTIVE_STATUS", SqlDbType.VarChar)
            If _ACTIVE_STATUS.Trim <> "" Then 
                cmbParam(21).Value = _ACTIVE_STATUS.Trim
            Else
                cmbParam(21).Value = DBNull.value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of TB_contactgoverment by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("memid")) = False Then _memid = Convert.ToInt32(Rdr("memid"))
                        If Convert.IsDBNull(Rdr("govnamethai")) = False Then _govnamethai = Rdr("govnamethai").ToString()
                        If Convert.IsDBNull(Rdr("govnameeng")) = False Then _govnameeng = Rdr("govnameeng").ToString()
                        If Convert.IsDBNull(Rdr("kong")) = False Then _kong = Rdr("kong").ToString()
                        If Convert.IsDBNull(Rdr("govnumber")) = False Then _govnumber = Rdr("govnumber").ToString()
                        If Convert.IsDBNull(Rdr("govroad")) = False Then _govroad = Rdr("govroad").ToString()
                        If Convert.IsDBNull(Rdr("tambol")) = False Then _tambol = Rdr("tambol").ToString()
                        If Convert.IsDBNull(Rdr("ampher")) = False Then _ampher = Rdr("ampher").ToString()
                        If Convert.IsDBNull(Rdr("province")) = False Then _province = Rdr("province").ToString()
                        If Convert.IsDBNull(Rdr("postcode")) = False Then _postcode = Rdr("postcode").ToString()
                        If Convert.IsDBNull(Rdr("telephone")) = False Then _telephone = Rdr("telephone").ToString()
                        If Convert.IsDBNull(Rdr("fax")) = False Then _fax = Rdr("fax").ToString()
                        If Convert.IsDBNull(Rdr("website")) = False Then _website = Rdr("website").ToString()
                        If Convert.IsDBNull(Rdr("detail")) = False Then _detail = Rdr("detail").ToString()
                        If Convert.IsDBNull(Rdr("ministry")) = False Then _ministry = Rdr("ministry").ToString()
                        If Convert.IsDBNull(Rdr("email")) = False Then _email = Rdr("email").ToString()
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                        If Convert.IsDBNull(Rdr("ACTIVE_STATUS")) = False Then _ACTIVE_STATUS = Rdr("ACTIVE_STATUS").ToString()
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of TB_contactgoverment by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As TbContactgovermentLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("memid")) = False Then _memid = Convert.ToInt32(Rdr("memid"))
                        If Convert.IsDBNull(Rdr("govnamethai")) = False Then _govnamethai = Rdr("govnamethai").ToString()
                        If Convert.IsDBNull(Rdr("govnameeng")) = False Then _govnameeng = Rdr("govnameeng").ToString()
                        If Convert.IsDBNull(Rdr("kong")) = False Then _kong = Rdr("kong").ToString()
                        If Convert.IsDBNull(Rdr("govnumber")) = False Then _govnumber = Rdr("govnumber").ToString()
                        If Convert.IsDBNull(Rdr("govroad")) = False Then _govroad = Rdr("govroad").ToString()
                        If Convert.IsDBNull(Rdr("tambol")) = False Then _tambol = Rdr("tambol").ToString()
                        If Convert.IsDBNull(Rdr("ampher")) = False Then _ampher = Rdr("ampher").ToString()
                        If Convert.IsDBNull(Rdr("province")) = False Then _province = Rdr("province").ToString()
                        If Convert.IsDBNull(Rdr("postcode")) = False Then _postcode = Rdr("postcode").ToString()
                        If Convert.IsDBNull(Rdr("telephone")) = False Then _telephone = Rdr("telephone").ToString()
                        If Convert.IsDBNull(Rdr("fax")) = False Then _fax = Rdr("fax").ToString()
                        If Convert.IsDBNull(Rdr("website")) = False Then _website = Rdr("website").ToString()
                        If Convert.IsDBNull(Rdr("detail")) = False Then _detail = Rdr("detail").ToString()
                        If Convert.IsDBNull(Rdr("ministry")) = False Then _ministry = Rdr("ministry").ToString()
                        If Convert.IsDBNull(Rdr("email")) = False Then _email = Rdr("email").ToString()
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                        If Convert.IsDBNull(Rdr("ACTIVE_STATUS")) = False Then _ACTIVE_STATUS = Rdr("ACTIVE_STATUS").ToString()
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table TB_contactgoverment
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (ID, MEMID, GOVNAMETHAI, GOVNAMEENG, KONG, GOVNUMBER, GOVROAD, TAMBOL, AMPHER, PROVINCE, POSTCODE, TELEPHONE, FAX, WEBSITE, DETAIL, MINISTRY, EMAIL, CREATED_BY, CREATED_DATE, ACTIVE_STATUS)"
                Sql += " OUTPUT INSERTED.ID, INSERTED.MEMID, INSERTED.GOVNAMETHAI, INSERTED.GOVNAMEENG, INSERTED.KONG, INSERTED.GOVNUMBER, INSERTED.GOVROAD, INSERTED.TAMBOL, INSERTED.AMPHER, INSERTED.PROVINCE, INSERTED.POSTCODE, INSERTED.TELEPHONE, INSERTED.FAX, INSERTED.WEBSITE, INSERTED.DETAIL, INSERTED.MINISTRY, INSERTED.EMAIL, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE, INSERTED.ACTIVE_STATUS"
                Sql += " VALUES("
                sql += "@_ID" & ", "
                sql += "@_MEMID" & ", "
                sql += "@_GOVNAMETHAI" & ", "
                sql += "@_GOVNAMEENG" & ", "
                sql += "@_KONG" & ", "
                sql += "@_GOVNUMBER" & ", "
                sql += "@_GOVROAD" & ", "
                sql += "@_TAMBOL" & ", "
                sql += "@_AMPHER" & ", "
                sql += "@_PROVINCE" & ", "
                sql += "@_POSTCODE" & ", "
                sql += "@_TELEPHONE" & ", "
                sql += "@_FAX" & ", "
                sql += "@_WEBSITE" & ", "
                sql += "@_DETAIL" & ", "
                sql += "@_MINISTRY" & ", "
                sql += "@_EMAIL" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE" & ", "
                sql += "@_ACTIVE_STATUS"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table TB_contactgoverment
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "MEMID = " & "@_MEMID" & ", "
                Sql += "GOVNAMETHAI = " & "@_GOVNAMETHAI" & ", "
                Sql += "GOVNAMEENG = " & "@_GOVNAMEENG" & ", "
                Sql += "KONG = " & "@_KONG" & ", "
                Sql += "GOVNUMBER = " & "@_GOVNUMBER" & ", "
                Sql += "GOVROAD = " & "@_GOVROAD" & ", "
                Sql += "TAMBOL = " & "@_TAMBOL" & ", "
                Sql += "AMPHER = " & "@_AMPHER" & ", "
                Sql += "PROVINCE = " & "@_PROVINCE" & ", "
                Sql += "POSTCODE = " & "@_POSTCODE" & ", "
                Sql += "TELEPHONE = " & "@_TELEPHONE" & ", "
                Sql += "FAX = " & "@_FAX" & ", "
                Sql += "WEBSITE = " & "@_WEBSITE" & ", "
                Sql += "DETAIL = " & "@_DETAIL" & ", "
                Sql += "MINISTRY = " & "@_MINISTRY" & ", "
                Sql += "EMAIL = " & "@_EMAIL" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" & ", "
                Sql += "ACTIVE_STATUS = " & "@_ACTIVE_STATUS" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table TB_contactgoverment
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table TB_contactgoverment
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT ID, MEMID, GOVNAMETHAI, GOVNAMEENG, KONG, GOVNUMBER, GOVROAD, TAMBOL, AMPHER, PROVINCE, POSTCODE, TELEPHONE, FAX, WEBSITE, DETAIL, MINISTRY, EMAIL, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE, ACTIVE_STATUS FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
