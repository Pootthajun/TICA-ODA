﻿Imports System.Data

Partial Class frmSettingOUPerson
    Inherits System.Web.UI.Page

    Dim BL As New ODAENG

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Public Property AllData As DataTable
        Get
            Try
                Return Session("UserList")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("UserList") = value
        End Set
    End Property

    Private Sub frmSettingOUPerson_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            BindList()
        End If
    End Sub

    Private Sub BindList()
        Dim dt As DataTable = BL.GetUserList(txtSearch.Text, ddlStatus.SelectedValue)
        rptList.DataSource = dt
        rptList.DataBind()

        lblCount.Text = dt.DefaultView.Count

        AllData = dt
        Pager.SesssionSourceName = "UserList"
        Pager.RenderLayout()
        pnlAdSearch.Visible = False
    End Sub

    Private Sub ClearForm()
        clearFormSearch()
        UCFormOUPerson.ClearData()
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnStatusReady As LinkButton = DirectCast(e.Item.FindControl("btnStatusReady"), LinkButton)
        Dim btnStatusDisabled As LinkButton = DirectCast(e.Item.FindControl("btnStatusDisabled"), LinkButton)

        If e.Item.DataItem("active_status").ToString = "Y" Then
            btnStatusReady.Visible = True
            btnStatusDisabled.Visible = False
        Else
            btnStatusDisabled.Visible = True
            btnStatusReady.Visible = False
        End If
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand

        Dim lblNodeID As Label = e.Item.FindControl("lblNodeID")

        If e.CommandName = "cmdEdit" Then
            ClearForm()
            pnlUserList.Visible = False
            pnlUserEdit.Visible = True
            UCFormOUPerson.BindDDLExecutingOrganize()
            UCFormOUPerson.FillInDataPerson(lblNodeID.Text)

        ElseIf e.CommandName = "cmdDelete" Then

            Dim BL As New ODAENG
            'Dim ret As ProcessReturnInfo
            'ret =

            If BL.DeleteOUPerson(e.CommandArgument) Then
                Alert("ลบข้อมูลเรียบร้อยแล้ว")
                BindList()
            Else
                Alert("ไม่สามารถลบข้อมูลได้")
            End If

        ElseIf e.CommandName = "cmdstatusready" Then
            Dim BL As New ODAENG
            'Dim ret As ProcessReturnInfo
            'ret =
            BL.EditStatusAllY(e.CommandArgument, "TB_OU_Person")
            'If ret.IsSuccess Then
            '    Alert("แก้ไขสถานะเรียบร้อยแล้ว")
            BindList()
            'Else
            '    Alert(ret.ErrorMessage)
            'End If
        ElseIf e.CommandName = "cmdstatusDisabled" Then
            Dim BL As New ODAENG
            'Dim ret As ProcessReturnInfo
            'ret =
            BL.EditStatusAllN(e.CommandArgument, "TB_OU_Person")
            'If ret.IsSuccess Then
            '    Alert("แก้ไขสถานะเรียบร้อยแล้ว")
            BindList()
            'Else
            '    Alert(ret.ErrorMessage)
            'End If
        End If

    End Sub

    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim ret As ProcessReturnInfo = UCFormOUPerson.SavePerson(UserName, False)
        If ret.IsSuccess Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว'); window.location ='frmSettingOUPerson.aspx';", True)
            ClearForm()
        Else
            Alert("ไม่สามารถบันทึกข้อมูลได้ " & ret.ErrorMessage)
        End If
    End Sub

    Private Sub btnCancle_Click(sender As Object, e As EventArgs) Handles btnCancle.Click
        ClearForm()
        pnlUserList.Visible = True
        pnlUserEdit.Visible = False
    End Sub



    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        pnlUserList.Visible = False
        pnlUserEdit.Visible = True

        ClearForm()
        UCFormOUPerson.BindDDLExecutingOrganize()
    End Sub

    Private Sub btnOpenSearch_Click(sender As Object, e As EventArgs) Handles btnOpenSearch.Click
        pnlAdSearch.Visible = True
    End Sub

    Private Sub btnCancelSearch_Click(sender As Object, e As EventArgs) Handles btnCancelSearch.Click
        clearFormSearch()
        ClearForm()
        pnlAdSearch.Visible = False
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Sub clearFormSearch()
        txtSearch.Text = ""

        ddlStatus.SelectedValue = ""
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindList()
    End Sub
End Class
