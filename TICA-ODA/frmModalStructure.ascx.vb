﻿Imports ODAENG
Imports ProcessReturnInfo
Imports Constants
Imports LinqDB.TABLE
Imports System.Data
Imports LinqDB


Public Class frmChart
    Inherits System.Web.UI.UserControl

    Dim ENG As New ODAENG
    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    'Private _strID As String
    'Public Property strID() As String
    '    Get
    '        Return _strID
    '    End Get
    '    Set(ByVal value As String)
    '        lblCountryName.Text = value

    '    End Set
    'End Property

    'Public Function SetData() As Boolean
    '    txtCountryNameTH.Text = lblCountryName.Text
    '    txtCountryNameEN.Text = lblCountryName.Text & "xx"

    '    myModalCountry.Visible = True
    '    Return True
    'End Function

    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


    '    If Not IsPostBack Then
    '        BindDataTODDL()
    '    End If
    'End Sub



    'Protected Sub GetDataCountry(ByVal NODE_ID As Integer)

    '    Dim dt As New DataTable
    '    dt = ENG.GetOrganizeUnitByNodeID(NODE_ID, OU.Country)
    '    Dim strWhere = "node_id=" & "'" & NODE_ID.ToString() & "'"
    '    dt.DefaultView.RowFilter = strWhere
    '    Dim _c As String = dt.DefaultView.Count

    '    For i = 0 To dt.DefaultView.Count - 1
    '        txtCountryNameEN.Text = dt(i)("name_en").ToString()
    '        txtCountryNameTH.Text = dt(i)("name_th").ToString()
    '        If dt(i)("description").ToString() <> "" Then
    '            txtDescriptionCountry.Text = dt(i)("description").ToString()
    '        Else
    '            txtDescriptionCountry.Text = ""
    '        End If

    '        If dt(i)("active_status").ToString() = "Y" Then
    '            chkACTIVE_STATUS_Country.Checked = True
    '        Else
    '            chkACTIVE_STATUS_Country.Checked = False
    '        End If
    '    Next

    'End Sub

    'Protected Sub BindDataTODDL()
    '    ENG.Bind_DDL_CountryRegion(DDL_CountryRegion)
    '    ENG.Bind_DDL_CountryGroup(DDL_CountryGroup)
    '    ENG.Bind_DDL_CountryZone(DDL_CountryZone)

    'End Sub

    Public Property EditCountryID As String
        Get
            Try
                Return ViewState("CountryID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            ViewState("CountryID") = value
        End Set
    End Property


    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub



    'Protected Sub btnSave_Country_Click1(sender As Object, e As EventArgs) Handles btnSave_Country.Click
    '    If txtCountryNameTH.Text = "" And txtCountryNameEN.Text = "" Then
    '        Alert("กรุณากรอกชื่อประเทศภาษาไทยและภาษาอังกฤษ")
    '        txtCountryNameTH.Focus()

    '        Exit Sub
    '    End If


    '    Dim linqCountry As New TbOuCountryLinqDB
    '    linqCountry.NODE_ID = EditCountryID
    '    linqCountry.NAME_TH = txtCountryNameTH.Text.Trim.Replace("'", "''")
    '    linqCountry.NAME_EN = txtCountryNameEN.Text.Trim.Replace("'", "''")
    '    linqCountry.DESCRIPTION = txtDescriptionCountry.Text.Trim.Replace("'", "''")

    '    If chkACTIVE_STATUS_Country.Checked Then
    '        linqCountry.ACTIVE_STATUS = "Y"
    '    Else
    '        linqCountry.ACTIVE_STATUS = "N"
    '    End If

    '    Dim ret As New ProcessReturnInfo

    '    Dim dtR As New DataTable 'Country Region
    '    dtR.Columns.Add("country_region_id")
    '    dtR.Columns.Add("country_id")
    '    Dim drR As DataRow
    '    drR = dtR.NewRow
    '    drR("country_region_id") = DDL_CountryRegion.SelectedValue.ToString()
    '    drR("country_id") = ""
    '    dtR.Rows.Add(drR)

    '    Dim dtG As New DataTable 'Country Group
    '    dtG.Columns.Add("country_group_id")
    '    dtG.Columns.Add("country_id")
    '    Dim drG As DataRow
    '    drG = dtG.NewRow
    '    drG("country_group_id") = DDL_CountryGroup.SelectedValue.ToString()
    '    drG("country_id") = ""
    '    dtG.Rows.Add(drG)


    '    Dim dtZ As New DataTable 'Country Zone
    '    dtZ.Columns.Add("country_zone_id")
    '    dtZ.Columns.Add("country_id")
    '    Dim drZ As DataRow
    '    drZ = dtZ.NewRow
    '    drZ("country_zone_id") = DDL_CountryZone.SelectedValue.ToString()
    '    drZ("country_id") = ""
    '    dtZ.Rows.Add(drZ)

    '    ret = ENG.SaveOUCountry(linqCountry, dtR, dtG, dtZ, "Administrator")
    '    If ret.IsSuccess = True Then
    '        ' Alert("บันทึกข้อมูลเรียบร้อยแล้ว")                                                                     ';window.location ='frmStructure.aspx';
    '        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');window.location ='frmStructure.aspx';", True)
    '        ClearData()
    '    Else
    '        Alert(ret.ErrorMessage)
    '    End If
    'End Sub

    Protected Sub ClearData()

        ''Clear Dialog Country
        'lblCountryName.Text = ""
        'txtCountryNameEN.Text = ""
        'txtCountryNameTH.Text = ""
        'txtDescriptionCountry.Text = ""
        'chkACTIVE_STATUS_Country.Checked = False



        'Clear Dialog Person
        lblPersonCountryName.Text = ""
        lblPersonOrganizeName.Text = ""
        lblPersonSubOrganizeName.Text = ""

        txtPersonNameEN.Text = ""
        txtPersonNameTH.Text = ""
        txtPersonIDCardPassport.Text = ""
        txtPersonBirthDay.Text = ""
        txtPersonTelephone.Text = ""
        txtPersonEmail.Text = ""
        txtPersonNote.Text = ""
        chkACTIVE_STATUS_Person.Checked = False
    End Sub

    'Protected Sub btnAgencySave_Click(sender As Object, e As EventArgs) Handles btnAgencySave.Click

    '    If txtAgencyNameTH.Text = "" And txtAgencyNameEN.Text = "" Then
    '        Alert("กรุณากรอกชื่อหน่วยงานภาษาไทยและภาษาอังกฤษ")
    '        txtAgencyNameTH.Focus()

    '        Exit Sub
    '    End If

    '    Dim linqOrganize As New TbOuOrganizeLinqDB
    '    linqOrganize.NODE_ID = EditCountryID
    '    linqOrganize.NAME_TH = txtAgencyNameTH.Text.Trim.Replace("'", "''")
    '    linqOrganize.NAME_EN = txtAgencyNameEN.Text.Trim.Replace("'", "''")
    '    linqOrganize.PARENT_ID = ""
    '    linqOrganize.ABBR_NAME_TH = txtAgencyABBRNameTH.Text.Trim.Replace("'", "''")
    '    linqOrganize.ABBR_NAME_EN = txtAgencyABBRNameEN.Text.Trim.Replace("'", "''")

    '    If chkACTIVE_STATUS_Agency.Checked Then
    '        linqOrganize.ACTIVE_STATUS = "Y"
    '    Else
    '        linqOrganize.ACTIVE_STATUS = "N"
    '    End If

    '    Dim ret As New ProcessReturnInfo

    '    Dim dtA As New DataTable
    '    dtA.Columns.Add("address")
    '    dtA.Columns.Add("telephone")
    '    dtA.Columns.Add("fax")
    '    dtA.Columns.Add("website")
    '    dtA.Columns.Add("description")
    '    dtA.Columns.Add("email")
    '    Dim drC As DataRow
    '    drC = dtA.NewRow
    '    drC("address") = txtAgencyLocation.Text.Trim.Replace("'", "''")
    '    drC("telephone") = txtAgencyTelephone.Text.Trim.Replace("'", "''")
    '    drC("fax") = txtAgencyFax.Text.Trim.Replace("'", "''")
    '    drC("website") = txtAgencyWebsite.Text.Trim.Replace("'", "''")
    '    drC("description") = txtAgencyNote.Text.Trim.Replace("'", "''")
    '    drC("email") = txtAgencyEmail.Text.Trim.Replace("'", "''")
    '    dtA.Rows.Add(drC)


    '    ret = ENG.SaveOUOrganize(linqOrganize, dtA, "Administrator")
    '    If ret.IsSuccess = True Then
    '        ' Alert("บันทึกข้อมูลเรียบร้อยแล้ว")                                                                     ';window.location ='frmStructure.aspx';
    '        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');window.location ='frmStructure.aspx';", True)
    '        ClearData()
    '    Else
    '        Alert(ret.ErrorMessage)
    '    End If

    'End Sub
    Protected Sub btnPersonSave_Click(sender As Object, e As EventArgs) Handles btnPersonSave.Click
        If txtPersonNameTH.Text = "" And txtPersonNameEN.Text = "" Then
            Alert("กรุณากรอกชื่อภาษาไทยและภาษาอังกฤษ")
            txtPersonNameTH.Focus()

            Exit Sub
        End If

        Dim linqPerson As New TbOuPersonLinqDB
        linqPerson.NODE_ID = EditCountryID
        linqPerson.NAME_TH = txtPersonNameTH.Text.Trim.Replace("'", "''")
        linqPerson.NAME_EN = txtPersonNameEN.Text.Trim.Replace("'", "''")


        If chkACTIVE_STATUS_Person.Checked Then
            linqPerson.ACTIVE_STATUS = "Y"
        Else
            linqPerson.ACTIVE_STATUS = "N"
        End If

        Dim ret As New ProcessReturnInfo

        Dim dtP As New DataTable
        dtP.Columns.Add("address")
        dtP.Columns.Add("telephone")
        dtP.Columns.Add("fax")
        dtP.Columns.Add("website")
        dtP.Columns.Add("description")
        dtP.Columns.Add("email")
        'Dim drP As DataRow
        'drP = dtP.NewRow
        'drP("address") = txtAgencyLocation.Text.Trim.Replace("'", "''")
        'drP("telephone") = txtAgencyTelephone.Text.Trim.Replace("'", "''")
        'drP("fax") = txtAgencyFax.Text.Trim.Replace("'", "''")
        'drP("website") = txtAgencyWebsite.Text.Trim.Replace("'", "''")
        'drP("description") = txtAgencyNote.Text.Trim.Replace("'", "''")
        'drP("email") = txtAgencyEmail.Text.Trim.Replace("'", "''")
        'dtP.Rows.Add(drP)


        ret = ENG.SaveOUPerson(linqPerson, dtP, Person_Type.Recipient, UserName, True)
        If ret.IsSuccess = True Then
            ' Alert("บันทึกข้อมูลเรียบร้อยแล้ว")                                                                     ';window.location ='frmStructure.aspx';
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');window.location ='frmStructure.aspx';", True)
            ClearData()
        Else
            Alert(ret.ErrorMessage)
        End If
    End Sub
End Class