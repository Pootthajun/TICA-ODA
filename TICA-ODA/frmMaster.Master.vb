﻿Imports System.Data
Imports System.Data.SqlClient
Imports LinqDB.ConnectDB
Imports Constants

Public Class frmMaster
    Inherits System.Web.UI.MasterPage
    Dim BL As New ODAENG
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindData()
            checkManu()
            If Session("UserName") Is Nothing Then

                Response.Redirect("frmLogin.aspx")
                Exit Sub
            End If

            lblName.Text = Session("FullNameTH")
            lblDetail.Text = Session("FullNameTH")

            BindMenu()
        End If

    End Sub

    Protected Sub checkManu()

        Dim DT_Authorize As New DataTable
        Dim sql As String = ""
        sql &= " select A.menu_id,is_save,is_view,is_na from TB_OU_Person As P join TB_Authorize As A On P.node_id = A.node_id " & VbLf
        sql &= " where P.username = '" + Session("UserName") + "' order by A.menu_id " & VbLf

        Dim DA As New SqlDataAdapter(sql, BL.ConnectionString)
        DA.Fill(DT_Authorize)
        Session("Authorize") = DT_Authorize

    End Sub

    Protected Sub BindData()
        Dim sql As String = "SELECT id,govnamethai,govnameeng,kong,govnumber,govroad,tambol,ampher,province,postcode" & vbLf
        sql &= ",telephone,fax,website,detail,ministry,email,ACTIVE_STATUS" & vbLf
        sql &= " FROM tb_contactgoverment WHERE id=1" & vbLf
        Dim trans As New TransactionDB

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(sql, BL.ConnectionString)
        DA.Fill(DT)


        If dt.Rows.Count > 0 Then
            trans.CommitTransaction()
            For i As Integer = 0 To dt.Rows.Count - 1
                If dt.Rows(i)("id").ToString() <> "" Then
                    lblGOVEN.Text = dt(i)("govnameeng").ToString()
                    lblBuliding.Text = dt(i)("govnumber").ToString()
                    lblRoad.Text = dt(i)("govroad").ToString()
                    lblProvice.Text = dt(i)("province").ToString()

                    lblZipCode.Text = dt(i)("postcode").ToString()
                    lblTelephone.Text = dt(i)("telephone").ToString()

                    lblFax.Text = dt(i)("fax").ToString()
                    lblWebSite.Text = dt(i)("website").ToString()

                    lblEmail.Text = dt(i)("email").ToString()
                    lblMinistry.Text = dt(i)("ministry").ToString()

                End If
            Next

        Else
            trans.RollbackTransaction()
            Dim _err = trans.ErrorMessage()
        End If
    End Sub

    Sub SetVisibleMenuToDefault()
        ''overall
        'mnuProjectStatus.Visible = False

        ''---Tab Allocated Budget---
        'mnuAllocatedBudget.Visible = False 'Budget
        'aBudget.Visible = False 'Incoming Budget
        'aBudgetReport.Visible = False 'Report

        'mnuMasterBudget.Visible = False 'Budget Setting
        'aGroupBudget.Visible = False 'Budget Group
        'aSubBudget.Visible = False 'Sub Budget

        ''---Tab Finance---
        ''Finance | mnuFinance 
        'aProjectExpense.Visible = False 'Expense
        'aExpenseReport.Visible = False 'Report

        ''Setting | mnuMasterExpense
        'aExpense.Visible = False 'Expense Group
        'aSubExpense.Visible = False 'Sub Expense

        ''---Tab Master---
        ''Master | mnuMaster
        ''aPlan.Visible = False 'Plan and policies
        'aCountryGroup.Visible = False 'Country Group
        'aOECD.Visible = False 'OECD
        'aRegionOECD.Visible = False 'OECD Region
        'aCooperationFramework.Visible = False 'Cooperation Framework
        'aCooperationType.Visible = False 'Cooperation Type
        'aSector.Visible = False 'Sector
        'aSubSector.Visible = False 'Sub Sector
        'aComponent.Visible = False 'Component
        'aMultilateral.Visible = False  'Multilateral
        'aInKind.Visible = False 'In Kind

        ''-- Org Structure --
        'mnuStructure.Visible = False

        ''-- Report --
        'mnuReports.Visible = False
    End Sub
    Protected Sub BindMenu()
        'SetVisibleMenuToDefault()
        Dim dt As New DataTable
        If Session("Authorize") Is Nothing Then Exit Sub
        dt = CType(Session("Authorize"), DataTable)

        Dim dtmenu As DataTable = BL.GetList_AllMenu()
        For i As Integer = 0 To dtmenu.Rows.Count - 1
            Dim _menu_id As String = dtmenu.Rows(i)("id").ToString
            Dim tmpdr() As DataRow = dt.Select("menu_id = '" & _menu_id & "' and (isnull(is_save,'N') ='Y' or isnull(is_view,'N') ='Y')")
            If tmpdr.Length = 0 Then
                Select Case _menu_id
                    Case Menu.Overall
                        mnuProjectStatus.Visible = False
                    Case Menu.Project

                    Case Menu.NonProject

                    Case Menu.Loan

                    Case Menu.Contribution

                    Case Menu.IncomingBudget
                        aBudget.Visible = False
                    Case Menu.BudgetGroup
                        aGroupBudget.Visible = False
                    Case Menu.SubBudget
                        aSubBudget.Visible = False
                    'Case Menu.BudgetReport
                    '    aBudgetReport.Visible = False
                    Case Menu.Expense
                        aProjectExpense.Visible = False
                    Case Menu.ExpenseGroup
                        aExpense.Visible = False
                    Case Menu.SubExpense
                        aSubExpense.Visible = False
                    'Case Menu.ExpenseReport
                    '    aExpenseReport.Visible = False
                    Case Menu.Report
                        mnuReports.Visible = False
                    Case Menu.OrganizeStructure
                        mnuStructure.Visible = False
                    Case Menu.CountryGroup
                        aCountryGroup.Visible = False
                    Case Menu.OECD
                        aOECD.Visible = False
                    Case Menu.OECDRegion
                        aRegionOECD.Visible = False
                    Case Menu.OECDRegionZone
                        aRegionZoneOECD.Visible = False
                    Case Menu.CooperationFramework
                        aCooperationFramework.Visible = False
                    Case Menu.CooperationType
                        aCooperationType.Visible = False
                    Case Menu.Sector
                        aSector.Visible = False
                    Case Menu.SubSector
                        aSubSector.Visible = False
                    Case Menu.Component
                        aComponent.Visible = False
                    Case Menu.Multilateral
                        aMultilateral.Visible = False
                    Case Menu.InKind
                        aInKind.Visible = False
                    Case Menu.Person
                        aPerson.Visible = False
                        'Case Menu.rptProject
                        '    arptProject.Visible = False
                        'Case Menu.rptCLMU
                        '    arptCLMU.Visible = False
                End Select
            End If
        Next

        If aExpense.Visible = False And aSubExpense.Visible = False Then
            mnuMasterExpense.Visible = False
        End If

        If aProjectExpense.Visible = False And aExpense.Visible = False And aSubExpense.Visible = False Then
            mnuFinance.Visible = False
        End If

        If aGroupBudget.Visible = False And aSubBudget.Visible = False Then
            mnuMasterBudget.Visible = False
        End If

        'If aBudget.Visible = False And aGroupBudget.Visible = False And aSubBudget.Visible = False And aBudgetReport.Visible = False Then
        '    mnuMasterBudget.Visible = False
        'End If
        If aBudget.Visible = False And aGroupBudget.Visible = False And aSubBudget.Visible = False Then
            mnuMasterBudget.Visible = False
        End If

        If aCountryGroup.Visible = False And aOECD.Visible = False And aRegionOECD.Visible = False And aRegionZoneOECD.Visible = False And aCooperationFramework.Visible = False _
            And aCooperationType.Visible = False And aSector.Visible = False And aSubSector.Visible = False And aComponent.Visible = False _
            And aMultilateral.Visible = False And aInKind.Visible = False And aPerson.Visible = False Then
            mnuMaster.Visible = False
        End If

        'If arptProject.Visible = False And arptCLMU.Visible = False Then
        '    mnuReports.Visible = False
        'End If
    End Sub
End Class