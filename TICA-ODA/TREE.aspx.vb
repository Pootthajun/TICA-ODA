﻿Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data
Imports System.Data.SqlClient

Partial Class TREE
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            BindTreeData()
        End If
    End Sub

    Protected Sub BindTreeData()

        Dim BL As New ODAENG
        Dim Sql As String = "SELECT * FROM TB_OU_Country" 'OU
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
        DA.Fill(DT)

        For i As Integer = 0 To DT.Rows.Count()
            If DT.Rows(i)("Node_ID").ToString() <> "" Then

                Dim node As New TreeNode
                node.Text = DT(i)("Name_EN").ToString()
                node.Value = DT(i)("Node_ID").ToString()

                TreeView1.Nodes.Add(node)

                If DT(i)("Node_ID") = 194 Then
                    Dim Sqlchild As String = "SELECT * FROM TB_OU_Organize where Parent_ID=" & DT(i)("Node_ID")

                    Dim dtchild As New DataTable
                    DA = New SqlDataAdapter(Sql, BL.ConnectionString)
                    DA.Fill(dtchild)

                    For j As Integer = 0 To dtchild.Rows.Count - 1
                        Dim childNode As New TreeNode
                        childNode.Text = dtchild(j)("Name_EN").ToString()
                        childNode.Value = dtchild(j)("Node_ID").ToString()
                        node.ChildNodes.Add(childNode)
                    Next
                    node.CollapseAll()
                    'Else
                    'Dim _node As New TreeNode
                    '_node.Text = ""
                    '_node.Value = ""

                    'node.ChildNodes.Add(_node)
                    'node.CollapseAll()
                End If
                Dim _node As New TreeNode
                _node.Text = ""
                _node.Value = ""

                node.ChildNodes.Add(_node)
                node.CollapseAll()
            End If
        Next

    End Sub

    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub

    Private Sub TreeView1_TreeNodeExpanded(sender As Object, e As TreeNodeEventArgs) Handles TreeView1.TreeNodeExpanded

        e.Node.ChildNodes.RemoveAt(0)
        If e.Node.Value <> "" Then
            Dim BL As New ODAENG
            Dim Sqlchild As String = "SELECT * FROM TB_OU_Organize where Parent_ID=" & e.Node.Value
            Dim DT As New DataTable
            Dim DA As New SqlDataAdapter(Sqlchild, BL.ConnectionString)
            DA.Fill(DT)

            For j As Integer = 0 To DT.Rows.Count - 1
                Dim childNode As New TreeNode
                childNode.Text = DT(j)("Name_EN").ToString()
                childNode.Value = DT(j)("Node_ID").ToString()
                e.Node.ChildNodes.Add(childNode)
            Next
        End If
    End Sub
End Class
