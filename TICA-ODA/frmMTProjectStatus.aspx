﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeFile="frmMTProjectStatus.aspx.vb" Inherits="frmMTProjectStatus" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<title>Project Status | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Project Status (สถานะโครงการ)
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-area-chart"></i> Dashboard</a></li>
            <li class="active">Country Group</li>
          </ol>
        </section>
        <br />

           <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <p>
                
                      <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn bg-orange margin-r-5 btn-social" OnClick="btnAdd_Click">
                          <i class="fa fa-plus"></i>Add Project Status
                      </asp:LinkButton>
                  </p>
                                 <div class="col-md-8">
                          <h4 class="text-primary">
                         พบทั้งหมด : <asp:Label ID="lblCount" runat="server" Text=""></asp:Label> รายการ</h4> 
                    </div>
                             <div class="col-md-4">
                              <div class="col-xs-6 col-sm-10 ">
                             <asp:TextBox ID="txtSearch" runat="server" placeholder="ค้นหาจากชื่อและรายละเอียด" CssClass="form-control"></asp:TextBox>
                    </div>
                        
                         <div class="col-xs-6 col-sm-2">
                              <asp:LinkButton ID="btnSearch" runat="server" Text="" CssClass="fa fa-search">
                             
                        </asp:LinkButton>
                    </div>
                    </div>
                  
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr class="bg-gray">
                        <th>Project Status (สถานะโครงการ)</th>
                        <th style="width: 70px">Edit(แก้ไข)</th>
                        <th style="width: 50px">Delete(ลบ)</th>
                      </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptList"  runat="server" OnItemDataBound="rptList_ItemDataBound" OnItemCommand="rptList_ItemCommand">
						<ItemTemplate>        
                      <tr>
                        <td><asp:Label ID="lblName" runat="server" Text='<%#Eval("pstatus_name") %>'></asp:Label></td>
                        <td>
                         <asp:LinkButton ID="btnEdit" runat="server"  CommandArgument='<%# Eval("id") %>' 
                             CommandName="cmdEdit">
                             <i class="fa fa-edit text-success"></i> Edit

                         </asp:LinkButton></td>
                        <td>
                        <asp:LinkButton ID="btnDelete" runat="server" OnClientClick='javascript:return confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?")' 
                            CommandArgument='<%# Eval("id") %>' CommandName="cmdDelete"><i class="fa fa-trash text-danger"></i> Delete</asp:LinkButton>
                        </td>
                      </tr>
                       </ItemTemplate>
                          </asp:Repeater>
                    
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->

                 </ContentTemplate>
        </asp:UpdatePanel>
   </div>
    <%--<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">

     <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <script src="dist/js/app.min.js"></script>
    <script src="dist/js/demo.js"></script>--%>
    <script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
</asp:Content>
