﻿<%@ Page Language="vb" AutoEventWireup="false" CodeFile="frmLogin.aspx.vb" Inherits="frmLogin" %>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TICA ODA | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-page">
        <style>
        body {
            background: url(dist/img/bg.jpg) no-repeat center center fixed; 
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .login {
            width: 300px;
            height: auto;
        }
        </style>
      
    <div class="login-box">
      <div class="login-logo">
          <%--<img src="dist/img/header/login-2.png" class="login"/>--%>
          <%--<img src="dist/img/header/login-3.png" />--%>
          <%--<img src="dist/img/header/login-4.png" />--%>
          <img src="dist/img/header/login-5.png" />
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <h3 class="login-box-msg">Login</h3>
        <form id="from1" runat="server">
          <div class="form-group has-feedback">
              <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <%--<input type="email" class="form-control" placeholder="User name">--%>
            <asp:TextBox ID="txtUserName" runat="server" placeholder="User name" CssClass="form-control" MaxLength="50"></asp:TextBox>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <%--<input type="password" class="form-control" placeholder="Password">--%>
            <asp:TextBox ID="txtPassword" runat="server" placeholder="Password" CssClass="form-control" TextMode="Password" MaxLength="50"></asp:TextBox>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-6">
                <asp:Button ID="btnSignIn" runat="server" CssClass="btn btn-primary btn-block btn-flat" Text="Sign In" />
            </div><!-- /.col -->
            <div class="col-xs-6">
                <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-danger btn-block btn-flat" Text="Cancel" />
            </div><!-- /.col -->
            <%--<div class="col-xs-12">
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox">  Remember Me
                </label>
                <a href="#">I forgot my password</a><br>
              </div>
            </div><!-- /.col -->--%>
            
          </div>
        </form>

        


      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>

