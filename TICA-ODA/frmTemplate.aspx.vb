﻿Imports System.Data
Imports Constants
Partial Class frmTemplate
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    Public Property AllData As DataTable
        Get
            Try
                Return Session("TemplatePage")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("TemplatePage") = value
        End Set
    End Property

    Protected ReadOnly Property AuthorizeDT As DataTable
        Get
            Try
                Return CType(Session("Authorize"), DataTable)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuFinance")
        li.Attributes.Add("class", "active")

        Dim li2 As HtmlGenericControl = Me.Page.Master.FindControl("mnuMasterExpense")
        li2.Attributes.Add("class", "active")

        Dim a As HtmlAnchor = Me.Page.Master.FindControl("aTemplate")
        a.Attributes.Add("style", "color:#FF8000")

        If Not IsPostBack Then
            BindList()
            Authorize()
        End If

    End Sub

    Sub Authorize()
        If AuthorizeDT Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If
        Dim tmpdr() As DataRow = AuthorizeDT.Select("menu_id = '" & Menu.SubExpense & "' and isnull(is_save,'N') ='Y'")

        If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
            btnAdd.Visible = False
            HeadColDelete.Visible = False
        End If

        For i As Integer = 0 To rptList.Items.Count - 1
            Dim btnEdit As LinkButton = DirectCast(rptList.Items(i).FindControl("btnEdit"), LinkButton)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)

            If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
                btnEdit.Text = "View"
                btnEdit.CommandName = "View"
                btnEdit.CommandArgument = lblID.Text
            End If
        Next
    End Sub

    Private Sub BindList()
        Dim DT As DataTable = BL.GetList_Template_Name(0, txtSearch.Text)
        rptList.DataSource = DT
        rptList.DataBind()

        lblTotalList.Text = DT.Rows.Count

        AllData = DT
        Pager.SesssionSourceName = "TemplatePage"
        Pager.RenderLayout()
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)
        Dim lblTemplate As Label = DirectCast(e.Item.FindControl("lblTemplate"), Label)

        lblID.Text = e.Item.DataItem("id").ToString
        lblTemplate.Text = e.Item.DataItem("template_name").ToString
    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click, txtSearch.TextChanged
        BindList()
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Response.Redirect("frmEditTemplate.aspx")
    End Sub

    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        If e.CommandName = "Edit" Then

            Response.Redirect("frmEditTemplate.aspx?id=" & e.CommandArgument & "&mode=edit")
        ElseIf e.CommandName = "View" Then
            Response.Redirect("frmEditTemplate.aspx?id=" & e.CommandArgument & "&mode=view")
        ElseIf e.CommandName = "Delete" Then
            'Dim ret As ProcessReturnInfo = 
            If BL.DeleteTemplate(e.CommandArgument) Then
                BindList()
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('ไม่สามารถลบข้อมูลได้');", True)
            End If

        End If

    End Sub

End Class
