﻿Public Class frmLogin
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Session.RemoveAll()
        End If

    End Sub

    Private Sub btnSignIn_Click(sender As Object, e As EventArgs) Handles btnSignIn.Click
        If txtUserName.Text.Trim = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุ Username');document.getElementById('txtUserName').focus();", True)
            Exit Sub
        End If

        If txtPassword.Text.Trim = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุ Password');document.getElementById('txtPassword').focus();", True)
            Exit Sub
        End If


        Dim ret As New LoginReturnInfo
        ret = BL.Check_Login(txtUserName.Text, txtPassword.Text)
        If ret.IsSuccess Then
            Session("UserName") = ret.UserName '"Administrator"
            Session("FullNameTH") = ret.Name_TH
            Session("Authorize") = BL.GetList_Authorize(ret.Node_ID)
            Response.Redirect("frmProjectStatus.aspx")
            'Response.Redirect("frmSettingOUPerson.aspx")
        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');document.getElementById('txtUserName').focus();", True)
        End If

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        txtUserName.Text = ""
        txtPassword.Text = ""
    End Sub
End Class