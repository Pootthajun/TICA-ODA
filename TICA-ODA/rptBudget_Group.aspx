﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="rptBudget_Group.aspx.vb" Inherits="rptBudget_Group" %>
 

<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <title>Search | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
 
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>รายงานการจัดสรรงบประมาณ </h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>Report</a></li>
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>รายงานภายในองค์กร</a></li>
                <li class="active">รายงานการจัดสรรงบประมาณ</li>
            </ol>
        </section>
        <br />


        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white"  DefaultButton="btnSearch">
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                   

                                    <div class="box-header">
                                        <div class="col-sm-12" style="text-align: right;margin-bottom:10px;">
                                            <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                                <button type="button" class="btn btn-success btn-flat dropdown-toggle btn-print" data-toggle="dropdown">
                                                    <i class="fa fa-print"></i>
                                                </button>
                                                <ul class="dropdown-menu pull-right">
                                                    
                                                    <li><asp:LinkButton ID="btnPDF" runat="server"><i class="fa fa-file-pdf-o text-red"></i>รูปแบบ PDF</asp:LinkButton></li>
											        <li><asp:LinkButton ID="btnExcel" runat="server"><i class="fa fa-file-excel-o text-green"></i>รูปแบบ Excel</asp:LinkButton></li>	

                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row" style="/*margin-top:30px;*/"></div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">Year :</label>
                                                 <div class="col-sm-9">
                                                     <asp:DropDownList ID="ddlBudgetYear" runat="server" CssClass="form-control select2" Style="width: 50%">
                                                       
                                                     </asp:DropDownList>
                                                 </div>
                                             </div>
                                             <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">    งบประมาณ :</label>
                                                 <div class="col-sm-9">
                                                     <asp:DropDownList ID="ddlBudgetGroup" runat="server" CssClass="form-control select2" Style="width: 50%">
                                                     </asp:DropDownList>

                                                 </div>
                                             </div>
                                        </div>
                                        
                                     <div class="row" style="margin-top:30px;"></div>
                                      

                                        <div class="row" style="margin-top:10px;">
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                 <div class="col-sm-9 ">

                                                 </div>
                                                </div>
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                 <div class="col-sm-9 ">
                                                   <%--<p class="pull-right" style="margin-right:60px;">--%>
                                                     <p class="pull-right" style="margin-right:10px;">
                                                        <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn bg-blue margin-r-5 btn-social"   >
                                                            <i class="fa fa-search"></i>
                                                            <asp:Label ID="Search" runat="server" Text="Search"  ></asp:Label> 
                                                        </asp:LinkButton>
                                                    </p>

                                                 </div>
                                                </div>

                                        </div>



                                        <div class="row"></div>
                                        <div class="row pull-left">

                                            <div class="col-md-12">
                                            <h4 class="text-primary" style="margin-left :20px;"><asp:Label ID="lblTotalRecord" runat="server" Text=""></asp:Label></h4>


                                        </div>
                                        </div>
                                         <div class="row pull-right">
                                            <div class="col-md-12" style="margin-right: 30px;">

                                            <h5  style="margin-left :20px;">Unit : THB</h5>
                                        
                                        </div> 
                                    </div> 
                                        </div>   
                                    <div class="box-body">
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray">
                                                    
                                                    <th>รายการ</th>
                                                    <th>งบประมาณ</th>
                                                    <th>จัดสรรแล้ว</th>
                                                    <th>เบิกจ่าย</th>
                                                    <th>คงเหลือ</th> 
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr id="trgroup_name" runat="server" style ="background-color :ivory;">
                                                            <td data-title="Group name" colspan="5" style="text-align:  left ">
                                                             <b><asp:Label ID="lblgroup_name" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label></b>
                                                            </td>
                                                        </tr>
                                                        <tr>

                                                         
                                                            <td   data-title="รายการ"><asp:Label ID="lblsub_name" runat="server" ForeColor="black"></asp:Label></td>

                                                            <td   data-title="งบประมาณ" style="text-align :right ;"><asp:Label ID="lblAmount_Budget" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="จัดสรรแล้ว" style="text-align :right ;"><asp:Label ID="lblPay_Amount_Plan" runat="server"></asp:Label></td>
                                                            <td data-title="เบิกจ่าย" style="text-align :right  ; "><asp:Label ID="lblPay_Amount_Actual" runat="server"></asp:Label></td>
                                                            <td data-title="คงเหลือ" style="text-align :right;"><asp:Label ID="lblBalance" runat="server" ForeColor="black"></asp:Label></td>
                                                            
                                                        
                                                        </tr>

                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </tbody>
                                            <asp:Panel ID="pnlFooter" runat="server" Visible ="True">
                                            <tfoot style="background-color:LemonChiffon;" >
                                        <tr id="trFooter_Qty" runat="server" >

                                                            <td   data-title="Total" style="text-align :center ;"><b> Total </b></td>
                                                            <td   data-title="Count_Project" style="text-align :right;text-decoration: underline;"><b><asp:Label ID="lblCount_Project" runat="server" ForeColor="black"></asp:Label></b></td>
                                                            <td data-title="Count_Component_Bachelor" style="text-align :right ;text-decoration: underline;"><b><asp:Label ID="lblCount_Component_Bachelor" runat="server"></asp:Label></b></td>
                                                            <td data-title="Count_Component_Training" style="text-align :right  ;text-decoration: underline; "><b><asp:Label ID="lblCount_Component_Training" runat="server"></asp:Label></b></td>
                                                            <td data-title="Count_Component_Other" style="text-align :right;text-decoration: underline;"><b><asp:Label ID="lblCount_Component_Other" runat="server" ForeColor="black"></asp:Label></b></td>
                                                           
										</tr>
                                    

                                        </tfoot>
                                            </asp:Panel>
                                        </table>

                                        <div class="row"></div>
                                        
                                    </div>
                                    <!-- /.box-body -->
                                  


                                    <!-- Page Navigation -->
                                    <uc1:PageNavigation runat="server" ID="Pager" PageSize="20" />

                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->

                     <asp:TextBox ID="txtClickProjectID" runat="server" Text="" Style="display: none"></asp:TextBox>
                     <%--<asp:Button ID="btnView" OnClick="btnView_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnEdit" OnClick="btnEdit_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnDelete" OnClick="btnDelete_Click" runat="server" Style="display: none" />--%>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

   
</asp:Content>






