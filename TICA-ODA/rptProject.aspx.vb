﻿Imports Constants
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class rptProject
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    Public Property ProjectType As Long
        Get
            Try
                Return ViewState("ProjectType")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ProjectType") = value
        End Set
    End Property

    Public Property AllData As DataTable
        Get
            Try
                Return Session("rptProjectPage")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("rptProjectPage") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
        li.Attributes.Add("class", "active")
        Dim a As HtmlAnchor = Me.Page.Master.FindControl("arptProject")
        a.Attributes.Add("style", "color:#FF8000")

        If Not IsPostBack Then
            ProjectType = 0  ' เนื่องจากเฉพาะประเภทของ Project Type=0
            BindList()
            'Authorize()
        End If

    End Sub


    Public Function GetProjectList(project_type As Integer) As DataTable

        Dim dt As New DataTable

        Try
            Dim Sql As String = ""

            Sql &= " SELECT  ROW_NUMBER() OVER(ORDER BY project_Code,project_name ASC) As Seq , * FROM _vw_Project_Budget_Plan_Actual " & VbLf

            Sql &= "  WHERE 1=1  " & VbLf

            If (ddlProject_Type.SelectedValue > -1) Then
                Sql &= "   And project_type = '" & ddlProject_Type.SelectedValue & "'" & VbLf
                Title += " โครงการประเภท " & ddlProject_Type.SelectedItem.ToString()
            Else
                Title += " ประเภทโครงการทั้งหมด "

            End If

            If (txtSearch_Project.Text <> "") Then
                Sql &= "  AND project_Code Like '%" & txtSearch_Project.Text & "%' OR " & VbLf
                Sql &= "  project_name Like '%" & txtSearch_Project.Text & "%'   " & VbLf
                Title += " ของ " & txtSearch_Project.Text
            End If

            If (ck_Balance_Y.Checked = True And ck_Balance_N.Checked = False) Then
                Sql &= "  AND ISNULL(Balance,0)  > 0  " & VbLf
                Title += " ที่มีเงินคงเหลือ "

            ElseIf (ck_Balance_Y.Checked = False And ck_Balance_N.Checked = True) Then
                Sql &= "  AND ISNULL(Balance,0) <= 0   " & VbLf
                Title += " ที่ไม่มีเงินคงเหลือ "
            End If

            If (txtStartDate.Text <> "" And txtEndDate.Text <> "") Then
                Dim date1 As DateTime = DateTime.ParseExact(txtStartDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                Dim StartDate As String = date1.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

                Dim date2 As DateTime = DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                Dim EndDate As String = date2.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

                Sql &= " AND start_date  Between '" & StartDate & "' And '" & EndDate & "' " & VbLf
                Title += "ตั้งแต่วันที่" & txtStartDate.Text & "ถึงวันที่" & txtEndDate.Text

            End If

            Sql &= "  ORDER BY project_type ,end_date, project_name  " & VbLf

            dt = SqlDB.ExecuteTable(Sql)

            lblTotalRecord.Text = Title
            If dt.Rows.Count = 0 Then
                lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
            Else
                lblTotalRecord.Text &= " พบ " & FormatNumber(dt.Rows.Count, 0) & " โครงการ"
            End If

            Session("Search_Project_List_Title") = lblTotalRecord.Text

        Catch ex As Exception
        End Try
        Return dt

    End Function


    Private Sub BindList()


        Dim DT As DataTable = GetProjectList(ProjectType)

        If (DT.Rows.Count > 0) Then
            lblBudget_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Commitment_budget)", "")).ToString("#,##0.00")
            lblDisbursement_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Pay_Amount_Actual)", "")).ToString("#,##0.00")
            lblBalance_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Balance)", "")).ToString("#,##0.00")
            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If


        rptList.DataSource = DT
        rptList.DataBind()

        Session("Search_Project_List") = DT


        AllData = DT
        Pager.SesssionSourceName = "rptProjectPage"
        Pager.RenderLayout()
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblNo As Label = DirectCast(e.Item.FindControl("lblNo"), Label)
        Dim lblProjectID As Label = DirectCast(e.Item.FindControl("lblProjectID"), Label)
        Dim lblDescription As Label = DirectCast(e.Item.FindControl("lblDescription"), Label)
        Dim lblPlanDate As Label = DirectCast(e.Item.FindControl("lblPlanDate"), Label)
        Dim lblProject_Type As Label = DirectCast(e.Item.FindControl("lblProject_Type"), Label)
        Dim lblSUM_Budget As Label = DirectCast(e.Item.FindControl("lblSUM_Budget"), Label)
        Dim lblDisbursement As Label = DirectCast(e.Item.FindControl("lblDisbursement"), Label)
        Dim lblBalance As Label = DirectCast(e.Item.FindControl("lblBalance"), Label)

        lblNo.Text = e.Item.DataItem("seq").ToString
        lblProjectID.Text = e.Item.DataItem("project_Code").ToString
        lblDescription.Text = e.Item.DataItem("project_name").ToString
        lblPlanDate.Text = "Start/End Date:" & e.Item.DataItem("start_date_th").ToString & "-" & e.Item.DataItem("end_date_th").ToString

        If Convert.IsDBNull(e.Item.DataItem("Commitment_budget")) = False Then
            lblSUM_Budget.Text = Convert.ToDecimal(e.Item.DataItem("Commitment_budget")).ToString("#,##0.00")
        End If
        If Convert.IsDBNull(e.Item.DataItem("Pay_Amount_Actual")) = False Then
            lblDisbursement.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Actual")).ToString("#,##0.00")
        End If
        If Convert.IsDBNull(e.Item.DataItem("Balance")) = False Then
            lblBalance.Text = Convert.ToDecimal(e.Item.DataItem("Balance")).ToString("#,##0.00")
        End If

        '================Type===========================

        If (e.Item.DataItem("project_type") = Constants.Project_Type.Project) Then
            lblProject_Type.Text = Constants.Project_Type.Project.ToString()
        ElseIf (e.Item.DataItem("project_type") = Constants.Project_Type.NonProject) Then
            lblProject_Type.Text = Constants.Project_Type.NonProject.ToString()
        ElseIf (e.Item.DataItem("project_type") = Constants.Project_Type.Loan) Then
            lblProject_Type.Text = Constants.Project_Type.Loan.ToString()
        Else
            lblProject_Type.Text = Constants.Project_Type.Contribuition.ToString
        End If



    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindList()

    End Sub

#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptProject.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptProject.aspx?Mode=EXCEL');", True)
    End Sub

#End Region


End Class
