﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TREE.aspx.vb" Inherits="TREE" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>

<body>
   
    <form id="form1" runat="server">
         <asp:scriptmanager runat="server"></asp:scriptmanager>
         <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
    <div>

        <asp:TreeView  ID="TreeView1" runat="server" ShowExpandCollapse="true" RootNodeStyle-ForeColor="Black" NodeStyle-ForeColor="Blue"></asp:TreeView>

    </div>
         </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
