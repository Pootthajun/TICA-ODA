﻿
Imports Constants
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class rptCooperation_Year
    Inherits System.Web.UI.Page

    Dim BL As New ODAENG
    Dim GL As New GenericLib


    Private Sub rptCooperation_Year_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
            li.Attributes.Add("class", "active")
            Dim li_mnuReports_foreign As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports_Admin")
            li_mnuReports_foreign.Attributes.Add("class", "active")
            Dim a As HtmlAnchor = Me.Page.Master.FindControl("Admin_8_1")
            a.Attributes.Add("style", "color:#FF8000")


            BL.Bind_DDL_Componance(ddlComponance, True)
            BindList()
            btnSearch_Click(Nothing, Nothing)
        End If

    End Sub


    Public Function GetList() As DataTable

        Dim DT As New DataTable

        Dim filter As String = ""
        Dim Title As String = ""
        Try
            Dim sql As String = ""

            sql &= "   SELECT ROW_NUMBER() OVER(ORDER BY vw_Activity.project_id ASC) As No,vw_Activity.project_id																		 " & VbLf
            sql &= "         , vw_Activity.project_name 																	 " & VbLf
            sql &= "         , vw_Activity.project_type 																	 " & VbLf
            sql &= "   																									 " & VbLf
            sql &= "   	  ,funding_agency_id																			 " & VbLf
            sql &= "   	  ,vw_ou.name_th funding_agency_TH																 " & VbLf
            sql &= "   	  ,vw_ou.name_en funding_agency_EN																 " & VbLf
            sql &= "   																									 " & VbLf
            sql &= "   																									 " & VbLf
            sql &= "         ,vw_Activity.activity_id																		 " & VbLf
            sql &= "         ,[activity_name]																				 " & VbLf
            sql &= "   																									 " & VbLf
            sql &= "   	  --หลักสูตร																						 " & VbLf
            sql &= "         ,vw_Activity.sector_id																		 " & VbLf
            sql &= "         ,vw_Activity.sub_sector_id																	 " & VbLf
            sql &= "   	  ,TB_Purpose.name sub_sector_name																 " & VbLf
            sql &= "   	  ,vw_Activity_Component.component_id															 " & VbLf
            sql &= "   	  ,vw_Activity_Component.Desc_name  Component_Name												 " & VbLf
            sql &= "   	  ,commitment_budget																								 " & VbLf
            sql &= "     FROM  vw_Activity																					 " & VbLf
            sql &= "     INNER JOIN vw_Activity_Component  ON vw_Activity_Component.activity_id = vw_Activity.activity_id	 " & VbLf
            sql &= "     LEFT JOIN TB_Project ON TB_Project.id = vw_Activity.project_id									 " & VbLf
            sql &= "     Left JOIN vw_ou ON vw_ou.node_id=TB_Project.funding_agency_id										 " & VbLf
            sql &= "     LEFT JOIN TB_Purpose ON TB_Purpose.id=vw_Activity.sub_sector_id									 " & VbLf
            sql &= "   																									 " & VbLf
            sql &= "     where sub_sector_id IS NOT NULL																	 " & VbLf


            If (txtSearch_OU.Text <> "") Then

                filter &= "  vw_ou.name_th Like '%" & txtSearch_OU.Text & "%'  AND " & VbLf
                Title += " ประเทศ / แหล่งคู่ร่วมมือ : " & txtSearch_OU.Text
            End If

            If (txtSearch_Sub_Sector.Text <> "") Then

                filter &= "  TB_Purpose.name Like '%" & txtSearch_Sub_Sector.Text & "%'  AND " & VbLf
                Title += " ในหลักสูตร " & txtSearch_Sub_Sector.Text
            End If

            If (ddlComponance.SelectedIndex > 0) Then
                filter += "   vw_Activity_Component.component_id = " & ddlComponance.SelectedValue & "  AND " & VbLf
                Title += " ประเภท" & ddlComponance.SelectedItem.ToString()
            Else
            End If

            'If (txtSearch_Recipience.Text <> "") Then

            '    filter &= "  TB_Purpose.name Like '%" & txtSearch_Recipience.Text & "%'  AND " & VbLf
            '    Title += " ในหลักสูตร " & txtSearch_Recipience.Text
            'End If


            If filter <> "" Then
                sql += " AND " & filter.Substring(0, filter.Length - 6) & vbLf
            End If

            Dim DA As New SqlDataAdapter(sql, BL.ConnectionString)
            DA.Fill(DT)

            lblTotalRecord.Text = Title
            If DT.Rows.Count = 0 Then
                lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
            Else
                lblTotalRecord.Text &= " พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
            End If

            Session("Search_Cooperation_Year_Title") = lblTotalRecord.Text

            Lastfunding_agency = ""
            Sum_QTY = 0
        Catch ex As Exception
            Return Nothing
        End Try
        Return DT

    End Function



    Private Sub BindList()


        Dim DT As DataTable = GetList()


        If (DT.Rows.Count > 0) Then
            lblAmount_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(commitment_budget)", "")).ToString("#,##0.00")

            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If

        DT = DT_GetListDetail(DT)

        rptList.DataSource = DT
        rptList.DataBind()

        lblQTY_Sum.Text = Sum_QTY

        Session("Search_Cooperation_Year") = DT

        Pager.SesssionSourceName = "Search_Cooperation_Year"
        Pager.RenderLayout()
    End Sub


    Public Function DT_GetListDetail(dtList As DataTable) As DataTable
        If dtList IsNot Nothing Then



            Dim activity_id As String = ""
            Dim Detail_activity_id As String = ""
            Dim dt_ListDetail As New DataTable
            With dt_ListDetail
                .Columns.Add("activity_id")
                .Columns.Add("Recipience")
                .Columns.Add("Recipience_Qty")
            End With
            Dim dr As DataRow


            For i As Integer = 0 To dtList.Rows.Count - 1

                activity_id = dtList.Rows(i).Item("activity_id")


                Dim DT_Recipience As DataTable = GetRecipience_List(activity_id)
                Dim Recipience_List As String = ""
                Dim Recipience_List_Qty As String = ""

                Recipience_List = ""
                If (DT_Recipience.Rows.Count > 0) Then
                    For j As Integer = 0 To DT_Recipience.Rows.Count - 1
                        Recipience_List += DT_Recipience.Rows(j).Item("name_th_OU") + " " + "(" + DT_Recipience.Rows(j).Item("amout_person").ToString() + ")  , "
                    Next

                    If Recipience_List <> "" Then
                        Recipience_List = Recipience_List.Substring(0, Recipience_List.Length - 3) & vbLf
                    End If

                    Recipience_List_Qty = Convert.ToDecimal(DT_Recipience.Compute("SUM(amout_person)", "")).ToString()

                    dr = dt_ListDetail.NewRow
                    dr("activity_id") = activity_id
                    dr("Recipience") = Recipience_List
                    dr("Recipience_Qty") = Recipience_List_Qty
                    dt_ListDetail.Rows.Add(dr)

                End If



            Next

            dtList.Columns.Add("Recipience_Add")
            dtList.Columns.Add("Recipience_Qty_Add", Type.GetType("System.Double"))
            For i As Integer = 0 To dtList.Rows.Count - 1
                activity_id = dtList.Rows(i).Item("activity_id")
                dt_ListDetail.DefaultView.RowFilter = "activity_id = " + activity_id
                dtList.Rows(i)("Recipience_Add") = dt_ListDetail.DefaultView(0).Item("Recipience")
                dtList.Rows(i)("Recipience_Qty_Add") = If(Not String.IsNullOrEmpty(dt_ListDetail.DefaultView(0).Item("Recipience_Qty").ToString), Convert.ToDouble(dt_ListDetail.DefaultView(0).Item("Recipience_Qty")), 0)

            Next
        End If


        Return dtList

    End Function


    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Dim Lastbudget_year As String = ""
    Dim Lastfunding_agency As String = ""
    Dim Sum_QTY As Integer = 0

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If


        Dim lblfunding_agency_TH As Label = DirectCast(e.Item.FindControl("lblfunding_agency_TH"), Label)
        Dim lblSeq As Label = DirectCast(e.Item.FindControl("lblSeq"), Label)
        Dim lblsub_sector_name As Label = DirectCast(e.Item.FindControl("lblsub_sector_name"), Label)
        Dim lblComponent_Name As Label = DirectCast(e.Item.FindControl("lblComponent_Name"), Label)
        Dim lblRecipience_List As Label = DirectCast(e.Item.FindControl("lblRecipience_List"), Label)

        Dim lblQTY_Recipience As Label = DirectCast(e.Item.FindControl("lblQTY_Recipience"), Label)
        Dim lblAmount As Label = DirectCast(e.Item.FindControl("lblAmount"), Label)

        Dim DuplicatedStyleTop As String = "border-top:none;"
        Dim tdfunding_agency As HtmlTableCell = e.Item.FindControl("tdfunding_agency")

        '--------------------

        'lblfunding_agency_TH.Text = e.Item.DataItem("funding_agency_TH").ToString

        If Lastfunding_agency <> e.Item.DataItem("funding_agency_TH").ToString Then
            Lastfunding_agency = e.Item.DataItem("funding_agency_TH").ToString
            lblfunding_agency_TH.Text = Lastfunding_agency

        Else
            tdfunding_agency.Attributes("style") &= DuplicatedStyleTop
        End If


        lblSeq.Text = e.Item.ItemIndex + 1

        lblsub_sector_name.Text = e.Item.DataItem("sub_sector_name").ToString
        lblComponent_Name.Text = e.Item.DataItem("Component_Name").ToString

        Dim DT_Recipience As DataTable = GetRecipience_List(e.Item.DataItem("activity_id"))

        lblRecipience_List.Text = ""
        If (DT_Recipience.Rows.Count > 0) Then
            For i As Integer = 0 To DT_Recipience.Rows.Count - 1
                lblRecipience_List.Text += DT_Recipience.Rows(i).Item("name_th_OU") + " " + "(" + DT_Recipience.Rows(i).Item("amout_person").ToString() + ")  , "
            Next

            If lblRecipience_List.Text <> "" Then
                lblRecipience_List.Text = lblRecipience_List.Text.Substring(0, lblRecipience_List.Text.Length - 3) & vbLf
            End If

            lblQTY_Recipience.Text = Convert.ToDecimal(DT_Recipience.Compute("SUM(amout_person)", "")).ToString()
            Sum_QTY = Sum_QTY + GL.ConvertCINT(lblQTY_Recipience.Text)
        Else
            lblQTY_Recipience.Text = ""

        End If

        If Convert.IsDBNull(e.Item.DataItem("commitment_budget")) = False Then
            lblAmount.Text = Convert.ToDecimal(e.Item.DataItem("commitment_budget")).ToString("#,##0.00")
        End If

    End Sub



    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click

        BindList()

    End Sub


    Public Function GetRecipience_List(activity_id As Long) As DataTable

        Dim DT As New DataTable

        Dim filter As String = ""
        Dim Title As String = ""
        Try
            Dim sql As String = ""

            sql &= "   SELECT  " & VbLf
            sql &= "         node_id_OU " & VbLf
            sql &= "         ,name_th_OU " & VbLf
            sql &= "         ,SUM(amout_person) amout_person " & VbLf
            sql &= "     FROM vw_Recipient_In_Project " & VbLf
            sql &= "     where activity_id=" + activity_id.ToString + " " & VbLf
            sql &= "     GROUP BY node_id_OU,name_th_OU " & VbLf

            Dim DA As New SqlDataAdapter(sql, BL.ConnectionString)
            DA.Fill(DT)

        Catch ex As Exception
            Return Nothing
        End Try
        Return DT

    End Function


#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptCooperation_Year.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptCooperation_Year.aspx?Mode=EXCEL');", True)
    End Sub

#End Region



End Class

