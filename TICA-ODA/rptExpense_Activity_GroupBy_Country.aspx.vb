﻿Imports Constants
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class rptExpense_Activity_GroupBy_Country
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG
    Dim GL As New GenericLib
    Private Sub rptExpense_Activity_GroupBy_Country_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then

            Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
            li.Attributes.Add("class", "active")
            Dim li_mnuReports_foreign As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports_Admin")
            li_mnuReports_foreign.Attributes.Add("class", "active")
            Dim a As HtmlAnchor = Me.Page.Master.FindControl("Admin_04")
            a.Attributes.Add("style", "color:#FF8000")

            BL.Bind_DDL_Year(ddlProject_Year, False)
            BL.Bind_DDL_Country_By_Expense_Activity_GroupBy_CountryNew(ddlProject_Country, True)
            BindList()
        End If

    End Sub


    Public Function GetList() As DataTable

        Dim DT_Activity As New DataTable
        Dim DT_All_Projet As New DataTable
        Dim DT_List As New DataTable
        Dim filter As String = ""
        Dim Title As String = ""
        Dim Check As String = "True"
        Try
            Dim sql As String = ""
            sql &= " select A.project_id,A.activity_name,A.actual_start,A.actual_end,AB.activity_id,ar.name_th_OU,ar.node_id_OU,AB.budget_year " & VbLf
            sql &= " ,(Select [dbo].[GetShortThaiDate](A.actual_start)) start_date_th,(Select [dbo].[GetShortThaiDate](A.actual_end)) end_date_th " & VbLf
            sql &= " ,isnull((select sum(AEAD.Pay_Amount_Actual) from TB_Activity_Expense_Actual_Detail As AEAD join TB_Activity_Expense_Header As AEH On AEH.id = AEAD.Header_id where AEH.Activity_id = A.id),0) As Pay_Amount_Actual " & VbLf
            sql &= " ,isnull((select sum(AEPD.Pay_Amount_Plan) from TB_Activity_Expense_Plan_Detail As AEPD join TB_Activity_Expense_Header As AEH On AEH.id = AEPD.Header_id where AEH.Activity_id = A.id),0) As Pay_Amount_Plan " & VbLf
            sql &= " ,isnull((select sum(AEPD.Pay_Amount_Plan) from TB_Activity_Expense_Plan_Detail As AEPD join TB_Activity_Expense_Header As AEH On AEH.id = AEPD.Header_id where AEH.Activity_id = A.id),0) - isnull((select sum(AEAD.Pay_Amount_Actual) from TB_Activity_Expense_Actual_Detail As AEAD join TB_Activity_Expense_Header As AEH On AEH.id = AEAD.Header_id where AEH.Activity_id = A.id),0) As Balance,'Activity' As Type_Row " & VbLf
            sql &= " from TB_Activity As A join TB_Activity_Budget As AB On A.id = AB.activity_id inner join vw_Activity_Recipic_3Type As ar On ar.Act_id = A.id where 1=1 " & VbLf

            If (ddlProject_Year.SelectedIndex > -1) Then
                sql &= " and AB.budget_year = '" & ddlProject_Year.SelectedValue & "'" & VbLf
            End If

            If (ddlProject_Country.SelectedIndex > 0) Then
                sql &= " and ar.node_id_OU = '" & ddlProject_Country.SelectedValue & "'" & VbLf
            End If

            Dim DA As New SqlDataAdapter(sql, BL.ConnectionString)
            DA.Fill(DT_Activity)

            sql = ""
            sql &= " select P.id,P.project_name,'Project' As Type_Row from TB_Project As P where 1=1" & VbLf

            If (txtSearch_Project.Text <> "") Then
                sql &= " and P.project_name Like '%" & txtSearch_Project.Text & "%'   " & VbLf
            End If
            DA = New SqlDataAdapter(sql, BL.ConnectionString)
            DA.Fill(DT_All_Projet)

            Dim dt As New DataTable
            dt.Columns.Add("project_id")
            dt.Columns.Add("project_name")
            dt.Columns.Add("name_th_OU")
            dt.Columns.Add("node_id_OU")
            dt.Columns.Add("Type_Row")

            Dim DR As DataRow

            If (DT_Activity.Rows.Count > 0) Then
                If (DT_All_Projet.Rows.Count > 0) Then
                    For i As Integer = 0 To DT_Activity.Rows.Count - 1
                        For j As Integer = 0 To DT_All_Projet.Rows.Count - 1
                            If DT_All_Projet.Rows(j)("id") = DT_Activity.Rows(i)("project_id") Then
                                If (dt.Rows.Count > 0) Then
                                    For k As Integer = 0 To dt.Rows.Count - 1
                                        If DT_Activity.Rows(i)("project_id") = dt.Rows(k)("project_id") And DT_Activity.Rows(i)("node_id_OU") = dt.Rows(k)("node_id_OU") Then
                                            Check = "False"
                                            Exit For
                                        Else
                                            Check = "True"
                                        End If
                                    Next
                                End If
                                If Check = "True" Then
                                    DR = dt.NewRow()
                                    DR("project_id") = DT_All_Projet.Rows(j)("id")
                                    DR("project_name") = DT_All_Projet.Rows(j)("project_name")
                                    DR("name_th_OU") = DT_Activity.Rows(i)("name_th_OU")
                                    DR("node_id_OU") = DT_Activity.Rows(i)("node_id_OU")
                                    DR("Type_Row") = DT_All_Projet.Rows(j)("Type_Row")
                                    dt.Rows.Add(DR)
                                End If
                            End If
                        Next
                    Next
                End If
            End If

            Check = "True"

            DT_List.Columns.Add("budget_year")
            DT_List.Columns.Add("project_id")
            DT_List.Columns.Add("project_name")
            DT_List.Columns.Add("start_date_th")
            DT_List.Columns.Add("end_date_th")
            DT_List.Columns.Add("node_id_OU")
            DT_List.Columns.Add("name_th_OU")
            DT_List.Columns.Add("activity_name")
            DT_List.Columns.Add("Pay_Amount_Plan")
            DT_List.Columns.Add("Amount_Loan")
            DT_List.Columns.Add("Pay_Amount_Actual")
            DT_List.Columns.Add("Balance")
            DT_List.Columns.Add("Type_Row")

            Dim DRList As DataRow

            For i As Integer = 0 To dt.Rows.Count - 1
                DRList = DT_List.NewRow()
                DRList("budget_year") = ddlProject_Year.SelectedValue
                DRList("project_id") = dt.Rows(i)("project_id")
                DRList("project_name") = dt.Rows(i)("project_name")
                DRList("node_id_OU") = dt.Rows(i)("node_id_OU")
                DRList("name_th_OU") = dt.Rows(i)("name_th_OU")
                DRList("activity_name") = dt.Rows(i)("project_name")
                DRList("Pay_Amount_Plan") = 0
                DRList("Amount_Loan") = 0
                DRList("Pay_Amount_Actual") = 0
                DRList("Balance") = 0
                DRList("Type_Row") = dt.Rows(i)("Type_Row")
                DT_List.Rows.Add(DRList)
                For j As Integer = 0 To DT_Activity.Rows.Count - 1
                    If dt.Rows(i)("project_id") = DT_Activity.Rows(j)("project_id") And dt.Rows(i)("node_id_OU") = DT_Activity.Rows(j)("node_id_OU") Then
                        DRList = DT_List.NewRow()
                        DRList("budget_year") = ddlProject_Year.SelectedValue
                        DRList("project_id") = DT_Activity.Rows(j)("project_id")
                        DRList("project_name") = dt.Rows(i)("project_name")
                        DRList("start_date_th") = DT_Activity.Rows(j)("start_date_th")
                        DRList("end_date_th") = DT_Activity.Rows(j)("end_date_th")
                        DRList("node_id_OU") = DT_Activity.Rows(j)("node_id_OU")
                        DRList("name_th_OU") = DT_Activity.Rows(j)("name_th_OU")
                        DRList("activity_name") = DT_Activity.Rows(j)("Activity_name")
                        DRList("Pay_Amount_Plan") = DT_Activity.Rows(j)("Pay_Amount_Plan")
                        DRList("Amount_Loan") = 0
                        DRList("Pay_Amount_Actual") = DT_Activity.Rows(j)("Pay_Amount_Actual")
                        DRList("Balance") = DT_Activity.Rows(j)("Balance")
                        DRList("Type_Row") = DT_Activity.Rows(j)("Type_Row")
                        DT_List.Rows.Add(DRList)
                    End If
                Next
            Next

            lblTotalRecord.Text = Title
            If DT.Rows.Count = 0 Then
                lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
            Else
                lblTotalRecord.Text &= " พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
            End If


            Session("Search_Project_List_Title") = lblTotalRecord.Text


        Catch ex As Exception
            Return Nothing
        End Try
        Return DT_List

    End Function



    Private Sub BindList()

        Dim DT As DataTable = GetList()

        rptList.DataSource = DT
        rptList.DataBind()

        Session("Search_Project_List") = DT

        Pager.SesssionSourceName = "Search_Project_List"
        Pager.RenderLayout()
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Dim Lastbudget_year As String = ""

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblNo As Label = DirectCast(e.Item.FindControl("lblNo"), Label)
        Dim lblPeriod As Label = DirectCast(e.Item.FindControl("lblPeriod"), Label)
        Dim lblProject As Label = DirectCast(e.Item.FindControl("lblProject"), Label)
        Dim lblPay_Amount_Plan As Label = DirectCast(e.Item.FindControl("lblPay_Amount_Plan"), Label)
        Dim lblAmount_Loan As Label = DirectCast(e.Item.FindControl("lblAmount_Loan"), Label)
        Dim lblPay_Amount_Actual As Label = DirectCast(e.Item.FindControl("lblPay_Amount_Actual"), Label)
        Dim lblBalance As Label = DirectCast(e.Item.FindControl("lblBalance"), Label)

        Dim DuplicatedStyleTop As String = "border-top:none;"


        Dim trbudget_year As HtmlTableRow = e.Item.FindControl("trbudget_year")
        Dim lblbudget_year As Label = e.Item.FindControl("lblbudget_year")


        '--------Lastbudget_year------------
        If Lastbudget_year <> e.Item.DataItem("name_th_OU").ToString Then
            Lastbudget_year = e.Item.DataItem("name_th_OU").ToString
            lblbudget_year.Text = Lastbudget_year
            trbudget_year.Visible = True

        Else
            trbudget_year.Visible = False
        End If


        If Convert.IsDBNull(e.Item.DataItem("start_date_th")) = False Then
            lblPeriod.Text = e.Item.DataItem("start_date_th").ToString & "-" & e.Item.DataItem("end_date_th").ToString

        End If


        If (e.Item.DataItem("Type_Row").ToString) = "Project" Then
            lblProject.Text = e.Item.DataItem("activity_name").ToString


        Else
            lblProject.Text = "&nbsp;&nbsp;&nbsp;- " + e.Item.DataItem("activity_name").ToString

            If Convert.IsDBNull(e.Item.DataItem("Pay_Amount_Plan")) = False Then
                lblPay_Amount_Plan.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Plan")).ToString("#,##0.00")
            End If

            If Convert.IsDBNull(e.Item.DataItem("Amount_Loan")) = False Then
                    lblAmount_Loan.Text = Convert.ToDecimal(e.Item.DataItem("Amount_Loan")).ToString("#,##0.00")
                End If

                If Convert.IsDBNull(e.Item.DataItem("Pay_Amount_Actual")) = False Then
                    lblPay_Amount_Actual.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Actual")).ToString("#,##0.00")
                End If

                If Convert.IsDBNull(e.Item.DataItem("Balance")) = False Then
                    lblBalance.Text = Convert.ToDecimal(e.Item.DataItem("Balance")).ToString("#,##0.00")
                End If

            End If

    End Sub



    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click

        BindList()

    End Sub

#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptExpense_Activity_GroupBy_Country.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptExpense_Activity_GroupBy_Country.aspx?Mode=EXCEL');", True)
    End Sub

#End Region


End Class



