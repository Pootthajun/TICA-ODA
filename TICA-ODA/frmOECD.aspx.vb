﻿Imports System.Data
Imports LinqDB.ConnectDB
Imports Constants
Imports System.Data.SqlClient

Public Class frmOECD
    Inherits System.Web.UI.Page

    Public Property AllData As DataTable
        Get
            Try
                Return Session("OECDData")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("OECDData") = value
        End Set
    End Property

    Protected ReadOnly Property AuthorizeDT As DataTable
        Get
            Try
                Return CType(Session("Authorize"), DataTable)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
    End Property
    Dim TypeMode As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuMaster")
        li.Attributes.Add("class", "active")

        Dim a As HtmlAnchor = Me.Page.Master.FindControl("aOECD")
        a.Attributes.Add("style", "color:#FF8000")
        Dim TB_Authorize As DataTable = Session("Authorize")
        For i As Integer = 0 To TB_Authorize.Rows.Count - 1
            If Menu.OECD = TB_Authorize.Rows(i)("menu_id") Then
                If TB_Authorize.Rows(i)("is_save") = "Y" Then
                    TypeMode = "save"
                End If
                If TB_Authorize.Rows(i)("is_view") = "Y" Then
                    TypeMode = "view"
                    btnAdd.Visible = False
                End If
            End If
        Next

        If IsPostBack = False Then
            BindData()
            'Authorize()
            pnlAdSearch.Visible = False
        End If
    End Sub

    Sub Authorize()
        If AuthorizeDT Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If
        Dim tmpdr() As DataRow = AuthorizeDT.Select("menu_id = '" & Menu.OECD & "' and isnull(is_save,'N') ='Y'")

        If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
            btnAdd.Visible = False
            HeadColDelete.Visible = False
        End If

        For i As Integer = 0 To rptList.Items.Count - 1
            Dim btnEdit As LinkButton = DirectCast(rptList.Items(i).FindControl("btnEdit"), LinkButton)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)
            Dim ColDelete As HtmlTableCell = DirectCast(rptList.Items(i).FindControl("ColDelete"), HtmlTableCell)

            If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
                btnEdit.Text = "View"
                btnEdit.CommandName = "View"
                btnEdit.CommandArgument = lblID.Text
                ColDelete.Visible = False
            End If
        Next
    End Sub

    Protected Sub BindData()
        Dim sql As String = "SELECT id,oecd_name,oecd_name_th,detail,ACTIVE_STATUS FROM tb_oecd WHERE 1=1"
        If txtSearch.Text.Trim <> "" Then
            sql &= " and oecd_name LIKE '%" + txtSearch.Text.Trim() + "%' OR oecd_name_th LIKE '%" + txtSearch.Text.Trim() + "%'"
        End If

        If ddlStatus.SelectedValue <> "" Then
            sql &= " And ACTIVE_STATUS LIKE '%" & ddlStatus.SelectedValue & "%'"
        End If
        sql &= " order by oecd_name"

        Dim BL As New ODAENG
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(sql, BL.ConnectionString)
        DA.Fill(DT)
        rptList.DataSource = dt
        rptList.DataBind()
        lblCount.Text = dt.DefaultView.Count

        AllData = dt
        Pager.SesssionSourceName = "OECDData"
        Pager.RenderLayout()
        pnlAdSearch.Visible = False
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs)

        If e.CommandName = "cmdEdit" Then
            Response.Redirect("frmEditOECD.aspx?id=" & e.CommandArgument & "&mode=edit")
        ElseIf e.CommandName = "View" Then
            Response.Redirect("frmEditOECD.aspx?id=" & e.CommandArgument & "&mode=view")
        ElseIf e.CommandName = "cmdDelete" Then

            Dim BL As New ODAENG
            'Dim ret As ProcessReturnInfo
            'ret = BL.DeleteOECD(e.CommandArgument)
            If BL.DeleteOECD(e.CommandArgument) Then
                Alert("ลบข้อมูลเรียบร้อยแล้ว")
                BindData()
            Else
                Alert("ไม่สามารถลบข้อมูลได้")
            End If

        ElseIf e.CommandName = "cmdstatusready" Then
            Dim BL As New ODAENG
            'Dim ret As ProcessReturnInfo
            'ret =
            BL.EditStatusAllY(e.CommandArgument, "TB_OECD")
            'If ret.IsSuccess Then
            '    Alert("แก้ไขสถานะเรียบร้อยแล้ว")
            BindData()
            'Else
            '    Alert(ret.ErrorMessage)
            'End If
        ElseIf e.CommandName = "cmdstatusDisabled" Then
            Dim BL As New ODAENG
            '    Dim ret As ProcessReturnInfo
            'ret =
            BL.EditStatusAllN(e.CommandArgument, "TB_OECD")
            'If ret.IsSuccess Then
            '        Alert("แก้ไขสถานะเรียบร้อยแล้ว")
            BindData()
            'Else
            '    Alert(ret.ErrorMessage)
            'End If
        End If

    End Sub
    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)
        Dim lblName As Label = DirectCast(e.Item.FindControl("lblName"), Label)
        Dim Labelname_Th As Label = DirectCast(e.Item.FindControl("Labelname_Th"), Label)
        Dim lblDetail As Label = DirectCast(e.Item.FindControl("lblDetail"), Label)
        Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)
        Dim btnStatusReady As LinkButton = DirectCast(e.Item.FindControl("btnStatusReady"), LinkButton)
        Dim btnStatusDisabled As LinkButton = DirectCast(e.Item.FindControl("btnStatusDisabled"), LinkButton)
        Dim liview As HtmlGenericControl = e.Item.FindControl("liview")
        Dim liedit As HtmlGenericControl = e.Item.FindControl("liedit")
        Dim lidelete As HtmlGenericControl = e.Item.FindControl("lidelete")
        If TypeMode = "view" Then
            liview.Visible = True
            liedit.Visible = False
            lidelete.Visible = False
        ElseIf TypeMode = "Edit" Then
            liview.Visible = False
            liedit.Visible = True
            lidelete.Visible = True
        End If

        lblID.Text = e.Item.DataItem("id").ToString
        lblName.Text = drv("oecd_name").ToString()
        Labelname_Th.Text = drv("oecd_name_Th").ToString()
        lblDetail.Text = drv("detail").ToString()

        Dim A As String = drv("ACTIVE_STATUS").ToString
        If A = "Y" Then
            btnStatusReady.Visible = True
            btnStatusDisabled.Visible = False

        ElseIf A = "N" Then
            btnStatusDisabled.Visible = True
            btnStatusReady.Visible = False
        End If
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        Response.Redirect("frmEditOECD.aspx")
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs)
        BindData()
    End Sub
    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub
    Protected Sub btnSearch_Click1(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindData()
    End Sub

    Private Sub btnOpenSearch_Click(sender As Object, e As EventArgs) Handles btnOpenSearch.Click
        pnlAdSearch.Visible = True
    End Sub

    Private Sub btnCancle_Click(sender As Object, e As EventArgs) Handles btnCancle.Click
        pnlAdSearch.Visible = False
        clearFormSearch()
        BindData()
    End Sub

    Private Sub clearFormSearch()
        txtSearch.Text = ""
        ddlStatus.SelectedValue = ""
    End Sub
End Class