﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeFile="frmCountry.aspx.vb" Inherits="frmCountry" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<title>Country | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
   
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Country (ประเทศ)
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-area-chart"></i> Dashboard</a></li>
            <li class="active">Country</li>
          </ol>
        </section>
        <br />
        <!-- Main content -->
        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <p>
                                        <asp:LinkButton runat="server" ID="btnAdd" CssClass="btn bg-orange margin-r-5 btn-social" OnClick="btnAdd_Click">
                                            <i class="fa fa-plus"></i><small>Add Country</small>
                                        </asp:LinkButton>
                                    </p>
                                    <div class="col-md-8">
                                        <h4 class="text-primary">พบทั้งหมด :
                                            <asp:Label ID="lblCount" runat="server" Text=""></asp:Label>
                                            รายการ</h4>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="col-xs-6 col-sm-10 ">
                                            <asp:TextBox ID="txtSearch" runat="server" placeholder="ค้นหาจากชื่อและรายละเอียด" CssClass="form-control"></asp:TextBox>
                                        </div>

                                        <div class="col-xs-6 col-sm-2">
                                            <asp:LinkButton ID="btnSearch" runat="server" Text="" CssClass="fa fa-search">
                             
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white" Height="500px" ScrollBars="Auto">
                                    <div class="box-body">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th style="width: 120px">Country (ประเทศ)</th>
                                                    <th style="width: 250px">Country Group (กลุ่มประเทศ)</th>
                                                    <th>Type (ประเภท)</th>
                                                    <th>Region OECD (ภูมิภาค)</th>
                                                    <th style="width: 70px">Edit(แก้ไข)</th>
                                                    <th style="width: 50px">Delete(ลบ)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server" OnItemDataBound="rptList_ItemDataBound" OnItemCommand="rptList_ItemCommand">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblCountryName" runat="server" Text=' <%#Eval("country") %> '></asp:Label></td>
                                                            <td>
                                                                <asp:Label ID="lblCountryGroup" runat="server" Text=' <%#Eval("countrygroup_name") %> '></asp:Label></td>
                                                            <td>
                                                                <asp:Label ID="lblCountryType" runat="server" Text=' <%#Eval("CountryTypeName") %> '></asp:Label></td>
                                                            <td>
                                                                <asp:Label ID="lblRegionZoneODA" runat="server" Text=' <%#Eval("regionoda_name") %> '></asp:Label></td>
                                                            <td>
                                                                <asp:LinkButton ID="btnEdit" runat="server" OnClick="btnEdit_Click" CommandArgument='<%# Eval("id") %>'
                                                                    CommandName="cmdEdit">
                             <i class="fa fa-edit text-success"></i> Edit

                                                                </asp:LinkButton>
                                                            </td>
                                                            <td>
                                                                <asp:LinkButton ID="btnDelete" runat="server" OnClick="btnDelete_Click" OnClientClick='javascript:return confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?")'
                                                                    CommandArgument='<%# Eval("id") %>' CommandName="cmdDelete">
                                   <i class="fa fa-trash text-danger"></i> Delete</asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>


                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </asp:Panel>
                            </div>
                            <!-- /.box -->


                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->
            </ContentTemplate>
        </asp:UpdatePanel>
   </div>
    <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">

     <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <script src="dist/js/app.min.js"></script>
    <script src="dist/js/demo.js"></script>
    <script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
</asp:Content>

