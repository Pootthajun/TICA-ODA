﻿
Imports Constants
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class rptSummary_ByCountry
    Inherits System.Web.UI.Page

    Dim BL As New ODAENG
    Dim GL As New GenericLib


    Private Sub rptSummary_ByCountry_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
            li.Attributes.Add("class", "active")
            Dim li_mnuReports_foreign As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports_Admin")
            li_mnuReports_foreign.Attributes.Add("class", "active")
            Dim a As HtmlAnchor = Me.Page.Master.FindControl("Admin_8_7")
            a.Attributes.Add("style", "color:#FF8000")
            BL.Bind_DDL_Year(ddlBudgetYear, True)
            BL.Bind_DDL_Country_By_rptSummary_ByCountry(ddlCountry, True)
            BindList()
        End If

    End Sub


    Public Function GetList() As DataTable

        Dim DT As New DataTable

        Dim filter As String = ""
        Dim Title As String = " การให้ความช่วยเหลือแก่ประเทศ (สรุปรายประเทศ) "
        Try
            Dim sql As String = ""

            sql += " 	SELECT * FROM (																								  " & VbLf
            sql += " 	 SELECT TB_Detail_By_Project.budget_year,country_node_id,country_name_th									  " & VbLf
            sql += " 		   ,COUNT(project_id) QTY_Project						--จำนวนโปรเจคทั้ง ภายใต้และนอกโครงการ				     " & VbLf
            sql += " 		   ,vw_Recipient_In_Year.amout_person QTY_Recipient		--จำนวนทุน									     " & VbLf
            sql += " 		   ,SUM(Total_Amount) SUM_Amount						--มูลค่า											  " & VbLf
            sql += " 		FROM (																									  " & VbLf
            sql += " 			SELECT budget_year																					  " & VbLf
            sql += " 				  ,vw_Summary_Aid_By_Country.project_id															  " & VbLf
            sql += " 				  ,vw_Summary_Aid_By_Country.project_name														  " & VbLf
            sql += " 				  ,tb_project.project_type																		  " & VbLf
            sql += " 				  ,country_node_id																				  " & VbLf
            sql += " 				  ,country_name_th																				  " & VbLf
            sql += " 				  ,commitment_budget																			  " & VbLf
            sql += " 				  ,convert(decimal ,Bachelor)+convert(decimal,Training)+convert(decimal,Expert)+convert(decimal,Volunteer)+convert(decimal,Equipment)+convert(decimal,Other) Total_Amount	  " & VbLf
            sql += "																														  " & VbLf
            sql += " 			  FROM  vw_Summary_Aid_By_Country																			  " & VbLf
            sql += " 			  left JOIN tb_project ON tb_project.id=vw_Summary_Aid_By_Country.project_id								  " & VbLf
            sql += " 	 ) TB_Detail_By_Project																								  " & VbLf
            sql += " 	 LEFT JOIN vw_Recipient_In_Year ON  vw_Recipient_In_Year.budget_year = TB_Detail_By_Project.budget_year				  " & VbLf
            sql += " 										AND vw_Recipient_In_Year.node_id_OU = TB_Detail_By_Project.country_node_id		  " & VbLf
            sql += " 																														  " & VbLf
            sql += "  																														  " & VbLf
            sql += " 	 GROUP BY  TB_Detail_By_Project.budget_year,country_node_id,country_name_th ,vw_Recipient_In_Year.amout_person		    " & VbLf
            sql += " 	 ) AS TB																											    " & VbLf
            sql += " 	 WHERE SUM_Amount>0																									    " & VbLf


            If (ddlBudgetYear.SelectedIndex > 0) Then
                filter += "   budget_year = '" & ddlBudgetYear.SelectedValue & "'  AND " & VbLf
                Title += " ปีงบประมาณ " & ddlBudgetYear.SelectedValue
            Else

            End If

            If (ddlCountry.SelectedIndex > 0) Then
                filter += "   convert(bigint,country_node_id) = " & ddlCountry.SelectedValue & "  AND " & VbLf
                Title += " ของประเทศ " & ddlCountry.SelectedItem.ToString()
            Else
            End If


            If filter <> "" Then
                sql += " AND " & filter.Substring(0, filter.Length - 6) & vbLf
            End If

            Dim DA As New SqlDataAdapter(sql, BL.ConnectionString)
            DA.Fill(DT)

            lblTotalRecord.Text = Title
            If DT.Rows.Count = 0 Then
                lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
            Else
                lblTotalRecord.Text &= " พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
            End If

            Session("Search_Summary_ByCountry_Title") = lblTotalRecord.Text
        Catch ex As Exception
        End Try
        Return DT

    End Function



    Private Sub BindList()


        Dim DT As DataTable = GetList()


        If (DT.Rows.Count > 0) Then
            lblQTY_Project.Text = GL.ConvertCINT(DT.Compute("SUM(QTY_Project)", ""))
            lblQTY_Recipient.Text = GL.ConvertCINT(DT.Compute("SUM(QTY_Recipient)", ""))
            lblSUM_Amount.Text = Convert.ToDecimal(DT.Compute("SUM(SUM_Amount)", "")).ToString("#,##0.00")

            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If

        rptList.DataSource = DT
        rptList.DataBind()


        Session("Search_Summary_ByCountry") = DT

        Pager.SesssionSourceName = "Search_Summary_ByCountry"
        Pager.RenderLayout()
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Dim Lastbudget_year As String = ""

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim lblCountry As Label = DirectCast(e.Item.FindControl("lblCountry"), Label)
        Dim lblQTY_Project As Label = DirectCast(e.Item.FindControl("lblQTY_Project"), Label)
        Dim lblQTY_Recipient As Label = DirectCast(e.Item.FindControl("lblQTY_Recipient"), Label)
        Dim lblSUM_Amount As Label = DirectCast(e.Item.FindControl("lblSUM_Amount"), Label)

        Dim DuplicatedStyleTop As String = "border-top:none;"
        Dim trbudget_year As HtmlTableRow = e.Item.FindControl("trbudget_year")
        Dim lblbudget_year As Label = e.Item.FindControl("lblbudget_year")

        '--------Lastbudget_year------------
        If Lastbudget_year <> e.Item.DataItem("budget_year").ToString Then
            Lastbudget_year = e.Item.DataItem("budget_year").ToString
            lblbudget_year.Text = Lastbudget_year
            trbudget_year.Visible = True

        Else
            trbudget_year.Visible = False
        End If


        lblCountry.Text = e.Item.DataItem("country_name_th").ToString

        If Convert.IsDBNull(e.Item.DataItem("QTY_Project")) = False Then
            lblQTY_Project.Text = GL.ConvertCINT(e.Item.DataItem("QTY_Project"))
        End If

        If Convert.IsDBNull(e.Item.DataItem("QTY_Recipient")) = False Then
            lblQTY_Recipient.Text = GL.ConvertCINT(e.Item.DataItem("QTY_Recipient"))
        End If

        If Convert.IsDBNull(e.Item.DataItem("SUM_Amount")) = False Then
            lblSUM_Amount.Text = Convert.ToDecimal(e.Item.DataItem("SUM_Amount")).ToString("#,##0.00")
        End If

    End Sub



    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click

        BindList()

    End Sub


#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptSummary_ByCountry.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptSummary_ByCountry.aspx?Mode=EXCEL');", True)
    End Sub

#End Region





End Class

