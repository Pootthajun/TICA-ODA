﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="frmReports.aspx.vb" Inherits="frmReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <title>Organization Structure | ODA</title><title>Organization Structure | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Report (รายงาน)
            </h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>Master</a></li>
                <li class="active">Report</li>
            </ol>
        </section>
        <br />
        <!-- Main content -->
        <asp:updatepanel id="udpList" runat="server">
            <ContentTemplate>
                
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                      <div class="col-md-4">
                          <label for="inputLevel" class="col-sm-5 control-label">
                               เลือกรายงาน :</label>
                               <div class="col-sm-7">
                                 <asp:DropDownList ID="ddlSelectReport" runat="server" CssClass="form-control select2" Style="width: 100%;" AutoPostBack="True" onselectedindexchanged="pDropDown_SelectedIndexChanged">
                                     <asp:ListItem Value="1">แผน</asp:ListItem>
                                     <asp:ListItem Value="2">โครงการ</asp:ListItem>
                                     <asp:ListItem Value="3">งบประมาณ</asp:ListItem>
                                     <asp:ListItem Value="4">การชำระเงิน</asp:ListItem>
                                     </asp:DropDownList>
                                 </div>

                        </div>
                        <div class="col-md-7">
                              <div class="col-xs-6 col-sm-2 ">
                                    <asp:Label ID="LableSearch" runat="server" Text="ชื่อโปรเจค" ></asp:Label>
                              </div>
                              <div class="col-xs-6 col-sm-4 ">
                                               <asp:TextBox ID="txtSearch" runat="server" placeholder="ค้นหาจากชื่อและรายละเอียด" CssClass="form-control"></asp:TextBox>
                               </div>
                            <div class="col-xs-6 col-sm-2 ">   
                                    <asp:Label ID="Label1" runat="server" Text="ชื่อโปรเจค"></asp:Label>
                                </div>
                            <div class="col-xs-6 col-sm-4 ">   
                                  <asp:TextBox ID="TextBox1" runat="server" placeholder="" CssClass="form-control"></asp:TextBox>
                              </div>
                        </div>
                      
                        <div class="col-md-1">                            
                                  <asp:LinkButton ID="btnSearch" runat="server" Text="" CssClass="fa fa-search" OnClick="btnSearch_Click">                   
                                  </asp:LinkButton>
                            </div>
                    </div>
                     <!-- /.box-header -->
                                    <div class="box-body">
                                                <asp:Label ID="ShowData" runat="server" Text=""></asp:Label>
                                    </div>
                    </div>
                </div>
              </div>
            </section>
                
                  </ContentTemplate>
        </asp:updatepanel>
    </div>
    <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">

   <%-- <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <script src="dist/js/app.min.js"></script>
    <script src="dist/js/demo.js"></script>--%>
</asp:Content>

