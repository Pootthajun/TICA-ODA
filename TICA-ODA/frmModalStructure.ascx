﻿<%@ Control Language="vb" AutoEventWireup="false" CodeFile="frmModalStructure.ascx.vb" Inherits="frmChart" %>



<link rel="stylesheet" href="dist/modal/modal.css">
<%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>

<div class="container">
    <!-- Trigger the modal with a button -->
  

    <!-- ModalCountry -->

      


    <!-- ModalAgency -->
    <div class="modal fade" id="myModalAgency" role="dialog">
       
    </div>

    <!-- ModalPerson -->
    <div class="modal fade" id="myModalPerson" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="dropdown pull-right" data-toggle="tooltip" title="เครื่องมือ">
                        <a class="dropdown-toggle text-muted" data-toggle="dropdown" href="#">
                            <i class="fa fa-gear"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><i class="fa fa-edit text-blue"></i>แก้ไข</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><i class="fa fa-user text-blue"></i>กำหนดสิทธิ์</a></li>
                            <li role="presentation" class="divider"></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><i class="fa fa-trash text-red"></i>ลบ</a></li>
                        </ul>
                    </div>

                </div>
                <div class="modal-body">
                    <table class="table table-bordered">
                        <tr class="bg-info">
                            <td colspan="2">
                                <h5 class="box-title"><a href="#">
                                    <asp:Label ID="lblPersonCountryName" runat="server" Text=""></asp:Label>
                                </a>><a href="#">
                                    <asp:Label ID="lblPersonOrganizeName" runat="server" Text=""></asp:Label>
                                </a>>
                                    <asp:Label ID="lblPersonSubOrganizeName" runat="server" Text=""></asp:Label></h5>
                            </td>
                        </tr>
                        <tr>
                            <th style="width: 180px"><b class="pull-right">ชื่อภาษาไทย :</b></th>
                            <td>
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>

                                        <asp:TextBox ID="txtPersonNameTH" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th style="width: 180px"><b class="pull-right">Name-Lastname :</b></th>
                            <td>
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <asp:TextBox ID="txtPersonNameEN" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th style="width: 180px"><b class="pull-right">Id Card/Passport :</b></th>
                            <td>
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-credit-card"></i>
                                        </div>
                                        <asp:TextBox ID="txtPersonIDCardPassport" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th style="width: 180px"><b class="pull-right">Birthday (วันเกิด) :</b></th>
                            <td>
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <%--<input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>--%>
                                        <asp:TextBox ID="txtPersonBirthDay" CssClass="form-control" runat="server" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask></asp:TextBox>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th style="width: 180px"><b class="pull-right">Telephone (โทรศัพท์) :</b></th>
                            <td>
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <asp:TextBox ID="txtPersonTelephone" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th style="width: 180px"><b class="pull-right">E-mail :</b></th>
                            <td>
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-envelope-o"></i>
                                        </div>
                                        <asp:TextBox ID="txtPersonEmail" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th style="width: 180px"><b class="pull-right">Note (หมายเหตุ) :</b></th>
                            <td>
                                <div class="col-sm-12">

                                    <asp:TextBox ID="txtPersonNote" runat="server" TextMode="MultiLine" Width="350px" CssClass="form-control"></asp:TextBox>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th style="width: 180px"><b class="pull-right">Active Status :</b></th>
                            <td>
                                <div class="col-sm-12">
                                    <label>
                                        <input type="checkbox" class="minimal" checked></label>
                                    <asp:CheckBox ID="chkACTIVE_STATUS_Person" runat="server" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">

                    <asp:LinkButton ID="btnPersonSave" runat="server" CssClass="btn  btn-social btn-success">
                            <i class="fa fa-save"></i> Save
                    </asp:LinkButton>
                    <asp:LinkButton ID="btnPersonCancel" runat="server" CssClass="btn  btn-social btn-google">
                            <i class="fa fa-reply"></i> Cancel
                    </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</div>

