﻿
Imports Constants
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class rptBudget_Group_CooFramework
    Inherits System.Web.UI.Page

    Dim BL As New ODAENG
    Dim GL As New GenericLib


    Private Sub rptSummary_ByProject_In_Period_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
            li.Attributes.Add("class", "active")
            Dim li_mnuReports_foreign As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports_Admin")
            li_mnuReports_foreign.Attributes.Add("class", "active")
            Dim a As HtmlAnchor = Me.Page.Master.FindControl("Admin_03_2")
            a.Attributes.Add("style", "color:#FF8000")
            BL.Bind_DDL_Year(ddlBudgetYear, False)
            BL.Bind_DDL_BudgetGroup(ddlBudgetGroup)
            BindList()
        End If

    End Sub


    Public Function GetList() As DataTable

        Dim DT As New DataTable

        Dim filter As String = ""
        Dim Title As String = " รายงานการจัดสรรงบประมาณแบ่งตามกรอบความร่วมมือ "
        Try
            Dim sql As String = ""
            sql += "  -- รายงานการจัดสรรงงประมาณ กลุ่มงบปรมาณ  แตกรายละเอียดกรอบความร่วมมือ																			" & VbLf
            sql += "  SELECT  																															" & VbLf
            sql += "  budget_year																														" & VbLf
            sql += "  --กรอบคึวามร่วมมือ--																													 " & VbLf
            sql += "  	,cooperation_framework_id 																										" & VbLf
            sql += "  	,fwork_name 																													" & VbLf
            sql += "  ---Group Budget -----																												" & VbLf
            sql += "  	,budget_group_id																												" & VbLf
            sql += "  	,group_name																														" & VbLf
            sql += "  	,budget_sub_id																													" & VbLf
            sql += "  	,sub_name																														" & VbLf
            sql += "  --, Amount_Budget																													" & VbLf
            sql += "  ,ISNULL(SUM(Amount_Budget),0.00) Amount_Budget																					" & VbLf
            sql += "  ,  ISNULL(SUM(Pay_Amount_Plan),0.00) Pay_Amount_Plan , ISNULL(SUM(Pay_Amount_Actual),00) Pay_Amount_Actual						" & VbLf
            sql += "  ,(ISNULL(SUM(Pay_Amount_Plan),0.00)-ISNULL(SUM(Pay_Amount_Actual),0.00))  Balance													" & VbLf
            sql += "  FROM (																															" & VbLf
            sql += "  	select TB_Activity_Budget.budget_year, TB_Project.id project_id,TB_Project.project_name , TB_Activity.id Activity_id			" & VbLf
            sql += "  	,TB_Activity.activity_name 																										" & VbLf
            sql += "  	, TB_Activity_Expense_Detail.* 																									" & VbLf
            sql += "  	,TB_Activity_Budget.amount Amount_Budget																						" & VbLf
            sql += "  	--กรอบคึวามร่วมมือ--																												 " & VbLf
            sql += "  	,TB_Project.cooperation_framework_id 																							" & VbLf
            sql += "  	,TB_CoperationFramework.fwork_name 																								" & VbLf
            sql += "  	---Group Budget -----																											" & VbLf
            sql += "  	,TB_Budget_Sub.budget_group_id																									" & VbLf
            sql += "  	,TB_Budget_Group.group_name																										" & VbLf
            sql += "  	,TB_Activity_Budget.budget_sub_id																								" & VbLf
            sql += "  	,TB_Budget_Sub.sub_name																											" & VbLf
            sql += "  	from TB_Activity																												" & VbLf
            sql += "  	LEFT JOIN TB_Activity_Budget ON TB_Activity_Budget.activity_id=TB_Activity.id													" & VbLf
            sql += "  	LEFT JOIN TB_Activity_Expense_Header ON TB_Activity_Expense_Header.activity_id=TB_Activity.id									" & VbLf
            sql += "  	LEFT JOIN TB_Activity_Expense_Detail ON TB_Activity_Expense_Detail.header_id = TB_Activity_Expense_Header.id					" & VbLf
            sql += "  	LEFT JOIN TB_Project ON TB_Project.id= TB_Activity.project_id																	" & VbLf
            sql += "  	LEFT JOIN TB_Budget_Sub ON TB_Budget_Sub.id=TB_Activity_Budget.budget_sub_id													" & VbLf
            sql += "  	LEFT JOIN TB_Budget_Group ON TB_Budget_Group.id=TB_Budget_Sub.budget_group_id													" & VbLf
            sql += "  	--กรอบคึวามร่วมมือ--																												 " & VbLf
            sql += "  	LEFT JOIN TB_CoperationFramework ON TB_CoperationFramework.id =TB_Project .cooperation_framework_id								" & VbLf
            sql += "  																																	" & VbLf
            sql += "  )AS TB																															" & VbLf
            sql += "  WHERE budget_year IS NOT NULL AND cooperation_framework_id IS NOT NULL															" & VbLf

            If (ddlBudgetYear.SelectedIndex > -1) Then
                filter += "   budget_year = '" & ddlBudgetYear.SelectedValue & "'  AND " & VbLf
                Title += " ปีงบประมาณ " & ddlBudgetYear.SelectedValue
            Else

            End If
            If (ddlBudgetGroup.SelectedIndex > 0) Then
                filter += "   budget_group_id = '" & ddlBudgetGroup.SelectedValue & "'  AND " & VbLf
                Title += "กลุ่ม " & ddlBudgetGroup.SelectedValue
            Else

            End If
            If filter <> "" Then
                sql += " AND " & filter.Substring(0, filter.Length - 6) & vbLf
            End If

            sql += "  GROUP BY budget_year																												" & VbLf
            sql += "  		---Group Budget -----																										" & VbLf
            sql += "  		,budget_group_id																											" & VbLf
            sql += "  		,group_name																													" & VbLf
            sql += "  		,budget_sub_id																												" & VbLf
            sql += "  		,sub_name																													" & VbLf
            sql += "  																																	" & VbLf
            sql += "  		--กรอบคึวามร่วมมือ--																											 " & VbLf
            sql += "  		,cooperation_framework_id 																									" & VbLf
            sql += "  		,fwork_name 																												" & VbLf
            sql += "  ORDER BY budget_year DESC,budget_group_id,budget_sub_id ,fwork_name																" & VbLf

            Dim DA As New SqlDataAdapter(sql, BL.ConnectionString)
            DA.Fill(DT)

            lblTotalRecord.Text = Title
            If DT.Rows.Count = 0 Then
                lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
            Else
                lblTotalRecord.Text &= " พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
            End If

            Session("Search_Budget_Group_CooFramework_Title") = lblTotalRecord.Text
        Catch ex As Exception
            Return Nothing
        End Try
        Return DT

    End Function



    Private Sub BindList()


        Dim DT As DataTable = GetList()


        If (DT.Rows.Count > 0) Then
            'lblCount_Project.Text = GL.ConvertCINT(DT.Compute("SUM(Count_Project)", ""))
            'lblCount_Component_Bachelor.Text = GL.ConvertCINT(DT.Compute("SUM(Count_Component_Bachelor)", ""))
            'lblCount_Component_Training.Text = GL.ConvertCINT(DT.Compute("SUM(Count_Component_Training)", ""))
            'lblCount_Component_Other.Text = GL.ConvertCINT(DT.Compute("SUM(Count_Component_Other)", ""))

            'lblSUM_Amount.Text = Convert.ToDecimal(DT.Compute("SUM(commitment_budget)", "")).ToString("#,##0.00")

            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If

        rptList.DataSource = DT
        rptList.DataBind()


        Session("Search_Budget_Group_CooFramework") = DT

        Pager.SesssionSourceName = "Search_Budget_Group_CooFramework"
        Pager.RenderLayout()
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Dim Lastsub_name As String = ""

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim lblsub_name As Label = DirectCast(e.Item.FindControl("lblsub_name"), Label)

        Dim lblAmount_Budget As Label = DirectCast(e.Item.FindControl("lblAmount_Budget"), Label)
        Dim lblPay_Amount_Plan As Label = DirectCast(e.Item.FindControl("lblPay_Amount_Plan"), Label)
        Dim lblPay_Amount_Actual As Label = DirectCast(e.Item.FindControl("lblPay_Amount_Actual"), Label)
        Dim lblBalance As Label = DirectCast(e.Item.FindControl("lblBalance"), Label)

        Dim DuplicatedStyleTop As String = "border-top:none;"
        Dim trgroup_name As HtmlTableRow = e.Item.FindControl("trgroup_name")
        Dim lblgroup_name As Label = e.Item.FindControl("lblgroup_name")

        '--------Lastbudget_year------------
        If Lastsub_name <> e.Item.DataItem("group_name").ToString & e.Item.DataItem("sub_name").ToString Then
            Lastsub_name = e.Item.DataItem("group_name").ToString & e.Item.DataItem("sub_name").ToString
            lblgroup_name.Text = e.Item.DataItem("group_name").ToString & "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & e.Item.DataItem("sub_name").ToString
            trgroup_name.Visible = True

        Else
            trgroup_name.Visible = False
        End If

        lblsub_name.Text = e.Item.ItemIndex + 1 & ". " & e.Item.DataItem("fwork_name").ToString

        If Convert.IsDBNull(e.Item.DataItem("Amount_Budget")) = False Then
            lblAmount_Budget.Text = Convert.ToDecimal(e.Item.DataItem("Amount_Budget")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Pay_Amount_Plan")) = False Then
            lblPay_Amount_Plan.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Plan")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Pay_Amount_Actual")) = False Then
            lblPay_Amount_Actual.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Actual")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Balance")) = False Then
            lblBalance.Text = Convert.ToDecimal(e.Item.DataItem("Balance")).ToString("#,##0.00")
        End If


    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click

        BindList()

    End Sub


#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptSummary_ByProject_In_Period.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptSummary_ByProject_In_Period.aspx?Mode=EXCEL');", True)
    End Sub

#End Region



End Class


