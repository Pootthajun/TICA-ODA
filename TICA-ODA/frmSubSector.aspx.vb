﻿Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data
Imports Constants
Imports System.Data.SqlClient

Public Class frmSubSector
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG
    Public Property AllData As DataTable
        Get
            Try
                Return Session("SubSectorData")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("SubSectorData") = value
        End Set
    End Property

    Protected ReadOnly Property AuthorizeDT As DataTable
        Get
            Try
                Return CType(Session("Authorize"), DataTable)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
    End Property
    Dim TypeMode As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuMaster")
        li.Attributes.Add("class", "active")

        Dim a As HtmlAnchor = Me.Page.Master.FindControl("aSubSector")
        a.Attributes.Add("style", "color:#FF8000")
        Dim TB_Authorize As DataTable = Session("Authorize")
        For i As Integer = 0 To TB_Authorize.Rows.Count - 1
            If Menu.SubSector = TB_Authorize.Rows(i)("menu_id") Then
                If TB_Authorize.Rows(i)("is_save") = "Y" Then
                    TypeMode = "save"
                End If
                If TB_Authorize.Rows(i)("is_view") = "Y" Then
                    TypeMode = "view"
                    btnAdd.Visible = False
                End If
            End If
        Next

        If IsPostBack = False Then
            BindData()
            'Authorize()
            BindPerposcat()
            BL.SetTextIntKeypress(TxtCRS)
            BL.SetTextIntKeypress(TxtDAC)
        End If
    End Sub

    Protected Sub BindPerposcat()
        Dim sql As String = "SELECT id,perposecat_name FROM tb_purposecat"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(sql, BL.ConnectionString)
        DA.Fill(DT)
        BL.BindData_dll(ddlPurposeCategory, "", DT, "Select Purpose Category", "perposecat_name")
    End Sub

    Sub Authorize()
        If AuthorizeDT Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If
        Dim tmpdr() As DataRow = AuthorizeDT.Select("menu_id = '" & Menu.SubSector & "' and isnull(is_save,'N') ='Y'")

        If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
            btnAdd.Visible = False
            HeadColDelete.Visible = False
        End If

        For i As Integer = 0 To rptList.Items.Count - 1
            Dim btnEdit As LinkButton = DirectCast(rptList.Items(i).FindControl("btnEdit"), LinkButton)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)
            Dim ColDelete As HtmlTableCell = DirectCast(rptList.Items(i).FindControl("ColDelete"), HtmlTableCell)

            If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
                btnEdit.Text = "View"
                btnEdit.CommandName = "View"
                btnEdit.CommandArgument = lblID.Text
                ColDelete.Visible = False
            End If
        Next
    End Sub

    Protected Sub BindData()
        Dim sql As String = "SELECT  tb_purpose.id,Name,CategoryID,tb_purposecat.perposecat_name,DAC5code,CRScode,tb_purpose.ACTIVE_STATUS" & vbLf
        sql += " From tb_purpose" & vbLf
        sql += " Left JOIN tb_purposecat ON tb_purposecat.id=tb_purpose.CategoryID WHERE 1=1 " & vbLf

        If txtSearch.Text.Trim <> "" Then
            sql &= " and Name LIKE '%" + txtSearch.Text.Trim().Replace("'", "''") + "%'" & vbLf
        End If

        If ddlPurposeCategory.SelectedValue <> "" Then
            sql &= " and tb_purpose.CategoryID =  '" + ddlPurposeCategory.SelectedValue + "' " & vbLf
        End If

        If TxtDAC.Text.Trim <> "" Then
            sql &= " and DAC5code LIKE '%" + TxtDAC.Text.Trim().Replace("'", "''") + "%'" & vbLf
        End If

        If TxtCRS.Text.Trim <> "" Then
            sql &= " and CRScode LIKE '%" + TxtCRS.Text.Trim().Replace("'", "''") + "%'" & vbLf
        End If

        sql &= " order by Name"

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(sql, BL.ConnectionString)
        DA.Fill(DT)

        rptList.DataSource = dt
        rptList.DataBind()
        lblCount.Text = dt.DefaultView.Count

        AllData = dt
        Pager.SesssionSourceName = "SubSectorData"
        Pager.RenderLayout()
        pnlAdSearch.Visible = False
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        Response.Redirect("frmEditSubSector.aspx")
    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindData()
    End Sub
    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)
        Dim lblName As Label = DirectCast(e.Item.FindControl("lblName"), Label)
        Dim lblCategory As Label = DirectCast(e.Item.FindControl("lblCategory"), Label)
        Dim lblDAC5code As Label = DirectCast(e.Item.FindControl("lblDAC5code"), Label)
        Dim lblCRScode As Label = DirectCast(e.Item.FindControl("lblCRScode"), Label)
        Dim btnStatusReady As LinkButton = DirectCast(e.Item.FindControl("btnStatusReady"), LinkButton)
        Dim btnStatusDisabled As LinkButton = DirectCast(e.Item.FindControl("btnStatusDisabled"), LinkButton)
        Dim liview As HtmlGenericControl = e.Item.FindControl("liview")
        Dim liedit As HtmlGenericControl = e.Item.FindControl("liedit")
        Dim lidelete As HtmlGenericControl = e.Item.FindControl("lidelete")
        If TypeMode = "view" Then
            liview.Visible = True
            liedit.Visible = False
            lidelete.Visible = False
        ElseIf TypeMode = "Edit" Then
            liview.Visible = False
            liedit.Visible = True
            lidelete.Visible = True
        End If

        Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)
        lblID.Text = e.Item.DataItem("id").ToString
        lblName.Text = drv("Name").ToString()
        lblCategory.Text = drv("perposecat_name").ToString()
        lblDAC5code.Text = drv("DAC5code").ToString()
        lblCRScode.Text = drv("CRScode").ToString()

        Dim A As String = drv("ACTIVE_STATUS").ToString
        If A = "Y" Then
            btnStatusReady.Visible = True
            btnStatusDisabled.Visible = False
        ElseIf A = "N" Then
            btnStatusDisabled.Visible = True
            btnStatusReady.Visible = False
        End If

    End Sub
    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs)
        If e.CommandName = "cmdEdit" Then
            Response.Redirect("frmEditSubSector.aspx?id=" & e.CommandArgument & "&mode=edit")
        ElseIf e.CommandName = "View" Then
            Response.Redirect("frmEditSubSector.aspx?id=" & e.CommandArgument & "&mode=view")
        ElseIf e.CommandName = "cmdDelete" Then
            Dim BL As New ODAENG
            'Dim ret As ProcessReturnInfo
            'ret = BL.DeleteSubSector(e.CommandArgument)
            If BL.DeleteSubSector(e.CommandArgument) Then
                Alert("ลบข้อมูลเรียบร้อยแล้ว")
                BindData()
            Else
                Alert("ไม่สามารถลบข้อมูลได้")
            End If
        ElseIf e.CommandName = "cmdstatusready" Then
            Dim BL As New ODAENG
            'Dim ret As ProcessReturnInfo
            'ret =
            BL.EditStatusAllY(e.CommandArgument, "TB_Purpose")
            'If ret.IsSuccess Then
            '    Alert("แก้ไขสถานะเรียบร้อยแล้ว")
            BindData()
            'Else
            '    Alert(ret.ErrorMessage)
            'End If
        ElseIf e.CommandName = "cmdstatusDisabled" Then
            Dim BL As New ODAENG
            'Dim ret As ProcessReturnInfo
            'ret =
            BL.EditStatusAllN(e.CommandArgument, "TB_Purpose")
            'If ret.IsSuccess Then
            '    Alert("แก้ไขสถานะเรียบร้อยแล้ว")
            BindData()
            'Else
            '    Alert(ret.ErrorMessage)
            'End If
        End If

    End Sub
    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub

    Private Sub btnOpenSearch_Click(sender As Object, e As EventArgs) Handles btnOpenSearch.Click
        pnlAdSearch.Visible = True
    End Sub

    Private Sub btnCancle_Click(sender As Object, e As EventArgs) Handles btnCancle.Click
        pnlAdSearch.Visible = False
        ClearEditForm()
        BindData()
    End Sub

    Private Sub ClearEditForm()
        txtSearch.Text = ""
        ddlPurposeCategory.SelectedValue = ""
        TxtCRS.Text = ""
        TxtDAC.Text = ""
    End Sub
End Class