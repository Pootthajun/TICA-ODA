﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeFile="frmEditUserLevel.aspx.vb" Inherits="frmEditUserLevel" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>
<%@ Register Src="~/usercontrol/UCFormAuthorize.ascx" TagPrefix="uc1" TagName="UCFormAuthorize" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Add User Level | ODA</title>

    <style>    
        .box {
            border: 0 !important; 
        }

        .trHead {            
            height: 42px;
            text-align: left;
            font-weight: bold;
            background-color: #ebedf0;
        }

        input[type=radio] {
            visibility: hidden;
            position: relative;
            margin-left: 5px;
            width: 20px;
            height: 20px;
            cursor:pointer;
        }
        input[type=radio]:before {
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            content: "";
            visibility: visible;
            position: absolute;
            border: 2px solid #09a1d8;
            border-radius: 50%;            
        }
        input[type=radio]:checked:before {            
            background-color: #44cbfc ;
        }

    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Authorization (กำหนดสิทธิ์การใช้งาน)
          </h1>
          <ol class="breadcrumb">
                <li class="active"><a href="frmStructure.aspx"><i class="fa fa-users"></i> Organization Structure</a></li>          
                <li class="active">User Level</li>
          </ol>
        </section>
        <uc1:UCFormAuthorize runat="server" ID="UCFormAuthorize" />


        <div class="box-footer">
            <div class="col-sm-9"></div>
            <div class="col-lg-8"></div>
            <div class="col-lg-2">
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-block btn-social btn-success"><i class="fa fa-save"></i>Save</asp:LinkButton>
            </div>
            <div class="col-lg-2">
                <asp:LinkButton ID="btnCancle" runat="server" CssClass="btn btn-block btn-social btn-google">
                    <i class="fa fa-reply"></i> Cancel   
                </asp:LinkButton>
            </div>
        </div>
   </div>
  
</asp:Content>
