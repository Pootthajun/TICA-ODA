﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="frmTestDefault.aspx.vb" Inherits="frmTestDefault" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <title>Test Default | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    
    <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Project
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-area-chart"></i>Dashboard</a></li>
                <li><a href="#">Project Status</a></li>
                <li class="active">Project</li>
            </ol>
        </section>
        <br />
    <div class="content-wrapper">
        
        <!-- Don't use -->
        <%--
            <table >
            <tr>
                <td>
                    <a href="#" onclick="showMenu('menu');" >Link1</a>
                    <div id="menu" style="display:none;position:absolute;background-color:white;border:solid 1px; " onmouseout="hideMenu('menu')"  >
                        <ul  >
                          <li onmouseover="showMenu('menu');" style="cursor:pointer;">
                              <a href="http://www.google.com"> Menu1</a>
                          </li>
                          <li onmouseover="showMenu('menu');" style="cursor:pointer;">
                              <a href="http://www.facebook.com">Menu2</a>
                          </li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td><a href="#" >Link2</a><br /></td>
            </tr>
            <tr>
                <td><a href="#" >Link3</a><br /></td>
            </tr>
            <tr>
                <td><a href="#" >Link4</a><br /></td>
            </tr>
            <tr>
                <td><a href="#" >Link5</a><br /></td>
            </tr>
            <tr>
                <td><a href="#" >Link6</a><br /></td>
            </tr>
        </table>
            
        --%>
        <!-- End -->

        <table class="table">
            <tr>
                <td style="width: 150px">
                    <p class="pull-right">File input (เพิ่มไฟล์) :</p>
                </td>
                <td class="text-primary">
                    <asp:FileUpload ID="browse" runat="server" Multiple="Multiple" />
                    <i class="text-green">(ขนาดไฟล์ไม่เกิน 5 MB)</i><br />

                    <span id="Span1" runat="Server" />
                    
                    <table class="table">
                        <!-- Example -->
                        <tr>
                            <td style="width: 250px">
                                <input type="text" class="form-control pull-right" placeholder="" data-toggle="tooltip" title="แก้ไขชื่อไฟล์"></td>
                            <td style="width: 400px"><i class="fa fa-file-pdf-o text-red"></i>ข่าวประชาสัมพันธ์.pdf</td>
                            <td><i class="fa fa-close  text-red" data-toggle="tooltip" title="ลบไฟล์"></i></td>
                        </tr>
                        <tr>
                            <td style="width: 250px">
                                <input type="text" class="form-control pull-right" placeholder="" data-toggle="tooltip" title="แก้ไขชื่อไฟล์"></td>
                            <td style="width: 400px"><i class="fa fa-file-excel-o text-green"></i>รายชื่อผู้รับทุน.xlsx</td>
                            <td><i class="fa fa-close  text-red" data-toggle="tooltip" title="ลบไฟล์"></i></td>
                        </tr>
                        <tr>
                            <td style="width: 250px">
                                <input type="text" class="form-control pull-right" placeholder="" data-toggle="tooltip" title="แก้ไขชื่อไฟล์"></td>
                            <td style="width: 400px"><i class="fa fa-picture-o"></i>ความช่วยเหลือ.png</td>
                            <td><i class="fa fa-close  text-red" data-toggle="tooltip" title="ลบไฟล์"></i></td>
                        </tr>
                        <!-- End Example -->
                    </table>

                    <p>                        
                        <asp:Button ID="Upload" runat="server" Text="Upload File" OnClick="btnUplaod_Click"/>
                    <p>

                </td>
            </tr>
        </table>

        <div class="row">
            <div class="col-sm-12 pull-right">
                <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" Text="เพิ่ม"
                    CssClass="btn btn-primary"/>
            </div>
        </div>

        <script type="text/javascript">
         function showMenu(mID){
             document.getElementById(mID).style.display = "block";
         }
         function hideMenu(mID) {
             document.getElementById(mID).style.display = "none";
         }
        </script>
    </div>
</asp:Content>


