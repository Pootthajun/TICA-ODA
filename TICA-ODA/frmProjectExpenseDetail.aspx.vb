﻿Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Partial Class frmProjectExpenseDetail
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

#Region "_Property"

    Public Property AllData As DataTable
        Get
            Try
                Return Session("ProjectExpenseData")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("ProjectExpenseData") = value
        End Set
    End Property

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Public Property mode As String
        'view/edit
        Get
            Try
                Return ViewState("mode")
            Catch ex As Exception
                Return "add"
            End Try
        End Get
        Set(value As String)
            ViewState("mode") = value
        End Set
    End Property

    Public Property EditExpenseID As Long
        Get
            Try
                Return ViewState("ExpenseID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ExpenseID") = value
        End Set
    End Property

    Public Property EditProjectID As Long
        Get
            Try
                Return ViewState("ProjectID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ProjectID") = value
        End Set
    End Property
#End Region

    Private Sub frmProjectExpenseDetail_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'Try
            '        EditExpenseID = CInt(Request.QueryString("ID"))
            '        EditProjectID = CInt(Request.QueryString("pjid"))
            '        mode = Request.QueryString("mode").ToString()
            '    Catch ex As Exception
            '        EditExpenseID = 0
            '    End Try
            '    'เรียก Template มาให้เลือก'
            '    BL.Bind_DDL_ExpenseTemplate(ddlTemplate)
            '    SetExpenseInfo(EditExpenseID)
            BindList(325)

        '    If mode = "view" Then
            '        SetControlToViewMode(True)
            '    Else
            '        SetControlToViewMode(False)
            '    End If

        End If
    End Sub

    Private Sub SetControlToViewMode(IsView As Boolean)
        txtPaymentDate.Enabled = Not IsView
        'txtReceivedBy.Enabled = Not IsView
        txtDescription.Enabled = Not IsView
        ddlTemplate.Enabled = Not IsView
        btnSave.Visible = Not IsView

        For i As Integer = 0 To rptList.Items.Count - 1
            Dim UCExpens As usercontrol_UCExpenseDetail = DirectCast(rptList.Items(i).FindControl("UCExpenseDetail"), usercontrol_UCExpenseDetail)
            'UCExpens.SetToViewMode(IsView)
        Next

    End Sub

    'Sub SetExpenseInfo(expense_id As String)
    '    txtProjectID.Text = ""
    '    txtProjectName.Text = ""
    '    txtRecord.Text = ""
    '    'txtReceivedBy.Text = ""
    '    txtPaymentDate.Text = ""
    '    txtDescription.Text = ""
    '    ddlTemplate.SelectedIndex = 0

    '    If mode = "add" Then
    '        txtProjectID.Text = EditProjectID

    '        Dim dtpj As New DataTable
    '        dtpj = BL.GetProjectInfoByIDAllType(EditProjectID)

    '        If dtpj.Rows.Count > 0 Then
    '            txtProjectID.Text = dtpj.Rows(0)("id").ToString
    '            txtProjectName.Text = dtpj.Rows(0)("project_name").ToString
    '        End If



    '        txtRecord.Text = "New"
    '    Else
    '        Dim dt As New DataTable
    '        dt = BL.GetList_Expense(expense_id, "", "")
    '        If dt.Rows.Count > 0 Then
    '            txtProjectID.Text = dt.Rows(0)("project_id").ToString
    '            txtProjectName.Text = dt.Rows(0)("project_name").ToString
    '            txtRecord.Text = dt.Rows(0)("record").ToString()
    '            'txtReceivedBy.Text = dt.Rows(0)("received_by").ToString
    '            txtPaymentDate.Text = Convert.ToDateTime(dt.Rows(0)("payment_date")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
    '            txtDescription.Text = dt.Rows(0)("description").ToString()
    '            ddlTemplate.SelectedValue = dt.Rows(0)("template_id").ToString()
    '        End If
    '    End If
    'End Sub

    Sub BindList(project_id As String)
        Dim dt As New DataTable
        dt = BL.GetProjectActivityWithRecipince(project_id)

        rptList.DataSource = dt
        rptList.DataBind()

        AllData = dt
        Pager.SesssionSourceName = "ProjectExpenseData"
        Pager.RenderLayout()
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim lblactivityid As Label = DirectCast(e.Item.FindControl("lblactivityid"), Label)
        Dim UCdt As usercontrol_UCExpenseDetail = DirectCast(e.Item.FindControl("UCExpenseDetail"), usercontrol_UCExpenseDetail)
        lblactivityid.Text = e.Item.DataItem("id").ToString

        Dim dt As DataTable = BL.GetActivityRecipince(325)
        If dt.Rows.Count > 0 Then
            UCdt.TemplateID = 55 'ddlTemplate.SelectedValue
            UCdt.setCol_rpt(55)
            '            UCdt.setHead_rpt(55)
            UCdt.EditExpenseID = EditExpenseID
            UCdt.ActivityID = 325
            'UCdt.DetailDT = dt
        Else
            UCdt.Visible = False
        End If
    End Sub

    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand

    End Sub

    Sub btnSave_Click(sender As Object, e As EventArgs)
        Try
            '## Validate
            If Validate() = False Then
                Exit Sub
            End If

            Dim lnqExpense As New TbExpenseLinqDB
            With lnqExpense
                .ID = EditExpenseID
                .PROJECT_ID = txtProjectID.Text
                .PAYMENT_DATE = Converter.StringToDate(txtPaymentDate.Text.Trim, "dd/MM/yyyy")
                '.RECEIVED_BY = txtReceivedBy.Text
                .DESCRIPTION = txtDescription.Text
                .TEMPLATE_ID = ddlTemplate.SelectedValue
            End With

            Dim dt As New DataTable
            With dt
                .Columns.Add("seq")
                .Columns.Add("activity_id")
                .Columns.Add("recipince_id")
                .Columns.Add("recipince_type")
                .Columns.Add("expense_sub_id")
                .Columns.Add("amount")
            End With

            'Dim dr As DataRow
            'For i As Integer = 0 To rptList.Items.Count - 1
            '    Dim lblactivityid As Label = DirectCast(rptList.Items(i).FindControl("lblactivityid"), Label)
            '    Dim UCExpens As usercontrol_UCExpenseDetail = DirectCast(rptList.Items(i).FindControl("UCExpenseDetail"), usercontrol_UCExpenseDetail)
            '    Dim dtDetail As DataTable = UCExpens.DetailDT
            '    For j As Integer = 0 To dtDetail.Rows.Count - 1
            '        dr = dt.NewRow
            '        dr("activity_id") = lblactivityid.Text
            '        dr("recipince_id") = dtDetail.Rows(j)("recipince_id").ToString
            '        dr("recipince_type") = dtDetail.Rows(j)("recipince_type").ToString
            '        dr("expense_sub_id") = dtDetail.Rows(j)("expense_sub_id").ToString
            '        dr("amount") = dtDetail.Rows(j)("amount").ToString
            '        dt.Rows.Add(dr)
            '    Next
            'Next

            Dim ret As New ProcessReturnInfo
            'ret = BL.SaveExpenseDetail(lnqExpense, dt, UserName)
            If ret.IsSuccess Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');window.location ='frmProjectExpense.aspx';", True)
            Else
                alertmsg("ไม่สามารถบันทึกข้อมูลได้ " & ret.ErrorMessage)
            End If

        Catch ex As Exception
            alertmsg("ไม่สามารถบันทึกข้อมูลได้ ")
        End Try
    End Sub

    Private Function Validate() As Boolean
        Dim ret As Boolean = True

        If txtPaymentDate.Text = "" Then
            alertmsg("กรุณาระบุวันที่จ่ายเงิน")
            ret = False

        Else
            Try
                Converter.StringToDate(txtPaymentDate.Text.Trim, "dd/MM/yyyy")
            Catch ex As Exception
                alertmsg("กรุณาตรวจสอบวันที่")
                ret = False
            End Try
        End If

        If rptList.Items.Count = 0 And mode <> "add" Then
            alertmsg("กรุณาระบุรายจ่าย")
            ret = False
        End If

        Return ret
    End Function

    Private Sub alertmsg(msg As String)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & msg & "');", True)
    End Sub

    Sub btnCancel_Click(sender As Object, e As EventArgs)
        Response.Redirect("frmProjectExpense.aspx")
    End Sub

    Private Sub ddlTemplate_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTemplate.SelectedIndexChanged
        BindList(EditProjectID)
    End Sub
End Class
