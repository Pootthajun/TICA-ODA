﻿
Imports Constants
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class rptRecipience_Loan
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG
    Dim GL As New GenericLib

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
        li.Attributes.Add("class", "active")
        Dim li_mnuReports_foreign As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports_Admin")
        li_mnuReports_foreign.Attributes.Add("class", "active")
        Dim a As HtmlAnchor = Me.Page.Master.FindControl("Admin_01")
        a.Attributes.Add("style", "color:#FF8000")

        If Not IsPostBack Then
            BindList()
        End If

    End Sub


    Public Function GetList() As DataTable

        Dim DT As New DataTable

        Dim filter As String = ""
        Dim Title As String = ""
        Try
            Dim sql As String = ""
            sql &= "    Select *  " & VbLf
            sql &= "    ,dbo.GetShortThaiDate(Payment_Date_Plan) Payment_Date_Plan_TH  " & VbLf
            sql &= "    ,dbo.GetShortThaiDate(Payment_Date_Actual) Payment_Date_Actual_TH  " & VbLf
            sql &= "    FROM vw_Recipience_Loan  " & VbLf

                If (txtSearch_Recipient.Text <> "") Then

                filter &= "  REPLACE(FullName_TH,'  ',' ') Like REPLACE('%" & txtSearch_Recipient.Text & "%','  ',' ')  AND " & VbLf
                Title += " ผู้ยืม " & txtSearch_Recipient.Text
            End If

            If (txtSearch_Objective.Text <> "") Then
                filter &= "  REPLACE(Objective,'  ',' ') Like REPLACE('%" & txtSearch_Objective.Text & "%','  ',' ')  AND " & VbLf
                Title += " สำหรับ " & txtSearch_Objective.Text
            End If

            If (txtStartDate.Text <> "" And txtEndDate.Text <> "") Then
                Dim date1 As DateTime = DateTime.ParseExact(txtStartDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                Dim StartDate As String = date1.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

                Dim date2 As DateTime = DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                Dim EndDate As String = date2.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

                filter &= "  Payment_Date_Plan Between '" & StartDate & "' And '" & EndDate & "'  AND " & VbLf
                Title += " ระหว่างวันที่ " & txtStartDate.Text & " ถึงวันที่ " & txtEndDate.Text

            End If

            '=======ยังไม่มี Field กรอกเลขที่ใบยืม=========
            'If (txtSearch_DocNo.Text <> "") Then
            '    filter &= "  REPLACE(Objective,'  ',' ') Like REPLACE('%" & txtSearch_DocNo.Text & "%','  ',' ')  AND " & VbLf
            '    Title += " เลขที่ใบยืม : " & txtSearch_DocNo.Text
            'End If

            If filter <> "" Then
                sql += " WHERE " & filter.Substring(0, filter.Length - 6) & VbLf
            End If

            sql &= "order by Payment_Date_Plan DESC   " & VbLf

            Dim DA As New SqlDataAdapter(sql, BL.ConnectionString)
            DA.Fill(DT)

            lblTotalRecord.Text = Title
            If DT.Rows.Count = 0 Then
                lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
            Else
                lblTotalRecord.Text &= " พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
            End If

            Session("Search_Recipience_Loan_Title") = lblTotalRecord.Text
        Catch ex As Exception
        End Try
        Return DT

    End Function



    Private Sub BindList()


        Dim DT As DataTable = GetList()
        If (DT.Rows.Count > 0) Then
            lbl_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Pay_Amount_Plan)", "")).ToString("#,##0.00")

            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If


        rptList.DataSource = DT
        rptList.DataBind()

        Session("Search_Recipience_Loan") = DT

        Pager.SesssionSourceName = "Search_Recipience_Loan"
        Pager.RenderLayout()
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub


    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblNo As Label = DirectCast(e.Item.FindControl("lblNo"), Label)


        Dim lblFullName_TH As Label = DirectCast(e.Item.FindControl("lblFullName_TH"), Label)
        Dim lblObjective As Label = DirectCast(e.Item.FindControl("lblObjective"), Label)
        Dim lblPayment_Date_Plan As Label = DirectCast(e.Item.FindControl("lblPayment_Date_Plan"), Label)

        Dim lblPayment_Date_Actual As Label = DirectCast(e.Item.FindControl("lblPayment_Date_Actual"), Label)
        Dim lblPay_Amount_Plan As Label = DirectCast(e.Item.FindControl("lblPay_Amount_Plan"), Label)
        Dim lblDocNo As Label = DirectCast(e.Item.FindControl("lblDocNo"), Label)


        lblNo.Text = e.Item.ItemIndex + 1
        lblFullName_TH.Text = lblNo.Text & "." & e.Item.DataItem("FullName_TH").ToString
        lblObjective.Text = e.Item.DataItem("Objective").ToString

        If Convert.IsDBNull(e.Item.DataItem("Payment_Date_Plan")) = False Then
            lblPayment_Date_Plan.Text = GL.ReportThaiDate(e.Item.DataItem("Payment_Date_Plan"))
        End If
        If Convert.IsDBNull(e.Item.DataItem("Payment_Date_Actual")) = False Then
            lblPayment_Date_Actual.Text = GL.ReportThaiDate(e.Item.DataItem("Payment_Date_Actual"))
        End If


        If Convert.IsDBNull(e.Item.DataItem("Pay_Amount_Plan")) = False Then
            lblPay_Amount_Plan.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Plan")).ToString("#,##0.00")
        End If

        lblDocNo.Text = ""

    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindList()

    End Sub

#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptRecipience_Loan.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptRecipience_Loan.aspx?Mode=EXCEL');", True)
    End Sub

#End Region


End Class

