﻿Imports System.Data
Imports Constants

Partial Class frmSubExpense
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    Public Property AllData As DataTable
        Get
            Try
                Return Session("SubExpensePage")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("SubExpensePage") = value
        End Set
    End Property

    Protected ReadOnly Property AuthorizeDT As DataTable
        Get
            Try
                Return CType(Session("Authorize"), DataTable)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuFinance")
        li.Attributes.Add("class", "active")

        Dim li2 As HtmlGenericControl = Me.Page.Master.FindControl("mnuMasterExpense")
        li2.Attributes.Add("class", "active")

        Dim a As HtmlAnchor = Me.Page.Master.FindControl("aSubExpense")
        a.Attributes.Add("style", "color:#FF8000")

        If Not IsPostBack Then
            BindList()
            Authorize()
            pnlAdSearch.Visible = False
            BL.Bind_DDL_ExpenseGroup(ddlExpenseGroup)
        End If

    End Sub

    Sub Authorize()
        If AuthorizeDT Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If
        Dim tmpdr() As DataRow = AuthorizeDT.Select("menu_id = '" & Menu.SubExpense & "' and isnull(is_save,'N') ='Y'")

        If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
            btnAdd.Visible = False
            HeadColDelete.Visible = False
        End If

        For i As Integer = 0 To rptList.Items.Count - 1
            Dim btnEdit As LinkButton = DirectCast(rptList.Items(i).FindControl("btnEdit"), LinkButton)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)
            Dim ColDelete As HtmlTableCell = DirectCast(rptList.Items(i).FindControl("ColDelete"), HtmlTableCell)

            If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
                btnEdit.Text = "View"
                btnEdit.CommandName = "View"
                btnEdit.CommandArgument = lblID.Text
                ColDelete.Visible = False
            End If
        Next
    End Sub

    Private Sub BindList()
        Dim DT As DataTable = BL.GetList_SubExpense(0, ddlExpenseGroup.SelectedValue, txtSearch.Text.Trim(), ddlStatus.SelectedValue)
        rptList.DataSource = DT
        rptList.DataBind()

        lblTotalList.Text = DT.Rows.Count

        AllData = DT
        Pager.SesssionSourceName = "SubExpensePage"
        Pager.RenderLayout()
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)
        Dim lblName As Label = DirectCast(e.Item.FindControl("lblNames"), Label)
        Dim lblGroupName As Label = DirectCast(e.Item.FindControl("lblGroupName"), Label)
        'Dim lblTemplate As Label = DirectCast(e.Item.FindControl("lblTemplate"), Label)
        Dim btnStatusReady As LinkButton = DirectCast(e.Item.FindControl("btnStatusReady"), LinkButton)
        Dim btnStatusDisabled As LinkButton = DirectCast(e.Item.FindControl("btnStatusDisabled"), LinkButton)

        lblID.Text = e.Item.DataItem("id").ToString
        lblName.Text = e.Item.DataItem("sub_name").ToString
        lblGroupName.Text = e.Item.DataItem("group_name").ToString
        'lblTemplate.Text = e.Item.DataItem("template_name").ToString
        Dim A As String = e.Item.DataItem("ACTIVE_STATUS").ToString
        If A = "Y" Then
            btnStatusReady.Visible = True
            btnStatusDisabled.Visible = False

        ElseIf A = "N" Then
            btnStatusDisabled.Visible = True
            btnStatusReady.Visible = False
        End If
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Response.Redirect("frmEditSubExpense.aspx")
    End Sub


    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        If e.CommandName = "Edit" Then
            Response.Redirect("frmEditSubExpense.aspx?id=" & e.CommandArgument & "&mode=edit")
        ElseIf e.CommandName = "View" Then
            Response.Redirect("frmEditSubExpense.aspx?id=" & e.CommandArgument & "&mode=view")
        ElseIf e.CommandName = "Delete" Then
            'Dim ret As ProcessReturnInfo =
            If BL.DeleteSubExpense(e.CommandArgument) Then
                BindList()
            Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('ไม่สามารถลบข้อมูลได้');", True)
        End If
        ElseIf e.CommandName = "cmdstatusready" Then
            Dim BL As New ODAENG
            'Dim ret As ProcessReturnInfo
            'ret =
            BL.EditStatusAllY(e.CommandArgument, "TB_Expense_Sub")
            'If ret.IsSuccess Then
            '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('แก้ไขสถานะเรียบร้อยแล้ว');", True)
            BindList()
            'End If
        ElseIf e.CommandName = "cmdstatusDisabled" Then
            Dim BL As New ODAENG
            'Dim ret As ProcessReturnInfo
            'ret =
            BL.EditStatusAllN(e.CommandArgument, "TB_Expense_Sub")
            'If ret.IsSuccess Then
            '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('แก้ไขสถานะเรียบร้อยแล้ว');", True)
            BindList()
            'End If
        End If

    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindList()
    End Sub
    Private Sub btnCancle_Click(sender As Object, e As EventArgs) Handles btnCancle.Click
        pnlAdSearch.Visible = False
        clearFormSearch()
        BindList()
    End Sub

    Private Sub btnOpenSearch_Click(sender As Object, e As EventArgs) Handles btnOpenSearch.Click
        pnlAdSearch.Visible = True
    End Sub

    Private Sub clearFormSearch()
        ddlExpenseGroup.SelectedValue = ""
        txtSearch.Text = ""
        ddlStatus.SelectedValue = ""
    End Sub
End Class

