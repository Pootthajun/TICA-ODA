﻿


Imports Constants
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class rptSummary_ByProject_In_Period
    Inherits System.Web.UI.Page

    Dim BL As New ODAENG
    Dim GL As New GenericLib


    Private Sub rptSummary_ByProject_In_Period_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
            li.Attributes.Add("class", "active")
            Dim li_mnuReports_foreign As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports_Admin")
            li_mnuReports_foreign.Attributes.Add("class", "active")
            Dim a As HtmlAnchor = Me.Page.Master.FindControl("Admin_8_6")
            a.Attributes.Add("style", "color:#FF8000")
            BL.Bind_DDL_Year(ddlBudgetYear, True)
            BL.Bind_DDL_Country_By_rptSummary_ByCountry(ddlCountry, True)
            BindList()
        End If

    End Sub


    Public Function GetList() As DataTable

        Dim DT As New DataTable

        Dim filter As String = ""
        Dim Title As String = " การให้ความช่วยเหลือแก่ประเทศ/ภูมิภาค "
        Try
            Dim sql As String = ""

            sql += "  --รายงาน การให้ความช่วยเหลือแก่ประเทศ /ภูมิภาคต่างของ ในปี													 " & VbLf
            sql += "  SELECT budget_year,country_node_id,country_name_th 											 " & VbLf
            sql += "  ,(		SELECT COUNT (project_id) FROM (													 " & VbLf
            sql += "  		SELECT DISTINCT  vw_Activity_PSN.project_id												 " & VbLf
            sql += "  			  ,[project_name]																	 " & VbLf
            sql += "  			  ,[project_type]																	 " & VbLf
            sql += "  			  ,[Country_Node_id]																 " & VbLf
            sql += "  			  ,[Country_name_th]																 " & VbLf
            sql += "  			  ,[Country_name_en]																 " & VbLf
            sql += "  		  FROM vw_Activity_PSN																	 " & VbLf
            sql += "  		  Left JOIN TB_activity On TB_activity.id = vw_Activity_PSN.activity_id					 " & VbLf
            sql += "  		  where getdate() between vw_Activity_PSN.start_date AND vw_Activity_PSN.end_date		 " & VbLf
            sql += "  		  ) AS TB_Count_Projrct																	 " & VbLf
            sql += "  																								 " & VbLf
            sql += "  		  Where convert(bigint, Country_Node_id)= TB.country_node_id 							 " & VbLf
            sql += "  		  )  Count_Project																		 " & VbLf
            sql += "  	   ,SUM(Count_Component_Bachelor) Count_Component_Bachelor									 " & VbLf
            sql += "  	   ,SUM(Count_Component_Training) Count_Component_Training									 " & VbLf
            sql += "  	   ,SUM(Count_Component_Other)	 Count_Component_Other										 " & VbLf
            sql += "  	   ,SUM(commitment_budget) commitment_budget												 " & VbLf
            sql += "  	FROM ( 																						 " & VbLf
            sql += "  																								 " & VbLf
            sql += "  		SELECT  budget_year																		 " & VbLf
            sql += "  				,country_node_id																 " & VbLf
            sql += "  				,country_name_th     															 " & VbLf
            sql += "  																								 " & VbLf
            sql += "  				,Count_Component_Bachelor														 " & VbLf
            sql += "  				,Count_Component_Training														 " & VbLf
            sql += "  																								 " & VbLf
            sql += "  				,Count_Component_Expert															 " & VbLf
            sql += "  				+Count_Component_Volunteer														 " & VbLf
            sql += "  				+Count_Component_Equipment														 " & VbLf
            sql += "  				+Count_All_Component  Count_Component_Other										 " & VbLf
            sql += "  				,commitment_budget																 " & VbLf
            sql += "  		  FROM vw_Project_Component																 " & VbLf
            sql += "  ) TB 																							 " & VbLf


            If (ddlBudgetYear.SelectedIndex > 0) Then
                filter += "   budget_year = '" & ddlBudgetYear.SelectedValue & "'  AND " & VbLf
                Title += " ปีงบประมาณ " & ddlBudgetYear.SelectedValue
            Else

            End If

            If (ddlCountry.SelectedIndex > 0) Then
                filter += "   convert(bigint,country_node_id) = " & ddlCountry.SelectedValue & "  AND " & VbLf
                Title += " ของประเทศ " & ddlCountry.SelectedItem.ToString()
            Else
            End If


            If filter <> "" Then
                sql += " WHERE " & filter.Substring(0, filter.Length - 6) & vbLf
            End If

            sql += "    GROUP BY	budget_year																		 " & VbLf
            sql += "  			,country_node_id																	 " & VbLf
            sql += "  			,country_name_th  																	 " & VbLf
            sql += "  	ORDER BY budget_year DESC																							 " & VbLf

            Dim DA As New SqlDataAdapter(sql, BL.ConnectionString)
            DA.Fill(DT)

            lblTotalRecord.Text = Title
            If DT.Rows.Count = 0 Then
                lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
            Else
                lblTotalRecord.Text &= " พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
            End If

            Session("Search_Summary_ByProject_In_Period_Title") = lblTotalRecord.Text
        Catch ex As Exception
        End Try
        Return DT

    End Function



    Private Sub BindList()


        Dim DT As DataTable = GetList()


        If (DT.Rows.Count > 0) Then
            lblCount_Project.Text = GL.ConvertCINT(DT.Compute("SUM(Count_Project)", ""))
            lblCount_Component_Bachelor.Text = GL.ConvertCINT(DT.Compute("SUM(Count_Component_Bachelor)", ""))
            lblCount_Component_Training.Text = GL.ConvertCINT(DT.Compute("SUM(Count_Component_Training)", ""))
            lblCount_Component_Other.Text = GL.ConvertCINT(DT.Compute("SUM(Count_Component_Other)", ""))

            lblSUM_Amount.Text = Convert.ToDecimal(DT.Compute("SUM(commitment_budget)", "")).ToString("#,##0.00")

            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If

        rptList.DataSource = DT
        rptList.DataBind()


        Session("Search_Summary_ByProject_In_Period") = DT

        Pager.SesssionSourceName = "Search_Summary_ByProject_In_Period"
        Pager.RenderLayout()
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Dim Lastbudget_year As String = ""

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If


        Dim lblCountry As Label = DirectCast(e.Item.FindControl("lblCountry"), Label)
        Dim lblCount_Project As Label = DirectCast(e.Item.FindControl("lblCount_Project"), Label)
        Dim lblCount_Component_Bachelor As Label = DirectCast(e.Item.FindControl("lblCount_Component_Bachelor"), Label)
        Dim lblCount_Component_Training As Label = DirectCast(e.Item.FindControl("lblCount_Component_Training"), Label)
        Dim lblCount_Component_Other As Label = DirectCast(e.Item.FindControl("lblCount_Component_Other"), Label)
        Dim lblSUM_Amount As Label = DirectCast(e.Item.FindControl("lblSUM_Amount"), Label)


        Dim DuplicatedStyleTop As String = "border-top:none;"
        Dim trbudget_year As HtmlTableRow = e.Item.FindControl("trbudget_year")
        Dim lblbudget_year As Label = e.Item.FindControl("lblbudget_year")

        '--------Lastbudget_year------------
        If Lastbudget_year <> e.Item.DataItem("budget_year").ToString Then
            Lastbudget_year = e.Item.DataItem("budget_year").ToString
            lblbudget_year.Text = Lastbudget_year
            trbudget_year.Visible = True

        Else
            trbudget_year.Visible = False
        End If



        lblCountry.Text = e.Item.DataItem("country_name_th").ToString

        If Convert.IsDBNull(e.Item.DataItem("Count_Project")) = False Then
            lblCount_Project.Text = GL.ConvertCINT(e.Item.DataItem("Count_Project"))
        End If

        If Convert.IsDBNull(e.Item.DataItem("Count_Component_Bachelor")) = False Then
            lblCount_Component_Bachelor.Text = GL.ConvertCINT(e.Item.DataItem("Count_Component_Bachelor"))
        End If

        If Convert.IsDBNull(e.Item.DataItem("Count_Component_Training")) = False Then
            lblCount_Component_Training.Text = GL.ConvertCINT(e.Item.DataItem("Count_Component_Training"))
        End If

        If Convert.IsDBNull(e.Item.DataItem("Count_Component_Other")) = False Then
            lblCount_Component_Other.Text = GL.ConvertCINT(e.Item.DataItem("Count_Component_Other"))
        End If

        If Convert.IsDBNull(e.Item.DataItem("commitment_budget")) = False Then
            lblSUM_Amount.Text = Convert.ToDecimal(e.Item.DataItem("commitment_budget")).ToString("#,##0.00")
        End If

    End Sub



    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click

        BindList()

    End Sub


#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptSummary_ByProject_In_Period.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptSummary_ByProject_In_Period.aspx?Mode=EXCEL');", True)
    End Sub

#End Region



End Class


