﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="rptOrganize_Loan.aspx.vb" Inherits="rptOrganize_Loan" %>

<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <title>Search | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
 
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>รายงานลูกหนี้เงินจ่ายล่วงหน้า  </h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>Report</a></li>
                <li class="active">รายงานลูกหนี้เงินจ่ายล่วงหน้า</li>
            </ol>
        </section>
        <br />


        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlList" runat="server" DefaultButton="btnSearch">
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                   

                                    <div class="box-header">
                                        <div class="col-sm-12" style="text-align: right;margin-bottom:10px;">
                                            <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                                <button type="button" class="btn btn-success btn-flat dropdown-toggle btn-print" data-toggle="dropdown">
                                                    <i class="fa fa-print"></i>
                                                </button>
                                                <ul class="dropdown-menu pull-right">
                                                    <%--<li><a href="#" id="aPDF" runat="server"><i class="fa fa-file-pdf-o text-red"></i>PDF</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#" id="aEXCEL" runat="server"><i class="fa fa-file-excel-o text-green"></i>EXCEL</a></li>--%>

                                                    <li><asp:LinkButton ID="btnPDF" runat="server"><i class="fa fa-file-pdf-o text-red"></i>รูปแบบ PDF</asp:LinkButton></li>
											        <li><asp:LinkButton ID="btnExcel" runat="server"><i class="fa fa-file-excel-o text-green"></i>รูปแบบ Excel</asp:LinkButton></li>	

                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-top:30px;"></div>




                                        <div class="row">
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">หน่วยงาน :</label>
                                                 <div class="col-sm-9">
                                                     <asp:TextBox ID="txtSearch_Organize" runat="server" placeholder="ค้นหาจากชื่อหน่วยงานที่ยืม" CssClass="form-control"></asp:TextBox>
                                                 </div>
                                             </div>
                                             <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">วัตถุประสงค์ :</label>
                                                 <div class="col-sm-9">
                                                     <asp:TextBox ID="txtSearch_Objective" runat="server" placeholder="ค้นหาจากวัตถุประสงค์" CssClass="form-control"></asp:TextBox>
                                                 </div>
                                             </div>
                                        </div>
                                        
                                        <div class="row" style="margin-top:10px;">
                                              <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height" width="100px;">วันยืม :</label>
                                                 <div class="col-sm-3">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </div>
                                                                        <asp:TextBox CssClass="form-control m-b" ID="txtStartDate" runat="server" placeholder="เริ่มต้น" Width="100px"></asp:TextBox>
                                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                                            Format="dd/MM/yyyy" TargetControlID="txtStartDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                                    </div>
                                                </div>
                                                 <div class="col-sm-1"><label for="inputname" class="col-sm-2 control-label line-height">To</label></div>
                                                     <div class="col-sm-4">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </div>
                                                                        <asp:TextBox CssClass="form-control m-b" ID="txtEndDate" runat="server" placeholder="สิ้นสุด" Width="100px"></asp:TextBox>
                                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                                            Format="dd/MM/yyyy" TargetControlID="txtEndDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                                    </div>
                                                 </div>
                                             </div>
                                            
                                             <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">เลขที่ใบยืม :</label>
                                                 <div class="col-sm-9">
                                                     <asp:TextBox ID="txtSearch_DocNo" runat="server" placeholder="ค้นหาจากเลขที่ใบยืม" CssClass="form-control"></asp:TextBox>
                                                 </div>
                                             </div>




                                    </div>

                                        <div class="row" style="margin-top:10px;">
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                 <div class="col-sm-9 ">

                                                 </div>
                                                </div>
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                 <div class="col-sm-9 ">
                                                   <%--<p class="pull-right" style="margin-right:60px;">--%>
                                                     <p class="pull-right" style="margin-right:10px;">
                                                        <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn bg-blue margin-r-5 btn-social"   >
                                                            <i class="fa fa-search"></i>
                                                            <asp:Label ID="Search" runat="server" Text="Search"  ></asp:Label> 
                                                        </asp:LinkButton>
                                                    </p>

                                                 </div>
                                                </div>

                                        </div>
                                        <div class="row"></div>
                                        <div class="row pull-left">

                                            <div class="col-md-12">
                                            <h4 class="text-primary" style="margin-left :20px;"><asp:Label ID="lblTotalRecord" runat="server" Text=""></asp:Label></h4>


                                        </div>
                                        </div>
                                         <div class="row pull-right">
                                            <div class="col-md-12" style="margin-right: 30px;">

                                            <h5  style="margin-left :20px;">Unit : THB</h5>
                                        
                                        </div> 
                                    </div> 
                                        </div>   
                                    <div class="box-body">
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th class="no"  id="th1" runat="server" visible="false">No.<br/>(ลำดับ)</th>
                                                    <th>หน่วยงาน</th>
                                                    <th>วัตถุประสงค์</th>
                                                    <th>วันยืม</th>
                                                    <th>จำนวนเงิน</th>
                                                    <th>เลขที่ใบยืม</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td data-title="No" class="center" id="tr1" runat="server" visible="false" >
                                                                <asp:Label ID="lblNo" runat="server"></asp:Label></td>
                                                            <td data-title="หน่วยงาน" style=" min-width :180px;" ><asp:Label ID="lblFullName_TH" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="วัตถุประสงค์" ><asp:Label ID="lblObjective" runat="server"></asp:Label></td>
                                                            <td data-title="วันยืม" style="text-align :center  ;" width="120;" ><asp:Label ID="lblPayment_Date_Plan" runat="server" ></asp:Label></td>
                                                            <td data-title="จำนวนเงิน"  style="text-align :right ; min-width :120px;" ><asp:Label ID="lblPay_Amount_Plan" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="เลขที่ใบยืม" style="text-align :center  ;min-width :100px;"><asp:Label ID="lblDocNo" runat="server" ForeColor="black"></asp:Label></td>
                                                        </tr>

                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </tbody>
                                            <asp:Panel ID="pnlFooter" runat="server" Visible ="True">
                                            <tfoot style="background-color:LemonChiffon;" >
                                        <tr id="trFooter_Qty" runat="server" >

                                            <td data-title="No" class="center" id="tr1" runat="server" visible="false">
                                                <asp:Label ID="lblNo" runat="server"></asp:Label></td>
                                            <td data-title="Total" style="text-align :center ;" colspan ="3" ><b>Total</b></td>
                                            
                                            <td data-title="Total"  style="text-align :right ;text-decoration: underline;" ><b><asp:Label ID="lbl_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title=""  style="text-align :right ;text-decoration: underline;" ></td>
                                                

										</tr>
                                    

                                        </tfoot>
                                            </asp:Panel>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->

                                    <!-- Page Navigation -->
                                    <uc1:PageNavigation runat="server" ID="Pager" PageSize="20" />

                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->

                     <asp:TextBox ID="txtClickProjectID" runat="server" Text="" Style="display: none"></asp:TextBox>
                     <%--<asp:Button ID="btnView" OnClick="btnView_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnEdit" OnClick="btnEdit_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnDelete" OnClick="btnDelete_Click" runat="server" Style="display: none" />--%>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

    <%--<script type="text/javascript">

        function btnEditClick(activity_id) {
            //alert(activity_id);
            document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
            document.getElementById('<%= btnEdit.ClientID %>').click();
        }
        function btnViewClick(activity_id) {
            document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
            document.getElementById('<%= btnView.ClientID %>').click();
        }
        function btnDeleteClick(activity_id) {

            var c = confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?");
            if (c == true) {
                document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
                document.getElementById('<%= btnDelete.ClientID %>').click();
            }

        }
    </script>--%>
</asp:Content>




