﻿
Imports System.Data
Imports System.Globalization
Imports Constants


Partial Class frmPlan
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    'Page Navigation
    Public Property AllData As DataTable
        Get
            Try
                Return Session("PlanData")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("PlanData") = value
        End Set
    End Property
    Dim TypeMode As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuMaster")
        li.Attributes.Add("class", "active")

        Dim a As HtmlAnchor = Me.Page.Master.FindControl("aPlan")
        a.Attributes.Add("style", "color:#FF8000")
        Dim TB_Authorize As DataTable = Session("Authorize")
        For i As Integer = 0 To TB_Authorize.Rows.Count - 1
            If Menu.Plan = TB_Authorize.Rows(i)("menu_id") Then
                If TB_Authorize.Rows(i)("is_save") = "Y" Then
                    TypeMode = "save"
                End If
                If TB_Authorize.Rows(i)("is_view") = "Y" Then
                    TypeMode = "view"
                    btnAdd.Visible = False
                End If
            End If
        Next

        If Not IsPostBack Then
            BindList()
            pnlAdSearch.Visible = False
        End If

    End Sub

    Private Sub BindList()
        If txtStartDate.Text <> "" And txtEndDate.Text <> "" Then
            If Validate() Then
                Dim date1 As DateTime = DateTime.ParseExact(txtStartDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                Dim StartDate As String = date1.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

                Dim date2 As DateTime = DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                Dim EndDate As String = date2.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

                Dim DT As DataTable = BL.GetList_Plan(0, txtSearch.Text, StartDate, EndDate, ddlStatus.SelectedValue)
                pnlAdSearch.Visible = False
                rptList.DataSource = DT
                rptList.DataBind()
                lblTotalList.Text = DT.DefaultView.Count
                'Page Navigation
                AllData = DT
                Pager.SesssionSourceName = "PlanData"
                Pager.RenderLayout()

            End If
        Else
            Dim DT As DataTable = BL.GetList_Plan(0, txtSearch.Text, "", "", ddlStatus.SelectedValue)
            pnlAdSearch.Visible = False
            rptList.DataSource = DT
            rptList.DataBind()
            lblTotalList.Text = DT.DefaultView.Count
            'Page Navigation
            AllData = DT
            Pager.SesssionSourceName = "PlanData"
            Pager.RenderLayout()
        End If
    End Sub

    'Page Navigation
    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

#Region "Repeater"
    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim lblPlanName As Label = DirectCast(e.Item.FindControl("lblPlanName"), Label)
        Dim lblPlanNameTh As Label = DirectCast(e.Item.FindControl("lblPlanNameTh"), Label)
        Dim lblStartDate As Label = DirectCast(e.Item.FindControl("lblStartDate"), Label)
        Dim lblEndDate As Label = DirectCast(e.Item.FindControl("lblEndDate"), Label)
        Dim btnStatusReady As LinkButton = DirectCast(e.Item.FindControl("btnStatusReady"), LinkButton)
        Dim btnStatusDisabled As LinkButton = DirectCast(e.Item.FindControl("btnStatusDisabled"), LinkButton)
        Dim liview As HtmlGenericControl = e.Item.FindControl("liview")
        Dim liedit As HtmlGenericControl = e.Item.FindControl("liedit")
        Dim lidelete As HtmlGenericControl = e.Item.FindControl("lidelete")
        If TypeMode = "view" Then
            liview.Visible = True
            liedit.Visible = False
            lidelete.Visible = False
        ElseIf TypeMode = "Edit" Then
            liview.Visible = False
            liedit.Visible = True
            lidelete.Visible = True
        End If


        lblPlanName.Text = e.Item.DataItem("plan_name").ToString
        lblPlanNameTh.Text = e.Item.DataItem("plan_name_th").ToString
        lblStartDate.Text = e.Item.DataItem("str_start_date").ToString
        lblEndDate.Text = e.Item.DataItem("str_end_date").ToString


        Dim A As String = e.Item.DataItem("ACTIVE_STATUS").ToString
        If A = "Y" Then
            btnStatusReady.Visible = True
            btnStatusDisabled.Visible = False

        ElseIf A = "N" Then
            btnStatusDisabled.Visible = True
            btnStatusReady.Visible = False
        End If

    End Sub

    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand

        If e.CommandName = "Edit" Then
            Response.Redirect("frmEditPlan.aspx?id=" & e.CommandArgument & "&mode=edit")
        ElseIf e.CommandName = "View" Then
            Response.Redirect("frmEditPlan.aspx?id=" & e.CommandArgument & "&mode=view")
        ElseIf e.CommandName = "Delete" Then
            Dim ret As Boolean = BL.DeletePlan(e.CommandArgument)
            If ret Then
                BindList()
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('ไม่สามารถลบข้อมูลได้');", True)
            End If
        ElseIf e.CommandName = "cmdstatusready" Then
            Dim BL As New ODAENG
            'Dim ret As ProcessReturnInfo
            'ret =
            BL.EditStatusAllY(e.CommandArgument, "TB_Plan")
            'If ret.IsSuccess Then
            '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('แก้ไขสถานะเรียบร้อยแล้ว');", True)
            BindList()
            'End If
        ElseIf e.CommandName = "cmdstatusDisabled" Then
            Dim BL As New ODAENG
            'Dim ret As ProcessReturnInfo
            'ret =
            BL.EditStatusAllN(e.CommandArgument, "TB_Plan")
            'If ret.IsSuccess Then
            '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('แก้ไขสถานะเรียบร้อยแล้ว');", True)
            BindList()
            'End If
        End If

    End Sub
#End Region

#Region "Button"
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindList()
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Response.Redirect("frmEditPlan.aspx")
    End Sub

    Private Sub btnCancle_Click(sender As Object, e As EventArgs) Handles btnCancle.Click
        pnlAdSearch.Visible = False
        clearFormSearch()
        BindList()
    End Sub

    Private Sub btnOpenSearch_Click(sender As Object, e As EventArgs) Handles btnOpenSearch.Click
        pnlAdSearch.Visible = True
    End Sub

    Function clearFormSearch()
        txtSearch.Text = ""
        txtStartDate.Text = ""
        txtEndDate.Text = ""
        ddlStatus.SelectedValue = ""
    End Function

    Private Function Validate() As Boolean
        Dim ret As Boolean = True
        If txtStartDate.Text <> "" And txtEndDate.Text <> "" Then
            Try
                Converter.StringToDate(txtStartDate.Text.Trim, "dd/MM/yyyy")
                Converter.StringToDate(txtEndDate.Text.Trim, "dd/MM/yyyy")
            Catch ex As Exception
                alertmsg("กรุณาตรวจสอบวันที่")
                ret = False
                pnlAdSearch.Visible = True
                Exit Function
            End Try
        End If

        If Converter.StringToDate(txtStartDate.Text.Trim, "dd/MM/yyyy") > Converter.StringToDate(txtEndDate.Text.Trim, "dd/MM/yyyy") Then
            alertmsg("กรุณาระบุวันที่เริ่มต้นและสิ้นสุดให้สอดคล้องกัน")
            ret = False
            pnlAdSearch.Visible = True
            Exit Function
        End If

        Return ret
    End Function

    Private Sub alertmsg(msg As String)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & msg & "');", True)
    End Sub

    Private Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged
        btnSearch.Focus()
    End Sub
#End Region

End Class
