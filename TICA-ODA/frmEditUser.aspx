﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeFile="frmEditUser.aspx.vb" Inherits="frmEditUser" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Add User Management | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            User Management (จัดการข้อมูลผู้ใช้)
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-area-chart"></i> Dashboard</a></li>
            <li><a href="#"> User Management</a></li>
            <li class="active">Add User Management</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content"><br />
          <!-- START CUSTOM TABS -->
          <div class="row">
            <div class="col-md-12">
              <!-- Horizontal Form -->
              <div class="box">
                <div class="box-header with-border bg-gray">
                  <h4 class="box-title">Add User</h4>
                </div><!-- /.box-header -->
                <!-- form start -->
                <table class="table table-bordered">
                        <tr>
                           <td colspan="2"><h4 class="box-title"><a href="#">Country </a>/<a href="#"> Thailand International Development Cooperation Agency </a>/ สำนักผู้อำนวยการ</h4></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">ชื่อภาษาไทย :</b></th>
                          <td><div class="col-sm-10"><input class="form-control" id="NameTH" placeholder=""></div></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">English Name :</b></th>
                          <td><div class="col-sm-10"><input class="form-control" id="NameEN" placeholder=""></div></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Id Card/Passport :</b></th>
                          <td><div class="col-sm-10"><input class="form-control" id="Id" placeholder=""></div></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Birthday (วันเกิด) :</b></th>
                          <td><div class="col-sm-10"><input class="form-control" id="Birthday" placeholder=""></div></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Telephone (โทรศัพท์) :</b></th>
                          <td><div class="col-sm-10"><input class="form-control" id="Telephone" placeholder=""></div></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">E-mail :</b></th>
                          <td><div class="col-sm-10"><input class="form-control" id="Website" placeholder=""></div></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Note (หมายเหตุ) :</b></th>
                          <td><div class="col-sm-10"><textarea class="form-control" rows="3" placeholder=""></textarea></div></td>
                        </tr>
                       <tr> 
                        <th style="width: 250px"><b class="pull-right">Active Status :</b></th>
                        <td><div class="col-sm-9">
                        <label><input type="checkbox" class="minimal" checked></label></div></td>
                        </tr>
                      </table>
                    <div class="box-footer">
                    <div class="col-sm-8"></div>
                    <div class="col-sm-2">
                      <a href="frmProjectExam.aspx" class="btn btn-block btn-social btn-success"><i class="fa fa-save"></i>Save </a>
                    </div>
                    <div class="col-sm-2">
                      <a href="frmProjectExam.aspx" class="btn btn-block btn-social btn-google"><i class="fa fa-reply"></i>Cancel </a>
                    </div>  
                    </div><!-- /.box-footer -->
                
              </div><!-- /.box -->
            </div><!-- /.col -->

          </div> <!-- /.row -->
          <!-- END CUSTOM TABS -->
          

        </section><!-- /.content -->
    </div>
   

<!----------------Page Advance---------------------->
        
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />

</asp:Content>