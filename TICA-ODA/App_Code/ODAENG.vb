﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports ProcessReturnInfo
Imports Constants
Imports System.Globalization
Imports System.Web.UI.UserControl

Public Class ODAENG

#Region "Set validate Textbox Property"
    Public Sub SetTextIntKeypress(txt As TextBox)
        txt.Attributes.Add("OnKeyPress", "ChkMinusInt(this,event);")
        txt.Attributes.Add("onKeyDown", "CheckKeyNumber(event);")
    End Sub
    Public Sub SetTextDblKeypress(txt As TextBox)
        txt.Attributes.Add("OnKeyPress", "ChkMinusDbl(this,event);")
        txt.Attributes.Add("onKeyDown", "CheckKeyNumber(event);")
        txt.Attributes.Add("onblur", "AddComma('" & txt.ClientID & "');")
        txt.Attributes.Add("onfocus", "ClearComma('" & txt.ClientID & "');")
    End Sub
    Public Sub SetTextAreaMaxLength(txt As TextBox, MaxLength As Int16)
        txt.Attributes.Add("onKeyDown", "checkTextAreaMaxLength(this,event,'" & MaxLength & "');")
    End Sub
#End Region

    Public ConnectionString As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString

#Region "Login"
    Public Function Check_Login(username As String, password As String) As LoginReturnInfo
        Dim Result As New LoginReturnInfo
        Try
            Dim SQL As String = " select * from TB_OU_Person where username='" & username.Replace("'", "''") & "' and password is not null  "
            Dim DT As New DataTable
            Dim DA As New SqlDataAdapter(SQL, ConnectionString)
            DA.Fill(DT)

            If DT.Rows.Count > 0 Then
                Dim pwd As String = EncryptString(password)
                'Dim pwd As String = password   'กรณีไม่มีการ Encrypt Password ให้ใช้บรรทัดนี้แทน
                Dim str As String = DecryptString(DT.Rows(0)("password"))

                If DT.Rows(0)("password") = pwd Then
                    Result.IsSuccess = True
                    With DT.Rows(0)
                        Result.Node_ID = .Item("node_id").ToString
                        Result.UserName = .Item("username").ToString
                        Result.Node_ID = .Item("node_id").ToString
                        Result.Name_TH = .Item("name_th").ToString
                    End With
                Else
                    Result.IsSuccess = False
                    Result.ErrorMessage = "Password ไม่ถูกต้อง"
                End If
            Else
                Result.IsSuccess = False
                Result.ErrorMessage = "Username ไม่ถูกต้อง"
            End If
        Catch ex As Exception
            Result.IsSuccess = False
            Result.ErrorMessage = ex.ToString()
        End Try
        Return Result
    End Function
#End Region

#Region "_Budget"
    Public Function GetList_GroupBudget(id As Long, strSearch As String, status As String) As DataTable
        Dim SQL As String = " select id,group_name,active_status from tb_budget_group where 1=1 "
        If id <> 0 Then
            SQL &= " and id = " & id
        End If
        If strSearch.Trim <> "" Then
            SQL &= " and (group_name LIKE '%" & strSearch.Replace("'", "''") & "%')"
        End If

        If status <> "" Then
            SQL &= "and active_status = '" & status & "'"
        End If
        SQL &= " order by group_name "
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function DeleteGroupBudget(ByVal id As Long) As Boolean

        Dim SQL As String = "Select 'y' from tb_budget_detail p  where budget_sub_id in (select id from tb_budget_sub where budget_group_id = " & id & ")"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Try
            DA.Fill(DT)
            If DT.Rows.Count > 0 Then
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try

        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM tb_budget_sub WHERE budget_group_id =" & id
                .ExecuteNonQuery()
                .CommandText = "DELETE FROM tb_budget_group WHERE id=" & id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True

    End Function

    Public Function GetList_SubBudget(ByVal id As Long, strSearch As String, BudgetGroup As String, status As String) As DataTable
        Dim SQL As String = " select tb_budget_sub.id,sub_name,budget_group_id,tb_budget_sub.active_status,group_name " & vbLf
        SQL &= " from tb_budget_sub inner join tb_budget_group On tb_budget_sub.budget_group_id= tb_budget_group.id " & vbLf
        SQL &= " where 1=1 " & vbLf
        If id <> 0 Then
            SQL &= " And tb_budget_sub.id =" & id
        End If
        If strSearch.Trim <> "" Then
            SQL &= " And sub_name Like '%" & strSearch.Replace("'", "''") & "%'"
        End If

        If BudgetGroup <> "" Then
            SQL &= " and budget_group_id = '" & BudgetGroup & "' "
        End If

        If status <> "" Then
            SQL &= " and tb_budget_sub.active_status = '" & status & "' "
        End If
        SQL &= " order by group_name,sub_name "

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function DeleteSubBudget(ByVal id As Long) As Boolean

        Dim SQL As String = "Select 'y' from tb_budget_detail p  where budget_sub_id = " & id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Try
            DA.Fill(DT)
            If DT.Rows.Count > 0 Then
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try

        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM tb_budget_sub WHERE id =" & id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Public Function GetList_BudgetDetail(ByVal id As Integer, ByVal strSearch As String, ByVal budgetYear As String, ByVal DateSearchS As String, ByVal DateSearchE As String) As DataTable
        Dim Title As String = ""
        Dim SQL As String = "select b.id,b.budget_year,(select [dbo].[GetFullThaiDate](b.received_date)) str_received_date,b.provide_by," & vbLf
        SQL &= "sum(bd.amount) As amount," & vbLf
        SQL &= "(select bg.group_name from TB_Budget_Sub As bs Join TB_Budget_Group As bg On bs.budget_group_id = bg.id Where bd.budget_sub_id = bs.id) As group_name" & vbLf
        SQL &= ",(select bs.sub_name from TB_Budget_Sub As bs Where bd.budget_sub_id = bs.id) As sub_name" & vbLf
        SQL &= "from TB_Budget As b left join TB_Budget_Detail As bd On b.id = bd.budget_id Where 1=1" & vbLf

        If id <> 0 Then
            SQL &= " And b.id = " & id & vbLf
        End If
        If strSearch <> "" Then
            SQL &= " AND (select bg.id from TB_Budget_Sub As bs Join TB_Budget_Group As bg On bs.budget_group_id = bg.id Where bd.budget_sub_id = bs.id) ='" & strSearch.Replace("'", "''") & "'  " & vbLf
        End If
        If budgetYear <> "" Then
            SQL &= " and b.budget_year ='" & budgetYear.Replace("'", "''") & "'" & vbLf
        End If
        If DateSearchS <> "" And DateSearchE <> "" Then
            SQL &= " AND b.received_date Between '" & DateSearchS.Replace("'", "''") & "' AND '" & DateSearchE.Replace("'", "''") & "'"
        End If
        SQL &= " Group By b.budget_year,b.received_date,bd.budget_sub_id,b.provide_by,b.id " & vbLf
        SQL &= " Order by b.budget_year DESC,b.received_date DESC" & vbLf

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetList_BudgetDetail1(ByVal id As Integer, strSearch As String, Optional budgetYear As String = "") As DataTable
        Dim SQL As String = "select b.id,b.budget_year,b.received_date,(select [dbo].[GetFullThaiDate](b.received_date)) str_received_date,b.provide_by,b.description," & vbLf
        SQL &= "sum(bd.amount) As amount," & vbLf
        SQL &= "(select bg.id from TB_Budget_Sub As bs Join TB_Budget_Group As bg On bs.budget_group_id = bg.id Where bd.budget_sub_id = bs.id) As group_id" & vbLf
        SQL &= ",(select bs.id from TB_Budget_Sub As bs where bs.id = bd.budget_sub_id) As sub_id" & vbLf
        SQL &= "from TB_Budget As b left join TB_Budget_Detail As bd On b.id = bd.budget_id Where 1=1" & vbLf

        If id <> 0 Then
            SQL &= " And b.id = " & id
        End If
        If strSearch.Trim <> "" Then
            SQL &= " And (b.budget_year Like '%" & strSearch.Replace("'", "''") & "%' or (select dbo.GetFullThaiDate(b.received_date)) like '%" & strSearch.Replace("'", "''") & "%')"
        End If
        If budgetYear <> "" Then
            SQL &= " and b.budget_year = '" & budgetYear.Replace("'", "''") & "'"
        End If
        SQL &= " Group By b.budget_year,b.received_date,bd.budget_sub_id,b.provide_by,b.id,b.description"

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function DeleteBudgetDetail(ByVal id As Long) As Boolean
        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM tb_budget_detail WHERE budget_id =" & id
                .ExecuteNonQuery()
                .CommandText = "DELETE FROM tb_budget WHERE id =" & id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Public Function GetList_SubBudgetDetail(ByVal group_id As Integer, ByVal budget_id As Integer, ByVal budget_year As String) As DataTable

        Dim SQL As String = ""
        SQL &= " Select  ROW_NUMBER() OVER(ORDER BY bg.group_name ASC) As Seq, " & vbLf
        SQL &= " bs.id, bs.sub_name, bg.group_name, x.budget_year, x.record, x.received_date, isnull(x.amount, 0) amount, " & vbLf
        SQL &= " bs.id budget_sub_id, isnull(x.budget, 0) budget, isnull(x.used, 0) used, isnull(x.budget, 0) - isnull(x.used, 0) balance" & vbLf
        SQL &= " From TB_Budget_Sub bs" & vbLf
        SQL &= " inner Join TB_Budget_Group bg on bg.id=bs.budget_group_id" & vbLf
        SQL &= " Left Join(" & vbLf
        SQL &= " Select  b.budget_year, b.record, b.received_date," & vbLf
        SQL &= " sum(bd.amount) amount, sum(isnull(bd.amount, 0)) budget," & vbLf
        SQL &= " sum(isnull(ab.amount, 0)) used, bs.id budget_sub_id, bs.budget_group_id" & vbLf
        SQL &= " From TB_Budget_Detail bd " & vbLf
        SQL &= " inner Join TB_Budget b on b.id=bd.budget_id" & vbLf
        SQL &= " inner Join TB_Budget_Sub bs on bs.id=bd.budget_sub_id" & vbLf
        SQL &= " left Join TB_Activity_Budget ab on ab.budget_sub_id=bs.id  And ab.budget_year=b.budget_year" & vbLf
        SQL &= "  left join TB_Activity a on ab.activity_id = a.id " & vbLf
        SQL &= " where 1=1" & vbLf
        If budget_id <> "0" Then
            SQL &= " and b.id=" & budget_id & vbLf
        End If
        SQL &= " And b.budget_year='" & budget_year & "'" & vbLf
        SQL &= " And a.parent_id = 0" & vbLf
        SQL &= " Group by b.budget_year, b.record, b.id, b.received_date, " & vbLf
        SQL &= " bd.budget_sub_id, bs.id, bs.budget_group_id" & vbLf
        SQL &= " ) x On x.budget_group_id=bg.id And x.budget_sub_id=bs.id" & vbLf
        SQL &= " where  bg.id =" & group_id & vbLf
        SQL &= " And isnull(bg.active_status,'Y')='Y'" & vbLf

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT

        ''------------ ทำไว้เพื่อ ? ------------
        'Dim DR As DataRow
        'DR = DT.NewRow
        'DR("id") = "-1"
        'DR("sub_name") = "รวม"
        'DR("amount") = "0"
        'DR("budget") = "0"
        'DR("used") = "0"
        'DR("balance") = "0"
        'DT.Rows.Add(DR)
        'Return DT
    End Function

    Public Function GetRecordBudget(ByVal budget_year As String) As Int16
        Dim SQL As String = "Select isnull(max(record), 0) + 1 As record from tb_budget where budget_year='" & budget_year.Replace("'", "''") & "' "
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT.Rows(0)(0)
    End Function

    Public Function SaveBudgetDetail(DTBudgetDetail As DataTable, budget_id As String, record As Int16, Username As String, TbBudget As TbBudgetLinqDB) As ProcessReturnInfo
        Dim ret As New ProcessReturnInfo
        Dim trans As New TransactionDB
        Try

            If DTBudgetDetail Is Nothing Or DTBudgetDetail.Rows.Count = 0 Then
                ret.IsSuccess = False
                ret.ErrorMessage = "ไม่พบข้อมูล"
                Return ret
            End If

            Dim exc As New ExecuteDataInfo
            Dim _lnq As New TbBudgetLinqDB
            With _lnq
                .GetDataByPK(budget_id, trans.Trans)
                .RECEIVED_DATE = TbBudget.RECEIVED_DATE 'DateTime.Now
                .BUDGET_YEAR = TbBudget.BUDGET_YEAR
                .RECEIVED_BY = TbBudget.RECEIVED_BY
                .DESCRIPTION = TbBudget.DESCRIPTION

                If .ID > 0 Then
                    exc = .UpdateData(Username, trans.Trans)
                Else
                    Dim _record As Int16 = GetRecordBudget(TbBudget.BUDGET_YEAR)
                    .RECORD = _record
                    exc = .InsertData(Username, trans.Trans)
                End If
            End With

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            For cnt As Integer = 0 To DTBudgetDetail.Rows.Count - 1
                Dim DTBudget As New DataTable
                DTBudget = DirectCast(DTBudgetDetail.Rows(cnt)("DT"), DataTable)
                Dim _group_id As String = DTBudgetDetail.Rows(cnt)("GROUP_ID").ToString

                If DTBudget IsNot Nothing AndAlso DTBudget.Rows.Count > 0 Then
                    '--TB_Budget_Detail
                    Dim p_ia(2) As SqlParameter
                    p_ia(0) = SqlDB.SetText("@_BUDGET_ID", _lnq.ID)
                    p_ia(1) = SqlDB.SetText("@_GROUP_ID", _group_id)
                    exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Budget_Detail WHERE budget_id=@_BUDGET_ID And  budget_sub_id In (Select id from TB_Budget_Sub where budget_group_id = @_GROUP_ID)", trans.Trans, p_ia)

                    If exc.IsSuccess = False Then
                        trans.RollbackTransaction()
                        ret.IsSuccess = False
                        ret.ErrorMessage = exc.ErrorMessage()
                        Return ret
                    End If

                    If Not DTBudget Is Nothing Then
                        Dim _budget_sub_id As String = ""
                        Dim _amount As String = ""
                        For i As Integer = 0 To DTBudget.Rows.Count - 1
                            _budget_sub_id = DTBudget.Rows(i)("budget_sub_id").ToString
                            _amount = DTBudget.Rows(i)("amount").ToString

                            Dim _lnqdetail As New TbBudgetDetailLinqDB
                            With _lnqdetail
                                .BUDGET_ID = _lnq.ID
                                .BUDGET_SUB_ID = _budget_sub_id
                                .AMOUNT = _amount

                                exc = .InsertData(Username, trans.Trans)
                            End With

                            If exc.IsSuccess = False Then
                                trans.RollbackTransaction()
                                ret.IsSuccess = False
                                ret.ErrorMessage = exc.ErrorMessage()
                                Return ret
                            End If
                        Next
                    End If
                End If
            Next

            trans.CommitTransaction()
            ret.IsSuccess = True
            ret.ErrorMessage = ""
        Catch ex As Exception
            trans.RollbackTransaction()
            ret.IsSuccess = False
            ret.ErrorMessage = ex.ToString()
        End Try
        Return ret
    End Function

    Public Function SaveBudgetDetail1(lnqBD As TbBudgetDetailLinqDB, budget_id As String, record As Int16, Username As String, TbBudget As TbBudgetLinqDB) As ProcessReturnInfo
        Dim ret As New ProcessReturnInfo
        Dim trans As New TransactionDB
        Try
            Dim exc As New ExecuteDataInfo
            Dim _TbBudget As New TbBudgetLinqDB
            With _TbBudget
                .GetDataByPK(TbBudget.ID, trans.Trans)

                .PROVIDE_BY = TbBudget.PROVIDE_BY
                .BUDGET_YEAR = TbBudget.BUDGET_YEAR
                .RECEIVED_DATE = TbBudget.RECEIVED_DATE
                .DESCRIPTION = TbBudget.DESCRIPTION
                .CREATED_BY = Username
                '.CREATED_DATE = 
                If .ID > 0 Then
                    .ID = TbBudget.ID
                    exc = .UpdateData(Username, trans.Trans)
                Else
                    .ID = TbBudget.ID()
                    exc = .InsertData(Username, trans.Trans)
                End If
            End With

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If



            Dim p_pjp(1) As SqlParameter
            p_pjp(0) = SqlDB.SetText("@_Budget_ID", _TbBudget.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Budget_Detail WHERE budget_id=@_Budget_ID", trans.Trans, p_pjp)
            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            Dim _lnqBD As New TbBudgetDetailLinqDB
            With _lnqBD
                .BUDGET_ID = _TbBudget.ID
                .AMOUNT = lnqBD.AMOUNT
                .BUDGET_SUB_ID = lnqBD.BUDGET_SUB_ID

                exc = .InsertData(Username, trans.Trans)
            End With

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            trans.CommitTransaction()
            ret.IsSuccess = True
            ret.ErrorMessage = ""
            ret.ID = _TbBudget.ID
        Catch ex As Exception
            trans.RollbackTransaction()
            ret.IsSuccess = False
            ret.ErrorMessage = ex.ToString()
        End Try

        Return ret

    End Function

#End Region

#Region "_Expense"
    Public Function GetList_GroupExpense(id As Long, strSearch As String, status As String) As DataTable
        Dim SQL As String = " Select id,group_name,active_status from TB_Expense_Group where 1=1 " & vbLf
        If id <> 0 Then
            SQL &= " And id = " & id & vbLf
        End If
        If strSearch.Trim <> "" Then
            SQL &= " And (group_name Like '%" & strSearch.Replace("'", "''") & "%')" & vbLf
        End If

        If status <> "" Then
            SQL &= " And (active_status = '" & status & "')" & vbLf
        End If
        SQL &= " order by group_name " & vbLf

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function DeleteGroupExpense(ByVal id As Long) As Boolean

        Dim SQL As String = "Select 'y' from tb_expense_detail p  where expense_sub_id in (select id from tb_expense_sub where expense_group_id = " & id & ")"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Try
            DA.Fill(DT)
            If DT.Rows.Count > 0 Then
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try

        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM tb_expense_sub WHERE expense_group_id =" & id
                .ExecuteNonQuery()
                .CommandText = "DELETE FROM tb_expense_group WHERE id=" & id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True

    End Function

    Public Function GetList_SubExpense(id As Long, Group As String, strSearch As String, status As String) As DataTable 'แสดงรายการทั้งหมด เพื่อให้ Template เลือก
        Dim SQL As String = " select s.id,sub_name,expense_group_id,expense_template_id,s.active_status,g.group_name " & vbLf
        SQL &= " From TB_Expense_Sub s inner join TB_Expense_Group g on s.expense_group_id=g.id" & vbLf
        SQL &= " where 1=1 " & vbLf
        If id <> 0 Then
            SQL &= " and s.id = " & id & vbLf
        End If
        If strSearch.Trim <> "" Then
            SQL &= " and  sub_name LIKE '%'" & strSearch.Replace(" '", "''") & "'%'" & vbLf
        End If

        If Group <> "" Then
            SQL &= " and expense_group_id = '" & Group & "'" & vbLf
        End If

        If status <> "" Then
            SQL &= " And s.active_status = '" & status & "'" & vbLf
        End If
        SQL &= " order by g.group_name,sub_name"

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetList_SubExpense_ForTemplate(id As Long, Group As String, strSearch As String, status As String) As DataTable 'แสดงรายการทั้งหมด เพื่อให้ Template เลือก
        Dim SQL As String = " select s.id,sub_name,expense_group_id,expense_template_id,s.active_status,g.group_name " & vbLf
        SQL &= " From TB_Expense_Sub s inner join TB_Expense_Group g on s.expense_group_id=g.id" & vbLf
        SQL &= " where 1=1 AND isnull(s.active_status,'Y')='Y' AND isnull(g.active_status,'Y')='Y'" & vbLf
        If id <> 0 Then
            SQL &= " and s.id = " & id & vbLf
        End If
        If strSearch.Trim <> "" Then
            SQL &= " and  sub_name LIKE '%" & strSearch.Replace("'", "''") & "%'" & vbLf
        End If

        If Group <> "" Then
            SQL &= " and expense_group_id = '" & Group & "'" & vbLf
        End If

        If status <> "" Then
            SQL &= " And s.active_status = '" & status & "'" & vbLf
        End If
        SQL &= " order by g.group_name,sub_name" & vbLf
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetList_SubExpenseNumRow() As DataTable
        Dim SQL As String = " SELECT expense_group_id,count(expense_group_id) num "
        SQL &= " FROM TB_Expense_Sub"
        SQL &= " Group By expense_group_id"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function DeleteSubExpense(ByVal id As Long) As Boolean

        Dim SQL As String = "Select 'y' from tb_expense_detail p  where expense_sub_id =" & id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Try
            DA.Fill(DT)
            If DT.Rows.Count > 0 Then
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try

        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM tb_expense_sub WHERE id =" & id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Public Function GetList_ExpenseList(id As Long, SDate As String, EDate As String, ProjectName As String, ActivityName As String, Status As String) As DataTable
        Dim SQL As String = "select AEH.id, A.activity_name, AEH.Activity_id, P.project_name, A.project_id, P.project_type, (Select [dbo].[GetFullThaiDate](AEH.Payment_Date_End)) As dateend " & vbLf
        SQL &= ", (Select [dbo].[GetFullThaiDate](AEH.Payment_Date_Start)) As datestart from" & vbLf
        SQL &= " TB_Activity_Expense_Header As AEH join TB_Activity As A On A.id = AEH.Activity_id" & vbLf
        SQL &= " join TB_Project As P On A.project_id = P.id Where AEH.id Like '%%'"

        If SDate <> "" And EDate <> "" Then
            SQL &= "AND AEH.Payment_Date_Start Between '" & SDate & "' ANd '" & EDate & "'"
        End If

        If ProjectName <> "" Then
            SQL &= "AND P.project_name Like '%" & ProjectName & "%'"
        End If

        If ActivityName <> "" Then
            SQL &= "AND A.activity_name Like '%" & ActivityName & "%'"
        End If

        If Status <> "" Then
            If Status = "0" Then
                SQL &= "AND (select isnull(sum(Pay_Amount_Plan),0) from TB_Activity_Expense_Detail As AED where AEH.id = AED.Header_id) = '0'"
            Else
                SQL &= "AND (select isnull(sum(Pay_Amount_Plan),0) from TB_Activity_Expense_Detail As AED where AEH.id = AED.Header_id) > '0'"
            End If
        End If

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetList_Expense(id As Long, strSearch As String, Optional project_type As String = "") As DataTable
        Dim SQL As String = "select ROW_NUMBER() OVER(ORDER BY e.id ASC) As Seq, isnull(e.id,0) id,e.payment_date," & vbLf
        SQL &= " p.id project_id, p.project_name, start_date, end_date, 0 budget,(select isnull(sum(amount),0)  from tb_expense_detail where expense_id = e.id) expense, " & vbLf
        SQL &= " (select [dbo].[GetFullThaiDate](e.payment_date)) str_payment_date," & vbLf
        SQL &= " (select [dbo].[GetFullThaiDate](start_date)) + '-' + (select [dbo].[GetFullThaiDate](end_date)) plan_date," & vbLf
        SQL &= " record, e.description,e.template_id" & vbLf
        SQL &= " From TB_Expense e inner join TB_Project p On e.project_id = p.id where 1=1 " & vbLf

        If id <> 0 Then
            SQL &= " And e.id =" & id & vbLf
        End If
        If strSearch.Trim <> "" Then
            SQL &= " And (e.project_id Like '%" & strSearch.Replace("'", "''") & "%' or p.project_name Like '%" & strSearch.Replace("'", "''") & "%' or (select [dbo].[GetFullThaiDate](e.payment_date)) like '%" & strSearch.Replace("'", "''") & "%')" & vbLf
        End If
        If project_type <> "" Then
            SQL &= " and project_type = " & project_type & vbLf
        End If
        SQL &= " order by p.project_id, p.project_name,record" & vbLf

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetList_Expense2(Activity_id As String) As DataTable
        Dim SQL As String = " select AEH.id,(Select [dbo].[GetFullThaiDate](Payment_Date_Start)) Payment_Date_Start,  (Select [dbo].[GetFullThaiDate](Payment_Date_End)) Payment_Date_End, "
        SQL &= " ISNULL((select sum(AEPD.Pay_Amount_Plan) from TB_Activity_Expense_Plan_Detail As AEPD where AEPD.Header_id=AEH.id),0) As Pay_Amount_Plan, " & vbLf
        SQL &= " ISNULL((select sum(AED.Pay_Amount_Actual) from TB_Activity_Expense_Actual_Detail As AED where AED.Header_id=AEH.id),0) AS Pay_Amount_Actual " & vbLf
        SQL &= "  from TB_Activity_Expense_Header As AEH where AEH.Activity_id = " & Activity_id & vbLf

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetList_Template_Name(template_id As Long, strSearch As String) As DataTable
        Dim SQL As String = "select * from TB_Template  where 1=1" & vbLf
        If template_id <> 0 Then
            SQL &= " And id =" & template_id & vbLf
        End If
        If strSearch.Trim <> "" Then
            SQL &= " And (template_name Like '%" & strSearch.Replace("'", "''") & "%')" & vbLf
        End If
        SQL &= " order by template_name " & vbLf

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetList_Template(template_id As Long, strSearch As String) As DataTable 'แสดงที่แต่ละ Template เลือก
        Dim SQL As String = "select	t.id,t.template_id,et.template_name, t.sub_expense_id, g.group_name,g.id" & vbLf
        SQL &= " from TB_Template_Item t" & vbLf
        SQL &= " inner join TB_Expense_Sub s on s.id = t.sub_expense_id " & vbLf
        SQL &= " Inner join TB_Expense_Group g on g.id = s.expense_group_id " & vbLf
        SQL &= " Inner Join TB_Template et On et.id = t.template_id  where 1=1"

        If template_id <> 0 Then
            SQL &= " And et.id =" & template_id & vbLf
        End If
        If strSearch.Trim <> "" Then
            SQL &= " And (et.template_name Like '%" & strSearch.Replace("'", "''") & "%' )" & vbLf
        End If
        SQL &= " order by et.template_name " & vbLf

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function DeleteTemplateExpense(ByVal ID As Long) As Boolean
        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM TB_Template_Item WHERE template_id =" & ID
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Public Function DeleteTemplate(ByVal id As Long) As Boolean
        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM TB_Template_Item WHERE template_id =" & id
                .ExecuteNonQuery()
                .CommandText = "DELETE FROM TB_Template WHERE id =" & id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True

    End Function

    Dim GL As New GenericLib
    ' ก่อนหน้านี้ type Long
    Public Function GetList_SubExpenseByTemplate(ByVal template_id As Long) As DataTable
        Dim SQL As String = "select Es.id, Es.sub_name, Es.expense_group_id, Es.expense_template_id, Es.active_status " & vbLf
        SQL &= " from TB_Expense_Sub As Es Join TB_Template_Item As TI On Es.id = TI.sub_expense_id Join TB_Template As T On T.id = TI.template_id  " & vbLf
        SQL &= " where isnull(Es.active_status,'Y')='Y' and Es.expense_group_id in (select id from TB_Expense_Group where isnull(active_status,'Y')='Y') and T.id =" & template_id & " order by Es.id"

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT

    End Function

    Public Function GetList_SubExpenseByHeaderId(ByVal Header As Long) As DataTable
        Dim SQL As String = " select ES.id,ES.sub_name " & vbLf
        SQL &= " from TB_Activity_Expense_Item As AEI join TB_Expense_Sub As ES On ES.id = AEI.sub_expense_id " & vbLf
        SQL &= " where AEI.header_id =" & Header & " order by ES.id " & vbLf

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetList_ExpensePlanForSum(ByVal Header As Long, ByVal Recipience_ID As Long) As DataTable
        Dim SQL As String = "select Expense_Sub_Activity_id,Pay_Amount_Plan" & vbLf
        SQL &= "From TB_Activity_Expense_Plan_Detail As AEPD " & vbLf
        SQL &= "Where AEPD.Header_id = " & Header & " And AEPD.Recipience_id = " & Recipience_ID & vbLf

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetList_TemplateByActivity(ByVal activity_id As Long) As Double
        Dim SQL As String = "Select template_id From TB_Expense_Template_Activity Where Activity_id = " & activity_id & vbLf

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)

        Return Val(DT.Rows(0)("template_id"))
    End Function

    Public Function GetList_SubExpenseDetailPlan(ByVal activity_id As Long, ByVal Header_ID As Long, ByVal template_id As Long, ByVal recipince_id As String) As DataTable
        Dim SQL As String = " Select T1.id expense_sub_id,sub_name,isnull(T2.Pay_Amount_Plan,0) As amount from (" & vbLf
        SQL &= "Select Es.ID,sub_name,expense_group_id,active_status from TB_Expense_Sub As Es join TB_Template_Item As ti On ti.sub_expense_id = Es.id  " & vbLf
        SQL &= " where isnull(active_status,'Y')='Y' and ti.template_id =" & template_id & " and expense_group_id in (select id from TB_Expense_Group where isnull(active_status,'Y')='Y') )T1" & vbLf
        SQL &= " Left join " & vbLf
        SQL &= "(" & vbLf
        SQL &= " Select AEPD.id,AEPD.Expense_Sub_Activity_id,AEPD.Pay_Amount_Plan From TB_Activity_Expense_Plan_Detail As AEPD " & vbLf
        SQL &= " join TB_Activity_Expense_Header As AEH On AEPD.Header_id = AEH.id  Where AEPD.Recipience_id ='" & recipince_id.Replace("'", "''") & "' and AEPD.Header_id =" & Header_ID & " and activity_id=" & activity_id & vbLf
        SQL &= " ) T2 On T1.id=T2.Expense_Sub_Activity_id " & vbLf
        SQL &= " Order By expense_sub_id"

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetList_SubExpenseDetailPlanListG(ByVal activity_id As Long, ByVal Header_ID As Long, ByVal template_id As Long, AEP As Long) As DataTable
        Dim SQL As String = " Select T1.id expense_sub_id,sub_name,isnull(T2.Pay_Amount_Plan,0) As amount from (" & vbLf
        SQL &= " Select Es.ID,sub_name,expense_group_id,active_status from TB_Expense_Sub As Es join TB_Activity_Expense_Item As ti On ti.sub_expense_id = Es.id " & vbLf
        SQL &= " where ti.template_id =" & Header_ID & ")T1" & vbLf
        SQL &= " Left join " & vbLf
        SQL &= "(" & vbLf
        SQL &= " Select AEPD.id,AEPD.Expense_Sub_Activity_id,AEPD.Pay_Amount_Plan From TB_Activity_Expense_Plan_Detail As AEPD " & vbLf
        SQL &= " join TB_Activity_Expense_Header As AEH On AEPD.Header_id = AEH.id  Where AEPD.Header_id =" & Header_ID & " and activity_id=" & activity_id & "  and AEPD.Activity_Expense_Plan_id =" & AEP & vbLf
        SQL &= " ) T2 On T1.id=T2.Expense_Sub_Activity_id " & vbLf
        SQL &= " Order By expense_sub_id"

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetList_SubExpenseDetailPlanList(ByVal activity_id As Long, ByVal Header_ID As Long, ByVal template_id As Long, ByVal recipince_id As String, ByVal AEP As Long) As DataTable
        Dim SQL As String = " Select T1.id expense_sub_id,sub_name,isnull(T2.Pay_Amount_Plan,0) As amount from (" & vbLf
        SQL &= "Select Es.ID,sub_name,expense_group_id,active_status from TB_Expense_Sub As Es join TB_Activity_Expense_Item As ti On ti.sub_expense_id = Es.id " & vbLf
        SQL &= "where ti.header_id =" & Header_ID & " )T1 " & vbLf
        SQL &= " Left join " & vbLf
        SQL &= "(" & vbLf
        SQL &= " Select AEPD.id,AEPD.Expense_Sub_Activity_id,AEPD.Pay_Amount_Plan From TB_Activity_Expense_Plan_Detail As AEPD " & vbLf
        SQL &= " join TB_Activity_Expense_Header As AEH On AEPD.Header_id = AEH.id  Where AEPD.Recipience_id ='" & recipince_id.Replace("'", "''") & "' and AEPD.Header_id =" & Header_ID & " and activity_id=" & activity_id & "  and AEPD.Activity_Expense_Plan_id =" & AEP & vbLf
        SQL &= " ) T2 On T1.id=T2.Expense_Sub_Activity_id " & vbLf
        SQL &= " Order By expense_sub_id"

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetList_SubExpenseDetailActualList(activity_id As Long, Header_ID As Long, template_id As Long, recipince_id As String, AEP As Long) As DataTable
        Dim SQL As String = " Select T1.id expense_sub_id,sub_name,isnull(T2.Pay_Amount_Actual,0) As amount from (" & vbLf
        SQL &= "Select Es.ID,sub_name,expense_group_id,active_status from TB_Expense_Sub As Es join TB_Activity_Expense_Item As AEI On AEI.sub_expense_id = Es.id " & vbLf
        SQL &= " where AEI.header_id =" & Header_ID & ")T1" & vbLf
        SQL &= " Left join " & vbLf
        SQL &= "(" & vbLf
        SQL &= " Select AEPD.id,AEPD.Expense_Sub_Activity_id,AEPD.Pay_Amount_Actual From TB_Activity_Expense_Actual_Detail As AEPD " & vbLf
        SQL &= " join TB_Activity_Expense_Header As AEH On AEPD.Header_id = AEH.id  Where AEPD.Recipience_id ='" & recipince_id.Replace("'", "''") & "' and AEPD.Header_id =" & Header_ID & " and activity_id=" & activity_id & " and AEPD.Activity_Expense_Actual_id =" & AEP & vbLf
        SQL &= " ) T2 On T1.id=T2.Expense_Sub_Activity_id " & vbLf
        SQL &= " Order By expense_sub_id"

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetList_SubExpenseDetailPlanGroup(activity_id As Long, Header_ID As Long, template_id As Long) As DataTable
        Dim SQL As String = " Select  T1.id expense_sub_id,sub_name, sum (isnull((T2.Pay_Amount_Plan),0) ) amount from ( " & vbLf
        SQL &= " Select Es.ID,sub_name,expense_group_id,active_status from TB_Expense_Sub As Es join TB_Template_Item As ti On ti.sub_expense_id = Es.id " & vbLf
        SQL &= " where isnull(active_status,'Y')='Y' and ti.template_id =" & template_id & " and expense_group_id in (select id from TB_Expense_Group where isnull(active_status,'Y')='Y') )T1 " & vbLf
        SQL &= " inner join " & vbLf
        SQL &= "(" & vbLf
        SQL &= " Select AEPD.id,AEPD.Expense_Sub_Activity_id,AEPD.Pay_Amount_Plan From TB_Activity_Expense_Plan_Detail As AEPD " & vbLf
        SQL &= " join TB_Activity_Expense_Header As AEH On AEPD.Header_id = AEH.id where AEH.Activity_id =" & activity_id & " and AEH.id=" & Header_ID & vbLf
        SQL &= " ) T2 On T1.id=T2.Expense_Sub_Activity_id " & vbLf
        SQL &= " group by T1.id,sub_name Order By expense_sub_id" & vbLf

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetList_SubExpenseDetailActual(ByVal activity_id As Long, Header_ID As Long, ByVal template_id As Long, ByVal recipince_id As String) As DataTable
        Dim SQL As String = " Select T1.id expense_sub_id,sub_name,isnull(T2.Pay_Amount_Actual,0) As amount from (" & vbLf
        SQL &= "Select Es.ID,sub_name,expense_group_id,active_status from TB_Expense_Sub As Es join TB_Template_Item As ti On ti.sub_expense_id = Es.id " & vbLf
        SQL &= " where isnull(active_status,'Y')='Y' and ti.template_id =" & template_id & " and expense_group_id in (select id from TB_Expense_Group where isnull(active_status,'Y')='Y') )T1" & vbLf
        SQL &= " Left join " & vbLf
        SQL &= "(" & vbLf
        SQL &= " Select AEAD.id,AEAD.Expense_Sub_Activity_id,AEAD.Pay_Amount_Actual From TB_Activity_Expense_Actual_Detail As AEAD " & vbLf
        SQL &= " join TB_Activity_Expense_Header As AEH On AEAD.Header_id = AEH.id  Where AEAD.Recipience_id ='" & recipince_id.Replace("'", "''") & "' and AEAD.Header_id =" & Header_ID & " and activity_id=" & activity_id & vbLf
        SQL &= " ) T2 On T1.id=T2.Expense_Sub_Activity_id " & vbLf
        SQL &= " Order By expense_sub_id"

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetList_SubExpenseDetailPay(ByVal activity_id As Long, ByVal Header_ID As Long, ByVal template_id As Long, ByVal recipince_id As String) As DataTable
        Dim SQL As String = "  Select T1.id expense_sub_id,sub_name,isnull(T2.Pay_Amount_Actual,0) amount,isnull(T2.Pay_Amount_Plan,0) Pay_Amount_Plan from (" & vbLf
        SQL &= "Select Es.ID,sub_name,expense_group_id,active_status from TB_Expense_Sub As Es join TB_Template_Item As ti On ti.sub_expense_id = Es.id  " & vbLf
        SQL &= " where isnull(active_status,'Y')='Y' and ti.template_id=" & template_id & " and expense_group_id in (select id from TB_Expense_Group where isnull(active_status,'Y')='Y') )T1" & vbLf
        SQL &= " Left join " & vbLf
        SQL &= "(" & vbLf
        SQL &= " Select AEH.id,AED.Expense_Sub_Activity_id,AED.Pay_Amount_Actual,AED.Pay_Amount_Plan From TB_Activity_Expense_Detail As AED join TB_Activity_Expense_Header As AEH On AED.Header_id = AEH.id Where AED.Recipience_id ='" & recipince_id.Replace("'", "''") & "' and AED.Header_id =" & Header_ID & " and activity_id=" & activity_id & vbLf
        SQL &= " ) T2 On T1.id=T2.Expense_Sub_Activity_id " & vbLf
        SQL &= " Order By expense_sub_id"

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetList_GroupExpenseForTB(ByVal template_id As Long) As DataTable
        Dim SQL As String = "select ES.expense_group_id,EG.group_name,COUNT(EG.group_name) As item_count "
        SQL &= "from TB_Expense_Group As EG join TB_Expense_Sub As ES On EG.id = ES.expense_group_id "
        SQL &= "Join TB_Template As TE On TE.sub_expense_id = ES.id WHERE TE.template_id =" & template_id & " Group By ES.expense_group_id,EG.group_name "

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetRecordExpense(ByVal project_id As String) As Int16
        Dim SQL As String = "select isnull(max(record),0)  + 1 as record from tb_expense where project_id =" & project_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return CInt(DT.Rows(0)("record"))
    End Function

    Public Function SaveExpenseDetailActual(RecipienceID As String, HeaderID As String, DTExpensePlan As DataTable, DTExpensePlanDetail As DataTable, Username As String) As ProcessReturnInfo
        Dim ret As New ProcessReturnInfo
        Dim trans As New TransactionDB
        Try

            Dim exc As New ExecuteDataInfo

            Dim p_ab(2) As SqlParameter
            p_ab(0) = SqlDB.SetText("@_Recipience_ID", RecipienceID)
            p_ab(1) = SqlDB.SetText("@_Header_ID", HeaderID)
            exc = SqlDB.ExecuteNonQuery("delete from TB_Activity_Expense_Actual where Recipience_id = @_Recipience_ID and Header_id = @_Header_ID", trans.Trans, p_ab)
            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage
                Return ret
            End If

            Dim p_EPD(2) As SqlParameter
            p_EPD(0) = SqlDB.SetText("@_Recipience_ID", RecipienceID)
            p_EPD(1) = SqlDB.SetText("@_Header_ID", HeaderID)
            exc = SqlDB.ExecuteNonQuery("delete from TB_Activity_Expense_Actual_Detail where Recipience_id = @_Recipience_ID and Header_id = @_Header_ID", trans.Trans, p_EPD)
            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage
                Return ret
            End If

            If Not DTExpensePlan Is Nothing Then
                For i As Integer = 0 To DTExpensePlan.Rows.Count - 1
                    Dim id As String = DTExpensePlan.Rows(i)("id").ToString()
                    Dim Payment_Date_Plan As String = DTExpensePlan.Rows(i)("Payment_Date_Actual").ToString()
                    Dim Pay_Plan_Detail As String = DTExpensePlan.Rows(i)("Pay_Actual_Detail").ToString()
                    Dim Payment_Plan_Detail As String = DTExpensePlan.Rows(i)("Payment_Actual_Detail").ToString()
                    Dim Recipience_id As String = DTExpensePlan.Rows(i)("Recipience_id").ToString()

                    'แยกวัน เดือน ปี
                    Dim splitPlan As String() = Payment_Date_Plan.Split("/")
                    'แปลง ค.ศ.เป็น พ.ศ.
                    Dim setPlanyear As Integer = Convert.ToInt32(splitPlan(2)) - 543
                    If setPlanyear < 1997 Then
                        setPlanyear += 543
                    End If
                    'มาจัดรูปแบบใหม่ว่าต้องการแบบไหน
                    Dim texPlan As String = ((splitPlan(0) & "/") + splitPlan(1) & "/") + setPlanyear.ToString()
                    Dim _lnqAEP As New TbActivityExpenseActualLinqDB
                    With _lnqAEP
                        .PAYMENT_DATE_ACTUAL = Converter.StringToDate(texPlan, "dd/MM/yyyy")
                        .PAY_ACTUAL_DETAIL = Pay_Plan_Detail
                        .PAYMENT_ACTUAL_DETAIL = Payment_Plan_Detail
                        .RECIPIENCE_ID = RecipienceID
                        .HEADER_ID = HeaderID

                        exc = .InsertData(Username, trans.Trans)
                    End With

                    If exc.IsSuccess = False Then
                        trans.RollbackTransaction()
                        ret.IsSuccess = False
                        ret.ErrorMessage = exc.ErrorMessage()
                        Return ret
                    End If
                    For j As Integer = 0 To DTExpensePlanDetail.Rows.Count - 1
                        Dim Expense_Plan_Id As String = DTExpensePlanDetail.Rows(j)("Activity_Expense_Actual_id").ToString()
                        Dim Expense_Sub_Activity_id As String = DTExpensePlanDetail.Rows(j)("Expense_Sub_Actual_id").ToString()
                        Dim Pay_Amount_Plan As String = DTExpensePlanDetail.Rows(j)("Pay_Amount_Actual").ToString()
                        If id = Expense_Plan_Id Then

                            Dim _lnqAEPD As New TbActivityExpenseActualDetailLinqDB
                            With _lnqAEPD
                                .HEADER_ID = HeaderID
                                .RECIPIENCE_ID = RecipienceID
                                .EXPENSE_SUB_ACTIVITY_ID = Expense_Sub_Activity_id
                                .PAY_AMOUNT_ACTUAL = Pay_Amount_Plan
                                .ACTIVITY_EXPENSE_ACTUAL_ID = _lnqAEP.ID

                                exc = .InsertData(Username, trans.Trans)

                            End With

                            If exc.IsSuccess = False Then
                                trans.RollbackTransaction()
                                ret.IsSuccess = False
                                ret.ErrorMessage = exc.ErrorMessage()
                                Return ret
                            End If

                        End If
                    Next
                Next
            End If

            trans.CommitTransaction()
            ret.IsSuccess = True
            ret.ErrorMessage = ""
        Catch ex As Exception
            trans.RollbackTransaction()
            ret.IsSuccess = False
            ret.ErrorMessage = ex.ToString()
        End Try
        Return ret

    End Function

    Public Function SaveExpenseDetailPlan(RecipienceID As String, HeaderID As String, DTExpensePlan As DataTable, DTExpensePlanDetail As DataTable, Username As String) As ProcessReturnInfo
        Dim ret As New ProcessReturnInfo
        Dim trans As New TransactionDB
        Try

            Dim exc As New ExecuteDataInfo

            Dim p_ab(2) As SqlParameter
            p_ab(0) = SqlDB.SetText("@_Recipience_ID", RecipienceID)
            p_ab(1) = SqlDB.SetText("@_Header_ID", HeaderID)
            exc = SqlDB.ExecuteNonQuery("delete from TB_Activity_Expense_Plan where Recipience_id = @_Recipience_ID and Header_id = @_Header_ID", trans.Trans, p_ab)
            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage
                Return ret
            End If

            Dim p_EPD(2) As SqlParameter
            p_EPD(0) = SqlDB.SetText("@_Recipience_ID", RecipienceID)
            p_EPD(1) = SqlDB.SetText("@_Header_ID", HeaderID)
            exc = SqlDB.ExecuteNonQuery("delete from TB_Activity_Expense_Plan_Detail where Recipience_id = @_Recipience_ID and Header_id = @_Header_ID", trans.Trans, p_EPD)
            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage
                Return ret
            End If

            If Not DTExpensePlan Is Nothing Then
                For i As Integer = 0 To DTExpensePlan.Rows.Count - 1
                    Dim id As String = DTExpensePlan.Rows(i)("id").ToString()
                    Dim Payment_Date_Plan As String = DTExpensePlan.Rows(i)("Payment_Date_Plan").ToString()
                    Dim Pay_Plan_Detail As String = DTExpensePlan.Rows(i)("Pay_Plan_Detail").ToString()
                    Dim Payment_Plan_Detail As String = DTExpensePlan.Rows(i)("Payment_Plan_Detail").ToString()
                    Dim Recipience_id As String = DTExpensePlan.Rows(i)("Recipience_id").ToString()
                    'แยกวัน เดือน ปี
                    Dim splitPlan As String() = Payment_Date_Plan.Split("/")
                    'แปลง ค.ศ.เป็น พ.ศ.
                    Dim setPlanyear As Integer = Convert.ToInt32(splitPlan(2)) - 543
                    If setPlanyear < 1997 Then
                        setPlanyear += 543
                    End If
                    'มาจัดรูปแบบใหม่ว่าต้องการแบบไหน
                    Dim texPlan As String = ((splitPlan(0) & "/") + splitPlan(1) & "/") + setPlanyear.ToString()
                    Dim _lnqAEP As New TbActivityExpensePlanLinqDB
                    With _lnqAEP
                        .PAYMENT_DATE_PLAN = Converter.StringToDate(texPlan, "dd/MM/yyyy")
                        .PAY_PLAN_DETAIL = Pay_Plan_Detail
                        .PAYMENT_PLAN_DETAIL = Payment_Plan_Detail
                        .RECIPIENCE_ID = RecipienceID
                        .HEADER_ID = HeaderID

                        exc = .InsertData(Username, trans.Trans)
                    End With

                    If exc.IsSuccess = False Then
                        trans.RollbackTransaction()
                        ret.IsSuccess = False
                        ret.ErrorMessage = exc.ErrorMessage()
                        Return ret
                    End If
                    For j As Integer = 0 To DTExpensePlanDetail.Rows.Count - 1
                        Dim Expense_Plan_Id As String = DTExpensePlanDetail.Rows(j)("Activity_Expense_Plan_id").ToString()
                        Dim Expense_Sub_Activity_id As String = DTExpensePlanDetail.Rows(j)("Expense_Sub_Activity_id").ToString()
                        Dim Pay_Amount_Plan As String = DTExpensePlanDetail.Rows(j)("Pay_Amount_Plan").ToString()
                        If id = Expense_Plan_Id Then

                            Dim _lnqAEPD As New TbActivityExpensePlanDetailLinqDB
                            With _lnqAEPD
                                .HEADER_ID = HeaderID
                                .RECIPIENCE_ID = RecipienceID
                                .EXPENSE_SUB_ACTIVITY_ID = Expense_Sub_Activity_id
                                .PAY_AMOUNT_PLAN = Pay_Amount_Plan
                                .ACTIVITY_EXPENSE_PLAN_ID = _lnqAEP.ID

                                exc = .InsertData(Username, trans.Trans)

                            End With

                            If exc.IsSuccess = False Then
                                trans.RollbackTransaction()
                                ret.IsSuccess = False
                                ret.ErrorMessage = exc.ErrorMessage()
                                Return ret
                            End If

                        End If
                    Next
                Next
            End If

            trans.CommitTransaction()
            ret.IsSuccess = True
            ret.ErrorMessage = ""
        Catch ex As Exception
            trans.RollbackTransaction()
            ret.IsSuccess = False
            ret.ErrorMessage = ex.ToString()
        End Try
        Return ret

    End Function

    Public Function SaveHeaderID(lnqAEH As TbActivityExpenseHeaderLinqDB, Username As String, template_id As Long) As Double
        Dim ret As New ProcessReturnInfo
        Dim trans As New TransactionDB
        Dim aid As Double
        Try
            Dim exc As New ExecuteDataInfo
            Dim _lnq As New TbActivityExpenseHeaderLinqDB
            With _lnq
                .GetDataByPK(lnqAEH.ID, trans.Trans)
                .ACTIVITY_ID = lnqAEH.ACTIVITY_ID
                .PAYMENT_DATE_START = lnqAEH.PAYMENT_DATE_START
                .PAYMENT_DATE_END = lnqAEH.PAYMENT_DATE_END
                .PAYMENT_DETAIL = lnqAEH.PAYMENT_DETAIL
                If .ID > 0 Then
                    .ID = lnqAEH.ID
                    exc = .UpdateData(Username, trans.Trans)
                Else
                    .ID = lnqAEH.ID()
                    exc = .InsertData(Username, trans.Trans)
                    aid = .ID
                End If
            End With

            Dim dt As New DataTable
            Dim sql As String = "select sub_expense_id from TB_Template_Item where template_id = @_TEMPLATE_ID"

            Dim p(1) As SqlParameter
            p(0) = SqlDB.SetText("@_TEMPLATE_ID", template_id)
            dt = SqlDB.ExecuteTable(sql, p)

            If dt.Rows.Count > 0 Then

                If Not dt Is Nothing Then
                    For i As Integer = 0 To dt.Rows.Count - 1
                        Dim Pay_Plan_Detail As String = dt.Rows(i)("sub_expense_id").ToString()

                        Dim _lnqAEI As New TbActivityExpenseItemLinqDB

                        With _lnqAEI
                            .HEADER_ID = aid
                            .SUB_EXPENSE_ID = Pay_Plan_Detail

                            exc = .InsertData(Username, trans.Trans)
                        End With
                    Next
                End If
            End If


            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()

            End If
            trans.CommitTransaction()
            ret.IsSuccess = True
            ret.ErrorMessage = ""
        Catch ex As Exception
            trans.RollbackTransaction()
            ret.IsSuccess = False
            ret.ErrorMessage = ex.ToString()
        End Try
        Return aid
    End Function

    Public Function SaveHeader(lnqAEH As TbActivityExpenseHeaderLinqDB, Username As String) As ProcessReturnInfo
        Dim ret As New ProcessReturnInfo
        Dim trans As New TransactionDB
        Try
            Dim exc As New ExecuteDataInfo
            Dim _lnq As New TbActivityExpenseHeaderLinqDB
            With _lnq
                .GetDataByPK(lnqAEH.ID, trans.Trans)
                .ACTIVITY_ID = lnqAEH.ACTIVITY_ID
                .PAYMENT_DATE_START = lnqAEH.PAYMENT_DATE_START
                .PAYMENT_DATE_END = lnqAEH.PAYMENT_DATE_END
                If .ID > 0 Then
                    .ID = lnqAEH.ID
                    exc = .UpdateData(Username, trans.Trans)
                Else
                    .ID = lnqAEH.ID()
                    exc = .InsertData(Username, trans.Trans)
                End If
            End With
            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()

            End If
            trans.CommitTransaction()
            ret.IsSuccess = True
            ret.ErrorMessage = ""
        Catch ex As Exception
            trans.RollbackTransaction()
            ret.IsSuccess = False
            ret.ErrorMessage = ex.ToString()
        End Try
        Return ret
    End Function

    Public Function SaveExpenseDetailPay(lnqAEH As TbActivityExpenseHeaderLinqDB, DTExpenseDetail As DataTable, Username As String) As ProcessReturnInfo
        Dim ret As New ProcessReturnInfo
        Dim trans As New TransactionDB
        Try
            Dim exc As New ExecuteDataInfo
            Dim _lnq As New TbActivityExpenseHeaderLinqDB
            With _lnq
                .GetDataByPK(lnqAEH.ID, trans.Trans)
                .ACTIVITY_ID = lnqAEH.ACTIVITY_ID
                '.PAYMENT_DATE_ACTUAL = lnqAEH.PAYMENT_DATE_ACTUAL
                '.PAYMENT_DATE_PLAN = lnqAEH.PAYMENT_DATE_PLAN
                .PAYMENT_DETAIL = lnqAEH.PAYMENT_DETAIL
                If .ID > 0 Then
                    .ID = lnqAEH.ID
                    exc = .UpdateData(Username, trans.Trans)
                Else
                    .ID = lnqAEH.ID()
                    exc = .InsertData(Username, trans.Trans)
                End If
            End With
            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            Dim p(2) As SqlParameter
            p(0) = SqlDB.SetText("@_ACTIVITY_ID", lnqAEH.ACTIVITY_ID)
            p(1) = SqlDB.SetText("@_AEH_ID", lnqAEH.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Activity_Expense_Detail WHERE Header_id = @_AEH_ID", trans.Trans, p)
            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            For cnt As Integer = 0 To DTExpenseDetail.Rows.Count - 1
                Dim _activity_id As String = DTExpenseDetail.Rows(cnt)("activity_id").ToString
                Dim _recipince_id As String = DTExpenseDetail.Rows(cnt)("recipince_id").ToString
                Dim _recipince_type As String = DTExpenseDetail.Rows(cnt)("recipince_type").ToString
                Dim _expense_sub_id As String = DTExpenseDetail.Rows(cnt)("expense_sub_id").ToString
                Dim _amount As String = DTExpenseDetail.Rows(cnt)("amount").ToString
                Dim _amount_plan As String = DTExpenseDetail.Rows(cnt)("Pay_Amount_Plan").ToString 'Convert.ToDecimal(DTExpenseDetail.Rows(cnt)("amount"))
                'Dim lnqAED As New TbActivityExpenseDetailLinqDB
                'With lnqAED
                '    .HEADER_ID = _lnq.ID
                '    .RECIPIENCE_ID = _recipince_id
                '    .EXPENSE_SUB_ACTIVITY_ID = _expense_sub_id
                '    .PAY_AMOUNT_PLAN = Convert.ToDecimal(_amount_plan)
                '    If Not _amount Is Nothing AndAlso _amount <> "" Then
                '        .PAY_AMOUNT_ACTUAL = Convert.ToDecimal(_amount)
                '    End If
                '    If .ID > 0 Then
                '        exc = .UpdateData(Username, trans.Trans)
                '    Else
                '        exc = .InsertData(Username, trans.Trans)
                '    End If

                'End With
                If exc.IsSuccess = False Then
                    trans.RollbackTransaction()
                    ret.IsSuccess = False
                    ret.ErrorMessage = exc.ErrorMessage()
                    Return ret
                End If
            Next

            trans.CommitTransaction()
            ret.IsSuccess = True
            ret.ErrorMessage = ""
        Catch ex As Exception
            trans.RollbackTransaction()
            ret.IsSuccess = False
            ret.ErrorMessage = ex.ToString()
        End Try
        Return ret
    End Function

    Public Function DeleteExpenseDetail(ByVal id As Long) As Boolean
        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM tb_expense_detail WHERE expense_id=" & id
                .ExecuteNonQuery()
                .CommandText = "DELETE FROM tb_expense WHERE id=" & id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

#End Region

#Region "_Plan"
    Public Function GetList_Plan(ByVal id As Long, ByVal strSearch As String, ByVal Startdate As String, ByVal Enddate As String, ByVal status As String) As DataTable

        Dim SQL As String
        SQL = " select  id, plan_name , plan_name_th," & vbLf
        SQL &= "        start_date, end_date," & vbLf
        SQL &= "        (select [dbo].[GetFullThaiDate](start_date)) str_start_date, " & vbLf
        SQL &= "        (select [dbo].[GetFullThaiDate](end_date)) str_end_date, " & vbLf
        SQL &= "        isnull(active_status,'Y') active_status" & vbLf
        SQL &= "from TB_Plan where 1=1"

        If id <> 0 Then
            SQL &= " And id =" & id
        End If
        If strSearch.Trim <> "" Then
            SQL &= " And plan_name Like '%" & strSearch.Replace("'", "''") & "%' Or plan_name_th Like '%" & strSearch.Replace("'", "''") & "%'" & vbLf
        End If
        If Startdate <> "" And Enddate <> "" Then
            SQL &= " And start_date Between '" & Startdate & "' AND '" & Enddate & "'" & vbLf
        End If
        If status <> "" Then
            SQL &= " And active_status = '" & status & "'" & vbLf
        End If
        SQL &= " order by plan_name "

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function DeletePlan(ByVal id As Long) As Boolean
        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM TB_plan WHERE id =" & id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Public Function EditStatusAllY(ByVal id As Long, table As String) As Boolean
        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "UPDATE  " & table & "  SET ACTIVE_STATUS = 'N' WHERE ID=" & id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True

    End Function

    Public Function EditStatusAllN(ByVal id As Long, Table As String) As Boolean
        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "UPDATE " & Table & "  SET ACTIVE_STATUS = 'Y' WHERE ID=" & id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
#End Region

#Region "_Multilateral"
    Public Function GetList_Multilateral(id As Long, strSearch As String, status As String) As DataTable
        Dim SQL As String = " Select id,names,detail,sort,isnull(active_status,'Y') active_status  from tb_multilateral where 1=1 " & vbLf
        If id <> 0 Then
            SQL &= " And id =" & id & vbLf
        End If
        If strSearch.Trim <> "" Then
            SQL &= " And (names Like '%" & strSearch.Replace("'", "''") & "%' or  detail like '%" & strSearch.Replace("'", "''") & "%')" & vbLf
        End If
        If status <> "" Then
            SQL &= " And (active_status Like '" & status & "' )" & vbLf
        End If
        SQL &= " order by names " & vbLf
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetList_Top5Multilateral() As DataTable
        Dim SQL As String = " select Top 5 id,names,detail,sort from tb_multilateral " & vbLf
        SQL &= " order by id desc " & vbLf

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function DeleteMultilateral(ByVal id As Long) As Boolean
        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM TB_Multilateral WHERE ID=" & id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Public Function EditStatusMultiY(ByVal id As Long) As Boolean
        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "UPDATE TB_Multilateral  SET ACTIVE_STATUS = 'N' WHERE ID=" & id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Public Function EditStatusMultiN(ByVal id As Long) As Boolean
        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "UPDATE TB_Multilateral  SET ACTIVE_STATUS = 'Y' WHERE ID=" & id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
#End Region

#Region "_OECD"
    Public Function GetList_OECD(ByVal id As Long, ByVal strSearch As String) As DataTable
        Dim SQL As String = "SELECT id,oecd_name,oecd_name_Th,detail,ACTIVE_STATUS FROM tb_oecd WHERE 1=1 " & vbLf
        If id <> 0 Then
            SQL &= " And id =" & id & vbLf
        End If
        If strSearch.Trim <> "" Then
            SQL &= " And (oecd_name Like '%" & strSearch.Replace("'", "''") & "%' or  detail like '%" & strSearch.Replace("'", "''") & "%')" & vbLf
        End If
        SQL &= " order by oecd_name "
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function DeleteOECD(ByVal id As Long) As Boolean

        Dim SQL As String = "Select 'y' from from tb_project where oecd_id =" & id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            Return False
        End If

        SQL = "Select 'y' from tb_project where oecd_aid_type_id=" & id
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            Return False
        End If

        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM TB_OECD WHERE ID=" & id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True

    End Function
#End Region

#Region "_RegionOECD"
    Public Function GetList_RegionOECD(ByVal id As Long, ByVal strSearch As String) As DataTable
        Dim SQL As String = "SELECT id,region_name,region_name_th,ACTIVE_STATUS FROM tb_region WHERE 1=1 " & vbLf
        If id <> 0 Then
            SQL &= " And id =" & id & vbLf
        End If
        If strSearch.Trim <> "" Then
            SQL &= " And (region_name Like '%" & strSearch.Replace("'", "''") & "%')" & vbLf
        End If
        SQL &= " order by region_name "
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function DeleteRegionOECD(ByVal id As Long) As Boolean
        Dim SQL As String = "select 'y' from TB_Project where region_oecd_id=" & id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            Return False
        End If

        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM TB_Region WHERE ID=" & id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
#End Region

#Region "_RegionZone"

    Public Function GetList_RegionZoneOECD(ByVal id As Long) As DataTable

        Dim SQL As String = "select Rz.id,Rz.regionoda_id ,r.region_name,Rz.regionoda_name,Rz.regionoda_name_Th,Rz.ACTIVE_STATUS" & vbLf
        SQL &= " From TB_Region R LEFT JOIN TB_Regionzoneoda Rz on R.id = Rz.regionoda_id WHERE 1=1" & vbLf

        If id <> 0 Then
            SQL &= " And Rz.id =" & id & vbLf
        End If
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function
#End Region

#Region "_CooperationFramework"

    Public Function GetList_CooperationFramework(ByVal id As Long, strSearch As String) As DataTable
        Dim SQL As String = "SELECT id,fwork_name,detail,detail_th,ACTIVE_STATUS FROM tb_coperationframework WHERE 1=1 " & vbLf
        If id <> 0 Then
            SQL &= " And id =" & id & vbLf
        End If
        If strSearch.Trim <> "" Then
            SQL &= " And (fwork_name Like '%" & strSearch.Replace("'", "''") & "%' or detail Like '%" & strSearch.Replace("'", "''") & "%' )" & vbLf
        End If
        SQL &= " order by fwork_name "

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function DeleteCooperationFramework(ByVal id As Long) As Boolean

        Dim SQL As String = "Select 'y' from tb_project where cooperation_framework_id =" & id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            Return False
        End If

        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM TB_CoperationFramework WHERE ID=" & id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

#End Region

#Region "_CooperationType"

    Public Function GetList_CooperationType(ByVal id As Long, ByVal strSearch As String) As DataTable
        Dim SQL As String = "SELECT cptype_name,detail,detail_th,ACTIVE_STATUS FROM tb_coperationtype WHERE  1=1 " & vbLf
        If id <> 0 Then
            SQL &= " And id =" & id & vbLf
        End If
        If strSearch.Trim <> "" Then
            SQL &= " And (cptype_name Like '%" & strSearch.Replace("'", "''") & "%' or detail Like '%" & strSearch.Replace("'", "''") & "%' )" & vbLf
        End If
        SQL &= " order by cptype_name "
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function DeleteCooperationType(ByVal id As Long) As Boolean

        Dim SQL As String = "Select 'y' from tb_project where cooperation_type_id =" & id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            Return False
        End If

        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM TB_CoperationType WHERE ID=" & id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True

    End Function

#End Region

#Region "_Sector & Sub Sector"
    Public Function GetList_Sector(ByVal id As Long, ByVal strSearch As String) As DataTable
        Dim SQL As String = "select id,perposecat_name,code,ACTIVE_STATUS  FROM tb_purposecat WHERE  1=1 " & vbLf
        If id <> 0 Then
            SQL &= " And id =" & id & vbLf
        End If
        If strSearch.Trim <> "" Then
            SQL &= " And (perposecat_name Like '%" & strSearch.Replace("'", "''") & "%' or code Like '%" & strSearch.Replace("'", "''") & "%' )" & vbLf
        End If
        SQL &= " order by perposecat_name "
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function DeleteSector(ByVal id As Long) As Boolean

        Dim SQL As String = "Select 'y' from tb_activity where sector_id =" & id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            Return False
        End If

        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM TB_Purposecat WHERE ID=" & id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True

    End Function

    Public Function GetList_SubSector(ByVal id As Long, ByVal strSearch As String) As DataTable
        Dim SQL As String = "SELECT  tb_purpose.id,Name,Description,DAC5code,CRScode,CategoryID " & vbLf
        SQL &= ", tb_purposecat.perposecat_name,tb_purpose.ACTIVE_STATUS" & vbLf
        SQL &= " From tb_purpose" & vbLf
        SQL &= " Left JOIN tb_purposecat  On tb_purposecat.id=tb_purpose.CategoryID WHERE 1=1" & vbLf
        If id <> 0 Then
            SQL &= " And tb_purpose.id =" & id & vbLf
        End If
        If strSearch.Trim <> "" Then
            SQL &= " And (Name Like '%" & strSearch.Replace("'", "''") & "%' or Description Like '%" & strSearch.Replace("'", "''") & "%' or tb_purposecat.perposecat_name Like '%" & strSearch.Replace("'", "''") & "%')" & vbLf
        End If
        SQL &= " order by Name "

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function DeleteSubSector(ByVal id As Long) As Boolean

        Dim SQL As String = "Select 'y' from tb_activity where sub_sector_id =" & id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            Return False
        End If

        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM TB_Purpose WHERE ID=" & id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Public Function DeleteExpenseHeaderDetail(ByVal id As Long) As Boolean

        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM TB_Activity_Expense_Plan_Detail WHERE Header_id=" & id
                .ExecuteNonQuery()
                .CommandText = "DELETE FROM TB_Activity_Expense_Plan WHERE Header_id=" & id
                .ExecuteNonQuery()
                .CommandText = "DELETE FROM TB_Activity_Expense_Actual_Detail WHERE Header_id=" & id
                .ExecuteNonQuery()
                .CommandText = "DELETE FROM TB_Activity_Expense_Actual WHERE Header_id=" & id
                .ExecuteNonQuery()
                .CommandText = "DELETE FROM TB_Activity_Expense_Item WHERE header_id=" & id
                .ExecuteNonQuery()
                .CommandText = "DELETE FROM TB_Activity_Expense_Header WHERE id=" & id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True

    End Function
#End Region

#Region "_Component"

    Public Function GetList_Component(ByVal id As Long, ByVal strSearch As String) As DataTable
        Dim SQL As String = "SELECT id,component_name,detail,allowcatetype,disbursetype,ACTIVE_STATUS FROM tb_component WHERE 1=1 " & vbLf
        If id <> 0 Then
            SQL &= " And id =" & id & vbLf
        End If
        If strSearch.Trim <> "" Then
            SQL &= " And (component_name Like '%" & strSearch.Replace("'", "''") & "%' or detail Like '%" & strSearch.Replace("'", "''") & "%' )" & vbLf
        End If
        SQL &= " order by component_name " & vbLf

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function DeleteComponent(ByVal id As Long) As Boolean

        Dim SQL As String = "select 'y' from TB_Activity_Component where component_id=" & id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            Return False
        End If

        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM TB_component WHERE ID=" & id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True

    End Function

#End Region

#Region "_Inkind"

    Public Function GetList_Inkind(ByVal id As Long, ByVal strSearch As String) As DataTable
        Dim SQL As String = "SELECT id,inkind_name,detail,ACTIVE_STATUS FROM tb_inkind WHERE 1=1 " & vbLf
        If id <> 0 Then
            SQL &= " And id =" & id & vbLf
        End If
        If strSearch.Trim <> "" Then
            SQL &= " And (inkind_name Like '%" & strSearch.Replace("'", "''") & "%' or detail Like '%" & strSearch.Replace("'", "''") & "%' )" & vbLf
        End If
        SQL &= " order by inkind_name " & vbLf

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function DeleteInkind(ByVal id As Long) As Boolean

        Dim SQL As String = "select 'y' from tb_activity_inkind where inkind_id=" & id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            Return False
        End If

        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM TB_Inkind WHERE ID=" & id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

#End Region

#Region "_CountryGroup"

    Public Function GetList_CountryGroup(ByVal id As Long, ByVal strSearch As String) As DataTable
        Dim SQL As String = " SELECT id,countrygroup_id,countrygroup_name,detail,ACTIVE_STATUS FROM tb_countrygroup WHERE 1=1 " & vbLf
        If id <> 0 Then
            SQL &= " And id = " & id & vbLf
        End If
        If strSearch.Trim <> "" Then
            SQL &= " And (countrygroup_name Like '%" & strSearch.Replace("'", "''") & "%' or  detail like '%" & strSearch.Replace("'", "''") & "%')" & vbLf
        End If
        SQL &= " order by countrygroup_name "

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function DeleteCountryGroup(ByVal id As Long) As Boolean

        Dim SQL As String = "select 'y' from TB_OU_CountryGroup where country_group_id=" & id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            Return False
        End If

        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM TB_Countrygroup WHERE ID=" & id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True

    End Function

#End Region

#Region "Menu&Authorize"

    Public Function GetList_AllMenu() As DataTable
        Dim SQL As String = " select * from tb_menu"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetList_Menu(node_id As String) As DataTable
        Dim SQL As String = " select distinct menu_group as menu_name,'g' menu_type from tb_menu "

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)

        Dim dttmp As New DataTable
        dttmp.Columns.Add("seq")
        dttmp.Columns.Add("id")
        dttmp.Columns.Add("menu_name")
        dttmp.Columns.Add("menu_type")
        dttmp.Columns.Add("is_save")
        dttmp.Columns.Add("is_view")
        dttmp.Columns.Add("is_na")
        Dim dr As DataRow
        Dim cnt As Integer = 0
        For i As Integer = 0 To DT.Rows.Count - 1
            dr = dttmp.NewRow

            Dim id As String = "0"
            Dim menu_name As String = DT.Rows(i)("menu_name").ToString
            Dim menu_type As String = DT.Rows(i)("menu_type").ToString
            Dim dtitem As DataTable = GetList_MenuByGroup(menu_name, node_id)

            If menu_name <> "" Then
                dr("seq") = cnt + 1
                dr("id") = id
                dr("menu_name") = menu_name
                dr("menu_type") = menu_type
                dttmp.Rows.Add(dr)
                cnt += 1
            End If

            For j As Integer = 0 To dtitem.Rows.Count - 1
                id = dtitem.Rows(j)("id").ToString
                menu_name = dtitem.Rows(j)("menu_name").ToString
                menu_type = dtitem.Rows(j)("menu_type").ToString

                Dim is_save As String = dtitem.Rows(j)("is_save").ToString
                Dim is_view As String = dtitem.Rows(j)("is_view").ToString
                Dim is_na As String = dtitem.Rows(j)("is_na").ToString

                dr = dttmp.NewRow
                dr("seq") = cnt + 1
                dr("id") = id
                dr("menu_name") = menu_name
                dr("menu_type") = menu_type
                dr("is_save") = is_save
                dr("is_view") = is_view
                dr("is_na") = is_na

                If is_save = "" And is_view = "" And is_na = "" Then
                    dr("is_na") = "Y"
                End If

                dttmp.Rows.Add(dr)
                cnt += 1
            Next
        Next
        Return dttmp
    End Function

    Public Function GetList_MenuByGroup(group_name As String, node_id As String) As DataTable
        Dim SQL As String = " select m.id,menu_name,'m' menu_type,isnull(is_save,'') is_save," & vbLf
        SQL &= " isnull(is_view,'') is_view,isnull(is_na,'') is_na from tb_menu m left join " & vbLf
        SQL &= " (select id,node_id,node_type,menu_id,is_save,is_view,is_na from TB_Authorize where node_id='" & node_id.Replace("'", "''") & "') a on m.id = a.menu_id " & vbLf
        SQL &= " where isnull(menu_group,'') ='" & group_name.Replace("'", "''") & "'"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Private Function SaveNodeAuthorize(UserName As String, Node_ID As String, trans As TransactionDB, dt As DataTable) As ProcessReturnInfo
        Dim ret As New ProcessReturnInfo
        Try
            Dim exc As New ExecuteDataInfo
            For i As Integer = 0 To dt.Rows.Count - 1
                Dim id As String = dt.Rows(i)("id").ToString

                Dim is_save As String = dt.Rows(i)("is_save").ToString
                Dim is_view As String = dt.Rows(i)("is_view").ToString
                Dim is_na As String = dt.Rows(i)("is_na").ToString

                Dim lnq As New TbAuthorizeLinqDB
                With lnq
                    Dim p(2) As SqlParameter
                    p(0) = SqlDB.SetBigInt("@_NODE_ID", Node_ID)
                    p(1) = SqlDB.SetBigInt("@_MENU_ID", id)
                    .ChkDataByWhere("node_id=@_NODE_ID and menu_id=@_MENU_ID", trans.Trans, p)

                    .NODE_ID = Node_ID
                    .MENU_ID = id
                    .IS_SAVE = is_save
                    .IS_VIEW = is_view
                    .IS_NA = is_na
                End With

                If lnq.ID > 0 Then
                    exc = lnq.UpdateData(UserName, trans.Trans)
                Else
                    exc = lnq.InsertData(UserName, trans.Trans)
                End If

                If exc.IsSuccess = False Then
                    ret.IsSuccess = False
                    ret.ErrorMessage = exc.ErrorMessage()
                    Exit For
                End If
                lnq = Nothing
            Next

            If exc.IsSuccess = True Then
                'Get Child Node and Save
                Dim NodeDT As DataTable = GetList_ChildNode(Node_ID)
                If NodeDT.Rows.Count > 0 Then
                    For Each NodeDr As DataRow In NodeDT.Rows
                        ret = SaveNodeAuthorize(UserName, NodeDr("node_id"), trans, dt)
                        If ret.IsSuccess = False Then
                            Exit For
                        End If
                    Next
                Else
                    ret.IsSuccess = True
                    ret.ErrorMessage = ""
                End If
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage
            End If
        Catch ex As Exception
            ret.IsSuccess = False
            ret.ErrorMessage = ex.ToString()
        End Try
        Return ret
    End Function

    Public Function SaveAuthorize(dt As DataTable, Node_ID As String, Username As String) As ProcessReturnInfo
        Dim trans As New TransactionDB
        Dim ret As New ProcessReturnInfo
        ret = SaveNodeAuthorize(Username, Node_ID, trans, dt)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        Return ret
    End Function

    Public Function GetList_Authorize(ByVal node_id As String) As DataTable
        Dim SQL As String = " select id,node_id,menu_id,is_save,is_view,is_na from TB_Authorize "
        SQL &= " where node_id In (Select node_id from GetAllChildNode('" & node_id.Replace("'", "''") & "'))"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetList_AuthorizeByNodeID(ByVal node_id As String) As DataTable
        Dim SQL As String = " select id,node_id,menu_id,is_save,is_view,is_na from TB_Authorize "
        SQL &= " where node_id = '" & node_id.Replace("'", "''") & "'"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

#End Region

#Region "_DDL"

    Public Sub BindData_dll(ByRef ddl As DropDownList, ByVal UserName As String, ByRef DataTableName As DataTable, ByVal SelectTitle As String, ByVal itemName As String)
        Dim dt As DataTable = DataTableName
        ddl.Items.Clear()
        ddl.Items.Add(New ListItem(SelectTitle, ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item(itemName), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    'Public Sub Bind_DDL_GroupBudget(ByRef ddl As DropDownList)
    '    Dim sql As String = " select id,group_name,active_status from tb_budget_group where isnull(active_status,'Y')='Y' order by group_name"
    '    Dim dt As DataTable = SqlDB.ExecuteTable(sql)

    '    ddl.Items.Clear()
    '    ddl.Items.Add(New ListItem("Select Group Budget", ""))
    '    For i As Integer = 0 To dt.Rows.Count - 1
    '        Dim Item As New ListItem(dt.Rows(i).Item("group_name"), dt.Rows(i).Item("id"))
    '        ddl.Items.Add(Item)
    '    Next
    '    ddl.SelectedIndex = 0
    'End Sub

    Public Sub Bind_DDL_Recipience(ByRef ddl As DropDownList)
        Dim SQL As String = " select * from vw_PSN_IN_Country where person_type = 1"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Recipience", ""))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("name_th"), DT.Rows(i).Item("node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_CountryByRecipience(ByRef ddl As DropDownList, ByVal node_ID As String)
        Dim SQL As String = " select * from vw_PSN_IN_Country where person_type = 1"
        SQL &= " and node_id = '" & node_ID & "'"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)

        ddl.Items.Clear()
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("name_th_OU"), DT.Rows(i).Item("node_id_OU"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_DisbursementType(ByRef ddl As DropDownList)
        Dim DT As New DataTable
        DT.Columns.Add("id")
        DT.Columns.Add("name")

        Dim DR As DataRow
        DR = DT.NewRow
        DR("id") = "1"
        DR("name") = "Loan"
        DT.Rows.Add(DR)

        DR = DT.NewRow
        DR("id") = "2"
        DR("name") = "Grant"
        DT.Rows.Add(DR)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Disbursement Type", ""))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("name"), DT.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0

    End Sub

    Public Sub Bind_DDL_CountryGroup(ByRef ddl As DropDownList)
        Dim SQL As String = " select id,countrygroup_name from tb_countrygroup where isnull(active_status,'Y') = 'Y' order by countrygroup_name"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Country Group", ""))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("countrygroup_name"), DT.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_CountryType(ByRef ddl As DropDownList)
        Dim SQL As String = "Select id,countrytypename from web_countrytype order by countrytypename"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("-- เลือก --", ""))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("countrytypename"), DT.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_RegionZone(ByRef ddl As DropDownList)
        Dim SQL As String = "select id,regionoda_name from tb_regionzoneoda order by regionoda_name"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Country Zone", ""))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("regionoda_name"), DT.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_RegionZone_By_Region(ByRef ddl As DropDownList, ByVal regionoda_id As Integer)
        Dim SQL As String = "select * from tb_regionzoneoda where regionoda_id=" & CInt(regionoda_id) & "  order by regionoda_name"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Country Zone", ""))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("regionoda_name"), DT.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Region(ByRef ddl As DropDownList)
        Dim SQL As String = "select id,region_name from tb_region  where isnull(active_status,'Y') = 'Y' order by region_name"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Region", ""))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("region_name"), DT.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Region_forSearch(ByRef ddl As DropDownList)
        Dim SQL As String = "select id,region_name from tb_region order by region_name"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Region", ""))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("region_name"), DT.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_CoperationFrameWork(ByRef ddl As DropDownList)
        Dim SQL As String = "select id,detail + '(' + fwork_name + ') ' as fwork_name from tb_coperationframework  where isnull(active_status,'Y') = 'Y' order by fwork_name"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Coperation Framework", ""))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("fwork_name"), DT.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Coperationtype(ByRef ddl As DropDownList)
        Dim SQL As String = "select id,detail + '(' + cptype_name + ')' as cptype_name from tb_coperationtype where isnull(active_status,'Y') = 'Y' order by cptype_name"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Coperation Type", ""))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("cptype_name"), DT.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_OECD(ByRef ddl As DropDownList)
        Dim SQL As String = "select id,oecd_name from tb_oecd where isnull(active_status,'Y') = 'Y' order by oecd_name"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select OECD", ""))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("oecd_name"), DT.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DLL_Plan(ByRef ddl As DropDownList)
        Dim SQL As String = "select id,plan_name from TB_Plan where isnull(active_status,'Y') = 'Y'"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Plan", ""))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("plan_name"), DT.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Organize(ByRef ddl As DropDownList)
        Dim SQL As String = "select node_id,name_en from TB_OU_Organize where isnull(active_status,'Y') = 'Y' order by name_th"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Organize", ""))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("name_en"), DT.Rows(i).Item("node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_ChildsOrganize(ByRef ddl As DropDownList, ByVal parent_id As String)
        Dim SQL As String = "select node_id,name_th from TB_OU_Organize where isnull(active_status,'Y') = 'Y' "
        If parent_id <> "" Then
            SQL &= " and parent_id= '" & parent_id.Replace("'", "''") & "'"
        End If
        SQL &= " order by name_th"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Organize", ""))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("name_th"), DT.Rows(i).Item("node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_OrganizeByCountry(ByRef ddl As DropDownList, ByVal country_id As String)
        Dim DT As DataTable = GetOrganizeUnitByNodeID(country_id, OU.Organize)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Organize", ""))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("name_th"), DT.Rows(i).Item("node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Function Bind_TableFormOrganize(dt As DataTable, ByVal dtou As DataTable) As DataTable
        Dim SQL As String = ""
        Dim DTOrganize As New DataTable
        Dim CheckId As String = "False"
        With DTOrganize
            .Columns.Add("node_id")
            .Columns.Add("name_th")
            .Columns.Add("name_en")
            .Columns.Add("parent_id")
        End With
        Dim dr As DataRow
        If dt.Rows.Count > 0 Then
            For i As Integer = 0 To dt.Rows.Count - 1
                dr = DTOrganize.NewRow
                dr("node_id") = dt.Rows(i).Item("node_id")
                dr("name_th") = dt.Rows(i).Item("name_th")
                dr("name_en") = dt.Rows(i).Item("name_en")
                dr("parent_id") = dt.Rows(i).Item("parent_id")
                DTOrganize.Rows.Add(dr)
                For j As Integer = 0 To dtou.Rows.Count - 1
                    If dt.Rows(i).Item("node_id").ToString = dtou.Rows(j).Item("parent_id").ToString Then
                        For k As Integer = 0 To dt.Rows.Count - 1
                            If dtou.Rows(j).Item("node_id").ToString = dt.Rows(k).Item("node_id").ToString Then
                                CheckId = "True"
                            End If
                        Next
                        If CheckId = "True" Then
                            CheckId = "False"
                        Else
                            dr = DTOrganize.NewRow
                            dr("node_id") = dtou.Rows(j).Item("node_id")
                            dr("name_th") = dtou.Rows(j).Item("name_th")
                            dr("name_en") = dtou.Rows(j).Item("name_en")
                            dr("parent_id") = dtou.Rows(j).Item("parent_id")
                            DTOrganize.Rows.Add(dr)
                            CheckId = "False"
                        End If

                    End If
                Next

            Next
        End If
        Return DTOrganize
    End Function

    Public Sub Bind_DDL_OrganizeFromCountry(ByRef ddl As DropDownList, country_id As String)
        Dim SQL As String = ""

        Dim DT As New DataTable

        Dim DTOrganize As New DataTable
        'Dim dt1 As New DataTable
        Dim DA As SqlDataAdapter
        '--------------------------------
        SQL = "select node_id,name_th,name_en,parent_id from TB_OU_Organize where parent_id = '" & country_id.ToString & "'"
        DA = New SqlDataAdapter(SQL, ConnectionString)
        Dim dttemp As New DataTable
        DA.Fill(dttemp)
        '--------------------------------
        SQL = "select node_id,name_th,name_en,parent_id from TB_OU_Organize"
        DA = New SqlDataAdapter(SQL, ConnectionString)
        Dim dtou As New DataTable
        DA.Fill(dtou)
        '-------------- ใครเขียนวะเนี่ยะ ---------------
        DT = Bind_TableFormOrganize(dttemp, dtou)
        DTOrganize = Bind_TableFormOrganize(DT, dtou)
        'Dim a As Integer = DTOrganize.Rows.Count
        'dt1 = DTOrganize


        'Dim dr As DataRow
        'For z As Integer = 0 To dt1.Rows.Count - 1
        '    dr = DT.NewRow
        '    dr("node_id") = dt1.Rows(z).Item("node_id")
        '    dr("name_th") = dt1.Rows(z).Item("name_th")
        '    dr("name_en") = dt1.Rows(z).Item("name_en")
        '    dr("parent_id") = dt1.Rows(z).Item("parent_id")
        '    DT.Rows.Add(dr)
        'Next

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Organize", ""))
        For i As Integer = 0 To DTOrganize.Rows.Count - 1
            Dim Item As New ListItem(DTOrganize.Rows(i).Item("name_th"), DTOrganize.Rows(i).Item("node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Person(ByRef ddl As DropDownList, person_type As String)
        Dim sql As String = "select node_id,name_th from TB_OU_Person where isnull(active_status,'Y') = 'Y' and person_type = " & person_type & " order by name_th"
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Person", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("name_th"), dt.Rows(i).Item("node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Multilateral(ByRef ddl As DropDownList)
        Dim sql As String = "select id,detail + '(' + names + ')' as names from tb_multilateral where isnull(active_status,'Y') = 'Y' order by names"
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Multilateral", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("names"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Component(ByRef ddl As DropDownList)
        Dim sql As String = "select id,component_name from tb_component where isnull(active_status,'Y') = 'Y' order by component_name"
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Component", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("component_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Country(ByRef ddl As DropDownList)
        Dim sql As String = "select node_id,name_en from TB_OU_Country where isnull(active_status,'Y') = 'Y' order by name_en"
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Country", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("name_en"), dt.Rows(i).Item("node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_CoFunding(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = 0)
        Dim sql As String = "select node_id,name_en,'c' datatype from TB_OU_Country where isnull(active_status,'Y') = 'Y' "
        sql &= " union "
        sql &= " Select  node_id,isnull(abbr_name_en + '-'  ,'')+ name_en as name_en,'o' datatype  "
        sql &= " From TB_OU_Organize where isnull(active_status,'Y') = 'Y' "
        sql &= " order by datatype ,name_en"
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("...", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("name_en"), dt.Rows(i).Item("node_id"))
            ddl.Items.Add(Item)
            If SelectedValue = dt.Rows(i).Item("node_id") Then
                ddl.SelectedIndex = i
            End If
        Next
        'ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Project(ByRef ddl As DropDownList)
        Dim sql As String = "select id,project_name from TB_Project order by project_name"
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Project", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("project_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Perposecat(ByRef ddl As DropDownList)
        Dim sql As String = "select id,perposecat_name from tb_purposecat where isnull(active_status,'Y') = 'Y' order by perposecat_name"
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Sector", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("perposecat_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Inkind(ByRef ddl As DropDownList)
        Dim sql As String = "select id,inkind_name from tb_inkind where isnull(active_status,'Y') = 'Y' order by inkind_name"
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Inkind", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("inkind_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Prefix_th(ByRef ddl As DropDownList)
        Dim sql As String = "select id,prefix_name_th from tb_prefix where isnull(active_status,'Y')='Y' order by prefix_name_th"
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Prefix", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("prefix_name_th"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Prefix_en(ByRef ddl As DropDownList)
        Dim sql As String = "select id,prefix_name_en from tb_prefix where isnull(active_status,'Y')='Y' order by prefix_name_en"
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Prefix", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("prefix_name_en"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Perpose(ByRef ddl As DropDownList, ByVal sectorid As String)
        Dim sql As String = "select id,name from tb_purpose where isnull(active_status,'Y') = 'Y'"
        If sectorid <> "" Then
            sql &= " and CategoryID=" & sectorid
        End If
        sql &= " order by name"
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Sub Sector", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_SubBudget(ByRef ddl As DropDownList, ByVal budget_group_id As String)
        Dim sql As String = " select id,sub_name from TB_Budget_Sub where isnull(active_status,'Y')='Y' and budget_group_id='" & budget_group_id.Replace("'", "''") & "' order by sub_name"
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Sub Budget", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("sub_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_BudgetGroup(ByRef ddl As DropDownList)
        Dim sql As String = " select id,group_name from TB_Budget_Group where isnull(active_status,'Y')='Y' order by group_name"
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Budget Group", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("group_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_BudgetGroupForSearch(ByRef ddl As DropDownList)
        Dim sql As String = " select id,group_name from TB_Budget_Group order by group_name"
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Budget Group", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("group_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Year(ByRef ddl As DropDownList, Optional IsAddFirstIndex As Boolean = False)
        Dim _year As Integer = Now.Date.Year
        If _year < 2500 Then _year += 543

        If IsAddFirstIndex Then
            ddl.Items.Add(New ListItem("All", ""))
        End If
        For i As Integer = 0 To 19
            Dim Item As New ListItem(_year - i, _year - i)
            ddl.Items.Add(Item)
        Next
    End Sub

    Public Sub Bind_DDL_ExpenseGroup(ByRef ddl As DropDownList)
        Dim sql As String = " select id,group_name from TB_Expense_Group where isnull(active_status,'Y')='Y' order by group_name"
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Expense Group", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("group_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_ExpenseSub(ByRef ddl As DropDownList)
        Dim sql As String = " select id,sub_name from TB_Expense_Sub where isnull(active_status,'Y')='Y' order by sub_name"
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Expense", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("sub_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_ExpenseTemplate(ByRef ddl As DropDownList, Optional IsAddFirstIndex As Boolean = False)
        Dim sql As String = " select id,template_name from TB_Template"
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        If IsAddFirstIndex = True Then
            ddl.Items.Add(New ListItem("Select Template", ""))
        End If

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("template_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_ProjectType(ByRef ddl As DropDownList)
        Dim dt As New DataTable
        dt.Columns.Add("ID")
        dt.Columns.Add("ProjectType")

        Dim dr As DataRow
        dr = dt.NewRow
        dr("ID") = CInt(Project_Type.Project)
        dr("ProjectType") = "Project"
        dt.Rows.Add(dr)

        dr = dt.NewRow
        dr("ID") = CInt(Project_Type.NonProject)
        dr("ProjectType") = "Non Project"
        dt.Rows.Add(dr)

        dr = dt.NewRow
        dr("ID") = CInt(Project_Type.Loan)
        dr("ProjectType") = "Loan"
        dt.Rows.Add(dr)

        dr = dt.NewRow
        dr("ID") = CInt(Project_Type.Contribuition)
        dr("ProjectType") = "Contribuition"
        dt.Rows.Add(dr)

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("ProjectType"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    '--Distinct เฉพ่าะประเทศที่อยู่ใน List
    Public Sub Bind_DDL_Country_By_rptAidByCountrySummary(ByRef ddl As DropDownList, Optional IsAddFirstIndex As Boolean = False)
        Dim sql As String = " select DISTINCT country_node_id,country_name_th  from vw_Project_Component "
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        If IsAddFirstIndex = True Then
            ddl.Items.Add(New ListItem("Select Country", ""))
        End If
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("country_name_th"), dt.Rows(i).Item("country_node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0


    End Sub

    '--Distinct เฉพ่าะประเทศที่อยู่ใน List
    Public Sub Bind_DDL_Country_By_Expense_Activity_GroupBy_Country(ByRef ddl As DropDownList, Optional IsAddFirstIndex As Boolean = False)
        Dim sql As String = ""
        sql &= " Select DISTINCT node_id_OU country_node_id, name_th_OU country_name_th " & vbLf
        sql &= " From vw_Expense_Activity_GroupBy_Country " & vbLf
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        If IsAddFirstIndex = True Then
            ddl.Items.Add(New ListItem("Select Country", ""))
        End If

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("country_name_th"), dt.Rows(i).Item("country_node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0


    End Sub

    Public Sub Bind_DDL_Country_By_Expense_Activity_GroupBy_CountryNew(ByRef ddl As DropDownList, Optional IsAddFirstIndex As Boolean = False)
        Dim sql As String = ""
        sql &= " Select DISTINCT node_id_OU country_node_id, name_th_OU country_name_th " & vbLf
        sql &= " From vw_Activity_Recipic_3Type " & vbLf
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        If IsAddFirstIndex = True Then
            ddl.Items.Add(New ListItem("Select Country", ""))
        End If

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("country_name_th"), dt.Rows(i).Item("country_node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0


    End Sub

    '--Distinct เฉพ่าะประเทศที่อยู่ใน List
    Public Sub Bind_DDL_Country_By_rptSummary_ByCountry(ByRef ddl As DropDownList, Optional IsAddFirstIndex As Boolean = False)
        Dim sql As String = ""

        sql += " 	SELECT DISTINCT country_node_id,country_name_th FROM (																								  " & VbLf
        sql += " 	 SELECT TB_Detail_By_Project.budget_year,country_node_id,country_name_th									  " & VbLf
        sql += " 		   ,COUNT(project_id) QTY_Project						--จำนวนโปรเจคทั้ง ภายใต้และนอกโครงการ				     " & VbLf
        sql += " 		   ,vw_Recipient_In_Year.amout_person QTY_Recipient		--จำนวนทุน									     " & VbLf
        sql += " 		   ,SUM(Total_Amount) SUM_Amount						--มูลค่า											  " & VbLf
        sql += " 		FROM (																									  " & VbLf
        sql += " 			SELECT budget_year																					  " & VbLf
        sql += " 				  ,vw_Summary_Aid_By_Country.project_id															  " & VbLf
        sql += " 				  ,vw_Summary_Aid_By_Country.project_name														  " & VbLf
        sql += " 				  ,tb_project.project_type																		  " & VbLf
        sql += " 				  ,country_node_id																				  " & VbLf
        sql += " 				  ,country_name_th																				  " & VbLf
        sql += " 				  ,commitment_budget																			  " & VbLf
        sql += " 				  ,convert(decimal ,Bachelor)+convert(decimal,Training)+convert(decimal,Expert)+convert(decimal,Volunteer)+convert(decimal,Equipment)+convert(decimal,Other) Total_Amount	  " & VbLf
        sql += "																														  " & VbLf
        sql += " 			  FROM  vw_Summary_Aid_By_Country																			  " & VbLf
        sql += " 			  left JOIN tb_project ON tb_project.id=vw_Summary_Aid_By_Country.project_id								  " & VbLf
        sql += " 	 ) TB_Detail_By_Project																								  " & VbLf
        sql += " 	 LEFT JOIN vw_Recipient_In_Year ON  vw_Recipient_In_Year.budget_year = TB_Detail_By_Project.budget_year				  " & VbLf
        sql += " 										AND vw_Recipient_In_Year.node_id_OU = TB_Detail_By_Project.country_node_id		  " & VbLf
        sql += " 																														  " & VbLf
        sql += "  																														  " & VbLf
        sql += " 	 GROUP BY  TB_Detail_By_Project.budget_year,country_node_id,country_name_th ,vw_Recipient_In_Year.amout_person		    " & VbLf
        sql += " 	 ) AS TB																											    " & VbLf
        sql += " 	 WHERE SUM_Amount>0																									    " & VbLf
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        If IsAddFirstIndex = True Then
            ddl.Items.Add(New ListItem("Select Country", ""))
        End If

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("country_name_th"), dt.Rows(i).Item("country_node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    '--Componance
    Public Sub Bind_DDL_Componance(ByRef ddl As DropDownList, Optional IsAddFirstIndex As Boolean = False)
        Dim sql As String = ""
        sql &= " SELECT * FROM vw_Component Order by Desc_name  " & VbLf
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        If IsAddFirstIndex = True Then
            ddl.Items.Add(New ListItem("Select", ""))
        End If
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("Desc_name"), dt.Rows(i).Item("component_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Year_By_rptAidByCountrySummary(ByRef ddl As DropDownList, Optional IsAddFirstIndex As Boolean = False)
        '--Distinct เฉพาะปีที่อยู่ใน List
        Dim sql As String = " select distinct(budget_year) FROM vw_Summary_Aid "
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        If IsAddFirstIndex = True Then
            ddl.Items.Add(New ListItem("Select Year", ""))
        End If

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("budget_year"), dt.Rows(i).Item("budget_year"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0


    End Sub

    Public Sub Bind_DDL_Country_name_th_By_rptCountryReceiveAidEveryYear(ByRef ddl As DropDownList, Optional IsAddFirstIndex As Boolean = False)
        '--Distinct เฉพาะประเทศที่อยู่ใน List
        Dim sql As String = " Select distinct country_name_th ,AP.country_node_id from vw_Activity_Disbursement AD inner join vw_Activity_PSN AP On AD.activity_id = AP.activity_id  order by AP.Country_name_th DESC"
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        If IsAddFirstIndex = True Then
            ddl.Items.Add(New ListItem("Select Country", ""))
        End If

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("country_name_th"), dt.Rows(i).Item("country_node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0


    End Sub

    Public Sub Bind_DDL_Country_name_th_By_rptAidByCountrySummary(ByRef ddl As DropDownList, Optional ByVal IsAddFirstIndex As Boolean = False)
        '--Distinct เฉพาะประเทศที่อยู่ใน List
        Dim sql As String = "select distinct(country_node_id),country_name_th FROM vw_Summary_Aid "
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        If IsAddFirstIndex = True Then
            ddl.Items.Add(New ListItem("Select Country", ""))
        End If

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("country_name_th"), dt.Rows(i).Item("country_node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0


    End Sub

    Public Sub Bind_DDL_Sector_th_By_rptAidByCountrySummary(ByRef ddl As DropDownList, Optional IsAddFirstIndex As Boolean = False)
        '--Distinct เฉพาะสาขาที่อยู่ใน List
        Dim sql As String = "select distinct(sector_id),perposecat_name FROM vw_Summary_Aid  where sector_id IS NOT NULL	"
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        If IsAddFirstIndex = True Then
            ddl.Items.Add(New ListItem("Select Sector", ""))
        End If

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("perposecat_name"), dt.Rows(i).Item("sector_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Year_Reprot_AidSummary(ByRef ddl As DropDownList)
        Dim sql As String = " select id,group_name from TB_Budget_Group where isnull(active_status,'Y')='Y' order by group_name"
        Dim DA As New SqlDataAdapter(sql, ConnectionString)
        Dim dt As New DataTable
        DA.Fill(dt)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Budget Group", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("group_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

#End Region

#Region "_Project"
    Public Function GetProjectID() As String
        Dim project_id As String = Convert.ToDateTime(DateTime.Now).ToString("yyyy", New System.Globalization.CultureInfo("en-US")) & "00001"
        Dim SQL As String = ""
        SQL = " Select isnull(substring(Convert(varchar(10), getdate(), 112), 1, 4) + '' + RIGHT('00000'+ convert(varchar,convert(int,(substring(max(isnull(project_id,0)),5,5)))+1),5),'') project_id" & vbLf
        SQL &= " From tb_project " & vbLf
        SQL &= " Where year(getdate()) = substring(project_id, 1, 4) " & vbLf
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)

        If DT.Rows.Count > 0 AndAlso DT.Rows(0)("project_id").ToString <> "" Then
            project_id = DT.Rows(0)("project_id").ToString()
        End If

        Return project_id
    End Function

    Function DeleteAdministrative_Cost(ByVal ID_Activity As String) As Boolean
        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM TB_Activity_Type_Administrative_Cost WHERE ID=" & ID_Activity
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Function DeleteActivityDisbursement(ByVal ID_Activity As String) As Boolean
        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM TB_Activity_Type_Disbursement WHERE ID=" & ID_Activity
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Function DeleteActivityCountryRe(ByVal ID_Activity As String) As Boolean
        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM TB_Country_Recipient WHERE ID=" & ID_Activity
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Public Function GetDataDisbursement_FromQurey(ByVal activity_id As String) As DataTable
        Dim SQL As String = " select id,activity_id,payment_date as date_payment,amount from TB_Activity_Type_Disbursement" & vbLf
        SQL &= " where activity_id = " & activity_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetDataCountryRecipent_FromQurey(ByVal activity_id As String) As DataTable
        Dim SQL As String = "select id,activity_id,country_node_id,amout_person from TB_Country_Recipient" & vbLf
        SQL &= " where activity_id = " & activity_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetDataActivityCountryRecipent_FromQurey(ByVal activity_id As String) As DataTable
        Dim SQL As String = ""
        SQL = "select * from TB_Activity_Country " & vbLf
        SQL &= "where activity_id = " & activity_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetDataExpenseActualList_FromQurey(ByVal header_id As String, ByVal ReId As String) As DataTable
        Dim SQL As String = ""
        SQL = "Select activity_id,EP.id,EP.Payment_Actual_Detail,EP.Pay_Actual_Detail,EP.Payment_Date_Actual ,(select ISnull(SUM(AEAD.Pay_Amount_Actual),0)" & vbLf
        SQL &= "From TB_Activity_Expense_Actual_Detail As AEAD Where AEAD.Activity_Expense_Actual_id = EP.id) As SumCol from TB_Activity_Expense_Header As aeh join TB_Activity_Expense_Actual As EP On Ep.Header_id = aeh.id " & vbLf
        SQL &= " where aeh.id = " + header_id + " And Ep.Recipience_id = " + ReId + "order by EP.Payment_Date_Actual"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetDataExpensePlanListGroup_FromQurey(ByVal header_id As String) As DataTable
        Dim SQL As String = "Select activity_id, EP.id, EP.Payment_Plan_Detail, EP.Pay_Plan_Detail, EP.Payment_Date_Plan " & vbLf
        SQL &= " From TB_Activity_Expense_Header As aeh Join TB_Activity_Expense_Plan As EP On Ep.Header_id = aeh.id  " & vbLf
        SQL &= " Where aeh.id = " & header_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetDataExpensePlanList_FromQurey(ByVal header_id As String, ByVal ReId As String) As DataTable
        Dim SQL As String = " Select activity_id, EP.id, EP.Payment_Plan_Detail, EP.Pay_Plan_Detail, EP.Payment_Date_Plan, " & vbLf
        SQL &= " (Select ISnull(SUM(AEPD.Pay_Amount_Plan), 0) From TB_Activity_Expense_Plan_Detail As AEPD Where AEPD.Activity_Expense_Plan_id = EP.id) As SumCol from TB_Activity_Expense_Header As aeh join TB_Activity_Expense_Plan As EP On Ep.Header_id = aeh.id " & vbLf
        SQL &= " where aeh.id = " & header_id & " And Ep.Recipience_id = " & ReId & " order by EP.Payment_Date_Plan"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetDataRecipent_FromQurey(ByVal activity_id As String) As DataTable
        Dim SQL As String = "Select  id,activity_id,country_id,recipient_id FROM TB_Activity_Recipience" & vbLf
        SQL &= " where activity_id = " & activity_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetDataAdministrative_Cost_FromQurey(ByVal activity_id As String) As DataTable
        Dim SQL As String = ""
        SQL = " Select id,activity_id,payment_date As date_payment,amount,Description " & vbLf
        SQL &= " From TB_Activity_Type_Administrative_Cost" & vbLf
        SQL &= " Where activity_id = " & activity_id

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetDataDisbursement_Cost_FromQurey(activity_id As String) As DataTable
        Dim SQL As String = ""
        SQL = " Select id, activity_id, payment_date As date_payment,amount " & vbLf
        SQL &= "From TB_Activity_Type_Disbursement" & vbLf
        SQL &= " Where activity_id = " & activity_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetDataCountryRe_Cost_FromQurey(activity_id As String) As DataTable
        Dim SQL As String = ""
        SQL = " Select id, activity_id, country_node_id, amout_person" & vbLf
        SQL &= " From TB_Country_Recipient" & vbLf
        SQL &= " Where activity_id = " & activity_id

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetCountProjectByProjectType() As DataTable
        Dim SQL As String = ""
        SQL = " Select Project_Type, count(id) cnt from tb_project group by project_type"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetCountProjectLastestFiveYear() As DataTable
        Dim SQL As String = ""
        SQL &= " Declare @tbyear table(stryear varchar(4) null," & vbLf
        SQL &= " a varchar(10) null," & vbLf
        SQL &= " b varchar(10) null," & vbLf
        SQL &= " c varchar(10) null," & vbLf
        SQL &= " d varchar(10) null)" & vbLf
        SQL &= " Declare @cnt INT = 0;" & vbLf
        SQL &= " Declare @year varchar(4)='';" & vbLf
        SQL &= " While @cnt < 5" & vbLf
        SQL &= " BEGIN	" & vbLf
        SQL &= "    Set @year = convert(varchar(4),year(getdate()));  " & vbLf
        SQL &= "    insert into @tbyear" & vbLf
        SQL &= "    Select @year - @cnt," & vbLf
        SQL &= "    (select count(id) cnt from tb_project 	 " & vbLf
        SQL &= "    where project_type='" & Project_Type.Project & "' and convert(varchar(4),created_date,112)=convert(varchar(10),@year - @cnt)" & vbLf
        SQL &= "    ) a," & vbLf
        SQL &= "(select count(id) cnt from tb_project  " & vbLf
        SQL &= "    where project_type='" & Project_Type.NonProject & "' and convert(varchar(4),created_date,112)=convert(varchar(10),@year - @cnt)" & vbLf
        SQL &= "    ) b," & vbLf
        SQL &= "    (select count(id) cnt from tb_project " & vbLf
        SQL &= "    where project_type='" & Project_Type.Loan & "' and convert(varchar(4),created_date,112)=convert(varchar(10),@year - @cnt)" & vbLf
        SQL &= "    ) c," & vbLf
        SQL &= "(select count(id) cnt from tb_project  " & vbLf
        SQL &= "    where project_type='" & Project_Type.Contribuition & "' and convert(varchar(4),created_date,112)=convert(varchar(10),@year - @cnt)" & vbLf
        SQL &= "    ) d" & vbLf
        SQL &= "    SET @cnt = @cnt + 1;" & vbLf
        SQL &= " End;" & vbLf
        SQL &= " select * from(select * from @tbyear) Y order by stryear" & vbLf
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetCountComponentProject() As DataTable
        Dim SQL As String = "declare @cntpj int = 0;" & vbLf
        SQL &= " Set @cntpj = (Select count(id) from tb_project p where convert(varchar(4),p.created_date,112) = convert(varchar(4),getdate(),112))" & vbLf
        SQL &= " select * from (" & vbLf
        SQL &= " Select top 5 count(a.project_id) count_pj,(count(a.project_id)*100)/@cntpj percent_pj,ap.component_id,c.component_name " & vbLf
        SQL &= " from tb_activity a  inner join tb_project p on a.project_id = p.id" & vbLf
        SQL &= " inner join tb_activity_component ap On a.id=ap.activity_id" & vbLf
        SQL &= " left join tb_component c on ap.component_id=c.id" & vbLf
        SQL &= " where convert(varchar(4),p.created_date,112) = convert(varchar(4),getdate(),112)" & vbLf
        SQL &= " group by ap.component_id,c.component_name " & vbLf
        SQL &= " order by component_name) T"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetProjectList(ByVal project_type As Integer, ByVal Str As String) As DataTable
        Dim SQL As String = ""

        If project_type = 0 Or project_type = 1 Then
            SQL = "Select ROW_NUMBER() OVER(ORDER BY tb_project.project_id,project_name ASC) As Seq,(select plan_name from TB_plan p join TB_projectplan pp On p.id = pp.plan_id Where pp.project_id = tb_project.id) As plan_name" & vbLf
            SQL &= ",id,tb_project.project_id,project_name,project_type,objective,description,start_date ,end_date,cooperation_framework_id,cooperation_type_id,region_oecd_id" & vbLf
            SQL &= ",oecd_id,location,funding_agency_id,executing_agency_id,implementing_agency_id,assistant ,remark,transfer_project_to,multilateral_id,allocate_budget" & vbLf
            SQL &= " ,(Select [dbo].[GetFullThaiDate](start_date)) start_date_th,(Select [dbo].[GetFullThaiDate](end_date)) end_date_th," & vbLf
            SQL &= "ISNULL((select SUM(Pay_Amount_Actual) from TB_Activity_Expense_Actual_Detail As AEAD join TB_Activity_Expense_Header As AEH On AEH.id = AEAD.Header_id" & vbLf
            SQL &= "join TB_activity as A On A.id = AEH.Activity_id where A.project_id = tb_project.id and A.active_status = '1'),0) Disbursement" & vbLf
            SQL &= ",ISNULL(vw_Budget.SUM_Amount,0) Budget , ISNULL(vw_Budget.SUM_Amount,0) -  ISNULL((select SUM(Pay_Amount_Actual) from TB_Activity_Expense_Actual_Detail As AEAD join TB_Activity_Expense_Header As AEH On AEH.id = AEAD.Header_id join TB_activity as A On A.id = AEH.Activity_id where A.project_id = tb_project.id and A.active_status = '1'),0) Balance From tb_project" & vbLf
            SQL &= "Left Join vw_Activity_Budget vw_Budget On tb_project.id=vw_Budget.project_id  where project_type =" & project_type & vbLf
            If Str <> "" Then
                SQL &= " And ((select plan_name from TB_plan p join TB_projectplan pp On p.id = pp.plan_id Where pp.project_id = tb_project.id) Like '%" & Str & "%' or tb_project.project_id Like '%" & Str & "%' or project_name like '%" & Str & "%' or convert(varchar(10),start_date,112) like '%" & Str & "%' or convert(varchar(10),end_date,112) like '%" & Str & "%' or allocate_budget like '%" & Str & "%')"
            End If
        Else
            SQL = "Select ROW_NUMBER() OVER(ORDER BY tb_project.project_id,project_name ASC) As Seq,(select plan_name from TB_plan p join TB_projectplan pp On p.id = pp.plan_id Where pp.project_id = tb_project.id) As plan_name,id,tb_project.project_id,project_name,project_type,objective,description,start_date ,end_date,cooperation_framework_id,cooperation_type_id,region_oecd_id" & vbLf
            SQL &= ",oecd_id,location,funding_agency_id,executing_agency_id,implementing_agency_id,assistant ,remark,transfer_project_to,multilateral_id,allocate_budget" & vbLf
            SQL &= ",(Select [dbo].[GetFullThaiDate](start_date)) start_date_th,(Select [dbo].[GetFullThaiDate](end_date)) end_date_th,ISNULL((select SUM(commitment_budget) from TB_activity As A where A.project_id = tb_project.id and  A.active_status ='1'),0) Budget" & vbLf
            SQL &= ",isnull((select SUM(Pay_Amount_Actual) from TB_Activity_Expense_Actual_Detail As AEAD join TB_Activity_Expense_Header As AEH On AEH.id = AEAD.Header_id join TB_activity as A On A.id = AEH.Activity_id where A.project_id = tb_project.id and A.active_status = '1'),0) Disbursement" & vbLf
            SQL &= ",ISNULL((select SUM(commitment_budget) from TB_activity As A where A.project_id = tb_project.id and  A.active_status ='1'),0) - (isnull((select SUM(Pay_Amount_Actual) from TB_Activity_Expense_Actual_Detail As AEAD join TB_Activity_Expense_Header As AEH On AEH.id = AEAD.Header_id join TB_activity as A On A.id = AEH.Activity_id where A.project_id = tb_project.id and A.active_status = '1'),0)) Balance From tb_project" & vbLf
            SQL &= "Left Join vwLoneConBudget AS vw_Budget On tb_project.id=vw_Budget.project_id  where project_type =" & project_type & vbLf
        End If

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetProjectListExpense() As DataTable
        Dim SQL As String = ""
        SQL = "Select ROW_NUMBER() OVER(ORDER BY tb_project.project_id,project_name ASC) As Seq,(select plan_name from TB_plan p join TB_projectplan pp On p.id = pp.plan_id Where pp.project_id = tb_project.id) As plan_name" & vbLf
        SQL &= ",id,tb_project.project_id,project_name,project_type,objective,description,start_date ,end_date,cooperation_framework_id,cooperation_type_id,region_oecd_id" & vbLf
        SQL &= ",oecd_id,location,funding_agency_id,executing_agency_id,implementing_agency_id,assistant ,remark,transfer_project_to,multilateral_id,allocate_budget" & vbLf
        SQL &= " ,(Select [dbo].[GetFullThaiDate](start_date)) start_date_th,(Select [dbo].[GetFullThaiDate](end_date)) end_date_th" & vbLf
        SQL &= ",ISNULL(vw_Budget.SUM_Amount,0) Budget ,isnull((Select sum(Disbursement) from vw_Activity_Disbursement where vw_Activity_Disbursement.project_id =tb_project.id),0) Disbursement  ,  ISNULL(vw_Budget.SUM_Amount,0) - (isnull((Select sum(Disbursement) from vw_Activity_Disbursement where vw_Activity_Disbursement.project_id =tb_project.id),0)) Balance From tb_project" & vbLf
        SQL &= "Left Join vw_Activity_Budget vw_Budget On tb_project.id=vw_Budget.project_id  where 1 = 1 " & vbLf
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetProjectListforAdSearch(project_type As Integer, code As String, Pl As String, Bs As String, Be As String, Ds As String, De As String, Bc As String, balance As String) As DataTable
        Dim SQL As String = ""

        SQL = "Select distinct p.id,p.project_id,p.project_name" & vbLf
        SQL &= ",ISNULL(vw_Budget.SUM_Amount,0) Budget" & vbLf
        SQL &= ",isnull((Select sum(Disbursement) from vw_Activity_Disbursement where vw_Activity_Disbursement.project_id =p.id),0) Disbursement" & vbLf
        SQL &= ",ISNULL(vw_Budget.SUM_Amount,0) - (isnull((Select sum(Disbursement) from vw_Activity_Disbursement where vw_Activity_Disbursement.project_id =p.id),0)) Balance" & vbLf
        SQL &= ",(Select [dbo].[GetFullThaiDate](start_date)) start_date_th,(Select [dbo].[GetFullThaiDate](end_date)) end_date_th" & vbLf
        SQL &= "From tb_project p Left Join vw_Activity_Budget vw_Budget On p.id=vw_Budget.project_id " & vbLf
        SQL &= " Left Join TB_Activity On p.id = TB_Activity.project_id " & vbLf
        SQL &= " Left Join TB_Country_Recipient cr on TB_Activity.id = cr.activity_id " & vbLf
        SQL &= " Left Join TB_Activity_Recipience ar on TB_Activity.id = ar.activity_id " & vbLf
        SQL &= " Left Join TB_OU_Country c on cr.country_node_id  = c.node_id " & vbLf
        SQL &= " Left Join TB_OU_Country c1 on ar.country_id = c1.node_id where project_type =" & project_type & vbLf
        If code <> "" Then
            SQL &= "And p.project_id Like '%" & code & "%'"
        End If
        If Pl <> "" Then
            SQL &= "AND p.project_name Like '%" & Pl & "%'"
        End If
        If Bs <> "" And Be <> "" Then
            SQL &= " And ISNULL(vw_Budget.SUM_Amount,0) between " & Bs & " and " & Be & ""
        End If
        If Ds <> "" And De <> "" Then
            SQL &= " AND convert(varchar(8),p.[start_date], 112) between '" & Ds & "' and '" & De & "'"
        End If
        If Bc <> "" Then
            SQL &= " And (c.name_en Like '%" & Bc & "%' OR c.name_th like '%" & Bc & "%' OR c1.name_en like '%" & Bc & "%' OR c1.name_th like '%" & Bc & "%')"
        End If
        If balance <> "" Then
            If balance = 0 Then
                SQL &= " AND  ISNULL(vw_Budget.SUM_Amount,0) - (isnull((Select sum(Disbursement) from vw_Activity_Disbursement where vw_Activity_Disbursement.project_id =p.id),0)) = 0"
            Else
                SQL &= " AND  ISNULL(vw_Budget.SUM_Amount,0) - (isnull((Select sum(Disbursement) from vw_Activity_Disbursement where vw_Activity_Disbursement.project_id =p.id),0)) > 0"
            End If
        End If
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetProjectInfoByID(project_id As String) As DataTable
        Dim SQL As String = ""

        SQL = " Select ROW_NUMBER() OVER(ORDER BY p.id ASC) As Seq,p.id,p.project_id,p.project_name,p.project_type,p.objective,p.description,p.start_date" & vbLf
        SQL &= " ,p.end_date,(Select [dbo].[GetFullThaiDate](p.start_date)) start_date_th,(Select [dbo].[GetFullThaiDate](p.end_date)) end_date_th,p.cooperation_framework_id,p.cooperation_type_id,p.region_oecd_id" & vbLf
        SQL &= " ,p.oecd_id,p.location,p.funding_agency_id,p.executing_agency_id,p.implementing_agency_id" & vbLf
        SQL &= " ,p.assistant ,p.remark,p.transfer_project_to,p.multilateral_id,p.allocate_budget,c.address,c.contact_name" & vbLf
        SQL &= " ,c.telephone,c.fax,c.website,c.description,c.email,c.position" & vbLf
        SQL &= " ,DATEPART(m,p.start_date) month_start,DATEPART(m,p.end_date) month_end," & vbLf
        SQL &= " Year(p.start_date) year_start, YEAR(p.end_date) year_end," & vbLf
        SQL &= " DatePart(m, getdate()) month_now,YEAR(getdate()) year_now, getdate() now_date" & vbLf
        SQL &= " ,oecd_aid_type_id,location,grance_period,maturity_period,interest_rate,implementing_agency_loan,implementing_agency_contribution" & vbLf
        SQL &= " ,f.fwork_name cooperation_framework_name,t.cptype_name cooperation_type_name,r.regionoda_name region_oecd_name,o.oecd_name oecd_name,fo.name_th funding_agency_name" & vbLf
        SQL &= " ,eo.name_th executing_agency_name,ipo.name_th implementing_agency_name,po.name_th" & vbLf
        SQL &= " ,m.names multilateral_name,oa.oecd_name oecd_aid_type_name,aco.name_th implementing_agency_contribution_name" & vbLf
        SQL &= " ,(select [dbo].GetFullThaiDate(p.start_date)) strStart_date,(select [dbo].GetFullThaiDate(p.end_date)) strEnd_date" & vbLf
        SQL &= " From tb_project p " & vbLf
        SQL &= " Left Join tb_ou_contact  c On p.id = c.parent_id And parent_type = " & Contact_Parent_Type.Project & "" & vbLf
        SQL &= "  Left Join TB_CoperationFramework f on p.cooperation_framework_id = f.id" & vbLf
        SQL &= " Left Join TB_CoperationType t On p.cooperation_type_id=t.id" & vbLf
        SQL &= " Left Join TB_Regionzoneoda r on p.region_oecd_id = r.id" & vbLf
        SQL &= " Left Join TB_oecd o On p.oecd_id = o.id" & vbLf
        SQL &= " Left Join TB_OU_Organize fo on p.funding_agency_id = fo.node_id" & vbLf
        SQL &= " Left Join TB_OU_Organize eo On p.executing_agency_id = eo.node_id" & vbLf
        SQL &= " Left Join TB_OU_Organize ipo on p.implementing_agency_id = ipo.node_id" & vbLf
        SQL &= " Left Join TB_OU_Person po On p.transfer_project_to=po.node_id" & vbLf
        SQL &= " Left Join TB_multilateral m on p.multilateral_id = m.id" & vbLf
        SQL &= " Left Join TB_oecd oa On p.oecd_id = oa.id" & vbLf
        SQL &= " Left Join TB_OU_Organize aco on p.implementing_agency_id = aco.node_id" & vbLf
        SQL &= " Where p.id = " & project_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT

    End Function

    Public Function GetProjectInfoByIDAllType(ByVal project_id As String) As DataTable
        Dim SQL As String = "select * from TB_Activity As A Join TB_Project As P On P.id = A.project_id where A.id =" & project_id

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetHeaderData(ByVal activity_id As Integer, ByVal HeaderId As Integer) As DataTable
        Dim SQL As String = "select AEH.Payment_Detail,A.activity_name,P.project_name,AEH.Payment_Date_Start," & vbLf
        SQL &= " (Select [dbo].[GetFullThaiDate](AEH.Payment_Date_Start)) As date,AEH.Payment_Date_End " & vbLf
        SQL &= " From TB_Activity As A Join TB_Project As P On p.id = A.project_id " & vbLf
        SQL &= " Join TB_Activity_Expense_Header As AEH On A.id = AEH.Activity_id " & vbLf
        SQL &= " Where A.id =" & activity_id & " And AEH.id =" & HeaderId & vbLf
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetRecipience_ById(ByVal Id As String) As String
        Dim SQL As String = "Select * From TB_Activity_Recipience Where id =" & Id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT.Rows(0)("recipient_id").ToString
    End Function

    Public Function GetRecipienceName_ById(Id As String) As String
        Dim SQL As String = "Select * From TB_OU_Person Where node_id ='" & Id.Replace("'", "''") & "'"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT.Rows(0)("name_en").ToString
    End Function

    Public Function GetCountryRe_ById(Id As String) As String
        Dim SQL As String = "Select * From TB_Activity_Country Where id ='" & Id.Replace("'", "''") & "'"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT.Rows(0)("country_node_id").ToString
    End Function

    Public Function GetCountry_ById(Id As String) As String
        Dim SQL As String = "Select * From TB_Country_Recipient Where id ='" & Id.Replace("'", "''") & "'"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT.Rows(0)("country_node_id").ToString
    End Function

    Public Function GetCountryName_ByCountryID(ByVal reid As String) As String
        Dim SQL As String = "Select * From TB_OU_Country node_id ='" & reid.Replace("'", "''") & "'"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT.Rows(0)("name_th").ToString
    End Function

    Public Function GetActivityBudget(ByVal Activity_id As String) As DataTable
        Dim SQL As String = "Select * From TB_Activity_Budget WHERE activity_id=" & Activity_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetActivityTemplate(ByVal ACTIVITY_ID As String) As DataTable
        Dim SQL As String = "Select * From TB_Expense_Template_Activity Where activity_id=" & ACTIVITY_ID
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetActivityComponent(ByVal activity_id As String) As DataTable
        Dim SQL As String = " Select id, activity_id, component_id, amount From TB_Activity_Component Where activity_id =" & activity_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetActivityComponentName(ByVal component_id As String) As String
        Dim SQL As String = "Select component_name From TB_Component  Where id =" & component_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT.Rows(0)("component_name").ToString
    End Function

    Public Function GetActivityInkindName(ByVal Inkind_id As String) As String
        Dim SQL As String = "Select inkind_name From TB_Inkind Where id =" & Inkind_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT.Rows(0)("inkind_name").ToString
    End Function

    Public Function GetProjectImplementingAgency(ByVal project_id As String) As DataTable
        Dim SQL As String
        SQL = "  Select pia.id, project_id, pia.country_id, pia.organize_id, isnull(cou.name_th, cou.name_en) country_name, oou.name_en organize_name " & vbLf
        SQL &= " From TB_Project_Implementing_Agency pia " & vbLf
        SQL &= " inner Join vw_ou cou On cou.node_id=pia.country_id" & vbLf
        SQL &= " inner Join vw_ou oou On oou.node_id=pia.organize_id" & vbLf
        SQL &= " Where pia.project_id =" & project_id

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetActivityOrganize(ByVal activity_id As String) As DataTable
        Dim SQL As String
        SQL = " Select pia.id, activity_id, pia.country_id, pia.organize_id, isnull(cou.name_th, cou.name_en) country_name, oou.name_en organize_name " & vbLf
        SQL &= " From TB_Activity_Organization pia " & vbLf
        SQL &= " inner Join vw_ou cou On cou.node_id=pia.country_id" & vbLf
        SQL &= " inner Join vw_ou oou On oou.node_id=pia.organize_id" & vbLf
        SQL &= " Where pia.activity_id =" & activity_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetProjectCoFunding(ByVal project_id As String) As DataTable
        Dim SQL As String = ""
        SQL = " Select p.id, project_id, country_id, amount, c.name_en, c.name_th  " & vbLf
        SQL &= " From TB_Project_CoFunding p " & vbLf
        SQL &= " inner Join " & vbLf
        SQL &= " (Select node_id, name_en,'c' datatype,name_th from TB_OU_Country where isnull(active_status,'Y') = 'Y' " & vbLf
        SQL &= " union" & vbLf
        SQL &= " Select  node_id,isnull(abbr_name_en + '-'  ,'')+ name_en as name_en,'o' datatype,name_th  " & vbLf
        SQL &= " From TB_OU_Organize Where isnull(active_status,'Y') = 'Y' ) c on p.country_id=c.node_id " & vbLf
        SQL &= "  where project_id =" & project_id & vbLf
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetProjectPlan(ByVal project_id As String) As DataTable
        Dim SQL As String = " select * from TB_ProjectPlan WHERE project_id =" & project_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetProjectFile(ByVal project_id As String) As DataTable
        Dim SQL As String = " Select id, id file_id,project_id,original_file_name,content_type,description,file_path " & vbLf
        SQL &= "from TB_Project_File where project_id=" & project_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetProjectSection(ByVal project_id As String) As DataTable
        Dim SQL As String = " Select id,project_id,country_id,organize_id from TB_Project_Section where project_id=" & project_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetProjectActivityList(ByVal project_id As String) As DataTable
        Dim SQL As String = ""
        SQL = " Select id,isnull(activity_name,description) activity_name,description,sector_id,sub_sector_id, plan_start, plan_end,actual_start,actual_end," & vbLf
        SQL &= " isnull((Select sum(amount) from tb_activity_budget where activity_id=ac.id),0) commitment_budget, " & vbLf
        SQL &= "(select isnull(sum(Pay_Amount_Actual),0) from TB_Activity_Expense_Actual_Detail As AEAD join TB_Activity_Expense_Header As AEH On AEAD.Header_id = AEH.id where AEH.Activity_id =ac.id) Expense," & vbLf
        SQL &= " disbursement,administrative,activity_status,beneficiary,assistance_type,notify,project_id," & vbLf
        SQL &= " parent_id,(datediff(day, actual_start, actual_end) + 1) duration,(Select count(id) from TB_Activity aa where aa.parent_id=ac.id) childs," & vbLf
        SQL &= " DATEPART(m,actual_start) month_start,DATEPART(m,actual_end) month_end,YEAR(actual_start) year_start, YEAR(actual_end) year_end," & vbLf
        SQL &= " (Select isnull(sum(amount),0) amount from tb_activity_budget where activity_id In (Select id from [GetAllNodeActivity] (ac.id) where id <> ac.id)) childs_budget " & vbLf
        SQL &= " From TB_Activity ac where project_id =" & project_id & " and active_status = '1'"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetProjectActivityListForDelete(ByVal project_id As String) As DataTable
        Dim SQL As String = ""
        SQL = " Select id,isnull(activity_name,description) activity_name,description,sector_id,sub_sector_id, plan_start, plan_end,actual_start,actual_end," & vbLf
        SQL &= " isnull((Select sum(amount) from tb_activity_budget where activity_id=ac.id),0) commitment_budget, " & vbLf
        SQL &= " disbursement,administrative,activity_status,beneficiary,assistance_type,notify,project_id," & vbLf
        SQL &= " parent_id,(datediff(day, actual_start, actual_end) + 1) duration,(Select count(id) from TB_Activity aa where aa.parent_id=ac.id) childs," & vbLf
        SQL &= " DATEPART(m,actual_start) month_start,DATEPART(m,actual_end) month_end,YEAR(actual_start) year_start, YEAR(actual_end) year_end," & vbLf
        SQL &= " (Select isnull(sum(amount),0) amount from tb_activity_budget where activity_id In (Select id from [GetAllNodeActivity] (ac.id) where id <> ac.id)) childs_budget " & vbLf
        SQL &= " From TB_Activity ac where project_id =" & project_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetProjectActivityListLone(ByVal project_id As String) As DataTable
        Dim SQL As String = ""
        SQL = " Select id,isnull(activity_name,description) activity_name,description,sector_id,sub_sector_id, plan_start, plan_end,actual_start,actual_end," & vbLf
        SQL &= " isnull((Select sum(commitment_budget) from TB_Activity where id=ac.id),0) commitment_budget, " & vbLf
        SQL &= " (select isnull(sum(Pay_Amount_Actual),0) from TB_Activity_Expense_Actual_Detail As AEAD join TB_Activity_Expense_Header As AEH On AEAD.Header_id = AEH.id where AEH.Activity_id =ac.id) Expense," & vbLf
        SQL &= " disbursement,administrative,activity_status,beneficiary,assistance_type,notify,project_id," & vbLf
        SQL &= " parent_id,(datediff(day, actual_start, actual_end) + 1) duration,(Select count(id) from TB_Activity aa where aa.parent_id=ac.id) childs," & vbLf
        SQL &= " DATEPART(m,actual_start) month_start,DATEPART(m,actual_end) month_end,YEAR(actual_start) year_start, YEAR(actual_end) year_end" & vbLf
        SQL &= " ,0.00 as childs_budget " & vbLf
        SQL &= " From TB_Activity ac where project_id =" & project_id & " and active_status = '1'"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function FindType(ByVal project_id As String) As DataTable

        Dim SQL As String = "select project_type from TB_Project where id = " & project_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    '===========GetAllActivityByProject  สำหรับ Project  /  non Project
    Public Function GetAllActivityByProject(ByVal project_id As String) As DataTable
        Dim SQL As String = ""
        SQL = " With activity (parent_id,id,activity_name, Level,plan_start, plan_end,duration) As" & vbLf
        SQL &= " (" & vbLf
        SQL &= " Select parent_id,id,isnull(activity_name,[description]) activity_name, 0 As Level, plan_start, plan_end,datediff(day, plan_start, plan_end) duration" & vbLf
        SQL &= " From TB_Activity a Where PROJECT_ID =" & project_id & " And parent_id=0" & vbLf
        SQL &= " UNION ALL" & vbLf
        SQL &= " Select a.parent_id,a.id,isnull(a.activity_name,[description]) activity_name, Level+1 As Level " & vbLf
        SQL &= " , a.plan_start, a.plan_end,datediff(day, a.plan_start, a.plan_end) duration" & vbLf
        SQL &= " From TB_Activity As a" & vbLf
        SQL &= " INNER JOIN activity As at" & vbLf
        SQL &= " On a.Parent_ID=at.ID " & vbLf
        SQL &= " )" & vbLf
        SQL &= " Select  DISTINCT ROW_NUMBER() OVER(ORDER BY id ASC) As Seq, parent_id,id,right(space(60) + activity_name,len(activity_name) + (level*3)) activity_name, Level,plan_start, plan_end,duration," & vbLf
        SQL &= " isnull((Select sum(amount) from tb_activity_budget where activity_id= activity.id),0) commitment_budget," & vbLf
        SQL &= " (Select [dbo].GetFullThaiDate(plan_start)) strStart_date,(Select [dbo].GetFullThaiDate(plan_end)) strEnd_date" & vbLf
        SQL &= " From activity" & vbLf
        SQL &= " ORDER BY ID"

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    '===========GetAllActivityByProject  สำหรับ Loan
    Public Function GetAllActivityByLoan(ByVal project_id As String) As DataTable
        Dim SQL As String = ""
        SQL = " With activity (parent_id,id,activity_name, Level,plan_start, plan_end,duration) As" & vbLf
        SQL &= " (" & vbLf
        SQL &= " Select parent_id,id,isnull(activity_name,[description]) activity_name, 0 As Level, plan_start, plan_end,datediff(day, plan_start, plan_end) duration" & vbLf
        SQL &= " From TB_Activity a Where PROJECT_ID =" & project_id & " And parent_id=0" & vbLf
        SQL &= " UNION ALL" & vbLf
        SQL &= " Select a.parent_id,a.id,isnull(a.activity_name,[description]) activity_name, Level+1 As Level " & vbLf
        SQL &= " , a.plan_start, a.plan_end,datediff(day, a.plan_start, a.plan_end) duration" & vbLf
        SQL &= " From TB_Activity As a" & vbLf
        SQL &= " INNER JOIN activity As at" & vbLf
        SQL &= " On a.Parent_ID=at.ID " & vbLf
        SQL &= " )" & vbLf
        SQL &= " Select  DISTINCT ROW_NUMBER() OVER(ORDER BY id ASC) As Seq, parent_id,id,right(space(60) + activity_name,len(activity_name) + (level*3)) activity_name, Level,plan_start, plan_end,duration," & vbLf
        SQL &= " isnull((Select sum(commitment_budget) from TB_Activity where id= activity.id),0) commitment_budget," & vbLf
        SQL &= " (Select [dbo].GetFullThaiDate(plan_start)) strStart_date,(Select [dbo].GetFullThaiDate(plan_end)) strEnd_date" & vbLf
        SQL &= " From activity" & vbLf
        SQL &= " ORDER BY ID"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetActivityExpenseDataIntoTypeR(ByVal activity_id As String, header_id As String) As DataTable
        Dim SQL As String = ""
        SQL = " select (select isnull(sum(AEPD.Pay_Amount_Plan),0) from TB_Activity_Expense_Plan_Detail As AEPD where AEH.id = AEPD.Header_id and AEPD.Recipience_id = AR.recipient_id) As budget," & vbLf
        SQL &= " (select isnull(sum(AEPA.Pay_Amount_Actual),0) from TB_Activity_Expense_Actual_Detail As AEPA where AEH.id = AEPA.Header_id and AEPA.Recipience_id = AR.recipient_id) As Expense," & vbLf
        SQL &= " AR.recipient_id,AR.country_id,(select OP.name_th from TB_OU_Person As OP where OP.node_id = AR.recipient_id) As Name" & vbLf
        SQL &= " from TB_Activity_Expense_Header As AEH join TB_Activity_Recipience As AR On Ar.activity_id = AEH.Activity_id where AEH.Activity_id =" & activity_id & " and AEH.id =" & header_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetActivityExpenseDataIntoTypeC(ByVal activity_id As String, ByVal header_id As String) As DataTable
        Dim SQL As String = ""
        SQL = " select (select isnull(sum(AEPD.Pay_Amount_Plan),0) from TB_Activity_Expense_Plan_Detail As AEPD where AEH.id = AEPD.Header_id and AEPD.Recipience_id = AR.country_node_id) As budget," & vbLf
        SQL &= " (select isnull(sum(AEPA.Pay_Amount_Actual),0) from TB_Activity_Expense_Actual_Detail As AEPA where AEH.id = AEPA.Header_id and AEPA.Recipience_id = AR.country_node_id) As Expense," & vbLf
        SQL &= " AR.country_node_id As recipient_id,AR.country_node_id As country_id,(select OP.name_th from TB_OU_Country As OP where OP.node_id = AR.country_node_id) As Name" & vbLf
        SQL &= " from TB_Activity_Expense_Header As AEH join TB_Activity_Country As AR On Ar.activity_id = AEH.Activity_id where AEH.Activity_id =" & activity_id & " and AEH.id =" & header_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetActivityExpenseDataIntoTypeO(ByVal activity_id As String, ByVal header_id As String) As DataTable
        Dim SQL As String = ""
        SQL = " select (select isnull(sum(AEPD.Pay_Amount_Plan),0) from TB_Activity_Expense_Plan_Detail As AEPD where AEH.id = AEPD.Header_id and AEPD.Recipience_id = AR.organize_id) As budget," & vbLf
        SQL &= " (select isnull(sum(AEPA.Pay_Amount_Actual),0) from TB_Activity_Expense_Actual_Detail As AEPA where AEH.id = AEPA.Header_id and AEPA.Recipience_id = AR.organize_id) As Expense," & vbLf
        SQL &= " AR.organize_id  As recipient_id,AR.country_id,(select OP.name_th from TB_OU_Organize As OP where OP.node_id = AR.organize_id) As Name" & vbLf
        SQL &= " from TB_Activity_Expense_Header As AEH join TB_Activity_Organization As AR On Ar.activity_id = AEH.Activity_id where AEH.Activity_id =" & activity_id & " and AEH.id =" & header_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetActivityExpenseDataIntoTypeG(ByVal activity_id As String, ByVal header_id As String) As DataTable
        Dim SQL As String = ""
        SQL = " select (select isnull(sum(AEPD.Pay_Amount_Plan),0) from TB_Activity_Expense_Plan_Detail As AEPD where AEH.id = AEPD.Header_id) As budget," & vbLf
        SQL &= " (select isnull(sum(AEPA.Pay_Amount_Actual),0) from TB_Activity_Expense_Actual_Detail As AEPA where AEH.id = AEPA.Header_id and AEPA.Recipience_id = CR.country_node_id) As Expense," & vbLf
        SQL &= " CR.country_node_id  As recipient_id,CR.country_node_id As country_id,(select CONCAT(OC.name_th , ' ผู้รับทุนจำนวน ', CR.amout_person,' คน') AS newfield) As Name" & vbLf
        SQL &= "from TB_Activity_Expense_Header As AEH join TB_Country_Recipient As CR On CR.activity_id = AEH.Activity_id join TB_OU_Country As OC On OC.node_id = CR.country_node_id where AEH.Activity_id =" & activity_id & " and AEH.id =" & header_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetProjectActivityWithRecipince(ByVal project_id As String) As DataTable
        Dim SQL As String = "select * from TB_Activity where id =" & project_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function BuiltChildNodeID(ByVal ParentNodeID As Long) As String
        Dim SQL As String = " Select id from TB_Activity where parent_id=" & ParentNodeID
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Dim ret As String = ""
        For Each dr As DataRow In DT.Rows
            ret += ", 'Node" & dr("id") & "'"
        Next
        Return ret
    End Function

    Public Function GetProjectActivityInfoByID(activity_id As String) As DataTable
        Dim SQL As String = " Select id,activity_name,description,sector_id,sub_sector_id,plan_start,plan_end,actual_start,actual_end,commitment_budget," & vbLf
        SQL &= " disbursement,administrative,activity_status,beneficiary,assistance_type,notify,project_id," & vbLf
        SQL &= " parent_id,(select count(id) from TB_Activity aa where aa.parent_id=ac.id) childs,commitment_budget_loan,disbursement_loan,payment_date_gant,payment_date_loan,payment_date_contribution,pay_as,disbursement_type,Recipient_Type"
        SQL &= " From TB_Activity ac where ac.id =" & activity_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetProjectActivityInkind(activity_id As String) As DataTable
        Dim SQL As String = ""
        SQL = " Select id,activity_id,inkind_id,estimate from TB_Activity_Inkind where activity_id =" & activity_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetProjectActivityDisbursement(activity_id As String) As DataTable
        Dim SQL As String = ""
        SQL = " select * from TB_Activity_Disbursement where activity_id =" & activity_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetProjectActivityDisbursementCon(activity_id As String) As DataTable
        Dim SQL As String = ""
        SQL = " select * from TB_Activity_Disbursement_Contribuition where activity_id =" & activity_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetProjectBudgetCon(project_id As String) As DataTable
        Dim SQL As String = ""
        SQL = " select * from TB_Activity_Budget_Contribuition where project_id =" & project_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetActivityRecipinceCountry(activity_id As String) As DataTable
        Dim SQL As String = ""
        SQL = " Select r.id,c.node_id,c.name_th from TB_Activity_Recipience r" & vbLf
        SQL &= " left join tb_ou_country c On r.country_id = c.node_id where activity_id =" & activity_id & " And r.country_id Is Not null " & vbLf
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetActivityRecipincePerson(activity_id As String) As DataTable
        Dim SQL As String = ""
        SQL = " Select r.id,p.node_id,p.name_th from TB_Activity_Recipience r " & vbLf
        SQL &= " Left Join TB_OU_Person p On r.recipient_id = p.node_id " & vbLf
        SQL &= " where activity_id =" & activity_id & " And r.recipient_id Is Not null " & vbLf
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetProjectRecipince(project_id As String) As DataTable
        Dim SQL As String = ""
        SQL = "select ROW_NUMBER() OVER(ORDER BY T.id ASC) As Seq,T.*,activity_name from (" & vbLf
        SQL &= " Select  r.id,p.node_id,p.name_th,activity_id,'P' rectype from TB_Activity_Recipience r " & vbLf
        SQL &= " Left Join TB_OU_Person p On r.recipient_id = p.node_id " & vbLf
        SQL &= " where activity_id in (select id from TB_Activity where project_id=" & project_id & ") And r.recipient_id Is Not null" & vbLf
        SQL &= " union " & vbLf
        SQL &= " Select r.id,c.node_id,c.name_th,activity_id,'C' rectype from TB_Activity_Recipience r" & vbLf
        SQL &= " Left join tb_ou_country c On r.country_id = c.node_id " & vbLf
        SQL &= " where activity_id in (select id from TB_Activity where project_id=" & project_id & ") And r.country_id Is Not null" & vbLf
        SQL &= " ) T left Join TB_Activity a on T.activity_id = a.id"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetRecipinceCountry(activity_id As String) As DataTable
        Dim SQL As String = ""
        SQL = "select ROW_NUMBER() OVER(ORDER BY C.id ASC) As Seq,activity_name,C.id,C.country_node_id as node_id,C.country_node_id,C.country_node_id as country_id,u.name_th,activity_id" & vbLf
        SQL &= " from TB_Activity As A join TB_Activity_Country As C On A.id = C.activity_id " & vbLf
        SQL &= " Left Join TB_OU_Country u On u.node_id = C.country_node_id " & vbLf
        SQL &= " where activity_id =" & activity_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetActivityRecipinceGroup(activity_id As String) As DataTable
        Dim SQL As String = ""
        SQL = "select ROW_NUMBER() OVER(ORDER BY r.id ASC) As Seq,activity_name,r.id,r.country_node_id,r.country_node_id As country_id,c.name_th,activity_id,r.amout_person" & vbLf
        SQL &= " from TB_Activity As A join TB_Country_Recipient r On A.id = r.activity_id " & vbLf
        SQL &= " Left join tb_ou_country c On r.country_node_id = c.node_id " & vbLf
        SQL &= " where activity_id = " & activity_id & " And r.country_node_id Is Not null"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetActivityRecipinceIndividual(activity_id As String) As DataTable
        Dim SQL As String = ""
        SQL = "select ROW_NUMBER() OVER(ORDER BY r.id ASC) As Seq,activity_name,r.id,p.node_id,r.country_id,p.name_th,activity_id" & vbLf
        SQL &= " from TB_Activity As A join TB_Activity_Recipience r On A.id = r.activity_id " & vbLf
        SQL &= " Left Join TB_OU_Person p On r.recipient_id = p.node_id " & vbLf
        SQL &= " where activity_id =" & activity_id & " And r.recipient_id Is Not null"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetActivityRecipinceOrganize(activity_id As String) As DataTable
        Dim SQL As String = ""
        SQL = "select ROW_NUMBER() OVER(ORDER BY AR.id ASC) As Seq,A.activity_name,A.id As activity_id,AR.organize_id  As recipient_id" & vbLf
        SQL &= " ,AR.country_id,AR.country_id As node_id ,(select OP.name_th from TB_OU_Organize As OP where OP.node_id = AR.organize_id) As name_th " & vbLf
        SQL &= " from TB_Activity_Expense_Header As AEH join TB_Activity_Organization As AR On Ar.activity_id = AEH.Activity_id join TB_Activity As A On A.id = AEH.Activity_id " & vbLf
        SQL &= " where AEH.Activity_id =" & activity_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    'เอาไว้เรียก Recipience
    Public Function GetActivityRecipince(activity_id As String) As DataTable
        Dim SQL As String = ""
        SQL &= "select ROW_NUMBER() OVER(ORDER BY T.id ASC) As Seq,T.*,activity_name from ( " & vbLf
        SQL &= "Select  r.id,p.node_id,p.name_th,activity_id from TB_Activity_Recipience r " & vbLf
        SQL &= "Left Join TB_OU_Person p On r.recipient_id = p.node_id " & vbLf
        SQL &= "where activity_id =" & activity_id & " And r.recipient_id Is Not null " & vbLf
        SQL &= "union " & vbLf
        SQL &= "Select r.id,c.node_id,c.name_th,activity_id from TB_Country_Recipient r " & vbLf
        SQL &= "Left join tb_ou_country c On r.country_node_id = c.node_id " & vbLf
        SQL &= "where activity_id =" & activity_id & " And r.country_node_id Is Not null " & vbLf
        SQL &= ") T left Join TB_Activity a on T.activity_id = a.id"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetTemplateActivity(activity_id As String) As DataTable
        Dim SQL As String = "select template_id from TB_Expense_Template_Activity Where Activity_id =" & activity_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetRecipinceType(activity_id As String) As DataTable
        Dim SQL As String = "select Recipient_Type from TB_Activity Where id =" & activity_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetProjectRecipincePerson(project_id As String) As DataTable
        Dim SQL As String = " Select ROW_NUMBER() OVER(ORDER BY r.id ASC) As Seq,r.id,r.activity_id,r.recipient_id,isnull(ft.prefix_name_th,'') + ' ' + p.name_th  name_th," & vbLf
        SQL &= " isnull(fe.prefix_name_en,'') + ' ' + p.name_en name_en,'' student_id,(select [dbo].[GetParentByChildNode](r.recipient_id,'TB_OU_Country')) country_name," & vbLf
        SQL &= " a.activity_name course, a.plan_start, a.plan_end, a.actual_start, a.actual_end, (select [dbo].[GetParentByChildNode](r.recipient_id,'TB_OU_Organize')) agency," & vbLf
        SQL &= " '' emp_current_positions,case -1 when  0 then 'กำลังศึกษา' when 1 then 'จบการศึกษา' when 2 then 'ไม่จบการศึกษา' end [status]" & vbLf
        SQL &= " ,(Select [dbo].[GetFullThaiDate](a.plan_start)) +'-'+ (Select [dbo].[GetFullThaiDate](a.plan_end)) period_plan_date " & vbLf

        SQL &= " From TB_Activity_Recipience r" & vbLf
        SQL &= " inner join tb_activity a on r.activity_id=a.id" & vbLf
        SQL &= " Left Join tb_ou_person p on r.recipient_id = p.node_id " & vbLf
        SQL &= " Left join tb_recipience n on p.id = n.person_id" & vbLf
        SQL &= " Left Join tb_prefix ft on p.prefix_th_id = ft.id" & vbLf
        SQL &= " Left join tb_prefix fe on p.prefix_en_id = fe.id where project_id =" & project_id & " And r.recipient_id Is Not null"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function SaveProject(lnqProject As TbProjectLinqDB, DTMiltilateral As DataTable, DTImplementingAgency As DataTable, DTCoFunding As DataTable, lnqContact As TbOuContactLinqDB, DTProjectFile As DataTable, DTProjectSection As DataTable, lnqprojectplan As TbProjectplanLinqDB, Username As String) As ProcessReturnInfo
        Dim ret As New ProcessReturnInfo
        Dim trans As New TransactionDB

        Try

            'บันทึกข้อมูลตารางหลัก
            Dim exc As New ExecuteDataInfo
            Dim _lnqProject As New TbProjectLinqDB
            With _lnqProject
                .GetDataByPK(lnqProject.ID, trans.Trans)

                .PROJECT_NAME = lnqProject.PROJECT_NAME
                .PROJECT_TYPE = lnqProject.PROJECT_TYPE
                .OBJECTIVE = lnqProject.OBJECTIVE
                .DESCRIPTION = lnqProject.DESCRIPTION
                .START_DATE = lnqProject.START_DATE
                .END_DATE = lnqProject.END_DATE
                .ALLOCATE_BUDGET = lnqProject.ALLOCATE_BUDGET
                '.BUDGETYEAR = lnqProject.BUDGETYEAR
                '.BUDGET_GROUP_ID = lnqProject.BUDGET_GROUP_ID
                '.BUDGET_SUB_ID = lnqProject.BUDGET_SUB_ID
                .COOPERATION_FRAMEWORK_ID = lnqProject.COOPERATION_FRAMEWORK_ID
                .COOPERATION_TYPE_ID = lnqProject.COOPERATION_TYPE_ID
                .OECD_AID_TYPE_ID = lnqProject.OECD_AID_TYPE_ID
                .LOCATION = lnqProject.LOCATION
                .FUNDING_AGENCY_ID = lnqProject.FUNDING_AGENCY_ID
                .EXECUTING_AGENCY_ID = lnqProject.EXECUTING_AGENCY_ID
                .ASSISTANT = lnqProject.ASSISTANT
                .REMARK = lnqProject.REMARK
                .TRANSFER_PROJECT_TO = lnqProject.TRANSFER_PROJECT_TO
                .IMPLEMENTING_AGENCY_ID = lnqProject.IMPLEMENTING_AGENCY_ID
                .REGION_OECD_ID = lnqProject.REGION_OECD_ID
                .OECD_ID = lnqProject.OECD_ID
                .MULTILATERAL_ID = lnqProject.MULTILATERAL_ID
                .ALLOCATE_BUDGET = lnqProject.ALLOCATE_BUDGET

                .IMPLEMENTING_AGENCY_LOAN = lnqProject.IMPLEMENTING_AGENCY_LOAN
                .GRANCE_PERIOD = lnqProject.GRANCE_PERIOD
                .MATURITY_PERIOD = lnqProject.MATURITY_PERIOD
                .INTEREST_RATE = lnqProject.INTEREST_RATE

                If .ID > 0 Then
                    .PROJECT_ID = lnqProject.PROJECT_ID
                    exc = .UpdateData(Username, trans.Trans)
                Else
                    .PROJECT_ID = GetProjectID()
                    exc = .InsertData(Username, trans.Trans)
                End If
            End With

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If


            '--TB_Activity_Budget_Contribuition
            Dim p_bc(1) As SqlParameter
            p_bc(0) = SqlDB.SetText("@_PROJECT_ID", _lnqProject.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Activity_Budget_Contribuition WHERE project_id=@_PROJECT_ID", trans.Trans, p_bc)

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            If Not DTMiltilateral Is Nothing Then
                For i As Integer = 0 To DTMiltilateral.Rows.Count - 1
                    Dim Multilateral_ID As String = DTMiltilateral.Rows(i)("Multilateral_ID").ToString()
                    Dim Amount As String = DTMiltilateral.Rows(i)("Amount").ToString()

                    Dim _lnqActivityBudgetCon As New TbActivityBudgetContribuitionLinqDB
                    With _lnqActivityBudgetCon
                        .PROJECT_ID = _lnqProject.ID
                        .MULTILATERAL_ID = Multilateral_ID
                        .AMOUNT = Amount
                        exc = .InsertData(Username, trans.Trans)
                    End With

                    If exc.IsSuccess = False Then
                        trans.RollbackTransaction()
                        ret.IsSuccess = False
                        ret.ErrorMessage = exc.ErrorMessage()
                        Return ret
                    End If
                Next
            End If

            '--TB_Project_Implementing_Agency
            Dim p_ia(1) As SqlParameter
            p_ia(0) = SqlDB.SetText("@_PROJECT_ID", _lnqProject.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Project_Implementing_Agency WHERE project_id=@_PROJECT_ID", trans.Trans, p_ia)

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            If Not DTImplementingAgency Is Nothing Then
                For i As Integer = 0 To DTImplementingAgency.Rows.Count - 1
                    Dim country_id As String = DTImplementingAgency.Rows(i)("country_id").ToString()
                    Dim organize_id As String = DTImplementingAgency.Rows(i)("organize_id").ToString()

                    If country_id <> "" AndAlso organize_id <> "" Then
                        Dim _lnqImplementingAgency As New TbProjectImplementingAgencyLinqDB
                        With _lnqImplementingAgency
                            .PROJECT_ID = _lnqProject.ID
                            .COUNTRY_ID = country_id
                            .ORGANIZE_ID = organize_id

                            exc = .InsertData(Username, trans.Trans)
                        End With

                        If exc.IsSuccess = False Then
                            trans.RollbackTransaction()
                            ret.IsSuccess = False
                            ret.ErrorMessage = exc.ErrorMessage()
                            Return ret
                        End If
                    End If

                Next
            End If


            '--TB_Project_CoFunding
            Dim p_cf(1) As SqlParameter
            p_cf(0) = SqlDB.SetText("@_PROJECT_ID", _lnqProject.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Project_CoFunding WHERE project_id=@_PROJECT_ID", trans.Trans, p_cf)

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            If Not DTCoFunding Is Nothing Then
                For i As Integer = 0 To DTCoFunding.Rows.Count - 1
                    Dim country_id As String = DTCoFunding.Rows(i)("country_id").ToString
                    Dim amount As String = DTCoFunding.Rows(i)("amount").ToString

                    If country_id <> "" Then
                        Dim _lnqCofunding As New TbProjectCofundingLinqDB
                        With _lnqCofunding
                            .PROJECT_ID = _lnqProject.ID
                            .COUNTRY_ID = country_id
                            .AMOUNT = amount

                            exc = .InsertData(Username, trans.Trans)
                        End With

                        If exc.IsSuccess = False Then
                            trans.RollbackTransaction()
                            ret.IsSuccess = False
                            ret.ErrorMessage = exc.ErrorMessage()
                            Return ret
                        End If
                    End If

                Next
            End If


            '--TB_OU_Contact
            Dim p_ct(1) As SqlParameter
            p_ct(0) = SqlDB.SetText("@_PROJECT_ID", _lnqProject.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_OU_Contact WHERE parent_id=@_PROJECT_ID and parent_type=" & Contact_Parent_Type.Project & "", trans.Trans, p_ct)

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            If Not lnqProject Is Nothing Then
                Dim _lnqContact As New TbOuContactLinqDB
                With _lnqContact
                    .PARENT_ID = _lnqProject.ID
                    .PARENT_TYPE = lnqContact.PARENT_TYPE
                    .CONTACT_NAME = lnqContact.CONTACT_NAME
                    .TELEPHONE = lnqContact.TELEPHONE
                    .FAX = lnqContact.FAX
                    .EMAIL = lnqContact.EMAIL
                    .POSITION = lnqContact.POSITION
                    exc = .InsertData(Username, trans.Trans)
                End With

                If exc.IsSuccess = False Then
                    trans.RollbackTransaction()
                    ret.IsSuccess = False
                    ret.ErrorMessage = exc.ErrorMessage()
                    Return ret
                End If
            End If


            '--TB_Project_File
            '#######################
            If Not DTProjectFile Is Nothing Then
                Dim cf As MsSysconfigLinqDB = ODAENG.GetSysconfig(trans)
                Dim FolderPath As String = cf.UPLOAD_PATH
                If FolderPath.EndsWith("\") = False Then
                    FolderPath += "\"
                End If

                If IO.Directory.Exists(FolderPath) = False Then
                    IO.Directory.CreateDirectory(FolderPath)
                End If

                For i As Integer = 0 To DTProjectFile.Rows.Count - 1
                    Dim ProjectFileID As Long = Convert.ToInt64(DTProjectFile.Rows(i)("file_id"))
                    If ProjectFileID = 0 Then
                        'บันทึกเฉพาะไฟล์ที่ถูก Upload ใหม่เท่านั้น เพราะเป็นการ Insert อย่างเดียว
                        Dim fInfo As New IO.FileInfo(DTProjectFile.Rows(i)("file_path"))
                        Dim OriginalFileName As String = DTProjectFile.Rows(i)("original_file_name")

                        Dim _lnqProjectFile As New TbProjectFileLinqDB
                        _lnqProjectFile.PROJECT_ID = _lnqProject.ID
                        _lnqProjectFile.ORIGINAL_FILE_NAME = OriginalFileName
                        _lnqProjectFile.CONTENT_TYPE = fInfo.Extension
                        _lnqProjectFile.DESCRIPTION = ""

                        'Format FILE_PATH # PROJECT_ID_yyyyMMddHHmmssfff
                        _lnqProjectFile.FILE_PATH = FolderPath & _lnqProject.PROJECT_ID & "_" & DateTime.Now.ToString("yyyyMMddHHmmssfff") & fInfo.Extension
                        exc = _lnqProjectFile.InsertData(Username, trans.Trans)

                        If exc.IsSuccess = False Then
                            trans.RollbackTransaction()
                            ret.IsSuccess = False
                            ret.ErrorMessage = exc.ErrorMessage()
                            Return ret
                        Else
                            'Move File from Temp to Upload Folder

                            If IO.File.Exists(fInfo.FullName) = True Then
                                IO.File.Move(fInfo.FullName, _lnqProjectFile.FILE_PATH)
                            End If
                        End If
                        fInfo = Nothing
                    End If
                Next
            End If
            '####### TB_Project_File

            '--TB_Project_Section
            Dim p_ps(1) As SqlParameter
            p_ps(0) = SqlDB.SetText("@_PROJECT_ID", _lnqProject.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Project_Section WHERE project_id=@_PROJECT_ID", trans.Trans, p_ps)

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            If Not DTProjectSection Is Nothing Then
                For i As Integer = 0 To DTProjectSection.Rows.Count - 1
                    Dim country_id As String = DTProjectSection.Rows(i)("country_id").ToString()
                    Dim organize_id As String = DTProjectSection.Rows(i)("organize_id").ToString()

                    If country_id <> "" AndAlso organize_id <> "" Then
                        Dim _lnqProjectSection As New TbProjectSectionLinqDB
                        With _lnqProjectSection
                            .PROJECT_ID = _lnqProject.ID
                            .COUNTRY_ID = country_id
                            .ORGANIZE_ID = organize_id

                            exc = .InsertData(Username, trans.Trans)
                        End With

                        If exc.IsSuccess = False Then
                            trans.RollbackTransaction()
                            ret.IsSuccess = False
                            ret.ErrorMessage = exc.ErrorMessage()
                            Return ret
                        End If
                    End If

                Next
            End If


            '---- Project Plan
            '--TB_Project_Plan
            Dim p_pjp(1) As SqlParameter
            p_pjp(0) = SqlDB.SetText("@_PROJECT_ID", _lnqProject.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_ProjectPlan WHERE project_id=@_PROJECT_ID", trans.Trans, p_pjp)
            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            Dim _lnqProjectPlan As New TbProjectplanLinqDB
            With _lnqProjectPlan
                .PROJECT_ID = _lnqProject.ID
                .PLAN_ID = lnqprojectplan.PLAN_ID

                exc = .InsertData(Username, trans.Trans)
            End With

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If



            trans.CommitTransaction()
            ret.IsSuccess = True
            ret.ErrorMessage = ""
            ret.PROJECT_ID = _lnqProject.PROJECT_ID
            ret.ID = _lnqProject.ID
        Catch ex As Exception
            trans.RollbackTransaction()
            ret.IsSuccess = False
            ret.ErrorMessage = ex.ToString()
        End Try

        Return ret
    End Function

    Function SaveProjectActivity_Disbursement(ID_Activity As String, UserName As String, DtDisbursemen As DataTable, DtAdministrativeCost As DataTable, DT_ProjectCountryRe As DataTable) As ProcessReturnInfo
        Dim ret As New ProcessReturnInfo
        Dim trans As New TransactionDB

        Try
            Dim exc As New ExecuteDataInfo
            For i As Integer = 0 To DtDisbursemen.Rows.Count - 1

                Dim lnqActivityTypeDisbursement As New TbActivityTypeDisbursementLinqDB
                With lnqActivityTypeDisbursement
                    .PAYMENT_DATE = Converter.StringToDate(DtDisbursemen.Rows(i)("date_payment"), "dd/MM/yyyy")
                    .AMOUNT = CDbl(DtDisbursemen.Rows(i)("amount"))
                    .ACTIVITY_ID = ID_Activity

                    If .ID > 0 Then
                        exc = .UpdateData(UserName, trans.Trans)
                    Else
                        exc = .InsertData(UserName, trans.Trans)
                    End If
                End With
                If exc.IsSuccess = False Then
                    trans.RollbackTransaction()
                    ret.IsSuccess = False
                    ret.ErrorMessage = exc.ErrorMessage()
                    Return ret
                End If
            Next

            For i As Integer = 0 To DT_ProjectCountryRe.Rows.Count - 1

                Dim InqCountryRecipient As New TbCountryRecipientLinqDB
                With InqCountryRecipient
                    .COUNTRY_NODE_ID = DT_ProjectCountryRe.Rows(i)("country_node_id")
                    .AMOUT_PERSON = CDbl(DT_ProjectCountryRe.Rows(i)("amout_person"))
                    .ACTIVITY_ID = ID_Activity

                    If .ID > 0 Then
                        exc = .UpdateData(UserName, trans.Trans)
                    Else
                        exc = .InsertData(UserName, trans.Trans)
                    End If
                End With
                If exc.IsSuccess = False Then
                    trans.RollbackTransaction()
                    ret.IsSuccess = False
                    ret.ErrorMessage = exc.ErrorMessage()
                    Return ret
                End If
            Next

            For i As Integer = 0 To DtAdministrativeCost.Rows.Count - 1
                Dim lnqActivityTypeAdministrativeCost As New TbActivityTypeAdministrativeCostLinqDB
                With lnqActivityTypeAdministrativeCost
                    .ID = DtAdministrativeCost.Rows(i)("id").ToString
                    .PAYMENT_DATE = Converter.StringToDate(DtAdministrativeCost.Rows(i)("date_payment"), "dd/MM/yyyy")
                    .AMOUNT = CDbl(DtAdministrativeCost.Rows(i)("amount"))
                    .DESCRIPTION = DtAdministrativeCost.Rows(i)("Description")
                    .ACTIVITY_ID = ID_Activity

                    If .ID > 0 Then
                        exc = .UpdateData(UserName, trans.Trans)
                    Else
                        exc = .InsertData(UserName, trans.Trans)
                    End If

                End With
                If exc.IsSuccess = False Then
                    trans.RollbackTransaction()
                    ret.IsSuccess = False
                    ret.ErrorMessage = exc.ErrorMessage()
                    Return ret
                End If
            Next

            trans.CommitTransaction()
            ret.IsSuccess = True
            ret.ErrorMessage = ""
        Catch ex As Exception
            trans.RollbackTransaction()
            ret.IsSuccess = False
            ret.ErrorMessage = ex.ToString()
        End Try

        Return ret
    End Function

    Public Function SaveProjectActivity(lnqActivity As TbActivityLinqDB, DTBudget As DataTable, DTComponent As DataTable, DTInkind As DataTable, DTRecipientPerson As DataTable, DTRecipentCountry As DataTable, DTDisbursement As DataTable, DTDisbursement_Con As DataTable, Username As String, DT_ProjectCountryRe As DataTable, DtDisbursemen As DataTable, DtAdministrativeCost As DataTable, DtRecipient As DataTable, DtCountry As DataTable, DtOrganize As DataTable) As ProcessReturnInfo
        Dim ret As New ProcessReturnInfo
        Dim trans As New TransactionDB
        Try
            'บันทึกข้อมูลตารางหลัก
            Dim exc As New ExecuteDataInfo
            Dim _lnqActivity As New TbActivityLinqDB
            With _lnqActivity
                .GetDataByPK(lnqActivity.ID, trans.Trans)

                .ACTIVITY_NAME = lnqActivity.ACTIVITY_NAME
                .DESCRIPTION = lnqActivity.DESCRIPTION
                .SECTOR_ID = lnqActivity.SECTOR_ID
                .SUB_SECTOR_ID = lnqActivity.SUB_SECTOR_ID
                .PLAN_START = lnqActivity.PLAN_START
                .PLAN_END = lnqActivity.PLAN_END
                .ACTUAL_START = lnqActivity.ACTUAL_START
                .ACTUAL_END = lnqActivity.ACTUAL_END
                .COMMITMENT_BUDGET = lnqActivity.COMMITMENT_BUDGET
                .DISBURSEMENT = lnqActivity.DISBURSEMENT
                .ADMINISTRATIVE = lnqActivity.ADMINISTRATIVE
                .ACTIVITY_STATUS = lnqActivity.ACTIVITY_STATUS
                .BENEFICIARY = lnqActivity.BENEFICIARY
                .ASSISTANCE_TYPE = lnqActivity.ASSISTANCE_TYPE
                .NOTIFY = lnqActivity.NOTIFY
                .PROJECT_ID = lnqActivity.PROJECT_ID
                .PARENT_ID = lnqActivity.PARENT_ID
                .ACTIVE_STATUS = lnqActivity.ACTIVE_STATUS
                .PAYMENT_DATE_GANT = lnqActivity.PAYMENT_DATE_GANT
                .COMMITMENT_BUDGET_LOAN = lnqActivity.COMMITMENT_BUDGET_LOAN
                .DISBURSEMENT_LOAN = lnqActivity.DISBURSEMENT_LOAN
                .PAYMENT_DATE_LOAN = lnqActivity.PAYMENT_DATE_LOAN
                .ADMINISTRATIVE = lnqActivity.ADMINISTRATIVE
                .PAYMENT_DATE_CONTRIBUTION = lnqActivity.PAYMENT_DATE_CONTRIBUTION
                .PAY_AS = lnqActivity.PAY_AS
                .DISBURSEMENT_TYPE = lnqActivity.DISBURSEMENT_TYPE
                .RECIPIENT_TYPE = lnqActivity.RECIPIENT_TYPE

                If .ID > 0 Then
                    exc = .UpdateData(Username, trans.Trans)
                Else
                    exc = .InsertData(Username, trans.Trans)
                End If
            End With
            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            ''ลบข้อมูลก่อนบันทึกใหม่
            '--TB_Activity_Budget
            Dim p_ab(1) As SqlParameter
            p_ab(0) = SqlDB.SetBigInt("@_ACTIVITY_ID", _lnqActivity.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Activity_Budget WHERE activity_id=@_ACTIVITY_ID", trans.Trans, p_ab)
            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage
                Return ret
            End If

            If Not DTBudget Is Nothing Then
                For i As Integer = 0 To DTBudget.Rows.Count - 1
                    Dim budget_year As String = DTBudget.Rows(i)("budget_year").ToString()
                    Dim budget_group_id As String = DTBudget.Rows(i)("budget_group_id").ToString()
                    Dim budget_sub_id As String = DTBudget.Rows(i)("budget_sub_id").ToString()
                    Dim amount As String = DTBudget.Rows(i)("amount").ToString()

                    If budget_group_id <> "" AndAlso budget_sub_id <> "" Then
                        Dim _lnqProjectBudget As New TbActivityBudgetLinqDB
                        With _lnqProjectBudget
                            .ACTIVITY_ID = _lnqActivity.ID
                            .BUDGET_YEAR = budget_year
                            .BUDGET_GROUP_ID = budget_group_id
                            .BUDGET_SUB_ID = budget_sub_id
                            .AMOUNT = amount

                            exc = .InsertData(Username, trans.Trans)
                        End With

                        If exc.IsSuccess = False Then
                            trans.RollbackTransaction()
                            ret.IsSuccess = False
                            ret.ErrorMessage = exc.ErrorMessage()
                            Return ret
                        End If
                    End If
                Next
            End If


            '-- TB_Activity_Component
            Dim p_pc(1) As SqlParameter
            p_pc(0) = SqlDB.SetText("@_ACTIVITY_ID", _lnqActivity.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Activity_Component WHERE activity_id=@_ACTIVITY_ID", trans.Trans, p_pc)
            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            If Not DTComponent Is Nothing Then
                For i As Integer = 0 To DTComponent.Rows.Count - 1
                    Dim component_id As String = DTComponent.Rows(i)("component_id").ToString()
                    'Dim amount As String = DTComponent.Rows(i)("amount").ToString()

                    If component_id <> "" Then
                        Dim _lnqActivityComponent As New TbActivityComponentLinqDB
                        With _lnqActivityComponent
                            .ACTIVITY_ID = _lnqActivity.ID
                            .COMPONENT_ID = component_id
                            '.AMOUNT = amount

                            exc = .InsertData(Username, trans.Trans)
                        End With

                        If exc.IsSuccess = False Then
                            trans.RollbackTransaction()
                            ret.IsSuccess = False
                            ret.ErrorMessage = exc.ErrorMessage()
                            Return ret
                        End If
                    End If
                Next
            End If


            '-- TB_Activity_Inkind 
            Dim p_ik(1) As SqlParameter
            p_ik(0) = SqlDB.SetText("@_ACTIVITY_ID", _lnqActivity.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Activity_Inkind WHERE activity_id=@_ACTIVITY_ID", trans.Trans, p_ik)

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            If Not DTInkind Is Nothing Then
                For i As Integer = 0 To DTInkind.Rows.Count - 1
                    Dim inkind_id As String = DTInkind.Rows(i)("inkind_id").ToString()
                    ' Dim estimate As String = DTInkind.Rows(i)("estimate").ToString()

                    If inkind_id <> "" Then
                        Dim _lnqActivityInkind As New TbActivityInkindLinqDB
                        With _lnqActivityInkind
                            .ACTIVITY_ID = _lnqActivity.ID
                            .INKIND_ID = inkind_id
                            '.ESTIMATE = estimate

                            exc = .InsertData(Username, trans.Trans)
                        End With

                        If exc.IsSuccess = False Then
                            trans.RollbackTransaction()
                            ret.IsSuccess = False
                            ret.ErrorMessage = exc.ErrorMessage()
                            Return ret
                        End If
                    End If

                Next
            End If


            '-- DTRecipientPerson  TB_Activity_Recipience
            Dim p_rp(1) As SqlParameter
            p_rp(0) = SqlDB.SetText("@_ACTIVITY_ID", _lnqActivity.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Activity_Recipience WHERE activity_id=@_ACTIVITY_ID", trans.Trans, p_rp)

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            If Not DTRecipientPerson Is Nothing Then
                For i As Integer = 0 To DTRecipientPerson.Rows.Count - 1
                    'Dim country_id As String = DTRecipientPerson.Rows(i)("country_id").ToString()
                    Dim recipient_id As String = DTRecipientPerson.Rows(i)("recipient_id").ToString()

                    If recipient_id <> "" Then
                        Dim _lnqActivityRecipience As New TbActivityRecipienceLinqDB
                        With _lnqActivityRecipience
                            .ACTIVITY_ID = _lnqActivity.ID
                            .RECIPIENT_ID = recipient_id

                            exc = .InsertData(Username, trans.Trans)
                        End With

                        If exc.IsSuccess = False Then
                            trans.RollbackTransaction()
                            ret.IsSuccess = False
                            ret.ErrorMessage = exc.ErrorMessage()
                            Return ret
                        End If
                    End If

                Next
            End If

            '-- DTRecipentCountry  TB_Activity_Recipience
            If Not DTRecipentCountry Is Nothing Then
                For i As Integer = 0 To DTRecipentCountry.Rows.Count - 1
                    Dim country_id As String = DTRecipentCountry.Rows(i)("country_id").ToString()

                    If country_id <> "" Then
                        Dim _lnqActivityRecipience As New TbActivityRecipienceLinqDB
                        With _lnqActivityRecipience
                            .ACTIVITY_ID = _lnqActivity.ID
                            .COUNTRY_ID = country_id

                            exc = .InsertData(Username, trans.Trans)
                        End With

                        If exc.IsSuccess = False Then
                            trans.RollbackTransaction()
                            ret.IsSuccess = False
                            ret.ErrorMessage = exc.ErrorMessage()
                            Return ret
                        End If
                    End If
                Next
            End If

            '-- DTDisbursement TB_Activity_Disbursement
            Dim p_dis_L(1) As SqlParameter
            p_dis_L(0) = SqlDB.SetText("@_ACTIVITY_ID", _lnqActivity.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Activity_Disbursement WHERE activity_id=@_ACTIVITY_ID", trans.Trans, p_dis_L)

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            If Not DTDisbursement Is Nothing Then
                For i As Integer = 0 To DTDisbursement.Rows.Count - 1
                    Dim type_id As String = DTDisbursement.Rows(i)("type_id").ToString()
                    Dim country_id As String = DTDisbursement.Rows(i)("country_id").ToString()
                    Dim txtPaymentDate As DateTime = DTDisbursement.Rows(i)("payment_date").ToString()
                    Dim txtAmount As String = DTDisbursement.Rows(i)("amount").ToString()

                    Dim _lnqActivityDisbursement As New TbActivityDisbursementLinqDB
                    With _lnqActivityDisbursement
                        .ACTIVITY_ID = _lnqActivity.ID
                        .TYPE_ID = type_id
                        .COUNTRY_ID = country_id
                        .PAYMENT_DATE = txtPaymentDate
                        .AMOUNT = txtAmount

                        exc = .InsertData(Username, trans.Trans)
                    End With

                    If exc.IsSuccess = False Then
                        trans.RollbackTransaction()
                        ret.IsSuccess = False
                        ret.ErrorMessage = exc.ErrorMessage()
                        Return ret
                    End If
                Next
            End If

            'DtDisbursemen
            If Not DtDisbursemen Is Nothing Then
                Dim p_Disbursemen(1) As SqlParameter
                p_Disbursemen(0) = SqlDB.SetText("@_ACTIVITY_ID", _lnqActivity.ID)
                exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Activity_Type_Disbursement WHERE activity_id=@_ACTIVITY_ID", trans.Trans, p_Disbursemen)

                For i As Integer = 0 To DtDisbursemen.Rows.Count - 1
                    Dim lnqActivityTypeDisbursement As New TbActivityTypeDisbursementLinqDB
                    With lnqActivityTypeDisbursement
                        .PAYMENT_DATE = Converter.StringToDate(DtDisbursemen.Rows(i)("date_payment"), "dd/MM/yyyy")
                        .AMOUNT = CDbl(DtDisbursemen.Rows(i)("amount"))
                        .ACTIVITY_ID = _lnqActivity.ID

                        If .ID > 0 Then
                            exc = .UpdateData(Username, trans.Trans)
                        Else
                            exc = .InsertData(Username, trans.Trans)
                        End If
                    End With
                    If exc.IsSuccess = False Then
                        trans.RollbackTransaction()
                        ret.IsSuccess = False
                        ret.ErrorMessage = exc.ErrorMessage()
                        Return ret
                    End If
                Next
            End If


            'DtAdministrativeCost
            If Not DtAdministrativeCost Is Nothing Then
                Dim p_AdministrativeCost(1) As SqlParameter
                p_AdministrativeCost(0) = SqlDB.SetText("@_ACTIVITY_ID", _lnqActivity.ID)
                exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Activity_Type_Administrative_Cost WHERE activity_id=@_ACTIVITY_ID", trans.Trans, p_AdministrativeCost)

                For i As Integer = 0 To DtAdministrativeCost.Rows.Count - 1
                    Dim lnqActivityTypeAdministrativeCost As New TbActivityTypeAdministrativeCostLinqDB
                    With lnqActivityTypeAdministrativeCost
                        .PAYMENT_DATE = Converter.StringToDate(DtAdministrativeCost.Rows(i)("date_payment"), "dd/MM/yyyy")
                        .AMOUNT = CDbl(DtAdministrativeCost.Rows(i)("amount"))
                        .DESCRIPTION = DtAdministrativeCost.Rows(i)("Description")
                        .ACTIVITY_ID = _lnqActivity.ID

                        If .ID > 0 Then
                            exc = .UpdateData(Username, trans.Trans)
                        Else
                            exc = .InsertData(Username, trans.Trans)
                        End If

                    End With
                    If exc.IsSuccess = False Then
                        trans.RollbackTransaction()
                        ret.IsSuccess = False
                        ret.ErrorMessage = exc.ErrorMessage()
                        Return ret
                    End If
                Next
            End If


            'DT_ProjectCountryRe
            If Not DT_ProjectCountryRe Is Nothing Then
                Dim p_dis_CountryRe(1) As SqlParameter
                p_dis_CountryRe(0) = SqlDB.SetText("@_ACTIVITY_ID", _lnqActivity.ID)
                exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Country_Recipient WHERE activity_id=@_ACTIVITY_ID", trans.Trans, p_dis_CountryRe)

                If exc.IsSuccess = False Then
                    trans.RollbackTransaction()
                    ret.IsSuccess = False
                    ret.ErrorMessage = exc.ErrorMessage()
                    Return ret
                End If

                For i As Integer = 0 To DT_ProjectCountryRe.Rows.Count - 1
                    Dim InqCountryRecipient As New TbCountryRecipientLinqDB
                    With InqCountryRecipient
                        .COUNTRY_NODE_ID = DT_ProjectCountryRe.Rows(i)("country_node_id")
                        .AMOUT_PERSON = CDbl(DT_ProjectCountryRe.Rows(i)("amout_person"))
                        .ACTIVITY_ID = _lnqActivity.ID

                        If .ID > 0 Then
                            exc = .UpdateData(Username, trans.Trans)
                        Else
                            exc = .InsertData(Username, trans.Trans)
                        End If
                    End With
                    If exc.IsSuccess = False Then
                        trans.RollbackTransaction()
                        ret.IsSuccess = False
                        ret.ErrorMessage = exc.ErrorMessage()
                        Return ret
                    End If
                Next
            End If

            'DtRecipient
            If Not DtRecipient Is Nothing Then
                Dim p_dis_CountryRe(1) As SqlParameter
                p_dis_CountryRe(0) = SqlDB.SetText("@_ACTIVITY_ID", _lnqActivity.ID)
                exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Activity_Recipience WHERE activity_id=@_ACTIVITY_ID", trans.Trans, p_dis_CountryRe)

                If exc.IsSuccess = False Then
                    trans.RollbackTransaction()
                    ret.IsSuccess = False
                    ret.ErrorMessage = exc.ErrorMessage()
                    Return ret
                End If

                For i As Integer = 0 To DtRecipient.Rows.Count - 1
                    Dim InqCountryRecipient As New TbActivityRecipienceLinqDB
                    With InqCountryRecipient
                        .COUNTRY_ID = DtRecipient.Rows(i)("country_id")
                        .RECIPIENT_ID = DtRecipient.Rows(i)("recipient_id")
                        .ACTIVITY_ID = _lnqActivity.ID

                        If .ID > 0 Then
                            exc = .UpdateData(Username, trans.Trans)
                        Else
                            exc = .InsertData(Username, trans.Trans)
                        End If
                    End With
                    If exc.IsSuccess = False Then
                        trans.RollbackTransaction()
                        ret.IsSuccess = False
                        ret.ErrorMessage = exc.ErrorMessage()
                        Return ret
                    End If
                Next
            End If

            If Not DtCountry Is Nothing Then
                Dim p_dis_Country(1) As SqlParameter
                p_dis_Country(0) = SqlDB.SetText("@_ACTIVITY_ID", _lnqActivity.ID)
                exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Activity_Country WHERE activity_id=@_ACTIVITY_ID", trans.Trans, p_dis_Country)

                If exc.IsSuccess = False Then
                    trans.RollbackTransaction()
                    ret.IsSuccess = False
                    ret.ErrorMessage = exc.ErrorMessage()
                    Return ret
                End If

                For i As Integer = 0 To DtCountry.Rows.Count - 1
                    Dim InqCountryRecipient As New TbActivityCountryLinqDB
                    With InqCountryRecipient
                        .COUNTRY_NODE_ID = DtCountry.Rows(i)("country_node_id")
                        .ACTIVITY_ID = _lnqActivity.ID
                        If .ID > 0 Then
                            exc = .UpdateData(Username, trans.Trans)
                        Else
                            exc = .InsertData(Username, trans.Trans)
                        End If
                    End With
                    If exc.IsSuccess = False Then
                        trans.RollbackTransaction()
                        ret.IsSuccess = False
                        ret.ErrorMessage = exc.ErrorMessage()
                        Return ret
                    End If
                Next
            End If

            If Not DtOrganize Is Nothing Then
                Dim p_dis_Organize(1) As SqlParameter
                p_dis_Organize(0) = SqlDB.SetText("@_ACTIVITY_ID", _lnqActivity.ID)
                exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Activity_Organization WHERE activity_id=@_ACTIVITY_ID", trans.Trans, p_dis_Organize)

                If exc.IsSuccess = False Then
                    trans.RollbackTransaction()
                    ret.IsSuccess = False
                    ret.ErrorMessage = exc.ErrorMessage()
                    Return ret
                End If

                For i As Integer = 0 To DtOrganize.Rows.Count - 1
                    Dim InqOrganize As New TbActivityOrganizationLinqDB
                    With InqOrganize
                        .COUNTRY_ID = DtOrganize.Rows(i)("country_id")
                        .ORGANIZE_ID = DtOrganize.Rows(i)("organize_id")
                        .ACTIVITY_ID = _lnqActivity.ID
                        If .ID > 0 Then
                            exc = .UpdateData(Username, trans.Trans)
                        Else
                            exc = .InsertData(Username, trans.Trans)
                        End If
                    End With
                    If exc.IsSuccess = False Then
                        trans.RollbackTransaction()
                        ret.IsSuccess = False
                        ret.ErrorMessage = exc.ErrorMessage()
                        Return ret
                    End If
                Next
            End If


            '-- DTDisbursement_Con TB_Activity_Disbursement_Contribuition
            Dim p_dis_Con(1) As SqlParameter
            p_dis_Con(0) = SqlDB.SetText("@_ACTIVITY_ID", _lnqActivity.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Activity_Disbursement_Contribuition WHERE activity_id=@_ACTIVITY_ID", trans.Trans, p_dis_Con)

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            If Not DTDisbursement_Con Is Nothing Then
                For i As Integer = 0 To DTDisbursement_Con.Rows.Count - 1
                    Dim title As String = DTDisbursement_Con.Rows(i)("title").ToString()
                    Dim multilateral_id As String = DTDisbursement_Con.Rows(i)("multilateral_id").ToString()
                    Dim txtPaymentDate As DateTime = DTDisbursement_Con.Rows(i)("payment_date").ToString()
                    Dim txtAmount As String = DTDisbursement_Con.Rows(i)("amount").ToString()

                    Dim _lnqActivityDisbursement As New TbActivityDisbursementContribuitionLinqDB
                    With _lnqActivityDisbursement
                        .ACTIVITY_ID = _lnqActivity.ID
                        .TITLE = title
                        .MULTILATERAL_ID = multilateral_id
                        .PAYMENT_DATE = txtPaymentDate
                        .AMOUNT = txtAmount

                        exc = .InsertData(Username, trans.Trans)
                    End With

                    If exc.IsSuccess = False Then
                        trans.RollbackTransaction()
                        ret.IsSuccess = False
                        ret.ErrorMessage = exc.ErrorMessage()
                        Return ret
                    End If
                Next
            End If

            trans.CommitTransaction()
            ret.IsSuccess = True
            ret.ErrorMessage = ""
        Catch ex As Exception
            trans.RollbackTransaction()
            ret.IsSuccess = False
            ret.ErrorMessage = ex.ToString()
        End Try
        Return ret
    End Function

    Public Function SaveProjectActivityforPlanExpand(lnqActivity As TbActivityLinqDB, DTBudget As DataTable, DTComponent As DataTable, DTInkind As DataTable, DTRecipientPerson As DataTable, DTRecipentCountry As DataTable, DTDisbursement As DataTable, DTDisbursement_Con As DataTable, Username As String, DT_ProjectCountryRe As DataTable, DtDisbursemen As DataTable, DtAdministrativeCost As DataTable, DtRecipient As DataTable, template_id As Long, DT_UCProjectCountry As DataTable, DtOrganize As DataTable) As Double
        Dim ret As New ProcessReturnInfo
        Dim trans As New TransactionDB
        Dim aid As Double

        Try
            'บันทึกข้อมูลตารางหลัก
            Dim exc As New ExecuteDataInfo
            Dim _lnqActivity As New TbActivityLinqDB
            With _lnqActivity
                .GetDataByPK(lnqActivity.ID, trans.Trans)

                .ACTIVITY_NAME = lnqActivity.ACTIVITY_NAME
                .DESCRIPTION = lnqActivity.DESCRIPTION
                .SECTOR_ID = lnqActivity.SECTOR_ID
                .SUB_SECTOR_ID = lnqActivity.SUB_SECTOR_ID
                .PLAN_START = lnqActivity.PLAN_START
                .PLAN_END = lnqActivity.PLAN_END
                .ACTUAL_START = lnqActivity.ACTUAL_START
                .ACTUAL_END = lnqActivity.ACTUAL_END
                .COMMITMENT_BUDGET = lnqActivity.COMMITMENT_BUDGET
                .DISBURSEMENT = lnqActivity.DISBURSEMENT
                .ADMINISTRATIVE = lnqActivity.ADMINISTRATIVE
                .ACTIVITY_STATUS = lnqActivity.ACTIVITY_STATUS
                .BENEFICIARY = lnqActivity.BENEFICIARY
                .ASSISTANCE_TYPE = lnqActivity.ASSISTANCE_TYPE
                .NOTIFY = lnqActivity.NOTIFY
                .PROJECT_ID = lnqActivity.PROJECT_ID
                .PARENT_ID = lnqActivity.PARENT_ID
                .PAYMENT_DATE_GANT = lnqActivity.PAYMENT_DATE_GANT
                .COMMITMENT_BUDGET_LOAN = lnqActivity.COMMITMENT_BUDGET_LOAN
                .DISBURSEMENT_LOAN = lnqActivity.DISBURSEMENT_LOAN
                .PAYMENT_DATE_LOAN = lnqActivity.PAYMENT_DATE_LOAN
                .ADMINISTRATIVE = lnqActivity.ADMINISTRATIVE
                .PAYMENT_DATE_CONTRIBUTION = lnqActivity.PAYMENT_DATE_CONTRIBUTION
                .PAY_AS = lnqActivity.PAY_AS
                .DISBURSEMENT_TYPE = lnqActivity.DISBURSEMENT_TYPE
                .RECIPIENT_TYPE = lnqActivity.RECIPIENT_TYPE

                If .ID > 0 Then
                    exc = .UpdateData(Username, trans.Trans)
                Else
                    exc = .InsertData(Username, trans.Trans)
                End If
            End With

            aid = _lnqActivity.ID
            ''ลบข้อมูลก่อนบันทึกใหม่
            '--TB_Activity_Budget
            Dim p_ab(1) As SqlParameter
            p_ab(0) = SqlDB.SetBigInt("@_ACTIVITY_ID", _lnqActivity.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Activity_Budget WHERE activity_id=@_ACTIVITY_ID", trans.Trans, p_ab)

            If Not DTBudget Is Nothing Then
                For i As Integer = 0 To DTBudget.Rows.Count - 1
                    Dim budget_year As String = DTBudget.Rows(i)("budget_year").ToString()
                    Dim budget_group_id As String = DTBudget.Rows(i)("budget_group_id").ToString()
                    Dim budget_sub_id As String = DTBudget.Rows(i)("budget_sub_id").ToString()
                    Dim amount As String = DTBudget.Rows(i)("amount").ToString()

                    If budget_group_id <> "" AndAlso budget_sub_id <> "" Then
                        Dim _lnqProjectBudget As New TbActivityBudgetLinqDB
                        With _lnqProjectBudget
                            .ACTIVITY_ID = _lnqActivity.ID
                            .BUDGET_YEAR = budget_year
                            .BUDGET_GROUP_ID = budget_group_id
                            .BUDGET_SUB_ID = budget_sub_id
                            .AMOUNT = amount

                            exc = .InsertData(Username, trans.Trans)
                        End With

                    End If
                Next
            End If


            Dim p_pet(1) As SqlParameter
            p_pet(0) = SqlDB.SetText("@_ACTIVITY_ID", _lnqActivity.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Expense_Template_Activity WHERE Activity_id=@_ACTIVITY_ID", trans.Trans, p_pet)

            If template_id <> 0 Then
                Dim _inqExpenseTemplate As New TbExpenseTemplateActivityLinqDB
                With _inqExpenseTemplate
                    .TEMPLATE_ID = template_id
                    .ACTIVITY_ID = _lnqActivity.ID

                    exc = .InsertData(Username, trans.Trans)
                End With
            End If


            '-- TB_Activity_Component
            Dim p_pc(1) As SqlParameter
            p_pc(0) = SqlDB.SetText("@_ACTIVITY_ID", _lnqActivity.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Activity_Component WHERE activity_id=@_ACTIVITY_ID", trans.Trans, p_pc)

            If Not DTComponent Is Nothing Then
                For i As Integer = 0 To DTComponent.Rows.Count - 1
                    Dim component_id As String = DTComponent.Rows(i)("component_id").ToString()
                    'Dim amount As String = DTComponent.Rows(i)("amount").ToString()

                    If component_id <> "" Then
                        Dim _lnqActivityComponent As New TbActivityComponentLinqDB
                        With _lnqActivityComponent
                            .ACTIVITY_ID = _lnqActivity.ID
                            .COMPONENT_ID = component_id
                            '.AMOUNT = amount

                            exc = .InsertData(Username, trans.Trans)
                        End With

                    End If
                Next
            End If


            '-- TB_Activity_Inkind 
            Dim p_ik(1) As SqlParameter
            p_ik(0) = SqlDB.SetText("@_ACTIVITY_ID", _lnqActivity.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Activity_Inkind WHERE activity_id=@_ACTIVITY_ID", trans.Trans, p_ik)

            If Not DTInkind Is Nothing Then
                For i As Integer = 0 To DTInkind.Rows.Count - 1
                    Dim inkind_id As String = DTInkind.Rows(i)("inkind_id").ToString()
                    ' Dim estimate As String = DTInkind.Rows(i)("estimate").ToString()

                    If inkind_id <> "" Then
                        Dim _lnqActivityInkind As New TbActivityInkindLinqDB
                        With _lnqActivityInkind
                            .ACTIVITY_ID = _lnqActivity.ID
                            .INKIND_ID = inkind_id
                            '.ESTIMATE = estimate

                            exc = .InsertData(Username, trans.Trans)
                        End With


                    End If

                Next
            End If


            '-- DTRecipientPerson  TB_Activity_Recipience
            Dim p_rp(1) As SqlParameter
            p_rp(0) = SqlDB.SetText("@_ACTIVITY_ID", _lnqActivity.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Activity_Recipience WHERE activity_id=@_ACTIVITY_ID", trans.Trans, p_rp)


            If Not DTRecipientPerson Is Nothing Then
                For i As Integer = 0 To DTRecipientPerson.Rows.Count - 1
                    'Dim country_id As String = DTRecipientPerson.Rows(i)("country_id").ToString()
                    Dim recipient_id As String = DTRecipientPerson.Rows(i)("recipient_id").ToString()

                    If recipient_id <> "" Then
                        Dim _lnqActivityRecipience As New TbActivityRecipienceLinqDB
                        With _lnqActivityRecipience
                            .ACTIVITY_ID = _lnqActivity.ID
                            .RECIPIENT_ID = recipient_id

                            exc = .InsertData(Username, trans.Trans)
                        End With


                    End If

                Next
            End If




            '-- DTRecipentCountry  TB_Activity_Recipience
            If Not DTRecipentCountry Is Nothing Then
                For i As Integer = 0 To DTRecipentCountry.Rows.Count - 1
                    Dim country_id As String = DTRecipentCountry.Rows(i)("country_id").ToString()

                    If country_id <> "" Then
                        Dim _lnqActivityRecipience As New TbActivityRecipienceLinqDB
                        With _lnqActivityRecipience
                            .ACTIVITY_ID = _lnqActivity.ID
                            .COUNTRY_ID = country_id

                            exc = .InsertData(Username, trans.Trans)
                        End With


                    End If
                Next
            End If



            Dim p_prc(1) As SqlParameter
            p_prc(0) = SqlDB.SetText("@_ACTIVITY_ID", _lnqActivity.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Activity_Country WHERE activity_id=@_ACTIVITY_ID", trans.Trans, p_prc)

            If Not DT_UCProjectCountry Is Nothing Then
                For i As Integer = 0 To DT_UCProjectCountry.Rows.Count - 1
                    Dim country_id As String = DT_UCProjectCountry.Rows(i)("country_node_id").ToString()

                    If country_id <> "" Then
                        Dim _lnqActivityRecipience As New TbActivityCountryLinqDB
                        With _lnqActivityRecipience
                            .ACTIVITY_ID = _lnqActivity.ID
                            .COUNTRY_NODE_ID = country_id

                            exc = .InsertData(Username, trans.Trans)
                        End With


                    End If
                Next
            End If

            '-- DTDisbursement TB_Activity_Disbursement
            Dim p_dis_L(1) As SqlParameter
            p_dis_L(0) = SqlDB.SetText("@_ACTIVITY_ID", _lnqActivity.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Activity_Disbursement WHERE activity_id=@_ACTIVITY_ID", trans.Trans, p_dis_L)

            If Not DTDisbursement Is Nothing Then
                For i As Integer = 0 To DTDisbursement.Rows.Count - 1
                    Dim type_id As String = DTDisbursement.Rows(i)("type_id").ToString()
                    Dim country_id As String = DTDisbursement.Rows(i)("country_id").ToString()
                    Dim txtPaymentDate As DateTime = DTDisbursement.Rows(i)("payment_date").ToString()
                    Dim txtAmount As String = DTDisbursement.Rows(i)("amount").ToString()

                    Dim _lnqActivityDisbursement As New TbActivityDisbursementLinqDB
                    With _lnqActivityDisbursement
                        .ACTIVITY_ID = _lnqActivity.ID
                        .TYPE_ID = type_id
                        .COUNTRY_ID = country_id
                        .PAYMENT_DATE = txtPaymentDate
                        .AMOUNT = txtAmount

                        exc = .InsertData(Username, trans.Trans)
                    End With


                Next
            End If

            'DtDisbursemen
            If Not DtDisbursemen Is Nothing Then
                Dim p_Disbursemen(1) As SqlParameter
                p_Disbursemen(0) = SqlDB.SetText("@_ACTIVITY_ID", _lnqActivity.ID)
                exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Activity_Type_Disbursement WHERE activity_id=@_ACTIVITY_ID", trans.Trans, p_Disbursemen)

                For i As Integer = 0 To DtDisbursemen.Rows.Count - 1
                    Dim lnqActivityTypeDisbursement As New TbActivityTypeDisbursementLinqDB
                    With lnqActivityTypeDisbursement
                        .PAYMENT_DATE = Converter.StringToDate(DtDisbursemen.Rows(i)("date_payment"), "dd/MM/yyyy")
                        .AMOUNT = CDbl(DtDisbursemen.Rows(i)("amount"))
                        .ACTIVITY_ID = _lnqActivity.ID

                        If .ID > 0 Then
                            exc = .UpdateData(Username, trans.Trans)
                        Else
                            exc = .InsertData(Username, trans.Trans)
                        End If
                    End With

                Next
            End If


            'DtAdministrativeCost
            If Not DtAdministrativeCost Is Nothing Then
                Dim p_AdministrativeCost(1) As SqlParameter
                p_AdministrativeCost(0) = SqlDB.SetText("@_ACTIVITY_ID", _lnqActivity.ID)
                exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Activity_Type_Administrative_Cost WHERE activity_id=@_ACTIVITY_ID", trans.Trans, p_AdministrativeCost)

                For i As Integer = 0 To DtAdministrativeCost.Rows.Count - 1
                    Dim lnqActivityTypeAdministrativeCost As New TbActivityTypeAdministrativeCostLinqDB
                    With lnqActivityTypeAdministrativeCost
                        .PAYMENT_DATE = Converter.StringToDate(DtAdministrativeCost.Rows(i)("date_payment"), "dd/MM/yyyy")
                        .AMOUNT = CDbl(DtAdministrativeCost.Rows(i)("amount"))
                        .DESCRIPTION = DtAdministrativeCost.Rows(i)("Description")
                        .ACTIVITY_ID = _lnqActivity.ID

                        If .ID > 0 Then
                            exc = .UpdateData(Username, trans.Trans)
                        Else
                            exc = .InsertData(Username, trans.Trans)
                        End If

                    End With

                Next
            End If


            'DT_ProjectCountryRe
            If Not DT_ProjectCountryRe Is Nothing Then
                Dim p_dis_CountryRe(1) As SqlParameter
                p_dis_CountryRe(0) = SqlDB.SetText("@_ACTIVITY_ID", _lnqActivity.ID)
                exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Country_Recipient WHERE activity_id=@_ACTIVITY_ID", trans.Trans, p_dis_CountryRe)


                For i As Integer = 0 To DT_ProjectCountryRe.Rows.Count - 1
                    Dim InqCountryRecipient As New TbCountryRecipientLinqDB
                    With InqCountryRecipient
                        .COUNTRY_NODE_ID = DT_ProjectCountryRe.Rows(i)("country_node_id")
                        .AMOUT_PERSON = CDbl(DT_ProjectCountryRe.Rows(i)("amout_person"))
                        .ACTIVITY_ID = _lnqActivity.ID

                        If .ID > 0 Then
                            exc = .UpdateData(Username, trans.Trans)
                        Else
                            exc = .InsertData(Username, trans.Trans)
                        End If
                    End With

                Next
            End If

            'DtRecipient
            If Not DtRecipient Is Nothing Then
                Dim p_dis_CountryRe(1) As SqlParameter
                p_dis_CountryRe(0) = SqlDB.SetText("@_ACTIVITY_ID", _lnqActivity.ID)
                exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Activity_Recipience WHERE activity_id=@_ACTIVITY_ID", trans.Trans, p_dis_CountryRe)

                For i As Integer = 0 To DtRecipient.Rows.Count - 1
                    Dim InqCountryRecipient As New TbActivityRecipienceLinqDB
                    With InqCountryRecipient
                        .COUNTRY_ID = DtRecipient.Rows(i)("country_id")
                        .RECIPIENT_ID = DtRecipient.Rows(i)("recipient_id")
                        .ACTIVITY_ID = _lnqActivity.ID

                        If .ID > 0 Then
                            exc = .UpdateData(Username, trans.Trans)
                        Else
                            exc = .InsertData(Username, trans.Trans)
                        End If
                    End With

                Next
            End If


            If Not DtOrganize Is Nothing Then
                Dim p_dis_Organize(1) As SqlParameter
                p_dis_Organize(0) = SqlDB.SetText("@_ACTIVITY_ID", _lnqActivity.ID)
                exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Activity_Organization WHERE activity_id=@_ACTIVITY_ID", trans.Trans, p_dis_Organize)
                For i As Integer = 0 To DtOrganize.Rows.Count - 1
                    Dim InqOrganize As New TbActivityOrganizationLinqDB
                    With InqOrganize
                        .COUNTRY_ID = DtOrganize.Rows(i)("country_id")
                        .ORGANIZE_ID = DtOrganize.Rows(i)("organize_id")
                        .ACTIVITY_ID = _lnqActivity.ID
                        If .ID > 0 Then
                            exc = .UpdateData(Username, trans.Trans)
                        Else
                            exc = .InsertData(Username, trans.Trans)
                        End If
                    End With
                Next
            End If

            '-- DTDisbursement_Con TB_Activity_Disbursement_Contribuition
            Dim p_dis_Con(1) As SqlParameter
            p_dis_Con(0) = SqlDB.SetText("@_ACTIVITY_ID", _lnqActivity.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Activity_Disbursement_Contribuition WHERE activity_id=@_ACTIVITY_ID", trans.Trans, p_dis_Con)



            If Not DTDisbursement_Con Is Nothing Then
                For i As Integer = 0 To DTDisbursement_Con.Rows.Count - 1
                    Dim title As String = DTDisbursement_Con.Rows(i)("title").ToString()
                    Dim multilateral_id As String = DTDisbursement_Con.Rows(i)("multilateral_id").ToString()
                    Dim txtPaymentDate As DateTime = DTDisbursement_Con.Rows(i)("payment_date").ToString()
                    Dim txtAmount As String = DTDisbursement_Con.Rows(i)("amount").ToString()

                    Dim _lnqActivityDisbursement As New TbActivityDisbursementContribuitionLinqDB
                    With _lnqActivityDisbursement
                        .ACTIVITY_ID = _lnqActivity.ID
                        .TITLE = title
                        .MULTILATERAL_ID = multilateral_id
                        .PAYMENT_DATE = txtPaymentDate
                        .AMOUNT = txtAmount

                        exc = .InsertData(Username, trans.Trans)
                    End With


                Next
            End If

            trans.CommitTransaction()
            ret.IsSuccess = True
            ret.ErrorMessage = ""
        Catch ex As Exception
            trans.RollbackTransaction()
            ret.IsSuccess = False
            ret.ErrorMessage = ex.ToString()
        End Try

        Return aid
    End Function

    Function DeleteProject(ByVal project_id As String) As Boolean
        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text

                '## delete all activity in this project
                Dim dtActivity As DataTable = GetProjectActivityListForDelete(project_id)
                For i As Integer = 0 To dtActivity.Rows.Count - 1
                    DeleteActivity(dtActivity.Rows(i)("id"))
                Next
                '## delete project
                .CommandText = "delete from TB_Project_Section where project_id=" & project_id & vbLf
                .CommandText &= "delete from TB_Project_File where project_id=" & project_id & vbLf
                .CommandText &= "delete from TB_OU_Contact where parent_id=" & project_id & " and parent_type = " & Contact_Parent_Type.Project & vbLf
                .CommandText &= "delete from TB_Project_CoFunding where project_id=" & project_id & vbLf
                .CommandText &= "delete from TB_Project_Implementing_Agency  where project_id=" & project_id & vbLf
                .CommandText &= "delete from TB_ProjectPlan where project_id=" & project_id & vbLf
                .CommandText &= "delete from tb_project  where id=" & project_id & vbLf
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Function DeleteProjectFile(ByVal ProjectFileID As Long) As Boolean
        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM TB_Project_File WHERE ID=" & ProjectFileID
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Function DeleteActivity(ByVal activity_id As String) As Boolean

        Dim Conn As New SqlConnection(ConnectionString)
        Conn.Open()
        Try
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "delete from TB_Activity_Component where activity_id in (select id from [dbo].[GetAllNodeActivity](" & activity_id & "))" & vbLf
                .CommandText &= "delete from TB_Activity_Inkind where activity_id in (select id from [dbo].[GetAllNodeActivity](" & activity_id & "))" & vbLf
                .CommandText &= "delete from TB_Activity_Recipience where activity_id In (Select id from [dbo].[GetAllNodeActivity](" & activity_id & "))" & vbLf
                .CommandText &= "delete from TB_Activity_Budget where activity_id In (Select id from [dbo].[GetAllNodeActivity](" & activity_id & "))" & vbLf
                .CommandText &= "delete from TB_Country_Recipient where activity_id In (Select id from [dbo].[GetAllNodeActivity](" & activity_id & "))" & vbLf
                .CommandText &= "delete from TB_Activity_Country where activity_id In (Select id from [dbo].[GetAllNodeActivity](" & activity_id & "))" & vbLf
                .CommandText &= "delete from TB_Activity_Recipience where activity_id In (Select id from [dbo].[GetAllNodeActivity](" & activity_id & "))" & vbLf
                .CommandText &= "delete from TB_Expense_Template_Activity where activity_id In (Select id from [dbo].[GetAllNodeActivity](" & activity_id & "))" & vbLf
                .ExecuteNonQuery()
                .Dispose()
            End With
        Catch ex As Exception
            Return False
        End Try


        Dim SQL As String = "Select id from TB_Activity_Expense_Header where activity_id In (Select id from [dbo].[GetAllNodeActivity](" & activity_id & "))"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)

        Try
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                For i As Integer = 0 To DT.Rows.Count - 1
                    Dim id As String = DT.Rows(i)("id").ToString
                    .CommandText = "delete from TB_Activity_Expense_Plan_Detail where Header_id =" & id & vbLf
                    .CommandText &= "delete from TB_Activity_Expense_Plan where Header_id =" & id & vbLf
                    .CommandText &= "delete from TB_Activity_Expense_Actual_Detail where Header_id =" & id & vbLf
                    .CommandText &= "delete from TB_Activity_Expense_Actual where Header_id =" & id & vbLf
                    .CommandText &= "DELETE FROM TB_Activity_Expense_Item WHERE header_id=" & id & vbLf
                    .ExecuteNonQuery()
                Next
                .CommandText = "delete from TB_Activity_Expense_Header where activity_id In (Select id from [dbo].[GetAllNodeActivity](" & activity_id & "))" & vbLf
                .CommandText &= "delete from TB_Activity where id In (Select id from [dbo].[GetAllNodeActivity](" & activity_id & "))" & vbLf
                .ExecuteNonQuery()
                .Dispose()
            End With
        Catch ex As Exception
            Return False
        End Try
        Conn.Close()
        Conn.Dispose()
        Return True

    End Function

    Public Function SetActivity(project_id As String, activity_type As String, mode As String, DisplayActivity_Type As String, ExpNodeID As String) As StringBuilder

        Dim str As New StringBuilder
        '##Gen Column Detail
        str.AppendLine("<table style='width:10000px;' runat='server' >")
        str.AppendLine("    <thead>")
        str.AppendLine("    <tr >")
        str.AppendLine("        <th style = 'width:300px' Class='tdbordertb tdborderl tdhearderinfo'>Activity Name</th>")
        str.AppendLine("        <th style = 'width:100px' Class='tdbordertb tdhearderinfo'>Start Date</th>")
        str.AppendLine("        <th style = 'width:100px' Class='tdbordertb tdhearderinfo'>End Date</th>")
        str.AppendLine("        <th style = 'width:100px;' Class='tdbordertb tdhearderinfo'>Duration</th>")


        If mode <> "view" Then
            str.AppendLine("        <th style = 'width:100px;' Class='tdbordertb tdhearderinfo'>Budget</th>")
            str.AppendLine("        <th style = 'width:30px;' Class='tdbordertb tdborderr tdhearderinfo'>")
            str.AppendLine("        </th>")
            str.AppendLine("        <th style = 'width:30px;' Class='tdbordertb tdborderr tdhearderinfo'>")
            str.AppendLine("        </th>")
            str.AppendLine("        <th style = 'width:30px;' Class='tdbordertb tdborderr tdhearderinfo'>")
            str.AppendLine("        </th>")
        Else
            str.AppendLine("        <th style = 'width:100px;' Class='tdbordertb tdborderr tdhearderinfo'>Budget</th>")
            str.AppendLine("        <th style = 'width:30px;' Class='tdbordertb tdborderr tdhearderinfo'>")
            str.AppendLine("        </th>")
        End If
        ''##End Gen Column Detail

        Try
            Dim dt As New DataTable
            dt = GetProjectActivityList(project_id)

            '##Gen Column Month
            Dim dtProject As New DataTable
            dtProject = GetProjectInfoByID(project_id)
            If dtProject.Rows.Count = 0 Then
                Return str
            End If

            Dim pjStartDate As DateTime = Convert.ToDateTime(dtProject.Rows(0)("start_date"))
            Dim pjEndDate As DateTime = Convert.ToDateTime(dtProject.Rows(0)("end_date"))
            Dim pjNowDate As DateTime = Convert.ToDateTime(dtProject.Rows(0)("now_date"))
            Dim monthyear_now As String = dtProject.Rows(0)("year_now").ToString() & dtProject.Rows(0)("month_now").ToString()

            Dim _dtProjectColumn As New DataTable
            With _dtProjectColumn
                .Columns.Add("seq")
                .Columns.Add("columnname")
            End With
            Dim _drProjectRow As DataRow

            Dim _seq As Integer = 1

            Dim _strDisplayActivityType As String = "yyyyMM"
            If DisplayActivity_Type = "year" Then
                _strDisplayActivityType = "yyyy"
            End If

            Dim LoopMonth As DateTime = pjStartDate
            Do
                Dim _strColumnName As String = LoopMonth.ToString(_strDisplayActivityType, New System.Globalization.CultureInfo("en-US"))
                Dim _columnname As String = _strColumnName
                If _strDisplayActivityType = "yyyyMM" Then
                    _columnname = GetColumnNameByMonth(_strColumnName)
                End If

                str.AppendLine("        <th style = 'width:100px' Class='tdborder tdhearder'>" & _columnname & "</th>")

                _drProjectRow = _dtProjectColumn.NewRow
                _drProjectRow("seq") = _seq
                _drProjectRow("columnname") = _strColumnName
                _dtProjectColumn.Rows.Add(_drProjectRow)
                _seq += 1
                LoopMonth = DateAdd(IIf(_strDisplayActivityType = "yyyy", DateInterval.Year, DateInterval.Month), 1, LoopMonth)
            Loop While LoopMonth.ToString(_strDisplayActivityType) <= pjEndDate.ToString(_strDisplayActivityType)

            str.AppendLine("        <th></th>")
            str.AppendLine("    </tr>")
            str.AppendLine("    </thead>")

            'Create Parent Node Level 0
            dt.DefaultView.RowFilter = "parent_id=0"
            Dim pDt As New DataTable
            pDt = dt.DefaultView.ToTable.Copy()
            If pDt.Rows.Count > 0 Then
                str = GenSubChildNode(str, 0, pDt, 0, dt, _dtProjectColumn, monthyear_now, mode, DisplayActivity_Type, ExpNodeID)
            End If
            pDt.Dispose()

            str.AppendLine("</table>")

        Catch ex As Exception
            Dim err = ex.Message.ToString()
        End Try
        Return str
    End Function

    Private Function GenSubChildNode(str As StringBuilder, ParentID As Long, pDt As DataTable, NodeLevel As Integer, dt As DataTable, dtpjcolumn As DataTable, monthyear_now As String, mode As String, DisplayActivity_Type As String, ExpNodeID As String) As StringBuilder
        'Gen Parent Node
        For i As Integer = 0 To pDt.Rows.Count - 1
            Dim _id As String = pDt.Rows(i)("id").ToString()
            Dim _childs As String = pDt.Rows(i)("childs").ToString()
            Dim _activity_name As String = pDt.Rows(i)("activity_name").ToString()
            Dim _plan_start As String = Convert.ToDateTime(pDt.Rows(i)("plan_start")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
            Dim _plan_end As String = Convert.ToDateTime(pDt.Rows(i)("plan_end")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
            Dim _duration As String = pDt.Rows(i)("duration").ToString()
            Dim _commitment_budget As String = pDt.Rows(i)("commitment_budget").ToString()

            Dim ChildNodeId As String = BuiltChildNodeID(_id)
            If ChildNodeId.Trim <> "" Then
                'ตัด , ที่อยู่ข้างหน้าออก
                ChildNodeId = ChildNodeId.Substring(2)
            End If

            Dim _NodeClass As String = ""
            If NodeLevel > 0 Then
                If ExpNodeID.IndexOf(ParentID & ";") > -1 Then
                    _NodeClass = "class='childNode" & NodeLevel & "'"
                Else
                    _NodeClass = "class='removed childNode" & NodeLevel & "'"
                End If
            End If

            str.AppendLine("    <tr id='Node" & _id & "' " & _NodeClass & "  onmouseover='ChangeBackgroundColor(this,txt" & _id & ")' onmouseout='RestoreBackgroundColor(this,txt" & _id & ")'  style='cursor:Default;'>")

            'str.AppendLine("        <td Class='tdbordertb tdborderl'>")

            'If _childs <> "0" Then
            '    str.AppendLine("            <span id='SpanNode" & _id & "' onclick=""toggleTreview(" & ChildNodeId & ");toggleImg('img" & _id & "','" & ParentID & "');""><img id='img" & _id & "' alt='Expand' src='dist/img/toggle-expand-icon.png' style='width: 10px;' /></span>")
            'Else
            '    'ใส่ช่องว่างด้านหน้าเพื่อให้เยื้องในกรณีที่ไม่มีปุ่ม Expand
            '    str.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;")
            'End If
            'str.AppendLine("            <input type = 'text' id='txt" & _id & "' value='" & _activity_name & "' size='30' style='border: none; cursor:Default;' ReadOnly />")
            'str.AppendLine("        </td>")
            'str.AppendLine("        <td Class='tdbordertb'>" & _plan_start & "</td>")
            'str.AppendLine("        <td Class='tdbordertb'>" & _plan_end & "</td>")
            'str.AppendLine("        <td Class='tdbordertb'>" & _duration & "</td>")


            'If mode <> "view" Then
            '    str.AppendLine("        <td Class='tdbordertb'>" & _commitment_budget & "</td>")
            '    str.AppendLine("        <td Class='tdbordertb tdborderr'>")
            '    str.AppendLine("            <center>")
            '    str.AppendLine("                <a href = '#' onclick='fnClickButton(""0"",""add""," & _id & ");'>")
            '    str.AppendLine("                    <img  id='btnOpenModal'  ><i Class='fa fa-plus text-success'></i></img>")
            '    str.AppendLine("                </a>")
            '    str.AppendLine("            </center>")
            '    str.AppendLine("        </td>")
            'Else
            '    str.AppendLine("        <td Class='tdbordertb tdborderr'>" & _commitment_budget & "</td>")
            '    'str.AppendLine("        <td Class='tdbordertb tdborderr'>&nbsp;")
            '    'str.AppendLine("        </td>")
            'End If


            str.AppendLine("        <td Class='tdbordertb tdborderr'>")
            str.AppendLine("            <center>")
            str.AppendLine("                <a href = '#' onclick='fnClickButton(" & _id & ",""edit""," & ParentID & ");'>")
            If mode <> "view" Then
                str.AppendLine("                    <img  id='btnOpenModal'  ><i Class='fa fa-edit text-success'></i></img>")
            Else
                str.AppendLine("                    <img  id='btnOpenModal'  ><i Class='fa fa-search text-success'></i></img>")
            End If

            str.AppendLine("                </a>")
            str.AppendLine("            </center>")
            str.AppendLine("        </td>")


            If mode <> "view" Then
                str.AppendLine("        <td Class='tdbordertb tdborderr'>")
                str.AppendLine("            <center>")
                str.AppendLine("                <a href = '#' onclick='btnDeleteClick(" & _id & ");'>")
                str.AppendLine("                    <img  id='btnOpenModal'  ><i Class='fa fa-trash text-danger'></i></img>")
                str.AppendLine("                </a>")
                str.AppendLine("            </center>")
                str.AppendLine("        </td>")
            End If


            Dim acStartMonthYear As DateTime = Convert.ToDateTime(pDt.Rows(i)("plan_start"))
            Dim acEndMonthYear As DateTime = Convert.ToDateTime(pDt.Rows(i)("plan_end"))
            Dim acLoopMonth As DateTime = acStartMonthYear

            Dim _dtActivityColumn As New DataTable
            With _dtActivityColumn
                .Columns.Add("seq")
                .Columns.Add("columnname")
                .Columns.Add("past")
            End With
            Dim _drActivityRow As DataRow

            Dim _strDisplayActivityType As String = "yyyyMM"
            If DisplayActivity_Type = "year" Then
                _strDisplayActivityType = "yyyy"
            End If
            Dim _seq As Integer = 1
            Do
                Dim _strColumnName As String = acLoopMonth.ToString(_strDisplayActivityType, New System.Globalization.CultureInfo("en-US"))
                _drActivityRow = _dtActivityColumn.NewRow
                _drActivityRow("seq") = _seq
                _drActivityRow("columnname") = _strColumnName

                If _strColumnName <= monthyear_now Then
                    _drActivityRow("past") = "Y"
                Else
                    _drActivityRow("past") = "N"
                End If
                _dtActivityColumn.Rows.Add(_drActivityRow)
                _seq += 1

                acLoopMonth = DateAdd(IIf(_strDisplayActivityType = "yyyy", DateInterval.Year, DateInterval.Month), 1, acLoopMonth)
            Loop While acLoopMonth.ToString(_strDisplayActivityType) <= acEndMonthYear.ToString(_strDisplayActivityType)

            '##Set Color
            For p As Integer = 0 To dtpjcolumn.Rows.Count - 1
                Dim _columnname As String = dtpjcolumn.Rows(p)("columnname").ToString()
                Dim _tmpdr As DataRow() = _dtActivityColumn.Select("columnname='" & _columnname & "'")
                If _tmpdr.Length > 0 Then
                    Dim _past As String = _tmpdr(0)("past").ToString()
                    If _past = "Y" Then
                        str.AppendLine("        <td Class='tdinprogress'></td>")
                    Else
                        str.AppendLine("        <td Class='tdallprogress'></td>")
                    End If
                Else
                    str.AppendLine("        <td >&nbsp;</td>")
                End If
            Next

            str.AppendLine("    </tr>")



            ''#################################
            'dt.DefaultView.RowFilter = "parent_id=" & _id
            'Dim cDt As New DataTable
            'cDt = dt.DefaultView.ToTable.Copy()
            'If cDt.Rows.Count > 0 Then
            '    'str.AppendLine("<tbody id = 'ChildNode" & _id & "' >")
            '    str = GenSubChildNode(str, _id, cDt, NodeLevel + 1, dt, dtpjcolumn, monthyear_now, mode, DisplayActivity_Type, ExpNodeID)
            '    'str.AppendLine("</tbody>")
            'End If
            'cDt.Dispose()
        Next


        Return str
    End Function

    Function GetColumnNameByMonth(ByVal monthyear As String) As String
        Dim _strfullname As String = ""
        Dim m As String = monthyear.Substring(4, monthyear.Length - 4)
        Dim y As String = monthyear.Substring(0, 4)
        Select Case m
            Case "01"
                '_strfullname = "ม.ค."
                _strfullname = "Jan"
            Case "02"
                '_strfullname = "ก.พ."
                _strfullname = "Feb"
            Case "03"
                '_strfullname = "มี.ค."
                _strfullname = "Mar"
            Case "04"
                '_strfullname = "เม.ย."
                _strfullname = "Apr"
            Case "05"
                '_strfullname = "พ.ค."
                _strfullname = "May"
            Case "06"
                '_strfullname = "มิ.ย."
                _strfullname = "Jun"
            Case "07"
                '_strfullname = "ก.ค."
                _strfullname = "Jul"
            Case "08"
                '_strfullname = "ส.ค."
                _strfullname = "Aug"
            Case "09"
                ' _strfullname = "ก.ย."
                _strfullname = "Sep"
            Case "10"
                '_strfullname = "ต.ค."
                _strfullname = "Oct"
            Case "11"
                '_strfullname = "พ.ย."
                _strfullname = "Nov"
            Case "12"
                '_strfullname = "ธ.ค."
                _strfullname = "Dec"
        End Select
        _strfullname &= " " & y
        Return _strfullname
    End Function

    Public Function CheckDupplicateProjectFile(filename As String) As DataTable
        Dim SQL As String = " select 'y' from tb_project_file where original_file_name='" & filename.Replace("'", "''") & "'"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

#End Region

#Region "_OU"

    Public Function GetOrganizeUnitByNodeID(node_id As String, node_type As Integer) As DataTable
        Dim SQL As String = ""
        Select Case (node_type)
            Case OU.Country
                SQL = " Select c.node_id,c.name_th,c.name_en,c.description,c.active_status ," & OU.Country & " node_type," & vbLf
                SQL &= " (Select  count(o.node_id)  from tb_ou_organize o where o.parent_id=c.node_id) " & vbLf
                SQL &= " + (Select  count(p.node_id)  from tb_ou_person p where p.parent_id=c.node_id) childs, '0' parent_id" & vbLf
                SQL &= " From tb_ou_country c" & vbLf
                SQL &= " where isnull(c.active_status,'Y') ='Y'" & vbLf
                SQL &= " order by c.name_th,c.name_en" & vbLf
            Case OU.Organize
                SQL = " select o.node_id,o.name_th,o.name_en ," & OU.Country & " node_type," & vbLf
                SQL &= " (select  count(p.node_id)  from tb_ou_person p where p.parent_id=o.node_id) + " & vbLf
                SQL &= " (select count(n.node_id) from tb_ou_organize n where n.parent_id = o.node_id) childs, o.parent_id" & vbLf
                SQL &= " From tb_ou_organize o" & vbLf
                SQL &= " where o.parent_id = '" & node_id.Replace("'", "''") & "'" & vbLf
                SQL &= " order by o.name_th,o.name_en" & vbLf
            Case OU.Person
                SQL = "select p.node_id,p.name_th,p.name_en," & OU.Country & " node_type,0 childs, p.parent_id from tb_ou_person p where parent_id='" & node_id.Replace("'", "''") & "'" & vbLf
                SQL &= " order by p.name_th,p.name_en" & vbLf
        End Select
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetCountryInfoByID(country_id As String) As DataTable
        Dim SQL As String = ""
        SQL = " select c.*,g.country_group_id,t.country_type_id,z.country_zone_id,r.country_region_id " & vbLf
        SQL &= " from  tb_ou_country c" & vbLf
        SQL &= " Left join tb_ou_countrygroup g on c.node_id = g.country_id" & vbLf
        SQL &= " left join tb_ou_countrytype t On c.node_id = t.country_id" & vbLf
        SQL &= " left join TB_OU_CountryZone z on c.node_id = z.country_id" & vbLf
        SQL &= " left join TB_OU_Country_Region r On c.node_id = r.country_id" & vbLf
        SQL &= " where c.node_id = '" & country_id.Replace("'", "''") & "'" & vbLf
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetOrganizeInfoByID(org_id As String) As DataTable
        Dim SQL As String = ""
        SQL = " select o.*,c.address,c.telephone,c.fax,c.website,c.description,c.email,c.id contact_id " & vbLf
        SQL &= " from tb_ou_organize o left join tb_ou_contact c on o.node_id = c.parent_id and parent_type = " & Contact_Parent_Type.Oraganize & " " & vbLf
        SQL &= " where o.node_id = '" & org_id & "'" & vbLf
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetPersonInfoByID(person_id As String) As DataTable
        Dim SQL As String = ""
        SQL = " select p.*,c.address,c.telephone,c.fax,c.website,c.description,c.email,c.id contact_id " & vbLf
        SQL &= " from tb_ou_person p left join tb_ou_contact c On p.node_id =c.parent_id And parent_type = " & Contact_Parent_Type.Person & "" & vbLf
        SQL &= " where p.node_id = '" & person_id.Replace("'", "''") & "'" & vbLf
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetPersonList(person_type As String) As DataTable
        Dim SQL As String = ""
        SQL = " select * from TB_OU_Person where isnull(active_status,'Y') ='Y'" & vbLf
        SQL &= " and person_type = '" & person_type.Replace("'", "''") & "'" & vbLf
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetUserList(TxtSearch As String, ActiveStatus As String) As DataTable
        Dim SQL As String = "" & vbLf
        SQL = " select p.*, ou.name_th org_name " & vbLf
        SQL &= " from TB_OU_Person p " & vbLf
        SQL &= " inner join  vw_ou ou on ou.node_id=p.parent_id" & vbLf
        Dim Filter As String = ""
        If TxtSearch.Trim <> "" Then
            Filter &= " and (p.name_th like '%" & TxtSearch.Replace("'", "''") & "%' or p.name_en like '%" & TxtSearch.Replace("'", "''") & "%' or ou.name_th like '%" & TxtSearch.Replace("'", "''") & "%')"
        End If
        If ActiveStatus <> "" Then
            Filter &= " and p.active_status='" & ActiveStatus.Replace("'", "''") & "'"
        End If
        If Filter <> "" Then
            SQL &= " WHERE " & Filter.Substring(5)
        End If
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function SaveOUCountry(lnqCountry As TbOuCountryLinqDB, CountryRegionDT As DataTable, CountryGroupDT As DataTable, CountryZoneDT As DataTable, Username As String) As ProcessReturnInfo

        Dim ret As New ProcessReturnInfo
        Dim trans As New TransactionDB

        Try

            'บันทึกข้อมูลตารางหลัก
            Dim exc As New ExecuteDataInfo
            Dim _lnqCountry As New TbOuCountryLinqDB
            With _lnqCountry
                .ChkDataByNODE_ID(lnqCountry.NODE_ID, trans.Trans)
                .NAME_TH = lnqCountry.NAME_TH
                .NAME_EN = lnqCountry.NAME_EN
                .URL = lnqCountry.URL
                '.COUNTRY_GROUP_ID = lnqCountry.COUNTRY_GROUP_ID
                '.COUNTRY_TYPE = lnqCountry.COUNTRY_TYPE
                '.COUNTRY_ZONE = lnqCountry.COUNTRY_ZONE
                .DESCRIPTION = lnqCountry.DESCRIPTION
                .ACTIVE_STATUS = lnqCountry.ACTIVE_STATUS
                If .ID > 0 Then
                    .NODE_ID = lnqCountry.NODE_ID
                    exc = .UpdateData(Username, trans.Trans)
                Else
                    .NODE_ID = GetOUNodeID()
                    exc = .InsertData(Username, trans.Trans)
                End If
            End With

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If


            Dim _country_id As String = _lnqCountry.NODE_ID

            'ลบข้อมูลก่อนบันทึกใหม่
            Dim p_r(1) As SqlParameter
            p_r(0) = SqlDB.SetText("@_NODE_ID", _country_id)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_OU_Country_Region WHERE COUNTRY_ID=@_NODE_ID", p_r)

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            'ถ้ามีข้อมูล Country Region ให้บันทึกเข้าไปใหม่
            If Not CountryRegionDT Is Nothing AndAlso CountryRegionDT.Rows.Count > 0 Then
                For i As Integer = 0 To CountryRegionDT.Rows.Count - 1
                    Dim _country_group_id As Long = Convert.ToInt32(CountryRegionDT.Rows(i)("COUNTRY_REGION_ID"))
                    Dim lnq As New TbOuCountryRegionLinqDB
                    With lnq
                        .COUNTRY_REGION_ID = _country_group_id
                        .COUNTRY_ID = _country_id
                        .InsertData(Username, trans.Trans)
                    End With
                    lnq = Nothing
                Next
            End If
            CountryRegionDT.Dispose()

            'ลบข้อมูลก่อนบันทึกใหม่
            Dim p_c(1) As SqlParameter
            p_c(0) = SqlDB.SetText("@_NODE_ID", _country_id)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_OU_CountryGroup WHERE COUNTRY_ID=@_NODE_ID", p_c)

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            'ถ้ามีข้อมูล Country Group ให้บันทึกเข้าไปใหม่
            If Not CountryGroupDT Is Nothing AndAlso CountryGroupDT.Rows.Count > 0 Then
                For i As Integer = 0 To CountryGroupDT.Rows.Count - 1
                    Dim _country_group_id As Long = Convert.ToInt32(CountryGroupDT.Rows(i)("COUNTRY_GROUP_ID"))
                    Dim lnq As New TbOuCountrygroupLinqDB
                    With lnq
                        .COUNTRY_GROUP_ID = _country_group_id
                        .COUNTRY_ID = _country_id
                        .InsertData(Username, trans.Trans)
                    End With
                    lnq = Nothing
                Next
            End If
            CountryGroupDT.Dispose()




            'ลบข้อมูลก่อนบันทึกใหม่
            Dim p_z(1) As SqlParameter
            p_z(0) = SqlDB.SetText("@_NODE_ID", _country_id)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_OU_CountryZone WHERE COUNTRY_ID=@_NODE_ID", p_z)
            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            'ถ้ามีข้อมูล Country Zone ให้บันทึกเข้าไปใหม่
            If Not CountryZoneDT Is Nothing AndAlso CountryZoneDT.Rows.Count > 0 Then
                For i As Integer = 0 To CountryZoneDT.Rows.Count - 1
                    Dim _country_zone_id As Long = Convert.ToInt32(CountryZoneDT.Rows(i)("COUNTRY_ZONE_ID"))
                    Dim lnq As New TbOuCountryzoneLinqDB
                    With lnq
                        .COUNTRY_ZONE_ID = _country_zone_id
                        .COUNTRY_ID = _country_id
                        exc = .InsertData(Username, trans.Trans)
                    End With
                    If exc.IsSuccess = False Then
                        trans.RollbackTransaction()
                        ret.IsSuccess = False
                        ret.ErrorMessage = exc.ErrorMessage()
                        Return ret
                    End If
                    lnq = Nothing
                Next
            End If
            CountryZoneDT.Dispose()


            'ถ้า Complete หมดให้ Commit Transaction
            trans.CommitTransaction()
            ret.IsSuccess = True
            ret.ErrorMessage = ""
        Catch ex As Exception
            trans.RollbackTransaction()
            ret.IsSuccess = False
            ret.ErrorMessage = ex.ToString()
        End Try

        Return ret
    End Function

    Public Function SaveOUOrganize(lnqOrganize As TbOuOrganizeLinqDB, ContactDT As DataTable, Username As String) As ProcessReturnInfo
        Dim ret As New ProcessReturnInfo
        Dim trans As New TransactionDB
        Try
            'บันทึกข้อมูลตารางหลัก
            Dim exc As New ExecuteDataInfo
            Dim _lnqOrganize As New TbOuOrganizeLinqDB
            With _lnqOrganize
                .ChkDataByNODE_ID(lnqOrganize.NODE_ID, trans.Trans)
                .NAME_TH = lnqOrganize.NAME_TH
                .NAME_EN = lnqOrganize.NAME_EN
                .PARENT_ID = lnqOrganize.PARENT_ID
                .URL = lnqOrganize.URL
                .ABBR_NAME_EN = lnqOrganize.ABBR_NAME_EN
                .ABBR_NAME_TH = lnqOrganize.NAME_TH
                .FUNDING_AGENCY_TYPE = lnqOrganize.FUNDING_AGENCY_TYPE
                .EXECUTING_AGENCY_TYPE = lnqOrganize.EXECUTING_AGENCY_TYPE
                .IMPLEMENTING_AGENCY_TYPE = lnqOrganize.IMPLEMENTING_AGENCY_TYPE
                .ACTIVE_STATUS = lnqOrganize.ACTIVE_STATUS
                If .ID > 0 Then
                    .NODE_ID = lnqOrganize.NODE_ID
                    exc = .UpdateData(Username, trans.Trans)
                Else
                    .NODE_ID = GetOUNodeID()
                    exc = .InsertData(Username, trans.Trans)

                    If exc.IsSuccess = True Then
                        'กรณีเป็นการเพิ่มข้อมูลบุคคลให้กำหนดสิทธิ์ให้อัตโนมัติ โดยไปดึงสิทธิ์จาก ประเทศ และหน่วยงานที่คนนี้อาศัยอยู่
                        exc = SaveNewOUAuthorize(Username, .NODE_ID, trans)
                    End If
                End If
            End With

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            'ลบข้อมูลก่อนบันทึกใหม่
            Dim p_c(1) As SqlParameter
            p_c(0) = SqlDB.SetText("@_NODE_ID", _lnqOrganize.NODE_ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_OU_Contact WHERE parent_id=@_NODE_ID", p_c)

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            If Not ContactDT Is Nothing AndAlso ContactDT.Rows.Count > 0 Then
                For i As Integer = 0 To ContactDT.Rows.Count - 1
                    Dim lnq As TbOuContactLinqDB = DirectCast(ContactDT.Rows(i)("DTOrganize"), TbOuContactLinqDB)
                    With lnq
                        .PARENT_ID = _lnqOrganize.NODE_ID
                        exc = .InsertData(Username, trans.Trans)
                    End With

                    If exc.IsSuccess = False Then
                        trans.RollbackTransaction()
                        ret.IsSuccess = False
                        ret.ErrorMessage = exc.ErrorMessage()
                        Return ret
                    End If

                    lnq = Nothing
                Next
            End If



            trans.CommitTransaction()
            ret.IsSuccess = True
            ret.ErrorMessage = ""
        Catch ex As Exception
            trans.RollbackTransaction()
            ret.IsSuccess = False
            ret.ErrorMessage = ex.ToString()
        End Try

        Return ret
    End Function

    Public Function SaveOUPerson(lnqPerson As TbOuPersonLinqDB, ContactDT As DataTable, PersonType As Integer, Username As String, IsSaveAuthorize As Boolean) As ProcessReturnInfo
        Dim ret As New ProcessReturnInfo
        Dim trans As New TransactionDB
        Try
            Dim exc As New ExecuteDataInfo
            Dim _lnqPerson As New TbOuPersonLinqDB
            With _lnqPerson
                .ChkDataByNODE_ID(lnqPerson.NODE_ID, trans.Trans)

                .NODE_ID = lnqPerson.NODE_ID
                .PREFIX_TH_ID = lnqPerson.PREFIX_TH_ID
                .NAME_TH = lnqPerson.NAME_TH
                .NAME_EN = lnqPerson.NAME_EN
                .PARENT_ID = lnqPerson.PARENT_ID
                .URL = lnqPerson.URL
                .PASSPORT_NO = lnqPerson.PASSPORT_NO
                .EXPIRED_DATE_PASSPORT = lnqPerson.EXPIRED_DATE_PASSPORT
                .ID_CARD = lnqPerson.ID_CARD
                ' .ADDRESS = lnqPerson.ADDRESS
                .BIRTHDATE = lnqPerson.BIRTHDATE
                ' .EMAIL = lnqPerson.EMAIL
                .USERNAME = lnqPerson.USERNAME

                If lnqPerson.PASSWORD <> "" Then
                    'ถ้ามีการเปลี่ยน Password
                    .PASSWORD = lnqPerson.PASSWORD
                End If

                .PERSON_TYPE = PersonType
                .PARENT_ID = lnqPerson.PARENT_ID
                .ACTIVE_STATUS = lnqPerson.ACTIVE_STATUS
                If .ID > 0 Then
                    exc = .UpdateData(Username, trans.Trans)
                Else
                    exc = .InsertData(Username, trans.Trans)

                    If exc.IsSuccess = True Then
                        'กรณีเป็นการเพิ่มข้อมูลบุคคลให้กำหนดสิทธิ์ให้อัตโนมัติ โดยไปดึงสิทธิ์จาก ประเทศ และหน่วยงานที่คนนี้อาศัยอยู่
                        If IsSaveAuthorize = True Then
                            exc = SaveNewOUAuthorize(Username, .NODE_ID, trans)
                        End If
                    End If
                End If
            End With

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If


            'ลบข้อมูลก่อนบันทึกใหม่
            Dim p_c(1) As SqlParameter
            p_c(0) = SqlDB.SetText("@_NODE_ID", _lnqPerson.NODE_ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_OU_Contact WHERE parent_id=@_NODE_ID", p_c)

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If


            If Not ContactDT Is Nothing AndAlso ContactDT.Rows.Count > 0 Then
                For i As Integer = 0 To ContactDT.Rows.Count - 1
                    Dim lnq As TbOuContactLinqDB = DirectCast(ContactDT.Rows(i)("DTPerson"), TbOuContactLinqDB)
                    With lnq
                        lnq.PARENT_ID = _lnqPerson.NODE_ID
                        exc = .InsertData(Username, trans.Trans)
                    End With

                    If exc.IsSuccess = False Then
                        trans.RollbackTransaction()
                        ret.IsSuccess = False
                        ret.ErrorMessage = exc.ErrorMessage()
                        Return ret
                    End If

                    lnq = Nothing
                Next
            End If

            trans.CommitTransaction()
            ret.IsSuccess = True
            ret.ErrorMessage = ""
        Catch ex As Exception
            trans.RollbackTransaction()
            ret.IsSuccess = False
            ret.ErrorMessage = ex.ToString()
        End Try

        Return ret
    End Function

    Private Function SaveNewOUAuthorize(Username As String, NodeID As String, trans As TransactionDB) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim p(1) As SqlParameter
        p(0) = SqlDB.SetText("@_NODE_ID", NodeID)
        Dim nDt As DataTable = SqlDB.ExecuteTable("exec sp_GetNodePath @_NODE_ID", trans.Trans, p)
        If nDt.Rows.Count > 0 Then
            Dim aDt As New DataTable
            'id,node_id,menu_id,is_save,is_view,is_na
            aDt.Columns.Add("id", GetType(Long))
            aDt.Columns.Add("menu_id", GetType(Long))
            aDt.Columns.Add("is_save")
            aDt.Columns.Add("is_view")
            aDt.Columns.Add("is_na")

            For Each nDr As DataRow In nDt.Rows
                If nDr("node_id") = NodeID Then Continue For
                Dim tmpDt As DataTable = GetList_AuthorizeByNodeID(nDr("node_id"))
                If tmpDt.Rows.Count > 0 Then
                    aDt.Merge(tmpDt)
                End If
            Next

            For Each aDr As DataRow In aDt.Rows
                ReDim p(2)
                p(0) = SqlDB.SetBigInt("@_NODE_ID", NodeID)
                p(1) = SqlDB.SetBigInt("@_MENU_ID", aDr("menu_id"))
                Dim lnq As New TbAuthorizeLinqDB
                lnq.ChkDataByWhere("node_id=@_NODE_ID And menu_id=@_MENU_ID", trans.Trans, p)

                'ถ้ามีข้อมูลอยู่แล้วก็ไม่ต้อง Update
                If lnq.ID = 0 Then
                    lnq.NODE_ID = NodeID
                    lnq.MENU_ID = aDr("menu_id")
                    lnq.IS_SAVE = aDr("is_save")
                    lnq.IS_VIEW = aDr("is_view")
                    lnq.IS_NA = aDr("is_na")

                    ret = lnq.InsertData(Username, trans.Trans)
                    If ret.IsSuccess = False Then
                        Exit For
                    End If
                End If
                lnq = Nothing
            Next
        Else
            ret.IsSuccess = True
        End If

        Return ret
    End Function

    Public Function GetOUNodeID() As String
        Return Convert.ToDateTime(DateTime.Now).ToString("yyyyMMddHHmmssff", New System.Globalization.CultureInfo("th-TH"))
    End Function

    Public Function DeleteOUCountry(ByVal node_id As String) As Boolean

        Dim SQL As String = "Select 'y' from tb_ou_person p  where parent_id =" & node_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            Return False
        End If

        SQL = "Select 'y' from tb_ou_organize o  where parent_id =" & node_id
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            Return False
        End If

        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM TB_OU_CountryGroup WHERE COUNTRY_ID=" & node_id
                .ExecuteNonQuery()
                .CommandText = "DELETE FROM TB_OU_Country_Region WHERE COUNTRY_ID=" & node_id
                .ExecuteNonQuery()
                .CommandText = "DELETE FROM TB_OU_CountryZone WHERE COUNTRY_ID=" & node_id
                .ExecuteNonQuery()
                .CommandText = "DELETE FROM TB_OU_COUNTRY WHERE COUNTRY_ID=" & node_id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True

    End Function

    Public Function DeleteOUOrganize(ByVal node_id As String) As Boolean

        Dim SQL As String = "Select 'y' from tb_ou_person p  where parent_id =" & node_id
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            Return False
        End If

        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM TB_OU_Contact WHERE parent_id=" & node_id
                .ExecuteNonQuery()
                .CommandText = "DELETE FROM TB_OU_ORGANIZE WHERE NODE_ID=" & node_id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Public Function DeleteOUPerson(ByVal node_id As String) As Boolean
        Try
            Dim Conn As New SqlConnection(ConnectionString)
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = "DELETE FROM TB_OU_Contact WHERE parent_id=" & node_id
                .ExecuteNonQuery()
                .CommandText = "DELETE FROM TB_OU_PERSON WHERE NODE_ID=" & node_id
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True

    End Function

    Public Function CheckDupplicateIDCardPassport(ByVal id_card As String, ByVal passport_no As String, ByVal node_id As String) As Boolean

        Dim SQL As String = " select 'y' from tb_ou_person where  node_id <>'" & node_id.Replace("'", "''") & "'" & vbLf
        If id_card <> "" And passport_no = "" Then
            SQL &= " and id_card ='" & id_card.Replace("'", "''") & "'" & vbLf
        End If
        If passport_no <> "" And id_card = "" Then
            SQL &= " and passport_no='" & passport_no.Replace("'", "''") & "'" & vbLf
        End If
        If id_card <> "" And passport_no <> "" Then
            SQL &= " and (id_card ='" & id_card.Replace("'", "''") & "' or passport_no='" & passport_no.Replace("'", "''") & "')"
        End If
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT.Rows.Count > 0
    End Function

    Public Function GetParentOUByNodeID(node_id As String, Optional parent_node_type As Integer = -1) As DataTable
        Dim SQL As String = " with OU as " & vbLf
        SQL &= " ( " & vbLf
        SQL &= " Select node_id,name_th,name_en,tb_name,parent_id" & vbLf
        SQL &= " From vw_ou " & vbLf
        SQL &= " where node_Id ='" & node_id.Replace("'", "''") & "' " & vbLf
        SQL &= " union all " & vbLf
        SQL &= " select c.node_id,c.name_th,c.name_en,c.tb_name,c.parent_id" & vbLf
        SQL &= " from vw_ou c " & vbLf
        SQL &= " inner join OU on c.node_id = OU.parent_Id" & vbLf
        SQL &= " )" & vbLf
        SQL &= " Select  top 1 * from(" & vbLf
        SQL &= " Select  ROW_NUMBER() OVER(ORDER BY id ASC) As seq,* From OU " & vbLf
        SQL &= " Where 1=1 "

        If parent_node_type = OU.Country Then
            SQL &= " and  tb_name ='TB_OU_Country'" & vbLf
        End If

        If parent_node_type = OU.Organize Then
            SQL &= " and tb_name ='TB_OU_Organize'" & vbLf
        End If

        If parent_node_type = OU.Person Then
            SQL &= " and tb_name ='TB_OU_Person'" & vbLf
        End If

        SQL &= " )T Order By seq desc" & vbLf

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetNodePathByNodeID(ByVal node_id As String) As DataTable
        Dim SQL As String = " with OU as " & vbLf
        SQL &= " ( " & vbLf
        SQL &= " Select node_id,name_th,name_en,tb_name,parent_id" & vbLf
        SQL &= " From vw_ou " & vbLf
        SQL &= " where node_Id ='" & node_id.Replace("'", "''") & "' " & vbLf
        SQL &= " union all " & vbLf
        SQL &= " select c.node_id,c.name_th,c.name_en,c.tb_name,c.parent_id" & vbLf
        SQL &= " from vw_ou c " & vbLf
        SQL &= " inner join OU on c.node_id = OU.parent_Id" & vbLf
        SQL &= " )" & vbLf
        SQL &= " select * from ou" & vbLf

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetNodePathTextByNodeID(ByVal node_id As String, ByVal Lang As String) As String
        Dim ret As String = ""

        Dim fldName As String = ""
        If Lang = "TH" Then
            fldName = "name_th"
        Else
            fldName = "name_en"
        End If
        Dim dt As DataTable = GetNodePathByNodeID(node_id)
        If dt.Rows.Count > 0 Then
            For i As Integer = dt.Rows.Count - 1 To 0 Step -1
                If ret = "" Then
                    ret = dt.Rows(i)(fldName)
                Else
                    ret += " >> " & dt.Rows(i)(fldName)
                End If
            Next
        End If
        dt.Dispose()
        Return ret
    End Function

    Public Function GetList_OU() As DataTable
        Dim SQL As String = "select node_id,isnull(name_th, name_en) node_name,parent_id from vw_ou"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetList_ChildNode(ByVal ParentNodeID As String) As DataTable
        Dim SQL As String = "select node_id,isnull(name_th, name_en) node_name,parent_id from vw_ou where parent_id='" & ParentNodeID.Replace("'", "''") & "'"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetList_ChildCountryNode(ByVal CountryNodeID As String) As DataTable
        Dim SQL As String = ""
        SQL &= " With OU As " & vbLf
        SQL &= " (  " & vbLf
        SQL &= " Select node_id,isnull(name_th,name_en) node_name,parent_id " & vbLf
        SQL &= " From vw_ou  " & vbLf
        SQL &= " where node_Id ='" & CountryNodeID.Replace("'", "''") & "'  " & vbLf
        SQL &= " union all  " & vbLf
        SQL &= " select c.node_id,isnull(c.name_th,c.name_en) node_name,c.parent_id " & vbLf
        SQL &= " from vw_ou c  " & vbLf
        SQL &= " inner join OU on c.parent_Id = OU.node_id " & vbLf
        SQL &= " ) " & vbLf
        SQL &= " select * from ou " & vbLf
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

#Region "__แบบรวม Table_ไม่ได้ใช้"
    Public Function GetOrganizeUnitInfoBy(ByVal id As Long, ByVal wh As String) As DataTable
        Dim SQL As String = " Select o.*,u.[user_id],u.username,u.[password] " & vbLf
        SQL &= " From web_organize_unit o left Join web_userlogin u On o.id = u.[user_id] Where 1 = 1 " & vbLf
        If id <> 0 Then
            SQL &= " And id = " & id
        End If
        If wh <> "" Then
            SQL &= wh
        End If
        SQL &= " order by code,name_th,name_en "

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetAllChildOrganizeUnitByID(ByVal id As Long) As DataTable
        Dim SQL As String = " with OU as " & vbLf
        SQL &= " ( " & vbLf
        SQL &= " Select  Id,name_th,name_en,parent_id,active_status " & vbLf
        SQL &= " from web_organize_unit " & vbLf
        SQL &= " where Id =" & id & " and isnull(active_status,'Y') = 'Y' " & vbLf
        SQL &= "  union all " & vbLf
        SQL &= " select c.Id,c.name_th,c.name_en,c.parent_id,c.active_status " & vbLf
        SQL &= " from web_organize_unit c " & vbLf
        SQL &= " inner join OU on c.parent_id = OU.Id  " & vbLf
        SQL &= " where  isnull(c.active_status,'Y') ='Y' " & vbLf
        SQL &= " ) " & vbLf
        SQL &= " Select * " & vbLf
        SQL &= " from OU  " & vbLf
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

#End Region


#End Region

#Region "_OtherFunction"
    Public Function GetTHDateAbbr(pDate As String) As String
        Dim strDate As String ' yyyyMMdd
        Dim strMonth As String = ""
        Dim NumMonth As String = pDate.Substring(4, 2)
        Dim intDay As Integer = 1

        Try
            intDay = Convert.ToInt16(pDate.Substring(6, 2))
        Catch ex As Exception
        End Try

        If NumMonth = "01" Then
            strMonth = "ม.ค."
        ElseIf NumMonth = "02" Then
            strMonth = "ก.พ."
        ElseIf NumMonth = "03" Then
            strMonth = "มี.ค."
        ElseIf NumMonth = "04" Then
            strMonth = "เม.ย."
        ElseIf NumMonth = "05" Then
            strMonth = "พ.ค."
        ElseIf NumMonth = "06" Then
            strMonth = "มิ.ย."
        ElseIf NumMonth = "07" Then
            strMonth = "ก.ค."
        ElseIf NumMonth = "08" Then
            strMonth = "ส.ค."
        ElseIf NumMonth = "09" Then
            strMonth = "ก.ย."
        ElseIf NumMonth = "10" Then
            strMonth = "ต.ค."
        ElseIf NumMonth = "11" Then
            strMonth = "พ.ย."
        ElseIf NumMonth = "12" Then
            strMonth = "ธ.ค."
        End If

        Dim Year As Long = 0
        Try
            Year = Convert.ToInt64(pDate.Substring(0, 4)) + 543
        Catch ex As Exception
        End Try

        strDate = intDay.ToString() + " " + strMonth + " " + Year.ToString()

        Return strDate
    End Function

    Public Function GetAssistantByID(ByVal ass_id As String) As DataTable
        Dim SQL As String = "select P.id,node_id,name_th from TB_OU_Person P"
        SQL &= " inner join (select * from dbo.Split('" & ass_id.Replace("'", "''") & "',',')) T on P.ID = T.Data"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function CalPlanDuration(startdate As String, enddate As String) As String
        If startdate = "" Or enddate = "" Then
            Return ""
        End If

        Dim d As Date = Converter.StringToDate(startdate, "dd/MM/yyyy")
        Dim ts As TimeSpan = Converter.StringToDate(enddate, "dd/MM/yyyy") - d
        Dim y As Double
        Dim m As Double
        Dim ds As Double = ts.TotalDays

        y = Math.Floor(ds / 365)
        ds -= y * 365
        m = Math.Floor(ds / 31)
        ds -= m * 31

        Dim yy As String = CStr(y)
        Dim mm As String = CStr(m)
        Dim dd As String = CStr(ds)

        Dim ret As String = ""
        If yy <> "0" Then ret &= yy & " ปี"
        If mm <> "0" Then ret &= mm & " เดือน"
        If dd <> "0" Then ret &= dd & " วัน"

        Return ret

        'Example: Dim strduration As String = BL.CalPlanDuration("06/01/2017", "08/02/2019")
    End Function

    Public Function GetPeriodMonth(y As String, m As String) As Integer

        Dim ym As Integer = 0
        If y <> "" Then
            ym = CInt(y) * 12
        End If

        Dim allm As Integer = ym
        If m <> "" Then
            allm += CInt(m)
        End If

        Return allm
    End Function

    Public Function GetConvertPeriodMonth(m As Integer) As ArrayList
        Dim result As String() = (m / 12).ToString.Split(".")
        Dim y As Integer = result(0)
        Dim totalm As Integer = m Mod 12

        Dim arr As New ArrayList
        arr.Add(y.ToString)
        arr.Add(totalm.ToString)

        Return arr
    End Function

#End Region

#Region "SYS CONFIG"
    Public Shared Function GetSysconfig(trans As TransactionDB) As MsSysconfigLinqDB
        Dim ret As New MsSysconfigLinqDB
        Try
            If trans IsNot Nothing Then
                ret.GetDataByPK(1, trans.Trans)
            Else
                ret.GetDataByPK(1, Nothing)
            End If
        Catch ex As Exception
            ret = New MsSysconfigLinqDB
        End Try
        Return ret
    End Function
#End Region


#Region "_Report"


#End Region

#Region "Cryptography"
    Private EncryptionKey As String = "TIT_Encrypt2017"
    Public Function EncryptString(ByVal Str As String) As String
        Dim AES As New System.Security.Cryptography.RijndaelManaged
        Dim Hash_AES As New System.Security.Cryptography.MD5CryptoServiceProvider
        Dim encrypted As String = ""
        Try
            Dim hash(31) As Byte
            Dim temp As Byte() = Hash_AES.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(EncryptionKey))
            Array.Copy(temp, 0, hash, 0, 16)
            Array.Copy(temp, 0, hash, 15, 16)
            AES.Key = hash
            AES.Mode = System.Security.Cryptography.CipherMode.ECB
            Dim DESEncrypter As System.Security.Cryptography.ICryptoTransform = AES.CreateEncryptor
            Dim Buffer As Byte() = System.Text.ASCIIEncoding.ASCII.GetBytes(Str)
            encrypted = Convert.ToBase64String(DESEncrypter.TransformFinalBlock(Buffer, 0, Buffer.Length))
        Catch ex As Exception
        End Try
        Return encrypted
    End Function

    Public Function DecryptString(ByVal Str As String) As String
        Dim AES As New System.Security.Cryptography.RijndaelManaged
        Dim Hash_AES As New System.Security.Cryptography.MD5CryptoServiceProvider
        Dim decrypted As String = ""
        Try
            Dim hash(31) As Byte
            Dim temp As Byte() = Hash_AES.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(EncryptionKey))
            Array.Copy(temp, 0, hash, 0, 16)
            Array.Copy(temp, 0, hash, 15, 16)
            AES.Key = hash
            AES.Mode = System.Security.Cryptography.CipherMode.ECB
            Dim DESDecrypter As System.Security.Cryptography.ICryptoTransform = AES.CreateDecryptor
            Dim Buffer As Byte() = Convert.FromBase64String(Str)
            decrypted = System.Text.ASCIIEncoding.ASCII.GetString(DESDecrypter.TransformFinalBlock(Buffer, 0, Buffer.Length))
        Catch ex As Exception
        End Try

        Return decrypted
    End Function
#End Region

End Class