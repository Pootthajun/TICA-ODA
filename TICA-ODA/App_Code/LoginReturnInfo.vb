﻿Imports Microsoft.VisualBasic

Public Class LoginReturnInfo
    Dim _IsSuccess As Boolean = False
    Dim _ErrorMessage As String = ""
    Dim _username As String = ""
    Dim _node_id As String = ""
    Dim _name_th As String = ""

    Public Property IsSuccess As Boolean
        Get
            Return _IsSuccess
        End Get
        Set(value As Boolean)
            _IsSuccess = value
        End Set
    End Property

    Public Property ErrorMessage As String
        Get
            Return _ErrorMessage.Trim
        End Get
        Set(value As String)
            _ErrorMessage = value
        End Set
    End Property

    Public Property Node_ID As String
        Get
            Return _node_id
        End Get
        Set(value As String)
            _node_id = value
        End Set
    End Property

    Public Property UserName As String
        Get
            Return _username
        End Get
        Set(value As String)
            _username = value
        End Set
    End Property

    Public Property Name_TH As String
        Get
            Return _name_th
        End Get
        Set(value As String)
            _name_th = value
        End Set
    End Property
End Class
