﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCDisbursementContribution.ascx.vb" Inherits="usercontrol_UCDisbursementContribution" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<div class="box-body no-padding">
    <table id="example2" class="table table-bordered table-hover">
        <thead>
            <tr class="bg-gray">
                <th class="data-table-ddl" style="vertical-align:middle;">Title (รายการ)</th>
                <th class="data-table-ddl" style="vertical-align:middle;">Multilateral (พหุภาคี)</th>
                <th class="date-table-input" style="vertical-align:middle;">Payment Date <br />(วันที่เบิกจ่าย)</th>
                <th style="vertical-align:middle;">Value <br />(มูลค่าความช่วยเหลือ)</th>
                <th style="width: 30px;vertical-align:middle;">Delete(ลบ)</th>
            </tr>
        </thead>
       <tbody>
             <asp:Repeater ID="rptList" runat="server">
                <ItemTemplate>
                    <tr>
                        <td class="data-table-ddl"   style ="padding:0px;">
                            <asp:Label ID="lblid" runat="server" Visible="false"></asp:Label>
                            <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control" placeholder=""></asp:TextBox>                            
                        </td>
                        <td class="data-table-ddl"   style ="padding:0px;">
                            <asp:DropDownList ID="ddlMultilateral" runat="server" CssClass="form-control select2" Style="width: 100%">
                            </asp:DropDownList>
                        </td>
                        <td   style ="padding:0px;">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox CssClass="form-control m-b" ID="txtPaymentDate" runat="server" placeholder=""></asp:TextBox>
                                <ajaxtoolkit:calendarextender id="CalendarExtender1" runat="server"
                                    format="dd/MM/yyyy" targetcontrolid="txtPaymentDate" popupposition="BottomLeft"></ajaxtoolkit:calendarextender>
                            </div>
                        </td>
                        <td   style ="padding:0px;">
                            <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control" Style="text-align: right" placeholder="" AutoPostBack="true" OnTextChanged="txtAmount_TextChanged" MaxLength="12"></asp:TextBox>
                        </td>
                        <td class="no-padding"   style ="padding:0px;">
                            <%--<center>
                                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-bricky"  CommandArgument='<%# Eval("id") %>' CommandName="Delete" OnClientClick="return confirm('ท่านต้องการลบรายการนี้หรือไม่?');">
                                    <i Class='fa fa-trash text-danger bin'></i>
                                </asp:LinkButton>
                            </center>--%>
                             <center>
                                <asp:Button ID="btnDelete" runat="server" Text="ลบ" CssClass="btn btn-bricky"  width="100%"  CommandArgument='<%# Eval("id") %>' CommandName="Delete" OnClientClick="return confirm('ท่านต้องการลบรายการนี้หรือไม่?');"/>
                            </center>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </tbody>
        <asp:Panel ID="pnlFooter" runat="server">
        <tfoot>
            <tr>
                <td style="background-color:darkkhaki;text-align: center ;" colspan="3">
                    <b>รวม</b>
                </td>
                <td style="background-color:darkkhaki;text-align: right;">
                     <b><asp:Label ID="lblTotal" runat="server" Text="0"></asp:Label></b>
                </td>
            </tr>

        </tfoot>
        </asp:Panel>
    </table>

<%--    <div class="row">
        <div class="col-sm-12 cost cost-uc">
            <b>รวม<asp:Label ID="lblTotal" runat="server" Text="0"></asp:Label></b>
        </div>
    </div>--%>

    <div class="row"  style="margin-top:5px;">
        <center>
            <div class="col-sm-12">
                <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" Text="เพิ่ม" CssClass="btn btn-primary" />
            </div>
        </center>
    </div>
</div>
