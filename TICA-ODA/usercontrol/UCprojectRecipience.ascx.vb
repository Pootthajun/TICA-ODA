﻿Imports System.Data
Imports LinqDB.ConnectDB

Partial Class usercontrol_UCprojectRecipience
    Inherits System.Web.UI.UserControl
    Dim BL As New ODAENG

    Dim _RecipienceDT As DataTable

    Public Property RecipienceDT As DataTable
        Get
            Return GetDataFromRpt(rptList)
        End Get
        Set(value As DataTable)
            _RecipienceDT = value
            rptList.DataSource = _RecipienceDT
            rptList.DataBind()

        End Set
    End Property

    Public Sub SetToViewMode(IsEnable As Boolean)
        btnAdd.Enabled = Not IsEnable
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlRecipient As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlRecipient"), DropDownList)
            Dim ddlCountry As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlCountry"), DropDownList)
            Dim btnDelete As Button = DirectCast(rptList.Items(i).FindControl("btnDelete"), Button)

            ddlRecipient.Enabled = Not IsEnable
            ddlCountry.Enabled = Not IsEnable
            btnDelete.Enabled = Not IsEnable
        Next
    End Sub

    Function SetDataRpt(Activity_id As String) As DataTable
        Dim dt As New DataTable
        dt = BL.GetDataRecipent_FromQurey(Activity_id)
        If dt.Rows.Count > 0 Then

            dt.Columns.Add("no", GetType(Long))

            For i As Integer = 0 To dt.Rows.Count - 1
                dt.Rows(i)("no") = i + 1
            Next

            rptList.DataSource = dt
            rptList.DataBind()
            Bindata_toRptCountRe()
        End If
        lblActivityID.Text = Activity_id

        Return dt
    End Function

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        Dim dt As DataTable = GetDataFromRpt(rptList)
        Dim dr As DataRow
        dr = dt.NewRow
        dr("id") = dt.Rows.Count + 1
        dr("recipient_id") = ""
        dr("country_id") = ""
        dt.Rows.Add(dr)

        dt.Columns.Add("no", GetType(Long))
        For i As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(i)("no") = i + 1
        Next

        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Function GetDataFromRpt(rpt As Repeater) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("recipient_id")
        dt.Columns.Add("country_id")

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlRecipient As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlRecipient"), DropDownList)
            Dim ddlCountry As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlCountry"), DropDownList)

            dr = dt.NewRow
            dr("id") = dt.Rows.Count + 1
            dr("recipient_id") = ddlRecipient.SelectedValue
            dr("country_id") = ddlCountry.SelectedValue
            dt.Rows.Add(dr)

        Next

        Return dt
    End Function

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim ddlRecipient As DropDownList = DirectCast(e.Item.FindControl("ddlRecipient"), DropDownList)
        Dim ddlCountry As DropDownList = DirectCast(e.Item.FindControl("ddlCountry"), DropDownList)
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)
        Dim lblNo As Label = DirectCast(e.Item.FindControl("lblNo"), Label)

        BL.Bind_DDL_Recipience(ddlRecipient)

        lblID.Text = e.Item.DataItem("id").ToString
        lblNo.Text = e.Item.DataItem("no").ToString
        ddlRecipient.SelectedValue = e.Item.DataItem("recipient_id").ToString
        ddlRecipient_SelectedIndexChanged(ddlCountry, Nothing)

        ddlCountry.SelectedValue = e.Item.DataItem("country_id").ToString
    End Sub

    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand

        If e.CommandName = "Delete" Then

            Dim _id As String = e.CommandArgument
            Dim dt As DataTable = DeleteDataInRpt(rptList, _id)
            rptList.DataSource = dt
            rptList.DataBind()
            Bindata_toRptCountRe()
        End If
    End Sub

    Public Sub ddlRecipient_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim ddlRecipient1 As DropDownList = DirectCast(sender, DropDownList)
        Dim ddlRecipient As DropDownList = DirectCast(ddlRecipient1.FindControl("ddlRecipient"), DropDownList)
        Dim ddlCountry As DropDownList = DirectCast(ddlRecipient1.FindControl("ddlCountry"), DropDownList)

        If ddlRecipient.SelectedValue <> "" Then
            BL.Bind_DDL_CountryByRecipience(ddlCountry, ddlRecipient.SelectedValue)
        End If
        If ddlRecipient.SelectedValue = "" Then
            BL.Bind_DDL_CountryByRecipience(ddlCountry, "")
        End If
        Bindata_toRptCountRe()
    End Sub

    Function DeleteDataInRpt(rpt As Repeater, id As String) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("recipient_id")
        dt.Columns.Add("country_id")


        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlRecipient As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlRecipient"), DropDownList)
            Dim ddlCountry As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlCountry"), DropDownList)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)

            If lblID.Text <> id Then
                dr = dt.NewRow
                dr("id") = dt.Rows.Count + 1
                dr("recipient_id") = ddlRecipient.SelectedValue
                dr("country_id") = ddlCountry.SelectedValue
                dt.Rows.Add(dr)
            End If
        Next

        dt.Columns.Add("no", GetType(Long))
        For i As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(i)("no") = i + 1
        Next

        rptList.DataSource = dt
        rptList.DataBind()

        Return dt
    End Function

    Function Bindata_toRptCountRe() As DataTable
        Dim otherDT As New DataTable
        otherDT.Columns.Add("Country")

        'เรียกข้อมูลจาก repeater ตัวเพิ่มผู้รับทุน
        Dim drow As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlRecipient As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlRecipient"), DropDownList)
            Dim ddlCountry As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlCountry"), DropDownList)

            drow = otherDT.NewRow
            drow("Country") = ddlCountry.SelectedValue
            otherDT.Rows.Add(drow)

        Next

        Dim dt As New DataTable
        dt.Columns.Add("Country")
        dt.Columns.Add("Count")
        Dim query = (From dr In (From d In otherDT.AsEnumerable Select New With {.date = d("Country")}) Select dr.date Distinct)
        For Each colName As String In query
            Dim cName = colName
            Dim cCount = (From row In otherDT.Rows Select row Where row("Country").ToString = cName).Count
            dt.Rows.Add(colName, cCount)
        Next

        rptCountRe.DataSource = dt
        rptCountRe.DataBind()
    End Function

    Protected Sub rptCountRe_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptCountRe.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblCountryPic As Label = DirectCast(e.Item.FindControl("lblCountryPic"), Label)
        Dim lblCountRe As Label = DirectCast(e.Item.FindControl("lblCountRe"), Label)

        Dim Country_id As String
        Country_id = e.Item.DataItem("Country").ToString

        lblCountryPic.Text = "<img src='Flag/" & Country_id & ".png' width='23' height='15' onError=""this.src='Flag/Default.png'"" /> "
        lblCountRe.Text = e.Item.DataItem("Count").ToString
    End Sub

    Protected Sub rptCountRe_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptCountRe.ItemCommand

    End Sub
End Class
