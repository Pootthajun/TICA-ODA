﻿Imports System.Data
Partial Class usercontrol_UCOrganize
    Inherits System.Web.UI.UserControl
    Dim BL As New ODAENG

    Dim _AgencyDT As DataTable
    Public Property AgencyDT As DataTable
        Get
            Return GetDataFromRpt(rptList)
        End Get
        Set(value As DataTable)
            _AgencyDT = value
            rptList.DataSource = _AgencyDT
            rptList.DataBind()

        End Set
    End Property

    Public Sub SetToViewMode(IsEnable As Boolean)
        btnAdd.Enabled = Not IsEnable
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlCountry As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlCountry"), DropDownList)
            Dim ddlAgency As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlAgency"), DropDownList)
            Dim btnDelete As Button = DirectCast(rptList.Items(i).FindControl("btnDelete"), Button)

            ddlCountry.Enabled = Not IsEnable
            ddlAgency.Enabled = Not IsEnable
            btnDelete.Enabled = Not IsEnable
        Next
    End Sub


    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        Dim dt As DataTable = GetDataFromRpt(rptList)
        Dim dr As DataRow
        dr = dt.NewRow
        dr("id") = dt.Rows.Count + 1
        dr("country_id") = ""
        dr("organize_id") = ""
        dt.Rows.Add(dr)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Function GetDataFromRpt(rpt As Repeater) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("country_id")
        dt.Columns.Add("organize_id")

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlCountry As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlCountry"), DropDownList)
            Dim ddlAgency As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlAgency"), DropDownList)

            dr = dt.NewRow
            dr("id") = dt.Rows.Count + 1
            dr("country_id") = ddlCountry.SelectedValue
            dr("organize_id") = ddlAgency.SelectedValue
            dt.Rows.Add(dr)

        Next

        Return dt
    End Function

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim ddlCountry As DropDownList = DirectCast(e.Item.FindControl("ddlCountry"), DropDownList)
        Dim ddlAgency As DropDownList = DirectCast(e.Item.FindControl("ddlAgency"), DropDownList)
        'Dim btnCountry As Button = DirectCast(e.Item.FindControl("btnCountry"), Button)
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)

        ' ddlCountry.Attributes.Add("onchange", "return bdCountryChange('" & btnCountry.ClientID & "');")
        BL.Bind_DDL_Country(ddlCountry)

        lblID.Text = e.Item.DataItem("id").ToString
        ddlCountry.SelectedValue = e.Item.DataItem("country_id").ToString
        ddlCountry_SelectedIndexChanged(ddlCountry, Nothing)

        ddlAgency.SelectedValue = e.Item.DataItem("organize_id").ToString
    End Sub

    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand

        If e.CommandName = "Delete" Then

            Dim _id As String = e.CommandArgument
            Dim dt As DataTable = DeleteDataInRpt(rptList, _id)
            rptList.DataSource = dt
            rptList.DataBind()

        End If
        If e.CommandName = "CountryChange" Then
            'Dim ddlCountry As DropDownList = DirectCast(e.Item.FindControl("ddlCountry"), DropDownList)
            'Dim ddlAgency As DropDownList = DirectCast(e.Item.FindControl("ddlAgency"), DropDownList)
            Dim btnCountry As Button = DirectCast(e.Item.FindControl("btnCountry"), Button)
            ' btnCountry_Click(btnCountry, Nothing)
        End If

    End Sub

    Public Sub ddlCountry_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim ddlCountry1 As DropDownList = DirectCast(sender, DropDownList)
        Dim ddlCountry As DropDownList = DirectCast(ddlCountry1.FindControl("ddlCountry"), DropDownList)
        Dim ddlAgency As DropDownList = DirectCast(ddlCountry1.FindControl("ddlAgency"), DropDownList)

        If ddlCountry.SelectedValue <> "" Then
            BL.Bind_DDL_OrganizeFromCountry(ddlAgency, ddlCountry.SelectedValue)
        End If
    End Sub

    'Protected Sub btnCountry_Click(sender As Object, e As EventArgs)
    '    Dim btnCountry As Button = DirectCast(sender, Button)
    '    Dim rowItem As RepeaterItem = btnCountry.Parent
    '    Dim ddlCountry As DropDownList = DirectCast(rowItem.FindControl("ddlCountry"), DropDownList)
    '    Dim ddlAgency As DropDownList = DirectCast(rowItem.FindControl("ddlAgency"), DropDownList)

    '    If ddlCountry.SelectedValue <> "" Then
    '        BL.Bind_DDL_OrganizeFromCountry(ddlAgency, ddlCountry.SelectedValue)
    '    End If
    'End Sub
    Function DeleteDataInRpt(rpt As Repeater, id As String) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("country_id")
        dt.Columns.Add("organize_id")

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlCountry As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlCountry"), DropDownList)
            Dim ddlAgency As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlAgency"), DropDownList)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)

            If lblID.Text <> id Then
                dr = dt.NewRow
                dr("id") = dt.Rows.Count + 1
                dr("country_id") = ddlCountry.SelectedValue
                dr("organize_id") = ddlAgency.SelectedValue
                dt.Rows.Add(dr)
            End If
        Next

        Return dt
    End Function
End Class
