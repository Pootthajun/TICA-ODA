﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCActivityExpenseActual.ascx.vb" Inherits="usercontrol_UCActivityExpenseActual" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<div class="tb-fix" runat="server">

    <table class="table table-bordered " style="border-collapse: initial; table-layout: fixed;">

        <thead>
            <tr class="bg-gray">
                <th style="width: 160px;">วันที่</th>
                <th style="width: 250px;">รายการ</th>
                <th style="width: 90px;">เลขเอกสาร</th>
                <th style="width: 170px;">รวมค่าใช้จ่าย</th>
                <asp:Label ID="lblActivityID1" runat="server" Text="Label" Visible="false"></asp:Label>
                <asp:Label ID="lblHeaderID" runat="server" Text="Label" Visible="false"></asp:Label>
                <asp:Label ID="lblTemplate" runat="server" Text="Label" Visible="false"></asp:Label>
                <asp:Label ID="lblRecipience" runat="server" Text="Label" Visible="false"></asp:Label>
                <asp:Label ID="lblTemplateId" runat="server" Text="Label" Visible="false"></asp:Label>
                <asp:Label ID="lblUsername" runat="server" Text="Label" Visible="false"></asp:Label>
                <asp:Label ID="lblType" runat="server" Text="Label" Visible="false"></asp:Label>
                <asp:Label ID="lblPID" runat="server" Text="Label" Visible="false"></asp:Label>
                <asp:Repeater ID="RptHead" runat="server">
                    <ItemTemplate>
                        <th style="min-width: 170px; width: 170px;">
                            <asp:Label ID="lbHeader" runat="server"></asp:Label></th>
                    </ItemTemplate>
                </asp:Repeater>
                <th style="width: 50px">ลบ</th>
            </tr>
        </thead>
        <asp:Label ID="lblActivityID" runat="server" Text="0" Visible="false"></asp:Label>
        <asp:Repeater ID="rptList" runat="server">
            <ItemTemplate>
                <tr>
                    <td data-title="วันที่" style="width: 150px; padding: 0px; vertical-align: middle; text-align: center;">
                        <asp:Label ID="lblNo" runat="server">

                        </asp:Label><div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox CssClass="form-control m-b" ID="txtPaymentDate" runat="server" placeholder=""></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                Format="dd/MM/yyyy" TargetControlID="txtPaymentDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                        </div>
                        <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                    </td>
                    <td style="padding: 0px; width: 200px;">
                        <asp:TextBox ID="txtDetail" runat="server" CssClass="form-control"></asp:TextBox>
                    </td>
                    <td id="td" runat="server" style="padding: 0px; width: 80px">
                        <asp:TextBox ID="txtDetailNo" runat="server" CssClass="form-control" placeholder="" Style="text-align: center" MaxLength="12"></asp:TextBox>
                    </td>
                    <td style="padding: 0px">
                        <asp:TextBox ID="txtSumCol" MaxLength="10" runat="server" Text="" Style="text-align: right; width: 100%; height: 35px" class="cost accounting" Enabled="false"></asp:TextBox>
                    </td>
                    <asp:Repeater ID="rptColItem" runat="server" OnItemDataBound="rptColItem_ItemDataBound">
                        <ItemTemplate>
                            <asp:UpdatePanel ID="udpList" runat="server">
                                <ContentTemplate>
                                    <td style="padding: 0px">
                                        <asp:Label ID="lblitem_subexpenseid" runat="server" Visible="false"></asp:Label>
                                        <asp:Label ID="Pay_Amount_Plan" runat="server" Visible="false"></asp:Label>
                                        <asp:TextBox ID="txtCol2" MaxLength="10" runat="server" Text="" Style="text-align: right; border: none; width: 100%; height: 35px"></asp:TextBox>
                                        <asp:TextBox ID="txtTemp" MaxLength="10" runat="server" Text="" Style="text-align: right; border: none; width: 100%; height: 35px; display: none;"></asp:TextBox>
                                    </td>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ItemTemplate>
                    </asp:Repeater>
                    <td data-title="Delete" id="ColDelete" runat="server" style="padding: 0px;">
                        <center>
                              <asp:Button ID="btnDelete" runat="server" Text="ลบ" CssClass="btn btn-bricky"  width="100%"  CommandArgument='<%# Eval("id") %>' CommandName="Delete" OnClientClick="return confirm('ท่านต้องการลบรายการนี้หรือไม่?');"/>
                            </center>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
        <tr>
            <td style="padding: 0px;" colspan="3" >
                <asp:Button ID="btnAdd" runat="server" Text="เพิ่ม" CssClass="btn btn-primary" />
                <asp:Button ID="btnSave" runat="server" Text="บันทึก" CssClass="btn btn-success" />
            </td>
            <td style="text-align: right; padding: 0px; vertical-align:bottom">รวม
            </td>
            <asp:Repeater ID="rptSum" runat="server">
                <ItemTemplate>
                    <td style="padding: 0px; vertical-align:bottom" >
                        <asp:TextBox ID="txtSum" MaxLength="10" runat="server" Text="" Style="text-align: right; width: 100%; height: 35px" class="cost accounting" Enabled="false"></asp:TextBox>
                    </td>
                </ItemTemplate>
            </asp:Repeater>
        </tr>
        <tr>
            <td style="padding: 0px;" colspan="3">
            </td>
            <td style="text-align: right; padding: 0px; vertical-align:bottom">งบประมาณคงเหลือ
            </td>
            <asp:Repeater ID="RptTotal" runat="server">
                <ItemTemplate>
                    <td style="padding: 0px">
                        <asp:TextBox ID="txtSumTotal" MaxLength="10" runat="server" Text="" Style="text-align: right; width: 100%; height: 35px" class="cost accounting" Enabled="false"></asp:TextBox>
                    </td>
                </ItemTemplate>
            </asp:Repeater>
        </tr>
    </table>
</div>

<script type="text/javascript">
    function myFunction(txt, txtOldVal, textSUM, textSUMCOL, textTotal) {
        if (txt.value == "") {
            txt.value = 0
        }
        var valtxt = txt.value;

        if (txt.value == "") {
            valtxt = 0;
        }
        while (valtxt.search(",") >= 0) {
            valtxt = (valtxt + "").replace(',', '');
        }
        var valtxtOldVal = parseFloat(document.getElementById(txtOldVal).value);
        var valDiff = valtxt - valtxtOldVal;
        var vSum = document.getElementById(textSUM).value;
        var vSumCol = document.getElementById(textSUMCOL).value;
        var vTotal = document.getElementById(textTotal).value;
        if (document.getElementById(textSUM).value == "") {
            vSum = 0;
        }
        while (vSum.search(",") >= 0) {
            vSum = (vSum + "").replace(',', '');
        }
        if (document.getElementById(textSUMCOL).value == "") {
            vSumCol = 0;
        }
        while (vSumCol.search(",") >= 0) {
            vSumCol = (vSumCol + "").replace(',', '');
        }
        if (document.getElementById(textTotal).value == "") {
            vTotal = 0;
        }
        while (vTotal.search(",") >= 0) {
            vTotal = (vTotal + "").replace(',', '');
        }

        var vSumConvert = parseFloat(vSum) + parseFloat(valDiff);
        document.getElementById(textSUM).value = addCommas(vSumConvert.toFixed(2));
        var vSumColConvert = parseFloat(vSumCol) + parseFloat(valDiff);
        document.getElementById(textSUMCOL).value = addCommas(vSumColConvert.toFixed(2));
        var vTotalConvert = parseFloat(vTotal) - parseFloat(valDiff);
        document.getElementById(textTotal).value = addCommas(vTotalConvert.toFixed(2));
        if (valtxt == 0) {
            txt.value = ""
        }
        document.getElementById(txtOldVal).value = valtxt;
    }
</script>
