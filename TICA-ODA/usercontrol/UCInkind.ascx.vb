﻿Imports System.Data
Partial Class usercontrol_UCInkind
    Inherits System.Web.UI.UserControl
    Dim BL As New ODAENG

    Public Event EditEstimate()

    Dim _InkindDT As DataTable
    Public Property InkindDT As DataTable
        Get
            Return GetDataFromRpt(rptList)
        End Get
        Set(value As DataTable)
            _InkindDT = value
            rptList.DataSource = _InkindDT
            rptList.DataBind()
            txtEstimate_TextChanged(Nothing, Nothing)

        End Set
    End Property

    Public ReadOnly Property TotalEstimate As Decimal
        Get
            Return CDec(lblTotal.Text)
        End Get
    End Property

    Public Sub SetToViewMode(IsEnable As Boolean)
        btnAdd.Enabled = Not IsEnable
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlInkind As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlInkind"), DropDownList)
            Dim txtEstimate As TextBox = DirectCast(rptList.Items(i).FindControl("txtEstimate"), TextBox)
            Dim btnDelete As Button = DirectCast(rptList.Items(i).FindControl("btnDelete"), Button)

            ddlInkind.Enabled = Not IsEnable
            txtEstimate.Enabled = Not IsEnable
            btnDelete.Enabled = Not IsEnable
        Next
    End Sub


    Protected Sub txtEstimate_TextChanged(sender As Object, e As EventArgs)
        Dim _total As Decimal = 0.0
        For i As Integer = 0 To rptList.Items.Count - 1
            Try
                Dim txtEstimate As TextBox = DirectCast(rptList.Items(i).FindControl("txtEstimate"), TextBox)
                _total += CDec(txtEstimate.Text)
            Catch ex As Exception
            End Try
        Next

        lblTotal.Text = _total.ToString("#,##0.00")
        RaiseEvent EditEstimate()
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        Dim dt As DataTable = GetDataFromRpt(rptList)
        Dim dr As DataRow
        dr = dt.NewRow
        dr("id") = dt.Rows.Count + 1
        dr("inkind_id") = ""
        dr("estimate") = 0.0
        dt.Rows.Add(dr)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Function GetDataFromRpt(rpt As Repeater) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("inkind_id")
        dt.Columns.Add("estimate")

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlInkind As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlInkind"), DropDownList)
            Dim txtEstimate As TextBox = DirectCast(rptList.Items(i).FindControl("txtEstimate"), TextBox)


            dr = dt.NewRow
            dr("id") = dt.Rows.Count + 1
            dr("inkind_id") = ddlInkind.SelectedValue
            If txtEstimate.Text = "" Then
                dr("estimate") = 0.0
            Else
                dr("estimate") = txtEstimate.Text
            End If

            dt.Rows.Add(dr)


        Next

        Return dt
    End Function

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim ddlInkind As DropDownList = DirectCast(e.Item.FindControl("ddlInkind"), DropDownList)
        Dim txtEstimate As TextBox = DirectCast(e.Item.FindControl("txtEstimate"), TextBox)
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)

        BL.Bind_DDL_Inkind(ddlInkind)
        BL.SetTextDblKeypress(txtEstimate)

        lblID.Text = e.Item.DataItem("id").ToString
        ddlInkind.SelectedValue = e.Item.DataItem("inkind_id").ToString
        txtEstimate.Text = Convert.ToDecimal(e.Item.DataItem("estimate")).ToString("#,##0.00")


    End Sub
    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand

        If e.CommandName = "Delete" Then

            Dim _id As String = e.CommandArgument
            Dim dt As DataTable = DeleteDataInRpt(rptList, _id)
            rptList.DataSource = dt
            rptList.DataBind()

            txtEstimate_TextChanged(Nothing, Nothing)
        End If

    End Sub

    Function DeleteDataInRpt(rpt As Repeater, id As String) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("inkind_id")
        dt.Columns.Add("estimate")

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlInkind As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlInkind"), DropDownList)
            Dim txtEstimate As TextBox = DirectCast(rptList.Items(i).FindControl("txtEstimate"), TextBox)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)

            If lblID.Text <> id Then
                dr = dt.NewRow
                dr("id") = dt.Rows.Count + 1
                dr("inkind_id") = ddlInkind.SelectedValue
                If txtEstimate.Text = "" Then
                    dr("estimate") = 0.0
                Else
                    dr("estimate") = txtEstimate.Text
                End If

                dt.Rows.Add(dr)
            End If
        Next

        Return dt
    End Function
End Class
