﻿Imports System.Data
Partial Class usercontrol_UCProjectComponent
    Inherits System.Web.UI.UserControl
    Dim BL As New ODAENG

    Public Event EditBudget()

    Dim _ComponentDT As DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnAdd.Visible = False
    End Sub
    Public Property ComponentDT As DataTable
        Get
            Return GetDataFromRpt(rptList)
        End Get
        Set(value As DataTable)
            _ComponentDT = value
            If (_ComponentDT.Rows.Count > 0) Then
                pnlFooter.Visible = True
            Else
                pnlFooter.Visible = False
            End If

            rptList.DataSource = _ComponentDT
            rptList.DataBind()
            txtExpand_TextChanged(Nothing, Nothing)

        End Set
    End Property

    Public ReadOnly Property TotalBudget As Decimal
        Get
            Return CDec(lblTotal.Text)
        End Get
    End Property

    Public Sub SetToViewMode(IsEnable As Boolean)
        btnAdd.Enabled = Not IsEnable
        For i As Integer = 0 To rptList.Items.Count - 1
            'Dim ddlExpense As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlExpense"), DropDownList)
            Dim txtExpand As TextBox = DirectCast(rptList.Items(i).FindControl("txtExpand"), TextBox)
            Dim btnDelete As Button = DirectCast(rptList.Items(i).FindControl("btnDelete"), Button)
            Dim txtDate_payment As TextBox = DirectCast(rptList.Items(i).FindControl("txtDate_payment"), TextBox)
            'ddlExpense.Enabled = Not IsEnable
            txtExpand.Enabled = Not IsEnable
            btnDelete.Enabled = Not IsEnable
            txtDate_payment.Enabled = Not IsEnable
        Next
    End Sub



    Protected Sub txtExpand_TextChanged(sender As Object, e As EventArgs)
        Dim _total As Decimal = 0.0
        For i As Integer = 0 To rptList.Items.Count - 1
            Try
                Dim txtExpand As TextBox = DirectCast(rptList.Items(i).FindControl("txtExpand"), TextBox)
                _total += CDec(txtExpand.Text)
            Catch ex As Exception
            End Try
        Next

        If (rptList.Items.Count > 0) Then
            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If

        lblTotal.Text = _total.ToString("#,##0.00")

        RaiseEvent EditBudget()
    End Sub

    Public Sub btnAdd_Click(sender As Object, e As EventArgs)

        Dim dt As DataTable = GetDataDisbursement_FromRpt(rptList)
        Dim dr As DataRow
        dr = dt.NewRow
        dr("id") = dt.Rows.Count + 1
        dr("date_payment") = ""
        'dr("component_id") = ""
        dr("amount") = 0.0
        dt.Rows.Add(dr)


        dt.Columns.Add("no", GetType(Long))
        For i As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(i)("no") = i + 1
        Next

        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Function SetDataRpt(Activity_id As String) As DataTable
        Dim dt As New DataTable
        Dim dt_cv_datepayment As New DataTable
        Dim row As DataRow

        dt = BL.GetDataDisbursement_FromQurey(Activity_id)
        If dt.Rows.Count > 0 Then

            dt_cv_datepayment.Columns.Add("date_payment")
            For j As Integer = 0 To dt.Rows.Count - 1
                Dim _dr As DataRow = dt.Rows(j)

                Dim date_payment As String = Convert.ToDateTime(_dr("date_payment")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))

                row = dt_cv_datepayment.NewRow()
                row("date_payment") = date_payment
                dt_cv_datepayment.Rows.Add(row)

            Next

            dt.Columns.Remove("date_payment")
            dt.Columns.Add("date_payment")
            For j As Integer = 0 To dt_cv_datepayment.Rows.Count - 1
                Dim _dr As DataRow = dt_cv_datepayment.Rows(j)
                Dim date_payment As String = _dr("date_payment")
                dt.Rows(j)("date_payment") = date_payment
            Next



            dt.Columns.Add("no", GetType(Long))
            For i As Integer = 0 To dt.Rows.Count - 1
                dt.Rows(i)("no") = i + 1
            Next

            rptList.DataSource = dt
            rptList.DataBind()

        End If
        txtExpand_TextChanged(Nothing, Nothing)
        Return dt
    End Function


    Function GetDataFromRpt(rpt As Repeater) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("date_payment")
        dt.Columns.Add("amount", GetType(Decimal))

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            'Dim ddlExpense As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlExpense"), DropDownList)
            Dim txtDate_payment As TextBox = DirectCast(rptList.Items(i).FindControl("txtDate_payment"), TextBox)
            Dim txtExpand As TextBox = DirectCast(rptList.Items(i).FindControl("txtExpand"), TextBox)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)
            dr = dt.NewRow
            dr("id") = lblID.Text
            dr("date_payment") = txtDate_payment.Text
            If txtExpand.Text = "" Then
                dr("amount") = 0.0
            Else
                dr("amount") = txtExpand.Text
            End If

            dt.Rows.Add(dr)

        Next

        Return dt
    End Function

    Function GetDataDisbursement_FromRpt(rpt As Repeater) As DataTable
        Dim dt As New DataTable

        dt.Columns.Add("id")
        dt.Columns.Add("date_payment")
        dt.Columns.Add("amount", GetType(Decimal))

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim txtDate_payment As TextBox = DirectCast(rptList.Items(i).FindControl("txtDate_payment"), TextBox)
            Dim txtExpand As TextBox = DirectCast(rptList.Items(i).FindControl("txtExpand"), TextBox)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)
            dr = dt.NewRow

            'dr("id") = dt.Rows.Count + 1
            dr("id") = lblID.Text
            dr("date_payment") = txtDate_payment.Text
            If txtExpand.Text = "" Then
                dr("amount") = 0.0
            Else
                dr("amount") = txtExpand.Text
            End If

            dt.Rows.Add(dr)

        Next

        Return dt
    End Function

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        'Dim ddlExpense As DropDownList = DirectCast(e.Item.FindControl("ddlExpense"), DropDownList)
        Dim txtExpand As TextBox = DirectCast(e.Item.FindControl("txtExpand"), TextBox)
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)
        Dim txtDate_payment As TextBox = DirectCast(e.Item.FindControl("txtDate_payment"), TextBox)
        Dim lblNo As Label = DirectCast(e.Item.FindControl("lblNo"), Label)

        'BL.Bind_DDL_ExpenseSub(ddlExpense)
        BL.SetTextDblKeypress(txtExpand)

        lblNo.Text = e.Item.DataItem("no").ToString
        txtDate_payment.Text = e.Item.DataItem("date_payment").ToString

        lblID.Text = e.Item.DataItem("id").ToString
        'ddlExpense.SelectedValue = e.Item.DataItem("component_id").ToString
        txtExpand.Text = Convert.ToDecimal(e.Item.DataItem("amount")).ToString("#,##0.00")


    End Sub
    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand

        'If e.CommandName = "Delete" Then

        '    Dim _id As String = e.CommandArgument
        '    Dim dt As DataTable
        '    Dim ret As New ProcessReturnInfo
        '    ret = BL.DeleteActivityDisbursement(_id)
        '    If ret.IsSuccess Then


        '        SetDataRpt(lblActivityID.Text)
        '    End If


        'End If
        If e.CommandName = "Delete" Then

            Dim _id As String = e.CommandArgument
            Dim dt As DataTable = DeleteDataInRpt(rptList, _id)
            rptList.DataSource = dt
            rptList.DataBind()

        End If

    End Sub

    Function SetToValueActivityId_Disbursement(Id_Activity As String) As String
        lblActivityID.Text = Id_Activity
        Return lblActivityID.Text
    End Function

    Function DeleteDataInRpt(rpt As Repeater, id As String) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("date_payment")
        dt.Columns.Add("amount")

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim txtExpand As TextBox = DirectCast(rptList.Items(i).FindControl("txtExpand"), TextBox)
            Dim txtDate_payment As TextBox = DirectCast(rptList.Items(i).FindControl("txtDate_payment"), TextBox)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)

            If lblID.Text <> id Then
                dr = dt.NewRow
                dr("id") = lblID.Text
                dr("date_payment") = txtDate_payment.Text
                If txtExpand.Text = "" Then
                    dr("amount") = 0.0
                Else
                    dr("amount") = txtExpand.Text
                End If

                dt.Rows.Add(dr)


            End If
        Next

        dt.Columns.Add("no", GetType(Long))

        For i As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(i)("no") = i + 1
        Next

        rptList.DataSource = dt
        rptList.DataBind()
        txtExpand_TextChanged(Nothing, Nothing)
        Return dt
    End Function
End Class
