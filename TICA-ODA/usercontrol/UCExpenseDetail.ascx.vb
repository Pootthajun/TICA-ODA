﻿Imports System.Data
Partial Class usercontrol_UCExpenseDetail
    Inherits System.Web.UI.UserControl
    Dim BL As New ODAENG
    Dim GL As New GenericLib

    Dim _DT As DataTable
    Dim _TemplateID As String
    Dim _ActivityID As String
    Dim _ExpenseID As String
    Dim _Type As String
    Dim _PID As String
    Dim _ViewMode As Boolean

#Region "_Property"
    Public Property DetailDT As DataTable
        Get
            '    Return GetDataFromRpt(rptList)
        End Get
        Set(value As DataTable)
            _DT = value
            rptList.DataSource = _DT
            rptList.DataBind()

            'txtAmount_TextChanged(Nothing, Nothing)
        End Set
    End Property

    Public WriteOnly Property TemplateID As String
        Set(value As String)
            _TemplateID = value
        End Set
    End Property

    Public WriteOnly Property Type As String
        Set(value As String)
            _Type = value
        End Set
    End Property

    Public WriteOnly Property PID As String
        Set(value As String)
            _PID = value
        End Set
    End Property

    Public WriteOnly Property ActivityID As String
        Set(value As String)
            _ActivityID = value
        End Set
    End Property

    Public WriteOnly Property EditExpenseID As Long
        Set(value As Long)
            _ExpenseID = value
        End Set
    End Property

    Public WriteOnly Property ViewMode As Boolean
        Set(value As Boolean)
            _ViewMode = value
        End Set
    End Property
#End Region

    Public Sub setCol_rpt(template_id As String)
        Dim dt_col As New DataTable

        dt_col = BL.GetList_SubExpenseByTemplate(GL.ConvertCINT(template_id))

        rptCol.DataSource = dt_col
        rptCol.DataBind()
    End Sub

    Public Sub setHead_rpt(template_id As String)


        Dim dt_col As New DataTable
        dt_col = BL.GetList_GroupExpenseForTB(GL.ConvertCINT(template_id))

        rptHead.DataSource = dt_col
        rptHead.DataBind()

        For i As Integer = 0 To rptHead.Items.Count - 1
            Dim lblCount As Label = rptHead.Items(i).FindControl("lblCount")
            Dim colspan As HtmlTableCell = rptHead.Items(i).FindControl("TB_head")
            colspan.Attributes("colspan") = lblCount.Text
        Next
    End Sub

    Public Sub setPlan_rpt(activity_id As String, template_id As String, header_id As String)
        Dim dt_plan As New DataTable
        dt_plan = BL.GetList_SubExpenseDetailPlanGroup(GL.ConvertCINT(activity_id), GL.ConvertCINT(header_id), GL.ConvertCINT(template_id))
        rptPlan.DataSource = dt_plan
        rptPlan.DataBind()
    End Sub

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim lblrecipince_type As Label = DirectCast(e.Item.FindControl("lblrecipince_type"), Label)
        Dim lblnodeid As Label = DirectCast(e.Item.FindControl("lblnodeid"), Label)
        Dim lblname As Label = DirectCast(e.Item.FindControl("lblname"), Label)
        Dim rptColItem2 As Repeater = DirectCast(e.Item.FindControl("rptColItem2"), Repeater)
        Dim img As HtmlImage = DirectCast(e.Item.FindControl("imgicon"), HtmlImage)

        lblnodeid.Text = e.Item.DataItem("country_node_id").ToString
        lblname.Text = e.Item.DataItem("name_th").ToString
        'lblrecipince_type.Text = e.Item.DataItem("rectype").ToString
        Dim lblcountryid As String = e.Item.DataItem("country_node_id").ToString


        img.Src = ""
        img.Src = "../Flag/" & lblcountryid & ".png"

        Dim dt As New DataTable
        dt.Columns.Add("expense_sub_id")
        dt.Columns.Add("amount_actual")



        Dim dtSubEx As DataTable = BL.GetList_SubExpenseByTemplate(_TemplateID)

        If dtSubEx.Rows.Count > 0 Then
            Dim drc As DataRow
            For i As Integer = 0 To dtSubEx.Rows.Count - 1
                drc = dt.NewRow()
                drc("expense_sub_id") = dtSubEx.Rows(i)("id").ToString
                drc("amount_actual") = 0
                dt.Rows.Add(drc)
            Next
        End If

        AddHandler rptColItem2.ItemDataBound, AddressOf rptColItem2_ItemDataBound

        Dim dtActual As DataTable = BL.GetList_SubExpenseDetailActual(GL.ConvertCINT(_ActivityID), GL.ConvertCINT(_ExpenseID), GL.ConvertCINT(_TemplateID), lblnodeid.Text)
        If dtActual.Rows.Count > 0 Then
            For i As Integer = 0 To dtActual.Rows.Count - 1
                Dim a As String = dtActual.Rows(i)("expense_sub_id").ToString
                For j As Integer = 0 To dt.Rows.Count - 1
                    If a = dt.Rows(j)("expense_sub_id").ToString Then
                        dt.Rows(j)("amount_actual") += dtActual.Rows(i)("amount")
                    End If
                Next
            Next
        End If
        rptColItem2.DataSource = dt
        rptColItem2.DataBind()

        lblType.Text = _Type
        lblPID.Text = _PID

        lblactid.Text = _ActivityID
        lblHid.Text = _ExpenseID
    End Sub

    Protected Sub rptColItem2_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim lbamount As TextBox = DirectCast(e.Item.FindControl("lbamount"), TextBox)
        Dim lblitem_subexpenseid As Label = DirectCast(e.Item.FindControl("lblitem_subexpenseid"), Label)
        Dim Pay_Amount_Actual As Label = DirectCast(e.Item.FindControl("Pay_Amount_Actual"), Label)
        BL.SetTextDblKeypress(lbamount)


        lbamount.Text = Convert.ToDecimal(e.Item.DataItem("amount_actual")).ToString("#,##0.00")
        If lbamount.Text = 0 Then
            lbamount.Text = ""
        End If
        lbamount.Enabled = False

        lblitem_subexpenseid.Text = e.Item.DataItem("expense_sub_id").ToString
        'Pay_Amount_Actual.Text = e.Item.DataItem("Pay_Amount_Actual").ToString

    End Sub


    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand

        If e.CommandName = "cmdEditPay" Then
            Dim a As Double = lblHid.Text
            Response.Redirect("frmActivityExActual.aspx?id=" & e.CommandArgument & "&mode=edit" & "&Header_id=" & a & "&Activity_id=" & lblactid.Text & "&PID=" & lblPID.Text & "&Type=" & lblType.Text)
        End If

    End Sub

    Private Sub rptPlan_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptPlan.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim lblsubexpenseid As Label = DirectCast(e.Item.FindControl("lblsubexpenseid"), Label)
        Dim txtCol As TextBox = DirectCast(e.Item.FindControl("txtCol"), TextBox)

        txtCol.Text = Convert.ToDecimal(e.Item.DataItem("amount")).ToString("#,##0.00")
        If txtCol.Text = 0 Then
            txtCol.Text = ""
        End If
        txtCol.Enabled = False

        lblsubexpenseid.Text = e.Item.DataItem("expense_sub_id").ToString
    End Sub

    Private Sub btnEditPlan_Click(sender As Object, e As EventArgs) Handles btnEditPlan.Click
        Dim a As Double = lblHid.Text
        Response.Redirect("frmActivityExPlan.aspx?id=" & lblactid.Text & "&mode=edit" & "&Header_id=" & a & "&Activity_id=" & lblactid.Text & "&PID=" & lblPID.Text & "&Type=" & lblType.Text)

    End Sub
End Class

