﻿Imports System.Data
Imports LinqDB.ConnectDB
Public Class UCActivityTreeNode
    Inherits System.Web.UI.UserControl

    Public Event ImageModeClick(sender As UCActivityTreeNode, IsExpand As Boolean)
    Public Event AddSubActivity(sender As UCActivityTreeNode)
    Public Event EditActivity(sender As UCActivityTreeNode)
    Public Event DeleteActivity(ActivityID As Long)
    Public Event ViewActivity(sender As UCActivityTreeNode)

    Dim urlExpand As String = "../dist/img/ExpandMore.png"   'แตกโหนด
    Dim urlCollapsed As String = "../dist/img/ChevronRight.png"
    Dim urlNoChild As String = "../dist/img/ExpandNull.png"


    Public Property ActivityID As String
        Get
            Return lblActivityID.Text.Trim
        End Get
        Set(value As String)
            lblActivityID.Text = value
        End Set
    End Property
    Public Property ActivityName As String
        Get
            Return lblActivityName.Text.Trim
        End Get
        Set(value As String)
            lblActivityName.Text = value
        End Set
    End Property



    Public Property ParentID As String
        Get
            Return lblParentID.Text
        End Get
        Set(value As String)
            lblParentID.Text = value
        End Set
    End Property


    Public Property ImageMode As System.Web.UI.WebControls.Image
        Get
            Return imgMode
        End Get
        Set(value As System.Web.UI.WebControls.Image)
            imgMode = value
        End Set
    End Property

    Public Property ImageIcon As System.Web.UI.WebControls.Image
        Get
            Return imgIcon
        End Get
        Set(value As System.Web.UI.WebControls.Image)
            imgIcon = value
        End Set
    End Property
    Public ReadOnly Property ImageCollapsed As String
        Get
            Return urlCollapsed
        End Get
    End Property


    Public Property IsExpand As Boolean
        Get
            Return lblExpand.Text
        End Get
        Set(value As Boolean)
            lblExpand.Text = value
        End Set
    End Property

    Public Property PlanStart As String
        Get
            Return Converter.StringToDate(lblPlanStart.Text, "dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
        End Get
        Set(value As String)
            lblPlanStart.Text = value
        End Set
    End Property

    Public Property PlanEnd As String
        Get
            Return Converter.StringToDate(lblPlanEnd.Text, "dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
        End Get
        Set(value As String)
            lblPlanEnd.Text = value
        End Set
    End Property

    Public Property CommitmentBudget As Decimal
        Get
            Return CDec(lblCommitmentBudget.Text)
        End Get
        Set(value As Decimal)
            lblCommitmentBudget.Text = value.ToString("##,0##.00")
        End Set
    End Property

    Public Property Expense As Decimal
        Get
            Return CDec(lblExpense.Text)
        End Get
        Set(value As Decimal)
            lblExpense.Text = value.ToString("##,0##.00")
        End Set
    End Property

    Public Property aid As String
        Get
            Return lblAid.Text.Trim
        End Get
        Set(value As String)
            lblAid.Text = value
        End Set
    End Property
    Public Property ChildQty As Integer
        Get
            Return lblChildQty.Text
        End Get
        Set(value As Integer)
            lblChildQty.Text = value

            If value > 0 Then
                imgMode.ImageUrl = urlCollapsed
                imgMode.Style.Add(HtmlTextWriterStyle.Cursor, "cursor")
            Else
                imgMode.Style.Add(HtmlTextWriterStyle.Cursor, "default")
            End If
        End Set
    End Property

    Public Sub SetBudgetColor(color As System.Drawing.Color)
        lblCommitmentBudget.ForeColor = color
    End Sub

    Public Sub SetToolTipCommitmentBudget(strtooltip As String)
        lblCommitmentBudget.ToolTip = strtooltip
    End Sub

    Public Property NodeLevel As Integer
        Get
            Return tdCellIndent.Attributes("Level")
        End Get
        Set(value As Integer)
            tdCellIndent.Attributes("Level") = value
            tdCellIndent.Style("padding-left") = (value * IndextLevelPixel) & "px"
        End Set
    End Property
    Public Property IndextLevelPixel As Integer
        Get
            Return tdCellIndent.Attributes("IndextLevelPixel")
        End Get
        Set(value As Integer)
            tdCellIndent.Attributes("IndextLevelPixel") = value
        End Set
    End Property
    Public Property NodeRepeaterItemIndex As Integer
        Get
            Return lblNodeRepeaterItemIndex.Text
        End Get
        Set(value As Integer)
            lblNodeRepeaterItemIndex.Text = value
        End Set
    End Property

    Public WriteOnly Property SetViewMode As Boolean
        Set(value As Boolean)
            likEditActivity.Visible = Not value
            likViewActivity.Visible = value

            likAddActivity.Visible = Not value
            likDeleteActivity.Visible = Not value

        End Set
    End Property

    Private Sub imgMode_Click(sender As Object, e As ImageClickEventArgs) Handles imgMode.Click
        If imgMode.ImageUrl <> urlNoChild Then
            If lblExpand.Text.ToLower = "true" Then
                lblExpand.Text = "false"
                imgMode.ImageUrl = urlCollapsed
            Else
                lblExpand.Text = "true"
                imgMode.ImageUrl = urlExpand
            End If
            RaiseEvent ImageModeClick(Me, lblExpand.Text)
            imgMode.Enabled = True
        Else
            imgMode.Enabled = False
        End If
    End Sub

    Private Sub likAddActivity_Click(sender As Object, e As EventArgs) Handles likAddActivity.Click
        RaiseEvent AddSubActivity(Me)
    End Sub

    Private Sub likEditActivity_Click(sender As Object, e As EventArgs) Handles likEditActivity.Click
        RaiseEvent EditActivity(Me)
    End Sub

    Private Sub likDeleteActivity_Click(sender As Object, e As EventArgs) Handles likDeleteActivity.Click
        RaiseEvent DeleteActivity(lblActivityID.Text)
    End Sub

    Private Sub likViewActivity_Click(sender As Object, e As EventArgs) Handles likViewActivity.Click
        RaiseEvent ViewActivity(Me)
    End Sub

End Class
