﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCActivityTreeNode.ascx.vb" Inherits="UCActivityTreeNode" %>


<style>
    .img-node{
        vertical-align: sub;
        margin: 0 3px;
    }
    .right {
        padding-right: 10px;
    }
    .gridScrollDiv
    {
        border: 1px solid #CCCCCC;
        height: 500px;
        overflow-y: scroll;
    }
</style>

<tr onmouseover='ChangeBackgroundColor(this,<%=lblActivityName.ClientID %>)' onmouseout='RestoreBackgroundColor(this,<%=lblActivityName.ClientID %>)'  style='cursor:Default;'>
    <td id="tdCellIndent" runat="server" Level="0" IndextLevelPixel="20" Mode="NoChild" Class='tdbordertb tdborderl'>
        <asp:ImageButton ID="imgMode" runat="server" Width="16px" Height="16px" CssClass="img-node" ImageUrl="../dist/img/ExpandNull.png"></asp:ImageButton>
        <asp:Image ID="imgIcon" runat="server"></asp:Image>
        <asp:Label ID="lblActivityName" runat="server"></asp:Label>
        <asp:Label ID="lblActivityID" runat="server" Visible="false" Text="0" ></asp:Label>    
        <asp:Label ID="lblParentID" runat="server" Visible="false" Text="0" ></asp:Label>
        <asp:Label ID="lblChildQty" runat="server" Visible="false" Text="0" ></asp:Label>
        <asp:Label ID="lblExpand" runat="server" Visible="false" ></asp:Label>
        <asp:Label ID="lblNodeRepeaterItemIndex" runat="server" Visible="false" Text="0" ></asp:Label>    
    </td>
    <td Class='tdbordertb date'><asp:Label ID="lblPlanStart" runat="server"></asp:Label></td>
    <td Class='tdbordertb date'><asp:Label ID="lblPlanEnd" runat="server"></asp:Label></td>  
<%--    <td Class='tdbordertb cost right'><asp:Label ID="lblDuration" runat="server"></asp:Label><span> Days</span></td>--%>
        <td Class='tdbordertb date'><asp:Label ID="lblAid" runat="server"></asp:Label></td>
    <td Class='tdbordertb cost right'><asp:Label ID="lblExpense" runat="server"></asp:Label></td>
    <td Class='tdbordertb cost right'><asp:Label ID="lblCommitmentBudget" runat="server"></asp:Label></td>
  

<%--    <asp:Literal ID="ltActivity" runat="server"></asp:Literal>--%>


<%--    <td Class='tdbordertb tdborderr' style="text-align:left;" id="tdAddButton" runat="server" >
        <asp:LinkButton ID="likAddActivity" runat="server" >
            <i Class='fa fa-plus text-success'></i>
        </asp:LinkButton>
    </td>
    <td Class='tdbordertb tdborderr' style="text-align:center;">
        <asp:LinkButton ID="likEditActivity" runat="server" >
            <i Class='fa fa-edit text-success'></i>
        </asp:LinkButton>

        <asp:LinkButton ID="likViewActivity" runat="server" Visible="false" >
            <i Class='fa fa-search text-success'></i>
        </asp:LinkButton>
    </td>
    <td Class='tdbordertb tdborderr' style="text-align:center;" id="tdDeleteButton" runat="server" >
        <asp:LinkButton ID="likDeleteActivity" runat="server" OnClientClick='javascript:return confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?")' >
            <i Class='fa fa-trash text-danger'></i>
        </asp:LinkButton>
    </td>--%>

<%--   <asp:Panel ID="pnlActivity" runat="server" style="overflow-x:scroll; overflow-y:auto" >--%>


<%--<td style = ' border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr "  > </td>
<td style = ' border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr "  > </td>
<td style = ' border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr "  > </td>
<td style = ' border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr "  > </td>
<td style = ' border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr "  > </td>
<td style = ' border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr "  > </td>
<td style = ' border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr "  > </td>
<td style = ' border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr "  > </td>
<td style = ' border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr "  > </td>
<td style = ' border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr "  > </td>
<td style = ' border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr "  > </td>
<td style = ' border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr "  > </td>--%>


<%--     <asp:Repeater ID="rptCol" runat="server" Visible ="true" >
                                                <ItemTemplate>
                                                    <th id="th_radius_2" runat ="server" >
                                                        <asp:Label ID="lbl_Col_id" runat="server" Visible="false" ></asp:Label>

                                                        <asp:Label ID="lbl_Col_Name" runat="server" ></asp:Label>
                                                    </th>
                                                </ItemTemplate>
                                            </asp:Repeater>--%>
<%--  </asp:Panel>--%>
    
    <td Class='tdbordertb tdborderr' style="text-align:center;border-left-width: 1px; border-left-style: outset; ">

        <%--ปุ่มเพิ่ม--%>
        <asp:LinkButton ID="likAddActivity" runat="server" ToolTip="เพิ่ม" style="padding-right: 5px;" >
            <i Class='fa fa-plus text-success'></i>
        </asp:LinkButton>

        <asp:LinkButton ID="likEditActivity" runat="server" ToolTip="แก้ไข"  style="padding-right: 5px;" >
            <i Class='fa fa-edit text-success'></i>
        </asp:LinkButton>

        <asp:LinkButton ID="likViewActivity" runat="server" Visible="false" ToolTip="เรียกดู"  style="padding-right: 5px;" >
            <i Class='fa fa-search text-success'></i>
        </asp:LinkButton>

        <asp:LinkButton ID="likDeleteActivity" runat="server" OnClientClick='javascript:return confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?")' ToolTip="ลบ"  >
            <i Class='fa fa-trash text-danger'></i>
        </asp:LinkButton>


    </td>



</tr> 