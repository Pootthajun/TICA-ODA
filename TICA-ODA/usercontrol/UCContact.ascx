﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCContact.ascx.vb" Inherits="usercontrol_UCContact" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<table>
    <tr>
       <%-- <td style="width: 250px">
            <p class="pull-right">Contact Person (ข้อมูลผู้ติดต่อ) :</p>
        </td>--%>
        <td>
            <table class="table table-bordered">
                <tr>
                    <th style="width: 150px" class="bg-gray">
                        <p class="pull-right">Name (ชื่อ) :</p>
                    </th>
                    <td>
                        <div class="col-sm-12">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <asp:TextBox ID="txtContactName" runat="server" CssClass="form-control" placeholder="" MaxLength="250"></asp:TextBox>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th style="width: 50px" class="bg-gray">
                        <p class="pull-right">Position (ตำแหน่ง):</p>
                    </th>
                    <td>
                        <div class="col-sm-12">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-building"></i>
                                </div>
                                <asp:TextBox ID="txtContactPosition" runat="server" CssClass="form-control" placeholder="" MaxLength="250"></asp:TextBox>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th style="width: 50px" class="bg-gray">
                        <p class="pull-right">Phone (โทรศัพท์) :</p>
                    </th>
                    <td>
                        <div class="col-sm-12">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <asp:TextBox ID="txtContactPhone" runat="server" CssClass="form-control" placeholder="" MaxLength="10"></asp:TextBox>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th style="width: 50px" class="bg-gray">
                        <p class="pull-right">Fax (โทรสาร) :</p>
                    </th>
                    <td>
                        <div class="col-sm-12">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-fax"></i>
                                </div>
                                <asp:TextBox ID="txtContactFax" runat="server" CssClass="form-control" placeholder="" MaxLength="10"></asp:TextBox>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th style="width: 50px" class="bg-gray">
                        <p class="pull-right">Email :</p>
                    </th>
                    <td>
                        <div class="col-sm-12">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-list-alt"></i>
                                </div>
                                <asp:TextBox ID="txtContactEmail" runat="server" CssClass="form-control" Width="350px" MaxLength="100"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Style="color: red"
                                    ControlToValidate="txtContactEmail" Display="Dynamic"
                                    ErrorMessage="กรุณากรอกอีเมล์ให้ถูกต้อง!"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True">
                                </asp:RegularExpressionValidator>
                                <%--<asp:TextBox ID="txtContactEmail" runat="server" CssClass="form-control" placeholder=""></asp:TextBox>--%>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
