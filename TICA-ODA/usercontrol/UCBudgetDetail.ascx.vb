﻿Imports System.Data
Partial Class usercontrol_UCBudgetDetail
    Inherits System.Web.UI.UserControl
    Dim BL As New ODAENG

    Dim _DT As DataTable
    Public Property DetailDT As DataTable
        Get
            Return GetDataFromRpt(rptList)
        End Get
        Set(value As DataTable)
            _DT = value
            rptList.DataSource = _DT
            rptList.DataBind()
            txtReceived_TextChanged(Nothing, Nothing)
            If _DT.Rows.Count > 0 Then
                lblTypeBudget.Text = "Budget Group(กลุ่มงบประมาณ) : " & _DT.Rows(0)("group_name").ToString
            End If
        End Set
    End Property

    Public Sub SetToViewMode(IsEnable As Boolean)
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim txtReceived As TextBox = DirectCast(rptList.Items(i).FindControl("txtReceived"), TextBox)
            txtReceived.Enabled = Not IsEnable
        Next
    End Sub


    Private Sub usercontrol_UCBudgetDetail(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then

        End If
    End Sub

    Protected Sub txtReceived_TextChanged(sender As Object, e As EventArgs)
        Dim _total_received As Double = 0.0
        Dim _total_Budget As Double = 0.0
        Dim _total_Used As Double = 0.0
        Dim _total_Balance As Double = 0.0
        For i As Integer = 0 To rptList.Items.Count - 1
            Try
                Dim txtReceived As TextBox = DirectCast(rptList.Items(i).FindControl("txtReceived"), TextBox)
                Dim lblbudget_sub_id As Label = DirectCast(rptList.Items(i).FindControl("lblbudget_sub_id"), Label)
                Dim lblReceived As Label = DirectCast(rptList.Items(i).FindControl("lblReceived"), Label)

                Dim lblBudget As Label = DirectCast(rptList.Items(i).FindControl("lblBudget"), Label)
                Dim lblUsed As Label = DirectCast(rptList.Items(i).FindControl("lblUsed"), Label)
                Dim lblBalance As Label = DirectCast(rptList.Items(i).FindControl("lblBalance"), Label)

                If lblbudget_sub_id.Text <> "-1" Then
                    If txtReceived.Text <> "" Then
                        _total_received += CDbl(txtReceived.Text)
                    End If

                    _total_Budget += CDbl(lblBudget.Text)
                    _total_Used += CDbl(lblUsed.Text)
                    _total_Balance += CDbl(lblBalance.Text)
                Else
                    _total_received += 0.0
                    _total_Budget += 0.0
                    _total_Used += 0.0
                    _total_Balance += 0.0
                End If


                If lblbudget_sub_id.Text = "-1" Then
                    txtReceived.Text = _total_received.ToString("#,##0.00")
                    lblReceived.Text = _total_received.ToString("#,##0.00")

                    lblBudget.Text = _total_Budget.ToString("#,##0.00")
                    lblUsed.Text = _total_Used.ToString("#,##0.00")
                    lblBalance.Text = _total_Balance.ToString("#,##0.00")
                End If
            Catch ex As Exception

            End Try
        Next

    End Sub


    Function GetDataFromRpt(rpt As Repeater) As DataTable
        Dim dt As New DataTable
        With dt
            .Columns.Add("seq")
            .Columns.Add("budget_sub_id")
            .Columns.Add("amount")
            .Columns.Add("Budget")
            .Columns.Add("Used")
            .Columns.Add("Balance")
        End With

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim txtReceived As TextBox = DirectCast(rptList.Items(i).FindControl("txtReceived"), TextBox)
            Dim lblbudget_sub_id As Label = DirectCast(rptList.Items(i).FindControl("lblbudget_sub_id"), Label)

            If lblbudget_sub_id.Text <> "-1" Then
                dr = dt.NewRow
                dr("seq") = dt.Rows.Count + 1
                dr("budget_sub_id") = lblbudget_sub_id.Text

                If txtReceived.Text = "-" Or txtReceived.Text = "" Then
                    dr("amount") = "0"
                Else
                    dr("amount") = txtReceived.Text
                End If

                dr("Budget") = "0"
                dr("Used") = "0"
                dr("Balance") = "0"
                dt.Rows.Add(dr)
            End If

        Next

        Return dt
    End Function

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim lblbudget_sub_id As Label = DirectCast(e.Item.FindControl("lblbudget_sub_id"), Label)
        Dim lblno As Label = DirectCast(e.Item.FindControl("lblno"), Label)
        Dim lblSubBudget As Label = DirectCast(e.Item.FindControl("lblSubBudget"), Label)
        Dim txtReceived As TextBox = DirectCast(e.Item.FindControl("txtReceived"), TextBox)
        Dim lblBudget As Label = DirectCast(e.Item.FindControl("lblBudget"), Label)
        Dim lblUsed As Label = DirectCast(e.Item.FindControl("lblUsed"), Label)
        Dim lblBalance As Label = DirectCast(e.Item.FindControl("lblBalance"), Label)
        Dim lblReceived As Label = DirectCast(e.Item.FindControl("lblReceived"), Label)
        Dim tdSubBudget As HtmlTableCell = DirectCast(e.Item.FindControl("tdSubBudget"), HtmlTableCell)

        lblbudget_sub_id.Text = e.Item.DataItem("id").ToString
        lblno.Text = e.Item.DataItem("seq").ToString
        lblSubBudget.Text = e.Item.DataItem("sub_name").ToString

        lblBudget.Text = Convert.ToDecimal(e.Item.DataItem("Budget")).ToString("#,##0.00") 'e.Item.DataItem("Budget").ToString
        lblUsed.Text = Convert.ToDecimal(e.Item.DataItem("Used")).ToString("#,##0.00") 'e.Item.DataItem("Used").ToString
        lblBalance.Text = Convert.ToDecimal(e.Item.DataItem("Balance")).ToString("#,##0.00") 'e.Item.DataItem("Balance").ToString 
        lblReceived.Text = Convert.ToDecimal(e.Item.DataItem("amount")).ToString("#,##0.00")

        BL.SetTextDblKeypress(txtReceived)

        If e.Item.DataItem("amount").ToString.Trim() = "0" Or e.Item.DataItem("amount").ToString.Trim() = "0.00" Then
            'txtReceived.Text = "-"
        Else
            txtReceived.Text = Convert.ToDecimal(e.Item.DataItem("amount")).ToString("#,##0.00")
        End If

        txtReceived.Visible = True
        lblReceived.Visible = False
        If lblbudget_sub_id.Text = "-1" Then
            lblSubBudget.Font.Bold = True
            tdSubBudget.Attributes.Add("style", "text-align:right")

            txtReceived.Visible = False
            lblReceived.Visible = True
        End If

        Try
            If CDbl(lblBalance.Text) < 0 Then
                lblUsed.ForeColor = System.Drawing.Color.Red
            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand

    End Sub


End Class
