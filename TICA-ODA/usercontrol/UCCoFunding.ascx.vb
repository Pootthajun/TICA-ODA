﻿Imports System.Data
Partial Class usercontrol_UCCoFunding
    Inherits System.Web.UI.UserControl
    Dim BL As New ODAENG

    Dim _CoFundingDT As DataTable
    Public Property CoFundingDT As DataTable
        Get
            Return GetDataFromRpt(rptList)
        End Get
        Set(value As DataTable)
            _CoFundingDT = value

            If (_CoFundingDT.Rows.Count > 0) Then
                pnlFooter.Visible = True
            Else
                pnlFooter.Visible = False
            End If

            rptList.DataSource = _CoFundingDT
            rptList.DataBind()

            txtAmount_TextChanged(Nothing, Nothing)
            'If _CoFundingDT.Rows.Count > 0 Then
            '    rptList.DataSource = _CoFundingDT
            '    rptList.DataBind()
            'Else
            '    Dim dt As DataTable = GetDataFromRpt(rptList)
            '    Dim dr As DataRow
            '    dr = dt.NewRow
            '    dr("id") = "1"
            '    dr("country_id") = ""
            '    dr("amount") = 0
            '    dt.Rows.Add(dr)

            '    rptList.DataSource = dt
            '    rptList.DataBind()
            'End If
        End Set
    End Property

    Public Sub SetToViewMode(IsEnable As Boolean)
        btnAdd.Enabled = Not IsEnable
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlCountry As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlCountry"), DropDownList)
            Dim txtAmount As TextBox = DirectCast(rptList.Items(i).FindControl("txtAmount"), TextBox)
            Dim btnDelete As Button = DirectCast(rptList.Items(i).FindControl("btnDelete"), Button)

            ddlCountry.Enabled = Not IsEnable
            txtAmount.Enabled = Not IsEnable
            btnDelete.Enabled = Not IsEnable
        Next
    End Sub

    Private Sub page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then

        End If
    End Sub

    Protected Sub txtAmount_TextChanged(sender As Object, e As EventArgs)


        Dim _total As Double = 0.0
        For i As Integer = 0 To rptList.Items.Count - 1
            Try
                Dim txtAmount As TextBox = DirectCast(rptList.Items(i).FindControl("txtAmount"), TextBox)
                _total += CDbl(txtAmount.Text)
            Catch ex As Exception
            End Try
        Next

        If (rptList.Items.Count > 0) Then
            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If
        lblTotal.Text = _total.ToString("#,##0.00")
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        Dim dt As DataTable = GetDataFromRpt(rptList)
        Dim dr As DataRow
        dr = dt.NewRow
        dr("id") = dt.Rows.Count + 1
        dr("country_id") = ""
        dr("amount") = 0.0
        dt.Rows.Add(dr)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Function GetDataFromRpt(rpt As Repeater) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("country_id")
        dt.Columns.Add("amount", GetType(Decimal))

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlCountry As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlCountry"), DropDownList)
            Dim txtAmount As TextBox = DirectCast(rptList.Items(i).FindControl("txtAmount"), TextBox)

            dr = dt.NewRow
            dr("id") = dt.Rows.Count + 1
            dr("country_id") = ddlCountry.SelectedValue
            If txtAmount.Text = "" Then
                dr("amount") = 0.0
            Else
                dr("amount") = CDbl(txtAmount.Text)
            End If

            dt.Rows.Add(dr)

        Next

        Return dt
    End Function

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim ddlCountry As DropDownList = DirectCast(e.Item.FindControl("ddlCountry"), DropDownList)
        Dim txtAmount As TextBox = DirectCast(e.Item.FindControl("txtAmount"), TextBox)
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)

        BL.Bind_DDL_CoFunding(ddlCountry)
        BL.SetTextDblKeypress(txtAmount)

        lblID.Text = e.Item.DataItem("id").ToString
        ddlCountry.SelectedValue = e.Item.DataItem("country_id").ToString
        txtAmount.Text = Convert.ToDecimal(e.Item.DataItem("amount")).ToString("#,##0.00")


    End Sub
    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand

        If e.CommandName = "Delete" Then

            Dim _id As String = e.CommandArgument
            Dim dt As DataTable = DeleteDataInRpt(rptList, _id)
            rptList.DataSource = dt
            rptList.DataBind()

            txtAmount_TextChanged(Nothing, Nothing)
        End If

    End Sub

    Function DeleteDataInRpt(rpt As Repeater, id As String) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("country_id")
        dt.Columns.Add("amount")

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlCountry As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlCountry"), DropDownList)
            Dim txtAmount As TextBox = DirectCast(rptList.Items(i).FindControl("txtAmount"), TextBox)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)

            If lblID.Text <> id Then
                dr = dt.NewRow
                dr("id") = dt.Rows.Count + 1
                dr("country_id") = ddlCountry.SelectedValue
                If txtAmount.Text = "" Then
                    dr("amount") = 0.0
                Else
                    dr("amount") = txtAmount.Text
                End If

                dt.Rows.Add(dr)
            End If
        Next

        Return dt
    End Function
End Class
