﻿Imports System.Data
Imports LinqDB.ConnectDB
Imports System.Data.SqlClient

Public Class UCActivityTreeList
    Inherits System.Web.UI.UserControl

    Public Event AddChildNode(ParentID As Long)
    Public Event EditChildNode(ActivityID As Long)
    Public Event ViewChildNode(ActivityID As Long)

    Dim BL As New ODAENG

    Public ReadOnly Property ProjectID As Long
        Get
            Return lblProjectID.Text
        End Get
    End Property
    Public ReadOnly Property ActivityMode As String
        Get
            Return lblActivityMode.Text
        End Get
    End Property

    Public WriteOnly Property SetViewMode As Boolean
        Set(value As Boolean)
            'tdHAdd.Visible = Not value
            'tdHDelete.Visible = Not value
        End Set
    End Property


    Public ReadOnly Property CurrrentDocumentsData() As DataTable
        Get
            Dim DT As DataTable = BlankNodesData()
            For Each Item As RepeaterItem In rpt.Items
                If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For

                Dim DR As DataRow = DT.NewRow
                Dim NodeItem As UCActivityTreeNode = Item.FindControl("UCActivityTreeNode1")
                DR("id") = NodeItem.ActivityID
                DR("activity_name") = NodeItem.ActivityName
                DR("parent_id") = NodeItem.ParentID
                DR("Level") = NodeItem.NodeLevel
                'DR("duration") = NodeItem.Duration
                DR("actual_start") = Converter.StringToDate(NodeItem.PlanStart, "dd/MM/yyyy")
                DR("actual_end") = Converter.StringToDate(NodeItem.PlanEnd, "dd/MM/yyyy")
                DR("IsExpanded") = NodeItem.IsExpand
                DR("expense") = NodeItem.Expense
                DR("commitment_budget") = NodeItem.CommitmentBudget
                DR("Child") = NodeItem.ChildQty
                DT.Rows.Add(DR)
            Next
            Return DT
        End Get
    End Property

    Private Function BlankNodesData() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("id", GetType(Long))
        DT.Columns.Add("activity_name")
        DT.Columns.Add("parent_id", GetType(Long))
        DT.Columns.Add("actual_start", GetType(Date))
        DT.Columns.Add("actual_end", GetType(Date))
        'DT.Columns.Add("duration", GetType(Integer))
        DT.Columns.Add("commitment_budget", GetType(Decimal))
        DT.Columns.Add("Level", GetType(Integer))
        DT.Columns.Add("IsExpanded", GetType(Boolean))
        DT.Columns.Add("Child", GetType(Integer))
        DT.Columns.Add("expense", GetType(Decimal))
        DT.Columns.Add("childs_budget", GetType(Decimal))
        Return DT
    End Function

    Public Sub GenerateActivityList(ProjectID As Long, ActivityMode As String)
        Dim ret As New DataTable
        Try
            lblProjectID.Text = ProjectID
            lblActivityMode.Text = ActivityMode

            'Bind_Col_Master_Year()

            Dim dt As New DataTable
            dt = BL.GetProjectActivityList(ProjectID)
            If dt.Rows.Count > 0 Then


                ret = BlankNodesData()

                'เริ่มต้นที่ ParentID=0
                dt.DefaultView.RowFilter = "parent_id=0"

                Dim pDt As New DataTable
                pDt = dt.DefaultView.ToTable.Copy()
                If pDt.Rows.Count > 0 Then
                    ret = GenerateSubChildNode(ret, pDt, dt, 0, 0)
                End If
                pDt.Dispose()

                If ret.Rows.Count > 0 Then
                    rpt.DataSource = ret
                    rpt.DataBind()
                End If

            Else
                ret = BlankNodesData()
                rpt.DataSource = ret
                rpt.DataBind()
            End If
            dt.Dispose()
        Catch ex As Exception

        End Try
    End Sub

    Public Sub GenerateActivityListLone(ProjectID As Long, ActivityMode As String)
        Dim ret As New DataTable
        Try
            lblProjectID.Text = ProjectID
            lblActivityMode.Text = ActivityMode

            Dim dt As New DataTable
            dt = BL.GetProjectActivityListLone(ProjectID)
            If dt.Rows.Count > 0 Then
                ret = BlankNodesData()

                'เริ่มต้นที่ ParentID=0
                dt.DefaultView.RowFilter = "parent_id=0"

                Dim pDt As New DataTable
                pDt = dt.DefaultView.ToTable.Copy()
                If pDt.Rows.Count > 0 Then
                    ret = GenerateSubChildNode(ret, pDt, dt, 0, 0)
                End If
                pDt.Dispose()

                If ret.Rows.Count > 0 Then
                    rpt.DataSource = ret
                    rpt.DataBind()
                End If

            Else
                ret = BlankNodesData()
                rpt.DataSource = ret
                rpt.DataBind()
            End If
            dt.Dispose()
        Catch ex As Exception

        End Try
    End Sub

    Private Function GenerateSubChildNode(ret As DataTable, pDt As DataTable, dt As DataTable, ParentID As Long, NodeLevel As Integer) As DataTable

        For i As Integer = 0 To pDt.Rows.Count - 1
            Try
                Dim dr As DataRow = pDt.Rows(i)
                Dim _ActivityID As String = dr("id")
                Dim _ActivityName As String = dr("activity_name")
                Dim _parent_id As String = dr("parent_id")
                Dim _PlanStart As Date = Convert.ToDateTime(dr("actual_start"))
                Dim _PlanEnd As Date = Convert.ToDateTime(dr("actual_end"))
                'Dim _Duration As String = dr("duration")
                Dim _Expense As String = dr("expense")
                Dim _CommitmentBudget As String = dr("commitment_budget")
                Dim _childs_budget As String = dr("childs_budget")

                Dim rdr As DataRow = ret.NewRow
                rdr("id") = _ActivityID
                rdr("activity_name") = _ActivityName
                rdr("parent_id") = _parent_id
                rdr("actual_start") = _PlanStart
                rdr("actual_end") = _PlanEnd
                'rdr("duration") = _Duration
                rdr("expense") = _Expense
                rdr("commitment_budget") = _CommitmentBudget
                rdr("Level") = NodeLevel
                rdr("IsExpanded") = False
                rdr("childs_budget") = _childs_budget



                dt.DefaultView.RowFilter = "parent_id=" & _ActivityID
                Dim cDt As New DataTable
                cDt = dt.DefaultView.ToTable.Copy()
                rdr("Child") = cDt.Rows.Count

                ret.Rows.Add(rdr)

                If cDt.Rows.Count > 0 Then
                    ret = GenerateSubChildNode(ret, cDt, dt, _parent_id, NodeLevel + 1)
                End If
                cDt.Dispose()
            Catch ex As Exception

            End Try

        Next

        Return ret
    End Function

    Private Sub rpt_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rpt.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                Dim NodeItem As UCActivityTreeNode = e.Item.FindControl("UCActivityTreeNode1")
                NodeItem.ActivityID = e.Item.DataItem("id")
                NodeItem.ActivityName = e.Item.DataItem("activity_name")
                NodeItem.ParentID = e.Item.DataItem("parent_id")
                NodeItem.NodeLevel = e.Item.DataItem("Level")
                NodeItem.IsExpand = e.Item.DataItem("IsExpanded")
                'NodeItem.Duration = e.Item.DataItem("duration")
                NodeItem.ChildQty = e.Item.DataItem("Child")
                NodeItem.Expense = e.Item.DataItem("expense")
                NodeItem.CommitmentBudget = e.Item.DataItem("commitment_budget")
                'NodeItem.Project_ID = ProjectID


                Dim aidname As String = ""
                Dim dtComponent As DataTable = BL.GetActivityComponent(e.Item.DataItem("id"))
                If dtComponent.Rows.Count > 0 Then
                    aidname = BL.GetActivityComponentName(dtComponent.Rows(0)("component_id").ToString)
                End If
                Dim dtInkind As DataTable = BL.GetProjectActivityInkind(e.Item.DataItem("id"))
                If dtInkind.Rows.Count > 0 Then
                    aidname = BL.GetActivityInkindName(dtInkind.Rows(0)("inkind_id").ToString)
                End If
                NodeItem.aid = aidname

                If Convert.IsDBNull(e.Item.DataItem("actual_start")) = False Then
                    NodeItem.PlanStart = Convert.ToDateTime(e.Item.DataItem("actual_start")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
                    'NodeItem.Year_MIN = Convert.ToDateTime(e.Item.DataItem("actual_start")).ToString("yyyy", New System.Globalization.CultureInfo("en-US"))
                End If
                If Convert.IsDBNull(e.Item.DataItem("actual_end")) = False Then
                    NodeItem.PlanEnd = Convert.ToDateTime(e.Item.DataItem("actual_end")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
                    'NodeItem.Year_MAX = Convert.ToDateTime(e.Item.DataItem("actual_end")).ToString("yyyy", New System.Globalization.CultureInfo("en-US"))
                End If

                '------------
                ' NodeItem.Bind_Col_Master_Year()
                Dim _expense As Decimal = DirectCast(e.Item.DataItem("expense"), Decimal)

                Dim _commitment_budget As Decimal = DirectCast(e.Item.DataItem("commitment_budget"), Decimal)
                Dim _childs_budget As Decimal = DirectCast(e.Item.DataItem("childs_budget"), Decimal)

                If _commitment_budget = _childs_budget Then
                    NodeItem.SetBudgetColor(Drawing.Color.Green)
                Else
                    If e.Item.DataItem("Child").ToString = "0" Then
                        NodeItem.SetBudgetColor(Drawing.Color.Green)
                    Else
                        NodeItem.SetBudgetColor(Drawing.Color.Red)
                        NodeItem.SetToolTipCommitmentBudget("ผลรวมไม่เท่ากัน")
                    End If
                End If


                If NodeItem.NodeLevel > 0 Then
                    e.Item.Visible = False
                End If

                If lblActivityMode.Text = "view" Then
                    NodeItem.SetViewMode = True
                End If
        End Select
    End Sub

#Region "Node Event"
    Protected Sub ImageModeClick(sender As UCActivityTreeNode, IsExpand As Boolean)
        SetExpandCollapt(sender.ActivityID, IsExpand)
    End Sub

    Private Sub SetExpandCollapt(ParentID As Long, IsExpand As Boolean)
        For Each grv As RepeaterItem In rpt.Items
            Dim uc As UCActivityTreeNode = grv.FindControl("UCActivityTreeNode1")
            If uc.ParentID = ParentID Then
                If IsExpand = True Then
                    grv.Visible = True
                Else
                    grv.Visible = False

                    If uc.ChildQty > 0 Then
                        uc.IsExpand = False
                        uc.ImageMode.ImageUrl = uc.ImageCollapsed
                        SetExpandCollapt(uc.ActivityID, IsExpand)
                    End If
                End If
            End If
        Next
    End Sub

    Protected Sub AddSubActivity(sender As UCActivityTreeNode)
        RaiseEvent AddChildNode(sender.ActivityID)
    End Sub

    Protected Sub EditActivity(sender As UCActivityTreeNode)
        'UCActivityTabProject1.EditActivity(sender.ActivityID)
        RaiseEvent EditChildNode(sender.ActivityID)
    End Sub

    Protected Sub ViewActivity(sender As UCActivityTreeNode)
        'UCActivityTabProject1.viewActivity(sender.ActivityID)
        RaiseEvent ViewChildNode(sender.ActivityID)
    End Sub

    Protected Sub DeleteActivity(ActivityID As Long)
        'Dim ret As New ProcessReturnInfo
        'ret =
        BL.DeleteActivity(ActivityID)
        Dim Type As DataTable = BL.FindType(lblProjectID.Text)
        Dim ty As String = Convert.ToDecimal(Type.Rows(0)("project_type")).ToString()
        'If ret.IsSuccess Then
        alertmsg("ลบข้อมูลเรียบร้อยแล้ว")
        If ty = "2" Or ty = "3" Then
            GenerateActivityListLone(lblProjectID.Text, lblActivityMode.Text)
        ElseIf ty = "0" Or ty = "1" Then
            GenerateActivityList(lblProjectID.Text, lblActivityMode.Text)
        End If
        'Else
        '        alertmsg("ไม่สามารถบันทึกข้อมูลได้ " & ret.ErrorMessage)
        'End If
    End Sub

    Private Sub alertmsg(msg As String)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & msg & "');", True)
    End Sub

    Private Sub UCActivityTreeList_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub


#End Region





    'Public Sub SetActivity(project_id As String, activity_type As String)
    '    Try
    '        Dim str As New StringBuilder
    '        str = BL.SetActivity(ProjectID, "", "view", "year", "")
    '        ltActivity.Text = str.ToString()
    '    Catch ex As Exception

    '    End Try
    'End Sub


    '========Col==================

    '------Get Min Max Rank Project Activity------
    Public Sub Get_Rank_Activity()

        Dim SQL As String = ""
        SQL &= " Select  DISTINCT * FROM( "
        SQL &= " Select  year(  [actual_start]) Rank_YEAR "
        SQL &= " From [TB_Activity] "
        SQL &= " UNION ALL "
        SQL &= "   Select  Year([actual_end])     Rank_YEAR  "
        SQL &= " From [TB_Activity] "
        SQL &= " ) As TB "
        SQL &= "  ORDER BY Rank_YEAR "

        Dim DT_Year As New DataTable
        Dim DR As DataRow
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        DA.Fill(DT)


        If (DT.Rows.Count > 0) Then
            If (DT.Rows.Count = 1) Then
                DR = DT_Year.NewRow
                DR("Year") = DT.Rows(0).Item("Year")
                DT_Year.Rows.Add(DR)
            Else
                For i As Integer = DT.Rows(0).Item("Year") To DT.Rows(DT.Rows.Count - 1).Item("Year")
                    DR = DT_Year.NewRow
                    DR("Year") = i
                    DT_Year.Rows.Add(DR)
                Next
            End If
        End If

    End Sub

    Function Get_Col_Year() As DataTable

        Dim SQL As String = ""
        SQL &= " Select  DISTINCT * FROM( " & vbLf
        SQL &= " Select  year(  [actual_start]) Year " & vbLf
        SQL &= " From [TB_Activity] where project_id=" & ProjectID & " " & vbLf
        SQL &= " UNION ALL " & vbLf
        SQL &= "   Select  Year([actual_end])     Year  " & vbLf
        SQL &= " From [TB_Activity] where project_id=" & ProjectID & " " & vbLf
        SQL &= " ) As TB "
        SQL &= "  ORDER BY Year "

        Dim DT_Year As New DataTable
        Dim DR As DataRow
        DT_Year.Columns.Add("Year")

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        DA.Fill(DT)

        If (DT.Rows.Count > 0) Then
            If (DT.Rows.Count = 1) Then
                DR = DT_Year.NewRow
                DR("Year") = DT.Rows(0).Item("Year")
                DT_Year.Rows.Add(DR)
            Else
                For i As Integer = DT.Rows(0).Item("Year") To DT.Rows(DT.Rows.Count - 1).Item("Year")
                    DR = DT_Year.NewRow
                    DR("Year") = i
                    DT_Year.Rows.Add(DR)
                Next
            End If
        End If
        Return DT_Year
    End Function

End Class
