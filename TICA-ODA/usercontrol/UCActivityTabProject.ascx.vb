﻿Imports System.Data
Imports LinqDB.TABLE
Partial Class usercontrol_UCActivityTabProject
    Inherits System.Web.UI.UserControl

    Public Event SaveActivityComplete()
    Public Event CancelAct()

    Dim BL As New ODAENG

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Public WriteOnly Property ProjectID As Long
        Set(value As Long)
            lblProjectID.Text = value
        End Set
    End Property
    Public WriteOnly Property ActivityMode As String
        Set(value As String)
            lblActivityMode.Text = value
        End Set
    End Property


    Public Property ProjectType As Long
        Get
            Try
                Return ViewState("ProjectType")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ProjectType") = value
        End Set
    End Property

    Protected ReadOnly Property AuthorizeDT As DataTable
        Get
            Try
                Return CType(Session("Authorize"), DataTable)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
    End Property

    Private Sub usercontrol_UCActivityTabProject_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            ProjectType = CInt(Request.QueryString("type"))
            BL.Bind_DDL_Year(ddlBudgetYear)

            'BL.SetTextDblKeypress(txtActCommitmentBudget)
            'BL.SetTextDblKeypress(txtActDisbursement)
            'BL.SetTextDblKeypress(txtActAdministrative)
            BL.SetTextDblKeypress(txtAmount)

            Authorize()
        End If


        If "edit" = lblActivityMode.Text Then
            If ddlSelectReport.SelectedValue = 1 Then
                ddlComponent.Visible = True
                ddlInkind.Visible = False
                chkActive.Visible = True
                Label1.Text = "<p class=""pull-right"">ประกาศรับสมัครผู้รับทุน :</p>"
                traid.Visible = True
            ElseIf ddlSelectReport.SelectedValue = 2 Then
                ddlComponent.Visible = False
                ddlInkind.Visible = True
                chkActive.Visible = False
                chkActive.Checked = False
                Label1.Text = ""
                traid.Visible = True
            ElseIf ddlSelectReport.SelectedValue = 0 Then
                ddlComponent.Visible = False
                ddlInkind.Visible = False
                chkActive.Visible = False
                Label1.Text = ""
                traid.Visible = False
            End If
        End If

        txtBenificiary.Visible = False
        txtRecipientCountry.Visible = False
        ctlSelectRecipientCountry.Visible = False
        txtRecipient.Visible = False
        ctlSelectRecipient.Visible = False

    End Sub

    Public Sub SetControlToViewMode(IsView As Boolean)

        '#TabActActivity
        txtActName.Enabled = Not IsView
        txtActDescription.Enabled = Not IsView
        ddlSector.Enabled = Not IsView
        ddlSubSector.Enabled = Not IsView
        chkActive.Enabled = Not IsView

        '#TabActExpense        
        'UCProjectComponent.SetToViewMode(IsView)
        'UCInkind.SetToViewMode(IsView)

        txtPlanActStart.Enabled = Not IsView
        txtplanActEnd.Enabled = Not IsView
        txtActualActStart.Enabled = Not IsView
        txtActualActEnd.Enabled = Not IsView
        'txtActCommitmentBudget.Enabled = Not IsView
        'txtActDisbursement.Enabled = Not IsView
        'txtActAdministrative.Enabled = Not IsView

        '#TabActReceipent
        'ctlSelectRecipient.Enabled = Not IsView
        'ctlSelectRecipientCountry.Enabled = Not IsView
        txtBenificiary.Enabled = Not IsView

        '#TabExpense
        ddlBudgetYear.Enabled = Not IsView
        ddlBudgetGroup.Enabled = Not IsView
        ddlBudgetSub.Enabled = Not IsView
        txtAmount.Enabled = Not IsView

        btnSaveAct.Visible = Not IsView
    End Sub

    Public Sub AddActivity(ParentID As Long)
        Session("status") = "add"
        ClearActivity()
        lblParentActivityID.Text = ParentID
        pnlAdd.Visible = True
        ''CurrentTabAct = Tab.ActActDetail
    End Sub

    Public Sub EditActivity(ActivityID As Long)
        ClearActivity()
        pnlAdd.Visible = True
        ''CurrentTabAct = Tab.ActActDetail
        SetAcitivityInfoByID(ActivityID)
    End Sub

    Public Sub ViewActivity(ActivityID As Long)
        ClearActivity()
        pnlAdd.Visible = True
        ''CurrentTabAct = Tab.ActActDetail

        SetAcitivityInfoByID(ActivityID)

        'UCProjectComponent.SetToViewMode(True)
        'UCInkind.SetToViewMode(True)

        ctlSelectRecipient.Visible = False
        txtRecipient.Visible = True
        txtRecipient.Enabled = False

        ctlSelectRecipientCountry.Visible = False
        txtRecipientCountry.Visible = True
        txtRecipientCountry.Enabled = False
    End Sub

    Public Sub SetAcitivityInfoByID(activity_id As String)


        '#TabActActDetail
        Dim dtActDetail As New DataTable
        dtActDetail = BL.GetProjectActivityInfoByID(activity_id)

        Dim dtActProjectCountryRe As New DataTable
        dtActProjectCountryRe = UCProjectCountryRe.SetDataRpt(activity_id)

        Dim dtActProjectCountry As New DataTable
        dtActProjectCountry = UCProjectCountry.SetDataRpt(activity_id)

        Dim dtAcProjectRecipience As New DataTable
        dtAcProjectRecipience = UCprojectRecipience.SetDataRpt(activity_id)

        Dim dtActAdministrative_Cost As New DataTable
        dtActAdministrative_Cost = UCProjectAdministrativeCost.SetDataRpt_Administrative_Cost(activity_id)

        UCProjectAdministrativeCost.SetToValueActivityId(activity_id)


        Dim DT_Administrative_Cost As New DataTable
        DT_Administrative_Cost = UCProjectAdministrativeCost.ComponentDT
        Dim DT_ProjectCountryRe As New DataTable
        DT_ProjectCountryRe = UCProjectCountryRe.ComponentDT
        Dim DT_UCProjectCountry As New DataTable
        DT_UCProjectCountry = UCProjectCountry.CountryDT
        Dim DT_ProjectRecipient As New DataTable
        DT_ProjectRecipient = UCprojectRecipience.RecipienceDT

        Dim _activity_name As String = ""
        Dim _description As String = ""
        Dim _sector_id As String = ""
        Dim _sub_sector_id As String = ""
        Dim _parent_id As String = ""
        Dim _plan_start As String = ""
        Dim _plan_end As String = ""
        Dim _actual_start As String = ""
        Dim _actual_end As String = ""
        Dim _commitment_budget As String = ""
        Dim _disbursement As String = ""
        Dim _administrative As String = ""
        Dim _beneficiary As String = ""
        Dim _notify As String = ""
        Dim _Recipient_type As String = ""

        If dtActDetail.Rows.Count > 0 Then
            Dim _dr As DataRow = dtActDetail.Rows(0)
            _activity_name = _dr("activity_name").ToString()
            _description = _dr("description").ToString()
            _sector_id = _dr("sector_id").ToString()
            _sub_sector_id = _dr("sub_sector_id").ToString()
            _parent_id = _dr("parent_id").ToString()
            _notify = _dr("notify").ToString
            If Session("mode") = "palnex" Then
                If _dr("actual_start").ToString = "" Or _dr("actual_end").ToString = "" Then
                    _plan_start = ""
                    _plan_end = ""
                Else
                    _plan_start = Convert.ToDateTime(_dr("actual_start")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
                    _plan_end = Convert.ToDateTime(_dr("actual_end")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
                End If
                If ddlSelectReport.SelectedValue = 1 Then
                    ddlComponent.Visible = True
                    ddlInkind.Visible = False
                    chkActive.Visible = True
                    Label1.Text = "<p class=""pull-right"">ประกาศรับสมัครผู้รับทุน :</p>"
                    traid.Visible = True
                ElseIf ddlSelectReport.SelectedValue = 2 Then
                    ddlComponent.Visible = False
                    ddlInkind.Visible = True
                    chkActive.Visible = False
                    chkActive.Checked = False
                    Label1.Text = ""
                    traid.Visible = True
                ElseIf ddlSelectReport.SelectedValue = 0 Then
                    ddlComponent.Visible = False
                    ddlInkind.Visible = False
                    chkActive.Visible = False
                    Label1.Text = ""
                    traid.Visible = False
                End If
            Else

            End If
            If _dr("plan_start").ToString() <> "" Then
                _plan_start = Convert.ToDateTime(_dr("plan_start")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
            End If

            If _dr("plan_end").ToString() <> "" Then
                _plan_end = Convert.ToDateTime(_dr("plan_end")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
            End If

            If _dr("actual_start").ToString() <> "" Then
                _actual_start = Convert.ToDateTime(_dr("actual_start")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
            End If

            If _dr("actual_end").ToString() <> "" Then
                    _actual_end = Convert.ToDateTime(_dr("actual_end")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
                End If
                If _dr("commitment_budget").ToString() <> "" Then
                    _commitment_budget = Convert.ToDecimal(_dr("commitment_budget")).ToString("#,##0.00")
                End If
                If _dr("disbursement").ToString() <> "" Then
                    _disbursement = Convert.ToDecimal(_dr("disbursement")).ToString("#,##0.00")
                End If
                If _dr("administrative").ToString() <> "" Then
                    _administrative = Convert.ToDecimal(_dr("administrative")).ToString("#,##0.00")
                End If
                _beneficiary = _dr("beneficiary").ToString()
                _Recipient_type = _dr("Recipient_Type").ToString()

            End If

            lblActivityID.Text = activity_id
        txtActName.Text = _activity_name
        txtActDescription.Text = _description

        If _Recipient_type = "G" Then
            RadioSelectRe.SelectedValue = 1
            UCProjectCountry.Visible = False
            UCProjectCountryRe.Visible = True
            UCprojectRecipience.Visible = False
        ElseIf _Recipient_type = "R" Then
            RadioSelectRe.SelectedValue = 2
            UCProjectCountry.Visible = False
            UCProjectCountryRe.Visible = False
            UCprojectRecipience.Visible = True
        ElseIf _Recipient_type = "C" Then
            RadioSelectRe.SelectedValue = 3
            UCProjectCountry.Visible = True
            UCProjectCountryRe.Visible = False
            UCprojectRecipience.Visible = False
        End If

        Try
            If _sector_id <> "" Then
                ddlSector.SelectedValue = _sector_id
                ddlSector_SelectedIndexChanged(Nothing, Nothing)
                If _sub_sector_id <> "" Then ddlSubSector.SelectedValue = _sub_sector_id

            End If


        Catch ex As Exception
        End Try

        lblParentActivityID.Text = _parent_id
        chkActive.Checked = IIf(_notify = "Y", True, False)

        Dim dtActemplate As DataTable = BL.GetActivityTemplate(activity_id)
        If dtActemplate.Rows.Count > 0 Then
            '    UCActivityBudget.BudgetDT = dtActivityBudget
            Dim _template_id As String = dtActemplate.Rows(0)("template_id").ToString
            lbltemplate.SelectedValue = _template_id

        End If

        '#TabActExpense
        Dim dtActivityBudget As DataTable = BL.GetActivityBudget(activity_id)
        If dtActivityBudget.Rows.Count > 0 Then
            '    UCActivityBudget.BudgetDT = dtActivityBudget
            Dim _budget_year As String = dtActivityBudget.Rows(0)("budget_year").ToString
            Dim _budget_group As String = dtActivityBudget.Rows(0)("budget_group_id").ToString
            Dim _budget_sub As String = dtActivityBudget.Rows(0)("budget_sub_id").ToString
            Dim _amount As String = Convert.ToDecimal(dtActivityBudget.Rows(0)("amount")).ToString("#,##0.00")

            ddlBudgetYear.SelectedValue = _budget_year
            ddlBudgetGroup.SelectedValue = _budget_group
            ddlBudgetGroup_SelectedIndexChanged(Nothing, Nothing)
            ddlBudgetSub.SelectedValue = _budget_sub
            txtAmount.Text = _amount
        End If



        Dim dtComponent As DataTable = BL.GetActivityComponent(activity_id)
        If dtComponent.Rows.Count > 0 Then
            '    UCActivityBudget.BudgetDT = dtActivityBudget
            Dim _component_id As String = dtComponent.Rows(0)("component_id").ToString
            ddlComponent.Visible = True
            ddlComponent.SelectedValue = _component_id
            ddlInkind.Visible = False
            ddlSelectReport.SelectedValue = 1
            chkActive.Visible = True
            Label1.Text = "<p class=""pull-right"">ประกาศรับสมัครผู้รับทุน :</p>"
            traid.Visible = True
        End If



        Dim dtInkind As DataTable = BL.GetProjectActivityInkind(activity_id)
        If dtInkind.Rows.Count > 0 Then
            '    UCActivityBudget.BudgetDT = dtActivityBudget
            Dim _inkind_id As String = dtInkind.Rows(0)("inkind_id").ToString
            ddlInkind.SelectedValue = _inkind_id
            ddlInkind.Visible = True
            ddlSelectReport.SelectedValue = 2
            ddlComponent.Visible = False
            Label1.Text = ""
            traid.Visible = True
        End If

        txtPlanActStart.Text = _plan_start
        txtplanActEnd.Text = _plan_end
        txtActualActStart.Text = _actual_start
        txtActualActEnd.Text = _actual_end

        '#TabActReceipent
        Dim _recipientname As String = ""
        Dim dtRecipientPerson As New DataTable
        dtRecipientPerson = BL.GetActivityRecipincePerson(activity_id)
        For Each Item As ListItem In ctlSelectRecipient.Items
            For i As Integer = 0 To dtRecipientPerson.Rows.Count - 1
                If Item.Value = dtRecipientPerson.Rows(i)("node_id").ToString Then
                    Item.Selected = True
                    _recipientname &= Item.Text & ","
                    Exit For
                End If
            Next
        Next

        Dim _recipientcountryname As String = ""
        Dim dtRecipientCountry As New DataTable
        dtRecipientCountry = BL.GetActivityRecipinceCountry(activity_id)
        For Each Item As ListItem In ctlSelectRecipientCountry.Items
            For i As Integer = 0 To dtRecipientCountry.Rows.Count - 1
                If Item.Value = dtRecipientCountry.Rows(i)("node_id").ToString Then
                    Item.Selected = True
                    _recipientcountryname &= Item.Text & ","
                    Exit For
                End If
            Next
        Next

        txtRecipient.Text = ""
        If _recipientname.Length > 0 Then
            txtRecipient.Text = _recipientname.Substring(0, _recipientname.Length - 1)
        End If

        txtRecipientCountry.Text = ""
        If _recipientcountryname.Length > 0 Then
            txtRecipientCountry.Text = _recipientcountryname.Substring(0, _recipientcountryname.Length - 1)
        End If

        txtBenificiary.Text = _beneficiary
        BindList(activity_id)
    End Sub

    Private Sub ClearActivity()
        '##tabActivity
        '#TabActActDetail
        lblActivityID.Text = "0"
        lblParentActivityID.Text = "0"
        txtActName.Text = ""
        txtActDescription.Text = ""
        BL.Bind_DDL_Perposecat(ddlSector)
        ddlSector_SelectedIndexChanged(Nothing, Nothing)

        chkActive.Checked = False
        ddlSelectReport.SelectedValue = 0
        ddlInkind.SelectedValue = ""
        ddlComponent.SelectedValue = ""
        chkActive.Visible = False
        Label1.Text = ""


        BL.Bind_DDL_ExpenseTemplate(lbltemplate)

        BL.Bind_DDL_Component(ddlComponent)
        BL.Bind_DDL_Inkind(ddlInkind)
        BL.Bind_DDL_Year(ddlBudgetYear)
        BL.Bind_DDL_BudgetGroup(ddlBudgetGroup)
        ddlBudgetGroup_SelectedIndexChanged(Nothing, Nothing)
        txtAmount.Text = ""

        Dim dtCountryRe As New DataTable
        With dtCountryRe
            .Columns.Add("id")
            .Columns.Add("country_node_id")
            .Columns.Add("amout_person")
        End With
        UCProjectCountryRe.ComponentDT = dtCountryRe

        Dim dtCountry As New DataTable
        With dtCountry
            .Columns.Add("id")
            .Columns.Add("country_node_id")
        End With
        UCProjectCountry.CountryDT = dtCountry

        Dim dtRecipient As New DataTable
        With dtRecipient
            .Columns.Add("id")
            .Columns.Add("country_id")
            .Columns.Add("recipient_id")
        End With
        UCprojectRecipience.RecipienceDT = dtRecipient
        UCprojectRecipience.Bindata_toRptCountRe()


        Dim AdministrativeCost As New DataTable
        With AdministrativeCost
            .Columns.Add("id")
            .Columns.Add("date_payment")
            .Columns.Add("Description")
            .Columns.Add("amount")
        End With
        UCProjectAdministrativeCost.ComponentDT = AdministrativeCost

        BindList(0)

        txtPlanActStart.Text = ""
        txtplanActEnd.Text = ""
        txtActualActStart.Text = ""
        txtActualActEnd.Text = ""

        '#TabActReceipent
        ctlSelectRecipient.Visible = True
        txtRecipient.Visible = False
        ctlSelectRecipientCountry.Visible = True
        txtRecipientCountry.Visible = False
        SetRecipient()
        SetRecipientCountry()
        txtBenificiary.Text = ""
    End Sub

    Private Sub SetRecipient()
        Dim dt As New DataTable
        dt = BL.GetPersonList(Constants.Person_Type.Recipient)

        ctlSelectRecipient.DataValueField = "node_id"
        ctlSelectRecipient.DataTextField = "name_th"

        ctlSelectRecipient.DataSource = dt
        ctlSelectRecipient.DataBind()
    End Sub

    Private Sub SetRecipientCountry()
        Dim dt As New DataTable
        dt = BL.GetOrganizeUnitByNodeID(0, Constants.OU.Country)

        ctlSelectRecipientCountry.DataValueField = "node_id"
        ctlSelectRecipientCountry.DataTextField = "name_th"

        ctlSelectRecipientCountry.DataSource = dt
        ctlSelectRecipientCountry.DataBind()

    End Sub

#Region "Tab Activity"
    Protected Enum Tab
        ActActDetail = 1
        ActExpense = 2
        ActReceipent = 3
        ActPeriod = 4
        ActDisversment = 5
    End Enum
    Protected Property CurrentTabAct As Tab
        Get
            Select Case True
                Case TabActActDetail.Visible
                    Return Tab.ActActDetail
                Case TabActExpense.Visible
                    Return Tab.ActExpense
                Case TabActRecipent.Visible
                    Return Tab.ActReceipent
                Case Else
                    'Return Tab.ActActDetail
            End Select
        End Get
        Set(value As Tab)
            TabActActDetail.Visible = False
            TabActPeriod.Visible = False
            TabActRecipent.Visible = False
            TabActExpense.Visible = False
            TabActDisversement.Visible = False

            liTabActActDetail.Attributes("class") = ""
            liTabActPeriod.Attributes("class") = ""
            liTabActReceipent.Attributes("class") = ""
            liTabActExpense.Attributes("class") = ""
            liTabActDisversment.Attributes("class") = ""

            Select Case value
                Case Tab.ActActDetail
                    TabActActDetail.Visible = True
                    liTabActActDetail.Attributes("class") = "active"
                Case Tab.ActReceipent
                    TabActRecipent.Visible = True
                    liTabActReceipent.Attributes("class") = "active"
                Case Tab.ActPeriod
                    TabActPeriod.Visible = True
                    liTabActPeriod.Attributes("class") = "active"
                Case Tab.ActExpense
                    TabActExpense.Visible = True
                    liTabActExpense.Attributes("class") = "active"
                Case Tab.ActDisversment
                    TabActDisversement.Visible = True
                    liTabActDisversment.Attributes("class") = "active"
                Case Else
            End Select
        End Set

    End Property

    Private Sub ChangeTabAct(sender As Object, e As System.EventArgs) Handles btnTabActActDetail.Click, btnTabActExpense.Click, btnTabActReceipent.Click, btnTabActPeriod.Click, btnTabActDisversment.Click
        Select Case True
            Case Equals(sender, btnTabActActDetail)
                CurrentTabAct = Tab.ActActDetail
            Case Equals(sender, btnTabActReceipent)
                CurrentTabAct = Tab.ActReceipent
            Case Equals(sender, btnTabActPeriod)
                CurrentTabAct = Tab.ActPeriod
            Case Equals(sender, btnTabActExpense)
                CurrentTabAct = Tab.ActExpense
            Case Equals(sender, btnTabActDisversment)
                CurrentTabAct = Tab.ActDisversment
            Case Else
        End Select
    End Sub

#End Region

#Region "Activity Event"
    Private Sub btnCancelAct_Click(sender As Object, e As EventArgs) Handles btnCancelAct.Click
        pnlAdd.Visible = False

        RaiseEvent CancelAct()
    End Sub

    Private Sub btnSaveAct_Click(sender As Object, e As EventArgs) Handles btnSaveAct.Click
        Try
            '## Validate
            If ValidateAct() = False Then
                Exit Sub
            End If

            If Session("status") = "add" Then
                '##tabActivity
                '#TabActActDetail

            End If

            Dim lnqActivity As New TbActivityLinqDB
            With lnqActivity
                .ID = lblActivityID.Text
                .ACTIVITY_NAME = txtActName.Text
                .DESCRIPTION = txtActDescription.Text

                If ddlSector.SelectedValue <> "" Then
                    .SECTOR_ID = ddlSector.SelectedValue
                End If
                If ddlSubSector.SelectedValue <> "" Then
                    .SUB_SECTOR_ID = ddlSubSector.SelectedValue
                End If

                .PARENT_ID = lblParentActivityID.Text
                .PROJECT_ID = lblProjectID.Text
                .NOTIFY = IIf(chkActive.Checked, "Y", "N").ToString
                .ACTIVE_STATUS = "1"
                If RadioSelectRe.SelectedValue = 1 Then
                    .RECIPIENT_TYPE = "G"
                ElseIf RadioSelectRe.SelectedValue = 2 Then
                    .RECIPIENT_TYPE = "R"
                ElseIf RadioSelectRe.SelectedValue = 3 Then
                    .RECIPIENT_TYPE = "C"
                End If
            End With

            '#TabActExpense
            Dim DTBudget As New DataTable
            With DTBudget
                .Columns.Add("budget_year")
                .Columns.Add("budget_group_id")
                .Columns.Add("budget_sub_id")
                .Columns.Add("amount")
            End With
            Dim dr As DataRow
            dr = DTBudget.NewRow
            dr("budget_year") = ddlBudgetYear.SelectedValue
            dr("budget_group_id") = ddlBudgetGroup.SelectedValue
            dr("budget_sub_id") = ddlBudgetSub.SelectedValue
            dr("amount") = txtAmount.Text
            DTBudget.Rows.Add(dr)

            With lnqActivity
                .ACTUAL_START = Converter.StringToDate(txtActualActStart.Text.Trim, "dd/MM/yyyy")
                .ACTUAL_END = Converter.StringToDate(txtActualActEnd.Text.Trim, "dd/MM/yyyy")

                If txtPlanActStart.Text <> "" Then
                    Try
                        .PLAN_START = Converter.StringToDate(txtPlanActStart.Text.Trim, "dd/MM/yyyy")
                    Catch ex As Exception
                        alertmsg("กรุณาตรวจสอบวันที่")
                        Exit Sub
                    End Try

                End If

                If txtplanActEnd.Text <> "" Then
                    Try
                        .PLAN_END = Converter.StringToDate(txtplanActEnd.Text.Trim, "dd/MM/yyyy")
                    Catch ex As Exception
                        alertmsg("กรุณาตรวจสอบวันที่")
                        Exit Sub
                    End Try

                End If
                If txtActualActEnd.Text <> "" Then
                    Try
                        .ACTUAL_END = Converter.StringToDate(txtActualActEnd.Text.Trim, "dd/MM/yyyy")
                    Catch ex As Exception
                        alertmsg("กรุณาตรวจสอบวันที่")
                        Exit Sub
                    End Try

                End If
            End With

            '#TabActReceipent
            Dim DTRecipientPerson As DataTable = GetRecipientPerson()
            Dim DTRecipentCountry As DataTable = GetRecipientCountry()

            With lnqActivity
                .BENEFICIARY = txtBenificiary.Text
            End With

            Dim DT_Administrative_Cost As New DataTable
            DT_Administrative_Cost = UCProjectAdministrativeCost.ComponentDT
            Dim DT_ProjectCountryRe As New DataTable
            DT_ProjectCountryRe = UCProjectCountryRe.ComponentDT
            Dim DT_UCProjectCountry As New DataTable
            DT_UCProjectCountry = UCProjectCountry.CountryDT
            Dim DT_ProjectRecipient As New DataTable
            DT_ProjectRecipient = UCprojectRecipience.RecipienceDT


            If RadioSelectRe.SelectedValue = 1 Then
                If DT_ProjectCountryRe.Rows.Count = 0 Then
                    alertmsg("กรุณาระบุประเทศที่รับทุน")
                    Exit Sub
                End If
            ElseIf RadioSelectRe.SelectedValue = 2 Then
                If DT_ProjectRecipient.Rows.Count = 0 Then
                    alertmsg("กรุณาระบุผู้รับทุน")
                    Exit Sub
                End If
            ElseIf RadioSelectRe.SelectedValue = 3 Then
                If DT_UCProjectCountry.Rows.Count = 0 Then
                    alertmsg("กรุณาระบุประเทศที่รับทุน")
                    Exit Sub
                End If

            End If

            If ddlSelectReport.SelectedValue = 0 Then
                alertmsg("กรุณาระบุประเภทความช่วยเหลือ")
                Exit Sub
            End If

            '#ถ้าเป็นComponent
            If ddlSelectReport.SelectedValue = 1 Then
                '#SetComponent
                If ddlComponent.SelectedValue = "" Then
                    alertmsg("กรุณาระบุประเภทความช่วยเหลือย่อย")
                    Exit Sub
                End If
                Dim DTComponent As New DataTable
                With DTComponent
                    .Columns.Add("component_id")
                End With
                Dim drcom As DataRow
                drcom = DTComponent.NewRow
                drcom("component_id") = ddlComponent.SelectedValue
                DTComponent.Rows.Add(drcom)

                Dim ret As New ProcessReturnInfo
                ret = BL.SaveProjectActivity(lnqActivity, DTBudget, DTComponent, Nothing, DTRecipientPerson, DTRecipentCountry, Nothing, Nothing, UserName, DT_ProjectCountryRe, Nothing, DT_Administrative_Cost, DT_ProjectRecipient, DT_UCProjectCountry, Nothing)
                If ret.IsSuccess Then

                    lblActivityID.Text = "0"
                    lblParentActivityID.Text = "0"
                    alertmsg("บันทึกข้อมูลเรียบร้อยแล้ว")

                    pnlAdd.Visible = False
                    RaiseEvent SaveActivityComplete()
                Else
                    alertmsg("ไม่สามารถบันทึกข้อมูลได้ " & ret.ErrorMessage)
                End If

                '#ถ้าเป็นInkind
            ElseIf ddlSelectReport.SelectedValue = 2 Then
                '#SetInkind
                If ddlInkind.SelectedValue = "" Then
                    alertmsg("กรุณาระบุประเภทความช่วยเหลือย่อย")
                    Exit Sub
                End If
                Dim DTInkind As New DataTable
                With DTInkind
                    .Columns.Add("inkind_id")
                End With
                Dim drik As DataRow
                drik = DTInkind.NewRow
                drik("inkind_id") = ddlInkind.SelectedValue
                DTInkind.Rows.Add(drik)

                Dim ret As New ProcessReturnInfo
                ret = BL.SaveProjectActivity(lnqActivity, DTBudget, Nothing, DTInkind, DTRecipientPerson, DTRecipentCountry, Nothing, Nothing, UserName, DT_ProjectCountryRe, Nothing, DT_Administrative_Cost, DT_ProjectRecipient, DT_UCProjectCountry, Nothing)
                If ret.IsSuccess Then

                    lblActivityID.Text = "0"
                    lblParentActivityID.Text = "0"
                    alertmsg("บันทึกข้อมูลเรียบร้อยแล้ว")

                    pnlAdd.Visible = False
                    RaiseEvent SaveActivityComplete()
                Else
                    alertmsg("ไม่สามารถบันทึกข้อมูลได้ " & ret.ErrorMessage)
                End If
            End If
        Catch ex As Exception
            alertmsg("ไม่สามารถบันทึกข้อมูลได้ ")
        End Try

    End Sub

    Private Function ValidateAct() As Boolean
        Dim ret As Boolean = True
        If txtActName.Text.Trim() = "" Then
            alertmsg("กรุณาระบุหัวข้อกิจกรรม")
            ret = False
        End If

        If ddlSector.SelectedValue = "" Then
            alertmsg("กรุณาระบุ Sector(สาขา)")
            ret = False
        End If

        If ddlSubSector.SelectedValue = "" Then
            alertmsg("กรุณาระบุ Sub Sector(สาขาย่อย)")
            ret = False
        End If

        If txtActualActStart.Text = "" Then
            alertmsg("กรุณาระบุวันเริ่มต้นโครงการ")
            ret = False
        End If

        If txtActualActEnd.Text = "" Then
            alertmsg("กรุณาระบุวันสิ้นสุดโครงการ")
            ret = False
        End If

        Try
            Converter.StringToDate(txtActualActStart.Text.Trim, "dd/MM/yyyy")
            Converter.StringToDate(txtActualActEnd.Text.Trim, "dd/MM/yyyy")
        Catch ex As Exception
            alertmsg("กรุณาตรวจสอบวันที่")
            ret = False
        End Try

        If Converter.StringToDate(txtActualActStart.Text.Trim, "dd/MM/yyyy") > Converter.StringToDate(txtActualActEnd.Text.Trim, "dd/MM/yyyy") Then
            alertmsg("แผนของวันเริ่มต้นต้องน้อยกว่าวันสิ้นสุดโครงการ")
            ret = False
        End If

        If ddlBudgetSub.SelectedValue = "" Then
            alertmsg("กรุณาระบุงบประมาณ")
            ret = False
        End If

        If txtAmount.Text = "" Then
            alertmsg("กรุณาระบุจำนวนเงิน")
            ret = False
        End If

        'Country re ประเทศผู้รับทุน
        If RadioSelectRe.SelectedValue = 1 Then
            Dim DTProjectCountryRe As DataTable = UCProjectCountryRe.ComponentDT
            For i As Integer = 0 To DTProjectCountryRe.Rows.Count - 1
                Dim id As String = DTProjectCountryRe.Rows(i)("id").ToString
                Dim country_node_id As String = DTProjectCountryRe.Rows(i)("country_node_id").ToString
                Dim amout_person As String = DTProjectCountryRe.Rows(i)("amout_person").ToString

                If country_node_id = "" Then
                    alertmsg("กรุณาระบุประเทศ ")
                    ret = False
                End If

                If amout_person = 0 Then
                    alertmsg("กรุณาระบุจำนวนคน ")
                    ret = False
                End If

                Dim tempdr() As DataRow
                tempdr = DTProjectCountryRe.Select("country_node_id='" & country_node_id & "' and id <> '" & id & "'")
                If tempdr.Length > 0 Then
                    alertmsg("รายชื่อประเทศรับทุนซ้ำ ")
                    ret = False
                End If
            Next
        ElseIf RadioSelectRe.SelectedValue = 2 Then
            'ผู้รับทุน Recipient
            Dim DTProjectRecipient As DataTable = UCprojectRecipience.RecipienceDT
            For i As Integer = 0 To DTProjectRecipient.Rows.Count - 1
                Dim id As String = DTProjectRecipient.Rows(i)("id").ToString
                Dim recipient_id As String = DTProjectRecipient.Rows(i)("recipient_id").ToString

                If recipient_id = "" Then
                    alertmsg("กรุณาระบุผู้รับทุน ")
                    ret = False
                End If

                Dim tempdr() As DataRow
                tempdr = DTProjectRecipient.Select("recipient_id ='" & recipient_id & "' and id <> '" & id & "'")
                If tempdr.Length > 0 Then
                    alertmsg("รายชื่อผู้รับทุนซ้ำ ")
                    ret = False
                End If
            Next
        End If
        'Admincost
        Dim DTProjectAdministrativeCost As DataTable = UCProjectAdministrativeCost.ComponentDT
        For i As Integer = 0 To DTProjectAdministrativeCost.Rows.Count - 1
            Dim id As String = DTProjectAdministrativeCost.Rows(i)("id").ToString
            Dim date_payment As String = DTProjectAdministrativeCost.Rows(i)("date_payment").ToString
            Dim Description As String = DTProjectAdministrativeCost.Rows(i)("Description").ToString
            Dim amount As String = DTProjectAdministrativeCost.Rows(i)("amount").ToString

            If date_payment = "" Then
                alertmsg("กรุณาระบุวันที่")
            End If


            Try
                Converter.StringToDate(date_payment, "dd/MM/yyyy")
            Catch ex As Exception
                alertmsg("กรุณาตรวจสอบวันที่")
                ret = False
            End Try

            If Description = "" Then
                alertmsg("กรุณาระบุรายละเอียด")
            End If

            If amount = 0 Then
                alertmsg("กรุณาระบุจำนวนเงิน")
                ret = False
            End If

            Dim tempdr() As DataRow
            tempdr = DTProjectAdministrativeCost.Select("date_payment='" & date_payment & "' and id <> '" & id & "'")
            If tempdr.Length > 0 Then
                alertmsg("วันที่จ่ายเงินซ้ำ ")
                ret = False
            End If
        Next

        Return ret
    End Function

#End Region

    Private Function GetRecipientPerson() As DataTable
        Dim dt As New DataTable
        With dt
            .Columns.Add("activity_id")
            .Columns.Add("recipient_id")
        End With

        Dim dr As DataRow
        For Each Item As ListItem In ctlSelectRecipient.Items
            If Item.Selected Then
                dr = dt.NewRow
                dr("recipient_id") = Item.Value
                dt.Rows.Add(dr)
            End If
        Next
        Return dt
    End Function

    Private Function GetRecipientCountry() As DataTable
        Dim dt As New DataTable
        With dt
            .Columns.Add("activity_id")
            .Columns.Add("country_id")
        End With

        Dim dr As DataRow
        For Each Item As ListItem In ctlSelectRecipientCountry.Items
            If Item.Selected Then
                dr = dt.NewRow
                dr("country_id") = Item.Value
                dt.Rows.Add(dr)
            End If
        Next
        Return dt
    End Function

    Private Sub alertmsg(msg As String)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & msg & "');", True)
    End Sub

    Private Sub ddlBudgetGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBudgetGroup.SelectedIndexChanged
        BL.Bind_DDL_SubBudget(ddlBudgetSub, ddlBudgetGroup.SelectedValue)
    End Sub

    Private Sub ddlSector_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSector.SelectedIndexChanged
        BL.Bind_DDL_Perpose(ddlSubSector, ddlSector.SelectedValue)
    End Sub

    Protected Sub lblGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        'Dim value As String = CType(sender, DropDownList).SelectedValue
        If ddlSelectReport.SelectedValue = 1 Then
            ddlComponent.Visible = True
            ddlInkind.Visible = False
            chkActive.Visible = True
            Label1.Text = "<p class=""pull-right"">ประกาศรับสมัครผู้รับทุน :</p>"
            traid.Visible = True
        ElseIf ddlSelectReport.SelectedValue = 2 Then
            ddlComponent.Visible = False
            ddlInkind.Visible = True
            chkActive.Visible = False
            chkActive.Checked = False
            Label1.Text = ""
            traid.Visible = True
        Else
            ddlComponent.Visible = False
            ddlInkind.Visible = True
            chkActive.Visible = False
            Label1.Text = ""
            traid.Visible = False
        End If
    End Sub

    Private Sub btnAddRow1_Click(sender As Object, e As EventArgs) Handles btnAddRow1.Click
        UCProjectAdministrativeCost.btnAdd_Click(Nothing, Nothing)
    End Sub

    Private Sub AddExpense_Click(sender As Object, e As EventArgs) Handles btnApply.Click
        Try

            Dim lnqActivity As New TbActivityLinqDB
            With lnqActivity
                .ID = lblActivityID.Text
                .ACTIVITY_NAME = txtActName.Text
                .DESCRIPTION = txtActDescription.Text

                If ddlSector.SelectedValue <> "" Then
                    .SECTOR_ID = ddlSector.SelectedValue
                End If
                If ddlSubSector.SelectedValue <> "" Then
                    .SUB_SECTOR_ID = ddlSubSector.SelectedValue
                End If

                .PARENT_ID = lblParentActivityID.Text
                .PROJECT_ID = lblProjectID.Text
                .NOTIFY = IIf(chkActive.Checked, "Y", "N").ToString


                If RadioSelectRe.SelectedValue = 1 Then
                    .RECIPIENT_TYPE = "G"
                ElseIf RadioSelectRe.SelectedValue = 2 Then
                    .RECIPIENT_TYPE = "R"
                ElseIf RadioSelectRe.SelectedValue = 3 Then
                    .RECIPIENT_TYPE = "C"
                End If

            End With

            '#TabActExpense
            Dim DTBudget As New DataTable
            With DTBudget
                .Columns.Add("budget_year")
                .Columns.Add("budget_group_id")
                .Columns.Add("budget_sub_id")
                .Columns.Add("amount")
            End With
            Dim dr As DataRow
            dr = DTBudget.NewRow
            dr("budget_year") = ddlBudgetYear.SelectedValue
            dr("budget_group_id") = ddlBudgetGroup.SelectedValue
            dr("budget_sub_id") = ddlBudgetSub.SelectedValue
            dr("amount") = txtAmount.Text
            DTBudget.Rows.Add(dr)

            With lnqActivity
                If txtActualActStart.Text = "" Or txtActualActEnd.Text = "" Then
                Else
                    .ACTUAL_START = Converter.StringToDate(txtActualActStart.Text.Trim, "dd/MM/yyyy")
                    .ACTUAL_END = Converter.StringToDate(txtActualActEnd.Text.Trim, "dd/MM/yyyy")
                End If
            End With

            Dim template_id As Long = lbltemplate.SelectedValue

            '#TabActReceipent
            Dim DTRecipientPerson As DataTable = GetRecipientPerson()
            Dim DTRecipentCountry As DataTable = GetRecipientCountry()

            With lnqActivity
                .BENEFICIARY = txtBenificiary.Text
            End With

            Dim DT_Administrative_Cost As New DataTable
            DT_Administrative_Cost = UCProjectAdministrativeCost.ComponentDT
            Dim DT_ProjectCountryRe As New DataTable
            DT_ProjectCountryRe = UCProjectCountryRe.ComponentDT
            Dim DT_UCProjectCountry As New DataTable
            DT_UCProjectCountry = UCProjectCountry.CountryDT
            Dim DT_ProjectRecipient As New DataTable
            DT_ProjectRecipient = UCprojectRecipience.RecipienceDT

            If RadioSelectRe.SelectedValue = 1 Then
                If DT_ProjectCountryRe.Rows.Count = 0 Then
                    alertmsg("กรุณาระบุประเทศที่รับทุน")
                    Exit Sub
                End If
            ElseIf RadioSelectRe.SelectedValue = 2 Then
                If DT_ProjectRecipient.Rows.Count = 0 Then
                    alertmsg("กรุณาระบุผู้รับทุน")
                    Exit Sub
                End If
            ElseIf RadioSelectRe.SelectedValue = 3 Then
                If DT_UCProjectCountry.Rows.Count = 0 Then
                    alertmsg("กรุณาระบุประเทศที่รับทุน")
                    Exit Sub
                End If

            End If

            '#ถ้าเป็นComponent
            If ddlSelectReport.SelectedValue = 1 Then
                '#SetComponent
                Dim DTComponent As New DataTable
                With DTComponent
                    .Columns.Add("component_id")
                End With
                Dim drcom As DataRow
                drcom = DTComponent.NewRow
                drcom("component_id") = ddlComponent.SelectedValue
                DTComponent.Rows.Add(drcom)

                Dim aid As Double
                aid = BL.SaveProjectActivityforPlanExpand(lnqActivity, DTBudget, DTComponent, Nothing, DTRecipientPerson, DTRecipentCountry, Nothing, Nothing, UserName, DT_ProjectCountryRe, Nothing, DT_Administrative_Cost, DT_ProjectRecipient, template_id, DT_UCProjectCountry, Nothing)
                lblActivityID.Text = aid
                '#ถ้าเป็นInkind
            ElseIf ddlSelectReport.SelectedValue = 2 Then
                '#SetInkind
                Dim DTInkind As New DataTable
                With DTInkind
                    .Columns.Add("inkind_id")
                End With
                Dim drik As DataRow
                drik = DTInkind.NewRow
                drik("inkind_id") = ddlInkind.SelectedValue
                DTInkind.Rows.Add(drik)

                Dim aid As Double
                aid = BL.SaveProjectActivityforPlanExpand(lnqActivity, DTBudget, Nothing, DTInkind, DTRecipientPerson, DTRecipentCountry, Nothing, Nothing, UserName, DT_ProjectCountryRe, Nothing, DT_Administrative_Cost, DT_ProjectRecipient, template_id, DT_UCProjectCountry, Nothing)
                lblActivityID.Text = aid
            Else
                Dim aid As Double
                aid = BL.SaveProjectActivityforPlanExpand(lnqActivity, DTBudget, Nothing, Nothing, DTRecipientPerson, DTRecipentCountry, Nothing, Nothing, UserName, DT_ProjectCountryRe, Nothing, DT_Administrative_Cost, DT_ProjectRecipient, template_id, DT_UCProjectCountry, Nothing)
                lblActivityID.Text = aid
            End If

        Catch ex As Exception
        End Try

        Dim lnqAEH As New TbActivityExpenseHeaderLinqDB
        With lnqAEH
            .ID = 0
            .ACTIVITY_ID = lblActivityID.Text
            '.PAYMENT_DATE_PLAN = Converter.StringToDate(txtPaymentDate.Text.Trim, "dd/MM/yyyy")
            '.PAYMENT_DETAIL = txtDetail.Text
        End With

        Dim tem_id As Long = lbltemplate.SelectedValue
        Dim HID As Double = BL.SaveHeaderID(lnqAEH, UserName, tem_id)


        Response.Redirect("frmActivityExpense.aspx?ActivityID=" & lblActivityID.Text & " &mode=" & "Add" & "&HeaderId=" & HID & "&PID=" & lblProjectID.Text & "&type=" & ProjectType)
    End Sub

    Sub Authorize()
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim btnEdit As LinkButton = DirectCast(rptList.Items(i).FindControl("btnEdit"), LinkButton)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)
            Dim ColDelete As HtmlTableCell = DirectCast(rptList.Items(i).FindControl("ColDelete"), HtmlTableCell)


        Next

    End Sub

    Private Sub BindList(activity_id As Long)

        Dim DT As DataTable = BL.GetList_Expense2(activity_id)
        rptList.DataSource = DT
        rptList.DataBind()

        If (DT.Rows.Count > 0) Then
            lblBudget_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Pay_Amount_Plan)", "")).ToString("#,##0.00")
            lblDisbursement_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Pay_Amount_Actual)", "")).ToString("#,##0.00")
            Dim SumSum As Decimal = lblBudget_Sum.Text - lblDisbursement_Sum.Text
            If SumSum < 0 Then
                lblpayTotalSum.Text = 0
            Else
                lblpayTotalSum.Text = SumSum.ToString("#,##0.00")

            End If

            lbltemplate.Enabled = False
            'Dim A As Long = txtAmount.Text - lblBudget_Sum.Text
            'txtbutged.Text = A.ToString("#,##0.00")
            pnlFooter.Visible = True
        Else
            lbltemplate.Enabled = True
            txtbutged.Text = txtAmount.Text
            pnlFooter.Visible = False
        End If
        txtbutged.Enabled = False
    End Sub

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim lbldatePlan As Label = DirectCast(e.Item.FindControl("lbldatePlan"), Label)
        Dim lbldateActual As Label = DirectCast(e.Item.FindControl("lbldateActual"), Label)
        Dim lblpayActual As Label = DirectCast(e.Item.FindControl("lblpayActual"), Label)
        Dim lblpayPlan As Label = DirectCast(e.Item.FindControl("lblpayPlan"), Label)
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)
        Dim lblpayTotal As Label = DirectCast(e.Item.FindControl("lblpayTotal"), Label)
        Dim tdPayActual As HtmlTableCell = e.Item.FindControl("tdPayActual")
        Dim td2 As HtmlTableCell = e.Item.FindControl("td2")

        Dim planpay As Long = e.Item.DataItem("Pay_Amount_Plan").ToString
        Dim actualpay As Long = e.Item.DataItem("Pay_Amount_Actual").ToString

        lblID.Text = e.Item.DataItem("id").ToString
        lbldatePlan.Text = e.Item.DataItem("Payment_Date_Start").ToString
        lbldateActual.Text = e.Item.DataItem("Payment_Date_End").ToString
        lblpayActual.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Actual")).ToString("#,##0.00")
        lblpayPlan.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Plan")).ToString("#,##0.00")


        Dim Sum As Decimal = lblpayPlan.Text - lblpayActual.Text

        If Sum < 0 Then
            tdPayActual.Attributes.Add("style", "background-color:red; text-align:right;")

            td2.Attributes.Add("style", "background-color:red; text-align:right;")
        End If

        lblpayTotal.Text = Sum.ToString("#,##0.00")
    End Sub


    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        If e.CommandName = "cmdEdit" Then
            Response.Redirect("frmActivityExpense.aspx?ActivityID=" & lblActivityID.Text & " &mode=" & "Edit" & "&HeaderId=" & e.CommandArgument & "&PID=" & lblProjectID.Text & "&type=" & ProjectType)

        ElseIf e.CommandName = "cmdDelete" Then
            Dim BL As New ODAENG
            'Dim ret As ProcessReturnInfo
            'ret = BL.DeleteExpenseHeaderDetail(e.CommandArgument)
            If BL.DeleteExpenseHeaderDetail(e.CommandArgument) Then
                alertmsg("ลบข้อมูลเรียบร้อยแล้ว")
                BindList(lblActivityID.Text)
            Else
                alertmsg("ไม่สามารถลบข้อมูลได้")
            End If
        ElseIf e.CommandName = "cmdPay" Then
            Response.Redirect("frmRename.aspx?ActivityID=" & lblActivityID.Text & " &mode=" & "payinac" & "&HeaderId=" & e.CommandArgument & "&PID=" & lblProjectID.Text & "&type=" & ProjectType)
        End If
    End Sub

    Private Sub RadioSelectRe_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RadioSelectRe.SelectedIndexChanged

        If RadioSelectRe.SelectedValue = "1" Then
            UCProjectCountryRe.Visible = True
            UCprojectRecipience.Visible = False
            UCProjectCountry.Visible = False
        ElseIf RadioSelectRe.SelectedValue = "2" Then
            UCprojectRecipience.Visible = True
            UCProjectCountryRe.Visible = False
            UCProjectCountry.Visible = False
        ElseIf RadioSelectRe.SelectedValue = "3" Then
            UCProjectCountry.Visible = True
            UCprojectRecipience.Visible = False
            UCProjectCountryRe.Visible = False
        End If
    End Sub
End Class



