﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCProjectCountry.ascx.vb" Inherits="usercontrol_UCProjectCountry" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<div class="box-body">
    <table id="example2" class="table table-bordered table-hover">
        <thead>
            <tr class="bg-gray">
                <th>ประเทศที่รับทุน (Country of Recipient)</th>
                <th style="width: 50px">Delete(ลบ)</th>
            </tr>
        </thead>
        <tbody>
              <asp:Label ID="lblActivityID" runat="server" Text ="0" Visible="false"></asp:Label>
            <asp:Repeater ID="rptList" runat="server">
                <ItemTemplate>
                    <tr>
                              <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                        <td data-title="ประเทศที่รับทุน (Country of Recipient)" style ="padding:0px;">
                            <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control select2" Style="width: 100%">
                            </asp:DropDownList>
                           <%-- <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>--%>
                        </td>
                        <td data-title="Delete" id="ColDelete" runat="server" style ="padding:0px;">
                            <center>
                                <asp:Button ID="btnDelete" runat="server" Text="ลบ" CssClass="btn btn-bricky"  width="100%"  CommandArgument='<%# Eval("id") %>' CommandName="Delete" OnClientClick="return confirm('ท่านต้องการลบรายการนี้หรือไม่?');"/>
                            </center>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </tbody>
    </table>


    <div class="row"  style="margin-top:5px;">
        <center>
            <div class="col-sm-12">
                <asp:Button ID="btnAdd"  runat="server" Text="เพิ่ม" 
                    CssClass="btn btn-primary" />
            </div>
        </center>
    </div>
</div>