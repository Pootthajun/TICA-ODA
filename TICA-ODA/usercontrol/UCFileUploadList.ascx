﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCFileUploadList.ascx.vb" Inherits="usercontrol_UCFileUploadList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">    
    <ContentTemplate>
        
        <div class="box-body">
            <table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:0px solid #efefef;">
                <tr>
                    <td style="width:360px;vertical-align:bottom;">
                        <progress id="progressBar" value="0" max="100" style="width: 340px;height:20px"></progress>
                        <asp:Label ID="lblfilter" runat="server" Text ="รองรับไฟล์นามสกุล DOCX,XLSX,VSDX,PDF,PPTX ขนาดไฟล์ไม่เกิน 5 MB" Font-Size="Smaller" ForeColor="Red"></asp:Label>
                    </td>
                    <td style="width:120px; vertical-align:top; padding-top: 3px;">
                        <asp:FileUpload ID="ful" ClientIDMode="Static" runat="server" Style="display: none;" onchange="uploadFile();return false;" />
                        <a href="#" onclick="return UploadVDO();" style="width:100px" class="btn btn-primary btn-block btn-flat">
                            <span>Choose File</span>
                        </a>
                    </td>
                    <td style="vertical-align:top; padding-top: 3px;"><asp:Button ID="btnUpload" runat="server" style="width:100px" class="btn btn-success btn-block btn-flat" Text="Upload" /></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <h4 id="status"  style="width:100%;"></h4>
                        <p id="loaded_n_total"></p>
                    </td>
                </tr>
            </table>

            <asp:TextBox ID="txtFileFilter" runat="server" style="display:none" ></asp:TextBox>
          
            <table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:0px solid #efefef;">
                <thead>
                    <tr>
                        <th style="width:10%;text-align:center">No.</th>
                        <th style="width:70%;text-align:center">File Name</th>
                        <th id="thDel" runat="server" style="width:20%;text-align:center">Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="rptFileList" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td><center><asp:Label ID="lblNo" runat="server"></asp:Label></center></td>
                                <td>                               
                                    <asp:Label ID="lblFileName" runat="server"  ></asp:Label>
                                    <asp:Label ID="lblFilePath" runat="server" Visible="false"  ></asp:Label>
                                    <asp:Label ID="lblFileID" runat="server" Visible="false" Text="0"  ></asp:Label>
                                </td>
                                <td id="ColDelete" runat="server">
                                    <center>
                                        <asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ImageUrl="../dist/img/cross.png" />
                                        <cc1:ConfirmButtonExtender ID="cfbDelete" runat="server" TargetControlID="btnDelete" ConfirmText="Are you sure to delete this file permanently?" />
                                    </center>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <tr>
                        <td colspan="3">&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <script type="text/javascript">
            function UploadVDO() {
                $('#ful').click();
                return false;
            }
            function uploadFile() {

                var FileFilter = document.getElementById('<%= txtFileFilter.ClientID %>').value;

                var file = $("#ful").prop("files")[0];
                //alert(file.name+" | "+file.size+" | "+file.type);
                var formdata = new FormData();
                formdata.append("ful", file);
                formdata.append("FileFilter", FileFilter);
                var ajax = new XMLHttpRequest();
                ajax.upload.addEventListener("progress", progressHandler, false);
                ajax.addEventListener("load", completeHandler, false);
                ajax.addEventListener("error", errorHandler, false);
                ajax.addEventListener("abort", abortHandler, false);
                ajax.open("POST", "usercontrol/frmRenderFileUpload.aspx");
                ajax.send(formdata);
            }
            function progressHandler(event) {

                $("#loaded_n_total").innerHTML = "Uploaded " + event.loaded + " bytes of " + event.total;
                var percent = (event.loaded / event.total) * 100;

                //$("#progressBar").value = Math.round(percent);
                document.getElementById("progressBar").value = Math.round(percent);
                document.getElementById("status").innerHTML = Math.round(percent) + "% uploaded... please wait";
            }
            function completeHandler(event) {
                var tmp = event.target.responseText.split("|");
                if (tmp[0] == "true") {
                    //alert("Upload Complete");
                    document.getElementById("status").innerHTML = tmp[1];  //FileName
                    document.getElementById("progressBar").value = 0;
                } else {
                    document.getElementById("status").innerHTML = "";
                    document.getElementById("progressBar").value = 0;
                    alert("Upload Fail !!!" + tmp[1]);
                }
            }
            function errorHandler(event) {
                $("#status").innerHTML = "Upload Failed";
            }
            function abortHandler(event) {
                $("#status").innerHTML = "Upload Aborted";
            }
        </script>
    </ContentTemplate>
</asp:UpdatePanel>