﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCInkind.ascx.vb" Inherits="usercontrol_UCInkind" %>

        <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                    <tr class="bg-gray">
                        <th>In kind(ความช่วยเหลือด้านอื่นๆ)</th>
                        <th>Estimate(บาท)</th>
                        <th style="width: 50px">Delete(ลบ)</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="rptList" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td data-title="In kind(ความช่วยเหลือด้านอื่นๆ)">
                                    <asp:DropDownList ID="ddlInkind" runat="server" CssClass="form-control select2" Style="width: 100%">
                                    </asp:DropDownList>
                                    <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                                </td>
                                <td data-title="Estimate(บาท)" id="td" runat="server">
                                    <asp:TextBox ID="txtEstimate" runat="server" CssClass="form-control" style="text-align: right" placeholder="" AutoPostBack="true" OnTextChanged="txtEstimate_TextChanged" MaxLength="12"></asp:TextBox>
                                </td>
                                <td data-title="Delete" id="ColDelete" runat="server">
                                    <center>
                                <asp:Button ID="btnDelete" runat="server" Text="ลบ" CssClass="btn btn-bricky"  CommandArgument='<%# Eval("id") %>' CommandName="Delete" OnClientClick="return confirm('ท่านต้องการลบรายการนี้หรือไม่?');"/>
                            </center>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>

            </table>
            <div class="row">
                <div class="col-sm-5 cost">
                    <b>รวม</b>
                </div>
                <div class="col-sm-6 cost cost-uc">
                    <b>
                        <asp:Label ID="lblTotal" runat="server" Text="0"></asp:Label></b>
                </div>
            </div>

            <div class="row">
                <center>
                    <div class="col-sm-12">
                        <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" Text="เพิ่ม"
                            CssClass="btn btn-primary" />
                    </div>
                </center>
            </div>
        </div>
