﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCActivityTabProject.ascx.vb" Inherits="usercontrol_UCActivityTabProject" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%--<%@ Register src="UCProjectComponent.ascx" tagname="UCProjectComponent" tagprefix="uc2" %>
<%@ Register src="UCInkind.ascx" tagname="UCInkind" tagprefix="uc3" %>--%>
<%@ Register Src="UCActivityBudget.ascx" TagName="UCActivityBudget" TagPrefix="uc4" %>
<%@ Register Src="UCProjectDisbursement.ascx" TagName="UCProjectDisbursement" TagPrefix="uc5" %>
<%@ Register Src="~/usercontrol/UCProjectAdministrativeCost.ascx" TagName="UCProjectAdministrativeCost" TagPrefix="uc6" %>
<%@ Register Src="~/usercontrol/UCProjectCountryRe.ascx" TagName="UCProjectCountryRe" TagPrefix="uc7" %>
<%@ Register Src="~/usercontrol/UCprojectRecipience.ascx" TagName="UCprojectRecipience" TagPrefix="uc8" %>
<%@ Register Src="~/usercontrol/UCProjectCountry.ascx" TagName="UCProjectCountry" TagPrefix="uc9" %>
<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>


<asp:UpdatePanel ID="UpdatePanel1" runat="server">


    <ContentTemplate>

        <asp:Panel ID="pnlAdd" runat="server">
            <div class="modal-header">
                <div class="col-sm-8" style="margin-left: -15px; margin-bottom: 15px;">
                    <%--<b>*Title (หัวข้อกิจกรรม)</b>--%>
                    <b><span style="color: red;">* </span>Title (หัวข้อกิจกรรม)</b>
                    <asp:TextBox ID="txtActName" runat="server" CssClass="form-control m-b" Style="width: 100%;"></asp:TextBox>
                </div>


                <div class="col-sm-4">
                    <b><span style="color: red;">* </span>Group Aid (ประเภทความช่วยเหลือ)</b>
                    <asp:DropDownList ID="ddlSelectReport" runat="server" CssClass="form-control select2" Style="width: 100%;" AutoPostBack="True" OnSelectedIndexChanged="lblGroup_SelectedIndexChanged">
                        <asp:ListItem Value="0">Select Group Aid</asp:ListItem>
                        <asp:ListItem Value="1">Component (ประเภทการให้ทุน)</asp:ListItem>
                        <asp:ListItem Value="2">In kind (ความช่วยเหลือด้านอื่นๆ)</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <br />
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <asp:Panel ID="pnlTab" runat="server" Visible="false">
                        <ul class="nav nav-tabs">
                            <li class="" id="liTabActActDetail" runat="server">
                                <asp:LinkButton ID="btnTabActActDetail" runat="server" Style="padding: 4px 10px 5px 20px;">
                                        <div class="iti-flag gb" style="float:left; margin-top:3px; margin-right:3px; cursor:pointer;"></div><h5><span style="cursor:pointer"><i class="fa fa-list-alt"></i>ข้อมูลกิจกรรม</span></h5>
                                </asp:LinkButton>
                            </li>

                            <li class="" id="liTabActReceipent" runat="server">
                                <asp:LinkButton ID="btnTabActReceipent" runat="server" Style="padding: 4px 4px 5px 5px;">
                                        <div class="iti-flag ru" style="float:left; margin-top:0px; margin-right:3px; cursor:pointer;"></div><h5><span style="cursor:pointer"><i class="fa fa-users"></i>ข้อมูลผู้รับทุน</span></h5>
                                </asp:LinkButton>
                            </li>

                            <li class="" id="liTabActPeriod" runat="server">
                                <asp:LinkButton ID="btnTabActPeriod" runat="server" Style="padding: 4px 4px 5px 5px;">
                                        <div class="iti-flag ru" style="float:left; margin-top:0px; margin-right:3px; cursor:pointer;"></div><h5><span style="cursor:pointer"><i class="fa fa-th-list"></i>ระยะเวลา</span></h5>
                                </asp:LinkButton>
                            </li>

                            <li class="" id="liTabActExpense" runat="server">
                                <asp:LinkButton ID="btnTabActExpense" runat="server" Style="padding: 4px 4px 5px 5px;">
                                        <div class="iti-flag ru" style="float:left; margin-top:0px; margin-right:3px; cursor:pointer;"></div><h5><span style="cursor:pointer"><i class="fa fa-th-list"></i>งบประมาณ</span></h5>
                                </asp:LinkButton>
                            </li>

                            <li class="" id="liTabActDisversment" runat="server">
                                <asp:LinkButton ID="btnTabActDisversment" runat="server" Style="padding: 4px 4px 5px 5px;">
                                        <div class="iti-flag ru" style="float:left; margin-top:0px; margin-right:3px; cursor:pointer;"></div><h5><span style="cursor:pointer"><i class="fa fa-th-list"></i>ค่าใช้จ่าย</span></h5>
                                </asp:LinkButton>
                            </li>

                        </ul>
                    </asp:Panel>
                    <div>
                        <%--TabActActDetail--%>
                        <asp:Panel ID="TabActActDetail" runat="server">
                            <table class="table table-bordered">
                                <%--<asp:TextBox ID="txtActName" runat="server" TextMode="MultiLine" Height="60px" Width="100%" MaxLength="250"></asp:TextBox>--%>
                                <asp:Label ID="lblActivityID" runat="server" Text="0" Visible="false"></asp:Label>
                                <asp:Label ID="lblParentActivityID" runat="server" Text="0" Visible="false"></asp:Label>
                                <asp:Label ID="lblProjectID" runat="server" Text="0" Visible="false"></asp:Label>
                                <asp:Label ID="lblActivityMode" runat="server" Text="" Visible="false"></asp:Label>
                                <tr>
                                    <td style="width: 200px">
                                        <p class="pull-right">Description (รายละเอียด) :</p>
                                    </td>
                                    <td>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtActDescription" runat="server" TextMode="MultiLine" Height="60px" Width="100%" MaxLength="1000"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="width: 200px">
                                        <p class="pull-right"><span style="color: red;">*</span>Sector (สาขา) :</p>
                                    </td>
                                    <td>
                                        <div class="col-sm-12">
                                            <asp:DropDownList ID="ddlSector" runat="server" CssClass="form-control select2" Style="width: 100%" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>

                                    </td>
                                </tr>

                                <tr>
                                    <td style="width: 200px">
                                        <p class="pull-right">Sector Type(สาขาย่อย) :</p>
                                    </td>
                                    <td>
                                        <div class="col-sm-12">
                                            <asp:DropDownList ID="ddlSubSector" runat="server" CssClass="form-control select2" Style="width: 100%">
                                            </asp:DropDownList>
                                        </div>

                                    </td>
                                </tr>
                                <tr runat="server" id ="traid">
                                    <td style="width: 200px">
                                        <p class="pull-right">
                                            </b>
                                                <br />
                                            <p class="pull-right">aid type(ความช่วยเหลือย่อย) :</p>
                                    </td>
                                    <td>
                                        <div class="col-sm-12">
                                            <asp:DropDownList ID="ddlComponent" runat="server" CssClass="form-control select2" Style="width: 50%">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlInkind" runat="server" CssClass="form-control select2" Style="width: 50%">
                                            </asp:DropDownList>
                                            <%--<uc2:UCProjectComponent runat="server" ID="UCProjectComponent" />--%>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px">
                                        <asp:Label ID="Label1" runat="server"></asp:Label>

                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <div class="col-sm-9">
                                                <asp:CheckBox ID="chkActive" runat="server" />
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>

                        <%--TabActReceipent--%>
                        <asp:Panel ID="TabActRecipent" runat="server">
                            <table class="table table-bordered">
                                <asp:Panel ID="pnlRecipient_PSN" runat="server" Visible="false">
                                    <asp:ListBox ID="ctlSelectRecipient" runat="server" CssClass="form-control select2" SelectionMode="Multiple"
                                        data-placeholder="-" Style="width: 100%;"></asp:ListBox>
                                    <asp:TextBox ID="txtRecipient" runat="server" CssClass="form-control" placeholder="" Visible="false"></asp:TextBox>

                                    <asp:ListBox ID="ctlSelectRecipientCountry" runat="server" CssClass="form-control select2" SelectionMode="Multiple"
                                        data-placeholder="-" Style="width: 100%;"></asp:ListBox>
                                    <asp:TextBox ID="txtRecipientCountry" runat="server" CssClass="form-control" placeholder="" Visible="false"></asp:TextBox>

                                    <asp:TextBox ID="txtBenificiary" runat="server" TextMode="MultiLine" Height="60px" Width="100%" MaxLength="1000"></asp:TextBox>


                                </asp:Panel>

                                <tr class="bg-info">
                                    <th colspan="2">
                                        <h5 style="margin: 2px;"><b class="text-blue"><span style="cursor: pointer"><i class="fa fa-users"></i>ข้อมูลผู้รับทุน</span></b></h5>
                                    </th>
                                </tr>
                                <tr>
                                    <td style="width: 210px; text-align: right">
                                        <asp:RadioButtonList ID="RadioSelectRe" runat="server" AutoPostBack="true" Style="text-align: left">
                                            <asp:ListItem Selected="true" Text="Country Recipient(ระยะสั้น)" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Recipient(ระยะยาว)" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Country(ประเทศ)" Value="3"></asp:ListItem>
                                        </asp:RadioButtonList></td>
                                    <td>
                                        
                                        <uc7:UCProjectCountryRe runat="server" ID="UCProjectCountryRe" />
                                        <uc8:UCprojectRecipience runat="server" ID="UCprojectRecipience" Visible="false" />
                                        <uc9:UCProjectCountry runat="server" ID="UCProjectCountry" Visible="false" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>

                        <%--TabActPeriod--%>
                        <asp:Panel ID="TabActPeriod" runat="server">
                            <table class="table table-bordered">
                                <tr class="bg-info">
                                    <th colspan="2">
                                        <h5 style="margin: 2px;"><b class="text-blue"><span style="cursor: pointer"><i class="fa fa-th-list"></i>ระยะเวลา</span></b></h5>
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <p class="pull-right">
                                            Plan(Start/End Date) </b>
                                                <br />
                                            <p class="pull-right">(วันเริ่มต้น/สิ้นสุด) :</p>
                                    </td>
                                    <td>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <asp:TextBox CssClass="form-control m-b" ID="txtPlanActStart" runat="server" placeholder=""></asp:TextBox>
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server"
                                                    Format="dd/MM/yyyy" TargetControlID="txtPlanActStart" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                            </div>
                                            <!-- /.input group -->

                                        </div>
                                        <div class="col-sm-1">
                                            <h5>
                                                <p class="text-black">(TO)ถึง</p>
                                            </h5>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <asp:TextBox CssClass="form-control m-b" ID="txtplanActEnd" runat="server" placeholder=""></asp:TextBox>
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server"
                                                    Format="dd/MM/yyyy" TargetControlID="txtplanActEnd" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <%--<label class="pull-right"> day, week, month, year</label>--%>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px">
                                        <p class="pull-right">
                                            <span style="color: red;">*</span>Actual (Start/End Date) </b>
                                                <br />
                                            <p class="pull-right">(วันเริ่มต้น/สิ้นสุด) :</p>
                                    </td>
                                    <td>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <asp:TextBox CssClass="form-control m-b" ID="txtActualActStart" runat="server" placeholder=""></asp:TextBox>
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender5" runat="server"
                                                    Format="dd/MM/yyyy" TargetControlID="txtActualActStart" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                            </div>
                                            <!-- /.input group -->

                                        </div>
                                        <div class="col-sm-1">
                                            <h5>
                                                <p class="text-black">(TO)ถึง</p>
                                            </h5>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <asp:TextBox CssClass="form-control m-b" ID="txtActualActEnd" runat="server" placeholder=""></asp:TextBox>
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender6" runat="server"
                                                    Format="dd/MM/yyyy" TargetControlID="txtActualActEnd" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <%--<label > day, week, month, year</label>--%>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>

                        <%--TabActExpense--%>
                        <asp:Panel ID="TabActExpense" runat="server">
                            <table class="table table-bordered">

                                <tr class="bg-info">
                                    <th colspan="4">
                                        <h5 style="margin: 2px;"><b class="text-blue"><span style="cursor: pointer"><i class="fa fa-th-list"></i>Allocated Budget (จัดสรรงบประมาณ)</span></b></h5>
                                    </th>
                                </tr>
                                <tr>
                                    <td style="width: 200px; text-align: right;">
                                        <p>Budget Year </p>
                                        <p class="pull-right">(ปีงบประมาณ) :</p>
                                    </td>
                                    <td colspan="3">
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlBudgetYear" runat="server" CssClass="form-control select2" Style="width: 100%">
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px; text-align: right;">
                                        <p><span style="color: red;">*</span>Budget Type</p>
                                        <p class="pull-right">(ประเภทงบประมาณ) :</p>
                                    </td>
                                    <td colspan="3">
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlBudgetGroup" runat="server" CssClass="form-control select2" Style="width: 100%" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px; text-align: right;">
                                        <p><span style="color: red;">*</span>Sub Budget </p>
                                        <p class="pull-right">(งบประมาณ) :</p>
                                    </td>
                                    <td colspan="3">
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlBudgetSub" runat="server" CssClass="form-control select2" Style="width: 100%">
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px; text-align: right;">
                                        <p><span style="color: red;">*</span>total budget</p>
                                        <p class="pull-right">(งบประมาณทั้งหมด) :</p>
                                    </td>
                                    <td>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control cost" placeholder="" MaxLength="12" Width="180px"></asp:TextBox>
                                        </div>
                                    </td>
                                    <td style="width: 200px; text-align: right;">
                                        <p><span style="color: red;">*</span>balance budget</p>
                                        <p class="pull-right">(งบประมาณที่ยังไม่ได้จัดสรรค์) :</p>
                                    </td>
                                    <td>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtbutged" runat="server" CssClass="form-control cost" placeholder="" MaxLength="12" Width="180px"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>

                        <%--TabActDisversement--%>
                        <asp:Panel ID="TabActDisversement" runat="server">
                            <table class="table table-bordered">
                                <tr class="bg-info">
                                    <th colspan="2">
                                        <h5 style="margin: 2px;"><b class="text-blue"><span style="cursor: pointer"><i class="fa fa-th-list"></i>Expenditure plan Costing(แผนการเบิกจ่ายและค่าใช้จ่ายในกิจกรรม)</span></b></h5>
                                    </th>
                                </tr>
                                <tr>
                                    <td style="width: 200px; text-align: right;">
                                        <p><span style="color: red;">*</span>Template</p>
                                        <p class="pull-right">(รูปแบบการชำระเงิน) :</p>
                                    </td>
                                    <td>
                                        <br/>
                                        <div class="col-sm-8">
                                            <asp:DropDownList ID="lbltemplate" runat="server" CssClass="form-control select2" Style="width: 100%">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:Button ID="btnApply" runat="server" Text="Apply" CssClass="btn btn-primary"/>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table id="example2" class="table table-bordered table-hover" style="width: 100%">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th style="width: 19%;">Budget Date 
                                                        <br />
                                                        (วันที่จัดสรร)</th>
                                                    <th style="width: 19%;">Payment Date 
                                                        <br />
                                                        (วันที่จ่ายจริง)</th>
                                                    <th style="width: 18%;">Budget
                                                        <br />
                                                        (งบประมาณ)</th>
                                                    <th style="width: 18%;">Expense
                                                        <br />
                                                        (ค่าใช้จ่าย)</th>
                                                    <th style="width: 18%;">Budget Total
                                                        <br />
                                                        (งบประมาณคงเหลือ)</th>
                                                    <th class="tools">tools</br>(เครื่องมือ)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td data-title="Project ID" id="td1" style="text-align:center;"  runat="server">
                                                                <asp:Label ID="lbldatePlan" runat="server"></asp:Label>
                                                                <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label></td>
                                                            <td data-title="Project Name" style="text-align: center;">
                                                                <asp:Label ID="lbldateActual" runat="server"></asp:Label></td>
                                                            <td data-title="Record" style="text-align: right;">
                                                                <asp:Label ID="lblpayPlan" runat="server"></asp:Label></td>
                                                            <td data-title="Payment Date"  style="text-align:right;" runat="server" id="tdPayActual">
                                                                <asp:Label ID="lblpayActual" runat="server"></asp:Label>
                                                            </td>
                                                            <td data-title="Payment Date" style="text-align: right;" runat="server" id="td2">
                                                                <asp:Label ID="lblpayTotal" runat="server"></asp:Label>
                                                            </td>
                                                            <td data-title="Edit" id="ColEdit" runat="server" class="center">
                                                                <div class="btn-group">
                                                                    <button type="button" data-toggle="dropdown"  style="height:25px; width:35px;"><i class="fa fa-navicon text-green"></i>
                                                                    </button>
                                                                    <ul class="dropdown-menu pull-right">
                                                                        <li id="lipay" runat="server">
                                                                            <asp:LinkButton ID="btnPay" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="cmdPay" Visible="false">
                                                      <i class="fa fa-credit-card text-blue"></i> ชำระค่าใช้จ่าย</asp:LinkButton>
                                                                        </li>
                                                                        <li id="liedit" runat="server">
                                                                            <asp:LinkButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="cmdEdit">
                                                      <i class="fa fa-pencil text-blue"></i> แก้ไข</asp:LinkButton>
                                                                        </li>
                                                                        <li id="lidelete" runat="server">
                                                                            <asp:LinkButton ID="btnDelete" runat="server" OnClientClick='javascript:return confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?")'
                                                                                CommandArgument='<%# Eval("id") %>' CommandName="cmdDelete"><i class="fa fa-trash text-danger"></i> ลบ</asp:LinkButton>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </tbody>
                                            <asp:Panel ID="pnlFooter" runat="server" Visible="True">
                                                <tfoot style="background-color: LemonChiffon;">
                                                    <tr id="trFooter_Qty" runat="server">

                                                        <td style="text-align: center;" colspan="2"><b>Total</b></td>

                                                        <td style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblBudget_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblDisbursement_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblpayTotalSum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td></td>
                                                    </tr>
                                                </tfoot>
                                            </asp:Panel>

                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px; text-align: right;">
                                        <p>Admincost </p>
                                        <p class="pull-right">(ค่าใช้จ่ายส่วนเกิน) :</p>
                                    </td>
                                    <td colspan="3"><center>
                                <uc6:UCProjectAdministrativeCost runat="server" ID="UCProjectAdministrativeCost" />
                                <asp:Button ID="btnAddRow1" runat="server" Text="เพิ่ม" CssClass="btn btn-primary" />
                                    </center></td>
                                    </tr>
                            </table>
                        </asp:Panel>

                    </div>
                </div>
                <div class="modal-footer">
                    <asp:LinkButton ID="btnSaveAct" runat="server" CssClass="btn  btn-social btn-success">
                            <i class="fa fa-save"></i> Save
                    </asp:LinkButton>
                    <asp:LinkButton ID="btnCancelAct" runat="server" CssClass="btn  btn-social btn-google">
                            <i class="fa fa-reply"></i> Cancel
                    </asp:LinkButton>
                </div>
            </div>

        </asp:Panel>

    </ContentTemplate>
</asp:UpdatePanel>

<uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />
