﻿Imports System.Data
Partial Class usercontrol_UCActivityBudget
    Inherits System.Web.UI.UserControl
    Dim BL As New ODAENG

    Dim _BudgetDT As DataTable
    Public Property BudgetDT As DataTable
        Get
            Return GetDataFromRpt(rptList)
        End Get
        Set(value As DataTable)
            _BudgetDT = value
            rptList.DataSource = _BudgetDT
            rptList.DataBind()
            txtAmount_TextChanged(Nothing, Nothing)

        End Set
    End Property

    Public Sub SetToViewMode(IsEnable As Boolean)
        btnAdd.Enabled = Not IsEnable
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlBudgetYear As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlBudgetYear"), DropDownList)
            Dim ddlBudgetGroup As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlBudgetGroup"), DropDownList)
            Dim ddlSubBudget As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlSubBudget"), DropDownList)
            Dim txtAmount As TextBox = DirectCast(rptList.Items(i).FindControl("txtAmount"), TextBox)

            Dim btnDelete As Button = DirectCast(rptList.Items(i).FindControl("btnDelete"), Button)

            ddlBudgetYear.Enabled = Not IsEnable
            ddlBudgetGroup.Enabled = Not IsEnable
            ddlSubBudget.Enabled = Not IsEnable
            txtAmount.Enabled = Not IsEnable
            btnDelete.Enabled = Not IsEnable
        Next
    End Sub

    Protected Sub txtAmount_TextChanged(sender As Object, e As EventArgs)
        Dim _total As Double = 0.0
        For i As Integer = 0 To rptList.Items.Count - 1
            Try
                Dim txtAmount As TextBox = DirectCast(rptList.Items(i).FindControl("txtAmount"), TextBox)
                _total += CDbl(txtAmount.Text)
            Catch ex As Exception
            End Try
        Next

        lblTotal.Text = _total.ToString("#,##0.00")
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        Dim dt As DataTable = GetDataFromRpt(rptList)
        Dim dr As DataRow
        dr = dt.NewRow
        dr("id") = dt.Rows.Count + 1
        dr("budget_year") = ""
        dr("budget_group_id") = ""
        dr("budget_sub_id") = ""
        dr("amount") = 0.0
        dt.Rows.Add(dr)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Function GetDataFromRpt(rpt As Repeater) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("budget_year")
        dt.Columns.Add("budget_group_id")
        dt.Columns.Add("budget_sub_id")
        dt.Columns.Add("amount", GetType(Decimal))

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlBudgetYear As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlBudgetYear"), DropDownList)
            Dim ddlBudgetGroup As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlBudgetGroup"), DropDownList)
            Dim ddlSubBudget As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlSubBudget"), DropDownList)
            Dim txtAmount As TextBox = DirectCast(rptList.Items(i).FindControl("txtAmount"), TextBox)

            dr = dt.NewRow
            dr("id") = dt.Rows.Count + 1
            dr("budget_year") = ddlBudgetYear.SelectedValue
            dr("budget_group_id") = ddlBudgetGroup.SelectedValue
            dr("budget_sub_id") = ddlSubBudget.SelectedValue
            If txtAmount.Text = "" Then
                dr("amount") = 0.0
            Else
                dr("amount") = CDbl(txtAmount.Text)
            End If

            dt.Rows.Add(dr)
        Next

        Return dt
    End Function

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim ddlBudgetYear As DropDownList = DirectCast(e.Item.FindControl("ddlBudgetYear"), DropDownList)
        Dim ddlBudgetGroup As DropDownList = DirectCast(e.Item.FindControl("ddlBudgetGroup"), DropDownList)
        Dim btnBudgetGroup As Button = DirectCast(e.Item.FindControl("btnBudgetGroup"), Button)
        Dim ddlSubBudget As DropDownList = DirectCast(e.Item.FindControl("ddlSubBudget"), DropDownList)
        Dim txtAmount As TextBox = DirectCast(e.Item.FindControl("txtAmount"), TextBox)
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)

        'ddlBudgetGroup.Attributes.Add("onchange", "return bdBudgetChange('" & btnBudgetGroup.ClientID & "');")

        BL.SetTextDblKeypress(txtAmount)
        BL.Bind_DDL_Year(ddlBudgetYear)
        BL.Bind_DDL_BudgetGroup(ddlBudgetGroup)

        ddlBudgetYear.SelectedValue = e.Item.DataItem("budget_year")
        ddlBudgetGroup.SelectedValue = e.Item.DataItem("budget_group_id")
        ddlBudgetGroup_SelectedIndexChanged(ddlBudgetGroup, Nothing)
        ddlSubBudget.SelectedValue = e.Item.DataItem("budget_sub_id")
        txtAmount.Text = Convert.ToDecimal(e.Item.DataItem("amount")).ToString("#,##0.00")

        lblID.Text = e.Item.DataItem("id").ToString
    End Sub

    Protected Sub ddlBudgetGroup_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim ddlBudgetGroup As DropDownList = DirectCast(sender, DropDownList)
        Dim rowItem As RepeaterItem = ddlBudgetGroup.Parent.Parent
        Dim ddlSubBudget As DropDownList = DirectCast(rowItem.FindControl("ddlSubBudget"), DropDownList)

        If ddlBudgetGroup.SelectedValue <> "" Then
            BL.Bind_DDL_SubBudget(ddlSubBudget, ddlBudgetGroup.SelectedValue)
        End If
    End Sub

    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand

        If e.CommandName = "Delete" Then

            Dim _id As String = e.CommandArgument
            Dim dt As DataTable = DeleteDataInRpt(rptList, _id)
            rptList.DataSource = dt
            rptList.DataBind()
            txtAmount_TextChanged(Nothing, Nothing)
        End If

    End Sub

    Function DeleteDataInRpt(rpt As Repeater, id As String) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("budget_year")
        dt.Columns.Add("budget_group_id")
        dt.Columns.Add("budget_sub_id")
        dt.Columns.Add("amount")

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlBudgetYear As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlBudgetYear"), DropDownList)
            Dim ddlBudgetGroup As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlBudgetGroup"), DropDownList)
            Dim ddlSubBudget As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlSubBudget"), DropDownList)
            Dim txtAmount As TextBox = DirectCast(rptList.Items(i).FindControl("txtAmount"), TextBox)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)

            If lblID.Text <> id Then
                dr = dt.NewRow
                dr("id") = dt.Rows.Count + 1
                dr("budget_year") = ddlBudgetYear.SelectedValue
                dr("budget_group_id") = ddlBudgetGroup.SelectedValue
                dr("budget_sub_id") = ddlSubBudget.SelectedValue
                If txtAmount.Text = "" Then
                    dr("amount") = 0.0
                Else
                    dr("amount") = txtAmount.Text
                End If

                dt.Rows.Add(dr)
            End If
        Next

        Return dt
    End Function
End Class
