﻿Imports System.Data
Partial Class usercontrol_UCProjectComponent
    Inherits System.Web.UI.UserControl
    Dim BL As New ODAENG

    Public Event EditBudget()

    Dim _ComponentDT As DataTable
    Public Property ComponentDT As DataTable
        Get
            Return GetDataFromRpt(rptList)
        End Get
        Set(value As DataTable)
            _ComponentDT = value
            rptList.DataSource = _ComponentDT
            rptList.DataBind()
            txtBudget_TextChanged(Nothing, Nothing)

        End Set
    End Property

    Public ReadOnly Property TotalBudget As Decimal
        Get
            Return CDec(lblTotal.Text)
        End Get
    End Property

    Public Sub SetToViewMode(IsEnable As Boolean)
        btnAdd.Enabled = Not IsEnable
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlComponent As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlComponent"), DropDownList)
            Dim txtBudget As TextBox = DirectCast(rptList.Items(i).FindControl("txtBudget"), TextBox)
            Dim btnDelete As Button = DirectCast(rptList.Items(i).FindControl("btnDelete"), Button)

            ddlComponent.Enabled = Not IsEnable
            txtBudget.Enabled = Not IsEnable
            btnDelete.Enabled = Not IsEnable
        Next
    End Sub



    Protected Sub txtBudget_TextChanged(sender As Object, e As EventArgs)
        Dim _total As Decimal = 0.0
        For i As Integer = 0 To rptList.Items.Count - 1
            Try
                Dim txtBudget As TextBox = DirectCast(rptList.Items(i).FindControl("txtBudget"), TextBox)
                _total += CDec(txtBudget.Text)
            Catch ex As Exception
            End Try
        Next

        lblTotal.Text = _total.ToString("#,##0.00")

        RaiseEvent EditBudget()
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        Dim dt As DataTable = GetDataFromRpt(rptList)
        Dim dr As DataRow
        dr = dt.NewRow
        dr("id") = dt.Rows.Count + 1
        dr("component_id") = ""
        dr("amount") = 0.0
        dt.Rows.Add(dr)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Function GetDataFromRpt(rpt As Repeater) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("component_id")
        dt.Columns.Add("amount", GetType(Decimal))

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlComponent As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlComponent"), DropDownList)
            Dim txtBudget As TextBox = DirectCast(rptList.Items(i).FindControl("txtBudget"), TextBox)

            dr = dt.NewRow
            dr("id") = dt.Rows.Count + 1
            dr("component_id") = ddlComponent.SelectedValue
            If txtBudget.Text = "" Then
                dr("amount") = 0.0
            Else
                dr("amount") = txtBudget.Text
            End If

            dt.Rows.Add(dr)

        Next

        Return dt
    End Function

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim ddlComponent As DropDownList = DirectCast(e.Item.FindControl("ddlComponent"), DropDownList)
        Dim txtBudget As TextBox = DirectCast(e.Item.FindControl("txtBudget"), TextBox)
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)

        BL.Bind_DDL_Component(ddlComponent)
        BL.SetTextDblKeypress(txtBudget)

        lblID.Text = e.Item.DataItem("id").ToString
        ddlComponent.SelectedValue = e.Item.DataItem("component_id").ToString
        txtBudget.Text = Convert.ToDecimal(e.Item.DataItem("amount")).ToString("#,##0.00")


    End Sub
    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand

        If e.CommandName = "Delete" Then

            Dim _id As String = e.CommandArgument
            Dim dt As DataTable = DeleteDataInRpt(rptList, _id)
            rptList.DataSource = dt
            rptList.DataBind()

            txtBudget_TextChanged(Nothing, Nothing)
        End If

    End Sub

    Function DeleteDataInRpt(rpt As Repeater, id As String) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("component_id")
        dt.Columns.Add("amount")

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlComponent As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlComponent"), DropDownList)
            Dim txtBudget As TextBox = DirectCast(rptList.Items(i).FindControl("txtBudget"), TextBox)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)

            If lblID.Text <> id Then
                dr = dt.NewRow
                dr("id") = dt.Rows.Count + 1
                dr("component_id") = ddlComponent.SelectedValue
                If txtBudget.Text = "" Then
                    dr("amount") = 0.0
                Else
                    dr("amount") = txtBudget.Text
                End If

                dt.Rows.Add(dr)
            End If
        Next

        Return dt
    End Function
End Class
