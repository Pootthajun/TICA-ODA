﻿Imports System.Data
Partial Class usercontrol_UCContact
    Inherits System.Web.UI.UserControl
    Dim BL As New ODAENG
    Dim _ContactDT As New DataTable
    Public Property ContactDT As DataTable
        Get
            Return GetContactDT()
        End Get
        Set(value As DataTable)
            _ContactDT = value
            SetContact(_ContactDT)
        End Set
    End Property



    Private Sub usercontrol_UCContact_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            'ClearForm()
            'BL.SetTextIntKeypress(txtContactPhone)
            'BL.SetTextIntKeypress(txtContactFax)
        End If
    End Sub

    Public Sub ClearForm()
        txtContactName.Text = ""
        txtContactPosition.Text = ""
        txtContactPhone.Text = ""
        txtContactFax.Text = ""
        txtContactEmail.Text = ""
    End Sub

    Public Sub SetToViewMode(IsEnable As Boolean)
        txtContactName.Enabled = Not IsEnable
        txtContactPosition.Enabled = Not IsEnable
        txtContactPhone.Enabled = Not IsEnable
        txtContactFax.Enabled = Not IsEnable
        txtContactEmail.Enabled = Not IsEnable
    End Sub

    Private Function GetContactDT() As DataTable
        Dim dt As New DataTable
        With dt
            ', address, telephon, fax, website, description, email, 
            .Columns.Add("contact_name")
            .Columns.Add("position")
            .Columns.Add("telephone")
            .Columns.Add("fax")
            .Columns.Add("email")
        End With

        Dim dr As DataRow
        dr = dt.NewRow
        dr("contact_name") = txtContactName.Text.Trim
        dr("position") = txtContactPosition.Text.Trim
        dr("telephone") = txtContactPhone.Text.Trim
        dr("fax") = txtContactFax.Text.Trim
        dr("email") = txtContactEmail.Text.Trim
        dt.Rows.Add(dr)

        Return dt

    End Function

    Private Sub SetContact(dt As DataTable)
        ClearForm()
        If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
            Dim dr As DataRow = dt.Rows(0)
            txtContactName.Text = dr("contact_name").ToString()
            txtContactPosition.Text = dr("position").ToString()
            txtContactPhone.Text = dr("telephone").ToString()
            txtContactFax.Text = dr("fax").ToString()
            txtContactEmail.Text = dr("email").ToString()
        End If
    End Sub
End Class
