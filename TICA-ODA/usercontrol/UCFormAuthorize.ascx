﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCFormAuthorize.ascx.vb" Inherits="usercontrol_UCFormAuthorize" %>

<asp:UpdatePanel ID="udp1" runat="server">
    <ContentTemplate>
        <section class="content" style="padding-top:0px;">
            <!-- Horizontal Form -->
            <div class="box">

                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr class="bg-gray">

                            <th style="text-align: center">Menu</th>
                            <th style="width: 150px; text-align: center">Edit/Delete
                            </th>
                            <th style="width: 150px; text-align: center">View</th>
                            <th style="width: 150px; text-align: center">N/A</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr class="trHead">
                            <td>
                                <span>Main Menu</span>
                            </td>
                            <td></td>
                            <td></td>
                            <td><asp:Label ID="lblNodeID" runat="server" Visible="false"></asp:Label></td>
                        </tr>
                        <asp:Repeater ID="rptList" runat="server">
                            <ItemTemplate>
                                <tr id="trHead" runat="server" class="">
                                    <td>
                                        <asp:Label ID="lblMenuName" runat="server" Style="text-align: left" placeholder=""></asp:Label>
                                        <asp:Label ID="lblMenuID" runat="server" Visible="false"></asp:Label>
                                        <asp:Label ID="lblMenuType" runat="server" Visible="false"></asp:Label>
                                        
                                    </td>
                                    <%--Readio Part--%>
                                    <td data-title="Update/Delete">
                                        <center>
                                            <asp:RadioButton id ="rdUpdate" runat="server" groupname="a"></asp:RadioButton>
                                            <label for="rdUpdate" class="label"></label>
                                        </center>
                                    </td>
                                    <td data-title="View">
                                        <center>
                                            <asp:RadioButton id ="rdView" runat="server" groupname="a"></asp:RadioButton>
                                            <label for="rdView" class="label"></label>
                                        </center>
                                    </td>
                                    <td data-title="N/A">
                                        <center>
                                            <asp:RadioButton id ="rdNa" runat="server" groupname="a"></asp:RadioButton>
                                            <label for="rdNa" class="label"></label>
                                        </center>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </section>
    </ContentTemplate>
</asp:UpdatePanel>