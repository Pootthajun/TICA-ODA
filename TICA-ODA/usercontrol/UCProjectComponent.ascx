﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCProjectComponent.ascx.vb" Inherits="usercontrol_UCProjectComponent" %>

<div class="box-body">
    <table id="example2" class="table table-bordered table-hover">
        <thead>
            <tr class="bg-gray">
                <th>Component (ประเภทความช่วยเหลือ)</th>
                <th>Budget (บาท)</th>
                <th style="width: 50px">Delete(ลบ)</th>
            </tr>
        </thead>
        <tbody>
            <asp:Repeater ID="rptList" runat="server">
                <ItemTemplate>
                    <tr>
                        <td data-title="Component (ประเภทความช่วยเหลือ)">
                             <asp:DropDownList ID="ddlComponent" runat="server" CssClass="form-control select2" Style="width: 100%">
                             </asp:DropDownList>
                             <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                        </td>
                        <td data-title="Budget (บาท)" id="td" runat="server">
                           <asp:TextBox  ID="txtBudget" runat="server" CssClass="form-control" placeholder="" style="text-align: right" AutoPostBack="true" OnTextChanged="txtBudget_TextChanged" MaxLength="12" ></asp:TextBox>
                        </td>
                        <td data-title="Delete" id="ColDelete" runat="server">
                            <center>
                                <asp:Button ID="btnDelete" runat="server" Text="ลบ" CssClass="btn btn-bricky"  CommandArgument='<%# Eval("id") %>' CommandName="Delete" OnClientClick="return confirm('ท่านต้องการลบรายการนี้หรือไม่?');"/>
                            </center>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </tbody>

    </table>
     <div class="row">
      <div class="col-sm-5 cost">
      <b>รวม</b>
      </div>
       <div class="col-sm-6 cost cost-uc">
        <b><asp:Label ID="lblTotal" runat="server" Text="0"></asp:Label></b>
      </div>
     </div>

    <div class="row">
        <center>
            <div class="col-sm-12">
                <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" Text="เพิ่ม"
                    CssClass="btn btn-primary" />
            </div>
        </center>
    </div>
</div>
