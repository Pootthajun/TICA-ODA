﻿Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE

Public Class frmEditUserLevel
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    Public Property _node_id As Long
        Get
            Try
                Return ViewState("EditNodeID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("EditNodeID") = value
        End Set
    End Property

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuStructure")
        li.Attributes.Add("class", "active")

        _node_id = Request.QueryString("NODE_ID")
        If IsPostBack = False Then
            UCFormAuthorize.BindMenu(_node_id)
        End If

    End Sub

    Private Sub btnCancle_Click(sender As Object, e As EventArgs) Handles btnCancle.Click
        Response.Redirect("frmStructure.aspx")
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If UCFormAuthorize.SaveAuthorize(UserName).IsSuccess = True Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว'); window.location ='frmStructure.aspx';", True)
        End If
    End Sub
End Class