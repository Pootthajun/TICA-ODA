﻿Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Partial Class reports_frmPreviewReport
    Inherits System.Web.UI.Page
    Dim PrintDate As String = ""
    Dim Username As String = ""
    Dim Reportname As String = ""
    Dim ReportFormat As String = ""
    Dim ReportPath As String = ""

    Dim BL As New ODAENG
    Private Sub reports_frmPreviewReport_Load(sender As Object, e As EventArgs) Handles Me.Load
        PrintDate = BL.GetTHDateAbbr(DateTime.Now.ToString("yyyyMMdd", New System.Globalization.CultureInfo("en-US"))) + " " + DateTime.Now.ToString("HH:mm", New System.Globalization.CultureInfo("en-US"))
        Username = Session("Username")
        Reportname = Request.QueryString("Reportname")
        ReportFormat = Request.QueryString("ReportFormat")
        'Dim home_floder As String = System.Web.HttpContext.Current.Request.ApplicationPath + "/"
        ReportPath = Server.MapPath("~/reports/" + Reportname + ".rpt")

        ''** for test **
        'Reportname = "CrystalReport" '"rptIncommingBudget"
        'ReportFormat = "PDF"
        'ReportPath = "C:\webroot\ODA\reports\rptProjectDetail.rpt"
        PrintReport(Reportname, ReportFormat, ReportPath)
    End Sub

    Private Sub PrintReport(reportname As String, ReportFormat As String, ReportPath As String)
        Dim rpt As ReportDocument = New ReportDocument()
        Dim notfoundmsg As String = "ไม่พบข้อมูล"
        Select Case reportname
            Case "rptIncommingBudget"
                rptIncommingBudget(rpt, ReportFormat, ReportPath, notfoundmsg)

            Case "rptProjectExpense"
                rptProjectExpense(rpt, ReportFormat, ReportPath, notfoundmsg)

            Case "rptProjectDetail"
                rptProjectDetail(rpt, ReportFormat, ReportPath, notfoundmsg)

            Case "rptLoanDetail"
                rptLoanDetail(rpt, ReportFormat, ReportPath, notfoundmsg)
            Case "CrystalReport"
                rptTest(rpt, ReportFormat, ReportPath, notfoundmsg)
            Case Else
                lblErrorMessage.Text = notfoundmsg
                CrystalReportViewer1.Visible = False
        End Select
    End Sub

#Region "SubReport"

    Sub rptTest(_rpt As ReportDocument, _ReportFormat As String, _ReportPath As String, _notfoundmsg As String)
        Dim dt As New DataTable


        ''** for test **
        'Dim _textsearch As String = ""
        'Dim _budget_year As String = ""

        dt = BL.GetList_OECD(0, "")
        If dt.Rows.Count > 0 Then
            lblErrorMessage.Text = ""

            _rpt.Load(_ReportPath)
            _rpt.SetDataSource(dt)

            If _ReportFormat = "EXCEL" Then
                CreateExcelReport(_rpt, Reportname)
            End If
            If ReportFormat = "PDF" Then
                CreatePDFReport(_rpt, Reportname)
            End If
        Else
            lblErrorMessage.Text = _notfoundmsg
            CrystalReportViewer1.Visible = False
        End If
    End Sub

    Sub rptIncommingBudget(_rpt As ReportDocument, _ReportFormat As String, _ReportPath As String, _notfoundmsg As String)
        Dim dt As New DataTable
        Dim _textsearch As String = Request.QueryString("TextSearch").ToString
        Dim _budget_year As String = Request.QueryString("BudgetYear").ToString
        Dim _DateSearchS As String = Request.QueryString("DateSearchS").ToString
        Dim _DateSearchE As String = Request.QueryString("DateSearchE").ToString

        ''** for test **
        'Dim _textsearch As String = ""
        'Dim _budget_year As String = ""

        dt = BL.GetList_BudgetDetail(0, _textsearch, _budget_year, _DateSearchS, _DateSearchE)
        If dt.Rows.Count > 0 Then
            lblErrorMessage.Text = ""

            _rpt.Load(_ReportPath)
            _rpt.DataDefinition.FormulaFields("PrintDate").Text = "'" + PrintDate + "'"
            _rpt.DataDefinition.FormulaFields("BudgetYear").Text = "'" + IIf(_budget_year = "", "ทั้งหมด", _budget_year) + "'"
            _rpt.DataDefinition.FormulaFields("TextSearch").Text = "'" + IIf(_textsearch = "", "-", _textsearch) + "'"
            _rpt.SetDataSource(dt)

            If _ReportFormat = "EXCEL" Then
                CreateExcelReport(_rpt, Reportname)
            End If
            If _ReportFormat = "PDF" Then
                CreatePDFReport(_rpt, Reportname)
            End If
        Else
            lblErrorMessage.Text = _notfoundmsg
            CrystalReportViewer1.Visible = False
        End If
    End Sub

    Sub rptProjectExpense(_rpt As ReportDocument, _ReportFormat As String, _ReportPath As String, _notfoundmsg As String)
        Dim _project_type As String = Request.QueryString("ProjectType").ToString
        Dim _project_type_name As String = Request.QueryString("ProjectTypeName").ToString
        Dim _textsearch As String = Request.QueryString("TextSearch").ToString

        Dim dt As New DataTable
        dt = BL.GetList_Expense(0, _textsearch, _project_type)
        If dt.Rows.Count > 0 Then
            lblErrorMessage.Text = ""

            _rpt.Load(_ReportPath)
            _rpt.DataDefinition.FormulaFields("PrintDate").Text = "'" + PrintDate + "'"

            Dim _pjTypeName As String = _project_type


            _rpt.DataDefinition.FormulaFields("ProjectType").Text = "'" + _project_type_name + "'"
            _rpt.DataDefinition.FormulaFields("TextSearch").Text = "'" + IIf(_textsearch = "", "-", _textsearch) + "'"
            _rpt.SetDataSource(dt)

            If _ReportFormat = "EXCEL" Then
                CreateExcelReport(_rpt, Reportname)
            End If
            If _ReportFormat = "PDF" Then
                CreatePDFReport(_rpt, Reportname)
            End If
        Else
            lblErrorMessage.Text = _notfoundmsg
            CrystalReportViewer1.Visible = False
        End If
    End Sub

    Sub rptProjectDetail(_rpt As ReportDocument, _ReportFormat As String, _ReportPath As String, _notfoundmsg As String)
        Dim _ProjectID As String = Request.QueryString("ProjectID")
        Dim _ProjectType As String = Request.QueryString("ProjectTypeName")
        Dim dt As New DataTable
        dt = BL.GetProjectInfoByID(_ProjectID)

        If dt.Rows.Count > 0 Then
            Dim _assistant As String = dt.Rows(0)("assistant").ToString
            Dim _assistantname As String = ""
            Dim _dtas As New DataTable
            If _assistant <> "" Then
                Dim _strAssistant() As String = _assistant.Split(", ")
                _dtas = BL.GetAssistantByID(_assistant)
                For i As Integer = 0 To _dtas.Rows.Count - 1
                    _assistantname &= _dtas.Rows(i)("name_th")
                    If i < (_dtas.Rows.Count - 1) Then _assistantname &= ","
                Next
            End If

            lblErrorMessage.Text = ""

            Dim _dtfile As New DataTable
            _dtfile = BL.GetProjectFile(_ProjectID)

            Dim _dtact As New DataTable
            _dtact = BL.GetAllActivityByProject(_ProjectID)

            Dim _dtImp As New DataTable
            _dtImp = BL.GetProjectImplementingAgency(_ProjectID)

            Dim _dtcof As New DataTable
            _dtcof = BL.GetProjectCoFunding(_ProjectID)

            Dim _dtrec As New DataTable
            _dtrec = BL.GetProjectRecipincePerson(_ProjectID)

            _rpt.Load(_ReportPath)
            _rpt.DataDefinition.FormulaFields("PrintDate").Text = "'" + PrintDate + "'"
            _rpt.DataDefinition.FormulaFields("ProjectType").Text = "'" + _ProjectType + "'"
            _rpt.DataDefinition.FormulaFields("Assistant").Text = "'" + _assistantname + "'"
            _rpt.Subreports("SUB_ImplementingAgency").SetDataSource(_dtImp)
            _rpt.Subreports("SUB_CoFunding").SetDataSource(_dtcof)
            _rpt.Subreports("SUB_File").SetDataSource(_dtfile)
            _rpt.Subreports("SUB_Activity").SetDataSource(_dtact)
            _rpt.Subreports("SUB_Recipience").SetDataSource(_dtrec)
            _rpt.SetDataSource(dt)

            If _ReportFormat = "EXCEL" Then
                CreateExcelReport(_rpt, Reportname)
            End If
            If _ReportFormat = "PDF" Then
                CreatePDFReport(_rpt, Reportname)
            End If
        Else
            lblErrorMessage.Text = _notfoundmsg
            CrystalReportViewer1.Visible = False
        End If
    End Sub

    Sub rptLoanDetail(_rpt As ReportDocument, _ReportFormat As String, _ReportPath As String, _notfoundmsg As String)
        Dim _ProjectID As String = Request.QueryString("ProjectID")
        Dim _ProjectType As String = Request.QueryString("ProjectTypeName")
        Dim dt As New DataTable
        dt = BL.GetProjectInfoByID(_ProjectID)
        If dt.Rows.Count > 0 Then
            Dim _assistant As String = dt.Rows(0)("assistant").ToString
            Dim _assistantname As String = ""
            Dim _dtas As New DataTable
            If _assistant <> "" Then
                Dim _strAssistant() As String = _assistant.Split(", ")
                _dtas = BL.GetAssistantByID(_assistant)
                For i As Integer = 0 To _dtas.Rows.Count - 1
                    _assistantname &= _dtas.Rows(i)("name_th")
                    If i < (_dtas.Rows.Count - 1) Then _assistantname &= ","
                Next
            End If

            lblErrorMessage.Text = ""

            Dim _dtfile As New DataTable
            _dtfile = BL.GetProjectFile(_ProjectID)

            Dim _dtrec As New DataTable
            _dtrec = BL.GetProjectRecipincePerson(_ProjectID)

            Dim _dtact As New DataTable
            _dtact = BL.GetAllActivityByProject(_ProjectID)

            _rpt.Load(_ReportPath)
            _rpt.DataDefinition.FormulaFields("PrintDate").Text = "'" + PrintDate + "'"
            _rpt.DataDefinition.FormulaFields("ProjectType").Text = "'" + _ProjectType + "'"
            _rpt.DataDefinition.FormulaFields("Assistant").Text = "'" + _assistantname + "'"
            _rpt.Subreports("SUB_File").SetDataSource(_dtfile)
            _rpt.Subreports("SUB_Activity").SetDataSource(_dtact)
            _rpt.Subreports("SUB_Recipience").SetDataSource(_dtrec)
            _rpt.SetDataSource(dt)

            If _ReportFormat = "EXCEL" Then
                CreateExcelReport(_rpt, Reportname)
            End If
            If _ReportFormat = "PDF" Then
                CreatePDFReport(_rpt, Reportname)
            End If
        Else
            lblErrorMessage.Text = _notfoundmsg
            CrystalReportViewer1.Visible = False
        End If
    End Sub

#End Region

    Private Sub CreateExcelReport(rpt As ReportDocument, ReportName As String)
        Try
            rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, True, ReportName)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub CreatePDFReport(rpt As ReportDocument, ReportName As String)
        Dim oStream As System.IO.Stream = Nothing
        oStream = rpt.ExportToStream(ExportFormatType.PortableDocFormat)
        Dim byteArray(oStream.Length) As Byte
        oStream.Read(byteArray, 0, Convert.ToInt32(oStream.Length - 1))
        Response.ClearContent()
        Response.ClearHeaders()
        Response.ContentType = "application/pdf"
        Response.AddHeader("Content-Disposition", "inline; filename=" + ReportName + ".pdf")
        Response.BinaryWrite(byteArray)
        Response.Flush()
        Response.Close()
        rpt.Close()
        rpt.Dispose()
    End Sub

End Class
