﻿Imports System.Data
Imports System.Data.SqlClient
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE

Public Class frmUserInformation
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then

            BindData()

        End If
    End Sub

    Protected Sub btnEdit_Click(sender As Object, e As EventArgs)

        Response.Redirect("frmEditAgencyInformation.aspx")

    End Sub
    Protected Sub BindData()
        Dim sql As String = "SELECT id,govnamethai,govnameeng,kong,govnumber,govroad,tambol,ampher,province,postcode" & vbLf
        sql &= ",telephone,fax,website,detail,ministry,email,ACTIVE_STATUS" & vbLf
        sql &= " FROM web_contactgoverment WHERE id=1" & vbLf

        Dim BL As New ODAENG
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(sql, BL.ConnectionString)
        DA.Fill(DT)

        For i As Integer = 0 To DT.Rows.Count - 1
            If DT.Rows(i)("id").ToString() <> "" Then
                lblGOVNameTH.Text = DT(i)("govnamethai").ToString()
                lblGOVNameEN.Text = DT(i)("govnameeng").ToString()
                lblDivision.Text = DT(i)("kong").ToString()
                lblMinistry.Text = DT(i)("ministry").ToString()

                lblLocation.Text = DT(i)("govnumber").ToString() & " ถนน " & DT(i)("govroad").ToString()
                lblLocation.Text += " แขวง " & DT(i)("tambol").ToString() & " เขต " & DT(i)("ampher").ToString()
                lblLocation.Text += " จังหวัด " & DT(i)("province").ToString() & " " & DT(i)("postcode").ToString()

                lblTelephone.Text = DT(i)("telephone").ToString()
                lblFax.Text = DT(i)("fax").ToString()

                lblWebSite.Text = DT(i)("website").ToString()
                lblEmail.Text = DT(i)("email").ToString()
                lblDivision.Text = DT(i)("kong").ToString()

                lblMinistry.Text = DT(i)("ministry").ToString()
                lblNote.Text = DT(i)("detail").ToString()

            End If
            Session("SESSION_ID_GOVCONTACT") = DT.Rows(i)("id").ToString()
        Next


    End Sub
End Class