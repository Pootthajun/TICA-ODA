﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmActivityExpense.aspx.vb" Inherits="frmActivityExpense" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%--<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>--%>
<%@ Register Src="~/usercontrol/UCActivityExpensePlan.ascx" TagName="UCActivityExpensePlan" TagPrefix="uc1" %>
<%@ Register Src="~/usercontrol/UCActivityExpenseActual.ascx" TagName="UCActivityExpenseActual" TagPrefix="uc2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <link rel="shortcut icon" href="assets/img/logo.ico" />
    <title></title>
    <style type="text/css">
        table {
            border-collapse: separate;
        }

        .tr-top {
            height: 45px;
            background-color: lightblue;
            border-color: black;
        }

        .tb-fix {
            width: 100%;
            overflow-x: scroll;
            margin-left: 1px;
            overflow-y: visible;
            position: relative;
        }

        .tb-fix-col-1 {
            position: fixed;
            width: 75px;
            left: 0;
            top: auto;
        }

        .tb-fix-col-2 {
            position: fixed;
            width: 170px;
            left: 75px;
            top: auto;
        }

        .tbExpense tr th {
            text-align: center;
        }
    </style>
    <script type="text/javascript" src="Scripts/txtClientControl.js"></script>
</head>
<body>
    <br />
    <form id="form1" runat="server">
        <div class="content-wrapper" style="margin-left: 0px;">

            <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release" EnableScriptLocalization="true" EnableScriptGlobalization="true"></asp:ScriptManager>
            <asp:UpdatePanel ID="udpList" runat="server">
                <ContentTemplate>
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white">
                                        <asp:Label ID="lblexpenseid" runat="server" Text="0" Visible="false"></asp:Label>
                                        <table class="table" style="margin-bottom: unset; position: fixed; background-color: white; border-bottom: 1px solid #cccccc; z-index: 100;">
                                            <tbody>
                                                <tr class="bg-light-blue-gradient">
                                                    <th colspan="2">
                                                        <h2 style="margin-top:0;">Expense (ค่าใช้จ่าย) : </h2>
                                                        <h4 style="color: white"><b>
                                                            <asp:Label ID="txtActivitytName" runat="server"></asp:Label>
                                                            <asp:Label ID="txtProjectName" runat="server"></asp:Label>
                                                        </b></h4>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td>Plan start (กำหนดแผนการจ่าย) :<span style="color: red">*</span>

                                                        <asp:TextBox CssClass="form-control m-b" ID="txtPaymentStartDate" Style="display: unset; width: 100px;" runat="server"></asp:TextBox>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                            Format="dd/MM/yyyy" TargetControlID="txtPaymentStartDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                        To (ถึง) :<span style="color: red">*</span>


                                                        <asp:TextBox CssClass="form-control m-b" ID="txtPaymentEndDate" Style="display: unset; width: 100px;" runat="server"></asp:TextBox>

                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                            Format="dd/MM/yyyy" TargetControlID="txtPaymentEndDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                        <asp:Button ID="btnLoad" runat="server" ClientIDMode="Static" Text="Test" Style="display: none;" />
                                                    </td>
                                                    <td style="text-align:right; padding-right:10px;">
                                                         <asp:LinkButton ID="btnSave" runat="server" CssClass="btn  btn-social btn-success">
                                                           <i class="fa fa-save"></i> Save
                                                         </asp:LinkButton>
                                                         <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn  btn-social btn-google">
                                                            <i class="fa fa-reply"></i> Cancel
                                                         </asp:LinkButton>

                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </asp:Panel>
                                    <div id="topmargin"></div>
                                    <asp:Panel runat="server" ID="PanelList" Visible="false">
                                        <table id="lp" class="table" style="border-collapse: initial; table-layout: fixed;">
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr class="tr-top">
                                                            <td style="width: 40%; border-left: none; border-right: none">
                                                                <div style="font-size: medium; color: #003d99">
                                                                    <b>
                                                                        <img id="imgicon" runat="server" src="" />
                                                                        <asp:Label ID="lblName" runat="server"></asp:Label></b>
                                                                </div>
                                                            </td>
                                                            <td style="width: 20%; border-left: none; border-right: none;">
                                                                <div style="cursor: pointer;">
                                                                    <asp:LinkButton ID="btnEditBudget" runat="server" CommandArgument='<%# Eval("recipient_id") %>' CommandName="budget">
                                                                        <asp:Label ID="lblbudget" runat="server" Text="งบประมาณ 3000 บาท"></asp:Label>
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </td>
                                                            <td style="width: 20%; border-left: none; border-right: none">
                                                                <div style="cursor: pointer;">
                                                                    <asp:LinkButton ID="btnEditExpense" runat="server" CommandArgument='<%# Eval("recipient_id") %>' CommandName="expense">
                                                                        <asp:Label ID="lblExpense" runat="server"></asp:Label>
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </td>
                                                            <td style="width: 20%; border-left: none; border-right: none">
                                                                <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <asp:Panel ID="PanelBudget" runat="server" Visible="false">
                                                            <tr id="trid1" runat="server" visible="true">
                                                                <td colspan="4">
                                                                    <uc1:UCActivityExpensePlan runat="server" ID="UCActivityExpensePlan" />
                                                                </td>
                                                            </tr>
                                                        </asp:Panel>
                                                        <asp:Panel ID="PanelExpense" runat="server" Visible="false">
                                                            <tr id="tr1" runat="server" visible="true">
                                                                <td colspan="4">
                                                                    <uc2:UCActivityExpenseActual runat="server" ID="UCActivityExpenseActual" />
                                                                </td>
                                                            </tr>

                                                        </asp:Panel>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <tr>
                                                    <td style="text-align: right;">รวม</td>
                                                    <td style="text-align: left;">
                                                        <asp:Label ID="lblSumbudget" runat="server"></asp:Label>
                                                        บาท</td>
                                                    <td style="text-align: left;">
                                                        <asp:Label ID="lblSumexpense" runat="server"></asp:Label>
                                                        บาท</td>
                                                    <td style="text-align: left;">
                                                        <asp:Label ID="lblSumTotal" runat="server"></asp:Label>
                                                        บาท</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="PanelListGroup">
                                        <table class="table table-bordered " style="border-collapse: initial; table-layout: fixed;">
                                            <tbody>
                                                <tr class="tr-top">
                                                    <td style="border-left: none; border-right: none" colspan="2">
                                                        <div style="font-size: medium; color: #003d99">
                                                            <b>
                                                                <asp:Label ID="NameBG" runat="server" Text="งบประมาณรวม"></asp:Label></b>
                                                        </div>
                                                    </td>
                                                    <td style="border-left: none; border-right: none; text-align: left;" colspan="2">
                                                        <div style="cursor: pointer;">
                                                            <asp:LinkButton ID="btnBudgetGroup" runat="server">
                                                                <asp:Label ID="lblBudgedGroup" runat="server" Text="หลายบาทอยู่"></asp:Label>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <asp:Panel ID="PanelBudgetGroup" runat="server" Visible="false">
                                                    <tr id="trid1" runat="server" visible="true">
                                                        <td colspan="4">
                                                            <uc1:UCActivityExpensePlan runat="server" ID="UCActivityExpensePlan" />
                                                        </td>
                                                    </tr>
                                                </asp:Panel>
                                                <asp:Repeater ID="rptGroup" runat="server">
                                                    <ItemTemplate>
                                                        <tr class="tr-top">
                                                            <td style="border-left: none; border-right: none" colspan="2">
                                                                <div style="font-size: medium; color: #003d99">
                                                                    <b>
                                                                        <img id="imgicon" runat="server" src="" />
                                                                        <asp:Label ID="lblNameGroup" runat="server"></asp:Label></b>
                                                                </div>
                                                            </td>
                                                            <td style="border-left: none; border-right: none" colspan="2">
                                                                <div style="cursor: pointer;">
                                                                    <asp:LinkButton ID="btnEditExpenseGroup" runat="server" CommandArgument='<%# Eval("recipient_id") %>' CommandName="expense">
                                                                        <asp:Label ID="lblExpenseGroup" runat="server"></asp:Label>
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <asp:Panel ID="PanelExpenseGroup" runat="server" Visible="false">
                                                            <tr id="tr1" runat="server" visible="true">
                                                                <td colspan="4">
                                                                    <uc2:UCActivityExpenseActual runat="server" ID="UCActivityExpenseActual" />
                                                                </td>
                                                            </tr>

                                                        </asp:Panel>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <tr>
                                                    <td style="text-align: right;" colspan="2">รวม</td>
                                                    <td style="text-align: left;" colspan="2">
                                                        <asp:Label ID="lblSumGroup" runat="server"></asp:Label>
                                                        บาท</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                        </div>

                    </section>

                </ContentTemplate>
            </asp:UpdatePanel>            
        </div>
    </form>
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            Tang();
            $('#btnLoad').click();
        });

        function Tang() {
            var a = $('#example2').height();
            $('#topmargin').height(a);
        }
    </script>
</body>
</html>
