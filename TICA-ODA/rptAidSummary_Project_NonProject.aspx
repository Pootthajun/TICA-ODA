﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="rptAidSummary_Project_NonProject.aspx.vb" Inherits="rptAidSummary_Project_NonProject" %>

<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <title>Search | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
 
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>การให้ความช่วยเหลือแก่ต่างประเทศ  </h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>Report</a></li>
                <li class="active">การให้ความช่วยเหลือแก่ต่างประเทศ</li>
            </ol>
        </section>
        <br />


        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlList" runat="server" DefaultButton="btnSearch">
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                   

                                    <div class="box-header">
                                        <div class="col-sm-12" style="text-align: right;margin-bottom:10px;">
                                            <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                                <button type="button" class="btn btn-success btn-flat dropdown-toggle btn-print" data-toggle="dropdown">
                                                    <i class="fa fa-print"></i>
                                                </button>
                                                <ul class="dropdown-menu pull-right">
                                                    
                                                    <li><asp:LinkButton ID="btnPDF" runat="server"><i class="fa fa-file-pdf-o text-red"></i>รูปแบบ PDF</asp:LinkButton></li>
											        <li><asp:LinkButton ID="btnExcel" runat="server"><i class="fa fa-file-excel-o text-green"></i>รูปแบบ Excel</asp:LinkButton></li>	

                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-top:30px;"></div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">Budget Year :</label>
                                                 <div class="col-sm-9">
                                                     <asp:DropDownList ID="ddlBudgetYear" runat="server" CssClass="form-control select2" Style="width: 50%">
                                                    </asp:DropDownList>

                                                 </div>
                                             </div>
                                             <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">    Country :</label>
                                                 <div class="col-sm-9">
                                                     <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control select2" Style="width: 50%">
                                                     </asp:DropDownList>

                                                 </div>
                                             </div>
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">    Region :</label>
                                                 <div class="col-sm-9">
                                                     <asp:DropDownList ID="ddlRegion" runat="server" AutoPostBack="true"  CssClass="form-control select2" Style="width: 50%">
                                                     </asp:DropDownList>

                                                 </div>
                                             </div>
                                             <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">    Country Zone :</label>
                                                 <div class="col-sm-9">
                                                     <asp:DropDownList ID="ddlRegionzone" runat="server" CssClass="form-control select2" Style="width: 50%">
                                                     </asp:DropDownList>

                                                 </div>
                                             </div>
                                        </div>
                                        

                                        <div class="row" style="margin-top:10px;">
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                 <div class="col-sm-9 ">

                                                 </div>
                                                </div>
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                 <div class="col-sm-9 ">
                                                   <%--<p class="pull-right" style="margin-right:60px;">--%>
                                                     <p class="pull-right" style="margin-right:10px;">
                                                        <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn bg-blue margin-r-5 btn-social"   >
                                                            <i class="fa fa-search"></i>
                                                            <asp:Label ID="Search" runat="server" Text="Search"  ></asp:Label> 
                                                        </asp:LinkButton>
                                                    </p>

                                                 </div>
                                                </div>

                                        </div>
                                        <div class="row"></div>
                                        <div class="row pull-left">

                                            <div class="col-md-12">
                                            <h4 class="text-primary" style="margin-left :20px;"><asp:Label ID="lblTotalRecord" runat="server" Text=""></asp:Label></h4>


                                        </div>
                                        </div>
                                         <div class="row pull-right">
                                            <div class="col-md-12" style="margin-right: 30px;">

                                            <h5  style="margin-left :20px;">Unit : THB</h5>
                                        
                                        </div> 
                                    </div> 
                                        </div>   
                                     <div style="width: 1080px;overflow-y: auto;">
                                    <div class="box-body">
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th rowspan ="2" style ="vertical-align :middle ;">ประเทศ</th>
                                                    <th colspan ="7">Project<br/>(ภายใต้โครงการ)</th>
                                                    <th colspan ="7">Non Project<br/>(นอกโครงการ)</th>
                                                    <th rowspan ="2" style ="vertical-align :middle ;">มูลค่ารวมทั้งสิ้น</th>
                                                </tr>
                                                <tr class="bg-gray">
                                                    <th>Bachelor<br/>(ทุนศึกษา)</th>
                                                    <th>Training<br/>(ทุนฝึกอบรม)</th>
                                                    <th>Expert<br/>(ผู้เชี่ยวชาญ)</th>
                                                    <th>Volunteer<br/>(อาสาสมัคร)</th>
                                                    <th>Equipment<br/>(วัสดุอุปกรณ์)</th>
                                                    <th>Other<br/>(อื่นๆ)</th>
                                                    <th>Amount<br/>(มูลค่า)</th>

                                                    <th>Bachelor<br/>(ทุนศึกษา)</th>
                                                    <th>Training<br/>(ทุนฝึกอบรม)</th>
                                                    <th>Expert<br/>(ผู้เชี่ยวชาญ)</th>
                                                    <th>Volunteer<br/>(อาสาสมัคร)</th>
                                                    <th>Equipment<br/>(วัสดุอุปกรณ์)</th>
                                                    <th>Other<br/>(อื่นๆ)</th>
                                                    <th>Amount<br/>(มูลค่า)</th>
                                                </tr>




                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr id="trbudget_year" runat="server" style ="background-color :ivory;">
                                                            <td data-title="Budget year" colspan="16" style="text-align: center">
                                                             <b>  ปี  <asp:Label ID="lblbudget_year" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label></b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td data-title="Project" ><asp:Label ID="lblProject" runat="server"></asp:Label></td>
                                                            <td data-title="Bachelor" style="text-align :right ;" width="120;"><asp:Label ID="lblProject_Bachelor" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Training" style="text-align :right ;" width="120;"><asp:Label ID="lblProject_Training" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Expert" style="text-align :right ;" width="120;"><asp:Label ID="lblProject_Expert" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Volunteer" style="text-align :right ;" width="120;"><asp:Label ID="lblProject_Volunteer" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Equipment" style="text-align :right ;" width="120;"><asp:Label ID="lblProject_Equipment" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Other" style="text-align :right ;" width="120;"><asp:Label ID="lblProject_Other" runat="server" ForeColor="black"  Text="0.00"></asp:Label></td>
                                                            <td data-title="Amount" style="text-align :right ;" width="120;"><b><asp:Label ID="lblProject_Amount" runat="server" ForeColor="black"></asp:Label></b></td>


                                                            <td data-title="Bachelor" style="text-align :right ;" width="120;"><asp:Label ID="lblNonProject_Bachelor" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Training" style="text-align :right ;" width="120;"><asp:Label ID="lblNonProject_Training" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Expert" style="text-align :right ;" width="120;"><asp:Label ID="lblNonProject_Expert" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Volunteer" style="text-align :right ;" width="120;"><asp:Label ID="lblNonProject_Volunteer" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Equipment" style="text-align :right ;" width="120;"><asp:Label ID="lblNonProject_Equipment" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Other" style="text-align :right ;" width="120;"><asp:Label ID="lblNonProject_Other" runat="server" ForeColor="black"  Text="0.00"></asp:Label></td>
                                                            <td data-title="Amount" style="text-align :right ;" width="120;"><b><asp:Label ID="lblNonProject_Amout" runat="server" ForeColor="black"></asp:Label></b></td>
 
                                                            <td data-title="Amount" style="text-align :right ;text-decoration: underline;" width="120;"><b><asp:Label ID="lblTotal_Amount" runat="server" ForeColor="black"></asp:Label></b></td>

                                                       
                                                        </tr>

                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </tbody>
                                            <asp:Panel ID="pnlFooter" runat="server" Visible ="True">
                                            <tfoot style="background-color:LemonChiffon;" >
                                        <tr id="trFooter_Qty" runat="server" >
                                            <td  data-title="Total" style="text-align :center ;" ><b>Total</b></td>
                                            <td data-title="Bachelor" style="text-align :right ;text-decoration: underline;" width="120;"><b><asp:Label ID="lblProject_Bachelor" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Training" style="text-align :right ;text-decoration: underline;" width="120;"><b><asp:Label ID="lblProject_Training" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Expert" style="text-align :right ;text-decoration: underline;" width="120;"><b><asp:Label ID="lblProject_Expert" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Volunteer" style="text-align :right ;text-decoration: underline;" width="120;"><b><asp:Label ID="lblProject_Volunteer" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Equipment" style="text-align :right ;text-decoration: underline;" width="120;"><b><asp:Label ID="lblProject_Equipment" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Other" style="text-align :right ;text-decoration: underline;" width="120;"><b><asp:Label ID="lblProject_Other" runat="server" ForeColor="black"  Text="0.00"></asp:Label></b></td>
                                            <td data-title="Amount" style="text-align :right ;text-decoration: underline;" width="120;"><b><asp:Label ID="lblProject_Amount" runat="server" ForeColor="black"></asp:Label></b></td>


                                            <td data-title="Bachelor" style="text-align :right ;text-decoration: underline;" width="120;"><b><asp:Label ID="lblNonProject_Bachelor" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Training" style="text-align :right ;text-decoration: underline;" width="120;"><b><asp:Label ID="lblNonProject_Training" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Expert" style="text-align :right ;text-decoration: underline;" width="120;"><b><asp:Label ID="lblNonProject_Expert" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Volunteer" style="text-align :right ;text-decoration: underline;" width="120;"><b><asp:Label ID="lblNonProject_Volunteer" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Equipment" style="text-align :right ;text-decoration: underline;" width="120;"><b><asp:Label ID="lblNonProject_Equipment" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Other" style="text-align :right ;text-decoration: underline;" width="120;"><b><asp:Label ID="lblNonProject_Other" runat="server" ForeColor="black"  Text="0.00"></asp:Label></b></td>
                                            <td data-title="Amount" style="text-align :right ;text-decoration: underline;" width="120;"><b><asp:Label ID="lblNonProject_Amout" runat="server" ForeColor="black"></asp:Label></b></td>
 
                                            <td data-title="Amount" style="text-align :right ;text-decoration: underline;" width="120;"><b><asp:Label ID="lblTotal_Amount" runat="server" ForeColor="black"></asp:Label></b></td>



                                           <%-- <td  data-title="Total" style="text-align :center ;" ><b>Total</b></td>
                                            <td data-title="Bachelor"  style="text-align :right ;text-decoration: underline;"><b><asp:Label ID="lblBachelor_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Training"  style="text-align :right ;text-decoration: underline;"><b><asp:Label ID="lblTraining_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Expert"  style="text-align :right ;text-decoration: underline;"><b><asp:Label ID="lblExpert_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Volunteer"  style="text-align :right ;text-decoration: underline;"><b><asp:Label ID="lblVolunteer_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Equipment"  style="text-align :right ;text-decoration: underline;"><b><asp:Label ID="lblEquipment_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Other"  style="text-align :right ;text-decoration: underline;"><b><asp:Label ID="lblOther_Sum" runat="server" ForeColor="black" Text="0.00"></asp:Label></b></td>
                                            
                                                       
                                            <td data-title="Total Amount" style="text-align :right ;text-decoration: underline;" ><b><asp:Label ID="lblAmout_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                  
                                             <td data-title="Bachelor"  style="text-align :right ;text-decoration: underline;"><b><asp:Label ID="Label1" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Training"  style="text-align :right ;text-decoration: underline;"><b><asp:Label ID="Label2" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Expert"  style="text-align :right ;text-decoration: underline;"><b><asp:Label ID="Label3" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Volunteer"  style="text-align :right ;text-decoration: underline;"><b><asp:Label ID="Label4" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Equipment"  style="text-align :right ;text-decoration: underline;"><b><asp:Label ID="Label5" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Other"  style="text-align :right ;text-decoration: underline;"><b><asp:Label ID="Label6" runat="server" ForeColor="black" Text="0.00"></asp:Label></b></td>
                                            <td data-title="Total Amount" style="text-align :right ;text-decoration: underline;" ><b><asp:Label ID="Label7" runat="server" ForeColor="black"></asp:Label></b></td>

                                            <td data-title="Total Amount" style="text-align :right ;text-decoration: underline;" ><b><asp:Label ID="Label8" runat="server" ForeColor="black"></asp:Label></b></td>
                                          --%>
                                                

										</tr>
                                    

                                        </tfoot>
                                            </asp:Panel>
                                        </table>
                                    </div>
                                         </div>
                                    <!-- /.box-body -->

                                    <!-- Page Navigation -->
                                    <uc1:PageNavigation runat="server" ID="Pager" PageSize="10" />

                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->

                     <asp:TextBox ID="txtClickProjectID" runat="server" Text="" Style="display: none"></asp:TextBox>
                     <%--<asp:Button ID="btnView" OnClick="btnView_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnEdit" OnClick="btnEdit_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnDelete" OnClick="btnDelete_Click" runat="server" Style="display: none" />--%>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

    <%--<script type="text/javascript">

        function btnEditClick(activity_id) {
            //alert(activity_id);
            document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
            document.getElementById('<%= btnEdit.ClientID %>').click();
        }
        function btnViewClick(activity_id) {
            document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
            document.getElementById('<%= btnView.ClientID %>').click();
        }
        function btnDeleteClick(activity_id) {

            var c = confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?");
            if (c == true) {
                document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
                document.getElementById('<%= btnDelete.ClientID %>').click();
            }

        }
    </script>--%>


</asp:Content>



