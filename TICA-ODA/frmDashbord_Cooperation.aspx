﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="frmDashbord_Cooperation.aspx.vb" Inherits="frmDashbord_Cooperation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register assembly="System.Web.DataVisualization" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <title>Project | ODA</title>
    <style>
        .icon {
            display: inline;
            padding: 5px;
        }

            .icon li {
                margin-right: 10px;
            }

            .icon i {
                margin-right: 5px;
            }

        .poiter {
            cursor: pointer;
        }
    </style>

    <style> 
 #th_radius_1 {
    border-radius: 30px 30px 0px 0px;
    background: #d3d4d3;
    padding: 20px; 
    
    height: 30px; 
}   #th_radius_2 {
    border-radius: 30px 30px 0px 0px;
    background: #73AD21;
    padding: 20px; 
    
    height: 50px; 
}#th_radius_3 {
    border-radius: 30px 30px 0px 0px;
    background: #d3d4d3;
    padding: 20px; 
    
    height: 30px; 
}
</style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
   
            
<%--ภูมิภาค
	    กลุ่มประเทศ
		    ประเทศ
			    โครงการ
				    ผู้รับทุน--%>
                  

         <h4>
              <div class="row">
           <span style="float :left ;margin-left: 15px;" > มูลค่าความช่วยเหลือของหลักสูตร &nbsp;&nbsp;<asp:label id="lbl_Title_Collumn" runat="server"></asp:label> &nbsp;&nbsp; แบ่งตามภูมิภาค ในปีงบประมาณ &nbsp;</span>  <asp:DropDownList ID="ddl_Year" runat="server" CssClass="form-control select2" style="width :100px; float :left ;margin-top: -6px;margin-left: 10px;" >
                <asp:listitem value="1" text="2560"></asp:listitem>
                                        </asp:DropDownList> 

                
                   </div>

         </h4>


            <ol class="breadcrumb">
                <li class="active"><i class="fa fa-area-chart"></i>Overall</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">


            <div class="row">

                                <div class="col-md-12">
                    <!-- LINE CHART -->
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <%--<h3 class="box-title">Total Value of Thailand International Cooperation Program By Programme</h3>--%>
                      <%--      <b><h2 class="box-title"><asp:label id="lbl_Title_Collumn" runat="server"></asp:label></h2></b>--%>
                             
                           <div style ="float :left   ;">
                                            <div class="btn-group" data-toggle="tooltip" title="Back to Overall">
                                                <asp:LinkButton ID="btnBack" runat="server" CssClass=" btn btn-google btn-flat dropdown-toggle btn-print"  style="margin-top:2px;"><i class="fa fa-reply"></i> Back to Overall</asp:LinkButton>
                                            </div>
                                </div>

                            <b style ="float :right  ;"><h2 class="box-title">หน่วย : บาท </h2> </b>


                           <%-- <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>--%>
                        </div>
                        <div class="box-body">

                           
                            <div class="row">
                                <div class="col-md-8">
                                    <asp:Chart ID="ChartByCooperation" runat="server"  CssClass="ChartHighligh" Height="500px" Width="700px"  BackSecondaryColor="White">
                                    <titles>
                                        <%--<asp:Title Font="Tahoma, 9.75pt, style=Bold" Name="Title1" Text="GSP#X" />--%>
                                        <%--<asp:Title Docking="Bottom" Name="Title2" Text="xxx Tag" />--%>
                                      
                                    </titles>
                                    <series>
                                        <asp:Series Name="Series_ChartByCooperation"   MarkerStyle="Square" 
                                            XValueType="String"     >
                                            <points>
                                               
                                            </points>
                                            
                                        </asp:Series>
                                    </series>
                                    <chartareas>
                                        <asp:ChartArea Name="ChartArea1">
                                            <axisy  intervalautomode="VariableCount"  title="จำนวนเงิน (บาท)" >
                                                 <LabelStyle Font="14pt"  />
                                            </axisy>
                                            <%--<axisx  intervalautomode="VariableCount">
                                                <customlabels>
                                                    <asp:CustomLabel />
                                                </customlabels>
                                            </axisx>--%>
                                        </asp:ChartArea>
                                    </chartareas>
                                        
                                </asp:Chart>

                                    <asp:panel id="pnlPie" runat="server" visible="false">


                                                                                                        <div class="col-md-12" style="text-align :center  " >
                                    <asp:Chart ID="Chart_Sub_region" runat="server"  CssClass="ChartHighligh" Height="500px" Width="500px" BackSecondaryColor="White" Palette="Pastel">
                                    <titles> 
                                      
                                    </titles>
                                    <series>
                                        <asp:Series ChartType="Pie" Name="Series_Sub_region" 
                                            XValueType="String" Legend="Legend_Sub_region"     >
                                            <points>
                                               
                                                <asp:DataPoint CustomProperties="PieLabelStyle=inside" YValues="0"  />
                                               
                                            </points>
                                            <emptypointstyle LabelAngle="10" />
                                        </asp:Series>
                                    </series>
                                    <chartareas>
                                        <asp:ChartArea Name="ChartArea_Sub_region">
                                            <area3dstyle enable3d="false" />
                                        </asp:ChartArea>
                                    </chartareas>
                                       <%-- <legends>
                                            <asp:Legend Docking="Bottom" Name="Legend_Sub_region" Font="Microsoft Sans Serif, 9.75pt" IsTextAutoFit="False" >
                                            </asp:Legend>
                                        </legends>--%>
                                </asp:Chart>
                                </div>


                                    </asp:panel>


                                </div>
                                


                                <div class="col-md-4">



                                <table id="example2" class="table table-bordered table-hover" style="border-collapse: initial">

                                    <thead>
                                        <tr class="bg-gray text-center">
                                            <th colspan ="2" >Sub-region</th>
                                            <th >Total</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                       
                                        <asp:Repeater ID="rptList" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td id="td_color_Legends" runat ="server" style ="width :2px;"  data-title="Regionoda Name" >
                                                        
                                                    </td>
                                                    <td data-title="Regionoda Name" >
                                                        <asp:Label ID="lbl_regionoda_name" runat ="server" ></asp:Label>
                                                    </td>
                                                     
                                                    
                                                    <td data-title="Total" id="td1" runat="server" style="text-align: right;">
                                                        <asp:Label ID="lblTotal_Row" runat="server" Text="0.00"></asp:Label>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                    <tfoot >
                                        <%--Total--%>
                                        <tr class="bg-gray text-center">
                                            <td style ="text-align :left ;"  colspan ="2"     ><b>Total</b></td>
                                             
                                            <td  style ="text-align :right ;"   ><asp:Label ID="lbl_Footer_Tatal" runat="server" ></asp:Label></td>
                                        </tr>


                                        <%--%Total--%>
                                        <tr class="bg-gray text-center">
                                            <td  style ="text-align :left ;"   colspan ="2"   ><b>%Total</b></td>
                                             
                                            <td  style ="text-align :right ;"  ><asp:Label ID="lbl_Per_Footer_Tatal" runat="server" ></asp:Label></td>
                                        </tr>
                                    </tfoot>
                                        
                                
                                </table>





                                </div>

                                <!-- /.col -->
                            </div>
                            <!-- /.box-body -->

                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->

                </div>





                <div class="col-md-6" id ="div_ChartByProgramme" runat ="server" visible ="false" >
                    <!-- LINE CHART -->
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <%--<h3 class="box-title">Total Value of Thailand International Cooperation Program By Programme</h3>--%>
                            <h3 class="box-title">By Programme</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:Chart ID="ChartByProgramme" runat="server"  CssClass="ChartHighligh" Height="500px" Width="500px" BackSecondaryColor="White">
                                    <titles>
                                        <%--<asp:Title Font="Tahoma, 9.75pt, style=Bold" Name="Title1" Text="GSP#X" />--%>
                                        <%--<asp:Title Docking="Bottom" Name="Title2" Text="xxx Tag" />--%>
                                      
                                    </titles>
                                    <series>
                                        <asp:Series ChartType="Pie" Name="Series1"   MarkerStyle="Square"
                                            XValueType="String" Legend="Legend1"     >
                                            <points>
                                               
                                            </points>
                                            <emptypointstyle LabelAngle="20" />
                                        </asp:Series>
                                    </series>
                                    <chartareas>
                                        <asp:ChartArea Name="ChartArea1">
                                        </asp:ChartArea>
                                    </chartareas>
                                        <legends>
                                            <asp:Legend Docking="Bottom" Name="Legend1" Font="Microsoft Sans Serif, 9.75pt" IsTextAutoFit="False">
                                            </asp:Legend>
                                        </legends>
                                </asp:Chart>
                                </div>
                                

                                <!-- /.col -->
                            </div>
                            <!-- /.box-body -->

                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->

                </div>


                <div class="col-md-6"  id ="div_ChartBySub_Region" runat ="server" visible ="false" >
                    <!-- LINE CHART -->
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">By Sub-region</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                

                                <!-- /.col -->
                            </div>
                            <!-- /.box-body -->

                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.col -->













            </div>

            <div class="row" id="divfooter"  runat ="server" visible ="false" >
                <div class="col-md-12">
                    <!-- LINE CHART -->
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <b><h2 class="box-title">Total Value of Thailand International Cooperation Program</h2></b>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                </div>




                            </div>
                            <!-- /.box-body -->

                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->

                </div>















            </div>
            <!-- /.row -->











        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


    <!-- Morris charts -->
    <link rel="stylesheet" href="plugins/morris/morris.css">
    <!-- Morris.js charts -->
    <script src="plugins/morris/raphael-min.js"></script>
    <script src="plugins/morris/morris.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <link rel="stylesheet" href="plugins/morris/morris.css">
    <script src="plugins/morris/raphael-min.js"></script>
    <script src="plugins/morris/morris.min.js"></script>
    <asp:Label ID="lblChart" runat="server"></asp:Label>

    <script>
        function link(_type) {
            window.location = 'frmProject.aspx?type=' + _type;

        }
    </script>

</asp:Content>

