﻿Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE

Partial Class frmActivityExpense
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    Public Property AllData As DataTable
        Get
            Try
                Return Session("ProjectExpenseData")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("ProjectExpenseData") = value
        End Set
    End Property

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Public Property mode As String
        'view/edit
        Get
            Try
                Return ViewState("mode")
            Catch ex As Exception
                Return "add"
            End Try
        End Get
        Set(value As String)
            ViewState("mode") = value
        End Set
    End Property


    Public Property type As String
        'view/edit
        Get
            Try
                Return ViewState("type")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            ViewState("type") = value
        End Set
    End Property

    Public Property ActivityID As Long
        Get
            Try
                Return ViewState("ActivityID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ActivityID") = value
        End Set
    End Property

    Public Property PID As Long
        Get
            Try
                Return ViewState("PID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("PID") = value
        End Set
    End Property

    Public Property HeaderId As Long
        Get
            Try
                Return ViewState("HeaderId")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("HeaderId") = value
        End Set
    End Property

    Private Sub frmActivityExpense_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            mode = Request.QueryString("mode").ToString()
            ActivityID = CInt(Request.QueryString("ActivityID"))
            HeaderId = CInt(Request.QueryString("HeaderId"))
            PID = CInt(Request.QueryString("PID"))
            type = CInt(Request.QueryString("type"))
            BindList(ActivityID)
        Else
            Tang()
        End If
    End Sub

    Private Sub Tang()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Tang", "Tang();", True)
    End Sub

    Sub BindList(activity_id As String)

        txtPaymentStartDate.Text = ""
        txtPaymentEndDate.Text = ""
        Dim dtpj As New DataTable
        dtpj = BL.GetHeaderData(activity_id, HeaderId)

        If dtpj.Rows.Count > 0 Then
            txtProjectName.Text = dtpj.Rows(0)("project_name").ToString
            If dtpj.Rows(0)("activity_name").ToString <> "Null" Then
                Dim Acname As String = dtpj.Rows(0)("activity_name").ToString
                txtActivitytName.Text = "ส่วนการเงินของ " + Acname + " จาก "
            End If
            txtPaymentStartDate.Text = dtpj.Rows(0)("Payment_Date_Start").ToString
            If txtPaymentStartDate.Text <> "" Then

                txtPaymentStartDate.Text = Convert.ToDateTime(dtpj.Rows(0)("Payment_Date_Start")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
            End If
            txtPaymentEndDate.Text = dtpj.Rows(0)("Payment_Date_End").ToString
                If txtPaymentEndDate.Text <> "" Then
                    txtPaymentEndDate.Text = Convert.ToDateTime(dtpj.Rows(0)("Payment_Date_End")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
                End If
            End If
            Dim dt As New DataTable

        Dim Recip As DataTable = BL.GetRecipinceType(ActivityID)
        Dim rt As String = Recip.Rows(0)("Recipient_Type").ToString()

        If rt = "R" Then
            dt = BL.GetActivityExpenseDataIntoTypeR(activity_id, HeaderId)
            rptList.DataSource = dt
            rptList.DataBind()

            AllData = dt
            PanelList.Visible = True
            PanelListGroup.Visible = False
        ElseIf rt = "C" Then
            dt = BL.GetActivityExpenseDataIntoTypeC(activity_id, HeaderId)
            rptList.DataSource = dt
            rptList.DataBind()

            AllData = dt
            PanelList.Visible = True
            PanelListGroup.Visible = False
        ElseIf rt = "O" Then
            dt = BL.GetActivityExpenseDataIntoTypeO(activity_id, HeaderId)
            rptList.DataSource = dt
            rptList.DataBind()

            AllData = dt
            PanelList.Visible = True
            PanelListGroup.Visible = False
        ElseIf rt = "G" Then
            dt = BL.GetActivityExpenseDataIntoTypeG(activity_id, HeaderId)

            lblBudgedGroup.Text = "งบประมาณ " + Convert.ToDecimal(dt.Rows(0)("budget")).ToString("#,##0.00") + " บาท"
            rptGroup.DataSource = dt
            rptGroup.DataBind()

            AllData = dt
            PanelList.Visible = False
            PanelListGroup.Visible = True
            Dim sumbudgetG As Double = 0
            Dim sumexpenseG As Double = 0
            Dim sumtotalG As Double = 0
            If dt.Rows.Count > 0 Then
                For i As Integer = 0 To dt.Rows.Count - 1

                    sumexpenseG += dt.Rows(i)("Expense")
                Next
                sumbudgetG = dt.Rows(0)("budget")
                sumtotalG = sumbudgetG - sumexpenseG
            End If
            lblSumGroup.Text = Convert.ToDecimal(sumtotalG).ToString("#,##0.00")

        End If
        Dim sumbudget As Double = 0
        Dim sumexpense As Double = 0
        Dim sumtotal As Double = 0
        If dt.Rows.Count > 0 Then
            For i As Integer = 0 To dt.Rows.Count - 1
                sumbudget += dt.Rows(i)("budget")
                sumexpense += dt.Rows(i)("Expense")
            Next
            sumtotal = sumbudget - sumexpense
        End If
        lblSumbudget.Text = Convert.ToDecimal(sumbudget).ToString("#,##0.00")
        lblSumexpense.Text = Convert.ToDecimal(sumexpense).ToString("#,##0.00")
        lblSumTotal.Text = Convert.ToDecimal(sumtotal).ToString("#,##0.00")


    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        Dim lblName As Label = DirectCast(e.Item.FindControl("lblName"), Label)
        Dim lblbudget As Label = DirectCast(e.Item.FindControl("lblbudget"), Label)
        Dim lblExpense As Label = DirectCast(e.Item.FindControl("lblExpense"), Label)
        Dim lblTotal As Label = DirectCast(e.Item.FindControl("lblTotal"), Label)
        Dim PanelBudget As Panel = DirectCast(e.Item.FindControl("PanelBudget"), Panel)
        Dim PanelExpense As Panel = DirectCast(e.Item.FindControl("PanelExpense"), Panel)
        Dim img As HtmlImage = DirectCast(e.Item.FindControl("imgicon"), HtmlImage)

        Dim lblcountryid As String = e.Item.DataItem("country_id").ToString
        img.Src = ""
        img.Src = "../Flag/" & lblcountryid & ".png"
        lblName.Text = e.Item.DataItem("Name").ToString
        lblbudget.Text = "งบประมาณ " + Convert.ToDecimal(e.Item.DataItem("budget")).ToString("#,##0.00") + " บาท"
        lblExpense.Text = "ค่าใช้จ่าย " + Convert.ToDecimal(e.Item.DataItem("Expense")).ToString("#,##0.00") + " บาท"
        Dim sum As Double = e.Item.DataItem("budget") - e.Item.DataItem("Expense")
        lblTotal.Text = "คงเหลือ " + Convert.ToDecimal(sum).ToString("#,##0.00") + " บาท"
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand

        'ใช้ในเครื่อง 88

        Dim lnqAEH As New TbActivityExpenseHeaderLinqDB
        With lnqAEH
            .ID = HeaderId
            .ACTIVITY_ID = ActivityID

            If txtPaymentStartDate.Text <> "" Then
                Dim a As String = txtPaymentStartDate.Text
                Dim d As String() = a.Split("/")
                Dim y As Integer = Convert.ToInt32(d(2)) - 543
                If y < 1997 Then
                    y += 543
                End If
                Dim z As String = ((d(0) & "/") + d(1) & "/") + y.ToString()
                .PAYMENT_DATE_START = Converter.StringToDate(z, "dd/MM/yyyy")
            End If
            If txtPaymentEndDate.Text <> "" Then
                Dim endtext As String = txtPaymentEndDate.Text
                Dim splitEnd As String() = endtext.Split("/")
                Dim setEndyear As Integer = Convert.ToInt32(splitEnd(2)) - 543
                If setEndyear < 1997 Then
                    setEndyear += 543
                End If
                Dim textend As String = ((splitEnd(0) & "/") + splitEnd(1) & "/") + setEndyear.ToString()
                .PAYMENT_DATE_END = Converter.StringToDate(textend, "dd/MM/yyyy")
            End If
        End With
        Dim ret As New ProcessReturnInfo
        ret = BL.SaveHeader(lnqAEH, UserName)
        If e.CommandName = "budget" Then
            Dim PanelExpense As Panel = DirectCast(e.Item.FindControl("PanelExpense"), Panel)
            Dim PanelBudget As Panel = DirectCast(e.Item.FindControl("PanelBudget"), Panel)

            Dim EditExpenseID As Long = 0

            Dim dtTemplate As DataTable = BL.GetTemplateActivity(ActivityID)

            Dim Template_id As Long = dtTemplate.Rows(0)("template_id").ToString()

            Dim Recip As DataTable = BL.GetRecipinceType(ActivityID)

            Dim rt As String = Recip.Rows(0)("Recipient_Type").ToString()
            Session("Recipient_Type") = rt

            Dim UCdt As usercontrol_UCActivityExpensePlan = DirectCast(e.Item.FindControl("UCActivityExpensePlan"), usercontrol_UCActivityExpensePlan)
            If rt = "G" Then
                UCdt.TemplateID = Template_id
                UCdt.RecipienceID = "0"
                UCdt.ActivityID = ActivityID
                UCdt.HeaderID = HeaderId
                UCdt.UserName = UserName
                UCdt.Type = type
                UCdt.PID = PID
                UCdt.SetHead(HeaderId)
                UCdt.SetDataRpt(HeaderId)
                UCdt.SetDataRptSum()
                UCdt.Visible = True
            ElseIf rt = "C" Then
                Dim dt As DataTable = BL.GetRecipinceCountry(ActivityID)
                If dt.Rows.Count > 0 Then
                    UCdt.TemplateID = Template_id
                    UCdt.RecipienceID = e.CommandArgument
                    UCdt.ActivityID = ActivityID
                    UCdt.HeaderID = HeaderId
                    UCdt.UserName = UserName
                    UCdt.Type = type
                    UCdt.PID = PID
                    UCdt.SetHead(HeaderId)
                    UCdt.SetDataRpt(HeaderId)
                    UCdt.SetDataRptSum()
                    UCdt.ComponentDT = dt
                    UCdt.Visible = True
                Else
                    UCdt.Visible = False
                End If
            ElseIf rt = "R" Then
                Dim dt As DataTable = BL.GetActivityRecipinceIndividual(ActivityID)
                If dt.Rows.Count > 0 Then
                    UCdt.TemplateID = Template_id
                    UCdt.RecipienceID = e.CommandArgument
                    UCdt.ActivityID = ActivityID
                    UCdt.HeaderID = HeaderId
                    UCdt.UserName = UserName
                    UCdt.Type = type
                    UCdt.PID = PID
                    UCdt.SetHead(HeaderId)
                    UCdt.SetDataRpt(HeaderId)
                    UCdt.SetDataRptSum()
                    UCdt.ComponentDT = dt
                    UCdt.Visible = True
                Else
                    UCdt.Visible = False
                End If
            ElseIf rt = "O" Then
                Dim dt As DataTable = BL.GetActivityRecipinceOrganize(ActivityID)
                If dt.Rows.Count > 0 Then
                    UCdt.TemplateID = Template_id
                    UCdt.RecipienceID = e.CommandArgument
                    UCdt.ActivityID = ActivityID
                    UCdt.HeaderID = HeaderId
                    UCdt.UserName = UserName
                    UCdt.Type = type
                    UCdt.PID = PID
                    UCdt.SetHead(HeaderId)
                    UCdt.SetDataRpt(HeaderId)
                    UCdt.SetDataRptSum()
                    UCdt.ComponentDT = dt
                    UCdt.Visible = True
                Else
                    UCdt.Visible = False
                End If
            End If
            PanelExpense.Visible = False
            PanelBudget.Visible = True
        ElseIf e.CommandName = "expense" Then
            Dim PanelExpense As Panel = DirectCast(e.Item.FindControl("PanelExpense"), Panel)
            Dim PanelBudget As Panel = DirectCast(e.Item.FindControl("PanelBudget"), Panel)

            Dim EditExpenseID As Long = 0

            Dim dtTemplate As DataTable = BL.GetTemplateActivity(ActivityID)

            Dim Template_id As Long = dtTemplate.Rows(0)("template_id").ToString()

            Dim Recip As DataTable = BL.GetRecipinceType(ActivityID)

            Dim rt As String = Recip.Rows(0)("Recipient_Type").ToString()
            Dim UCdt As usercontrol_UCActivityExpenseActual = DirectCast(e.Item.FindControl("UCActivityExpenseActual"), usercontrol_UCActivityExpenseActual)
            If rt = "G" Then
                Dim dt As DataTable = BL.GetActivityRecipinceGroup(ActivityID)
                'Dim reid As String
                'reid = BL.GetCountry_ById(ID)
                'lblRecipience.Text = reid
                'txtNameRecipience.Text = "ประเทศ" + BL.GetCountryName_ByCountryID(reid)
                If dt.Rows.Count > 0 Then
                    UCdt.TemplateID = Template_id
                    UCdt.RecipienceID = e.CommandArgument
                    UCdt.ActivityID = ActivityID
                    UCdt.HeaderID = HeaderId
                    UCdt.UserName = UserName
                    UCdt.Type = type
                    UCdt.PID = PID
                    UCdt.SetHead(HeaderId)
                    UCdt.SetDataRpt(HeaderId)
                    UCdt.SetDataRptSum()
                    UCdt.ComponentDT = dt
                    UCdt.Visible = True
                Else
                    UCdt.Visible = False
                End If
            ElseIf rt = "C" Then
                Dim dt As DataTable = BL.GetRecipinceCountry(ActivityID)
                'Dim reid As String
                'reid = BL.GetCountryRe_ById(ID)
                'lblRecipience.Text = reid
                'txtNameRecipience.Text = "ประเทศ" + BL.GetCountryName_ByCountryID(reid)
                If dt.Rows.Count > 0 Then
                    UCdt.TemplateID = Template_id
                    UCdt.RecipienceID = e.CommandArgument
                    UCdt.ActivityID = ActivityID
                    UCdt.HeaderID = HeaderId
                    UCdt.UserName = UserName
                    UCdt.Type = type
                    UCdt.PID = PID
                    UCdt.SetHead(HeaderId)
                    UCdt.SetDataRpt(HeaderId)
                    UCdt.SetDataRptSum()
                    UCdt.ComponentDT = dt
                    UCdt.Visible = True
                Else
                    UCdt.Visible = False
                End If

            ElseIf rt = "R" Then
                Dim dt As DataTable = BL.GetActivityRecipinceIndividual(ActivityID)
                'Dim reid As String
                'reid = BL.GetRecipience_ById(ID)
                'lblRecipience.Text = reid
                'txtNameRecipience.Text = BL.GetRecipienceName_ById(reid)
                If dt.Rows.Count > 0 Then
                    UCdt.TemplateID = Template_id
                    UCdt.RecipienceID = e.CommandArgument
                    UCdt.ActivityID = ActivityID
                    UCdt.HeaderID = HeaderId
                    UCdt.UserName = UserName
                    UCdt.Type = type
                    UCdt.PID = PID
                    UCdt.SetHead(HeaderId)
                    UCdt.SetDataRpt(HeaderId)
                    UCdt.SetDataRptSum()
                    UCdt.ComponentDT = dt
                    UCdt.Visible = True
                Else
                    UCdt.Visible = False
                End If
            ElseIf rt = "O" Then
                Dim dt As DataTable = BL.GetActivityRecipinceOrganize(ActivityID)
                If dt.Rows.Count > 0 Then
                    UCdt.TemplateID = Template_id
                    UCdt.RecipienceID = e.CommandArgument
                    UCdt.ActivityID = ActivityID
                    UCdt.HeaderID = HeaderId
                    UCdt.UserName = UserName
                    UCdt.Type = type
                    UCdt.PID = PID
                    UCdt.SetHead(HeaderId)
                    UCdt.SetDataRpt(HeaderId)
                    UCdt.SetDataRptSum()
                    UCdt.ComponentDT = dt
                    UCdt.Visible = True
                Else
                    UCdt.Visible = False
                End If
            End If
            PanelExpense.Visible = True
            PanelBudget.Visible = False
        End If
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            '## Validate

            If txtPaymentStartDate.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('โปรดระบุวันที่เริ่มต้น');", True)
                Exit Sub
            End If
            If txtPaymentEndDate.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('โปรดระบุวันที่สิ้นสุด');", True)
                Exit Sub
            End If
            'ใช้ในเครื่อง 88
            Dim a As String = txtPaymentStartDate.Text
            'แยกวัน เดือน ปี
            Dim d As String() = a.Split("/")
            'แปลง ค.ศ.เป็น พ.ศ.
            Dim y As Integer = Convert.ToInt32(d(2)) - 543
            If y < 1997 Then
                y += 543
            End If
            'มาจัดรูปแบบใหม่ว่าต้องการแบบไหน
            Dim z As String = ((d(0) & "/") + d(1) & "/") + y.ToString()

            Dim endtext As String = txtPaymentEndDate.Text
            'แยกวัน เดือน ปี
            Dim splitEnd As String() = endtext.Split("/")
            'แปลง ค.ศ.เป็น พ.ศ.
            Dim setEndyear As Integer = Convert.ToInt32(splitEnd(2)) - 543
            If setEndyear < 1997 Then
                setEndyear += 543
            End If
            'มาจัดรูปแบบใหม่ว่าต้องการแบบไหน
            Dim textend As String = ((splitEnd(0) & "/") + splitEnd(1) & "/") + setEndyear.ToString()

            Dim lnqAEH As New TbActivityExpenseHeaderLinqDB
            With lnqAEH
                .ID = HeaderId
                .ACTIVITY_ID = ActivityID
                .PAYMENT_DATE_START = Converter.StringToDate(z, "dd/MM/yyyy")
                .PAYMENT_DATE_END = Converter.StringToDate(textend, "dd/MM/yyyy")
            End With

            Dim ret As New ProcessReturnInfo
            ret = BL.SaveHeader(lnqAEH, UserName)
            If ret.IsSuccess Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');", True)
                If type = "2" Then
                    Response.Redirect("frmLoan_Detail_Activity.aspx?id=" & PID & "&mode=palnex" & "&type=" & type & "&mo=" & ActivityID)
                ElseIf type = "3" Then
                    Response.Redirect("frmContribuiltion_Detail_Activity.aspx?id=" & PID & "&mode=palnex" & "&type=" & type & "&mo=" & ActivityID)
                ElseIf type = "1" Or type = "0" Then
                    Response.Redirect("frmProject_Detail_Activity.aspx?id=" & PID & "&mode=palnex" & "&type=" & type & "&mo=" & ActivityID)
                Else
                    Response.Redirect("frmOverduePlan.aspx")
                End If
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('ไม่สามารถบันทึกข้อมูลได้');", True)
            End If

        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('ไม่สามารถบันทึกข้อมูลได้');", True)
        End Try
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        If type = "2" Then
            Response.Redirect("frmLoan_Detail_Activity.aspx?id=" & PID & "&mode=palnex" & "&type=" & type & "&mo=" & ActivityID)
        ElseIf type = "3" Then
            Response.Redirect("frmContribuiltion_Detail_Activity.aspx?id=" & PID & "&mode=palnex" & "&type=" & type & "&mo=" & ActivityID)
        ElseIf type = "1" Or type = "0" Then
            Response.Redirect("frmProject_Detail_Activity.aspx?id=" & PID & "&mode=palnex" & "&type=" & type & "&mo=" & ActivityID)
        Else
            Response.Redirect("frmOverduePlan.aspx")
        End If

    End Sub

    Private Sub rptGroup_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptGroup.ItemDataBound
        Dim lblNameGroup As Label = DirectCast(e.Item.FindControl("lblNameGroup"), Label)
        Dim lblExpenseGroup As Label = DirectCast(e.Item.FindControl("lblExpenseGroup"), Label)
        Dim PanelExpenseGroup As Panel = DirectCast(e.Item.FindControl("PanelExpenseGroup"), Panel)
        Dim img As HtmlImage = DirectCast(e.Item.FindControl("imgicon"), HtmlImage)

        Dim lblcountryid As String = e.Item.DataItem("country_id").ToString
        img.Src = ""
        img.Src = "../Flag/" & lblcountryid & ".png"
        lblNameGroup.Text = e.Item.DataItem("Name").ToString
        lblExpenseGroup.Text = "ค่าใช้จ่าย " + Convert.ToDecimal(e.Item.DataItem("Expense")).ToString("#,##0.00") + " บาท"
    End Sub

    Private Sub rptGroup_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptGroup.ItemCommand

        Dim PanelExpenseGroup As Panel = DirectCast(e.Item.FindControl("PanelExpenseGroup"), Panel)
        'ใช้ในเครื่อง 88

        Dim lnqAEH As New TbActivityExpenseHeaderLinqDB
        With lnqAEH
            .ID = HeaderId
            .ACTIVITY_ID = ActivityID

            If txtPaymentStartDate.Text <> "" Then
                Dim a As String = txtPaymentStartDate.Text
                Dim d As String() = a.Split("/")
                Dim y As Integer = Convert.ToInt32(d(2)) - 543
                If y < 1997 Then
                    y += 543
                End If
                Dim z As String = ((d(0) & "/") + d(1) & "/") + y.ToString()
                .PAYMENT_DATE_START = Converter.StringToDate(z, "dd/MM/yyyy")
            End If
            If txtPaymentEndDate.Text <> "" Then
                Dim endtext As String = txtPaymentEndDate.Text
                Dim splitEnd As String() = endtext.Split("/")
                Dim setEndyear As Integer = Convert.ToInt32(splitEnd(2)) - 543
                If setEndyear < 1997 Then
                    setEndyear += 543
                End If
                Dim textend As String = ((splitEnd(0) & "/") + splitEnd(1) & "/") + setEndyear.ToString()
                .PAYMENT_DATE_END = Converter.StringToDate(textend, "dd/MM/yyyy")
            End If
        End With
        If e.CommandName = "expense" Then


            Dim EditExpenseID As Long = 0

            Dim dtTemplate As DataTable = BL.GetTemplateActivity(ActivityID)

            Dim Template_id As Long = dtTemplate.Rows(0)("template_id").ToString()

            Dim Recip As DataTable = BL.GetRecipinceType(ActivityID)

            Dim rt As String = Recip.Rows(0)("Recipient_Type").ToString()
            Dim UCdt As usercontrol_UCActivityExpenseActual = DirectCast(e.Item.FindControl("UCActivityExpenseActual"), usercontrol_UCActivityExpenseActual)
            If rt = "G" Then
                Dim dt As DataTable = BL.GetActivityRecipinceGroup(ActivityID)
                'Dim reid As String
                'reid = BL.GetCountry_ById(ID)
                'lblRecipience.Text = reid
                'txtNameRecipience.Text = "ประเทศ" + BL.GetCountryName_ByCountryID(reid)
                If dt.Rows.Count > 0 Then
                    UCdt.TemplateID = Template_id
                    UCdt.RecipienceID = e.CommandArgument
                    UCdt.ActivityID = ActivityID
                    UCdt.HeaderID = HeaderId
                    UCdt.UserName = UserName
                    UCdt.Type = type
                    UCdt.PID = PID
                    UCdt.SetHead(HeaderId)
                    UCdt.SetDataRpt(HeaderId)
                    UCdt.SetDataRptSum()
                    UCdt.ComponentDT = dt
                    UCdt.Visible = True
                Else
                    UCdt.Visible = False
                End If
            ElseIf rt = "C" Then
                Dim dt As DataTable = BL.GetRecipinceCountry(ActivityID)
                'Dim reid As String
                'reid = BL.GetCountryRe_ById(ID)
                'lblRecipience.Text = reid
                'txtNameRecipience.Text = "ประเทศ" + BL.GetCountryName_ByCountryID(reid)
                If dt.Rows.Count > 0 Then
                    UCdt.TemplateID = Template_id
                    UCdt.RecipienceID = e.CommandArgument
                    UCdt.ActivityID = ActivityID
                    UCdt.HeaderID = HeaderId
                    UCdt.UserName = UserName
                    UCdt.Type = type
                    UCdt.PID = PID
                    UCdt.SetHead(HeaderId)
                    UCdt.SetDataRpt(HeaderId)
                    UCdt.SetDataRptSum()
                    UCdt.ComponentDT = dt
                    UCdt.Visible = True
                Else
                    UCdt.Visible = False
                End If

            ElseIf rt = "R" Then
                Dim dt As DataTable = BL.GetActivityRecipinceIndividual(ActivityID)
                'Dim reid As String
                'reid = BL.GetRecipience_ById(ID)
                'lblRecipience.Text = reid
                'txtNameRecipience.Text = BL.GetRecipienceName_ById(reid)
                If dt.Rows.Count > 0 Then
                    UCdt.TemplateID = Template_id
                    UCdt.RecipienceID = e.CommandArgument
                    UCdt.ActivityID = ActivityID
                    UCdt.HeaderID = HeaderId
                    UCdt.UserName = UserName
                    UCdt.Type = type
                    UCdt.PID = PID
                    UCdt.SetHead(HeaderId)
                    UCdt.SetDataRpt(HeaderId)
                    UCdt.SetDataRptSum()
                    UCdt.ComponentDT = dt
                    UCdt.Visible = True
                Else
                    UCdt.Visible = False
                End If
            ElseIf rt = "O" Then
                Dim dt As DataTable = BL.GetActivityRecipinceOrganize(ActivityID)
                If dt.Rows.Count > 0 Then
                    UCdt.TemplateID = Template_id
                    UCdt.RecipienceID = e.CommandArgument
                    UCdt.ActivityID = ActivityID
                    UCdt.HeaderID = HeaderId
                    UCdt.UserName = UserName
                    UCdt.Type = type
                    UCdt.PID = PID
                    UCdt.SetHead(HeaderId)
                    UCdt.SetDataRpt(HeaderId)
                    UCdt.SetDataRptSum()
                    UCdt.ComponentDT = dt
                    UCdt.Visible = True
                Else
                    UCdt.Visible = False
                End If
            End If
        End If
        PanelExpenseGroup.Visible = True
    End Sub

    Private Sub btnBudgetGroup_Click(sender As Object, e As EventArgs) Handles btnBudgetGroup.Click

        'ใช้ในเครื่อง 88

        Dim lnqAEH As New TbActivityExpenseHeaderLinqDB
        With lnqAEH
            .ID = HeaderId
            .ACTIVITY_ID = ActivityID

            If txtPaymentStartDate.Text <> "" Then
                Dim a As String = txtPaymentStartDate.Text
                Dim d As String() = a.Split("/")
                Dim y As Integer = Convert.ToInt32(d(2)) - 543
                If y < 1997 Then
                    y += 543
                End If
                Dim z As String = ((d(0) & "/") + d(1) & "/") + y.ToString()
                .PAYMENT_DATE_START = Converter.StringToDate(z, "dd/MM/yyyy")
            End If
            If txtPaymentEndDate.Text <> "" Then
                Dim endtext As String = txtPaymentEndDate.Text
                Dim splitEnd As String() = endtext.Split("/")
                Dim setEndyear As Integer = Convert.ToInt32(splitEnd(2)) - 543
                If setEndyear < 1997 Then
                    setEndyear += 543
                End If
                Dim textend As String = ((splitEnd(0) & "/") + splitEnd(1) & "/") + setEndyear.ToString()
                .PAYMENT_DATE_END = Converter.StringToDate(textend, "dd/MM/yyyy")
            End If
        End With
        Dim ret As New ProcessReturnInfo
        ret = BL.SaveHeader(lnqAEH, UserName)
        Dim EditExpenseID As Long = 0

            Dim dtTemplate As DataTable = BL.GetTemplateActivity(ActivityID)

            Dim Template_id As Long = dtTemplate.Rows(0)("template_id").ToString()

            Dim Recip As DataTable = BL.GetRecipinceType(ActivityID)

            Dim rt As String = Recip.Rows(0)("Recipient_Type").ToString()
            Session("Recipient_Type") = rt

        Dim UCdt As usercontrol_UCActivityExpensePlan = DirectCast(FindControl("UCActivityExpensePlan"), usercontrol_UCActivityExpensePlan)
        If rt = "G" Then
                UCdt.TemplateID = Template_id
                UCdt.RecipienceID = "0"
                UCdt.ActivityID = ActivityID
                UCdt.HeaderID = HeaderId
                UCdt.UserName = UserName
                UCdt.Type = type
                UCdt.PID = PID
                UCdt.SetHead(HeaderId)
                UCdt.SetDataRpt(HeaderId)
                UCdt.SetDataRptSum()
                UCdt.Visible = True
            End If
        PanelBudgetGroup.Visible = True

    End Sub
End Class
