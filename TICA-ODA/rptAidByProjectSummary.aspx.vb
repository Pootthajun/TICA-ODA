﻿Imports Constants
Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class rptAidByProjectSummary
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    Public Property ProjectType As Long
        Get
            Try
                Return ViewState("ProjectType")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ProjectType") = value
        End Set
    End Property

    Public Property AllData As DataTable
        Get
            Try
                Return Session("rptAidByProjectSummary")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("rptAidByProjectSummary") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
        li.Attributes.Add("class", "active")
        Dim a As HtmlAnchor = Me.Page.Master.FindControl("arptAidByCountrySummary")
        a.Attributes.Add("style", "color:#FF8000")

        If Not IsPostBack Then
            BindList()
            BL.Bind_DDL_Year(ddlBudgetYear, True)
        End If

    End Sub

    Private Sub BindList()

        Dim Title As String = ""
        Dim sql As String = ""

        sql += " Select *,Summary_Actual_Bachelor +Summary_Actual_Training +Summary_Actual_Expert +Summary_Actual_Volunteer +Summary_Actual_Equipment +Summary_Actual_Other  Summary_Actual  " + Environment.NewLine
        sql += " FROM _vw_Summary_Aid_Group_By_Project  " + Environment.NewLine
        sql += " WHERE 1=1  " + Environment.NewLine

            If (ddlBudgetYear.SelectedIndex > 0) Then
            sql += " AND  budget_year = '" & ddlBudgetYear.SelectedValue & "' " + Environment.NewLine
            Title += " ปีงบประมาณ " & ddlBudgetYear.SelectedValue
            End If
            If (txtSearch_Project.Text <> "") Then
            sql += " AND project_name Like '%" & txtSearch_Project.Text & "%'   " + Environment.NewLine
            Title += " ของ " & txtSearch_Project.Text
            End If

            sql += "  ORDER BY   budget_year DESC,project_name "

            Dim DA As SqlDataAdapter = New SqlDataAdapter(sql, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

            lblTotalRecord.Text = Title
            If DT.Rows.Count = 0 Then
                lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
            Else
            lblTotalRecord.Text &= " พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
        End If

        Session("Search_Summary_AidByProject_Title") = lblTotalRecord.Text

        If (DT.Rows.Count > 0) Then
            lblBachelor_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_Actual_Bachelor)", "")).ToString("#,##0.00")
            lblTraining_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_Actual_Training)", "")).ToString("#,##0.00")
            lblExpert_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_Actual_Expert)", "")).ToString("#,##0.00")
            lblVolunteer_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_Actual_Volunteer)", "")).ToString("#,##0.00")
            lblEquipment_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_Actual_Equipment)", "")).ToString("#,##0.00")
            lblOther_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_Actual_Other)", "")).ToString("#,##0.00")

            lblAmout_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_Actual)", "")).ToString("#,##0.00")
            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If


        Session("Search_Summary_rptAidByProject") = DT

        AllData = DT
        Pager.SesssionSourceName = "rptAidByProjectSummary"
        Pager.RenderLayout()
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Dim Lastbudget_year As String = ""
    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim lblBachelor As Label = DirectCast(e.Item.FindControl("lblBachelor"), Label)
        Dim lblTraining As Label = DirectCast(e.Item.FindControl("lblTraining"), Label)
        Dim lblExpert As Label = DirectCast(e.Item.FindControl("lblExpert"), Label)
        Dim lblVolunteer As Label = DirectCast(e.Item.FindControl("lblVolunteer"), Label)
        Dim lblEquipment As Label = DirectCast(e.Item.FindControl("lblEquipment"), Label)
        Dim lblOther As Label = DirectCast(e.Item.FindControl("lblOther"), Label)

        Dim lblProject As Label = DirectCast(e.Item.FindControl("lblProject"), Label)
        Dim lblAmout As Label = DirectCast(e.Item.FindControl("lblAmout"), Label)

        Dim DuplicatedStyleTop As String = "border-top:none;"

        Dim trbudget_year As HtmlTableRow = e.Item.FindControl("trbudget_year")
        Dim lblbudget_year As Label = e.Item.FindControl("lblbudget_year")

        '--------Lastbudget_year------------
        If Lastbudget_year <> e.Item.DataItem("budget_year").ToString Then
            Lastbudget_year = e.Item.DataItem("budget_year").ToString
            lblbudget_year.Text = Lastbudget_year
            trbudget_year.Visible = True
        Else
            trbudget_year.Visible = False
        End If

        lblProject.Text = e.Item.DataItem("project_name").ToString

        If Convert.IsDBNull(e.Item.DataItem("Summary_Actual_Bachelor")) = False Then
            lblBachelor.Text = Convert.ToDecimal(e.Item.DataItem("Summary_Actual_Bachelor")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Summary_Actual_Training")) = False Then
            lblTraining.Text = Convert.ToDecimal(e.Item.DataItem("Summary_Actual_Training")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Summary_Actual_Expert")) = False Then
            lblExpert.Text = Convert.ToDecimal(e.Item.DataItem("Summary_Actual_Expert")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Summary_Actual_Volunteer")) = False Then
            lblVolunteer.Text = Convert.ToDecimal(e.Item.DataItem("Summary_Actual_Volunteer")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Summary_Actual_Equipment")) = False Then
            lblEquipment.Text = Convert.ToDecimal(e.Item.DataItem("Summary_Actual_Equipment")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Summary_Actual_Other")) = False Then
            lblOther.Text = Convert.ToDecimal(e.Item.DataItem("Summary_Actual_Other")).ToString("#,##0.00")
        End If
        If Convert.IsDBNull(e.Item.DataItem("Summary_Actual")) = False Then
            lblAmout.Text = Convert.ToDecimal(e.Item.DataItem("Summary_Actual")).ToString("#,##0.00")
        End If


    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindList()
    End Sub


#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptAidByCountrySummary.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptAidByCountrySummary.aspx?Mode=EXCEL');", True)
    End Sub

#End Region


    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub


End Class
