﻿Imports System.Data 'ต้อง Import เข้ามา
Imports LinqDB.ConnectDB 'ต้อง Import เข้ามา
Imports LinqDB.TABLE 'ต้อง Import เข้ามา
Imports Constants
Imports System.Data.SqlClient

Public Class frmCooperationType
    Inherits System.Web.UI.Page

    Public Property AllData As DataTable
        Get
            Try
                Return Session("CooperationTypeData")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("CooperationTypeData") = value
        End Set
    End Property

    Protected ReadOnly Property AuthorizeDT As DataTable
        Get
            Try
                Return CType(Session("Authorize"), DataTable)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
    End Property

    Dim TypeMode As String
    Dim BL As New ODAENG

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuMaster")
        li.Attributes.Add("class", "active")

        Dim a As HtmlAnchor = Me.Page.Master.FindControl("aCooperationType")
        a.Attributes.Add("style", "color:#FF8000")
        Dim TB_Authorize As DataTable = Session("Authorize")
        For i As Integer = 0 To TB_Authorize.Rows.Count - 1
            If Menu.CooperationType = TB_Authorize.Rows(i)("menu_id") Then
                If TB_Authorize.Rows(i)("is_save") = "Y" Then
                    TypeMode = "save"
                End If
                If TB_Authorize.Rows(i)("is_view") = "Y" Then
                    TypeMode = "view"
                    btnAdd.Visible = False
                End If
            End If
        Next

        If IsPostBack = False Then
            BindData() 'เมื่อ PageLoad สั่ง Bind Data
            'Authorize()
        End If

    End Sub

    Sub Authorize()
        If AuthorizeDT Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If
        Dim tmpdr() As DataRow = AuthorizeDT.Select("menu_id = '" & Menu.CooperationType & "' and isnull(is_save,'N') ='Y'")

        If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
            btnAdd.Visible = False
            HeadColDelete.Visible = False
        End If

        For i As Integer = 0 To rptList.Items.Count - 1
            Dim btnEdit As LinkButton = DirectCast(rptList.Items(i).FindControl("btnEdit"), LinkButton)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)
            Dim ColDelete As HtmlTableCell = DirectCast(rptList.Items(i).FindControl("ColDelete"), HtmlTableCell)

            If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
                btnEdit.Text = "View"
                btnEdit.CommandName = "View"
                btnEdit.CommandArgument = lblID.Text
                ColDelete.Visible = False
            End If
        Next
    End Sub

    Protected Sub BindData() 'Fubction Bind Data 
        Dim sql As String = "SELECT id,cptype_id,cptype_name,detail,detail_th,ACTIVE_STATUS FROM tb_coperationtype WHERE 1=1"
        If txtSearch.Text.Trim <> "" Then
            sql &= " and (cptype_name LIKE '%" + txtSearch.Text.Trim() + "%' OR detail LIKE '%" + txtSearch.Text.Trim() + "%' OR detail_th LIKE '%" + txtSearch.Text.Trim() + "%')" & vbLf
        End If

        If ddlStatus.SelectedValue <> "" Then
            sql &= " And ACTIVE_STATUS LIKE '%" & ddlStatus.SelectedValue & "%'" & vbLf
        End If

        sql &= " order by cptype_name"

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(sql, BL.ConnectionString)
        DA.Fill(DT)

        rptList.DataSource = DT
        rptList.DataBind()
        lblCount.Text = DT.Rows.Count

        AllData = dt
        Pager.SesssionSourceName = "CooperationTypeData"
        Pager.RenderLayout()
        pnlAdSearch.Visible = False
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)
        Dim lblName As Label = DirectCast(e.Item.FindControl("lblName"), Label) 'ประกาศตัวแปร Label และ FindControl ที่อยู่ในหน้า Design
        Dim lblDetail As Label = DirectCast(e.Item.FindControl("lblDetail"), Label) 'ประกาศตัวแปร Label และ FindControl ที่อยู่ในหน้า Design
        Dim lblDetialTh As Label = DirectCast(e.Item.FindControl("lblDetialTh"), Label) 'ประกาศตัวแปร Label และ FindControl ที่อยู่ในหน้า Design
        Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView) 'ประกาศตัวแปร DataRowView
        Dim btnStatusReady As LinkButton = DirectCast(e.Item.FindControl("btnStatusReady"), LinkButton)
        Dim btnStatusDisabled As LinkButton = DirectCast(e.Item.FindControl("btnStatusDisabled"), LinkButton)
        Dim liview As HtmlGenericControl = e.Item.FindControl("liview")
        Dim liedit As HtmlGenericControl = e.Item.FindControl("liedit")
        Dim lidelete As HtmlGenericControl = e.Item.FindControl("lidelete")
        If TypeMode = "view" Then
            liview.Visible = True
            liedit.Visible = False
            lidelete.Visible = False
        ElseIf TypeMode = "Edit" Then
            liview.Visible = False
            liedit.Visible = True
            lidelete.Visible = True
        End If

        lblID.Text = e.Item.DataItem("id").ToString
        lblName.Text = drv("cptype_name").ToString() 'ให้ lblName = DataRowView("ชื่อฟิลด์ใน Table")
        lblDetail.Text = drv("detail").ToString() 'ให้ lblDetail = DataRowView("ชื่อฟิลด์ใน Table")
        lblDetialTh.Text = drv("detail_th").ToString() 'ให้ lblDetailth = DataRowView("ชื่อฟิลด์ใน Table")


        Dim A As String = drv("ACTIVE_STATUS").ToString
        If A = "Y" Then
            btnStatusReady.Visible = True
            btnStatusDisabled.Visible = False
        ElseIf A = "N" Then
            btnStatusDisabled.Visible = True
            btnStatusReady.Visible = False
        End If

    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click, txtSearch.TextChanged 'ปุ่ม Search
        BindData() 'เมื่อคลิกปุ่ม btnSearch ให้ BindData เหมือนใน PageLoad
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click 'ปุ่ม Add

        Response.Redirect("frmEditCooperationType.aspx") 'เมื่อคลิกปุ่ม btnAdd ให้เปิดหน้า frmEditCooperationType

    End Sub

    Protected Sub Alert(ByVal message As String) 'Function Alert
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub

    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs)
        If e.CommandName = "cmdEdit" Then
            Response.Redirect("frmEditCooperationType.aspx?id=" & e.CommandArgument & "&mode=edit")
        ElseIf e.CommandName = "View" Then
            Response.Redirect("frmEditCooperationType.aspx?id=" & e.CommandArgument & "&mode=view")
        ElseIf e.CommandName = "cmdDelete" Then 'เช็คว่า CommandName = "cmdDelete" หรือไม่ 
            Dim tran As New TransactionDB 'ประกาศตัวแปร TransactionDB
            Dim lnq As New TbCoperationtypeLinqDB 'ประกาศตัวแปร Class Linq ของตารางที่ใช้ในการจัดการข้อมูล (เป็นชื่อตารางที่ใช้)

            Dim ret As ExecuteDataInfo = lnq.DeleteByPK(e.CommandArgument, tran.Trans) 'ลบข้อมูลตาม Primary Key
            If ret.IsSuccess = True Then

                tran.CommitTransaction() 'สั่ง CommitTransaction
                Alert("ลบข้อมูลเรียบร้อยแล้ว")

            Else
                tran.RollbackTransaction() 'สั่ง RollbackTransaction
                Alert("ไม่สามารถลบข้อมูลได้")
                Dim _err As String = tran.ErrorMessage() 'จะได้ Error 
            End If
            lnq = Nothing 'สั่ง Clear ตัวแปร lnq เสมอ หลังทำงานเสร็จ
        ElseIf e.CommandName = "cmdstatusready" Then
            Dim BL As New ODAENG
            'Dim ret As ProcessReturnInfo
            'ret =
            BL.EditStatusAllY(e.CommandArgument, "TB_CoperationType")
            'If ret.IsSuccess Then
            '    Alert("แก้ไขสถานะเรียบร้อยแล้ว")
            BindData()
            'Else
            '    Alert(ret.ErrorMessage)
            'End If
        ElseIf e.CommandName = "cmdstatusDisabled" Then
            Dim BL As New ODAENG
            'Dim ret As ProcessReturnInfo
            'ret =
            BL.EditStatusAllN(e.CommandArgument, "TB_CoperationType")
            'If ret.IsSuccess Then
            '    Alert("แก้ไขสถานะเรียบร้อยแล้ว")
            BindData()
            'Else
            '    Alert(ret.ErrorMessage)
            'End If
        End If
        BindData() 'ลบเสร็จก็สั่ง BindData อีกที
    End Sub

    Private Sub btnOpenSearch_Click(sender As Object, e As EventArgs) Handles btnOpenSearch.Click
        pnlAdSearch.Visible = True
    End Sub

    Private Sub btnCancle_Click(sender As Object, e As EventArgs) Handles btnCancle.Click
        clearFormSearch()
        BindData()
        pnlAdSearch.Visible = False
    End Sub
    Function clearFormSearch()
        txtSearch.Text = ""
        ddlStatus.SelectedValue = ""
    End Function
End Class