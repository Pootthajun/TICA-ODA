﻿Imports System.Data
Imports System.Data.SqlClient
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE

Partial Class frmActivityExActual
    Inherits System.Web.UI.Page


    Dim BL As New ODAENG
    Public Property AllData As DataTable
        Get
            Try
                Return Session("ProjectExpenseData")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("ProjectExpenseData") = value
        End Set
    End Property

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Public Property PID As Long
        Get
            Try
                Return ViewState("PID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("PID") = value
        End Set
    End Property
    Public Property type As String
        'view/edit
        Get
            Try
                Return ViewState("type")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            ViewState("type") = value
        End Set
    End Property

    Public Property mode As String
        'view/edit
        Get
            Try
                Return ViewState("mode")
            Catch ex As Exception
                Return "add"
            End Try
        End Get
        Set(value As String)
            ViewState("mode") = value
        End Set
    End Property

    Public Property ActivityID As Long
        Get
            Try
                Return ViewState("ActivityID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ActivityID") = value
        End Set
    End Property

    Public Property HeaderId As Long
        Get
            Try
                Return ViewState("HeaderId")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("HeaderId") = value
        End Set
    End Property

    Public Property id As Long
        Get
            Try
                Return ViewState("id")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("id") = value
        End Set
    End Property

    Private Sub frmRename_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then

            PID = Request.QueryString("PID").ToString()
            ActivityID = CInt(Request.QueryString("Activity_id"))
            HeaderId = CInt(Request.QueryString("Header_id"))
            id = CInt(Request.QueryString("id"))
            type = CInt(Request.QueryString("type"))
            lblHeaderId.Text = HeaderId
            BindList(ActivityID)
        End If
    End Sub




    Sub BindList(activity_id As String)
        Dim dt As New DataTable
        dt = BL.GetProjectActivityWithRecipince(ActivityID)


        rptList.DataSource = dt
        rptList.DataBind()

        AllData = dt
    End Sub

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If



        Dim EditExpenseID As Long = 0

        Dim dtTemplate As DataTable = BL.GetTemplateActivity(ActivityID)

        Dim Template_id As Long = dtTemplate.Rows(0)("template_id").ToString()

        Dim Recip As DataTable = BL.GetRecipinceType(ActivityID)

        Dim rt As String = Recip.Rows(0)("Recipient_Type").ToString()
        Dim UCdt As usercontrol_UCExpenseActual = DirectCast(e.Item.FindControl("UCExpenseActual"), usercontrol_UCExpenseActual)
        If rt = "G" Then
            Dim dt As DataTable = BL.GetActivityRecipinceGroup(ActivityID)
            Dim reid As String
            reid = BL.GetCountry_ById(id)
            lblRecipience.Text = reid
            txtNameRecipience.Text = "ประเทศ" + BL.GetCountryName_ByCountryID(reid)
            If dt.Rows.Count > 0 Then
                UCdt.TemplateID = Template_id
                UCdt.RecipienceID = reid
                UCdt.ActivityID = ActivityID
                UCdt.HeaderID = HeaderId
                UCdt.SetHead(ActivityID)
                UCdt.SetDataRpt(HeaderId)
                'UCdt.Type = mode
                UCdt.ComponentDT = dt
            Else
                UCdt.Visible = False
            End If
        ElseIf rt = "C" Then
            Dim dt As DataTable = BL.GetRecipinceCountry(ActivityID)
            Dim reid As String
            reid = BL.GetCountryRe_ById(id)
            lblRecipience.Text = reid
            txtNameRecipience.Text = "ประเทศ" + BL.GetCountryName_ByCountryID(reid)
            If dt.Rows.Count > 0 Then
                UCdt.TemplateID = Template_id
                UCdt.RecipienceID = reid
                UCdt.ActivityID = ActivityID
                UCdt.HeaderID = HeaderId
                UCdt.SetHead(ActivityID)
                UCdt.SetDataRpt(HeaderId)
                'UCdt.Type = mode
                UCdt.ComponentDT = dt
            Else
                UCdt.Visible = False
            End If
        ElseIf rt = "R" Then
            Dim dt As DataTable = BL.GetActivityRecipinceIndividual(ActivityID)
            Dim reid As String
            reid = BL.GetRecipience_ById(id)
            lblRecipience.Text = reid
            txtNameRecipience.Text = BL.GetRecipienceName_ById(reid)
            If dt.Rows.Count > 0 Then
                UCdt.TemplateID = Template_id
                UCdt.RecipienceID = reid
                UCdt.ActivityID = ActivityID
                UCdt.HeaderID = HeaderId
                UCdt.SetHead(ActivityID)
                UCdt.SetDataRpt(HeaderId)
                UCdt.ComponentDT = dt
            Else
                UCdt.Visible = False
            End If
        End If
    End Sub


    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            Dim dtH As New DataTable
            With dtH
                .Columns.Add("id")
                .Columns.Add("Payment_Date_Actual")
                .Columns.Add("Pay_Actual_Detail")
                .Columns.Add("Payment_Actual_Detail")
                .Columns.Add("Recipience_id")
            End With

            Dim drH As DataRow
            For i As Integer = 0 To rptList.Items.Count - 1
                Dim lblactivityid As Label = DirectCast(rptList.Items(i).FindControl("lblactivityid"), Label)
                Dim UCExpens As usercontrol_UCExpenseActual = DirectCast(rptList.Items(i).FindControl("UCExpenseActual"), usercontrol_UCExpenseActual)
                Dim dtDetail As DataTable = UCExpens.ComponentDT
                For j As Integer = 0 To dtDetail.Rows.Count - 1
                    drH = dtH.NewRow
                    drH("id") = dtDetail.Rows(j)("id").ToString
                    drH("Payment_Date_Actual") = dtDetail.Rows(j)("Payment_Date_Actual").ToString
                    drH("Pay_Actual_Detail") = dtDetail.Rows(j)("Pay_Actual_Detail").ToString
                    drH("Payment_Actual_Detail") = dtDetail.Rows(j)("Payment_Actual_Detail").ToString
                    drH("Recipience_id") = dtDetail.Rows(j)("Payment_Actual_Detail").ToString
                    dtH.Rows.Add(drH)
                Next
            Next

            Dim dt As New DataTable
            With dt
                .Columns.Add("Header_id")
                .Columns.Add("Recipience_id")
                .Columns.Add("Expense_Sub_Actual_id")
                .Columns.Add("Pay_Amount_Actual")
                .Columns.Add("Activity_Expense_Actual_id")
            End With

            Dim dr As DataRow
            For i As Integer = 0 To rptList.Items.Count - 1
                Dim lblactivityid As Label = DirectCast(rptList.Items(i).FindControl("lblactivityid"), Label)
                Dim UCExpens As usercontrol_UCExpenseActual = DirectCast(rptList.Items(i).FindControl("UCExpenseActual"), usercontrol_UCExpenseActual)
                Dim dtDetail As DataTable = UCExpens.ExpenstListDT
                For j As Integer = 0 To dtDetail.Rows.Count - 1
                    dr = dt.NewRow
                    dr("Header_id") = dtDetail.Rows(j)("Header_id").ToString
                    dr("Recipience_id") = dtDetail.Rows(j)("Recipience_id").ToString
                    dr("Expense_Sub_Actual_id") = dtDetail.Rows(j)("Expense_Sub_Actual_id").ToString
                    dr("Pay_Amount_Actual") = dtDetail.Rows(j)("Pay_Amount_Actual").ToString
                    dr("Activity_Expense_Actual_id") = dtDetail.Rows(j)("Activity_Expense_Actual_id").ToString
                    dt.Rows.Add(dr)
                Next
            Next
            Dim ret As New ProcessReturnInfo
            ret = BL.SaveExpenseDetailActual(lblRecipience.Text, lblHeaderId.Text, dtH, dt, UserName)
            If ret.IsSuccess Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');", True)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');", True)
                Response.Redirect("frmActivityExpensePlan.aspx?PID=" & PID & "&mode=palnex" & "&type=" & type & "&ActivityID=" & ActivityID & "&HeaderId=" & HeaderId)
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('ไม่สามารถบันทึกข้อมูลได้');", True)
            End If

        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        If type = "2" Then
            Response.Redirect("frmLoan_Detail_Activity.aspx?id=" & id & "&mode=palnex" & "&type=" & type & "&mo=" & ActivityID)
        ElseIf type = "3" Then
            Response.Redirect("frmContribuiltion_Detail_Activity.aspx?id=" & id & "&mode=palnex" & "&type=" & type & "&mo=" & ActivityID)
        ElseIf type = "1" Or type = "0" Then
            Response.Redirect("frmProject_Detail_Activity.aspx?id=" & id & "&mode=palnex" & "&type=" & type & "&mo=" & ActivityID)
        Else
            Response.Redirect("frmOverduePlan.aspx")
        End If

    End Sub
End Class

