USE [TICA-ODA]
GO

/****** Object:  View [dbo].[vw_Summary_Aid_Project_NonProject]    Script Date: 8/5/2560 16:12:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE view [dbo].[vw_Summary_Aid_Project_NonProject]
AS
SELECT  budget_year,country_node_id,country_name_th
		,TB_Regionzoneoda.id Regionzone_id
		,TB_Regionzoneoda.regionoda_name Regionzone_Name
		,SUM(Project_Bachelor)		Project_Bachelor
		,SUM(Project_Training)		Project_Training
		,SUM(Project_Expert)		Project_Expert
		,SUM(Project_Volunteer)		Project_Volunteer
		,SUM(Project_Equipment)		Project_Equipment
		,SUM(Project_Other)			Project_Other
		
		,SUM(Project_Bachelor)+	
		SUM(Project_Training)+	
		SUM(Project_Expert)	+
		SUM(Project_Volunteer)+	
		SUM(Project_Equipment)+	
		SUM(Project_Other)	 	Project_Amount

		,SUM(NonProject_Bachelor)	NonProject_Bachelor
		,SUM(NonProject_Training)	NonProject_Training
		,SUM(NonProject_Expert)		NonProject_Expert
		,SUM(NonProject_Volunteer)	NonProject_Volunteer
		,SUM(NonProject_Equipment)	NonProject_Equipment
		,SUM(NonProject_Other)		NonProject_Other

		,SUM(NonProject_Bachelor)+	
		SUM(NonProject_Training)+	
		SUM(NonProject_Expert)	+	
		SUM(NonProject_Volunteer)+	
		SUM(NonProject_Equipment)	+
		SUM(NonProject_Other)		NonProject_Amount
FROM (
		SELECT	budget_year
		,project_type
				,country_node_id,country_name_th
		
				, CASE WHEN  project_type=0 THEN SUM (Bachelor)		 ELSE 0 END Project_Bachelor
				, CASE WHEN  project_type=0 THEN SUM (Training)		 ELSE 0 END Project_Training
				, CASE WHEN  project_type=0 THEN SUM (Expert)		 ELSE 0 END Project_Expert
				, CASE WHEN  project_type=0 THEN SUM (Volunteer)	 ELSE 0 END Project_Volunteer
				, CASE WHEN  project_type=0 THEN SUM (Equipment)	 ELSE 0 END Project_Equipment
				, CASE WHEN  project_type=0 THEN SUM (Other)		 ELSE 0 END Project_Other

				--=================
				, CASE WHEN  project_type=1 THEN SUM (Bachelor)		 ELSE 0 END NonProject_Bachelor
				, CASE WHEN  project_type=1 THEN SUM (Training)		 ELSE 0 END NonProject_Training
				, CASE WHEN  project_type=1 THEN SUM (Expert)		 ELSE 0 END NonProject_Expert
				, CASE WHEN  project_type=1 THEN SUM (Volunteer)	 ELSE 0 END NonProject_Volunteer
				, CASE WHEN  project_type=1 THEN SUM (Equipment)	 ELSE 0 END NonProject_Equipment
				, CASE WHEN  project_type=1 THEN SUM (Other)		 ELSE 0 END NonProject_Other

				--, Bachelor
				--, Training
				--, Expert
				--, Volunteer
				--, Equipment
				--, Other


			FROM (
			Select TB_Project.project_type  ,vw_Summary_Aid_By_Country.* 

			FROM vw_Summary_Aid_By_Country  
			LEFT JOIN TB_Project ON TB_Project.id=vw_Summary_Aid_By_Country.project_id
			) TB
		GROUP BY budget_year
		,project_type
				,country_node_id,country_name_th
				, Bachelor
				, Training
				, Expert
				, Volunteer
				, Equipment
				, Other
) TB_Project_NonProject
LEFT JOIN TB_OU_CountryZone ON TB_OU_CountryZone.country_id = TB_Project_NonProject.country_node_id

LEFT JOIN TB_Regionzoneoda ON TB_Regionzoneoda.id = TB_OU_CountryZone.country_zone_id
GROUP BY 
 budget_year,country_node_id,country_name_th
 ,TB_Regionzoneoda.id,TB_Regionzoneoda.regionoda_name



GO


