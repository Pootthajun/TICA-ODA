USE [TICA-ODA]
GO

/****** Object:  View [dbo].[vw_Project_Component]    Script Date: 8/5/2560 16:11:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE view [dbo].[vw_Project_Component]
as
-- ����Ѻ���� Component �ͧ���л���� ���¡����ç���
SELECT  DISTINCT
	budget_year,tb_Activity_Country.project_id
	,vw_AllProject.project_name
	,country_node_id,name_th country_name_th
	--,dbo.AVG_Disbursement_For_Recipient(activity_id)  AVG_Disbursement		--�������¨ӹǹ������ҳ��¤�
	--,SUM(dbo.AVG_Disbursement_For_Recipient(activity_id)*amout_person)  Disbursement_For_Country		--������ҳ���л���ȵ���ӹǹ����Ѻ�ع
	--,activity_id
	--,SUM(dbo.AVG_Disbursement_For_Recipient(activity_id)) AVG_Disbursement
	,SUM(commitment_budget) commitment_budget
	,dbo.Count_Component_For_Project(tb_Activity_Country.project_id,15)   Count_Component_Bachelor
	,dbo.Count_Component_For_Project(tb_Activity_Country.project_id,1)    Count_Component_Training
	,dbo.Count_Component_For_Project(tb_Activity_Country.project_id,6)    Count_Component_Expert
	,dbo.Count_Component_For_Project(tb_Activity_Country.project_id,7)    Count_Component_Volunteer
	,dbo.Count_Component_For_Project(tb_Activity_Country.project_id,10)   Count_Component_Equipment
	,dbo.Count_All_Component_For_Project(tb_Activity_Country.project_id)   Count_All_Component

	FROM (
		select 
			   vw_Activity_Disbursement.budget_year
			  ,vw_Activity_Disbursement.project_id
			  ,vw_Activity_Disbursement.activity_id
			  ,vw_Activity_Disbursement.activity_name
			  ,vw_Activity_Disbursement.parent_id
			  ,vw_Activity_Disbursement.commitment_budget
			  ,vw_Activity_Disbursement.Disbursement
			  ,vw_Activity_Disbursement.childs
			  ,vw_Activity_Disbursement.sector_id
			  ,vw_Activity_Disbursement.sub_sector_id
			 -------country--------------
			 ,TB_OU_Country.name_th
			 ,TB_OU_Country.name_en
			 -------Recipient--------------
			  ,TB_Country_Recipient.country_node_id
			  ,TB_Country_Recipient.amout_person

		from TB_Country_Recipient
		INNER JOIN vw_Activity_Disbursement ON vw_Activity_Disbursement.activity_id = TB_Country_Recipient.activity_id		--�¡����է�
		INNER JOIN TB_OU_Country ON TB_OU_Country.node_id=TB_Country_Recipient.country_node_id
	) AS tb_Activity_Country
LEFT JOIN vw_AllProject ON vw_AllProject.id = tb_Activity_Country.project_id
GROUP BY budget_year,tb_Activity_Country.project_id
		--,activity_id
		--,commitment_budget
		,country_node_id,name_th
		,vw_AllProject.project_name

--ORDER BY budget_year DESC,TB_Purposecat.perposecat_name,vw_AllProject.project_name



GO


