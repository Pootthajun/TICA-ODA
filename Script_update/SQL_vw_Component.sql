USE [TICA-ODA]
GO

/****** Object:  View [dbo].[vw_Component]    Script Date: 8/5/2560 16:11:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create view [dbo].[vw_Component]
as
SELECT id component_id 
	  ,component_name Desc_name
	  , 'TB_Component' Type
  FROM TB_Component 
  UNION ALL
  SELECT id component_id
	  ,inkind_name Desc_name
	  , 'TB_Inkind' Type
  FROM TB_Inkind
GO


