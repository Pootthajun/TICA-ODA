USE [TICA-ODA]
GO

/****** Object:  View [dbo].[vw_Activity_Component]    Script Date: 8/5/2560 16:09:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Script for SelectTopNRows command from SSMS  ******/

create view [dbo].[vw_Activity_Component]
AS
SELECT activity_id
      ,component_id 
	  ,TB_Component.component_name Desc_name
	  , 'TB_Component' Type
  FROM TB_Activity_Component
  LEFT JOIN TB_Component ON TB_Component.id = TB_Activity_Component.component_id
  UNION ALL
  SELECT activity_id
      ,TB_Activity_Inkind.inkind_id component_id
	  ,inkind_name Desc_name
	  , 'TB_Inkind' Type
  FROM TB_Activity_Inkind
  LEFT JOIN TB_Inkind ON TB_Inkind.id= TB_Activity_Inkind.inkind_id
GO


