﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmOrg_Chart.aspx.vb" Inherits="frmOrg_Chart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="assets/scripts/google-charts/Org_Chart.js"></script>

    <link href="assets/scripts/google-charts/Org_Chart.css" rel="stylesheet" />

    <style type="text/css">
        .chart {
            margin: 50px 15px 0;
        }
            .chart table {
                border-collapse: separate;
            }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

    <%--Organizational Chart--%>
    <div class="">
        <div id="chart_div" class="chart"></div>
    </div>
    <%--<div class="container">
        <h3 class="">โครงสร้างองค์กร</h3>
        
        <div class="structure">
            <ul>
                <li>
                    <div id="root" class="structure">
                        <p>อธิบดีกรม</p>
                    </div>
                    <ul>
                        <li>
                            <div id="node1" class="structure">
                                <p>รองอธิบดีกรม ๑</p>
                            </div>
                            <ul>
                                <li>
                                    <div id="node1-child1" class="structure">
                                        <p>ภารกิจให้ความร่วมมือเพื่อการพัฒนากับต่างประเทศ</p>
                                        <ul>
                                            <li>
                                                <div id="node1-child1-sub1" class="structure">
                                                    <p>ส่วนให้ความร่วมมือกับต่างประเทศ 1</p>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <div id="node1-child1-sub2" class="structure">
                                                    <p>ส่วนให้ความร่วมมือกับต่างประเทศ 2</p>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <div id="node1-child1-sub3" class="structure">
                                                    <p>
                                                        <span style="color: red;">*</span>
                                                        ส่วนวางแผนและติดตามผลการให้ความร่วมมือกับต่างประเทศ
                                                    </p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                            <ul>
                                <li>
                                    <div id="node1-child2" class="structure">
                                        <p>ภารกิจให้ความร่วมมือกับต่างประเทศเพื่อการพัฒนาทรัพยากรมนุษย์</p>
                                        <ul>
                                            <li>
                                                <div id="node1-child2-sub1" class="structure">
                                                    <p>ส่วนความร่วมมือเพื่อการพัฒนาทรัพยากรมนุษย์ 1</p>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <div id="node1-child2-sub2" class="structure">
                                                    <p>ส่วนความร่วมมือเพื่อการพัฒนาทรัพยากรมนุษย์ 2</p>
                                                </div>
                                            </li>
                                        </ul>

                                    </div>
                                </li>
                            </ul>
                            <ul>
                                <li>
                                    <div id="node1-child3" class="structure">
                                        <p>ภารกิจอำนวยสิทธิพิเศษและพัสดุโครงการ</p>
                                        <ul>
                                            <li>
                                                <div id="node1-child3-sub1" class="structure">
                                                    <p>ส่วนอำนวยสิทธิพิเศษและพัสดุโครงการ</p>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <div id="node1-child3-sub2" class="structure">
                                                    <p>ส่วนบริหารงบประมาณความร่วมมือเพื่อการพัฒนา</p>
                                                </div>
                                            </li>
                                        </ul>

                                    </div>
                                </li>
                            </ul>

                        </li>

                        <li>
                            <div id="node2" class="structure">
                                <p>รองอธิบดีกรม 2</p>
                            </div>
                            <ul>
                                <li>
                                    <div id="node2-child1" class="structure">
                                        <p>สำนักผู้อำนวยการ</p>
                                        <ul>
                                            <li>
                                                <div id="node2-child1-sub1" class="structure">
                                                    <p>กลุ่มงานบริหารงานทั่วไป</p>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <div id="node2-child1-sub2" class="structure">
                                                    <p>กลุ่มงานประชาสัมพันธ์</p>
                                                </div>
                                            </li>
                                        </ul>

                                    </div>
                                </li>
                            </ul>
                            <ul>
                                <li>
                                    <div id="node2-child2" class="structure">
                                        <p>ภารกิจความร่วมมือหุ้นส่วนเพื่อการพัฒนา</p>
                                        <ul>
                                            <li>
                                                <div id="node2-child2-sub1" class="structure">
                                                    <p>ส่วนความร่วมมือหุ้นส่วนทวิภาคี</p>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <div id="node2-child2-sub2" class="structure">
                                                    <p>ส่วนความร่วมมือหุ้นส่วนพหุภาคี</p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>

        <p><b>หมายเหตุ</b> <span style="color:red;">*</span><b> อยู่ภายใต้การกำกับดูแลโดยรองอธิบดี ๒ </b></p>   
                   
    </div>--%>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
</asp:Content>


