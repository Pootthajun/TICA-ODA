﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UC_GOVERNMENTAUTHORISATION.ascx.vb" Inherits="usercontrol_UC_GOVERNMENTAUTHORISATION" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<div>
    <h4 class="text-info"><b>E.GOVERNMENT AUTHORISATION</b></h4>
    <!-- BEGIN FORM-->
    <table class="table">
        <tr>
            <td colspan="2">I certify that, to the best of my knowledge,
                <br />
                (a) all information supplied by the nominee is complete and correct ;
                <br />
                (b) the nominee has adequate knowledge and experience in related fields and has adequate English proficiency for the purpose of the fellowship in Thailand.<br />
                On return from the fellowship, the nominee will be employed in the following position :
            </td>
        </tr>
        <tr>
            <th style="width: 180px"><b class="pull-right">Title of post :</b></th>
            <td>
                <asp:TextBox ID="txtTitleOfPost" runat="server" TextMode="MultiLine" Width="90%" MaxLength="500" Height="60px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <th style="width: 180px"><b class="pull-right">Duties and responsibilities :</b></th>
            <td>
                <asp:TextBox ID="txtDuties" runat="server" Width="90%" MaxLength="250"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <th style="width: 180px"><b class="pull-right">Official stamp:</b></th>
            <td>
                <div class="col">
                    <asp:TextBox ID="txtOfficialStamp" runat="server" Width="90%" MaxLength="250"></asp:TextBox></div>
                <div class="coltext"><b>Title :</b></div>
                <div class="col">
                    <asp:TextBox ID="txtTitle" runat="server" Width="90%" MaxLength="250"></asp:TextBox></div>
            </td>
        </tr>
        <tr>
            <th style="width: 180px"><b class="pull-right">Organisation :</b></th>
            <td>
                <div class="col">
                    <asp:TextBox ID="txtOrganisation" runat="server" Width="90%" MaxLength="250"></asp:TextBox></div>
                <div class="coltext"><b>Official Address  :</b></div>
                <div class="col">
                    <asp:TextBox ID="txtOrgOfficialAddress" runat="server" TextMode="MultiLine" Width="90%" MaxLength="500" Height="60px"></asp:TextBox>
                </div>

            </td>
        </tr>

        <tr>
            <th style="width: 180px"><b class="pull-right">Date :</b></th>
            <td>
                <asp:TextBox CssClass="form-control m-b" ID="txtDate" runat="server" placeholder=""></asp:TextBox>
                <ajaxtoolkit:calendarextender id="CalendarExtender3" runat="server"
                    format="dd/MM/yyyy" targetcontrolid="txtDate" popupposition="BottomLeft"></ajaxtoolkit:calendarextender>
            </td>
        </tr>
    </table>
    <hr>
    <!--<div class="control-group">
								<label class="control-label"></label>
									<div class="controls">
										<a type="submit" class="btn green"><i class="icon-ok"></i> Save </a>
										<button type="button" class="btn red">Cancel</button>
									</div>
								</div> -->


</div>
