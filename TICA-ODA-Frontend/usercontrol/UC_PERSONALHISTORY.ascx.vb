﻿Imports LinqDB.ConnectDB
Imports LinqDB.TABLE

Partial Class usercontrol_UC_PERSONALHISTORY
    Inherits System.Web.UI.UserControl
    Dim BL As New ODAENG

    'Dim _ddlPrefix_value As String
    'Dim _FamilyName As String

#Region "Property"

    Public Property Recipient_ID As Integer
        Get
            Return txtFamilyName.Attributes("Recipient_ID")
        End Get
        Set(value As Integer)
            txtFamilyName.Attributes("Recipient_ID") = value
        End Set
    End Property

    Public Property ddlActivity_value As String
        Get
            'Return ddlActivity.SelectedValue
            Return Request.QueryString("id")
        End Get
        Set(value As String)
            ddlActivity.SelectedValue = value
        End Set
    End Property

    Public Property ddlPrefix_value As String
        Get
            Return ddlPrefix.SelectedValue
        End Get
        Set(value As String)
            ddlPrefix.SelectedValue = value
        End Set
    End Property

    Public Property FamilyName As String
        Get
            Return txtFamilyName.Text
        End Get
        Set(value As String)
            txtFamilyName.Text = value
        End Set
    End Property

    Public Property GivenName As String
        Get
            Return txtGivenName.Text
        End Get
        Set(value As String)
            txtGivenName.Text = value
        End Set
    End Property

    Public Property ddlSex_value As String
        Get
            Return ddlSex.SelectedValue
        End Get
        Set(value As String)
            ddlSex.SelectedValue = value
        End Set
    End Property

    Public Property ddlCountry_value As String
        Get
            Return ddlCountry.SelectedValue
        End Get
        Set(value As String)
            ddlCountry.SelectedValue = value
        End Set
    End Property

    Public Property City As String
        Get
            Return txtCity.Text
        End Get
        Set(value As String)
            txtCity.Text = value
        End Set
    End Property

    Public Property Nationality As String
        Get
            Return txtNationality.Text
        End Get
        Set(value As String)
            txtNationality.Text = value
        End Set
    End Property

    Public Property BirthDate As String
        Get
            Return txtBirthDate.Text
        End Get
        Set(value As String)
            txtBirthDate.Text = Convert.ToDateTime(value).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
        End Set
    End Property

    Public Property ddlMaritalStatus_value As String
        Get
            Return ddlMaritalStatus.SelectedValue
        End Get
        Set(value As String)
            ddlMaritalStatus.SelectedValue = value
        End Set
    End Property

    Public Property Religion As String
        Get
            Return txtReligion.Text
        End Get
        Set(value As String)
            txtReligion.Text = value
        End Set
    End Property

    Public Property WorkAddress As String
        Get
            Return txtWorkAddress.Text
        End Get
        Set(value As String)
            txtWorkAddress.Text = value
        End Set
    End Property

    Public Property WorkTelphone As String
        Get
            Return txtWorkTelphone.Text
        End Get
        Set(value As String)
            txtWorkTelphone.Text = value
        End Set
    End Property

    Public Property Fax As String
        Get
            Return txtFax.Text
        End Get
        Set(value As String)
            txtFax.Text = value
        End Set
    End Property

    Public Property Email As String
        Get
            Return txtEmail.Text
        End Get
        Set(value As String)
            txtEmail.Text = value
        End Set
    End Property

    Public Property HomeAddress As String
        Get
            Return txtHomeAddress.Text
        End Get
        Set(value As String)
            txtHomeAddress.Text = value
        End Set
    End Property

    Public Property HomeTelephone As String
        Get
            Return txtHomeTelephone.Text
        End Get
        Set(value As String)
            txtHomeTelephone.Text = value
        End Set
    End Property

    Public Property CityDepartture As String
        Get
            Return txtCityDepartture.Text
        End Get
        Set(value As String)
            txtCityDepartture.Text = value
        End Set
    End Property

    Public Property EmergencyPerson As String
        Get
            Return txtEmergencyPerson.Text
        End Get
        Set(value As String)
            txtEmergencyPerson.Text = value
        End Set
    End Property

    Public Property EmergencyTelephone As String
        Get
            Return txtEmergencyTelephone.Text
        End Get
        Set(value As String)
            txtEmergencyTelephone.Text = value
        End Set
    End Property

    Public Property EmergencyRelationship As String
        Get
            Return txtEmergencyRelationship.Text
        End Get
        Set(value As String)
            txtEmergencyRelationship.Text = value
        End Set
    End Property
#End Region


    Private Sub usercontrol_UC_PERSONALHISTORY_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            BL.Bind_DDL_Activity(ddlActivity)

            ddlActivity.SelectedValue = Request.QueryString("id")

            BL.Bind_DDL_Prefix_En(ddlPrefix)
            BL.Bind_DDL_Sex(ddlSex)
            BL.Bind_DDL_Country_En(ddlCountry)
            BL.Bind_DDL_MaritalStatus(ddlMaritalStatus)
        End If
    End Sub

    Public Sub ClearForm()
        BL.Bind_DDL_Prefix_En(ddlPrefix)
        txtFamilyName.Text = ""
        txtGivenName.Text = ""
        BL.Bind_DDL_Prefix_En(ddlPrefix)
        BL.Bind_DDL_Country_En(ddlCountry)
        txtCity.Text = ""
        txtNationality.Text = ""
        txtBirthDate.Text = ""
        BL.Bind_DDL_MaritalStatus(ddlMaritalStatus)
        txtReligion.Text = ""
        txtWorkAddress.Text = ""
        txtWorkTelphone.Text = ""
        txtFax.Text = ""
        txtEmail.Text = ""
        txtHomeAddress.Text = ""
        txtHomeTelephone.Text = ""
        txtCityDepartture.Text = ""
        txtEmergencyPerson.Text = ""
        txtEmergencyTelephone.Text = ""
        txtEmergencyRelationship.Text = ""
    End Sub

    Private Function Validate() As Boolean
        Dim ret As Boolean = True
        'If DropDownListPlan.SelectedValue = "" Then
        '    alertmsg("กรุณาระบุแผน")
        '    ret = False
        'End If



        'BL.Bind_DDL_Prefix_En(ddlPrefix)
        'txtFamilyName.Text = ""
        'txtGivenName.Text = ""
        'BL.Bind_DDL_Prefix_En(ddlPrefix)
        'BL.Bind_DDL_Country_En(ddlCountry)
        'txtCity.Text = ""
        'txtNationality.Text = ""
        'txtBirthDate.Text = ""
        'BL.Bind_DDL_MaritalStatus(ddlMaritalStatus)
        'txtReligion.Text = ""
        'txtWorkAddress.Text = ""
        'txtWorkTelphone.Text = ""
        'txtFax.Text = ""
        'txtEmail.Text = ""
        'txtHomeAddress.Text = ""
        'txtHomeTelephone.Text = ""
        'txtCityDepartture.Text = ""
        'txtEmergencyPerson.Text = ""
        'txtEmergencyTelephone.Text = ""
        'txtEmergencyRelationship.Text = ""







        Return ret
    End Function

    'Public Sub SaveForm()

    '    If Validate() = False Then
    '        Exit Sub
    '    End If

    '    '-------Save To Table 

    '    Dim ret As New ProcessReturnInfo
    '    Dim trans As New TransactionDB
    '    Try


    '        'บันทึกข้อมูลตารางหลัก
    '        Dim exc As New ExecuteDataInfo
    '        Dim _lnqProject As New TbRecipienceLinqDB
    '        With _lnqProject
    '            .GetDataByPK(Recipient_ID, trans.Trans)

    '            .ID = Recipient_ID
    '            .ACTIVITY_ID = "167"
    '            .PERSON_ID = Recipient_ID
    '            .PSH_SEX = ps
    '            .PSH_CITY
    '            .PSH_NATIONALITY
    '            .PSH_MARITAL_STATUS
    '            .PSH_RELIGION
    '            .PSH_WORK_ADDRESS
    '            .PSH_WORK_TELEPHONE
    '            .PSH_WORK_FAX
    '            .PSH_WORK_EMAIL
    '            .PSH_HOME_ADDRESS
    '            .PSH_HOME_TELEPHONE
    '            .PSH_CITY_DEPARTTURE
    '            .PSH_EMERGENCY_CONTACT
    '            .PSH_EMERGENCY_TELEPHONE
    '            .PSH_EMERGENCY_RELATIONSHIP
    '            .EDU_TOEFLSCORE
    '            .EDU_IELTSSCORE
    '            .EDU_OTHERSCORE
    '            .EDU_TRAINED_DETAIL
    '            .EDU_PUBLICATIONS
    '            .EXP_EXPECTATION_DESCRIPTION
    '            .EXP_SIGNATURE
    '            .EXP_PRINT_NAME
    '            .EXP_PRINT_DATE
    '            .GA_TITLE_POST
    '            .GA_DUTIES
    '            .GA_STAMP
    '            .GA_TITLE
    '            .GA_ORG
    '            .GA_ADDRESS
    '            .GA_DATE
    '            .PROFILE_IMAGE
    '            .CREATED_BY
    '            .CREATED_DATE
    '            .UPDATED_BY
    '            .UPDATED_DATE
    '            .ACTIVE_STATUS





    '            .PROJECT_NAME = lnqProject.PROJECT_NAME
    '            .PROJECT_TYPE = lnqProject.PROJECT_TYPE
    '            .OBJECTIVE = lnqProject.OBJECTIVE
    '            .DESCRIPTION = lnqProject.DESCRIPTION
    '            .START_DATE = lnqProject.START_DATE
    '            .END_DATE = lnqProject.END_DATE
    '            .ALLOCATE_BUDGET = lnqProject.ALLOCATE_BUDGET
    '            '.BUDGETYEAR = lnqProject.BUDGETYEAR
    '            '.BUDGET_GROUP_ID = lnqProject.BUDGET_GROUP_ID
    '            '.BUDGET_SUB_ID = lnqProject.BUDGET_SUB_ID
    '            .COOPERATION_FRAMEWORK_ID = lnqProject.COOPERATION_FRAMEWORK_ID
    '            .COOPERATION_TYPE_ID = lnqProject.COOPERATION_TYPE_ID
    '            .OECD_AID_TYPE_ID = lnqProject.OECD_AID_TYPE_ID
    '            .LOCATION = lnqProject.LOCATION
    '            .FUNDING_AGENCY_ID = lnqProject.FUNDING_AGENCY_ID
    '            .EXECUTING_AGENCY_ID = lnqProject.EXECUTING_AGENCY_ID
    '            .ASSISTANT = lnqProject.ASSISTANT
    '            .REMARK = lnqProject.REMARK
    '            .TRANSFER_PROJECT_TO = lnqProject.TRANSFER_PROJECT_TO
    '            .IMPLEMENTING_AGENCY_ID = lnqProject.IMPLEMENTING_AGENCY_ID
    '            .REGION_OECD_ID = lnqProject.REGION_OECD_ID
    '            .OECD_ID = lnqProject.OECD_ID
    '            .MULTILATERAL_ID = lnqProject.MULTILATERAL_ID
    '            .ALLOCATE_BUDGET = lnqProject.ALLOCATE_BUDGET

    '            .IMPLEMENTING_AGENCY_LOAN = lnqProject.IMPLEMENTING_AGENCY_LOAN
    '            .GRANCE_PERIOD = lnqProject.GRANCE_PERIOD
    '            '.MATURITY_PERIOD = lnqProject.MATURITY_PERIOD
    '            .INTEREST_RATE = lnqProject.INTEREST_RATE

    '            'If .ID > 0 Then
    '            '    .PROJECT_ID = lnqProject.PROJECT_ID
    '            '    exc = .UpdateData(Username, trans.Trans)
    '            'Else
    '            '    .PROJECT_ID = GetProjectID()
    '            '    exc = .InsertData(Username, trans.Trans)
    '            'End If
    '        End With

    '        If exc.IsSuccess = False Then
    '            trans.RollbackTransaction()
    '            ret.IsSuccess = False
    '            ret.ErrorMessage = exc.ErrorMessage()
    '            Return ret
    '        End If



    '    Catch ex As Exception
    '        trans.RollbackTransaction()
    '        ret.IsSuccess = False
    '        ret.ErrorMessage = ex.ToString()
    '    End Try


    'End Sub

End Class
