﻿Imports System.Data
Partial Class usercontrol_UC_EDUCATIONRECORD
    Inherits System.Web.UI.UserControl

#Region "Property"
    Public Property MotherTongue As String
        Get
            Return txtMotherTongue.Text
        End Get
        Set(value As String)
            txtMotherTongue.Text = value
        End Set
    End Property

    Public Property MotherRead As String
        Get
            Dim val As String = ""
            If rdMotherReadEx.Checked Then
                val = "1"
            End If
            If rdMotherReadG.Checked Then
                val = "2"
            End If
            If rdMotherReadF.Checked Then
                val = "3"
            End If

            Return val
        End Get
        Set(value As String)
            If value = "1" Then
                rdMotherReadEx.Checked = True
            End If
            If value = "2" Then
                rdMotherReadG.Checked = True
            End If
            If value = "3" Then
                rdMotherReadF.Checked = True
            End If
        End Set
    End Property

    Public Property MotherWrite As String
        Get
            Dim val As String = ""
            If rdMotherWriteEx.Checked Then
                val = "1"
            End If
            If rdMotherWriteG.Checked Then
                val = "2"
            End If
            If rdMotherWriteF.Checked Then
                val = "3"
            End If

            Return val
        End Get
        Set(value As String)
            If value = "1" Then
                rdMotherWriteEx.Checked = True
            End If
            If value = "2" Then
                rdMotherWriteG.Checked = True
            End If
            If value = "3" Then
                rdMotherWriteF.Checked = True
            End If
        End Set
    End Property

    Public Property MotherSpeak As String
        Get
            Dim val As String = ""
            If rdMotherSpeakEx.Checked Then
                val = "1"
            End If
            If rdMotherSpeakG.Checked Then
                val = "2"
            End If
            If rdMotherSpeakF.Checked Then
                val = "3"
            End If

            Return val
        End Get
        Set(value As String)
            If value = "1" Then
                rdMotherSpeakEx.Checked = True
            End If
            If value = "2" Then
                rdMotherSpeakG.Checked = True
            End If
            If value = "3" Then
                rdMotherSpeakF.Checked = True
            End If
        End Set
    End Property

    Public Property EnRead As String
        Get
            Dim val As String = ""
            If rdEnReadEx.Checked Then
                val = "1"
            End If
            If rdEnReadG.Checked Then
                val = "2"
            End If
            If rdEnReadF.Checked Then
                val = "3"
            End If

            Return val
        End Get
        Set(value As String)
            If value = "1" Then
                rdEnReadEx.Checked = True
            End If
            If value = "2" Then
                rdEnReadG.Checked = True
            End If
            If value = "3" Then
                rdEnReadF.Checked = True
            End If
        End Set
    End Property

    Public Property EnWrite As String
        Get
            Dim val As String = ""
            If rdEnWriteEx.Checked Then
                val = "1"
            End If
            If rdEnWriteG.Checked Then
                val = "2"
            End If
            If rdEnWriteF.Checked Then
                val = "3"
            End If

            Return val
        End Get
        Set(value As String)
            If value = "1" Then
                rdEnWriteEx.Checked = True
            End If
            If value = "2" Then
                rdEnWriteG.Checked = True
            End If
            If value = "3" Then
                rdEnWriteF.Checked = True
            End If
        End Set
    End Property

    Public Property EnSpeak As String
        Get
            Dim val As String = ""
            If rdEnSpeakEx.Checked Then
                val = "1"
            End If
            If rdEnSpeakG.Checked Then
                val = "2"
            End If
            If rdEnSpeakF.Checked Then
                val = "3"
            End If

            Return val
        End Get
        Set(value As String)
            If value = "1" Then
                rdEnSpeakEx.Checked = True
            End If
            If value = "2" Then
                rdEnSpeakG.Checked = True
            End If
            If value = "3" Then
                rdEnSpeakF.Checked = True
            End If
        End Set
    End Property

    Public Property OtherLanguage As String
        Get
            Return txtOtherLanguage.Text
        End Get
        Set(value As String)
            txtOtherLanguage.Text = value
        End Set
    End Property

    Public Property OtherRead As String
        Get
            Dim val As String = ""
            If rdOtherReadEx.Checked Then
                val = "1"
            End If
            If rdOtherReadG.Checked Then
                val = "2"
            End If
            If rdOtherReadF.Checked Then
                val = "3"
            End If

            Return val
        End Get
        Set(value As String)
            If value = "1" Then
                rdOtherReadEx.Checked = True
            End If
            If value = "2" Then
                rdOtherReadG.Checked = True
            End If
            If value = "3" Then
                rdOtherReadF.Checked = True
            End If
        End Set
    End Property

    Public Property OtherWrite As String
        Get
            Dim val As String = ""
            If rdOtherWriteEx.Checked Then
                val = "1"
            End If
            If rdOtherWriteG.Checked Then
                val = "2"
            End If
            If rdOtherWriteF.Checked Then
                val = "3"
            End If

            Return val
        End Get
        Set(value As String)
            If value = "1" Then
                rdOtherWriteEx.Checked = True
            End If
            If value = "2" Then
                rdOtherWriteG.Checked = True
            End If
            If value = "3" Then
                rdOtherWriteF.Checked = True
            End If
        End Set
    End Property

    Public Property OtherSpeak As String
        Get
            Dim val As String = ""
            If rdOtherSpeakEx.Checked Then
                val = "1"
            End If
            If rdOtherSpeakG.Checked Then
                val = "2"
            End If
            If rdOtherSpeakF.Checked Then
                val = "3"
            End If

            Return val
        End Get
        Set(value As String)
            If value = "1" Then
                rdOtherSpeakEx.Checked = True
            End If
            If value = "2" Then
                rdOtherSpeakG.Checked = True
            End If
            If value = "3" Then
                rdOtherSpeakF.Checked = True
            End If
        End Set
    End Property

    Public Property EducationInstitution1 As String
        Get
            Return txtEducationInstitution1.Text
        End Get
        Set(value As String)
            txtEducationInstitution1.Text = value
        End Set
    End Property

    Public Property EducationCity1 As String
        Get
            Return txtEducationCity1.Text
        End Get
        Set(value As String)
            txtEducationCity1.Text = value
        End Set
    End Property

    Public Property EducationYearsFrom1 As String
        Get
            Return txtEducationYearsFrom1.Text
        End Get
        Set(value As String)
            txtEducationYearsFrom1.Text = value
        End Set
    End Property

    Public Property EducationYearsTo1 As String
        Get
            Return txtEducationYearsTo1.Text
        End Get
        Set(value As String)
            txtEducationYearsTo1.Text = value
        End Set
    End Property

    Public Property EducationDegrees1 As String
        Get
            Return txtEducationDegrees1.Text
        End Get
        Set(value As String)
            txtEducationDegrees1.Text = value
        End Set
    End Property

    Public Property EducationSpecial1 As String
        Get
            Return txtEducationSpecial1.Text
        End Get
        Set(value As String)
            txtEducationSpecial1.Text = value
        End Set
    End Property

    Public Property EducationInstitution2 As String
        Get
            Return txtEducationInstitution2.Text
        End Get
        Set(value As String)
            txtEducationInstitution2.Text = value
        End Set
    End Property

    Public Property EducationCity2 As String
        Get
            Return txtEducationCity2.Text
        End Get
        Set(value As String)
            txtEducationCity2.Text = value
        End Set
    End Property

    Public Property EducationYearsFrom2 As String
        Get
            Return txtEducationYearsFrom2.Text
        End Get
        Set(value As String)
            txtEducationYearsFrom2.Text = value
        End Set
    End Property

    Public Property EducationYearsTo2 As String
        Get
            Return txtEducationYearsTo2.Text
        End Get
        Set(value As String)
            txtEducationYearsTo2.Text = value
        End Set
    End Property

    Public Property EducationDegrees2 As String
        Get
            Return txtEducationDegrees2.Text
        End Get
        Set(value As String)
            txtEducationDegrees2.Text = value
        End Set
    End Property

    Public Property EducationSpecial2 As String
        Get
            Return txtEducationSpecial2.Text
        End Get
        Set(value As String)
            txtEducationSpecial2.Text = value
        End Set
    End Property

    Public Property EducationInstitution3 As String
        Get
            Return txtEducationInstitution3.Text
        End Get
        Set(value As String)
            txtEducationInstitution3.Text = value
        End Set
    End Property

    Public Property EducationCity3 As String
        Get
            Return txtEducationCity3.Text
        End Get
        Set(value As String)
            txtEducationCity3.Text = value
        End Set
    End Property

    Public Property EducationYearsFrom3 As String
        Get
            Return txtEducationYearsFrom3.Text
        End Get
        Set(value As String)
            txtEducationYearsFrom3.Text = value
        End Set
    End Property

    Public Property EducationYearsTo3 As String
        Get
            Return txtEducationYearsTo3.Text
        End Get
        Set(value As String)
            txtEducationYearsTo3.Text = value
        End Set
    End Property

    Public Property EducationDegrees3 As String
        Get
            Return txtEducationDegrees3.Text
        End Get
        Set(value As String)
            txtEducationDegrees3.Text = value
        End Set
    End Property

    Public Property EducationSpecial3 As String
        Get
            Return txtEducationSpecial3.Text
        End Get
        Set(value As String)
            txtEducationSpecial3.Text = value
        End Set
    End Property

    Public Property TOEFLScore As String
        Get
            Return txtTOEFLScore.Text
        End Get
        Set(value As String)
            txtTOEFLScore.Text = value
        End Set
    End Property

    Public Property IELTsScore As String
        Get
            Return txtIELTsScore.Text
        End Get
        Set(value As String)
            txtIELTsScore.Text = value
        End Set
    End Property

    Public Property OtherScore As String
        Get
            Return txtOtherScore.Text
        End Get
        Set(value As String)
            txtOtherScore.Text = value
        End Set
    End Property

    Public Property Trained As String
        Get
            Return txtTrained.Text
        End Get
        Set(value As String)
            txtTrained.Text = value
        End Set
    End Property

    Public Property Researches As String
        Get
            Return txtResearches.Text
        End Get
        Set(value As String)
            txtResearches.Text = value
        End Set
    End Property

    Public Property GetEducationDT As DataTable
        Get
            Dim DTEducation As New DataTable
            DTEducation.Columns.Add("RECORD")
            DTEducation.Columns.Add("INSTITUTION")
            DTEducation.Columns.Add("CITY")
            DTEducation.Columns.Add("ATTENDED_FROM")
            DTEducation.Columns.Add("ATTENDED_TO")
            DTEducation.Columns.Add("CERTIFICATES")
            DTEducation.Columns.Add("SPECIAL_FIELDS")

            Dim dr As DataRow
            dr = DTEducation.NewRow
            dr("RECORD") = "1"
            dr("INSTITUTION") = EducationInstitution1
            dr("CITY") = EducationCity1
            dr("ATTENDED_FROM") = EducationYearsFrom1
            dr("ATTENDED_TO") = EducationYearsTo1
            dr("CERTIFICATES") = EducationDegrees1
            dr("SPECIAL_FIELDS") = EducationSpecial1
            DTEducation.Rows.Add(dr)

            dr = DTEducation.NewRow
            dr("RECORD") = "2"
            dr("INSTITUTION") = EducationInstitution2
            dr("CITY") = EducationCity2
            dr("ATTENDED_FROM") = EducationYearsFrom2
            dr("ATTENDED_TO") = EducationYearsTo2
            dr("CERTIFICATES") = EducationDegrees2
            dr("SPECIAL_FIELDS") = EducationSpecial2
            DTEducation.Rows.Add(dr)

            dr = DTEducation.NewRow
            dr("RECORD") = "3"
            dr("INSTITUTION") = EducationInstitution3
            dr("CITY") = EducationCity3
            dr("ATTENDED_FROM") = EducationYearsFrom3
            dr("ATTENDED_TO") = EducationYearsTo3
            dr("CERTIFICATES") = EducationDegrees3
            dr("SPECIAL_FIELDS") = EducationSpecial3
            DTEducation.Rows.Add(dr)

            Return DTEducation
        End Get
        Set(value As DataTable)
            Dim DTEducation As New DataTable
            DTEducation = value
            For i As Integer = 0 To DTEducation.Rows.Count - 1
                Dim dr As DataRow = DTEducation.Rows(i)
                Select Case dr("RECORD").ToString
                    Case "1"
                        EducationInstitution1 = dr("INSTITUTION").ToString
                        EducationCity1 = dr("CITY").ToString
                        EducationYearsFrom1 = dr("ATTENDED_FROM").ToString
                        EducationYearsTo1 = dr("ATTENDED_TO").ToString
                        EducationDegrees1 = dr("CERTIFICATES").ToString
                        EducationSpecial1 = dr("SPECIAL_FIELDS").ToString
                    Case "2"
                        EducationInstitution2 = dr("INSTITUTION").ToString
                        EducationCity2 = dr("CITY").ToString
                        EducationYearsFrom2 = dr("ATTENDED_FROM").ToString
                        EducationYearsTo2 = dr("ATTENDED_TO").ToString
                        EducationDegrees2 = dr("CERTIFICATES").ToString
                        EducationSpecial2 = dr("SPECIAL_FIELDS").ToString
                    Case "3"
                        EducationInstitution3 = dr("INSTITUTION").ToString
                        EducationCity3 = dr("CITY").ToString
                        EducationYearsFrom3 = dr("ATTENDED_FROM").ToString
                        EducationYearsTo3 = dr("ATTENDED_TO").ToString
                        EducationDegrees3 = dr("CERTIFICATES").ToString
                        EducationSpecial3 = dr("SPECIAL_FIELDS").ToString
                End Select
            Next

        End Set
    End Property

    Public Property GetLanguageDT As DataTable
        Get
            Dim DTLanguage As New DataTable
            DTLanguage.Columns.Add("RECORD")
            DTLanguage.Columns.Add("LANGUAGES")
            DTLanguage.Columns.Add("SKILL_READ")
            DTLanguage.Columns.Add("SKILL_WRITE")
            DTLanguage.Columns.Add("SKILL_SPEAK")

            Dim dr As DataRow
            dr = DTLanguage.NewRow
            dr("RECORD") = "1"
            dr("LANGUAGES") = MotherTongue
            dr("SKILL_READ") = MotherRead
            dr("SKILL_WRITE") = MotherWrite
            dr("SKILL_SPEAK") = MotherSpeak
            DTLanguage.Rows.Add(dr)

            dr = DTLanguage.NewRow
            dr("RECORD") = "2"
            dr("LANGUAGES") = "English"
            dr("SKILL_READ") = EnRead
            dr("SKILL_WRITE") = EnWrite
            dr("SKILL_SPEAK") = EnSpeak
            DTLanguage.Rows.Add(dr)

            dr = DTLanguage.NewRow
            dr("RECORD") = "3"
            dr("LANGUAGES") = OtherLanguage
            dr("SKILL_READ") = OtherRead
            dr("SKILL_WRITE") = OtherWrite
            dr("SKILL_SPEAK") = OtherSpeak
            DTLanguage.Rows.Add(dr)

            Return DTLanguage
        End Get
        Set(value As DataTable)
            Dim DTLanguage As New DataTable
            DTLanguage = value

            For i As Integer = 0 To DTLanguage.Rows.Count - 1
                Dim dr As DataRow = DTLanguage.Rows(i)
                Select Case dr("RECORD").ToString
                    Case "1"
                        MotherTongue = dr("LANGUAGES").ToString
                        MotherRead = dr("SKILL_READ").ToString
                        MotherWrite = dr("SKILL_WRITE").ToString
                        MotherSpeak = dr("SKILL_SPEAK").ToString
                    Case "2"
                        EnRead = dr("SKILL_READ").ToString
                        EnWrite = dr("SKILL_WRITE").ToString
                        EnSpeak = dr("SKILL_SPEAK").ToString
                    Case "3"
                        OtherLanguage = dr("LANGUAGES").ToString
                        OtherRead = dr("SKILL_READ").ToString
                        OtherWrite = dr("SKILL_WRITE").ToString
                        OtherSpeak = dr("SKILL_SPEAK").ToString
                End Select
            Next

        End Set
    End Property


#End Region

    Public Sub ClearForm()
        txtMotherTongue.Text = ""
        rdMotherReadEx.Checked = True
        rdMotherReadG.Checked = False
        rdMotherReadF.Checked = False

        rdMotherWriteEx.Checked = True
        rdMotherWriteG.Checked = False
        rdMotherWriteF.Checked = False

        rdMotherSpeakEx.Checked = True
        rdMotherSpeakG.Checked = False
        rdMotherSpeakF.Checked = False

        rdEnReadEx.Checked = True
        rdEnReadG.Checked = False
        rdEnReadF.Checked = False

        rdEnWriteEx.Checked = True
        rdEnWriteG.Checked = False
        rdEnWriteF.Checked = False

        rdEnSpeakEx.Checked = True
        rdEnSpeakG.Checked = False
        rdEnSpeakF.Checked = False

        txtOtherLanguage.Text = ""
        rdOtherReadEx.Checked = True
        rdOtherReadG.Checked = False
        rdOtherReadF.Checked = False

        rdOtherWriteEx.Checked = True
        rdOtherWriteG.Checked = False
        rdOtherWriteF.Checked = False

        rdOtherSpeakEx.Checked = True
        rdOtherSpeakG.Checked = False
        rdOtherSpeakF.Checked = False

        txtEducationInstitution1.Text = ""
        txtEducationCity1.Text = ""
        txtEducationYearsFrom1.Text = ""
        txtEducationYearsTo1.Text = ""
        txtEducationDegrees1.Text = ""
        txtEducationSpecial1.Text = ""

        txtEducationInstitution2.Text = ""
        txtEducationCity2.Text = ""
        txtEducationYearsFrom2.Text = ""
        txtEducationYearsTo2.Text = ""
        txtEducationDegrees2.Text = ""
        txtEducationSpecial2.Text = ""

        txtEducationInstitution3.Text = ""
        txtEducationCity3.Text = ""
        txtEducationYearsFrom3.Text = ""
        txtEducationYearsTo3.Text = ""
        txtEducationDegrees3.Text = ""
        txtEducationSpecial3.Text = ""

        txtTOEFLScore.Text = ""
        txtIELTsScore.Text = ""
        txtOtherScore.Text = ""

        txtTrained.Text = ""
        txtResearches.Text = ""
    End Sub

    Dim BL As New ODAENG

    Private Sub usercontrol_UC_EDUCATIONRECORD_Load(sender As Object, e As EventArgs) Handles Me.Load
        BL.SetTextIntKeypress(txtEducationYearsFrom1)
        BL.SetTextIntKeypress(txtEducationYearsFrom2)
        BL.SetTextIntKeypress(txtEducationYearsFrom3)
        BL.SetTextIntKeypress(txtEducationYearsTo1)
        BL.SetTextIntKeypress(txtEducationYearsTo2)
        BL.SetTextIntKeypress(txtEducationYearsTo3)
        BL.SetTextIntKeypress(txtTOEFLScore)
        BL.SetTextIntKeypress(txtIELTsScore)
    End Sub
End Class
