﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UC_EDUCATIONRECORD.ascx.vb" Inherits="usercontrol_UC_EDUCATIONRECORD" %>
<div >
    <h4 class="text-info"><b>B. EDUCATION RECORD</b></h4>
    <!-- BEGIN FORM-->

    <table class="table table-bordered" style="width: 100%">
        <tr>
            <th style="text-align: center;vertical-align:middle;" class="bg-gray" rowspan="2"><b>Languages</b></th>
            <th style="width: 50px; text-align: center;vertical-align:middle;" class="bg-gray" colspan="3"><b>Read</b></th>
            <th style="width: 50px; text-align: center;vertical-align:middle;" class="bg-gray" colspan="3"><b>Write</b></th>
            <th style="width: 50px; text-align: center;vertical-align:middle;" class="bg-gray" colspan="3"><b>Speak</b></th>
        </tr>
        <tr>
            <th style="width: 50px; text-align: center;vertical-align:middle;" class="bg-gray"><b>Excellent</b></th>
            <th style="width: 50px; text-align: center;vertical-align:middle;" class="bg-gray"><b>Good</b></th>
            <th style="width: 50px; text-align: center;vertical-align:middle;" class="bg-gray"><b>Fair</b></th>
            <th style="width: 50px; text-align: center;vertical-align:middle;" class="bg-gray"><b>Excellent</b></th>
            <th style="width: 50px; text-align: center;vertical-align:middle;" class="bg-gray"><b>Good</b></th>
            <th style="width: 50px; text-align: center;vertical-align:middle;" class="bg-gray"><b>Fair</b></th>
            <th style="width: 50px; text-align: center;vertical-align:middle;" class="bg-gray"><b>Excellent</b></th>
            <th style="width: 50px; text-align: center;vertical-align:middle;" class="bg-gray"><b>Good</b></th>
            <th style="width: 50px; text-align: center;vertical-align:middle;" class="bg-gray"><b>Fair</b></th>
        </tr>
        <tr>
            <%--<td><b>Mother Tongue :</b>
                <asp:TextBox ID="txtMotherTongue" runat="server" Width="90%" MaxLength="250"></asp:TextBox>
            </td>--%>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtMotherTongue" runat="server"  MaxLength="250" placeholder="Mother Tongue"  style=" margin:0px; width :94%;"></asp:TextBox></td>
             
            <td style="margin: 0px;padding: 0px;text-align: center">
                <asp:RadioButton ID="rdMotherReadEx" runat="server" GroupName="G1" />
            </td>
            <td style="margin: 0px;padding: 0px;text-align: center">
                <asp:RadioButton ID="rdMotherReadG" runat="server" GroupName="G1" />
            </td>
            <td style="margin: 0px;padding: 0px;text-align: center">
                <asp:RadioButton ID="rdMotherReadF" runat="server" GroupName="G1" />
            </td>
            <td style="margin: 0px;padding: 0px;text-align: center">
                <asp:RadioButton ID="rdMotherWriteEx" runat="server" GroupName="G2" />
            </td>
            <td style="margin: 0px;padding: 0px;text-align: center">
                <asp:RadioButton ID="rdMotherWriteG" runat="server" GroupName="G2" />
            </td>
            <td style="margin: 0px;padding: 0px;text-align: center">
                <asp:RadioButton ID="rdMotherWriteF" runat="server" GroupName="G2" />
            </td>
            <td style="margin: 0px;padding: 0px;text-align: center">
                <asp:RadioButton ID="rdMotherSpeakEx" runat="server" GroupName="G3" />
            </td>
            <td style="margin: 0px;padding: 0px;text-align: center">
                <asp:RadioButton ID="rdMotherSpeakG" runat="server" GroupName="G3" />
            </td>
            <td style="margin: 0px;padding: 0px;text-align: center">
                <asp:RadioButton ID="rdMotherSpeakF" runat="server" GroupName="G3" />
            </td>
        </tr>
        <tr>
            <td><b>English</b>
            </td>
            <td style="text-align: center">
                <asp:RadioButton ID="rdEnReadEx" runat="server" GroupName="G4" />
            </td>
            <td style="text-align: center">
                <asp:RadioButton ID="rdEnReadG" runat="server" GroupName="G4" />
            </td>
            <td style="text-align: center">
                <asp:RadioButton ID="rdEnReadF" runat="server" GroupName="G4" />
            </td>
            <td style="text-align: center">
                <asp:RadioButton ID="rdEnWriteEx" runat="server" GroupName="G5" />
            </td>
            <td style="text-align: center">
                <asp:RadioButton ID="rdEnWriteG" runat="server" GroupName="G5" />
            </td>
            <td style="text-align: center">
                <asp:RadioButton ID="rdEnWriteF" runat="server" GroupName="G5" />
            </td>
            <td style="text-align: center">
                <asp:RadioButton ID="rdEnSpeakEx" runat="server" GroupName="G6" />
            </td>
            <td style="text-align: center">
                <asp:RadioButton ID="rdEnSpeakG" runat="server" GroupName="G6" />
            </td>
            <td style="text-align: center">
                <asp:RadioButton ID="rdEnSpeakF" runat="server" GroupName="G6" />
            </td>
        </tr>
        <tr>
            <%--<td><b>Other :</b>
                <asp:TextBox ID="txtOtherLanguage" runat="server" Width="90%" MaxLength="250"></asp:TextBox>
            </td>--%>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtOtherLanguage" runat="server"  MaxLength="250" placeholder="Other"  style=" margin:0px; width :94%;"></asp:TextBox></td>

            <td style="margin: 0px;padding: 0px;text-align: center">
                <asp:RadioButton ID="rdOtherReadEx" runat="server" GroupName="G7" />
            </td>
            <td style="margin: 0px;padding: 0px;text-align: center">
                <asp:RadioButton ID="rdOtherReadG" runat="server" GroupName="G7" />
            </td>
            <td style="margin: 0px;padding: 0px;text-align: center">
                <asp:RadioButton ID="rdOtherReadF" runat="server" GroupName="G7" />
            </td>
            <td style="margin: 0px;padding: 0px;text-align: center">
                <asp:RadioButton ID="rdOtherWriteEx" runat="server" GroupName="G8" />
            </td>
            <td style="margin: 0px;padding: 0px;text-align: center">
                <asp:RadioButton ID="rdOtherWriteG" runat="server" GroupName="G8" />
            </td>
            <td style="margin: 0px;padding: 0px;text-align: center">
                <asp:RadioButton ID="rdOtherWriteF" runat="server" GroupName="G8" />
            </td>
            <td style="margin: 0px;padding: 0px;text-align: center">
                <asp:RadioButton ID="rdOtherSpeakEx" runat="server" GroupName="G9" />
            </td>
            <td style="margin: 0px;padding: 0px;text-align: center">
                <asp:RadioButton ID="rdOtherSpeakG" runat="server" GroupName="G9" />
            </td>
            <td style="margin: 0px;padding: 0px;text-align: center">
                <asp:RadioButton ID="rdOtherSpeakF" runat="server" GroupName="G9" />
            </td>
        </tr>
    </table>


    <table class="table table-bordered"  style="width: 100%">
       
        <tr>
            <th style="text-align:center; vertical-align:middle;" rowspan="2"><b>Education Institution</b></th>
            <th style="text-align:center; vertical-align:middle;" rowspan="2"><b>City / Country</b></th>
            <th style="text-align:center; vertical-align:middle;" colspan="2"><b>Years Attended</b></th>
            <th style="text-align:center; vertical-align:middle;" rowspan="2"><b>Degrees, Diplomas <br />and Certificates</b></th>
            <th style="text-align:center; vertical-align:middle;" rowspan="2"><b>Special fields <br />of study</b></th>
        </tr>
        <tr>
            <th style="width: 50px;text-align:center; vertical-align:middle;" ><b>From</b></th>
            <th style="width: 50px;text-align:center; vertical-align:middle;" ><b>To</b></th>
        </tr>

        <tr    >        

            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationInstitution1" runat="server"  MaxLength="250"   style=" margin:0px; width :95%;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationCity1" runat="server"  MaxLength="250"          style=" margin:0px; width :95%;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationYearsFrom1" runat="server"  MaxLength="50"      style=" margin:0px; width :95%;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationYearsTo1" runat="server"   MaxLength="50"       style=" margin:0px; width :95%;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationDegrees1" runat="server"   MaxLength="250"      style=" margin:0px; width :95%;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationSpecial1" runat="server"   MaxLength="250"      style=" margin:0px; width :93%;"></asp:TextBox></td>
        </tr>
        <tr  >        

            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationInstitution2" runat="server"  MaxLength="250"   style=" margin:0px; width :95%;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationCity2" runat="server"  MaxLength="250"          style=" margin:0px; width :95%;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationYearsFrom2" runat="server"  MaxLength="50"      style=" margin:0px; width :95%;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationYearsTo2" runat="server"   MaxLength="50"       style=" margin:0px; width :95%;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationDegrees2" runat="server"   MaxLength="250"      style=" margin:0px; width :95%;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationSpecial2" runat="server"   MaxLength="250"      style=" margin:0px; width :93%;"></asp:TextBox></td>
        </tr>
        <tr  >        

            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationInstitution3" runat="server"  MaxLength="250"   style=" margin:0px; width :95%;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationCity3" runat="server"  MaxLength="250"          style=" margin:0px; width :95%;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationYearsFrom3" runat="server"  MaxLength="50"      style=" margin:0px; width :95%;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationYearsTo3" runat="server"   MaxLength="50"       style=" margin:0px; width :95%;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationDegrees3" runat="server"   MaxLength="250"      style=" margin:0px; width :95%;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationSpecial3" runat="server"   MaxLength="250"      style=" margin:0px; width :93%;"></asp:TextBox></td>
        </tr>

        </table>
<%--<div class="row">


   <table class="table table-bordered"  style="width: 100%">
       
        <tr>
            <th style="  text-align: center;" class="bg-gray" rowspan="2"><b>Education Institution</b></th>
            <th style="  text-align: center;" class="bg-gray" rowspan="2"><b>City / Country</b></th>
            <th style="  text-align: center;" class="bg-gray" colspan="2"><b>Years Attended</b></th>
            <th style="  text-align: center;" class="bg-gray" rowspan="2"><b>Degrees, Diplomas <br />and Certificates</b></th>
            <th style="  text-align: center;" class="bg-gray" rowspan="2"><b>Special fields <br />of study</b></th>
        </tr>
        <tr>
            <th style="  text-align: center;" class="bg-gray"><b>From</b></th>
            <th style="  text-align: center;" class="bg-gray"><b>To</b></th>
        </tr>

       <tr id="tr1" runat="server" visible ="false" >        

            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationInstitution1" runat="server"  MaxLength="250"   style="text-align:right; margin:0px; width :95%;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationCity1" runat="server"  MaxLength="250"          style="text-align:right; margin:0px; width :95%;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationYearsFrom1" runat="server"  MaxLength="50"      style="text-align:right; margin:0px; width :95%;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationYearsTo1" runat="server"   MaxLength="50"       style="text-align:right; margin:0px; width :95%;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationDegrees1" runat="server"   MaxLength="250"      style="text-align:right; margin:0px; width :95%;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationSpecial1" runat="server"   MaxLength="250"      style="text-align:right; margin:0px; width :95%;"></asp:TextBox></td>
        </tr>
        <tr id="tr2" runat="server" visible ="false" >
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationInstitution2" runat="server"   MaxLength="250" style="width:90%;margin: 0px;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationCity2" runat="server"   MaxLength="250" style="width:90%;margin: 0px;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationYearsFrom2" runat="server"  MaxLength="50" style="width:90%;margin: 0px;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationYearsTo2" runat="server"  MaxLength="50" style="width:90%;margin: 0px;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationDegrees2" runat="server" MaxLength="250" style="width:90%;margin: 0px;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;"><asp:TextBox ID="txtEducationSpecial2" runat="server"  MaxLength="250" style="width:90%;margin: 0px;"></asp:TextBox></td>
        </tr>
        <tr  id="tr3" runat="server" visible ="false" >
            <td style="margin: 0px;padding: 0px;">
                <asp:TextBox ID="txtEducationInstitution3" runat="server"  MaxLength="250" style="width:90%;margin: 0px;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;">
                <asp:TextBox ID="txtEducationCity3" runat="server"   MaxLength="250" style="width:90%;margin: 0px;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;">
                <asp:TextBox ID="txtEducationYearsFrom3" runat="server"  MaxLength="50" style="width:90%;margin: 0px;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;">
                <asp:TextBox ID="txtEducationYearsTo3" runat="server"   MaxLength="50" style="width:90%;margin: 0px;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;">
                <asp:TextBox ID="txtEducationDegrees3" runat="server"   MaxLength="250" style="width:90%;margin: 0px;"></asp:TextBox></td>
            <td style="margin: 0px;padding: 0px;">
                <asp:TextBox ID="txtEducationSpecial3" runat="server"  MaxLength="250" style="width:90%;margin: 0px;"></asp:TextBox></td>
        </tr>
    </table>
</div> --%>

    <table style="width: 50%">
        <tr>
            <th style="width: 230px"><b class="pull-left">English Proficiency Test :</b></th>
        </tr>
        <tr>
            <td>
                <table style="width: 100%;">
                    <tr>
                        <th style="width: 230px" class="bg-gray"><b class="pull-right">TOEFL Score :</b></th>
                        <td>
                            <div class="col-sm-12">
                                <asp:TextBox ID="txtTOEFLScore" runat="server" Width="90%" MaxLength="5"></asp:TextBox>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <th style="width: 230px" class="bg-gray"><b class="pull-right">IELTs Score :</b></th>
                        <td>
                            <div class="col-sm-12">
                                <asp:TextBox ID="txtIELTsScore" runat="server" Width="90%" MaxLength="5"></asp:TextBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 230px" class="bg-gray"><b class="pull-right">Other (specify) :</b></th>
                        <td>
                            <div class="col-sm-12">
                                <asp:TextBox ID="txtOtherScore" runat="server" Width="90%" MaxLength="250"></asp:TextBox>
                            </div>

                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>


    <table class="table">
        <tr>
            <th style="width: 230px"><b class="pull-right">Have you ever been trained in Thailand? If yes, please specify title of the course, where and for how long? </b></th>
            <td>
                <div class="col-sm-12">
                    <asp:TextBox ID="txtTrained" runat="server" TextMode="MultiLine" Width="90%" MaxLength="500" Height="60px"></asp:TextBox>
                </div>

            </td>
        </tr>
        <tr>
            <th style="width: 230px"><b class="pull-right">For a candidate for a degree program, please give a list of relevant publications/researches (do not attach details) </b></th>
            <td>
                <div class="col-sm-12">
                    <asp:TextBox ID="txtResearches" runat="server" TextMode="MultiLine" Width="90%" MaxLength="500" Height="60px"></asp:TextBox>
                </div>
            </td>
        </tr>

    </table>

    <hr>
    <!--<div class="control-group">
								    <label class="control-label"></label>
									    <div class="controls">
										    <a  href="#Emp" data-toggle="tab" type="submit" class="btn green"><i class="icon-ok"></i> Save </a>
										    <button type="button" class="btn red">Cancel</button>
									    </div>
								    </div>-->
</div>
