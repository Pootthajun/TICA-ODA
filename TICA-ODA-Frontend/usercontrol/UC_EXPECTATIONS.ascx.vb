﻿
Partial Class usercontrol_UC_EXPECTATIONS
    Inherits System.Web.UI.UserControl
#Region "Property"
    Public Property ExpectationDescription As String
        Get
            Return txtExpectationDescription.Text
        End Get
        Set(value As String)
            txtExpectationDescription.Text = value
        End Set
    End Property

    Public Property Signature As Byte()
        Get
            Dim ByteImage As Byte() = CType(Session("SignatureImage"), Byte())
            Return ByteImage
        End Get
        Set(value As Byte())
            Session("Image") = CType(value, Byte())
            SignatureImg.Attributes.Remove("src")
            btnUpdateSignatureImage_Click(Nothing, Nothing)
        End Set
    End Property

    Public Property PrintName As String
        Get
            Return txtPrintName.Text
        End Get
        Set(value As String)
            txtPrintName.Text = value
        End Set
    End Property

    Public Property PrintDate As String
        Get
            Return txtPrintDate.Text
        End Get
        Set(value As String)
            txtPrintDate.Text = Convert.ToDateTime(value).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
        End Set
    End Property

#End Region

    Public Sub ClearForm()
        txtExpectationDescription.Text = ""
        'txtSignature.Text = ""
        txtPrintName.Text = ""
        txtPrintDate.Text = ""
    End Sub

    Private Sub usercontrol_UC_EXPECTATIONS_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub

    Private Sub btnUpdateSignatureImage_Click(sender As Object, e As EventArgs) Handles btnUpdateSignatureImage.Click
        Dim ByteImage As Byte() = CType(Session("Image"), Byte())
        Dim base64String As String = Convert.ToBase64String(ByteImage, 0, ByteImage.Length)
        SignatureImg.Attributes.Remove("src")
        SignatureImg.Attributes.Add("src", "data:image/jpeg;base64," + base64String)
        Session("SignatureImage") = Session("Image")
        Session.Remove("Image")
    End Sub
End Class
