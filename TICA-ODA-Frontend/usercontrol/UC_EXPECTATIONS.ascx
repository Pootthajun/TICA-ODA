﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UC_EXPECTATIONS.ascx.vb" Inherits="usercontrol_UC_EXPECTATIONS" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<div >
    <h4 class="text-info"><b>D. EXPECTATIONS</b></h4>
    <!-- BEGIN FORM-->
    <table class="table">
        <tr>
            <td colspan="2"><b>Please describe the practical use you will make of this training/study on your return home in relation to the responsibilities you expect to assume and the conditions existing in your country in the field of your training.(give the attached paper, if necessary) </b>
                <br />
                <asp:TextBox ID="txtExpectationDescription" runat="server" TextMode="MultiLine" Width="90%" MaxLength="500" Height="60px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2"><b>REFERENCES</b>(only a candidate for a degree program please attach the recommendation letters from two persons acquainted with your academic and professional experiences.)<br />

                I certify that my statements in answer to the foregoing questions are true, complete and correct to the best of my knowledge and belief.
                <br />
                If accepted for a training award, I undertake to :-<br />
                (a)	carry out such instructions and abide by such conditions as may be stipulated by both the nominating government and the host government in respect of this course of training;<br />
                (b)	follow the course of training, and abide by the rules of the University or other institutions or establishment in which I undertake to train;<br />
                (c)	refrain from engaging in political activities, or any form of employment for profit or gain;<br />
                (d)	submit any progress reports which may be prescribed;<br />
                (e)	return to my home country promptly upon the completion of my course of training
            </td>
        </tr>
        <tr>
            <td colspan="2">I also fully understand that if I am granted a fellowship award, it may be subsequently withdrawn if I fail to make adequate progress or for other sufficient cause determined by the host Government.</td>
        </tr>
        <tr>
            <th style="width: 550px"><b class="pull-right">Signature of applicant :</b></th>
            <td>
                <%--<asp:TextBox ID="txtSignature" runat="server" Width="90%" MaxLength="250"></asp:TextBox>--%>
                <center>
                        <div class="row" style="padding-left:20px">
                            <img id="SignatureImg" runat="server" src="" style="height: 250px; width: 250px; cursor: pointer;" />
                        </div>
                        <div class="row" id="divSignatureImage" runat="server" >
                            <a href="#" onclick="$('#fulInsertSignatureImage').click();return false;"  id="btnInsertSignatureImage" runat="server">
                                <i class="icon-plus"></i>
                                <span >Add Image</span>
                            </a>
                            <asp:FileUpload ID="fulInsertSignatureImage" ClientIDMode="Static" runat="server" Style="display: none;" onchange="asyncSignatureImageInsert();" />
                            <asp:Button ID="btnUpdateSignatureImage" ClientIDMode="Static" runat="server" Style="display: none;" />
                        </div>
                  </center>
            </td>
        </tr>
        <tr>
            <th style="width: 550px"><b class="pull-right">Printed name :</b></th>
            <td>
                <asp:TextBox ID="txtPrintName" runat="server" Width="90%" MaxLength="250"></asp:TextBox></td>
        </tr>
        <tr>
            <th style="width: 550px"><b class="pull-right">Date :</b></th>
            <td>
                <asp:TextBox CssClass="form-control m-b" ID="txtPrintDate" runat="server" placeholder=""></asp:TextBox>
                <ajaxtoolkit:calendarextender id="CalendarExtender2" runat="server"
                    format="dd/MM/yyyy" targetcontrolid="txtPrintDate" popupposition="BottomLeft"></ajaxtoolkit:calendarextender>
            </td>
        </tr>
        
    </table>

    <hr>
    <!--<div class="control-group">
								<label class="control-label"></label>
									<div class="controls">
										<a type="submit" class="btn green"><i class="icon-ok"></i> Save </a>
										<button type="button" class="btn red">Cancel</button>
									</div>
								</div>-->

    <script type="text/javascript">
    //function UploadInsertSignatureImage() {
    //    alert('aaa');
    //    //$('#fulInsertSignatureImage').click();
    //   }

    //function asyncSignatureImageInsert() {
    //        var ful = $("#fulInsertSignatureImage");
    //        var filename = ful.val();
            
    //        if (filename.trim() == "")
    //            return;

    //        var filecontent = ful.prop("files")[0];
    //        if (filecontent.size > (1 * 1024 * 1024)) {
    //            //alert("ไฟล์ที่อัพโหลดต้องไม่เกิน 1 Mb");
    //            alert("File size must not larger than 12MB");
    //            return false;
    //        }

    //        if (!inFileNameValid(filename)) return;
    //        //################ Validate Image Dimension Width ajax #################
    //        var img = new Image();
    //        img.src = window.URL.createObjectURL(filecontent);
           
    //        img.onload = function () {
    //            window.URL.revokeObjectURL(img.src);
    //            if ((parseInt(img.width) > 1000) || (parseInt(img.height) > 1000)) {
    //                alert("File dimention must not larger than 1000px x 1000px");
    //                return false;
    //            } else {
    //                var formData = new FormData();
    //                formData.append("filecontent", filecontent);
    //                sendPostDataToURLUpdateSignatureImage("Image_Upload.aspx", formData);
    //            }
    //        };
    //    }

    //function sendPostDataToURLUpdateSignatureImage(url, formData) {
    //    $.ajax({
    //        url: url,
    //        type: 'POST',
    //        success: function () { $("#btnUpdateSignatureImage").click(); },
    //        data: formData,
    //        cache: false,
    //        contentType: false,
    //        processData: false
    //    });
    //}

    //function inFileNameValid(filename) {
    //        var lastDot = filename.lastIndexOf(".");
    //        var lastSlash = filename.lastIndexOf("\\");

    //        //Firefox=Windows NT 10.0; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0
    //        //ถ้า navigator.userAgent เป็น Firefox ให้กำหนดค่า lastSlash = 1 เลย
    //        var browserDetail = navigator.userAgent;
    //        if (browserDetail.indexOf("Firefox") != -1) {
    //            lastSlash = 1;
    //        }

    //        if (lastDot == -1 || lastSlash == -1) {
    //            //alert('รูปแบบชื่อไฟล์ไม่ถูกต้อง\n\nระบบรองรับไฟล์ภาพประเภท png jpg jpeg หรือ gif เท่านั้น');
    //            alert("Support only image jpeg gif png 1");
    //            return false;
    //        }

    //        var ext = filename.substring(lastDot + 1, filename.length).toLowerCase();
    //        switch (ext) {
    //            case 'jpg':
    //            case 'jpeg':
    //            case 'png':
    //            case 'gif': return true;
    //            default:
    //                //alert('รูปแบบชื่อไฟล์ไม่ถูกต้อง\n\nระบบรองรับไฟล์ภาพประเภท png jpg jpeg หรือ gif เท่านั้น');
    //                alert("Support only image jpeg gif png 2");
    //                return false;
    //        }
    //    }
  </script>

</div>


