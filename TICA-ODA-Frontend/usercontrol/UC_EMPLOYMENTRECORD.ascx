﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UC_EMPLOYMENTRECORD.ascx.vb" Inherits="usercontrol_UC_EMPLOYMENTRECORD" %>
<div >
    <h4 class="text-info"><b>C. EMPLOYMENT RECORD</b></h4>
    <!-- BEGIN FORM-->
    <table class="table">
        <tr>
            <th style="width: 200px"><b class="pull-right">Present or most recent post :</b></th>
            <td>
                <div class="col">
                    <asp:TextBox ID="txtRecentPostFrom" runat="server" MaxLength="100"></asp:TextBox>
                </div>
                <div class="coltext">
                    <b class="pull-right">To :</b>
                </div>
                <div class="col">
                    <asp:TextBox ID="txtRecentPostTo" runat="server" MaxLength="100"></asp:TextBox>
                </div>

                <!-- <input type="text" placeholder="2555" class="m-wrap small" /> <b>To</b>
                                  <input type="text" placeholder="2558" class="m-wrap small" />-->
            </td>
        </tr>
        <tr>
            <th style="width: 200px"><b class="pull-right">Title of your post :</b></th>
            <td>
                <asp:TextBox ID="txtRecentTitleOfPost" runat="server" Width="90%" MaxLength="250"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <th style="width: 200px"><b class="pull-right">Name of organisation :</b></th>
            <td>
                <asp:TextBox ID="txtRecentNameOfOrg" runat="server" Width="90%" MaxLength="250"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <th style="width: 200px"><b class="pull-right">Type of organisation :</b></th>
            <td>
                <asp:TextBox ID="txtRecentTypeOfOrg" runat="server" Width="90%" MaxLength="250"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <th style="width: 200px"><b class="pull-right">Official Address :</b></th>
            <td>
                <asp:TextBox ID="txtRecentOfficialAddress" runat="server" TextMode="MultiLine" Width="90%" MaxLength="500" Height="60px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <th style="width: 200px"><b class="pull-right">Description :</b></th>
            <td>
                <asp:TextBox ID="txtRecentDescription" runat="server" TextMode="MultiLine" Width="90%" MaxLength="500" Height="60px"></asp:TextBox>
            </td>
        </tr>
    </table>
    <table class="table">
        <tr>
            <th style="width: 200px"><b class="pull-right">Previous post : </b></th>
            <td>
                <div class="col">
                    <asp:TextBox ID="txtPreviousPostFrom" runat="server" MaxLength="100"></asp:TextBox>
                </div>
                <div class="coltext">
                    <b>To</b>
                </div>
                <div class="col">
                    <asp:TextBox ID="txtPreviousPostTo" runat="server" MaxLength="100"></asp:TextBox>
                </div>
            </td>
        </tr>
        <tr>
            <th style="width: 200px"><b class="pull-right">Title of your post :</b></th>
            <td>
                <asp:TextBox ID="txtPreviousTitleOfPost" runat="server" Width="90%" MaxLength="250"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <th style="width: 200px"><b class="pull-right">Name of organisation :</b></th>
            <td>
                <asp:TextBox ID="txtPreviousNameOfOrg" runat="server" Width="90%" MaxLength="250"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <th style="width: 200px"><b class="pull-right">Type of organisation :</b></th>
            <td>
                <asp:TextBox ID="txtPreviousTypeOfOrg" runat="server" Width="90%" MaxLength="250"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <th style="width: 200px"><b class="pull-right">Official Address :</b></th>
            <td>
                <asp:TextBox ID="txtPreviousOfficialAddress" runat="server" TextMode="MultiLine" Width="90%" MaxLength="500" Height="60px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <th style="width: 200px"><b class="pull-right">Description :</b></th>
            <td>
                <asp:TextBox ID="txtPreviousDescription" runat="server" TextMode="MultiLine" Width="90%" MaxLength="500" Height="60px"></asp:TextBox>
            </td>
        </tr>
    </table>
    <hr>
    <!--<div class="control-group">
								<label class="control-label"></label>
									<div class="controls">
										<a type="submit" class="btn green"><i class="icon-ok"></i> Save </a>
										<button type="button" class="btn red">Cancel</button>
									</div>
								</div>-->

</div>
