﻿
Partial Class usercontrol_UC_GOVERNMENTAUTHORISATION
    Inherits System.Web.UI.UserControl
#Region "Property"
    Public Property TitleOfPost As String
        Get
            Return txtTitleOfPost.Text
        End Get
        Set(value As String)
            txtTitleOfPost.Text = value
        End Set
    End Property

    Public Property Duties As String
        Get
            Return txtDuties.Text
        End Get
        Set(value As String)
            txtDuties.Text = value
        End Set
    End Property

    Public Property OfficialStamp As String
        Get
            Return txtOfficialStamp.Text
        End Get
        Set(value As String)
            txtOfficialStamp.Text = value
        End Set
    End Property
    Public Property Title As String
        Get
            Return txtTitle.Text
        End Get
        Set(value As String)
            txtTitle.Text = value
        End Set
    End Property

    Public Property Organisation As String
        Get
            Return txtOrganisation.Text
        End Get
        Set(value As String)
            txtOrganisation.Text = value
        End Set
    End Property

    Public Property OrgOfficialAddress As String
        Get
            Return txtOrgOfficialAddress.Text
        End Get
        Set(value As String)
            txtOrgOfficialAddress.Text = value
        End Set
    End Property

    Public Property txtDate_value As String
        Get
            Return txtDate.Text
        End Get
        Set(value As String)
            txtDate.Text = Convert.ToDateTime(value).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
        End Set
    End Property

#End Region
    Public Sub ClearForm()
        txtTitleOfPost.Text = ""
        txtDuties.Text = ""
        txtOfficialStamp.Text = ""
        txtTitle.Text = ""
        txtOrganisation.Text = ""
        txtOrgOfficialAddress.Text = ""
        txtDate.Text = ""
    End Sub
    Private Sub usercontrol_UC_GOVERNMENTAUTHORISATION_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub
End Class
