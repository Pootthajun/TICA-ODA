﻿Imports System.Data
Partial Class usercontrol_UC_EMPLOYMENTRECORD
    Inherits System.Web.UI.UserControl
#Region "Property"
    Public Property RecentPostFrom As String
        Get
            Return txtRecentPostFrom.Text
        End Get
        Set(value As String)
            txtRecentPostFrom.Text = value
        End Set
    End Property

    Public Property RecentPostTo As String
        Get
            Return txtRecentPostTo.Text
        End Get
        Set(value As String)
            txtRecentPostTo.Text = value
        End Set
    End Property

    Public Property RecentTitleOfPost As String
        Get
            Return txtRecentTitleOfPost.Text
        End Get
        Set(value As String)
            txtRecentTitleOfPost.Text = value
        End Set
    End Property

    Public Property RecentNameOfOrg As String
        Get
            Return txtRecentNameOfOrg.Text
        End Get
        Set(value As String)
            txtRecentNameOfOrg.Text = value
        End Set
    End Property

    Public Property RecentTypeOfOrg As String
        Get
            Return txtRecentTypeOfOrg.Text
        End Get
        Set(value As String)
            txtRecentTypeOfOrg.Text = value
        End Set
    End Property

    Public Property RecentOfficialAddress As String
        Get
            Return txtRecentOfficialAddress.Text
        End Get
        Set(value As String)
            txtRecentOfficialAddress.Text = value
        End Set
    End Property

    Public Property RecentDescription As String
        Get
            Return txtRecentDescription.Text
        End Get
        Set(value As String)
            txtRecentDescription.Text = value
        End Set
    End Property

    Public Property PreviousPostFrom As String
        Get
            Return txtPreviousPostFrom.Text
        End Get
        Set(value As String)
            txtPreviousPostFrom.Text = value
        End Set
    End Property

    Public Property PreviousPostTo As String
        Get
            Return txtPreviousPostTo.Text
        End Get
        Set(value As String)
            txtPreviousPostTo.Text = value
        End Set
    End Property

    Public Property PreviousTitleOfPost As String
        Get
            Return txtPreviousTitleOfPost.Text
        End Get
        Set(value As String)
            txtPreviousTitleOfPost.Text = value
        End Set
    End Property

    Public Property PreviousNameOfOrg As String
        Get
            Return txtPreviousNameOfOrg.Text
        End Get
        Set(value As String)
            txtPreviousNameOfOrg.Text = value
        End Set
    End Property

    Public Property PreviousTypeOfOrg As String
        Get
            Return txtPreviousTypeOfOrg.Text
        End Get
        Set(value As String)
            txtPreviousTypeOfOrg.Text = value
        End Set
    End Property

    Public Property PreviousOfficialAddress As String
        Get
            Return txtPreviousOfficialAddress.Text
        End Get
        Set(value As String)
            txtPreviousOfficialAddress.Text = value
        End Set
    End Property

    Public Property PreviousDescription As String
        Get
            Return txtPreviousDescription.Text
        End Get
        Set(value As String)
            txtPreviousDescription.Text = value
        End Set
    End Property

    Public Property GetEmpRecpordDT As DataTable
        Get
            Dim DTEmpRecpord As New DataTable
            DTEmpRecpord.Columns.Add("RECORD")
            DTEmpRecpord.Columns.Add("POST_FROM")
            DTEmpRecpord.Columns.Add("POST_TO")
            DTEmpRecpord.Columns.Add("TITLE_POST")
            DTEmpRecpord.Columns.Add("ORG_NAME")
            DTEmpRecpord.Columns.Add("ORG_TYPE")
            DTEmpRecpord.Columns.Add("ADDRESS")
            DTEmpRecpord.Columns.Add("DESCRIPTION")

            Dim dr As DataRow
            dr = DTEmpRecpord.NewRow
            dr("RECORD") = "1"
            dr("POST_FROM") = RecentPostFrom
            dr("POST_TO") = RecentPostTo
            dr("TITLE_POST") = RecentTitleOfPost
            dr("ORG_NAME") = RecentNameOfOrg
            dr("ORG_TYPE") = RecentTypeOfOrg
            dr("ADDRESS") = RecentOfficialAddress
            dr("DESCRIPTION") = RecentDescription
            DTEmpRecpord.Rows.Add(dr)

            dr = DTEmpRecpord.NewRow
            dr("RECORD") = "2"
            dr("POST_FROM") = PreviousPostFrom
            dr("POST_TO") = PreviousPostTo
            dr("TITLE_POST") = PreviousTitleOfPost
            dr("ORG_NAME") = PreviousNameOfOrg
            dr("ORG_TYPE") = PreviousTypeOfOrg
            dr("ADDRESS") = PreviousOfficialAddress
            dr("DESCRIPTION") = PreviousDescription
            DTEmpRecpord.Rows.Add(dr)

            Return DTEmpRecpord
        End Get
        Set(value As DataTable)
            Dim DTEmpRecpord As New DataTable
            DTEmpRecpord = value

            For i As Integer = 0 To DTEmpRecpord.Rows.Count - 1
                Dim dr As DataRow = DTEmpRecpord.Rows(i)
                Select Case dr("RECORD").ToString
                    Case "1"
                        RecentPostFrom = dr("POST_FROM")
                        RecentPostTo = dr("POST_TO")
                        RecentTitleOfPost = dr("TITLE_POST")
                        RecentNameOfOrg = dr("ORG_NAME")
                        RecentTypeOfOrg = dr("ORG_TYPE")
                        RecentOfficialAddress = dr("ADDRESS")
                        RecentDescription = dr("DESCRIPTION")
                    Case "2"
                        PreviousPostFrom = dr("POST_FROM")
                        PreviousPostTo = dr("POST_TO")
                        PreviousTitleOfPost = dr("TITLE_POST")
                        PreviousNameOfOrg = dr("ORG_NAME")
                        PreviousTypeOfOrg = dr("ORG_TYPE")
                        PreviousOfficialAddress = dr("ADDRESS")
                        PreviousDescription = dr("DESCRIPTION")
                End Select
            Next
        End Set
    End Property

#End Region

    Public Sub ClearForm()
        txtRecentPostFrom.Text = ""
        txtRecentPostTo.Text = ""
        txtRecentTitleOfPost.Text = ""
        txtRecentNameOfOrg.Text = ""
        txtRecentTypeOfOrg.Text = ""
        txtRecentOfficialAddress.Text = ""
        txtRecentDescription.Text = ""
        txtPreviousPostFrom.Text = ""
        txtPreviousPostTo.Text = ""
        txtPreviousTitleOfPost.Text = ""
        txtPreviousNameOfOrg.Text = ""
        txtPreviousTypeOfOrg.Text = ""
        txtPreviousOfficialAddress.Text = ""
        txtPreviousDescription.Text = ""
    End Sub

    Dim BL As New ODAENG

    Private Sub usercontrol_UC_EMPLOYMENTRECORD_Load(sender As Object, e As EventArgs) Handles Me.Load
        BL.SetTextIntKeypress(txtRecentPostFrom)
        BL.SetTextIntKeypress(txtRecentPostTo)
        BL.SetTextIntKeypress(txtPreviousPostFrom)
        BL.SetTextIntKeypress(txtPreviousPostTo)
    End Sub


End Class
