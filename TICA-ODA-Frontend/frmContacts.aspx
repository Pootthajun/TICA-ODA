﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmContacts.aspx.vb" Inherits="frmContacts" %>

<%--Header part--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <style>

        .white {
            color: #fff;
        }

        /* Main */
        .main{
            font-size: 14px;     
            color: #fff;      
            background-color: #0693cf; 
        }              

        form {
            flex: 1;      
            
            background: url('/assets/img/bg/Location_BG.JPG') no-repeat center center fixed; 
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;     
            
            padding: 63px 0 0;
            
            height: auto;
            position: relative;
            overflow: hidden;           
        }
        
    </style>

    <script type="text/javascript" async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBiq-DMWPHElrMDkYHfwtLP62JqV-e31_Q&callback=initialize"></script>

</asp:Content>

<%--Body part--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    
    <div class="container">
        <div class="row-fluid main " style="max-height: 400px;">

            <%--Add Google Map--%>
            <div id="googleMap" class="span7" style="height: 400px;"></div>  

            <%--Detail--%>
            <div class="span5 main front-lists-v3" style="padding: 15px; max-height:400px;">
                <h3 class="white">สถานที่ตั้งหน่วยงาน</h3>
                <p>
                    กรมความร่วมมือระหว่างประเทศ <br/> <i>(สำนักงานความร่วมมือเพื่อการพัฒนาระหว่างประเทศเดิม)</i>                            
                </p>

                <%--building--%>
                <ul class="unstyled">                            
                    <li><i class="icon-map-marker"></i>ศูนย์ราชการเฉลิมพระเกียรติ ๘๐ พรรษา ๕ ธันวาคม ๒๕๕๐</li>
                    <li><i class="icon-building"></i>อาคารรัฐประศาสนภักดี (อาคาร B) ฝั่งทิศใต้ ชั้น ๘</li>
                    <li><i class="icon-road"></i>ถนนแจ้งวัฒนะ แขวงทุ่งสองห้อง เขตหลักสี่ กรุงเทพฯ ๑๐๒๑๐</li>     
                </ul>

                <%--contract--%>
                <ul class="unstyled">                               
                    <li><i class="icon-phone"></i>โทร. ๐๒ ๒๐๓ ๕๐๐๐</li>                            
                    <li><i class="icon-comments"></i>Fax. ๐๒ ๑๔๓ ๙๓๒๗</li>
                    <li><i class="icon-envelope-alt"></i>E-mail : tica@mfa.go.th</li>
                </ul>

            </div> 

        </div>
    </div> 
                
</asp:Content>

<%--JS part--%>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <!-- Java Script Google Map -->
    <script type="text/javascript">        

        // <!-- Google Map Location -->
        var myCenter = new google.maps.LatLng(13.881412, 100.564897);

        function initialize() {
        var mapDetail = {
            center: myCenter,
            zoom: 12,
            scrollwheel: true,
            draggable: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP
            };

        var map = new google.maps.Map(document.getElementById("googleMap"), mapDetail);

        var marker = new google.maps.Marker({
            position: myCenter,
        });

        marker.setMap(map);
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</asp:Content>

