﻿Imports System.Data
Imports System.Data.SqlClient
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Partial Class MasterPage
    Inherits System.Web.UI.MasterPage

    Private Sub MasterPage_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindData()
        End If
    End Sub

    Protected Sub BindData()
        Dim sql As String = "SELECT id,govnamethai,govnameeng,kong,govnumber,govroad,tambol,ampher,province,postcode"
        sql += ",telephone,fax,website,detail,ministry,email,ACTIVE_STATUS"
        sql += " FROM tb_contactgoverment WHERE id=1"
        Dim trans As New TransactionDB
        'Dim lnq As New WebContactgovermentLinqDB
        Dim p(1) As SqlParameter

        p(0) = SqlDB.SetBigInt("@_ID", 1)
        Dim dt As DataTable = SqlDB.ExecuteTable(sql, trans.Trans, p)

        If dt.Rows.Count > 0 Then
            trans.CommitTransaction()
            For i As Integer = 0 To dt.Rows.Count - 1
                If dt.Rows(i)("id").ToString() <> "" Then
                    lblGOVEN.Text = dt(i)("govnameeng").ToString()
                    lblBuliding.Text = dt(i)("govnumber").ToString()
                    lblRoad.Text = dt(i)("govroad").ToString()
                    lblProvice.Text = dt(i)("province").ToString()

                    lblZipCode.Text = dt(i)("postcode").ToString()
                    lblTelephone.Text = dt(i)("telephone").ToString()

                    lblFax.Text = dt(i)("fax").ToString()
                    lblWebSite.Text = dt(i)("website").ToString()

                    'lblEmail.Text = dt(i)("email").ToString()
                    lblMinistry.Text = dt(i)("ministry").ToString()

                End If
            Next

        Else
            trans.RollbackTransaction()
            Dim _err = trans.ErrorMessage()
        End If
    End Sub
End Class

