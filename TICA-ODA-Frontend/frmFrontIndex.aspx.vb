﻿Imports System.Data
Imports LinqDB.TABLE
Imports LinqDB.ConnectDB

Partial Class frmFrontIndex
    Inherits System.Web.UI.Page

    Private Sub frmFrontIndex_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuIndex")
        'li.Attributes.Add("class", "active")

        BindList()
    End Sub


    Private Sub BindList()

        Dim sql As String = ""
        'Dim sql As String = " select id,activity_name from tb_activity where notify ='Y' and isnull(activity_name,'') <> '' order by activity_name"

        sql &= " select tb_activity.id,activity_name,TB_Project.project_name  " & vbLf
        sql &= " from tb_activity  " &vbLf
        sql &= " LEFT JOIN  TB_Project ON TB_Project.id = tb_activity.project_id  " & vbLf
        sql &= " where notify ='Y' and isnull(activity_name,'') <> ''   " &vbLf
        sql &= " order by activity_name  " & vbLf

        Dim DT As DataTable = SqlDB.ExecuteTable(sql)

        rptList.DataSource = DT
        rptList.DataBind()

        Session("Activity_News") = DT

        Pager.SesssionSourceName = "Activity_News"
        Pager.RenderLayout()
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Dim Lastbudget_year As String = ""
    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lbl_Activity_News As Label = DirectCast(e.Item.FindControl("lbl_Activity_News"), Label)
        Dim lbl_Project_Name As Label = DirectCast(e.Item.FindControl("lbl_Project_Name"), Label)
        Dim lnk_Register As LinkButton = DirectCast(e.Item.FindControl("lnk_Register"), LinkButton)


        lbl_Activity_News.Text = e.Item.DataItem("activity_name").ToString
        lbl_Project_Name.Text = e.Item.DataItem("project_name").ToString


        lnk_Register.CommandArgument = e.Item.DataItem("id")
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lnk_Register As LinkButton = DirectCast(e.Item.FindControl("lnk_Register"), LinkButton)

        Select Case e.CommandName
            Case "lnk_Select"
                Response.Redirect("frmFrontRecipient.aspx?id=" & lnk_Register.CommandArgument)

        End Select
    End Sub
End Class
