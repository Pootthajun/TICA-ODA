﻿Imports Microsoft.VisualBasic

Public Class Constants
    Public Enum OU
        Country = 1
        Organize = 2
        Person = 3
    End Enum

    Public Enum Contact_Parent_Type
        Oraganize = 0
        Person = 1
        Project = 2
    End Enum

    Public Enum Person_Type
        Officer = 0
        Recipient = 1
    End Enum

    Public Enum AgencyType
        FunddingAgency = 0
        ExcutingAgency = 1
        ImplementingAgency = 2
    End Enum

    Public Enum Project_Type
        Project = 0
        NonProject = 1
        Loan = 2
        Contribuition = 3
    End Enum

    'Structure Prefix_Language
    '    Const Thai = "T"
    '    Const English = "E"
    'End Structure



End Class
