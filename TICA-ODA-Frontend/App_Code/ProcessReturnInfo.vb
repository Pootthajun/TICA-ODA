﻿Imports Microsoft.VisualBasic

Public Class ProcessReturnInfo
    Dim _IsSuccess As Boolean = False
    Dim _ErrorMessage As String = ""
    Dim _ID As String = ""
    Dim _PROJECT_ID As String = ""

    Public Property IsSuccess As Boolean
        Get
            Return _IsSuccess
        End Get
        Set(value As Boolean)
            _IsSuccess = value
        End Set
    End Property

    Public Property ErrorMessage As String
        Get
            Return _ErrorMessage.Trim
        End Get
        Set(value As String)
            _ErrorMessage = value
        End Set
    End Property

    Public Property ID As Long
        Get
            Return _ID
        End Get
        Set(value As Long)
            _ID = value
        End Set
    End Property

    Public Property PROJECT_ID As String
        Get
            Return _PROJECT_ID
        End Get
        Set(value As String)
            _PROJECT_ID = value
        End Set
    End Property
End Class
