﻿Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Partial Class _Default
    Inherits System.Web.UI.Page
    Dim PrintDate As String = ""
    Dim Username As String = ""
    Dim Reportname As String = ""
    Dim ReportFormat As String = ""
    Dim ReportPath As String = ""

    Dim BL As New ODAENG
    Dim BL1 As New ODAENG
    Private Sub Preview1_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim ReportName As String = "Reportpj" '"rptIncommingBudget"
        Dim ReportFormat As String = "PDF"

        PrintDate = BL.GetTHDateAbbr(DateTime.Now.ToString("yyyyMMdd", New System.Globalization.CultureInfo("en-US"))) + " " + DateTime.Now.ToString("HH:mm", New System.Globalization.CultureInfo("en-US"))
        Username = Session("Username")


        'ReportName = Request.QueryString("Reportname")
        'ReportFormat = Request.QueryString("ReportFormat")
        Dim home_floder As String = System.Web.HttpContext.Current.Request.ApplicationPath + "/"
        ReportPath = Server.MapPath(home_floder + "/" + ReportName + ".rpt")

        PrintReport(ReportName, ReportFormat, ReportPath)
    End Sub

    Private Sub PrintReport(reportname As String, ReportFormat As String, ReportPath As String)
        Dim rpt As ReportDocument = New ReportDocument()
        Dim notfoundmsg As String = "ไม่พบข้อมูล"
        Select Case reportname
            Case "Reportpj"
                rptReportpj(rpt, ReportFormat, ReportPath, notfoundmsg)
            Case Else
                lblErrorMessage.Text = notfoundmsg
                CrystalReportViewer1.Visible = False
        End Select
    End Sub

    Sub rptReportpj(_rpt As ReportDocument, _ReportFormat As String, _ReportPath As String, _notfoundmsg As String)
        'Dim project_id As String = Request.QueryString("project_id").ToString
        Dim project_name As String = ""
        Dim start_datae As String = ""
        Dim end_date As String = ""
        Dim budget As String = ""


        'for test
        Dim project_id As String = "85"

        Dim dtpj As DataTable = BL.GetProject(project_id)
        project_name = dtpj.Rows(0)("project_name").ToString
        start_datae = dtpj.Rows(0)("strStartP_date").ToString
        end_date = dtpj.Rows(0)("strEndP_date").ToString
        budget = dtpj(0)("budget").ToString

            Dim dtpjdetail As DataTable = BL.GetAllActivityByProject(project_id)
        If dtpjdetail.Rows.Count > 0 Then
            lblErrorMessage.Text = ""

            _rpt.Load(_ReportPath)
            _rpt.DataDefinition.FormulaFields("PrintDate").Text = "'" + PrintDate + "'"
            _rpt.DataDefinition.FormulaFields("project_name").Text = "'" + project_name + "'"
            _rpt.DataDefinition.FormulaFields("start_datae").Text = "'" + start_datae + "'"
            _rpt.DataDefinition.FormulaFields("end_date").Text = "'" + end_date + "'"
            _rpt.DataDefinition.FormulaFields("budget").Text = "'" + budget + "'"
            _rpt.SetDataSource(dtpjdetail)

            If _ReportFormat = "EXCEL" Then
                CreateExcelReport(_rpt, Reportname)
            End If
            If _ReportFormat = "PDF" Then
                CreatePDFReport(_rpt, Reportname)
            End If
        End If

    End Sub

    Private Sub CreateExcelReport(rpt As ReportDocument, ReportName As String)
        Try
            rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, True, ReportName)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub CreatePDFReport(rpt As ReportDocument, ReportName As String)
        Dim oStream As System.IO.Stream = Nothing
        oStream = rpt.ExportToStream(ExportFormatType.PortableDocFormat)
        Dim byteArray(oStream.Length) As Byte
        oStream.Read(byteArray, 0, Convert.ToInt32(oStream.Length - 1))
        Response.ClearContent()
        Response.ClearHeaders()
        Response.ContentType = "application/pdf"
        Response.AddHeader("Content-Disposition", "inline; filename=" + ReportName + ".pdf")
        Response.BinaryWrite(byteArray)
        Response.Flush()
        Response.Close()
        rpt.Close()
        rpt.Dispose()
    End Sub
End Class
