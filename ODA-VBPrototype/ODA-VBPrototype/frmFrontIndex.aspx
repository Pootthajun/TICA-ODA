﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmFrontMaster.Master" CodeBehind="frmFrontIndex.aspx.vb" Inherits="ODA_VBPrototype.frmFrontIndex" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>กรมความร่วมมือระหว่างประเทศ | หน้าหลัก</title>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
      <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <li class="active"><a href="frmFrontIndex.aspx">หน้าหลัก</a></li>
                            <li>
                                <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="frmAbout.aspx">
                                    เกี่ยวกับกรมฯ
                                    <i class="icon-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="frmFrontTICAAbout.aspx">ประวัติกรมความร่วมมือระหว่างประเทศ</a></li>
                                    <li><a href="frmFrontPolicyPlan.aspx">วิสัยทัศน์ พันธกิจ สัญลักษณ์</a></li>
                                    <li><a href="frmFrontOrganization">โครงสร้างองค์กร</a></li>
                                    <li><a href="frmFrontPerformance.aspx">ผลการดำเนินงานของกรม</a></li>
                                </ul>
                            </li>
                            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="frmAbout.aspx">
                                    ศูนย์ข่าว
                                    <i class="icon-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="frmFrontNews.aspx">ข่าวทั่วไป</a></li>
                                    <li><a href="frmFrontNewsODA.aspx">ข่าวกรมฯ</a></li>
                            </ul></li>
                            <li><a href="frmFrontRecipient.aspx">เอกสารสมัครรับทุน</a></li>  
                            <li><a href="frmFrontRecipient.aspx">ติดต่อกรมฯ</a></li>                        
                          
                          
                         </ul>    
                    </div>
                    <!-- BEGIN TOP NAVIGATION MENU -->
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <div class="fullwidบาทanner-container slider-main margin-bottom-10">
        <div class="fullwidthabnner">
            <ul id="revolutionul" style="display:none;">            
                    <!-- THE FIRST SLIDE -->
                    <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400" data-thumb="assets/img/sliders/revolution/thumbs/thumb2.jpg">
                        <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                        <img src="assets/img/sliders/slid/bg1.png" alt="">
                        
                        <div class="caption lft slide_title slide_item_left"
                             data-x="0"
                             data-y="125"
                             data-speed="400"
                             data-start="1500"
                             data-easing="easeOutExpo">
                             Need a website design ? 
                        </div>
                        <div class="caption lft slide_subtitle slide_item_left"
                             data-x="0"
                             data-y="180"
                             data-speed="400"
                             data-start="2000"
                             data-easing="easeOutExpo">
                             This is what you were looking for
                        </div>
                        <div class="caption lft slide_desc slide_item_left"
                             data-x="0"
                             data-y="220"
                             data-speed="400"
                             data-start="2500"
                             data-easing="easeOutExpo">
                             Lorem ipsum dolor sit amet, dolore eiusmod<br>
                             quis tempor incididunt. Sed unde omnis iste.
                        </div>
                        <a class="caption lft btn green slide_btn slide_item_left" href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes"
                             data-x="0"
                             data-y="290"
                             data-speed="400"
                             data-start="3000"
                             data-easing="easeOutExpo">
                             Purchase Now!
                        </a>    <%--                    
                        <div class="caption lfb"
                             data-x="640" 
                             data-y="55" 
                             data-speed="700" 
                             data-start="1000" 
                             data-easing="easeOutExpo"  >
                             <img src="assets/img/sliders/revolution/man-winner.png" alt="Image 1">
                        </div>--%>
                    </li>

                    <!-- THE SECOND SLIDE -->
                    <li data-transition="fade" data-slotamount="7" data-masterspeed="300" data-delay="9400" data-thumb="assets/img/sliders/revolution/thumbs/thumb2.jpg">                        
                        <img src="assets/img/sliders/slid/bg2.png" alt="">
                        <div class="caption lfl slide_title slide_item_left"
                             data-x="0"
                             data-y="145"
                             data-speed="400"
                             data-start="3500"
                             data-easing="easeOutExpo">
                             Powerfull & Clean
                        </div>
                        <div class="caption lfl slide_subtitle slide_item_left"
                             data-x="0"
                             data-y="200"
                             data-speed="400"
                             data-start="4000"
                             data-easing="easeOutExpo">
                             Responsive Admin & Website Theme
                        </div>
                        <div class="caption lfl slide_desc slide_item_left"
                             data-x="0"
                             data-y="245"
                             data-speed="400"
                             data-start="4500"
                             data-easing="easeOutExpo">
                             Lorem ipsum dolor sit amet, consectetuer elit sed diam<br> nonummy amet euismod dolore.
                        </div>                        
                      <div class="caption lfr slide_item_right" 
                             data-x="635" 
                             data-y="105" 
                             data-speed="1200" 
                             data-start="1500" 
                             data-easing="easeOutBack">
                             <img src="assets/img/sliders/revolution/mac.png" alt="Image 1">
                        </div>
                         <%-- <div class="caption lfr slide_item_right" 
                             data-x="580" 
                             data-y="245" 
                             data-speed="1200" 
                             data-start="2000" 
                             data-easing="easeOutBack">
                             <img src="assets/img/sliders/revolution/ipad.png" alt="Image 1">
                        </div>
                        <div class="caption lfr slide_item_right" 
                             data-x="735" 
                             data-y="290" 
                             data-speed="1200" 
                             data-start="2500" 
                             data-easing="easeOutBack">
                             <img src="assets/img/sliders/revolution/iphone.png" alt="Image 1">
                        </div>
                        <div class="caption lfr slide_item_right" 
                             data-x="835" 
                             data-y="230" 
                             data-speed="1200" 
                             data-start="3000" 
                             data-easing="easeOutBack">
                             <img src="assets/img/sliders/revolution/macbook.png" alt="Image 1">
                        </div>--%>
                        <div class="caption lft slide_item_right" 
                             data-x="865" 
                             data-y="45" 
                             data-speed="500" 
                             data-start="5000" 
                             data-easing="easeOutBack">
                             <img src="assets/img/sliders/revolution/hint1-blue.png" id="rev-hint1" alt="Image 1">
                        </div>                        
                        <div class="caption lfb slide_item_right" 
                             data-x="355" 
                             data-y="355" 
                             data-speed="500" 
                             data-start="5500" 
                             data-easing="easeOutBack">
                             <img src="assets/img/sliders/revolution/hint2-blue.png" id="rev-hint2" alt="Image 1">
                        </div>

                    </li>
                    
                    <!-- THE THIRD SLIDE -->
                    <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400" data-thumb="assets/img/sliders/revolution/thumbs/thumb2.jpg">
                        <img src="assets/img/sliders/slid/bg3.png" alt="">
                        <div class="caption lfl slide_item_left" 
                             data-x="20" 
                             data-y="95" 
                             data-speed="400" 
                             data-start="1500" 
                             data-easing="easeOutBack">
                             <iframe src="http://player.vimeo.com/video/56974716?portrait=0" width="420" height="240" style="border:0" allowFullScreen></iframe> 
                        </div>
                        <div class="caption lfr slide_title"
                             data-x="470"
                             data-y="120"
                             data-speed="400"
                             data-start="2000"
                             data-easing="easeOutExpo">
                             Responsive Video Support
                        </div>
                        <div class="caption lfr slide_subtitle"
                             data-x="470"
                             data-y="170"
                             data-speed="400"
                             data-start="2500"
                             data-easing="easeOutExpo">
                             Youtube, Vimeo and others.
                        </div>
                        <div class="caption lfr slide_desc"
                             data-x="470"
                             data-y="220"
                             data-speed="400"
                             data-start="3000"
                             data-easing="easeOutExpo">
                             Lorem ipsum dolor sit amet, consectetuer elit sed diam<br> nonummy amet euismod dolore.
                        </div>
                        <a class="caption lfr btn yellow slide_btn" href="#"
                             data-x="470"
                             data-y="280"
                             data-speed="400"
                             data-start="3500"
                             data-easing="easeOutExpo">
                             Watch more Videos!
                        </a>
                    </li>               
                    
                    <!-- THE FORTH SLIDE -->
                    <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400" data-thumb="assets/img/sliders/revolution/thumbs/thumb2.jpg">
                        <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                        <img src="assets/img/sliders/slid/bg4.png" alt="">                        
                         <div class="caption lft slide_title"
                             data-x="0"
                             data-y="125"
                             data-speed="400"
                             data-start="1500"
                             data-easing="easeOutExpo">
                             What else included ?
                        </div>
                        <div class="caption lft slide_subtitle"
                             data-x="0"
                             data-y="180"
                             data-speed="400"
                             data-start="2000"
                             data-easing="easeOutExpo">
                             The Most Complete Admin Theme
                        </div>
                        <div class="caption lft slide_desc"
                             data-x="0"
                             data-y="225"
                             data-speed="400"
                             data-start="2500"
                             data-easing="easeOutExpo">
                             Lorem ipsum dolor sit amet, consectetuer elit sed diam<br> nonummy amet euismod dolore.
                        </div>
                        <a class="caption lft slide_btn btn red slide_item_left" href="http://www.keenthemes.com/preview/index.php?theme=metronic_admin" target="_blank" 
                             data-x="0"
                             data-y="300"
                             data-speed="400"
                             data-start="3000"
                             data-easing="easeOutExpo">
                             Learn More!
                        </a>   
                    </li>
            </ul>
            <div class="tp-bannertimer tp-bottom"></div>
        </div>
    </div>

     <!-- BEGIN CONTAINER -->   
    <div class="container">

        <!-- BEGIN RECENT WORKS -->
        <div class="row-fluid recent-work margin-bottom-40">
          
              
             <!-- BEGIN CAROUSEL -->            
            <div class="span3 front-carousel">
                <h2><a href="portfolio.html">ข่าวประชาสัมพันธ์</a></h2>
                <div id="myCarousel" class="carousel slide">
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        <div class="active item">
                            <img src="assets/img/works/img1.jpg" alt="">
                            <div class="carousel-caption">
                                <p>Excepturi sint occaecati cupiditate non provident</p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="assets/img/works/img2.jpg" alt="">
                            <div class="carousel-caption">
                                <p>Ducimus qui blanditiis praesentium voluptatum</p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="assets/img/works/img3.jpg" alt="">
                            <div class="carousel-caption">
                                <p>Ut non libero consectetur adipiscing elit magna</p>
                            </div>
                        </div>
                    </div>
                    <!-- Carousel nav -->
                    <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                        <i class="icon-angle-left"></i>
                    </a>
                    <a class="carousel-control right" href="#myCarousel" data-slide="next">
                        <i class="icon-angle-right"></i>
                    </a>
                </div>                
            </div>
            <!-- END CAROUSEL --> 

            <!-- TABS -->
            <div class="span9 tab-style-1 margin-bottom-20">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-1" data-toggle="tab">ข่าวทั่วไป</a></li>
                    <li><a href="#tab-2" data-toggle="tab">ข่าวกรมฯ</a></li>
                    <li><a href="#tab-3" data-toggle="tab">ข่าวรับสมัครทุน</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane row-fluid fade in active" id="tab-1">
                        <div class="span3">
                            <a href="assets/img/news/n1.jpg" class="fancybox-button" title="Image Title" data-rel="fancybox-button">
                                <img class="img-circle" src="assets/img/news/n1.jpg" alt="" />
                            </a>
                        </div>
                        <div class="span9">
                            <p class="margin-bottom-10">พล.อ.ดาว์พงษ์ รัตนสุวรรณ รัฐมนตรีว่าการกระทรวงศึกษาธิการ มอบนโยบายในการประชุมเชิงปฏิบัติการเพื่อเตรียมความพร้อมผู้ผ่านการคัดเลือกเข้าร่วมโครงการผลิตครูเพื่อพัฒนาท้องถิ่น รุ่นแรก ปีการศึกษา 2559 จำนวน 4,079 คน โดยมี พล.ต.ณัฐพงษ์ เพราแก้ว เลขานุการรัฐมนตรีว่าการกระทรวงศึกษาธิการ เป็นวิทยากรบรรยายแผนการปฏิรูปการศึกษาด้านการผลิตและพัฒนาครู และมี ดร.สุภัทร จำปาทอง เลขาธิการคณะกรรมการการอุดมศึกษา นายการุณ สกุลประดิษฐ์ เลขาธิการคณะกรรมการการศึกษาขั้นพื้นฐาน ผู้บริหารและข้าราชการสำนักงานคณะกรรมการการอุดมศึกษา (สกอ.) ให้การต้อนรับ และร่วมการประชุมในครั้งนี้</p>
                            <p><a href="http://www.kroobannok.com/80329" class="read-more">Read more</a></p>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab-2">
                        <div class="span9">
                            <p>ตามที่กระทรวงศึกษาธิการกำหนดนโยบายการพัฒนาหลักสูตรและกระบวนการจัดการเรียนรู้ เป็นยุทธศาสตร์หนึ่งของการปฏิรูปการศึกษา นั้น เพื่อให้สอดคล้องกับนโยบายดังกล่าว สำนักงานคณะกรรมการการศึกษาขั้นพื้นฐาน จึงได้วิเคราะห์และจัดกลุ่มตัวชี้วัดที่ต้องรู้และควรรู้ ตามหลักสูตรแกนกลางการศึกษาขั้นพื้นฐาน พุทธศักราช ๒๕๕๑ เพื่อให้การจัดการเรียนรู้กับการวัดและประเมินผลระดับชาติมีความสอดคล้องกัน และเพื่อให้เกิดความเข้าใจที่ตรงกัน สำนักงานคณะกรรมการการศึกษาขั้นพื้นฐานจึงได้จัดทำนิยามของตัวชี้วัดต้องรู้และควรรู้ ดังนี้</p>
                            <p><a href="http://www.kroobannok.com/80320" class="read-more">Read more</a></p>
                        </div>
                        <div class="span3">
                            <a href="assets/img/news/n2.jpg" class="fancybox-button" title="Image Title" data-rel="fancybox-button">
                                <img class="img-circle" src="assets/img/news/n2.jpg" alt="" />
                            </a>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab-3">
                        <div class="span9">
                        <p>ด่วนที่สุด ที่ ศธ 04009/ว1045 ลว 18 ตุลาคม 2559 เรื่อง การคัดเลือกนักศึกษาทุนโครงการผลิตครูเพื่อพัฒนาท้องถิ่น ปีการศึกษา 2559 </p>
                        <p><a href="http://www.kroobannok.com/80328." class="read-more">Read more</a></p>
                        </div>
                        <div class="span3">
                            <a href="assets/img/news/n3.jpg" class="fancybox-button" title="Image Title" data-rel="fancybox-button">
                                <img class="img-circle" src="assets/img/news/n3.jpg" alt="" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END TABS -->

        </div>   
        <!-- END RECENT WORKS -->

        <div class="clearfix"></div>

     <!-- BEGIN SERVICE BLOCKS -->               
            <div class="span12">
                <div>
							<h3>Wells</h3>
							<div class="well">
								<h4>Default well</h4>
								Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet.Integer molestie lorem at massa Integer molestie lorem at massa Integer molestie lorem at massa Integer molestie lorem at massa.
							</div>
							<div class="well well-large">
								<h4>Large Well</h4>
								Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet. Integer molestie lorem at massa Integer molestie lorem at massa  Integer molestie lorem at massa
							</div>
						</div>
                <!-- BEGIN BLOCKQUOTE BLOCK -->   
        <div class="row-fluid quote-v1">
            <div class="span3 quote-v1-inner">
                <span>หน่วยงานที่เกี่ยวข้อง</span>
            </div>
            <div class="span9 quote-v1-inner text-right">
                <a class="btn-transparent" href="#"><i class="icon-rocket margin-right-10"></i>ดูทั้งหมด</a>
            </div>
        </div>
        <!-- END BLOCKQUOTE BLOCK -->
                <%--<div class="row-fluid margin-bottom-20">
                    <div class="span3 service-box-v1">
                        <div><i class="icon-home color-green"></i></div>
                        <h2>กระทรวงการต่างประเทศ</h2>
                   </div>
                    <div class="span3 service-box-v1">
                        <div><i class="icon-home color-grey"></i></div>
                        <h2>กรมการกงสุล</h2>
                        <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse.</p>
                    </div>
                    <div class="span3 service-box-v1">
                        <div><i class="icon-home color-grey"></i></div>
                        <h2>กรมยุโรป</h2>
                        <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse.</p>
                    </div>
                    <div class="span3 service-box-v1">
                        <div><i class="icon-home color-grey"></i></div>
                        <h2>กรมเอเชียใต้ ตะวันออกกลางและแอฟริกา</h2>
                        <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse.</p>
                    </div>
                </div>--%>
            </div>
            <!-- END SERVICE BLOCKS --> 
        <div class="clearfix"></div>

       <!-- BEGIN RECENT WORKS -->
        <div class="row-fluid recent-work margin-bottom-40">
            
            <div class="span8">
                    <h2>Our Friends</h2>
                <ul class="bxslider">
                    <li>
                        <em>
                            <img src="assets/img/clients/1.jpg" alt="" />
                            <a href="#"><i class="icon-link icon-hover icon-hover-1"></i></a>
                            <a href="assets/img/works/img1.jpg" class="fancybox-button" title="Project Name #1" data-rel="fancybox-button"><i class="icon-search icon-hover icon-hover-2"></i></a>
                        </em>
                    </li>
                    <li>
                        <em>
                            <img src="assets/img/clients/2.jpg" alt="" />
                            <a href="#"><i class="icon-link icon-hover icon-hover-1"></i></a>
                            <a href="assets/img/works/img2.jpg" class="fancybox-button" title="Project Name #2" data-rel="fancybox-button"><i class="icon-search icon-hover icon-hover-2"></i></a>
                        </em>
                    </li>
                    <li>
                        <em>
                            <img src="assets/img/clients/4.jpg" alt="" />
                            <a href="#"><i class="icon-link icon-hover icon-hover-1"></i></a>
                            <a href="assets/img/works/img3.jpg" class="fancybox-button" title="Project Name #3" data-rel="fancybox-button"><i class="icon-search icon-hover icon-hover-2"></i></a>
                        </em>
                    </li>
                    <li>
                        <em>
                            <img src="assets/img/clients/5.jpg" alt="" />
                            <a href="#"><i class="icon-link icon-hover icon-hover-1"></i></a>
                            <a href="assets/img/works/img4.jpg" class="fancybox-button" title="Project Name #4" data-rel="fancybox-button"><i class="icon-search icon-hover icon-hover-2"></i></a>
                        </em>
                    </li>
                    <li>
                        <em>
                            <img src="assets/img/clients/6.jpg" alt="" />
                            <a href="#"><i class="icon-link icon-hover icon-hover-1"></i></a>
                            <a href="assets/img/works/img5.jpg" class="fancybox-button" title="Project Name #5" data-rel="fancybox-button"><i class="icon-search icon-hover icon-hover-2"></i></a>
                        </em>
                    </li>
                </ul>        
            </div>

            <div class="span4">
            <!-- BEGIN BLOG PHOTOS STREAM -->
                <div class="blog-photo-stream margin-bottom-20">
                    <h2>Photos Stream</h2>
                    <ul class="unstyled">
                        <li><a href="#"><img src="assets/img/pics/img1-large.jpg" alt=""></a></li>
                        <li><a href="#"><img src="assets/img/pics/img1-large.jpg" alt=""></a></li>
                        <li><a href="#"><img src="assets/img/pics/img1-large.jpg" alt=""></a></li>
                        <li><a href="#"><img src="assets/img/pics/img1-large.jpg" alt=""></a></li>
                        <li><a href="#"><img src="assets/img/pics/img1-large.jpg" alt=""></a></li>
                        <li><a href="#"><img src="assets/img/pics/img1-large.jpg" alt=""></a></li>
                        <li><a href="#"><img src="assets/img/pics/img1-large.jpg" alt=""></a></li>
                        <li><a href="#"><img src="assets/img/pics/img1-large.jpg" alt=""></a></li>
                        <li><a href="#"><img src="assets/img/pics/img1-large.jpg" alt=""></a></li>
                        <li><a href="#"><img src="assets/img/pics/img1-large.jpg" alt=""></a></li>
                    </ul>                    
                </div>
             </div>
                <!-- END BLOG PHOTOS STREAM -->
        </div>   
        <!-- END RECENT WORKS --> 
        
        <!-- END CLIENTS -->
    </div>
    <!-- END CONTAINER -->
</asp:Content>
