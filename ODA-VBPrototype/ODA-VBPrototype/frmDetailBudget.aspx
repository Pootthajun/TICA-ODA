﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="frmDetailBudget.aspx.vb" Inherits="ODA_VBPrototype.frmDetailBudget" %>


<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Budget | Budget</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

    <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
        <li>
            <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
            </a>
        </li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-calculator"></i><span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i>Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i>Report</a></li>
                <li>
                    <a href="#"><i class="fa fa-gear"></i>Master<i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i>Expense Group</a></li>
                        <li><a href="frmSubExpense.aspx"><i class="fa fa-list"></i>Sub Expense</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="active treeview">
            <a href="#">
                <i class="fa fa-dollar"></i><span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="active"><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i>Budget</a></li>
                <li><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i>Report</a></li>
                <li>
                    <a href="#"><i class="fa fa-gear"></i>Master<i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                        <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i>Sub Budget</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="treeview">
            <a href="frmReport.aspx">
                <i class="fa fa-file-text-o"></i><span>Report</span></a>
        </li>

        <li class="treeview">
            <a href="frmStructure.aspx">
                <i class="fa fa-sitemap"></i><span>Organize Structure</span>
            </a>

        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-gear"></i><span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i>Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i>OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i>RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i>Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i>Cooperation Type</a></li>
                <li class="active"><a href="frmSector.aspx"><i class="fa fa-file"></i>Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file"></i>Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-list"></i>Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-list"></i>Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-list"></i>In Kind</a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
        </li>
    </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h4>Detail Budget (รายละเอียดงบประมาณ)
            </h4>
            <ol class="breadcrumb">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i>Budget</a></li>
                <li class="active">Detail Budget</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                  
                        <div class="box-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="bg-gray text-center">
                                        <th class="text-center">No. (ลำดับ)</th>
                                        <th class="text-center">Type Butget<br /> (ประเภทงบประมาณ)</th>
                                        <th style="width: 300px" class="text-center">Sub Budget (งบประมาณย่อย)</th>
                                        <th class="text-center">Received (ได้รับ)</th>
                                        <th class="text-center">Budget (งบประมาณ)</th>
                                        <th class="text-center">Used (ใช้ไป)</th>
                                        <th class="text-center">Balance (คงเหลือ)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center">1</td>
                                        <td class="text-center"><a href="#">งบเงินอุดหนุน</a></td>
                                        <td><a href="#">เงินอุดหนุนองค์กรการระหว่างประเทศที่ประเทศไทยเข้าเป็นสมาชิก</a></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">2</td>
                                        <td class="text-center"><a href="#"></a></td>
                                        <td><a href="#">เงินอุดหนุนกรให้ความช่วยเหลือและความร่วมมือทางด้านวิชาการฯ (ภูมิภาคอื่นๆ)</a></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">3</td>
                                        <td class="text-center"><a href="#"></a></td>
                                        <td><a href="#">เงินอุดหนุนค่าใช้จ่ายในการเดินทางสำหรับผู้รับทุนรัฐบาลต่างประเทศ</a></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">4</td>
                                        <td class="text-center"><a href="#"></a></td>
                                        <td><a href="#">เงินอุดหนุนการให้ความช่วยเหลือและความร่วมมือทางด้านวิชาการและเศรษฐกิจแก่ต่างประเทศ (ประเทศเพื่อนบ้าน)</a></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">5</td>
                                        <td class="text-center"><a href="#"></a></td>
                                        <td><a href="#">เงินอุดหนุนแผนงานพัฒนาชุมชนในเขตเศรษฐกิจพิเศษทวาย</a></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">6</td>
                                        <td class="text-center"><a href="#"></a></td>
                                        <td><a href="#">เงินอุดหนุนในการสร้างความตระหนักและเตรียมความพร้อมสำหรับโรคติดต่อและโรคอุบัติใหม่ตามแนวชายแดนไทย-ประเทศเพื่อนบ้าน</a></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">7</td>
                                        <td class="text-center"><a href="#"></a></td>
                                        <td><a href="#">เงินอุดหนุนการพัฒนาฝีมือแรงงานตามแนวชายแดนไทย-เพื่อนบ้าน</a></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-right" colspan="3"><b>Total</b></td>
                                        <td class="text-right"><b>00.00</b></td>
                                        <td class="text-right"><b>00.00</b></td>
                                        <td class="text-right"><b>00.00</b></td>
                                        <td class="text-right"><b>00.00</b></td>
                                    </tr>

                                    <tr>
                                        <td class="text-center">8</td>
                                        <td class="text-center"><a href="#">งบรายจ่ายอื่น</a></td>
                                        <td><a href="#">ค่าใช้จ่ายในการเจรจาและการประชุมนานาชาติ</a></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">9</td>
                                        <td class="text-center"><a href="#"></a></td>
                                        <td><a href="#">ค่าใช้จ่ายในการเดินทางไปราชการต่างประเทศชั่วคราว</a></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">10</td>
                                        <td class="text-center"><a href="#"></a></td>
                                        <td><a href="#">ค่าใช้จ่ายในการจัดการประชุมระหว่างประเทศ</a></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">11</td>
                                        <td class="text-center"><a href="#"></a></td>
                                        <td><a href="#">ค่าใช้จ่ายในการสนับสนุนความร่วมมือทางวิชาการกับต่างประเทศ</a><br />
                                            - ภาษีชดเชย ผชช. และมูลนิธิ
                                        </td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">12</td>
                                        <td class="text-center"><a href="#"></a></td>
                                        <td><a href="#">ค่าใช้จ่ายในการสนับสนุนความร่วมมือทางวิชาการกับต่างประเทศ</a><br />
                                            - จัดทำแผนให้ฯ
                                        </td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">13</td>
                                        <td class="text-center"><a href="#"></a></td>
                                        <td><a href="#">ค่าใช้จ่ายในการสนับสนุนความร่วมมือทางวิชาการกับต่างประเทศ</a><br />
                                            - สมทบ ผชช./อาสาสมัคร
                                        </td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                        <td><input type="text" class="form-control text-right" placeholder="00.00"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-right" colspan="3"><b>Total</b></td>
                                        <td class="text-right"><b>00.00</b></td>
                                        <td class="text-right"><b>00.00</b></td>
                                        <td class="text-right"><b>00.00</b></td>
                                        <td class="text-right"><b>00.00</b></td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />
</asp:Content>

