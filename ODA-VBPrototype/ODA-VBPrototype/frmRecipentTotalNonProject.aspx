﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="frmRecipentTotalNonProject.aspx.vb" Inherits="ODA_VBPrototype.frmRecipentTotalNonProject" %>
<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Recipent Total | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
            
           <li class="treeview">
              <a href="frnFinance.aspx">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li ><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i> Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li><a href="frmSubExpense.aspx"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="treeview">
              <a href="frmAllocatedBudget.aspx">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i> Budget</a></li>
                <li><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                    <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i> Sub Budget</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmReport.aspx">
                <i class="fa fa-file-text-o"></i> <span>Report</span></a>
            </li> 

            <li class="treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-sitemap"></i> <span>Organize Structure</span>
              </a>
             
            </li>
             <li>
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li><a href="frmSector.aspx"><i class="fa fa-file"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-list"></i> Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-list"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-list"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
 
     <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
           List Recipient (รายชื่อผู้รับทุน)
          </h4>
          <ol class="breadcrumb">
            <li><a href="frmDashborad.aspx"><i class="fa fa-area-chart"></i> Dashboard</a></li>
            <li class="active">Sector</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                 <div class="box-header">
                     <p><a href="frmEditComponent.aspx"><button class="btn bg-orange margin-r-5 btn-social"><i class="fa fa-plus"></i>Add Recipient</button></a></p>
                   <div class="col-lg-7">
                     <h4 class="text-primary">พบทั้งหมด 6 รายการ</h4>
                  </div>
                  <div class="input-group col-sm-5 pull-right">
                   <input type="text" class="form-control">
                       <span class="input-group-addon"><i class="fa fa-search"></i></span>
                  </div>
                <div class="clearfix"></div>
              </div><!-- /.box-header -->
                <div class="box-body">
                  <div style="overflow-x:auto;">
                  <table class="table table-bordered">
                    <tr class="bg-gray">
                      <th style="width: 10px">No.</th>
                      <th>Student ID</th>
                      <th style="width: 200px">Name</th>
                      <th>Country</th>
                      <th>Course</th>
                      <th>Start-End Date</th>
                      <th>Institute</th>
                      <th>Agency</th>
                      <th>Position</th>
                      <th>Status</th>
                    </tr>
                    <tr>
                      <td>1.</td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">56199130401</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Hussain Raafiu</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Mozambique</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Sufficiency Economy in Agriculture</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">2013/05/02-2013/06/01</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Office of the National Human Rights Commision of Thailand</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Ministry of Fisheries and Agriculture</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Senior Project Officer</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">กำลังศึกษา</a></td>
                    </tr>
                    <tr>
                      <td>2.</td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">56199130402</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Anina Trefina Mangahele</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Mozambique</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Sufficiency Economy in Agriculture</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">2013/05/02-2013/06/01</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Office of the National Human Rights Commision of Thailand</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Ministry of Fisheries and Agriculture</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Head of Service</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">กำลังศึกษา</a></td>
                    </tr>
                    <tr>
                      <td>3.</td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">56199130403</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Larbi Kamoune</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Morocco</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Sufficiency Economy in Agriculture</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">2013/05/02-2013/06/01</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Office of the National Human Rights Commision of Thailand</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Ministry of Fisheries and Agriculture</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Head of Service</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">กำลังศึกษา</a></td>
                    </tr>
                    <tr>
                      <td>4.</td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">56199130404</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Yvette Jocelyne Rafaraniaina</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Madagascar</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Sufficiency Economy in Agriculture</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">2013/05/02-2013/06/01</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Office of the National Human Rights Commision of Thailand</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Irrigation and Watershed Management Project</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Watershed Development Responsible</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">กำลังศึกษา</a></td>
                    </tr>
                    <tr>
                      <td>5.</td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">56199130405</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Anina Trefina Mangahele</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Mozambique</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Sufficiency Economy in Agriculture</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">2013/05/02-2013/06/01</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Office of the National Human Rights Commision of Thailand</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Ministry of Agriculture</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Agricultural Policy Analyst</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">กำลังศึกษา</a></td>
                    </tr>
                    <tr>
                      <td>6.</td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">56199130406</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Antonio Vicente Daci-Lelo</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Timo Leste</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Sufficiency Economy in Agriculture</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">2013/05/02-2013/06/01</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Office of the National Human Rights Commision of Thailand</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">National Directore of Admih and Finance</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Planing Officer</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">กำลังศึกษา</a></td>
                    </tr>
                  </table>
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
   </div>
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />
</asp:Content>
