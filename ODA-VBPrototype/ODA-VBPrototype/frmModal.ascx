﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmModal.ascx.vb" Inherits="ODA_VBPrototype.frmModal" %>
<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>


<link rel="stylesheet" href="dist/modal/modal.css">

<div class="container">
  <!-- Trigger the modal with a button -->

  <!-------------------------- ModalCountry ---------------------------->
  <div class="modal fade" id="myModalCountry" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Country</h4>
        </div>
        <div class="modal-body">
           <div class="box-body">
               <table class="table table-bordered">
                 
                        <tr class="bg-info">
                           <td colspan="2"><h4 class="box-title"><a href="#">Country </a>> Thailand International Development Cooperation Agency</h4></td>
                        </tr>
                        <tr>
                          <th style="width: 180px"><b class="pull-right">Country :</b></th>
                          <td><div class="col-sm-12">
                              <input class="form-control" data-toggle="tooltip" title="ชื่ออังกฤษ" id="CountryEN" placeholder=""></div></td>
                        </tr>
                        <tr>
                          <th style="width: 180px"><b class="pull-right">ชื่อประเทศ :</b></th>
                          <td><div class="col-sm-12">
                            <input class="form-control" data-toggle="tooltip" title="ชื่อไทย" id="CountryTH" placeholder="">
                        </div></td>
                        </tr>
                  
                        <tr>
                          <th style="width: 180px"><b class="pull-right">Description<br /> (รายละเอียด) :</b></th>
                          <td><div class="col-sm-12"><textarea class="form-control" rows="3" placeholder=""></textarea></div></td>
                        </tr>
                      <tr>
                          <th style="width: 180px"><b class="pull-right">Country Group<br /> (กลุ่มประเทศ) :</b></th>
                          <td class="text-primary">
                              <div class="col-sm-12">
                                <select class="form-control select2" style="width: 100%;">
                                  <option selected="selected"></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                                </div><!-- /.form-group --></td>
                         </tr>
                        <tr>
                          <th style="width: 180px"><b class="pull-right">Country Zone<br /> (โซนประเทศ) :</b></th>
                          <td class="text-primary">
                              <div class="col-sm-12">
                                <select class="form-control select2" style="width: 100%;">
                                  <option selected="selected"></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                                </div><!-- /.form-group --></td>
                         </tr>
                         <tr>
                          <th style="width: 180px"><b class="pull-right">Region OECD (ภูมิภาค) :</b></th>
                          <td class="text-primary">
                              <div class="col-sm-12">
                                <select class="form-control select2" style="width: 100%;">
                                  <option selected="selected"></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                                </div><!-- /.form-group --></td>
                         </tr>
                       <tr> 
                        <th style="width: 180px"><b class="pull-right">Active Status :</b></th>
                        <td><div class="col-sm-12">
                        <label><input type="checkbox" class="minimal-red" checked></label></div></td>
                        </tr>
                        
                          </table>
                    
                    </div>
                    <div class="modal-footer">
                      <div class="col-sm-6"></div>
                    <div class="col-sm-3">
                      <a href="frmProjectExam.aspx" class="btn btn-block btn-social btn-success"><i class="fa fa-save"></i>Save </a>
                    </div>
                    <div class="col-sm-3">
                      <a href="frmProjectExam.aspx" class="btn btn-block btn-social btn-google" data-dismiss="modal"><i class="fa fa-reply"></i>Cancel </a>
                    </div> 
                     </div>
                  </div>
                  </div>
                </div>
              </div>

    <!---------------------------------- ModalCountryDetail ----------------------------------->
  <div class="modal fade" id="myModalCountryDetail" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Country</h4>
        </div>
        <div class="modal-body">
           <div class="box-body">
               <table class="table table-bordered">
                 
                        <tr class="bg-info">
                           <td colspan="2"><h4 class="box-title"><a href="#">Country </a>> Thailand International Development Cooperation Agency</h4></td>
                        </tr>
                        <tr>
                          <th style="width: 180px"><b class="pull-right">Country :</b></th>
                          <td>ThaiLand</td>
                        </tr>
                        <tr>
                          <th style="width: 180px"><b class="pull-right">ชื่อประเทศ :</b></th>
                          <td>ไทย</td>
                        </tr>
                  
                        <tr>
                          <th style="width: 180px"><b class="pull-right">Description<br /> (รายละเอียด) :</b></th>
                          <td><br /><br /><br /></td>
                        </tr>
                      <tr>
                          <th style="width: 180px"><b class="pull-right">Country Group<br /> (กลุ่มประเทศ) :</b></th>
                          <td></td>
                         </tr>
                        <tr>
                          <th style="width: 180px"><b class="pull-right">Country Zone<br /> (โซนประเทศ) :</b></th>
                          <td></td>
                         </tr>
                         <tr>
                          <th style="width: 180px"><b class="pull-right">Region OECD (ภูมิภาค) :</b></th>
                          <td></td>
                         </tr>
                       <tr> 
                        <th style="width: 180px"><b class="pull-right">Active Status :</b></th>
                        <td><div class="col-sm-12">
                        <label><input type="checkbox" class="minimal-red" checked></label></div></td>
                        </tr>
                        
                    </table>
                    
                    </div>
                    <div class="modal-footer">
                      <div class="col-sm-6"></div>
                    <div class="col-sm-3">
                      <a href="frmProjectExam.aspx" class="btn btn-block btn-social btn-success"><i class="fa fa-save"></i>Save </a>
                    </div>
                    <div class="col-sm-3">
                      <a href="frmProjectExam.aspx" class="btn btn-block btn-social btn-google" data-dismiss="modal"><i class="fa fa-reply"></i>Cancel </a>
                    </div> 
                     </div>
                  </div>
                  </div>
                </div>
              </div>


    <!----------------------------------- ModalAgency ---------------------------------------->
  <div class="modal fade" id="myModalAgency" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Agency</h4>
        </div>
        <div class="modal-body">
          <table class="table table-bordered">
                 
                        <tr class="bg-info">
                           <td colspan="2"><h4 class="box-title"><a href="#">Country </a>> Thailand International Development Cooperation Agency</h4></td>
                        </tr>
                        <tr>
                          <th style="width: 180px"><b class="pull-right">ชื่อหน่วยงาน :</b></th>
                          <td><div class="col-sm-12"><div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-building-o"></i>
                              </div>
                              <input class="form-control" id="AgencyNameTH" placeholder="">
                            </div></div></td>
                        </tr>
                        <tr>
                          <th style="width: 180px"><b class="pull-right">Agency Name :</b></th>
                          <td><div class="col-sm-12"><div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-building-o"></i>
                              </div>
                              <input class="form-control" id="AgencyNameEN" placeholder="">
                            </div></div></td>
                        </tr>
                        <tr>
                          <th style="width: 180px"><b class="pull-right">Location (ที่ตั้ง) :</b></th>
                          <td><div class="col-sm-12"><textarea class="form-control" rows="3" placeholder=""></textarea></div></td>
                        </tr>
                        <tr>
                          <th style="width: 180px"><b class="pull-right">Telephone (โทรศัพท์) :</b></th>
                          <td><div class="col-sm-12"><div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-phone"></i>
                              </div>
                              <input class="form-control" id="phone" placeholder="">
                            </div></div></td>
                        </tr>
                        <tr>
                          <th style="width: 180px"><b class="pull-right">Fax. (โทรสาร) :</b></th>
                          <td><div class="col-sm-12"><div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-fax"></i>
                              </div>
                              <input class="form-control" id="Fax" placeholder="">
                            </div></div></td>
                        </tr>
                        <tr>
                          <th style="width: 180px"><b class="pull-right">Website :</b></th>
                          <td><div class="col-sm-12"><input class="form-control" id="Website" placeholder=""></div></td>
                        </tr>
                        <tr>
                          <th style="width: 180px"><b class="pull-right">Note (หมายเหตุ) :</b></th>
                          <td><div class="col-sm-12"><textarea class="form-control" rows="3" placeholder=""></textarea></div></td>
                        </tr>
                       <tr>
                          <th style="width: 180px"><b class="pull-right">Agency Type (ประเภทหน่วยงาน) :</b></th>
                           <td>
                           <div class="form-group">
                            <label>
                              <input type="checkbox" class="minimal" checked>
                                Funding Agency
                            </label><br />
                            <label>
                              <input type="checkbox" class="minimal">
                                Executing Agency
                            </label><br />
                            <label>
                              <input type="checkbox" class="minimal">
                                Implementing Agency 
                            </label>
                          </div></td>
                       </tr>
                       <tr> 
                        <th style="width: 180px"><b class="pull-right">Active Status :</b></th>
                        <td><div class="col-sm-12">
                        <label><input type="checkbox" class="minimal-red" checked></label></div></td>
                        </tr>
                       </table>
                    </div>
                    <div class="modal-footer">
                    <div class="col-sm-6"></div>
                     <div class="col-sm-3">
                      <a href="frmProjectExam.aspx" class="btn btn-block btn-social btn-success"><i class="fa fa-save"></i>Save </a>
                    </div>
                    <div class="col-sm-3">
                      <a href="frmProjectExam.aspx" class="btn btn-block btn-social btn-google" data-dismiss="modal"><i class="fa fa-reply"></i>Cancel </a>
                    </div>
                    </div>
                  </div>
      
                </div>
              </div>
  
    <!--------------------------------------------- ModalPerson ------------------------------------------>
  <div class="modal fade" id="myModalPerson" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Preson</h4>
        </div>
        <div class="modal-body">
           <table class="table table-bordered">
                        <tr class="bg-info">
                           <td colspan="2"><h5 class="box-title"><a href="#">Country </a>><a href="#"> Thailand International Development Cooperation Agency </a>> สำนักผู้อำนวยการ</h5></td>
                        </tr>
                        <tr>
                          <th style="width: 180px"><b class="pull-right">ชื่อภาษาไทย :</b></th>
                          <td><div class="col-sm-12"><div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-user"></i>
                              </div>
                              <input class="form-control" id="Thai" placeholder="">
                            </div></div></td>
                        </tr>
                        <tr>
                          <th style="width: 180px"><b class="pull-right">English Name :</b></th>
                          <td><div class="col-sm-12"><div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-user"></i>
                              </div>
                              <input class="form-control" id="English" placeholder="">
                            </div></div></td>
                        </tr>
                        <tr>
                          <th style="width: 180px"><b class="pull-right">Id Card/Passport :</b></th>
                          <td><div class="col-sm-12"><div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-credit-card"></i>
                              </div>
                              <input class="form-control" id="Passport" placeholder="">
                            </div></div></td>
                        </tr>
                        <tr>
                          <th style="width: 180px"><b class="pull-right">Birthday (วันเกิด) :</b></th>
                          <td><div class="col-sm-12">
                              <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                             <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                           </div><!-- /.input group -->
                         </div></td>
                        </tr>
                        <tr>
                          <th style="width: 180px"><b class="pull-right">Telephone (โทรศัพท์) :</b></th>
                          <td><div class="col-sm-12"><div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-phone"></i>
                              </div>
                              <input class="form-control" id="phoneuser" placeholder="">
                            </div></div></td>
                        </tr>
                        <tr>
                          <th style="width: 180px"><b class="pull-right">E-mail :</b></th>
                          <td><div class="col-sm-12"><div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-envelope-o"></i>
                              </div>
                              <input class="form-control" id="mail" placeholder="">
                            </div></div></td>
                        </tr>
                        <tr>
                          <th style="width: 180px"><b class="pull-right">Note (หมายเหตุ) :</b></th>
                          <td><div class="col-sm-12"><textarea class="form-control" rows="3" placeholder=""></textarea></div></td>
                        </tr>
                       <tr> 
                        <th style="width: 180px"><b class="pull-right">Active Status :</b></th>
                        <td><div class="col-sm-12">
                        <label><input type="checkbox" class="minimal" checked></label></div></td>
                        </tr>
                      </table>
            </div>
        <div class="modal-footer">
           <div class="col-sm-6"></div>
                    <div class="col-sm-3">
                      <a href="frmProjectExam.aspx" class="btn btn-block btn-social btn-success"><i class="fa fa-save"></i>Save </a>
                    </div>
                    <div class="col-sm-3">
                      <a href="frmProjectExam.aspx" class="btn btn-block btn-social btn-google" data-dismiss="modal"><i class="fa fa-reply"></i>Cancel </a>
                    </div>
        </div>
      </div>
    </div>
  </div>

     <!--------------------------------------------- ModalSearch ------------------------------------------>
  <div class="modal fade" id="Notify" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Recipient (เพิ่มผู้รับทุน)</h4>
        </div>
        <div class="modal-body">
           <table class="table table-striped">
                    <tr class="bg-gray">
                      <th style="width: 10px">No.</th>
                      <th>Student ID</th>
                      <th style="width: 200px">Name</th>
                      <th>Country</th>
                      <th>Course</th>
                    </tr>
                    <tr>
                      <td><input type="checkbox" class="minimal-red" checked></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">56199130400</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Viengthone Thoummachan</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Laos</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Master of Arts</a></td>
                    </tr>
                    <tr>
                      <td><input type="checkbox" class="minimal-red" checked></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">56199130401</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Khamphet Sengouly</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Laos</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Master of Arts</a></td>
                    </tr>
                  </table>
            </div>
        <div class="modal-footer">
         <div class="col-sm-6"></div>
           <div class="col-sm-3">
           <a href="frmProjectExam.aspx" class="btn btn-block btn-social btn-success"><i class="fa fa-save"></i>Save </a>
            </div>
             <div class="col-sm-3">
            <a href="frmProjectExam.aspx" class="btn btn-block btn-social btn-google" data-dismiss="modal"><i class="fa fa-reply"></i>Cancel </a>
          </div>
        </div>
      </div>
    </div>
  </div>

      <!--------------------------------------------- ModalAddCourse------------------------------------------>
  <div class="modal fade" id="AddCourse" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Recipient (เพิ่มผู้รับทุน)</h4>
        </div>
        <div class="modal-body">
           <table class="table table-striped">
                    <tr class="bg-gray">
                      <th style="width: 10px">No.</th>
                      <th>Student ID</th>
                      <th style="width: 200px">Name</th>
                      <th>Country</th>
                      <th>Course</th>
                    </tr>
                    <tr>
                      <td><input type="checkbox" class="minimal-red" checked></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">56199130400</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Viengthone Thoummachan</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Laos</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Master of Arts</a></td>
                    </tr>
                    <tr>
                      <td><input type="checkbox" class="minimal-red" checked></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">56199130401</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Khamphet Sengouly</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Laos</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Master of Arts</a></td>
                    </tr>
                  </table>
            </div>
        <div class="modal-footer">
         <div class="col-sm-6"></div>
           <div class="col-sm-3">
           <a href="frmProjectExam.aspx" class="btn btn-block btn-social btn-success"><i class="fa fa-save"></i>Save </a>
            </div>
             <div class="col-sm-3">
            <a href="frmProjectExam.aspx" class="btn btn-block btn-social btn-google" data-dismiss="modal"><i class="fa fa-reply"></i>Cancel </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
        <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />