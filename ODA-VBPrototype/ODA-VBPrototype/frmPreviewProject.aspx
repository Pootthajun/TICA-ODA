﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="frmPreviewProject.aspx.vb" Inherits="ODA_VBPrototype.frmPreviewProject" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Preview Project | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
            
           <li class="treeview">
              <a href="frnFinance.aspx">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li ><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i> Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li><a href="frmSubExpense.aspx"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="treeview">
              <a href="frmAllocatedBudget.aspx">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i> Budget</a></li>
                <li><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                    <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i> Sub Budget</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmReport.aspx">
                <i class="fa fa-file-text-o"></i> <span>Report</span></a>
            </li> 

            <li class="treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-sitemap"></i> <span>Organize Structure</span>
              </a>
             
            </li>
             <li>
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li><a href="frmSector.aspx"><i class="fa fa-file"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-list"></i> Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-list"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-list"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
     <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <!-- Main content -->
        <section class="invoice">
          <!-- title row -->
          <div class="row">
           
          </div>
          <!-- info row -->
       

          <!-- Table row -->
          <div class="row">
            <div class="col-xs-12 table-responsive">
              <table class="table table-condensed">
                         <tr class="bg-info">
                          <th style="width: 150px" colspan="2">
                          <h5><b class="text-blue">Informations (ข้อมูลทั่วไป) </b><b class="text-orange pull-right">CODE ID : 201400265</b></h5></th>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Project Title (ชื่อโครงการ) :</b></th>
                          <td>โครงการพัฒนาวิทยาลัยศิลปศึกษา สปป.ลาว ประจำปี 2556</td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Objectives (วัตถุประสงค์) :</b></th>
                          <td>1. เพื่อพัฒนาบุคลากรของวิทยาลัยศิลปศึกษา ให้สำเร็จการศึกษาใน ป.โท และมีความรู้เฉพาะทางในด้านศิลปดนตรี และศิลปกรรม<br />
2. เพื่อให้การสนับสนุนอุปกรณ์ด้านศิลปกรรมและศิลปดนตรี ตามความจำเป็นและเหมาะสม <br />
3. เพื่อให้วิทยาลัยมีความพร้อมในการเปิดการเรียนการสอนในระดับปริญญาตรี </td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Description (รายละเอียด) :</b></th>
                          <td>ระยะเวลาโครงการ 2 ปี ( 2012 – 2013) ขยายระยะเวลาโครงการให้ดำเนินการในกิจกรรมที่ยังไม่แล้วเสร็จให้ สำเร็จ ภายในปี 2016 (ตามแผนงานด้านการศึกษาระยะที่ 2 ( 3 ปี 2014-2016)</td>
                        </tr>
                        
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Start/End Date</b><b class="pull-right"><br /> (วันเริ่มต้น/สิ้นสุดโครงการ) :</b></th>
                          <td>24 มีนาคม 2556 - 31 พฤษภาคม 2558 </td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Cooperation Framework <br />(กรอบความร่วมมือ) :</b></th>
                          <td>- (N/A)</td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Cooperation Type<br />(ประเภทความร่วมมือ):</b></th>
                          <td>- (Bilateral)</td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">OECD Aid Type<br />(ประเภท OECD):</b></th>
                          <td>Free-Standing Technical Cooperation</td>
                        </tr>
                    <%--<tr>
                          <th style="width: 250px"><b class="pull-right">Region OECD :</b></th>
                          <td>- </td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">OECD :</b></th>
                         <td>- </td>
                        </tr>--%>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Project Component<br /> (ประเภทความช่วยเหลือ) :</b></th>
                          <td class="text-primary">
                            <table class="table table-bordered">
                                <tr class="bg-gray">
                                  <th>Project Component (ประเภทความช่วยเหลือ)</th>
                                  <th style="width: 340px" colspan="2">Amount (จำนวนเงิน)</th>
                                </tr>
                                <tr>
                                 <td>Master </td>
                                <td style="width: 340px" colspan="2">700,000.00 </td>
                             </tr>
                         </table></td>
                        </tr>

                         <tr>
                          <th style="width: 150px" colspan="2">
                          <h5><b class="text-blue">Agency (หน่วยงาน) </b></h5></th>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Funding Agency<br/>(หน่วยงานให้ความช่วยเหลือ) :</b></th>
                            <td>MINISTRY OF FOREIGN AFFAIRS</td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Executing Agency<br/>(หน่วยงานดำเนินการ) :</b></th>
                             <td>Thailand International Development Cooperation Agency (TICA)</td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Implementing Agency <br />(หน่วยงานดำเนินการ) :</b></th>
                          <td>
                            <table class="table table-bordered">
                                <tr class="bg-gray">
                                  <th>Country (ประทศ)</th>
                                  <th style="width: 550px">Name Agency (ชื่อหน่วยงาน)</th>
                                </tr>
                                <tr>
                                  <td>Thailand </td>
                                  <td>มหาวิทยาลัยศรีนครินทรวิโรฒ</td>
                                </tr>
                                <tr>
                                  <td>Lao </td>
                                  <td>วิทยาลัยศิลปศึกษา กระทรวงศึกษาธิการและการกีฬา</td>
                                </tr>

                           </table></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Co-Funding(ผู้ร่วมให้เงินทุน) :</b></th>
                          <td class="text-primary">
                            <table class="table table-bordered">
                                <tr class="bg-gray">
                                  <th>Agency (หน่วยงาน)</th>
                                  <th style="width: 340px" colspan="2">Amount (จำนวนเงิน)</th>
                                </tr>
                                <tr>
                                 <td>- </td>
                                <td style="width: 340px" colspan="2">- </td>
                              </tr>
                           </table></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Assistant (ผู้ช่วยโครงการ) :</b></th>
                          <td>- <br /></td> 
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Contact Person :</b></th>
                              <td>
                                  <table class="table table-bordered">
                                    <tr>
                                      <th style="width: 150px"><b>ชื่อผู้ประสานงาน :</b></th>
                                      <td>นางสาวปนัดดา ริคารมย์</td>
                                     </tr>
                                     <tr>
                                      <th style="width: 50px"><b>ตำแหน่ง :</b></th>
                                     <td>- </td>
                                    </tr>
                                    <tr>
                                      <th style="width: 50px"><b>โทรศัพท์ :</b></th>
                                      <td>022035000</td>
                                    </tr>
                                    <tr>
                                      <th style="width: 50px"><b>โทรสาร :</b></th>
                                      <td>- </td>
                                    </tr>
                                    <tr>
                                      <th style="width: 50px"><b>Email :</b></th>
                                      <td>- </td>
                                    </tr>
                                   </table></td>
                        </tr>
                        
                        <tr>
                          <th style="width: 150px" colspan="2">
                          <h5><b class="text-blue">Other Detail (รายละเอียดอื่นๆ) </b></h5></th>
                        </tr>
                        <tr>
                          <th style="width: 150px"><b class="pull-right">Note (หมายเหตุ) :</b></th>
                          <td>- <br /><br /><br /></td>
                        </tr>
                        <tr>
                          <th style="width: 150px"><b class="pull-right">File input (เพิ่มไฟล์) :</b></th>
                          <td>- <br /><br /><br /></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Transfer Project To <br />(มอบหมายโครงการให้) :</b></th>
                          <td>- <br /></td>
                        </tr>
              </table>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <!-- this row will not appear when printing -->
          <div class="row no-print">
            <div class="col-xs-12">
               <%--<a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>--%>
              <a href="Project-print.aspx" target="_blank" class="btn btn-success pull-right"><i class="fa fa-print"></i> Print</a>
              <button class="btn btn-danger pull-right" style="margin-right: 5px;"><i class="fa fa-file-pdf-o"></i> PDF</button>
            </div>
          </div>
        </section><!-- /.content -->
        <div class="clearfix"></div>
      </div><!-- /.content-wrapper -->
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />
</asp:Content>
