﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="frmMultilateral.aspx.vb" Inherits="ODA_VBPrototype.frmMultilateral" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<title>Multilateral | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
            
           <li class="treeview">
              <a href="frnFinance.aspx">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li ><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i> Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li><a href="frmSubExpense.aspx"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="treeview">
              <a href="frmAllocatedBudget.aspx">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i> Budget</a></li>
                <li><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                    <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i> Sub Budget</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmReport.aspx">
                <i class="fa fa-file-text-o"></i> <span>Report</span></a>
            </li> 

            <li class="treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-users"></i> <span>Organize Structure</span>
              </a>
             
            </li>
             <li class="active treeview">
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li><a href="frmSector.aspx"><i class="fa fa-file"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-list"></i> Component</a></li>
                <li class="active"><a href="frmMultilateral.aspx"><i class="fa fa-list"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-list"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
  
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
           Multilateral (พหุภาคี)
          </h4>
          <ol class="breadcrumb">
            <li><a href="frmDashborad.aspx"><i class="fa fa-area-chart"></i> Dashboard</a></li>
            <li class="active">Multilateral</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                     <p><a href="frmEditMultilateral.aspx"><button class="btn bg-orange margin-r-5 btn-social"><i class="fa fa-plus"></i>Add Multilateral</button></a></p>
                  
                        <div class="col-lg-5">
                           <h4 class="text-primary">พบทั้งหมด 15 รายการ</h4>
                         </div>
                        <div class="col-sm-3"></div>
                     <div class="input-group col-sm-4 pull-right">
                       <input type="text" class="form-control">
                       <span class="input-group-addon"><i class="fa fa-search"></i></span>
                     </div>
                     <div class="clearfix"></div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr class="bg-gray">
                        <th>Multilateral</th>
                        <th style="width: 600px">Detail (รายละเอียด)</th>
                        <th style="width: 70px">Edit(แก้ไข)</th>
                        <th style="width: 50px">Delete(ลบ)</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>IOM</td>
                        <td>กระบวนการบาหลี</td>
                        <td><i class="fa fa-edit text-success"></i> Edit</td>
                        <td><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>UN</td>
                        <td>สหประชาชาติ</td>
                        <td ><i class="fa fa-edit text-success"></i> Edit</td>
                        <td ><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>UN Peace Keeping Operation</td>
                        <td>กองกำลังรักษาสันติภาพของสหประชาชาติ</td>
                        <td ><i class="fa fa-edit text-success"></i> Edit</td>
                        <td ><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>UNICEF</td>
                        <td>กองทุนสำหรับเด็กแห่งสหประชาชาติ</td>
                        <td ><i class="fa fa-edit text-success"></i> Edit</td>
                        <td ><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>UNHCR</td>
                        <td>สนง.ข้าหลวงใหญ่ผู้ลี้ภัยแห่งสหประชาชาติ</td>
                        <td ><i class="fa fa-edit text-success"></i> Edit</td>
                        <td ><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>OHCHR</td>
                        <td>สนง.ข้าหลวงใหญ่สิทธิมนุษยชนแห่งสหประชาชาติ</td>
                        <td ><i class="fa fa-edit text-success"></i> Edit</td>
                        <td ><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>UNRWA</td>
                        <td>ทบวงการบรรเทาทุกข์และจัดหางานเพื่อผู้ลี้ภัยชาวปาเลสไตน์ ในตะวันออกใกล้</td>
                        <td ><i class="fa fa-edit text-success"></i> Edit</td>
                        <td ><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>UNCDF</td>
                        <td>กองทุนเงินเพื่อการพัฒนาแห่งสหประชาชาติ</td>
                        <td ><i class="fa fa-edit text-success"></i> Edit</td>
                        <td ><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>UNITAR</td>
                        <td>กองทุนทั่วไปของสถาบันเพื่อการฝึกอบรมและวิจัยแห่งสหประชาชาติ</td>
                        <td ><i class="fa fa-edit text-success"></i> Edit</td>
                        <td ><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>UNDIP</td>
                        <td>โครงการข้อมูลด้านลดอาวุธแห่งสหประชาชาติ</td>
                        <td ><i class="fa fa-edit text-success"></i> Edit</td>
                        <td ><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>UNRCPD</td>
                        <td>ศุนย์สันติภาพและการลดอาวุธแห่งสหประชาชาติ ประจำภูมิภาคเอเชียและแปซิฟิก</td>
                        <td ><i class="fa fa-edit text-success"></i> Edit</td>
                        <td ><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ESCAP</td>
                        <td>กองทุนทั่วไปสำหรับกลุ่มประเทศที่เสียเปรียบในเชิงเศรษฐกิจ เพื่อส่งผู้แทนเข้าร่วมการประชุมเอสแคป</td>
                        <td ><i class="fa fa-edit text-success"></i> Edit</td>
                        <td ><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>TFPIC</td>
                        <td>กองทุนร่วมสำหรับหมู่เกาะแปซิฟิก</td>
                        <td ><i class="fa fa-edit text-success"></i> Edit</td>
                        <td ><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>APF</td>
                        <td>สถาบันสิทธิมนุษยชนแห่งชาติ ของภูมิภาคเอเชียแปซิฟิก</td>
                        <td ><i class="fa fa-edit text-success"></i> Edit</td>
                        <td ><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>CERF</td>
                        <td>กองทุนกลางเพื่อรับสถานการณ์ฉุกเฉิน</td>
                        <td ><i class="fa fa-edit text-success"></i> Edit</td>
                        <td ><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>UNAT</td>
                        <td>สมาคมสหประชาชาติแห่งประเทศไทย</td>
                        <td ><i class="fa fa-edit text-success"></i> Edit</td>
                        <td ><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>UNDP</td>
                        <td>โครงการพัฒนาแห่งสหประชาชาติ</td>
                        <td ><i class="fa fa-edit text-success"></i> Edit</td>
                        <td ><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
   </div>
    
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />
</asp:Content>
