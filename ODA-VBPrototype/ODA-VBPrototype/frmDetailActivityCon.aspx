﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="frmDetailActivityCon.aspx.vb" Inherits="ODA_VBPrototype.frmDetailActivityCon" %>
<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>

<%@ Register Src="~/frmModal.ascx" TagPrefix="uc1" TagName="frmModal" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<title>Add Activity | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
           <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
            
           <li class="treeview">
              <a href="frnFinance.aspx">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li ><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i> Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li><a href="frmSubExpense.aspx"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="treeview">
              <a href="frmAllocatedBudget.aspx">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i> Budget</a></li>
                <li><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                    <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i> Sub Budget</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmReport.aspx">
                <i class="fa fa-file-text-o"></i> <span>Report</span></a>
            </li> 

            <li class="treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-sitemap"></i> <span>Organize Structure</span>
              </a>
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li><a href="frmSector.aspx"><i class="fa fa-file-o"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file-o"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-align-justify"></i> Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-align-justify"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-align-justify"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">

  <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
           <h5>
          <b class="text-yellow">CODE ID 201500346 :</b> <b class="text-blue">Contribution to UN Capital Development Fund</b>
          </h5>
          <ol class="breadcrumb">
            <li><a href="frmDetailContribuiltion.aspx"><i class="fa fa-sign-out"></i> Back</a></li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-body">
                      <table class="table table-bordered">
                       <%-- <tr>
                          <th style="width: 280px"><b class="pull-right">Course (หลักสูตร) :</b></th>
                          <td class="text-primary">
                            <textarea class="form-control" rows="3" placeholder="งบประมาณเงินให้เปล่า" disabled></textarea></td>
                        </tr>--%>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Description (รายละเอียด) :</b></th>
                          <td class="text-primary">
                              <div class="col-sm-12"><textarea class="form-control" rows="2" placeholder="-"></textarea></div></td>
                        </tr>
                        
                        <tr>
                          <th style="width: 280px"><b class="pull-right">Sector (สาขา) :</b></th>
                          <td class="text-primary">
                              <div class="col-sm-12">
                                <select class="form-control select2" style="width: 100%;">
                                  <option selected="selected">MULTISECTOR/CROSS-CUTTING 400</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                                </div><!-- /.form-group --></td>
                        </tr>
                        
                        <tr>
                          <th style="width: 280px"><b class="pull-right">Sub Sector (สาขาย่อย) :</b></th>
                          <td class="text-primary">
                              <div class="col-sm-12">
                                <select class="form-control select2" style="width: 100%;">
                                  <option selected="selected">-</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                                </div><!-- /.form-group --></td>
                        </tr>
                        <tr>
                          <th style="width: 280px"><b class="pull-right">Plan (Start/End Date)</b><br /><b class="pull-right"> (วางแผนวันเริ่มต้น/สิ้นสุด) :</b></th>
                          <td class="text-primary">
                            <div class="col-sm-6">
                                <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                                <input type="text" class="form-control pull-right" id="reservation">
                                 </div><!-- /.input group -->
                              </div></td>
                        </tr>
                        
                        <tr>
                          <th style="width: 280px"><b class="pull-right">Actual (Start/End Date)</b><br /><b class="pull-right"> (วันเริ่มต้น/สิ้นสุด) :</b></th>
                          <td class="text-primary">
                            <div class="col-sm-6">
                                <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                                <input type="text" class="form-control pull-right" id="reservation1">
                                 </div><!-- /.input group -->
                              </div></td>
                        </tr>
                       <tr>
                          <th style="width: 250px"><b class="pull-right">Commitment (งบประมาณ) :</b></th>
                          <td class="text-primary">
                            <div class="col-sm-6">
                             <div class="form-group">
                                <div class="input-group">
                                  <div class="input-group-addon">
                                    <i class="fa fa-dollar "></i>
                                  </div>
                                  <input type="text" class="form-control pull-right" id="Commitment" placeholder="664,000.00 บาท">
                                </div><!-- /.input group -->
                              </div><!-- /.form group -->
                            </div></td>
                        </tr>
                         <tr>
                          <th style="width: 250px"><b class="pull-right">Disbursement (เบิกจ่ายจริง) :</b></th>
                          <td class="text-primary">
                            <div class="col-sm-6">
                             <div class="form-group">
                                <div class="input-group">
                                  <div class="input-group-addon">
                                    <i class="fa fa-dollar "></i>
                                  </div>
                                  <input type="text" class="form-control pull-right" id="Disbursement" placeholder="664,000.00 บาท">
                                </div><!-- /.input group -->
                              </div><!-- /.form group -->
                            </div></td>
                        </tr>
                         <tr>
                          <th style="width: 250px"><b class="pull-right">Payment Date (วันที่จ่ายเงิน) :</b></th>
                          <td class="text-primary">
                           <!-- Date dd/mm/yyyy -->
                            <div class="col-sm-6">
                              <div class="form-group">
                                <div class="input-group">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                </div><!-- /.input group -->
                              </div><!-- /.form group -->
                            </div></td>
                        </tr>
                        
                         <tr>
                          <th style="width: 250px"><b class="pull-right">Pay As (จ่ายเงินตาม) :</b></th>
                          <td class="text-primary">
                           <!-- radio -->
                              <div class="form-group">
                                <label>
                                  <input type="radio" name="r1" class="minimal" checked>
                                    Mandary (ตามพันธกรณี)
                                </label>
                                <label>
                                  <input type="radio" name="r1" class="minimal">
                                   Voluntary (ตามความสมัครใจ)
                                </label>
                           </div></td>
                        </tr>
                    
                      </table>
                  <div class="box-footer">
                        <div class="col-sm-8"></div>
                        <div class="col-sm-2">
                          <a href="#" class="btn btn-block btn-social btn-success"><i class="fa fa-save"></i>Save </a>
                        </div>
                        <div class="col-sm-2">
                          <a href="#" class="btn btn-block btn-social btn-google"><i class="fa fa-reply"></i>Cancel </a>
                        </div>
                        
                    </div>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
        </div>
    
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />
    <uc1:frmModal runat="server" ID="frmModal" />
</asp:Content>
