﻿<%@ Page Title="" Language="vb" AutoEventWireup="false"  MasterPageFile="~/frmMaster.Master" CodeBehind="frmDetailFinanceProject.aspx.vb" Inherits="ODA_VBPrototype.frmDetailFinanceProject" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Detail Project | ODA</title>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
           <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
            
           <li class="treeview">
              <a href="frnFinance.aspx">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li ><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i> Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li><a href="frmSubExpense.aspx"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="treeview">
              <a href="frmAllocatedBudget.aspx">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i> Budget</a></li>
                <li><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                    <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i> Sub Budget</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmReport.aspx">
                <i class="fa fa-file-text"></i> <span>Report</span></a>
            </li> 

            <li class="treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-sitemap"></i> <span>Organize Structure</span>
              </a>
             
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li><a href="frmSector.aspx"><i class="fa fa-file-o"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file-o"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-align-justify"></i> Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-align-justify"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-align-justify"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
  
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
           <a class="text-yellow"> CODE ID 201400265 :</a> <a class="text-blue"> โครงการพัฒนาวิทยาลัยศิลปศึกษา สปป.ลาว ประจำปี 2556 </a> 
          </h4>
          <ol class="breadcrumb">
            <li><a href="frmDashborad.aspx"><i class="fa fa-area-chart"></i> Dashborad</a></li>
            <li><a href="frmProjectStatus.aspx">Project Status</a></li>
            <li><a href="frmProject.aspx">Project</a></li>
            <li class="active">Detail Project</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">

              <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-list-alt"></i> Infomation(ข้อมูลโครงการ)</a></li>
                  <li><a href="#tab_2" data-toggle="tab"><i class="fa fa-th-list"></i> Activity(กิจกรรม)</a></li>
                  <li><a href="#tab_3" data-toggle="tab"><i class="fa fa-users"></i> Recipience(ผู้รับทุน)</a></li>
                    <li class="dropdown pull-right" data-toggle="tooltip" title="เครื่องมือ">
                    <a class="dropdown-toggle text-muted" data-toggle="dropdown" href="#">
                      <i class="fa fa-gear"></i>
                    </a>
                    <ul class="dropdown-menu">
                      <li role="presentation"><a role="menuitem" tabindex="-1" href="frmDetailActivityProject1.aspx"><i class="fa fa-edit text-blue"></i> Edit</a></li>
                      <li role="presentation"><a role="menuitem" tabindex="-1" href="frmPreviewProject.aspx"><i class="fa fa-print text-blue"></i> Print</a></li>
                      <li role="presentation" class="divider"></li>
                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><i class="fa fa-trash text-red"></i> Delete</a></li>
                    </ul>
                  </li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                    
                    <div class="box-body">
                      <table class="table table-bordered">
                       <tr class="bg-info">
                          <th style="width: 150px" colspan="2">
                          <h4 class="text-blue">Informations (ข้อมูลทั่วไป)</h4></th>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Project Title (ชื่อโครงการ) :</p></td>
                          <td class="text-primary">
                            <textarea class="form-control" rows="3" placeholder="โครงการพัฒนาวิทยาลัยศิลปศึกษา สปป.ลาว ประจำปี 2556" disabled></textarea></td>
                        </tr>

                        <tr>
                          <td style="width: 250px"><p class="pull-right">Objectives (วัตถุประสงค์) :</p></td>
                          <td class="text-primary">
                            <textarea class="form-control" rows="4" placeholder="1. เพื่อพัฒนาศักยภาพและเพิ่มขีดความสามารถของ จนท. กรมเลี้ยงสัตว์ในการวิเคราะห์คุณภาพอาหารสัตว์และตรวจสอบสารพิษในอาหารสัตว์ 
2. เพื่อเพิ่มประสิทธิภาพห้องปฏิบัติการวิจัยเพื่อวิเคราะห์คุณภาพอาหารสัตว์ให้ได้ระดับมาตรฐาน ในการ ตรวจสอบสารพิษตกค้าง วัตถุเจือปน โลหะหนัก แร่ธาตุ รวมทั้งตรวจสอบความปลอดภัยในอาหารสัตว์" disabled></textarea></td>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Description (รายละเอียด) :</p></td>
                          <td class="text-primary"><textarea class="form-control" rows="3" placeholder="หน่วยงานดำเนินงาน   
ฝ่ายไทย  :    มหาวิทยาลัยอุบลราชธานี
ฝ่ายลาว   :    กรมเลี้ยงสัตว์และการประมง " disabled></textarea></td>
                        </tr>
                        
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Start/End Date (วันเริ่มต้น/สิ้นสุดโครงการ) :</p></td>
                          <td class="text-primary">
                            <div class="col-sm-6">
                                <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                                <input type="text" class="form-control pull-right" id="StartPro" placeholder="24/03/2013 - 31/05/2015" disabled>
                                 </div><!-- /.input group -->
                              </div></td>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Budget Year (ปีงบประมาณ) :</p></td>
                          <td class="text-primary">
                              <div class="col-sm-6">
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option>Select Year</option>
                                   <option>2560</option>
                                  <option>2559</option>
                                  <option selected="selected">2558</option>
                                  <option>2557</option>
                                  <option>2556</option>
                                  <option>2555</option>
                                  <option>2554</option>
                                  <option>2553</option>
                                  <option>2552</option>
                                  <option>2551</option>
                                  <option>2550</option>
                                  <option>2549</option>
                                </select>
                                </div><!-- /.form-group --></td>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Type Budget (ประเภทงบประมาณ) :</p></td>
                          <td class="text-primary">
                              <div class="col-sm-4">
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">ประเภทงบประมาณ</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                                </div><!-- /.form-group -->
                              <div class="col-sm-8">
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">งบประมาณย่อย</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                                </div><!-- /.form-group --></td>
                        </tr>
                         <tr>
                          <td style="width: 280px"><p class="pull-right">Allocated  Budget (จัดสรรงบประมาณ) :</p></td>
                          <td class="text-success">
                             <div class="col-sm-6">
                               <div class="input-group">
                                 <div class="input-group-addon">
                                 <i class="fa fa-dollar"></i>
                                 </div>
                                <input type="text" class="form-control pull-right" id="Budget" placeholder="10,000,000.00 บาท" disabled >
                               </div><!-- /.input group -->
                              </div></td>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Cooperation Framework  </p><br /><p class="pull-right">(กรอบความร่วมมือ) :</p></td>
                          <td class="text-primary">
                              <div class="col-sm-12">
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">- (N/A)</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                                </div><!-- /.form-group --></td>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Cooperation Type (ประเภทความร่วมมือ) :</p></td>
                          <td class="text-primary">
                              <div class="col-sm-12">
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">- (Bilateral)</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                              </div><!-- /.form-group --></td>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">OECD Aid Type (ประเภท OECD):</p></td>
                          <td class="text-primary">
                              <div class="col-sm-12">
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">N/A</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                                </div><!-- /.form-group --></td>
                        </tr>
                  
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Location (พื้นที่โครงการ) :</p></td>
                          <td class="text-primary">
                              <div class="col-sm-12">   
                                <input class="form-control" type="text" placeholder="" disabled>
                              </div></td>
                        </tr>
                        
                         <tr class="bg-info">
                          <td style="width: 150px" colspan="2">
                          <h4 class="text-blue">Agency (หน่วยงาน)</h4></td>
                        </tr>
                        
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Funding Agency </p><p class="pull-right">(หน่วยงานให้ความช่วยเหลือ) :</p></td>
                          <td class="text-primary">
                              <div class="col-sm-12">
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">MINISTRY OF FOREIGN AFFAIRS</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                                </div><!-- /.form-group --></td>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Executing Agency (หน่วยงานดำเนินการ) :</p></td>
                          <td class="text-primary">
                          <div class="col-sm-12">
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">Thailand International Development Cooperation Agency (TICA)</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                                </div><!-- /.form-group -->
                          </td>
                        </tr>
                        <tr>
                        <td style="width: 250px"><p class="pull-right">Implementing Agency </p><p class="pull-right">(หน่วยงานดำเนินการ) :</p></td>
                      
                          <td class="text-primary">
                              <table class="table table-bordered">
                                <tr class="bg-gray">
                                  <td>Country (ประเทศ)</td>
                                  <td style="width: 350px" colspan="2">Agency (หน่วยงาน)</td>
                                </tr>
                             <tr>
                              <td>
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">Thailand</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select></td>
                               <td><select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">มหาวิทยาลัยศรีนครินทรวิโรฒ</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select></td>
                                <td style="width: 50px"><a class="btn btn-social-icon btn-google btn-sm" data-toggle="tooltip" title="ลบ"><i class="fa fa-trash"></i></a></td>
                                </tr>
                              <tr>
                              <td>
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">Lao</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select></td>
                               <td><select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">วิทยาลัยศิลปศึกษา กระทรวงศึกษาธิการและการกีฬา</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select></td>
                                <td style="width: 50px"><a class="btn btn-social-icon btn-google btn-sm" data-toggle="tooltip" title="ลบ"><i class="fa fa-trash"></i></a></td>
                                </tr>
                                    <tr>
                                    <td colspan="2"></td>
                                    <td><a class="btn btn-social-icon btn-linkedin btn-sm" data-toggle="tooltip" title="เพิ่มหน่วยงานดำเนินการ"><i class="fa fa-plus"></i></a></td>
                                  </tr>
                           </table>
                         </td>
                        </tr>

                         
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Co-Funding (ผู้ร่วมให้เงินทุน) :</p></td>
                          <td class="text-primary">
                            <table class="table table-bordered">
                                <tr class="bg-gray">
                                  <td>Country (ประเทศ)</td>
                                  <td style="width: 340px" colspan="2">Amount (จำนวนเงิน)</td>
                                </tr>
                                <tr>
                                  <td>
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">-</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select></td>
                                <td><div class="input-group">
                                 <div class="input-group-addon">
                                   <i class="fa fa-dollar"></i>
                                 </div>
                                <input type="text" class="form-control pull-right" id="Amount2" placeholder="0.00 บาท" disabled>
                               </div></td>
                               <td style="width: 50px"><a class="btn btn-social-icon btn-google btn-sm" data-toggle="tooltip" title="ลบ"><i class="fa fa-trash"></i></a></td>
                                </tr>
                              <tr>
                                <td class="pull-right"><b>รวมทั้งสิ้น</b></td>
                                <td>0.00 บาท</td>
                                <td><a class="btn btn-social-icon btn-linkedin btn-sm" data-toggle="tooltip" title="เพิ่มผู้ร่วมให้เงินทุน"><i class="fa fa-plus"></i></a></td>
                              </tr>
                           </table></td>
                        </tr>
                        
                        
                        <tr class="bg-info">
                          <td style="width: 150px" colspan="2">
                          <h4 class="text-blue">Other Detail (รายละเอียดอื่นๆ)</h4></td>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Assistant (ผู้ช่วยโครงการ) :</p></td>
                          <td class="text-primary">
                                <select class="form-control select2" multiple="multiple" data-placeholder="-" style="width: 100%;" >

                                  <option>เจนจิรา  ทองมี</option>
                                  <option>ภัทรา  ลีลาทนาพร</option>
                                  <option>อุษา  ชิตวิลัย</option>
                                  <option>อาณัติชัย  สงมา</option>
                                  <option>ธวัชชัย  มูลมาตร</option>
                                  <option>พัชรินทร์  อาบครบุรี</option>
                                  <option>ตะวัน  เวียงธงษารักษ์</option>
                                </select>
                             </td>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Contact Person (ข้อมูลผู้ติดต่อ) :</p></td>
                              <td class="text-primary">
                                  <table class="table table-bordered">
                                    <tr>
                                      <td style="width: 150px" class="bg-gray"><p class="pull-right">Name (ชื่อ) :</p></td>
                                      <td>
                                        <div class="input-group">
                                         <div class="input-group-addon">
                                           <i class="fa fa-user"></i>
                                         </div>
                                        <input type="text" class="form-control pull-right" placeholder="" disabled>
                                       </div></td>
                                     </tr>
                                     <tr>
                                      <td style="width: 50px" class="bg-gray"><p class="pull-right">Position (ตำแหน่ง):</p></td>
                                      <td><div class="input-group">
                                         <div class="input-group-addon">
                                           <i class="fa fa-building"></i>
                                         </div>
                                        <input type="text" class="form-control pull-right" placeholder="" disabled>
                                       </div></td>
                                    </tr>
                                    <tr>
                                      <td style="width: 50px" class="bg-gray"><p class="pull-right">Phone (โทรศัพท์) :</p></td>
                                      <td><div class="input-group">
                                         <div class="input-group-addon">
                                           <i class="fa fa-phone"></i>
                                         </div>
                                        <input type="text" class="form-control pull-right" placeholder="022035000" disabled>
                                       </div></td>
                                    </tr>
                                    <tr>
                                      <td style="width: 50px" class="bg-gray"><p class="pull-right">Fax (โทรสาร) :</p></td>
                                      <td><div class="input-group">
                                         <div class="input-group-addon">
                                           <i class="fa fa-fax"></i>
                                         </div>
                                        <input type="text" class="form-control pull-right" placeholder="" disabled>
                                       </div></td>
                                    </tr>
                                    <tr>
                                      <td style="width: 50px" class="bg-gray"><p class="pull-right">Email :</p></td>
                                      <td><div class="input-group">
                                         <div class="input-group-addon">
                                           <i class="fa fa-list-alt"></i>
                                         </div>
                                        <input type="text" class="form-control pull-right" placeholder="" disabled>
                                       </div></td>
                                    </tr>
                                   </table>
                               </td>
                        </tr>
                        <tr>
                          <td style="width: 150px"><p class="pull-right">Note (หมายเหตุ) :</p></td>
                          <td class="text-primary"><textarea class="form-control" rows="3" placeholder="-" disabled></textarea></td>
                        </tr>
                      <tr>
                          <td style="width: 150px"><p class="pull-right">File input (เพิ่มไฟล์) :</p> </td>
                          <td class="text-primary"><input type="file" id="exampleInputFile"> <i class="text-green">(ขนาดไฟล์ไม่เกิน 5 MB)</i><br />
                              <table class="table">
                                    <tr>
                                      <td style="width: 250px"><input type="text" class="form-control pull-right" placeholder="" data-toggle="tooltip" title="แก้ไขชื่อไฟล์"></td>
                                      <td style="width: 400px"><i class="fa fa-file-pdf-o text-red"></i> ข่าวประชาสัมพันธ์.pdf</td>
                                      <td><i class="fa fa-close  text-red" data-toggle="tooltip" title="ลบไฟล์"></i></td>
                                     </tr>
                                     <tr>
                                      <td style="width: 250px"><input type="text" class="form-control pull-right" placeholder="" data-toggle="tooltip" title="แก้ไขชื่อไฟล์"></td>
                                      <td style="width: 400px"><i class="fa fa-file-excel-o text-green"></i> รายชื่อผู้รับทุน.xlsx</td>
                                      <td><i class="fa fa-close  text-red" data-toggle="tooltip" title="ลบไฟล์"></i></td>
                                     </tr>
                                    <tr>
                                      <td style="width: 250px"><input type="text" class="form-control pull-right" placeholder="" data-toggle="tooltip" title="แก้ไขชื่อไฟล์"></td>
                                      <td style="width: 400px"><i class="fa fa-picture-o"></i> ความช่วยเหลือ.png</td>
                                      <td><i class="fa fa-close  text-red" data-toggle="tooltip" title="ลบไฟล์"></i></td>
                                     </tr>
                                   </table>
                            
                          </td>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Transfer Project To (มอบหมายโครงการให้) :</p></td>
                          <td class="text-primary"> 
                             <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-users"></i>
                              </div>
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">-</option>
                                  <option>เจนจิรา  ทองมี</option>
                                  <option>ภัทรา  ลีลาทนาพร</option>
                                  <option>อุษา  ชิตวิลัย</option>
                                  <option>อาณัติชัย  สงมา</option>
                                  <option>ธวัชชัย  มูลมาตร</option>
                                  <option>พัชรินทร์  อาบครบุรี</option>
                                  <option>ตะวัน  เวียงธงษารักษ์</option>
                                </select>
                                </div><!-- /.form-group --></td>
                        </tr>
                      </table>

                    <div class="box-footer">
                        <div class="col-sm-8"></div>
                        <div class="col-sm-2">
                          <a href="#" class="btn btn-block btn-social btn-success"><i class="fa fa-save"></i>Save </a>
                        </div>
                        <div class="col-sm-2">
                          <a href="#" class="btn btn-block btn-social btn-google"><i class="fa fa-reply"></i>Cancel </a>
                        </div>
                    </div>
                    </div><!-- /.box-body -->
                  </div><!-- /.tab-pane -->
                  
                <div class="tab-pane" id="tab_2">

                    <div class="box-header with-border">        
                      <div class="col-sm-8">
                                <p><a data-toggle="modal" data-target="#AddCourse">
                                <button class="btn bg-orange margin-r-5 btn-social"><i class="fa fa-plus"></i>Add Activity</button></a></p> </div>
                        <div class="col-sm-2">
                          <a data-toggle="collapse" data-parent="#accordion" href="#Year" class="btn btn-block btn-social btn-info"><i class="fa fa-calendar" data-toggle="tooltip" title="ดูตามปี"></i>Year </a>
                        </div>
                        <div class="col-sm-2">
                          <a data-toggle="collapse" data-parent="#accordion" href="#Month" class="btn btn-block btn-social btn-primary"><i class="fa fa-calendar" data-toggle="tooltip" title="ดูตามเดือน"></i>Month </a>
                        </div>

                      </div>
                <div id="Year" class="panel-collapse collapse active">
                <div style="overflow-x:auto;"> 
                 <table class="table table-bordered" style="width: 100%">
                    <tr>
                        <th colspan="9" class="text-center bg-info">Detail (รายละเอียด)</th>
                        <th colspan="7" class="text-center bg-gray">Year (ปี)</th>
                    </tr>
                    <tr>
                       
                      <th class="bg-info"></th>
                      <th class="bg-info"></th>
                      <th style="width: 800px" class="bg-info">Course (หลักสูตร)</th>
                      
                      <th class="bg-info">Start Date</th>
                      <th class="bg-info">End Date</th>
                      <th class="bg-info">Duration</th>
                      <th class="bg-info">Butget</th>
                      <th class="bg-info">Tool</th>
                      <th class="bg-info">Status</th>

                      <th class="bg-gray">2552</th>
                      <th class="bg-gray">2553</th>
                      <th class="bg-gray">2554</th>
                      <th class="bg-gray">2555</th>
                      <th class="bg-gray">2556</th>
                      <th class="bg-gray">2557</th>
                      <th class="bg-gray">2558</th>
                    </tr>
                    <tr>
                      <td colspan="2"><a class="icon fa fa-plus"></a></td>
                      <td><input type="text" id="txt1" value="กิจกรรมที่ 1 แผนงานพัฒนาบุคลากรของวิทยาลัย" size="40" style="border:none; cursor:default;" readonly /></td>
                      <td>01/01/2552</td>
                      <td>01/01/2555</td>
                      <td>1430 วัน</td>
                      <td>50,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td><span class="badge bg-yellow">เปิดรับสมัคร</span></td>
                      <td colspan="4">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="3"></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a class="icon fa fa-minus"></a></td>
                      <td style="width: 800px">1.1 ทุนดูงานสำหรับผู้บริหาร 7 คน ณ ปทท.</td>
                      <td>01/01/2552</td>
                      <td>01/01/2554</td>
                      <td>1065 วัน</td>
                      <td>25,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td><span class="badge bg-yellow">เปิดรับสมัคร</span></td>
                      <td colspan="3">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="4"></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a class="icon fa fa-minus"></a></td>
                      <td style="width: 800px">1.2 ทุนศึกษา ป.ตรี 2 ทุน ป.โท 2 ทุน/ปี รวม 4 ทุน</td>
                      <td>01/01/2553</td>
                      <td>01/01/2555</td>
                      <td>1064 วัน</td>
                      <td>25,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td><span class="badge bg-gray">ปิดรับสมัคร</span></td>
                     <td colspan="1"></td>
                      <td colspan="3">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="3"></td>
                    </tr>
                    <tr>
                      <td colspan="2"><a class="icon fa fa-plus"></a></td>
                      <td style="width: 800px">กิจกรรมที่ 2 แผนงานถ่ายทอดความรู้</td>
                      <td>01/01/2554</td>
                      <td>01/01/2556</td>
                      <td>1065 วัน</td>
                      <td>100,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td><span class="badge bg-yellow">เปิดรับสมัคร</span></td>
                     <td colspan="2"></td>
                      <td colspan="3">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="2"></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a class="icon fa fa-minus"></a></td>
                      <td style="width: 800px">2.1 ส่ง ผชช. ไทย สาขาละ 2 คน ไปนิเทศน์การถ่ายทอดความรู้ ณ สปป.ลาว 2 ครั้ง/ปี</td>
                      <td>01/01/2554</td>
                      <td>01/01/2556</td>
                      <td>1065 วัน</td>
                      <td>100,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td><span class="badge bg-yellow">เปิดรับสมัคร</span></td>
                     <td colspan="2"></td>
                      <td colspan="3">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="2"></td>
                    </tr>
                    <tr>
                      <td colspan="2"><a class="icon fa fa-plus"></a></td>
                      <td style="width: 800px">กิจกรรมที่ 3 แผนงานสนับสนุนอุปกรณ์และสื่อการเรียนการสอนและคอมพิวเตอร์ 41 ชุด</td>
                      <td>01/01/2555</td>
                      <td>01/01/2557</td>
                      <td>1065 วัน</td>
                      <td>2,050,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                     <td><span class="badge bg-gray">ปิดรับสมัคร</span></td>
                     <td colspan="3"></td>
                      <td colspan="3">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="1"></td>
                    </tr>
                    <tr>
                      <td colspan="2"><a class="icon fa fa-plus"></a></td>
                      <td style="width: 800px">กิจกรรมที่ 4 แผนงานติดตามและประเมินผล</td>
                      <td>01/01/2555</td>
                      <td>01/01/2558</td>
                      <td>1430 วัน</td>
                      <td>1,000,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td><span class="badge bg-gray">ปิดรับสมัคร</span></td>
                     <td colspan="3"></td>
                      <td colspan="4">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a class="icon fa fa-minus"></a></td>
                      <td style="width: 800px">4.1 การประชุม PMC ทุกปี ๆ ละ 1 ครั้ง</td>
                      <td>01/01/2555</td>
                      <td>01/01/2557</td>
                      <td>1065 วัน</td>
                      <td>500,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td><span class="badge bg-gray">ปิดรับสมัคร</span></td>
                     <td colspan="3"></td>
                      <td colspan="3">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="1"></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a class="icon fa fa-minus"></a></td>
                      <td style="width: 800px">4.2 การติดตามประเมินผลโครงการ หลังจากสิ้นสุดระยะเวลา</td>
                      <td>01/01/2556</td>
                      <td>01/01/2558</td>
                      <td>1430 วัน</td>
                      <td>500,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td><span class="badge bg-yellow">เปิดรับสมัคร</span></td>
                     <td colspan="4"></td>
                      <td colspan="3">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                    </tr>
                  </table>
                </div>
               </div>

                <div id="Month" class="panel-collapse collapse">
                <div style="overflow-x:auto;"> 
                 <table class="table table-bordered" style="width: 100%">
                    <tr class="bg-gray text-center">
                        <th colspan="9" class="text-center bg-info">Detail (รายละเอียด)</th>
                        <th colspan="84" class="text-center bg-gray">Month (เดือน)</th>
                    </tr>
                    <tr class="bg-gray">
                      <th class="bg-info"></th>
                      <th class="bg-info"></th>
                      <th class="bg-info" style="width: 70%;">Course (หลักสูตร)</th>
                      
                      <th class="bg-info">Start Date</th>
                      <th class="bg-info">End Date</th>
                      <th class="bg-info">Duration</th>
                      <th class="bg-info">Budget</th>
                      <th class="bg-info">Tool</th>
                      <th class="bg-info">Status</th>
                        
                      <th class="bg-gray text-center">January 2552</th>
                      <th class="bg-gray text-center">February 2552</th>
                      <th class="bg-gray text-center">March 2552</th>
                      <th class="bg-gray text-center">April 2552</th>
                      <th class="bg-gray text-center">May 2552</th>
                      <th class="bg-gray text-center">June 2552</th>
                      <th class="bg-gray text-center">July 2552</th>
                      <th class="bg-gray text-center">August 2552</th>
                      <th class="bg-gray text-center">September 2552</th>
                      <th class="bg-gray text-center">October 2552</th>
                      <th class="bg-gray text-center">November 2552</th>
                      <th class="bg-gray text-center">December 2552</th>
                      <th class="bg-gray text-center">January 2553</th>
                      <th class="bg-gray text-center">February 2553</th>
                      <th class="bg-gray text-center">March 2553</th>
                      <th class="bg-gray text-center">April 2553</th>
                      <th class="bg-gray text-center">May 2553</th>
                      <th class="bg-gray text-center">June 2553</th>
                      <th class="bg-gray text-center">July 2553</th>
                      <th class="bg-gray text-center">August 2553</th>
                      <th class="bg-gray text-center">September 2553</th>
                      <th class="bg-gray text-center">October 2553</th>
                      <th class="bg-gray text-center">November 2553</th>
                      <th class="bg-gray text-center">December 2553</th>
                      <th class="bg-gray text-center">January 2554</th>
                      <th class="bg-gray text-center">February 2554</th>
                      <th class="bg-gray text-center">March 2554</th>
                      <th class="bg-gray text-center">April 2554</th>
                      <th class="bg-gray text-center">May 2554</th>
                      <th class="bg-gray text-center">June 2554</th>
                      <th class="bg-gray text-center">July 2554</th>
                      <th class="bg-gray text-center">August 2554</th>
                      <th class="bg-gray text-center">September 2554</th>
                      <th class="bg-gray text-center">October 2554</th>
                      <th class="bg-gray text-center">November 2554</th>
                      <th class="bg-gray text-center">December 2554</th>
                      <th class="bg-gray text-center">January 2555</th>
                      <th class="bg-gray text-center">February 2555</th>
                      <th class="bg-gray text-center">March 2555</th>
                      <th class="bg-gray text-center">April 2555</th>
                      <th class="bg-gray text-center">May 2555</th>
                      <th class="bg-gray text-center">June 2555</th>
                      <th class="bg-gray text-center">July 2555</th>
                      <th class="bg-gray text-center">August 2555</th>
                      <th class="bg-gray text-center">September 2555</th>
                      <th class="bg-gray text-center">October 2555</th>
                      <th class="bg-gray text-center">November 2555</th>
                      <th class="bg-gray text-center">December 2555</th>
                      <th class="bg-gray text-center">January 2556</th>
                      <th class="bg-gray text-center">February 2556</th>
                      <th class="bg-gray text-center">March 2556</th>
                      <th class="bg-gray text-center">April 2556</th>
                      <th class="bg-gray text-center">May 2556</th>
                      <th class="bg-gray text-center">June 2556</th>
                      <th class="bg-gray text-center">July 2556</th>
                      <th class="bg-gray text-center">August 2556</th>
                      <th class="bg-gray text-center">September 2556</th>
                      <th class="bg-gray text-center">October 2556</th>
                      <th class="bg-gray text-center">November 2556</th>
                      <th class="bg-gray text-center">December 2556</th>
                      <th class="bg-gray text-center">January 2557</th>
                      <th class="bg-gray text-center">February 2557</th>
                      <th class="bg-gray text-center">March 2557</th>
                      <th class="bg-gray text-center">April 2557</th>
                      <th class="bg-gray text-center">May 2557</th>
                      <th class="bg-gray text-center">June 2557</th>
                      <th class="bg-gray text-center">July 2557</th>
                      <th class="bg-gray text-center">August 2557</th>
                      <th class="bg-gray text-center">September 2557</th>
                      <th class="bg-gray text-center">October 2557</th>
                      <th class="bg-gray text-center">November 2557</th>
                      <th class="bg-gray text-center">December 2557</th>
                      <th class="bg-gray text-center">January 2558</th>
                      <th class="bg-gray text-center">February 2558</th>
                      <th class="bg-gray text-center">March 2558</th>
                      <th class="bg-gray text-center">April 2558</th>
                      <th class="bg-gray text-center">May 2558</th>
                      <th class="bg-gray text-center">June 2558</th>
                      <th class="bg-gray text-center">July 2558</th>
                      <th class="bg-gray text-center">August 2558</th>
                      <th class="bg-gray text-center">September 2558</th>
                      <th class="bg-gray text-center">October 2558</th>
                      <th class="bg-gray text-center">November 2558</th>
                      <th class="bg-gray text-center">December 2558</th>
                    </tr>
                    <tr>
                      <td colspan="2"><a class="icon fa fa-plus"></a></td>
                      <td><input type="text" id="txtm1" value="กิจกรรมที่ 1 แผนงานพัฒนาบุคลากรของวิทยาลัย" size="40" style="border:none; cursor:default;" readonly /></td>
                      <td>01/01/2552</td>
                      <td>01/01/2555</td>
                      <td>1430 วัน</td>
                      <td>10,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td><span class="badge bg-yellow">เปิดรับสมัคร</span></td>
                      <td colspan="48">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="36"></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a class="icon fa fa-minus"></a></td>
                      <td><input type="text" id="txtm11" value="1.1 ทุนดูงานสำหรับผู้บริหาร 7 คน ณ ปทท." size="40" style="border:none; cursor:default;" readonly /></td>
                      <td>01/01/2552</td>
                      <td>01/01/2554</td>
                      <td>1065 วัน</td>
                      <td>10,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td><span class="badge bg-yellow">เปิดรับสมัคร</span></td>
                      <td colspan="36">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="48"></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a class="icon fa fa-minus"></a></td>
                      <td><input type="text" id="txtm12" value="1.2 ทุนศึกษา ป.ตรี 2 ทุน ป.โท 2 ทุน/ปี รวม 4 ทุน" size="40" style="border:none; cursor:default;" readonly /></td>
                      <td>03/01/2553</td>
                      <td>03/10/2555</td>
                      <td>1064 วัน</td>
                      <td>10,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td><span class="badge bg-gray">ปิดรับสมัคร</span></td>
                     <td colspan="12"></td>
                      <td colspan="36">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="36"></td>
                    </tr>
                    <tr>
                      <td colspan="2"><a class="icon fa fa-plus"></a></td>
                      <td><input type="text" id="txtm2" value="กิจกรรมที่ 2 แผนงานถ่ายทอดความรู้" size="40" style="border:none; cursor:default;" readonly /></td>
                      <td>01/01/2554</td>
                      <td>01/01/2556</td>
                      <td>1065 วัน</td>
                      <td>10,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td><span class="badge bg-yellow">เปิดรับสมัคร</span></td>
                     <td colspan="24"></td>
                      <td colspan="24">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="36"></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a class="icon fa fa-minus"></a></td>
                      <td><input type="text" id="txtm21" value="2.1 ส่ง ผชช. ไทย สาขาละ 2 คน ไปนิเทศน์การถ่ายทอดความรู้ ณ สปป.ลาว 2 ครั้ง/ปี" size="40" style="border:none; cursor:default;" readonly /></td>
                      <td>01/01/2554</td>
                      <td>01/01/2556</td>
                      <td>1065 วัน</td>
                      <td>10,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td><span class="badge bg-yellow">เปิดรับสมัคร</span></td>
                     <td colspan="24"></td>
                      <td colspan="24">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="36"></td>
                    </tr>
                    <tr>
                      <td colspan="2"><a class="icon fa fa-plus"></a></td>
                      <td><input type="text" id="txtm3" value="กิจกรรมที่ 3 แผนงานสนับสนุนอุปกรณ์และสื่อการเรียนการสอนและคอมพิวเตอร์ 41 ชุด" size="40" style="border:none; cursor:default;" readonly /></td>
                      <td>01/01/2555</td>
                      <td>01/01/2557</td>
                      <td>1065 วัน</td>
                      <td>10,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td><span class="badge bg-gray">ปิดรับสมัคร</span></td>
                     <td colspan="36"></td>
                      <td colspan="24">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="24"></td>
                    </tr>
                    <tr>
                      <td colspan="2"><a class="icon fa fa-plus"></a></td>
                      <td><input type="text" id="txtm4" value="กิจกรรมที่ 4 แผนงานติดตามและประเมินผล" size="40" style="border:none; cursor:default;" readonly /></td>
                      <td>01/01/2555</td>
                      <td>01/01/2558</td>
                      <td>1430 วัน</td>
                      <td>10,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td><span class="badge bg-gray">ปิดรับสมัคร</span></td>
                     <td colspan="36"></td>
                      <td colspan="48">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a class="icon fa fa-minus"></a></td>
                      <td><input type="text" id="txtm41" value="4.1 การประชุม PMC ทุกปี ๆ ละ 1 ครั้ง" size="40" style="border:none; cursor:default;" readonly /></td>
                      <td>01/01/2555</td>
                      <td>01/01/2557</td>
                      <td>1065 วัน</td>
                      <td>10,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td><span class="badge bg-gray">ปิดรับสมัคร</span></td>
                     <td colspan="36"></td>
                      <td colspan="36">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="12"></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a class="icon fa fa-minus"></a></td>
                      <td><input type="text" id="txtm42" value="4.2 การติดตามประเมินผลโครงการ หลังจากสิ้นสุดระยะเวลา" size="40" style="border:none; cursor:default;" readonly /></td>
                      <td>01/01/2556</td>
                      <td>01/01/2558</td>
                      <td>1430 วัน</td>
                      <td>10,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourse"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td><span class="badge bg-yellow">เปิดรับสมัคร</span></td>
                     <td colspan="36"></td>
                      <td colspan="48">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="10"></td>
                    </tr>
                  </table>
                </div>
               </div>


                  </div><!-- /.tab-pane -->

                  <div class="tab-pane" id="tab_3">
                  <div class="box-header with-border">
                                
                             <div class="col-lg-5"><br />
                                <h5 class="text-primary"><b>พบทั้งหมด 4 รายการ</b></h5>
                             </div>

                            <div class="col-lg-5">
                                <div class="input-group margin">
                                <input type="text" class="form-control">
                                <span class="input-group-btn">
                                  <button class="btn btn-info btn-flat" type="button"><i class="fa fa-search" data-toggle="tooltip" title="ค้นหา"></i></button>
                                </span>
                              </div><!-- /input-group -->
                           </div>
                           <div class="col-lg-2">
                                <div class="input-group margin">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collap1">
                                    <button type="button" class="btn btn-success btn-flat" data-toggle="tooltip" title="ค้นหาเพิ่มเติม">
                                    <i class="fa fa-magic"></i>
                                    </button></a>
                                    <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                        <button type="button" class="btn btn-success btn-flat dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-print"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="#"><i class="fa fa-file-pdf-o text-red"></i>PDF</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#"><i class="fa fa-file-excel-o text-green"></i>EXCEL</a></li>
                                        </ul>
                                    </div>
                                </div>
                              </div>
                            
                        <!-- /.box-header -->
                        <div class="clearfix"></div>
                       <div id="collap1" class="panel-collapse collapse">
                           
                        <div class="box-header with-border">
                            <div class="box-body">
                                <!-- form start -->
                                <form class="form-horizontal">
                                  <div class="box-body">
                                    <div class="form-group">
                                      <label for="Sector" class="col-sm-2 control-label">Student ID :</label>
                                      <div class="col-sm-4">
                                        <input class="form-control" id="Project" placeholder="">
                                      </div>

                                       <label for="Sector" class="col-sm-2 control-label">Country :</label>
                                      <div class="col-sm-4">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Select</option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                        </select>
                                      </div>
                                   </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                      <label for="Sector" class="col-sm-2 control-label">Course (หลักสูตร) :</label>
                                      <div class="col-sm-4">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Select</option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                        </select>
                                      </div>
                                      
                                      <label for="Sector" class="col-sm-2 control-label">Start/End Date :</label>
                                      <div class="col-sm-4">
                                        <div class="input-group">
                                          <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                           </div>
                                         <input type="text" class="form-control pull-right" id="reservation">
                                        </div><!-- /.input group -->
                                      </div>
                                    </div>
                                  
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                      <label for="inputname" class="col-sm-2 control-label">Sector :</label>
                                      <div class="col-sm-4">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Select</option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                        </select>
                                      </div>

                                      <label for="inputname" class="col-sm-2 control-label">Status :</label>
                                      <div class="col-sm-4">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Select</option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                        </select>
                                      </div>


                                  </div><!-- /.box-body -->
                                  </div>
                                </form>
                               
                                <!-- /.row -->
                            </div>
                            <!-- /.box-body -->
                           
                        </div>
                        </div>
                      </div>

                 <table class="table table-bordered">
                      <tr class="bg-gray text-center">
                      <th style="width: 10px">No.</th>
                      <th>Student ID</th>
                      <th style="width: 200px">Name</th>
                      <th>Country</th>
                      <th>Course</th>
                      <th>Start-End Date</th>
                      <th>Status</th>
                    </tr>
                    <tr>
                      <td>1.</td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">56199130400</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Viengthone Thoummachan</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Laos</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Master of Arts</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">24/03/2013-31/05/2015</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"><span class="badge bg-green">กำลังศึกษา</span></a></td>
                    </tr>
                    <tr>
                      <td>2.</td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">56199130401</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Khamphet Sengouly</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Laos</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Master of Arts</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">24/03/2013-31/05/2015</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"><span class="badge bg-green">กำลังศึกษา</span></a></td>
                    </tr>
                  </table>
                
                   </div><!-- /.tab-content -->
              
                </div><!-- nav-tabs-custom -->

              </div><!-- /.col -->
            </div>
          </div><!-- /.row -->
        </section><!-- /.content -->
   </div>
    
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />
</asp:Content>

