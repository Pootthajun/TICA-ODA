﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmFrontMaster.Master" CodeBehind="frmFrontNews.aspx.vb" Inherits="ODA_VBPrototype.frmFrontNews" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>ข่าวทั่วไป | เกี่ยวกับกรมฯ</title>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
      <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <li><a href="frmFrontIndex.aspx">หน้าหลัก</a></li>
                            <li>
                                <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="frmAbout.aspx">
                                    เกี่ยวกับกรมฯ
                                    <i class="icon-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="frmFrontTICAAbout.aspx">ประวัติกรมความร่วมมือระหว่างประเทศ</a></li>
                                    <li><a href="frmFrontPolicyPlan.aspx">วิสัยทัศน์ พันธกิจ สัญลักษณ์</a></li>
                                    <li><a href="frmFrontOrganization">โครงสร้างองค์กร</a></li>
                                    <li><a href="frmFrontPerformance.aspx">ผลการดำเนินงานของกรม</a></li>
                                </ul>
                            </li>
                            <li class="dropdown active"><a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="frmAbout.aspx">
                                    ศูนย์ข่าว
                                    <i class="icon-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="active"><a href="frmFrontNews.aspx">ข่าวทั่วไป</a></li>
                                    <li><a href="frmFrontNewsODA.aspx">ข่าวกรมฯ</a></li>
                            </ul></li>
                            <li><a href="frmFrontRecipient.aspx">ฟอร์มกรอกเอกสาร</a></li>  
                            <li><a href="frmFrontContact.aspx">ติดต่อกรมฯ</a></li>                        
                            <%--<li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">
                                    รายงาน
                                    <i class="icon-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="portfolio_4.html">Project</a></li>
                                    <li><a href="portfolio_3.html">Non-Project</a></li>
                                    <li><a href="portfolio_2.html">Loan</a></li>
                                    <li><a href="portfolio_item.html">Contribuition</a></li>
                                </ul>
                            </li>--%>
                          
                         </ul>    
                    </div>
                    <!-- BEGIN TOP NAVIGATION MENU -->
       
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
      
     <!-- BEGIN BREADCRUMBS -->   
    <div class="row-fluid breadcrumbs margin-bottom-40">
        <div class="container">
            <div class="span4">
                <h2>ข่าวทั่วไป</h2>
            </div>
            <div class="span8">
                <ul class="pull-right breadcrumb">
                    <li><a href="frmFrontIndex.aspx"><i class="icon-home"></i></a> <span class="divider">/</span></li>
                    <li class="active">ข่าวทั่วไป</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END BREADCRUMBS -->

    <!-- BEGIN CONTAINER -->   
    <div class="container min-hight">
        <!-- BEGIN BLOG -->
        <div class="row-fluid">
            <!-- BEGIN LEFT SIDEBAR -->            
            <div class="span9 blog-posts margin-bottom-40">
                <div class="row-fluid">
                    <div class="span4">
                        <img src="assets/img/news/n1.jpg" alt="">
                    </div>
                    <div class="span8">
                        <h2><a href="blog_item.html">รมว.กระทรวงศึกษามอบนโยบาย</a></h2>
                        <ul class="blog-info">
                            <li><i class="icon-calendar"></i> 19/10/2016</li>
                            <li><i class="icon-comments"></i> 17</li>
                            <li><i class="icon-tags"></i> Metronic, Keenthemes, UI Design</li>
                        </ul>
                         <p class="margin-bottom-10">พล.อ.ดาว์พงษ์ รัตนสุวรรณ รัฐมนตรีว่าการกระทรวงศึกษาธิการ มอบนโยบายในการประชุมเชิงปฏิบัติการเพื่อเตรียมความพร้อมผู้ผ่านการคัดเลือกเข้าร่วมโครงการผลิตครูเพื่อพัฒนาท้องถิ่น รุ่นแรก ปีการศึกษา 2559 จำนวน 4,079 คน โดยมี พล.ต.ณัฐพงษ์ เพราแก้ว เลขานุการรัฐมนตรีว่าการกระทรวงศึกษาธิการ เป็นวิทยากรบรรยายแผนการปฏิรูปการศึกษาด้านการผลิตและพัฒนาครู และมี ดร.สุภัทร จำปาทอง เลขาธิการคณะกรรมการการอุดมศึกษา นายการุณ สกุลประดิษฐ์ เลขาธิการคณะกรรมการการศึกษาขั้นพื้นฐาน ผู้บริหารและข้าราชการสำนักงานคณะกรรมการการอุดมศึกษา (สกอ.) ให้การต้อนรับ และร่วมการประชุมในครั้งนี้</p>
                             <a class="more" href="#">Read more <i class="icon-angle-right"></i></a>
                    </div>
                </div>
                <hr class="blog-post-sep">
                <div class="row-fluid">
                    <div class="span4">
                        <img src="assets/img/news/n4.jpg" alt="">
                    </div>
                    <div class="span8">
                        <h2><a href="blog_item.html">กฎระเบียบต่างๆเกี่ยวกับการเลื่อนเงินเดือนข้าราชการพลเรือนสามัญ</a></h2>
                        <ul class="blog-info">
                            <li><i class="icon-calendar"></i> 19/10/2016</li>
                            <li><i class="icon-comments"></i> 17</li>
                            <li><i class="icon-tags"></i> Metronic, Keenthemes, UI Design</li>
                        </ul>
                        <p>เมื่อวันที่ 19 ตุลาคม 2559 เว็บไซต์กระทรวงศึกษาธิการ ได้เผยแพร่ข้อมูลเรื่อง กฎระเบียบต่างๆเกี่ยวกับการเลื่อนเงินเดือนข้าราชการพลเรือนสามัญ โดยท่านสามารถดาวน์โหลดรายละเอียดต่างๆ ได้ที่นี่</p>
                        <a class="more" href="#">Read more <i class="icon-angle-right"></i></a>
                    </div>
                </div>
                <hr class="blog-post-sep">
                <div class="row-fluid">
                    <div class="span4">
                        <img src="assets/img/news/n5.jpg" alt="">
                    </div>
                    <div class="span8">
                        <h2><a href="blog_item.html">ด่วนที่สุด อัพเดท 19ต.ค.59 เรื่อง การคัดเลือกนักศึกษาทุนโครงการผลิตครูเพื่อพัฒนาท้องถิ่น ปีการศึกษา 2559 (ปรับกำหนดการคัดเลือก)</a></h2>
                        <ul class="blog-info">
                            <li><i class="icon-calendar"></i> 19/10/2016</li>
                            <li><i class="icon-comments"></i> 17</li>
                            <li><i class="icon-tags"></i> Metronic, Keenthemes, UI Design</li>
                        </ul>
                        <p>ด่วนที่สุด ที่ ศธ 04009/1046 ลงวันที่ 19 ตุลาคม 2559 เรื่อง การคัดเลือกนักศึกษาทุนโครงการผลิตครูเพื่อพัฒนาท้องถิ่น ปีการศึกษา 2559 (แจ้ง กศจ.) </p>
                        <a class="more" href="#">Read more <i class="icon-angle-right"></i></a>
                    </div>
                </div>
                <hr class="blog-post-sep">
                <div class="row-fluid">
                    <div class="span4">
                        <img src="assets/img/news/n6.jpg" alt="">
                    </div>
                    <div class="span8">
                        <h2><a href="blog_item.html">การย้ายผู้บริหารสถานศึกษาในโครงการโรงเรียนประชารัฐ</a></h2>
                        <ul class="blog-info">
                            <li><i class="icon-calendar"></i> 19/10/2016</li>
                            <li><i class="icon-comments"></i> 17</li>
                            <li><i class="icon-tags"></i> Metronic, Keenthemes, UI Design</li>
                        </ul>
                        <p>ด่วนที่สุด ที่ ศธ 04009/ว1059 ลงวันที่ 19 ตุลาคม 2559 เรื่อง การย้ายผู้บริหารสถานศึกษาในโครงการโรงเรียนประชารัฐ</p>
                        <a class="more" href="blog_item.html">Read more <i class="icon-angle-right"></i></a>
                    </div>
                </div>
                <div class="pagination pagination-centered">
                    <ul>
                        <li><a href="#">Prev</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li class="active"><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">Next</a></li>
                    </ul>
                </div>                
            </div>
            <!-- END LEFT SIDEBAR -->

            <!-- BEGIN RIGHT SIDEBAR -->            
            <div class="span3 blog-sidebar">
                <!-- BEGIN RECENT NEWS -->                            
                <h2>Recent News</h2>
                <div class="recent-news margin-bottom-10">
                    <div class="row-fluid margin-bottom-10">
                        <div class="span3">
                            <img src="assets/img/people/img2-large.jpg" alt="">                        
                        </div>
                        <div class="span9 recent-news-inner">
                            <h3><a href="#">Letiusto gnissimos</a></h3>
                            <p>Decusamus tiusto odiodig nis simos ducimus qui sint</p>
                        </div>                        
                    </div>
                    <hr />
                    <div class="row-fluid margin-bottom-10">
                        <div class="span3">
                            <img src="assets/img/people/img1-large.jpg" alt="">                        
                        </div>
                        <div class="span9 recent-news-inner">
                            <h3><a href="#">Deiusto anissimos</a></h3>
                            <p>Decusamus tiusto odiodig nis simos ducimus qui sint</p>
                        </div>                        
                    </div>
                    <hr />
                    <div class="row-fluid margin-bottom-10">
                        <div class="span3">
                            <img src="assets/img/people/img3-large.jpg" alt="">                        
                        </div>
                        <div class="span9 recent-news-inner">
                            <h3><a href="#">Tesiusto baissimos</a></h3>
                            <p>Decusamus tiusto odiodig nis simos ducimus qui sint</p>
                        </div>                        
                    </div>
                    <hr />
                    <div class="row-fluid margin-bottom-10">
                        <div class="span3">
                            <img src="assets/img/people/img1-large.jpg" alt="">                        
                        </div>
                        <div class="span9 recent-news-inner">
                            <h3><a href="#">Deiusto anissimos</a></h3>
                            <p>Decusamus tiusto odiodig nis simos ducimus qui sint</p>
                        </div>                        
                    </div>
                    <hr />
                    <div class="row-fluid margin-bottom-10">
                        <div class="span3">
                            <img src="assets/img/people/img3-large.jpg" alt="">                        
                        </div>
                        <div class="span9 recent-news-inner">
                            <h3><a href="#">Tesiusto baissimos</a></h3>
                            <p>Decusamus tiusto odiodig nis simos ducimus qui sint</p>
                        </div>                        
                    </div>
                    <hr />
                    <div class="row-fluid margin-bottom-10">
                        <div class="span3">
                            <img src="assets/img/people/img3-large.jpg" alt="">                        
                        </div>
                        <div class="span9 recent-news-inner">
                            <h3><a href="#">Tesiusto baissimos</a></h3>
                            <p>Decusamus tiusto odiodig nis simos ducimus qui sint</p>
                        </div>                        
                    </div>
                </div>
                <!-- END RECENT NEWS -->                            

                <!-- BEGIN BLOG TALKS -->
                <div class="blog-talks margin-bottom-30">
                    <h2>Popular Talks</h2>
                    <div class="tab-style-1">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab-1" data-toggle="tab">Multipurpose</a></li>
                            <li><a href="#tab-2" data-toggle="tab">Documented</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane row-fluid fade in active" id="tab-1">
                                <p class="margin-bottom-10">Raw denim you probably haven't heard of them jean shorts Austin. eu banh mi, qui irure terry richardson ex squid Aliquip placeat salvia cillum iphone.</p>
                                <p><a href="#" class="read-more">Read more</a></p>
                            </div>
                            <div class="tab-pane fade" id="tab-2">
                                <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. aliquip jean shorts ullamco ad vinyl aesthetic magna delectus mollit. Keytar helvetica VHS salvia..</p>
                            </div>
                        </div>
                    </div>
                </div>                            
                <!-- END BLOG TALKS -->


                <!-- END BLOG TAGS -->
            </div>
            <!-- END RIGHT SIDEBAR -->            
        </div>
        <!-- END BEGIN BLOG -->
    </div>
    <!-- END CONTAINER -->
</asp:Content>
