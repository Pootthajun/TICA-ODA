﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="frmDashborad.aspx.vb" Inherits="ODA_VBPrototype.frmDashborad" %>


<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Dashboard | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
            <li class="treeview">
              <a href="frnFinance.aspx">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li ><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i> Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li><a href="frmSubExpense.aspx"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="treeview">
              <a href="frmAllocatedBudget.aspx">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i> Budget</a></li>
                <li><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                    <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i> Sub Budget</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmReport.aspx">
                <i class="fa fa-file-text"></i> <span>Report</span></a>
            </li> 
            <li class="treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-sitemap"></i> <span>Organize Structure</span>
              </a>
             
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li><a href="frmSector.aspx"><i class="fa fa-file"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-list"></i> Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-list"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-list"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
   
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
            จำนวนโครงการ (Project, Non-Project)
          </h4>
          <ol class="breadcrumb">
            <li><a href="frmDashborad.aspx"><i class="fa fa-area-chart"></i> Dashborad</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Info boxes -->
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-2 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green-gradient">
                <div class="inner">
                  <h4 data-toggle="tooltip" title="Project,Non-Project">1550 โครงการ</h4>
                  <p>ปี 2561</p>
                </div>
                <div class="icon">
                   <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">
                  More info <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-2 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow-gradient">
                <div class="inner">
                  <h4 data-toggle="tooltip" title="Project,Non-Project">1150 โครงการ</h4>
                  <p>ปี 2558</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">
                  More info <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-2 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green-gradient">
                <div class="inner">
                  <h4 data-toggle="tooltip" title="Project,Non-Project">1490 โครงการ</h4>
                  <p>ปี 2559</p>
                </div>
                <div class="icon">
                   <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">
                  More info <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-2 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow-gradient">
                <div class="inner">
                  <h4 data-toggle="tooltip" title="Project,Non-Project">1340 โครงการ</h4>
                  <p>ปี 2560</p>
                </div>
                <div class="icon">
                   <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">
                  More info <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-2 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green-gradient">
                <div class="inner">
                  <h4 data-toggle="tooltip" title="Project,Non-Project">1550 โครงการ</h4>
                  <p>ปี 2561</p>
                </div>
                <div class="icon">
                   <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">
                  More info <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-2 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow-gradient">
                <div class="inner">
                  <h4 data-toggle="tooltip" title="Project,Non-Project">2340 โครงการ</h4>
                  <p>ปี 2562</p>
                </div>
                <div class="icon">
                   <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">
                  More info <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
            </div><!-- ./col -->
          </div><!-- /.row -->

          <div class="row">
            <div class="col-md-7">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Recap Report</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <div class="btn-group">
                      <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                      </ul>
                    </div>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->

                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">
                    <div class="col-md-6">
                   
                    </div>
                    <div class="col-md-6">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">Select Year</option>
                      <option>2560</option>
                      <option>2559</option>
                      <option>2558</option>
                      <option>2557</option>
                      <option>2556</option>
                      <option>2555</option>
                      <option>2554</option>
                      <option>2553</option>
                      <option>2552</option>
                      <option>2551</option>
                      <option>2550</option>
                      <option>2549</option>
                    </select>
                    </div>
                  <div class="clearfix"></div><br />
                      <div class="chart">
                        <!-- Sales Chart Canvas -->
	                    <div id="chartContainer3" style="height: 300px; width: 100%;"></div>
                      </div><!-- /.chart-responsive -->
                    </div><!-- /.col -->
                  </div><!-- /.row -->
                </div><!-- ./box-body -->
                </div>
            </div>
           <div class="col-md-5">
              <!-- LINE CHART -->
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h5 class="box-title">Project Component (ประเภทความช่วยเหลือ)</h5>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                 <div class="box-body">
                  <div class="row">
                     <div class="col-md-6">
                    <h5><b class="text-blue">Top 5 Component</b></h5>
                    <%--<select class="form-control select2" style="width: 100%;">
                      <option selected="selected">Select Component</option>
                      <option>Bachelor</option>
                      <option>Certificate</option>
                      <option>Diploma</option>
                      <option>Equipment</option>
                      <option>Expert</option>
                      <option>Master</option>
                      <option>Meeting</option>
                      <option>Mission</option>
                      <option>Other</option>
                      <option>PhD.</option>
                      <option>Postgraduate</option>
                      <option>Study Visit</option>
                      <option>Training</option>
                      <option>Undergraduate</option>
                      <option>Postgraduate</option>
                      <option>Volunteer</option>
                    </select>--%>
                    </div>

                      <div class="col-md-6">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">Select Year</option>
                      <option>2560</option>
                      <option>2559</option>
                      <option>2558</option>
                      <option>2557</option>
                      <option>2556</option>
                      <option>2555</option>
                      <option>2554</option>
                      <option>2553</option>
                      <option>2552</option>
                      <option>2551</option>
                      <option>2550</option>
                      <option>2549</option>
                    </select>
                    </div>
                  <div class="clearfix"></div><br />
                    <div class="col-md-12">
                      <div class="chart-responsive">
	                    <div id="chartContainer1" style="height: 2px; width: 0%;"></div>
                      </div><!-- ./chart-responsive -->
                    </div><!-- /.col -->
                   
                    <div class="col-sm-12">
                      <!-- Progress bars -->
                      <div class="clearfix">
                        <span class="pull-left">1. Master</span>
                        <small class="pull-right">630 โครงการ</small>
                      </div>
                      <div class="progress sm">
                        <div class="progress-bar progress-bar-light-blue" style="width: 87%;"></div>
                      </div>

                     <div class="clearfix">
                        <span class="pull-left">2. Bachelor</span>
                        <small class="pull-right">520 โครงการ</small>
                      </div>
                      <div class="progress sm">
                        <div class="progress-bar progress-bar-light-blue" style="width: 75%;"></div>
                      </div>

                    <div class="clearfix">
                        <span class="pull-left">3. Meeting</span>
                        <small class="pull-right">400 โครงการ</small>
                      </div>
                      <div class="progress sm">
                        <div class="progress-bar progress-bar-light-blue" style="width: 60%;"></div>
                      </div>

                    <div class="clearfix">
                        <span class="pull-left">4. Certificate</span>
                        <small class="pull-right">384 โครงการ</small>
                      </div>
                      <div class="progress sm">
                        <div class="progress-bar progress-bar-light-blue" style="width: 45%;"></div>
                      </div>

                    <div class="clearfix">
                        <span class="pull-left">5. PhD.</span>
                        <small class="pull-right">256 โครงการ</small>
                      </div>
                      <div class="progress sm">
                        <div class="progress-bar progress-bar-light-blue" style="width: 30%;"></div>
                      </div>
                    </div><!-- /.col -->
                   
                   </div><!-- /.box-body -->
                  </div><!-- /.row -->
                </div><!-- /.box-body -->

                </div>

            <div class="col-md-12">
                 <!-- Map box -->
              <div class="box box-solid bg-light-blue-gradient">
                <div class="box-header">
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-primary btn-sm daterange pull-right" data-toggle="tooltip" title="Date range"><i class="fa fa-calendar"></i></button>
                    <button class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->

                  <i class="fa fa-users"></i>
                  <h3 class="box-title">
                    Recipient Country (ประเทศที่รับทุน)
                  </h3>
                </div>
                <div class="box-body">
                  <div class="col-md-8">
                   <div class="col-md-6">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">Select Country</option>
                      <option>Alaska</option>
                      <option>California</option>
                      <option>Delaware</option>
                      <option>Tennessee</option>
                      <option>Texas</option>
                      <option>Washington</option>
                    </select>
                    </div>
                    <div class="col-md-6">
                    <!-- Date dd/mm/yyyy -->
                      <div class="form-group">
                        <select class="form-control select2" style="width: 100%;">
                          <option selected="selected">Select Year</option>
                          <option>2560</option>
                          <option>2559</option>
                          <option>2558</option>
                          <option>2557</option>
                          <option>2556</option>
                          <option>2555</option>
                          <option>2554</option>
                          <option>2553</option>
                          <option>2552</option>
                        </select>
                    </div>
                    </div>
                  <div class="clearfix"></div><br />
                    <div class="form-group">
                      <div id="world-map" style="height: 250px; width: 100%;"></div>
                    </div><!-- /.box-body-->
                   </div> 
                  <div class="col-md-4">
                    <h5><b>Top 5 Recipient Country</b></h5>
                     <div class="box-footer no-padding">
                      <ul class="nav nav-pills nav-stacked">
                        <li><a href="#"><b>Country(ประเทศ)</b> <span class="pull-right text-yellow">Amount(จำนวนเงิน)</span></a></li>
                        <li><a href="#">Laos <span class="pull-right text-green"><i class="fa fa-dollar"></i> 10,000,000.00</span></a></li>
                        <li><a href="#">Myanmar <span class="pull-right text-green"><i class="fa fa-dollar"></i> 9,000,000.00</span></a></li>
                        <li><a href="#">Cambodia <span class="pull-right text-green"><i class="fa fa-dollar"></i> 8,000,000.00</span></a></li>
                        <li><a href="#">Vietnam <span class="pull-right text-green"><i class="fa fa-dollar"></i> 6,500,000.00</span></a></li>
                        <li><a href="#">Thailand <span class="pull-right text-green"><i class="fa fa-dollar"></i> 5,000,000.00</span></a></li>
                      </ul>
                    </div><!-- /.footer -->
                    </div>
               
              </div>
             </div><!-- /.col -->
            </div>
            </div>
        </section><!-- /.content -->
      </div>
    
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />
    
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="plugins/morris/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="plugins/knob/jquery.knob.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script src="dist/js/pages/dashboard.js"></script>


    <script type="text/javascript">
        window.onload = function () {

            var chart = new CanvasJS.Chart("chartContainer1",
	        {

	            data: [
		        {
		            type: "pie",
		            dataPoints: [
				        { y: 630 },
				        { y: 520 },
				        { y: 400 },
				        { y: 384 },
				        { y: 256 }
		            ]
		        }
	            ]
	        });
            chart.render();

	            var chart = new CanvasJS.Chart("chartContainer3", {
	                //title: {
	                //    text: "Site Traffic",
	                //    fontSize: 20
	                //},
	                animationEnabled: true,
	                axisX: {
	                    gridColor: "Silver",
	                    tickColor: "silver",
	                    valueFormatString: "DD/MMM"
	                },
	                toolTip: {
	                    shared: true
	                },
	                theme: "theme7",
	                axisY: {
	                    gridColor: "Silver",
	                    tickColor: "silver"
	                },
	                legend: {
	                    verticalAlign: "center",
	                    horizontalAlign: "right"
	                },
	                data: [
                     {
                         type: "line",
                         showInLegend: true,
                         lineThickness: 2,
                         name: "Project",
                         color: "#00c0ef",
                         dataPoints: [
                         { x: new Date(2010, 0, 9), y: 890 },
                         { x: new Date(2011, 0, 11), y: 987 },
                         { x: new Date(2012, 0, 13), y: 600 },
                         { x: new Date(2013, 0, 15), y: 677 },
                         { x: new Date(2014, 0, 17), y: 900 },
                         { x: new Date(2015, 0, 19), y: 777 },
                         { x: new Date(2016, 0, 21), y: 345 },
                         { x: new Date(2017, 0, 23), y: 233 }
                         ]
                     },
                    {
                        type: "line",
                        showInLegend: true,
                        lineThickness: 2,
                        name: "Non-Project",
                        color: "#00a65a",
                        lineThickness: 3,

                        dataPoints: [
                        { x: new Date(2010, 0, 9), y: 640 },
                        { x: new Date(2011, 0, 11), y: 544 },
                        { x: new Date(2012, 0, 13), y: 693 },
                        { x: new Date(2013, 0, 15), y: 657 },
                        { x: new Date(2014, 0, 17), y: 663 },
                        { x: new Date(2015, 0, 19), y: 639 },
                        { x: new Date(2016, 0, 21), y: 673 },
                        { x: new Date(2017, 0, 23), y: 660 }
                        ]
                    },
                    {
                        type: "line",
                        showInLegend: true,
                        lineThickness: 2,
                        name: "Loan",
                        color: "#dd4b39",
                        dataPoints: [
                        { x: new Date(2010, 0, 9), y: 558 },
                        { x: new Date(2011, 0, 11), y: 888 },
                        { x: new Date(2012, 0, 13), y: 666 },
                        { x: new Date(2013, 0, 15), y: 332 },
                        { x: new Date(2014, 0, 17), y: 566 },
                        { x: new Date(2015, 0, 19), y: 457 },
                        { x: new Date(2016, 0, 21), y: 354 },
                        { x: new Date(2017, 0, 23), y: 876 }
                        ]
                    },
                    {
                        type: "line",
                        showInLegend: true,
                        lineThickness: 2,
                        name: "Contribuiltion",
                        color: "#f39c12",
                        dataPoints: [
                        { x: new Date(2010, 0, 9), y: 750 },
                        { x: new Date(2011, 0, 11), y: 500 },
                        { x: new Date(2012, 0, 13), y: 600 },
                        { x: new Date(2013, 0, 15), y: 700 },
                        { x: new Date(2014, 0, 17), y: 300 },
                        { x: new Date(2015, 0, 19), y: 544 },
                        { x: new Date(2016, 0, 21), y: 677 },
                        { x: new Date(2017, 0, 23), y: 345 }
                        ]
                    }
	                ],
	                legend: {
	                    cursor: "pointer",
	                    itemclick: function (e) {
	                        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
	                            e.dataSeries.visible = false;
	                        }
	                        else {
	                            e.dataSeries.visible = true;
	                        }
	                        chart.render();
	                    }
	                }
	            });

	            chart.render();

	            var chart = new CanvasJS.Chart("Chart",
            {
                title: {
                    text: "Employees Salary in a Company",
                },
                axisY: {
                    includeZero: false,
                    title: "Salary in USD(Thousands)",
                    interval: 10,
                },
                axisX: {
                    interval: 10,
                    title: "Departments",
                },
                data: [
                {
                    type: "rangeBar",
                    showInLegend: true,
                    yValueFormatString: "$#0.##K",
                    indexLabel: "{y[#index]}",
                    legendText: "Department wise Min and Max Salary",
                    dataPoints: [   // Y: [Low, High]
                        { x: 10, y: [80, 110], label: "Data Scientist" },
                        { x: 20, y: [95, 141], label: "Product Manager" },
                        { x: 30, y: [98, 115], label: "Web Developer" },
                        { x: 40, y: [90, 160], label: "Software Engineer" },
                        { x: 50, y: [100, 152], label: "Quality Assurance" }
                    ]
                }
                ]
            });
	            chart.render();
        }
	</script>
	<script src="dist/canvasjs/canvasjs.min.js"></script>
    

</asp:Content>
