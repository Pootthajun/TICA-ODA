﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="frmEditUser.aspx.vb" Inherits="ODA_VBPrototype.frmEditUser" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Add User Management | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
           <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
           <li class="treeview">
              <a href="frnFinance.aspx">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li ><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i> Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li><a href="frmSubExpense.aspx"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="treeview">
              <a href="frmAllocatedBudget.aspx">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i> Budget</a></li>
                <li><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                    <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i> Sub Budget</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmReport.aspx">
                <i class="fa fa-file-text-o"></i> <span>Report</span></a>
            </li> 

            <li class="active treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-users"></i> <span>Organize Structure</span>
              </a>
             
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li><a href="frmSector.aspx"><i class="fa fa-file"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-list"></i> Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-list"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-list"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
            User Management (จัดการข้อมูลผู้ใช้)
          </h4>
          <ol class="breadcrumb">
            <li><a href="frmDashborad.aspx"><i class="fa fa-area-chart"></i> Dashboard</a></li>
            <li><a href="frmStructure.aspx"> Organization Structure</a></li>
            <li class="active">Add User</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content"><br />
          <!-- START CUSTOM TABS -->
          <div class="row">
            <div class="col-md-12">
              <!-- Horizontal Form -->
              <div class="box">
                <div class="box-header with-border bg-gray">
                  <h4 class="box-title">Add User</h4>
                </div><!-- /.box-header -->
                <!-- form start -->
                <table class="table table-bordered">
                        <tr>
                           <td colspan="2"><h4 class="box-title"><a href="#">Country </a>><a href="#"> Thailand International Development Cooperation Agency </a>> สำนักผู้อำนวยการ</h4></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">ชื่อภาษาไทย :</b></th>
                          <td><div class="col-sm-10"><div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-user"></i>
                              </div>
                              <input class="form-control" id="Thai" placeholder="">
                            </div></div></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">English Name :</b></th>
                          <td><div class="col-sm-10"><div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-user"></i>
                              </div>
                              <input class="form-control" id="English" placeholder="">
                            </div></div></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Id Card/Passport :</b></th>
                          <td><div class="col-sm-10"><div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-credit-card"></i>
                              </div>
                              <input class="form-control" id="Passport" placeholder="">
                            </div></div></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Birthday (วันเกิด) :</b></th>
                          <td><div class="col-sm-10">
                              <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                             <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                           </div><!-- /.input group -->
                         </div></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Telephone (โทรศัพท์) :</b></th>
                          <td><div class="col-sm-10"><div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-phone"></i>
                              </div>
                              <input class="form-control" id="phone" placeholder="">
                            </div></div></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">E-mail :</b></th>
                          <td><div class="col-sm-10"><div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-envelope-o"></i>
                              </div>
                              <input class="form-control" id="mail" placeholder="">
                            </div></div></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Note (หมายเหตุ) :</b></th>
                          <td><div class="col-sm-10"><textarea class="form-control" rows="3" placeholder=""></textarea></div></td>
                        </tr>
                       <tr> 
                        <th style="width: 250px"><b class="pull-right">Active Status :</b></th>
                        <td><div class="col-sm-9">
                        <label><input type="checkbox" class="minimal" checked></label></div></td>
                        </tr>
                      </table>
                    <div class="box-footer">
                    <div class="col-sm-8"></div>
                    <div class="col-sm-2">
                      <a href="frmProjectExam.aspx" class="btn btn-block btn-social btn-success"><i class="fa fa-save"></i>Save </a>
                    </div>
                    <div class="col-sm-2">
                      <a href="frmProjectExam.aspx" class="btn btn-block btn-social btn-google"><i class="fa fa-reply"></i>Cancel </a>
                    </div>  
                    </div><!-- /.box-footer -->
                
              </div><!-- /.box -->
            </div><!-- /.col -->

          </div> <!-- /.row -->
          <!-- END CUSTOM TABS -->
          

        </section><!-- /.content -->
    </div>
   

<!----------------Page Advance---------------------->
        
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />

</asp:Content>