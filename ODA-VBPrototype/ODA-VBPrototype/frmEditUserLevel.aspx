﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="frmEditUserLevel.aspx.vb" Inherits="ODA_VBPrototype.frmEditUserLevel" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Add User Level | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-file-text-o"></i> Expense</a></li>
                <li><a href="#"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li><a href="#"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-file-text-o"></i> Allocated</a></li>
                <li><a href="#"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-list"></i> Allocated Group</a></li>
                    <li><a href="#"><i class="fa fa-list"></i> Sub Allocated</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmReport.aspx">
                <i class="fa fa-file-text-o"></i> <span>Report</span></a>
            </li> 

            <li class="treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-users"></i> <span>Organize Structure</span>
              </a>
             
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li><a href="frmSector.aspx"><i class="fa fa-file"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-list"></i> Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-list"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-list"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
            User Level (ระดับผู้ใช้งาน)
          </h4>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-area-chart"></i> Dashboard</a></li>
            <li><a href="#"> User Level</a></li>
            <li class="active">Add User Level</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content"><br />
          <!-- START CUSTOM TABS -->
          <div class="row">
            <div class="col-md-12">
              <!-- Horizontal Form -->
              <div class="box">
                <div class="box-header with-border bg-blue-gradient">
                  <h4 class="box-title">Add User Level</h4>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal"><br />
                 
                <div class="box-body">
                    <div class="col-md-2"></div>
                <div class="col-md-8">
                  <table class="table table-bordered table-hover table-striped">
                    <thead>
                      <tr class="bg-gray">
                        <td><b>Menu</b></td>
                        <td><b>Page</b></td>
                        <td style="width: 150px"><b>Update/Edit/Delete</b></td>
                        <td style="width: 150px"><b>View</b></td>
                        <td style="width: 150px"><b>N/A</b></td>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Setting</td>
                        <td>User Information</td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                      </tr>
                      <tr>
                        <td>Setting</td>
                        <td>Grop work</td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                      </tr>
                      <tr>
                        <td>Master</td>
                        <td>User Management</td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                      </tr>
                      <tr>
                        <td>Setting</td>
                        <td>User Level</td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                      </tr>
                      <tr>
                        <td>Master</td>
                        <td>Ministry</td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                      </tr>
                      <tr>
                        <td>Master</td>
                        <td>Department</td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                      </tr>
                      <tr>
                        <td>Setting</td>
                        <td>News Category</td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                      </tr>
                      <tr>
                        <td>Setting</td>
                        <td>News</td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                      </tr>
                      <tr>
                        <td>Master</td>
                        <td>Region (OECD)</td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                      </tr>
                      <tr>
                        <td>Master</td>
                        <td>OECD</td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                      </tr>
                      <tr>
                        <td>Master</td>
                        <td>Sector</td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                      </tr>
                      <tr>
                        <td>Master</td>
                        <td>Sub Sector</td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                      </tr>
                      <tr>
                        <td>Master</td>
                        <td>Funding Agency</td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                      </tr>
                      <tr>
                        <td>Master</td>
                        <td>Executing Agency</td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                      </tr>
                      <tr>
                        <td>Master</td>
                        <td>Multilateral</td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                      </tr>
                      <tr>
                        <td>Master</td>
                        <td>IN KIND</td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                      </tr>
                      <tr>
                        <td>Master</td>
                        <td>Project Status</td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                      </tr>
                      <tr>
                        <td>Setting</td>
                        <td>Disbursement Project</td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                      </tr>
                      <tr>
                        <td>Setting</td>
                        <td>Expenses</td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                      </tr>
                      <tr>
                        <td>Contact</td>
                        <td>Contact Admin</td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                      </tr>
                      <tr>
                        <td>Contact</td>
                        <td>ตอบคำถามที่หน่วยงานติดต่อ</td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                      </tr>
                      <tr>
                        <td>Finance</td>
                        <td>Finance</td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                      </tr>
                      <tr>
                        <td>Recipient</td>
                        <td>Recipient</td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                        <td><input type="checkbox" class="flat-red"></td>
                      </tr>
                      
                    </tbody>
                    
                  </table>
                </div>
                </div><!-- /.box-body -->
                
                <div class="col-md-2"></div>
                 <div class="box-footer">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-2">
                      <a href="frmProjectExam.aspx" class="btn btn-block btn-social btn-vk"><i class="fa fa-check"></i>Select All </a>
                    </div>
                    <div class="col-sm-2">
                      <a href="frmProjectExam.aspx" class="btn btn-block btn-social btn-vk"><i class="fa fa-remove"></i>UN Select All </a>
                    </div> 
                    <div class="col-sm-2">
                      <a href="frmProjectExam.aspx" class="btn btn-block btn-social btn-success"><i class="fa fa-save"></i>Save </a>
                    </div>
                    <div class="col-sm-2">
                      <a href="frmProjectExam.aspx" class="btn btn-block btn-social btn-google"><i class="fa fa-reply"></i>Cancel </a>
                    </div>  
                    </div><!-- /.box-footer -->
                </form>
                
                
              </div><!-- /.box -->

            </div><!-- /.col -->

          </div> <!-- /.row -->
          <!-- END CUSTOM TABS -->
          

        </section><!-- /.content -->
   </div>


<!----------------Page Advance---------------------->
        
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />  

</asp:Content>
