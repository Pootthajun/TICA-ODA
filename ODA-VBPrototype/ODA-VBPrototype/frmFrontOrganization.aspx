﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmFrontMaster.Master" CodeBehind="frmFrontOrganization.aspx.vb" Inherits="ODA_VBPrototype.frmFrontOrganization" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>โครงสร้างองค์กร | เกี่ยวกับกรมฯ</title>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
      <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <li><a href="frmFrontIndex.aspx">หน้าหลัก</a></li>
                            <li class="dropdown active">
                                <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="frmAbout.aspx">
                                    เกี่ยวกับกรมฯ
                                    <i class="icon-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="frmFrontTICAAbout.aspx">ประวัติกรมความร่วมมือระหว่างประเทศ</a></li>
                                    <li><a href="frmFrontPolicyPlan.aspx">วิสัยทัศน์ พันธกิจ สัญลักษณ์</a></li>
                                    <li class="active"><a href="frmFrontOrganization">โครงสร้างองค์กร</a></li>
                                    <li><a href="frmFrontPerformance.aspx">ผลการดำเนินงานของกรม</a></li>
                                </ul>
                            </li>
                            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="frmAbout.aspx">
                                    ศูนย์ข่าว
                                    <i class="icon-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="frmFrontNews.aspx">ข่าวทั่วไป</a></li>
                                    <li><a href="frmFrontNewsODA.aspx">ข่าวกรมฯ</a></li>
                            </ul></li>
                            <li><a href="frmFrontRecipient.aspx">ฟอร์มกรอกเอกสาร</a></li>  
                            <li><a href="frmFrontContact.aspx">ติดต่อกรมฯ</a></li>                        
                            <%--<li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">
                                    รายงาน
                                    <i class="icon-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="portfolio_4.html">Project</a></li>
                                    <li><a href="portfolio_3.html">Non-Project</a></li>
                                    <li><a href="portfolio_2.html">Loan</a></li>
                                    <li><a href="portfolio_item.html">Contribuition</a></li>
                                </ul>
                            </li>--%>
                          
                         </ul>    
                    </div>
                    <!-- BEGIN TOP NAVIGATION MENU -->
       
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
      
    <!-- BEGIN BREADCRUMBS -->   
    <div class="row-fluid breadcrumbs margin-bottom-40">
        <div class="container">
            <div class="span4">
                <h2>โครงสร้างองค์กร</h2>
            </div>
            <div class="span8">
                <ul class="pull-right breadcrumb">
                    <li><a href="index.html">Home</a> <span class="divider">/</span></li>
                    <li><a href="#">Pages</a> <span class="divider">/</span></li>
                    <li class="active">โครงสร้างองค์กร</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END BREADCRUMBS -->

    <!-- BEGIN CONTAINER -->   
    <div class="container min-hight">
        <!-- BEGIN ABOUT INFO -->   
        <div class="row-fluid margin-bottom-30">
            <!-- BEGIN INFO BLOCK -->               
            <div class="span7 space-mobile">
                <h4>ความเป็นมา</h4>
                <p>สพร. จัดตั้งขึ้นเมื่อวันที่  19 ตุลาคม 2547 ตามพระราชกฤษฎีกาจัดตั้งสำนักงานความร่วมมือเพื่อการพัฒนาระหว่างประเทศ พ.ศ. 2547 โดยเป็นส่วนราชการในกระทรวงการต่างประเทศ มีหน้าที่ความรับผิดชอบในการบริหารความร่วมมือเพื่อการพัฒนาในด้านเศรษฐกิจ สังคม วิชาการ กับประเทศเพื่อนบ้านและประเทศในภูมิภาคอื่นๆ ทั้งนี้ ความร่วมมือมีหลายรูปแบบ เช่น โครงการพัฒนา อาสาสมัคร และผู้เชี่ยวชาญ ทุนการศึกษา/ฝึกอบรม</p> 
                <p>Idest laborum et dolorum fuga. Et harum quidem rerum et quas molestias excepturi sint occaecati facilis est et expedita distinctio lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero consectetur adipiscing elit magna. Sed et quam lacus.</p>
                <!-- BEGIN LISTS -->
                <div class="row-fluid front-lists-v1">
                    <div class="span6">
                        <ul class="unstyled margin-bottom-20">
                            <li><i class="icon-ok"></i> Officia deserunt molliti</li>
                            <li><i class="icon-ok"></i> Consectetur adipiscing </li>
                            <li><i class="icon-ok"></i> Deserunt fpicia</li>
                        </ul>
                    </div>
                    <div class="span6">
                        <ul class="unstyled">
                            <li><i class="icon-ok"></i> Officia deserunt molliti</li>
                            <li><i class="icon-ok"></i> Consectetur adipiscing </li>
                            <li><i class="icon-ok"></i> Deserunt fpicia</li>
                        </ul>
                    </div>
                </div>
                <!-- END LISTS -->
            </div>
            <!-- END INFO BLOCK -->   

            <!-- BEGIN CAROUSEL -->            
            <div class="span5 front-carousel">
                <div id="myCarousel" class="carousel slide">
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        <div class="active item">
                            <img src="assets/img/pics/profile1.jpg" alt="">
                            <div class="carousel-caption">
                                <p>Excepturi sint occaecati cupiditate non provident</p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="assets/img/pics/profile2.jpg" alt="">
                            <div class="carousel-caption">
                                <p>Ducimus qui blanditiis praesentium voluptatum</p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="assets/img/pics/profile3.jpg" alt="">
                            <div class="carousel-caption">
                                <p>Ut non libero consectetur adipiscing elit magna</p>
                            </div>
                        </div>
                    </div>
                    <!-- Carousel nav -->
                    <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                        <i class="icon-angle-left"></i>
                    </a>
                    <a class="carousel-control right" href="#myCarousel" data-slide="next">
                        <i class="icon-angle-right"></i>
                    </a>
                </div>                
            </div>
            <!-- END CAROUSEL -->             
        </div>
        <!-- END ABOUT INFO -->   

    </div>
  

</asp:Content>

