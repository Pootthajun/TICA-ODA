﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmFrontMaster.Master" CodeBehind="frmFrontContact.aspx.vb" Inherits="ODA_VBPrototype.frmFrontContact" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>ประวัติกรมความร่วมมือระหว่างประเทศ | เกี่ยวกับกรมฯ</title>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
      <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <li><a href="frmFrontIndex.aspx">หน้าหลัก</a></li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="frmAbout.aspx">
                                    เกี่ยวกับกรมฯ
                                    <i class="icon-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="active"><a href="frmFrontTICAAbout.aspx">ประวัติกรมความร่วมมือระหว่างประเทศ</a></li>
                                    <li><a href="frmFrontPolicyPlan.aspx">วิสัยทัศน์ พันธกิจ สัญลักษณ์</a></li>
                                    <li><a href="frmFrontOrganization">โครงสร้างองค์กร</a></li>
                                    <li><a href="frmFrontPerformance.aspx">ผลการดำเนินงานของกรม</a></li>
                                </ul>
                            </li>
                           <li><a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="frmAbout.aspx">
                                    ศูนย์ข่าว
                                    <i class="icon-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="frmFrontNews.aspx">ข่าวทั่วไป</a></li>
                                    <li><a href="frmFrontNewsODA.aspx">ข่าวกรมฯ</a></li>
                            </ul></li>
                            <li><a href="frmFrontRecipient.aspx">ฟอร์มกรอกเอกสาร</a></li>  
<%--                            <li class="active"><a href="frmFrontContact.aspx">ติดต่อกรมฯ</a></li>--%> 
                            <li class="dropdown active">
                                <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="frmAbout.aspx">
                                    ติดต่อกรมฯ
                                    <i class="icon-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="frmLogin.aspx">สำหรับเจ้าหน้าที่</a></li>
                                    <li class="active"><a href="frmFrontContact.aspx">ติดต่อสอบถาม</a></li>
                            </ul></li>                       
                            <%--<li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">
                                    รายงาน
                                    <i class="icon-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="portfolio_4.html">Project</a></li>
                                    <li><a href="portfolio_3.html">Non-Project</a></li>
                                    <li><a href="portfolio_2.html">Loan</a></li>
                                    <li><a href="portfolio_item.html">Contribuition</a></li>
                                </ul>
                            </li>--%>
                          
                         </ul>    
                    </div>
                    <!-- BEGIN TOP NAVIGATION MENU -->
       
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
      
    <!-- BEGIN BREADCRUMBS -->   
    <div class="row-fluid breadcrumbs">
        <div class="container">
            <div class="span4">
                <h2>ติดต่อ-สอบถาม</h2>
            </div>
            <div class="span8">
                <ul class="pull-right breadcrumb">
                    <li><a href="frmFrontIndex.aspx"><i class="icon-home"></i></a> <span class="divider">/</span></li>
                    <li class="active">ติดต่อ-สอบถาม</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END BREADCRUMBS -->

 

    <!-- BEGIN CONTAINER -->   
    <div class="container min-hight">
        <div class="row-fluid">
            <div class="span9">
               <div class="space20"></div>
                <!-- BEGIN FORM-->
                <form action="#" class="horizontal-form margin-bottom-40">
                    <div class="control-group">
                        <label class="control-label">Name</label>
                        <div class="controls">
                            <input type="text" class="m-wrap span12" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" >Email <span class="color-red">*</span></label>
                        <div class="controls">
                            <input type="text" class="m-wrap span12" >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" >Message</label>
                        <div class="controls">
                            <textarea class="m-wrap span12" rows="8"></textarea>
                        </div>
                    </div>
                    <button type="submit" class="btn theme-btn"><i class="icon-ok"></i> Send</button>
                    <button type="button" class="btn red">Cancel</button>
                </form>
                <!-- END FORM-->                  
            </div>

            <div class="span3">
                <h2>Our Contacts</h2>
                <address>
                    <strong>Building B (South Zone),</strong><br>
                     8th Floor, <br>
                    Laksi District, BKK 10210<br>
                    <abbr title="Phone">P:</abbr>0-2203-5000 ext. 42011
                </address>
                <address>
                    <strong>Email</strong><br>
                    <a href="mailto:#">tica@mfa.go.th</a><br>
                    <a href="mailto:#">http://www.tica.thaigov.net </a>
                </address>
                <ul class="social-icons margin-bottom-10">
                    <li><a href="#" data-original-title="facebook" class="social_facebook"></a></li>
                    <li><a href="#" data-original-title="github" class="social_github"></a></li>
                    <li><a href="#" data-original-title="Goole Plus" class="social_googleplus"></a></li>
                    <li><a href="#" data-original-title="linkedin" class="social_linkedin"></a></li>
                    <li><a href="#" data-original-title="rss" class="social_rss"></a></li>
                </ul>

                <div class="clearfix margin-bottom-30"></div>

                <h2>About Us</h2>
                <p>Thailand International Development Cooperation Agency (TICA)</p>
                <ul class="unstyled">
                    <li><i class="icon-ok"></i> Officia deserunt molliti</li>
                    <li><i class="icon-ok"></i> Consectetur adipiscing </li>
                    <li><i class="icon-ok"></i> Deserunt fpicia</li>
                </ul>                                
            </div>            
        </div>
    </div>
    <!-- END CONTAINER -->
  

</asp:Content>
