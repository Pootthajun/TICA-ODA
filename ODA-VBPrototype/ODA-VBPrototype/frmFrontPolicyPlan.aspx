﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmFrontMaster.Master" CodeBehind="frmFrontPolicyPlan.aspx.vb" Inherits="ODA_VBPrototype.frmFrontPolicyPlan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>วิสัยทัศน์ พันธกิจ สัญลักษณ์ | เกี่ยวกับกรมฯ</title>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
      <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <li><a href="frmFrontIndex.aspx">หน้าหลัก</a></li>
                            <li class="dropdown active">
                                <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="frmAbout.aspx">
                                    เกี่ยวกับกรมฯ
                                    <i class="icon-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="frmFrontTICAAbout.aspx">ประวัติกรมความร่วมมือระหว่างประเทศ</a></li>
                                    <li class="active"><a href="frmFrontPolicyPlan.aspx">วิสัยทัศน์ พันธกิจ สัญลักษณ์</a></li>
                                    <li><a href="frmFrontOrganization">โครงสร้างองค์กร</a></li>
                                    <li><a href="frmFrontPerformance.aspx">ผลการดำเนินงานของกรม</a></li>
                                </ul>
                            </li>
                            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="frmAbout.aspx">
                                    ศูนย์ข่าว
                                    <i class="icon-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="frmFrontNews.aspx">ข่าวทั่วไป</a></li>
                                    <li><a href="frmFrontNewsODA.aspx">ข่าวกรมฯ</a></li>
                            </ul></li>
                            <li><a href="frmFrontRecipient.aspx">ฟอร์มกรอกเอกสาร</a></li>  
                            <li><a href="frmFrontContact.aspx">ติดต่อกรมฯ</a></li>                        
                            <%--<li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">
                                    รายงาน
                                    <i class="icon-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="portfolio_4.html">Project</a></li>
                                    <li><a href="portfolio_3.html">Non-Project</a></li>
                                    <li><a href="portfolio_2.html">Loan</a></li>
                                    <li><a href="portfolio_item.html">Contribuition</a></li>
                                </ul>
                            </li>--%>
                          
                         </ul>    
                    </div>
                    <!-- BEGIN TOP NAVIGATION MENU -->
       
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
      
    <!-- BEGIN BREADCRUMBS -->   
    <div class="row-fluid breadcrumbs margin-bottom-40">
        <div class="container">
            <div class="span4">
                <h2>วิสัยทัศน์ พันธกิจ</h2>
            </div>
            <div class="span8">
                 <ul class="pull-right breadcrumb">
                    <li><a href="frmFrontIndex.aspx"><i class="icon-home"></i></a> <span class="divider">/</span></li>
                    <li class="active">วิสัยทัศน์ พันธกิจ</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END BREADCRUMBS -->

    <!-- BEGIN CONTAINER -->   
    <div class="container min-hight">
        <!-- BEGIN ABOUT INFO -->   
        <div class="row-fluid margin-bottom-30">
           <div class="span2"></div>
            <!-- BEGIN CAROUSEL -->            
            <div class="span8 front-carousel">
                <div id="myCarousel" class="carousel slide">
                    <!-- Carousel items -->
                        <div class="active item">
                            <img src="assets/img/pics/วิสัยทัศน์.png" alt="">
                            <div class="carousel-caption">
                                <p>Excepturi sint occaecati cupiditate non provident</p>
                            </div>
                        </div>
                </div>                
            </div>
            <div class="span2"></div>
            <!-- END CAROUSEL -->             
        </div>
        <!-- END ABOUT INFO -->   

    </div>
  

</asp:Content>