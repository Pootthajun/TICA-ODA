﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="frmDetailActivityNon1.aspx.vb" Inherits="ODA_VBPrototype.frmDetailActivityNon1" %>
<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<title>Add Activity | ODA</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
           <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
            
           <li class="treeview">
              <a href="frnFinance.aspx">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li ><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i> Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li><a href="frmSubExpense.aspx"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="treeview">
              <a href="frmAllocatedBudget.aspx">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i> Budget</a></li>
                <li><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                    <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i> Sub Budget</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmReport.aspx">
                <i class="fa fa-file-text-o"></i> <span>Report</span></a>
            </li> 

            <li class="treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-sitemap"></i> <span>Organize Structure</span>
              </a>
             
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li><a href="frmSector.aspx"><i class="fa fa-file-o"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file-o"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-align-justify"></i> Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-align-justify"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-align-justify"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">

  <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4><b>
              Sufficiency Economy in Agriculture 2013
          </b></h4>
          <ol class="breadcrumb">
            <li><a href="frmDetailNonProject1.aspx"><i class="fa fa-sign-out"></i> Back</a></li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-body">
                      <table class="table table-bordered">
                        <tr class="bg-info">
                          <th style="width: 150px" colspan="2">
                          <h5><b class="text-blue">Detail (รายละเอียด) </b><b class="text-orange pull-right">CODE ID : 201500020</b></h5></th>
                        </tr>
                        <tr>
                          <th style="width: 280px"><b class="pull-right">Course (หลักสูตร) :</b></th>
                          <td class="text-primary">
                            <textarea class="form-control" rows="3" placeholder="แผนงานที่ 1  จัดฝึกอบรมหลักสูตรเฉพาะทางระยะสั้น จำนวน 17 คน เกี่ยวกับ  Sufficiency Economy in Agriculture" disabled></textarea></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Description (รายละเอียด) :</b></th>
                          <td class="text-primary"><textarea class="form-control" rows="3" placeholder="-" disabled></textarea></td>
                        </tr>
                         
                        <tr>
                          <th style="width: 280px"><b class="pull-right">Sector (สาขา) :</b></th>
                          <td class="text-primary">
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">AGRICULTURE (311)</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select></td>
                        </tr>
                        
                        <tr>
                          <th style="width: 280px"><b class="pull-right">Sub Sector (สาขาย่อย) :</b></th>
                          <td class="text-primary">
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">Agricultural policy and administrative management 31110</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select></td>
                        </tr>
                        <tr>
                          <th style="width: 280px"><b class="pull-right">Plan (Start/End Date) </b><br /><b class="pull-right">(วางแผนวันเริ่มต้น/สิ้นสุด) :</b></th>
                          <td class="text-primary">
                            <div class="col-sm-6">
                                <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                                <input type="text" class="form-control pull-right" id="reservation" disabled>
                                 </div><!-- /.input group -->
                              </div></td>
                        </tr>
                        
                        <tr>
                          <th style="width: 280px"><b class="pull-right">Actual (Start/End Date) </b><br /><b class="pull-right">(วันเริ่มต้น/สิ้นสุดจริง) :</b></th>
                          <td class="text-primary">
                            <div class="col-sm-6">
                                <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                                <input type="text" class="form-control pull-right" id="reservation1" disabled>
                                 </div><!-- /.input group -->
                              </div></td>
                        </tr>
                        <tr>
                          <th style="width: 280px"><b class="pull-right">Commitment Budget </b><br /><b class="pull-right">(จัดสรรงบประมาณ) :</b></th>
                          <td class="text-success">
                             <div class="col-sm-6">
                               <div class="input-group">
                                 <div class="input-group-addon">
                                 <i class="fa fa-dollar"></i>
                                 </div>
                                <input type="text" class="form-control pull-right" id="Budget" placeholder="2,437,602 บาท" disabled >
                               </div><!-- /.input group -->
                              </div></td>
                        </tr>
                        <tr>
                          <th style="width: 280px"><b class="pull-right">Disbursement (จ่ายจริง) :</b></th>
                          <td class="text-success">
                             <div class="col-sm-6">
                               <div class="input-group">
                                 <div class="input-group-addon">
                                 <i class="fa fa-dollar"></i>
                                 </div>
                                <input type="text" class="form-control pull-right" id="Disbursement" placeholder="500,000,000 บาท" disabled >
                               </div><!-- /.input group -->
                              </div></td>
                        </tr>
                        <tr>
                          <th style="width: 280px"><b class="pull-right">Administrative (ค่าบริหารจัดการ):</b></th>
                          <td class="text-success">
                             <div class="col-sm-6">
                               <div class="input-group">
                                 <div class="input-group-addon">
                                 <i class="fa fa-dollar"></i>
                                 </div>
                                <input type="text" class="form-control pull-right" id="Administrative" placeholder="0 บาท" disabled >
                               </div><!-- /.input group -->
                              </div></td>
                        </tr>
                        
                        <tr>
                          <th style="width: 280px"><b class="pull-right">In kind (ความช่วยเหลือด้านอื่นๆ):</b></th>
                          <td class="text-primary">
                              <table class="table table-bordered">
                               <tr>
                                <th style="width: 300px" class="bg-gray"><b>In kind (ความช่วยเหลือด้านอื่นๆ)</b></th>
                                <th style="width: 250px" class="bg-gray" colspan="2"><b>Estimated (โดยประมาณ)</b></th>
                            </tr>
                            <tr>
                              <td><select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">Select</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select></td>
                              <td class="text-success">
                                   <div class="input-group">
                                     <div class="input-group-addon">
                                     <i class="fa fa-dollar"></i>
                                     </div>
                                    <input type="text" class="form-control pull-right" id="Estimated" placeholder="500,000,000 บาท" disabled >
                                   </div><!-- /.input group --></td>
                              <td style="width: 50px"><a class="btn btn-social-icon btn-google btn-sm" data-toggle="tooltip" title="ลบ"><i class="fa fa-trash"></i></a></td>
                            </tr>
                              <tr>
                                  <td class="pull-right"><b>รวมทั้งสิ้น</b></td>
                                <td>500,000,000 บาท</td>    
                                <td><a class="btn btn-social-icon btn-twitter btn-sm" data-toggle="tooltip" title="เพิ่มประภทความช่วยเหลืออื่นๆ"><i class="fa fa-plus"></i></a></td>
                                
                             </tr>
                         </table>
                        </td>
                        </tr>
                        <tr><td></td>
                            <td>
                                <div class="col-sm-4"><a href="frmRecipentTotalNonProject.aspx" target="_blank" class="btn btn-block btn-social btn-dropbox">
                                  <i class="fa fa-user"></i>Recipient (ผู้รับทุน) </a></div>
                               
                                <div class="col-sm-4"><a href="frmDetaiAllocated1.aspx" target="_blank" class="btn btn-block btn-social btn-dropbox">
                                 <i class="fa fa-dollar"></i>Allocated (จัดสรรงบประมาณ) </a></div>
                            </td>
                        </tr>
                        <tr>
                          <th style="width: 280px"><b class="pull-right">Recipient Country</b><br /><b class="pull-right"> (ประเทศที่รับทุน) :</b></th>
                          <td class="text-primary">
                              <div class="col-sm-12">
                                 <select class="form-control select2" multiple="multiple" data-placeholder="Select Benificiary" style="width: 100%;">
                                  <option>Thailand</option>
                                  <option>Lao</option>
                                  <option>Malaysia</option>
                                  <option></option>
                                  <option>Vietnam</option>
                                  <option>Singapore</option>
                                  <option>Chinese</option>
                                </select>
                             </div></td>
                        </tr>
                        <tr>
                          <th style="width: 280px"><b class="pull-right">Benificiary (ผู้รับผลประโยชน์) :</b></th>
                          <td class="text-primary">
                              <div class="col-sm-12">
                                <input type="text" class="form-control pull-right" id="Benificiary" placeholder="0 บาท" disabled >
                             </div></td>
                        </tr>
                        <tr>
                        <th style="width: 280px"><b class="pull-right" data-toggle="tooltip" title="เปิดรับสมัครผู้รับทุน">ประกาศรับสมัครผู้รับทุน :</b></th>
                        <td><div class="form-group">
                          <div class="col-sm-9">
                         <label><input type="checkbox" class="minimal" checked></label>
                          </div>
                        </div></td>
                        </tr>
                      
                      </table>
                  <div class="box-footer">
                        <div class="col-sm-8"></div>
                        <div class="col-sm-2">
                          <a href="#" class="btn btn-block btn-social btn-success"><i class="fa fa-save"></i>Save </a>
                        </div>
                        <div class="col-sm-2">
                          <a href="#" class="btn btn-block btn-social btn-google"><i class="fa fa-reply"></i>Cancel </a>
                        </div>
                        
                    </div>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
</div>
    
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />
</asp:Content>
