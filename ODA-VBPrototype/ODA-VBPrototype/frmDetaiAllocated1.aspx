﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="frmDetaiAllocated1.aspx.vb" Inherits="ODA_VBPrototype.frmDetaiAllocated1" %>
<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Allocated Budget | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
            <li class="treeview">
              <a href="frmFinance.aspx">
                <i class="fa fa-calculator"></i> <span>Finance</span></a>
            </li> 
            <li class="treeview">
              <a href="frnFinance.aspx">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li ><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i> Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li><a href="frmSubExpense.aspx"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="treeview">
              <a href="frmAllocatedBudget.aspx">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i> Budget</a></li>
                <li><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                    <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i> Sub Budget</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-sitemap"></i> <span>Organize Structure</span>
              </a>
             
            </li>
             <li>
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li><a href="frmSector.aspx"><i class="fa fa-file"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-list"></i> Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-list"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-list"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
 
     <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
           CODE ID 201400126 : โครงการพัฒนาห้องปฏิบัติการวิจัยโรคปลา ประจำปี 2555   
          </h4>
          <ol class="breadcrumb">
            <li><a href="frmDashborad.aspx"><i class="fa fa-area-chart"></i> Dashboard</a></li>
            <li class="active">Allocated Budget</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
                  <table class="table">
                    <tr>
                        <th colspan="2" class="bg-danger">การจ่ายเงิน</th>
                    </tr>
                    <tr>
                      <th class="bg-gray" style="width: 150px">ชื่อโครงการ :</th>
                      <td>โครงการพัฒนาวิทยาลัยศิลปศึกษา สปป.ลาว ประจำปี 2556</td>
                    </tr>
                    <tr>
                      <th class="bg-gray" style="width: 150px">ระยะเวลา :</th>
                      <td>จากวันที่ 24/03/2556 ถึงวันที่ 31/05/2558</td>
                    </tr>
                    <tr>
                      <th class="bg-gray" style="width: 150px">Component :</th>
                      <td>Master</td>
                    </tr>
                    <tr>
                      <th class="bg-gray" style="width: 150px">ลักษณะการจ่ายเงิน :</th>
                      <td>รายบุคคล</td>
                    </tr>
                  </table>
                </div><!-- /.box-body -->
                
               <div class="box-body">
                <div style="overflow-x:auto;">
                  <table class="table table-bordered">
                    <tr>
                        <th colspan="16" class="bg-danger">รายละเอียดที่จัดสรร</th>
                    </tr>
                    <tr class="bg-gray">
                      <th style="width: 10px">ลำดับ</th>
                      <th style="width: 300px">วันที่</th>
                      <th style="width: 200px">รายการ</th>
                      <th>ยอดรวม</th>
                      <th>ค่ากินอยู่	</th>
                      <th style="width: 200px">ค่าโดยสารเครื่องบิน</th>
                      <th style="width: 200px">ค่า ธ.ตรวจลงตรา/ขยายVISA</th>
                      <th style="width: 200px">ค่าหนังสือ/อุปกรณ์</th>
                      <th style="width: 200px">ค่าธรรมเนียมการศึกษา</th>
                      <th style="width: 200px">ค่าพาหนะต่างชาติ</th>
                      <th style="width: 300px">ค่าเบี้ยประกันชีวิตและสุขภาพ</th>
                      <th>หมายเหตุ</th>
                      <th style="width: 200px">ปีงบประมาณ</th>
                      <th>ประเทศ</th>
                      <th style="width: 300px">เจ้าหน้าที่</th>
                      <th>แก้ไขล่าสุด</th>
                    </tr>
                    <tr>
                      <td>1.</td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">13-03-2556</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">งปม.</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">72,980.00</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">37,612.00</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">2,000.00</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">29,000.00</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">200.00</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">4,168.00</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">พบ. 587</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">2556</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Laos</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">จริยา กิตชัย อิสิสิงห์</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">05-08-2558 14:59:24</a></td>
                    </tr>
                    <tr>
                      <td>2.</td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">21-05-2556</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">งปม.</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">61,000.00</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">56,000.00</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">5,000.00</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">พบ. 587</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">2556</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Laos</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">จริยา กิตชัย อิสิสิงห์</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">05-08-2558 15:00:49</a></td>
                    </tr>
                    <tr>
                      <td>3.</td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">21-05-2556</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">งปม.</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">4,500.00</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">3,500.00</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">1,000.00</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">2556</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">พบ. 869</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">2556</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Laos</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">จริยา กิตชัย อิสิสิงห์</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">5-08-2558 15:01:58</a></td>
                    </tr>
                    <tr class="bg-warning">
                      <td></td>
                      <td><a class="text-primary"><b>รวม</b></a></td>
                      <td><a class="text-primary"><b>296,280.00</b></a></td>
                      <td><a class="text-primary"><b>205,612.00</b></a></td>
                      <td><a class="text-primary"><b>3,500.00</b></a></td>
                      <td><a class="text-primary"><b>2,000.00</b></a></td>
                      <td><a class="text-primary"><b>10,000.00</b></a></td>
                      <td><a class="text-primary"><b>69,800.00</b></a></td>
                      <td><a class="text-primary"><b>1,200.00</b></a></td>
                      <td><a class="text-primary"><b>4,168.00</b></a></td>
                      <td><a class="text-primary"><b>2556</b></a></td>
                      <td><a class="text-primary"><b>พบ. 869</b></a></td>
                      <td><a class="text-primary"><b>2556</b></a></td>
                      <td><a class="text-primary"><b>Laos</b></a></td>
                      <td><a class="text-primary"><b>จริยา กิตชัย อิสิสิงห์</b></a></td>
                      <td><a class="text-primary"><b>5-08-2558 15:01:58</b></a></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">02-04-2556</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">ค่าใช้จ่าย</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">(39.00)</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">(39.00)</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">พบ. 645</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">2556</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Laos</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">จริยา กิตชัย อิสิสิงห์</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">05-08-2558 15:17:04</a></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">02-04-2556</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">ค่าใช้จ่าย</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">(39.00)</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">(39.00)</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">พบ. 645</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">2556</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Laos</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">จริยา กิตชัย อิสิสิงห์</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">05-08-2558 15:17:04</a></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">02-04-2556</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">ค่าใช้จ่าย</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">(39.00)</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">(39.00)</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"></a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">พบ. 645</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">2556</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Laos</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">จริยา กิตชัย อิสิสิงห์</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">05-08-2558 15:17:04</a></td>
                    </tr>
                    <tr class="bg-info">
                      <td></td>
                      <td><a class="text-primary"><b>รวม</b></a></td>
                      <td><a class="text-primary"><b>296,280.00</b></a></td>
                      <td><a class="text-primary"><b>205,612.00</b></a></td>
                      <td><a class="text-primary"><b>3,500.00</b></a></td>
                      <td><a class="text-primary"><b>2,000.00</b></a></td>
                      <td><a class="text-primary"><b>10,000.00</b></a></td>
                      <td><a class="text-primary"><b>69,800.00</b></a></td>
                      <td><a class="text-primary"><b>1,200.00</b></a></td>
                      <td><a class="text-primary"><b>4,168.00</b></a></td>
                      <td><a class="text-primary"><b>2556</b></a></td>
                      <td><a class="text-primary"><b>พบ. 869</b></a></td>
                      <td><a class="text-primary"><b>2556</b></a></td>
                      <td><a class="text-primary"><b>Laos</b></a></td>
                      <td><a class="text-primary"><b>จริยา กิตชัย อิสิสิงห์</b></a></td>
                      <td><a class="text-primary"><b>5-08-2558 15:01:58</b></a></td>
                    </tr>
                    <tr class="bg-info">
                      <td></td>
                      <td><a class="text-primary"><b>รวมค่าใช้จ่าย</b></a></td>
                      <td><a class="text-primary"><b>296,280.00</b></a></td>
                      <td><a class="text-primary"><b></b></a></td>
                      <td><a class="text-primary"><b></b></a></td>
                      <td><a class="text-primary"><b></b></a></td>
                      <td><a class="text-primary"><b></b></a></td>
                      <td><a class="text-primary"><b></b></a></td>
                      <td><a class="text-primary"><b></b></a></td>
                      <td><a class="text-primary"><b></b></a></td>
                      <td><a class="text-primary"><b></b></a></td>
                      <td><a class="text-primary"><b></b></a></td>
                      <td><a class="text-primary"><b></b></a></td>
                      <td><a class="text-primary"><b></b></a></td>
                      <td><a class="text-primary"><b></b></a></td>
                      <td><a class="text-primary"><b></b></a></td>
                    </tr>
                  </table>
                </div><!-- /.box -->
              </div>
              </div>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
   </div>
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />
</asp:Content>
