﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="frmLoan.aspx.vb" Inherits="ODA_VBPrototype.frmLoan" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<title>Loan | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
            
           <li class="treeview">
              <a href="frnFinance.aspx">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li ><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i> Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li><a href="frmSubExpense.aspx"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="treeview">
              <a href="frmAllocatedBudget.aspx">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i> Budget</a></li>
                <li><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                    <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i> Sub Budget</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmReport.aspx">
                <i class="fa fa-file-text-o"></i> <span>Report</span></a>
            </li> 

            <li class="treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-sitemap"></i> <span>Organize Structure</span>
              </a>
             
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li><a href="frmSector.aspx"><i class="fa fa-file"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-list"></i> Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-list"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-list"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4><b>
           Loan
          </b></h4>
          <ol class="breadcrumb">
            <li><a href="frmDashborad.aspx"><i class="fa fa-area-chart"></i> Dashborad</a></li>
            <li><a href="frmProjectStatus.aspx">Project Status</a></li>
            <li class="active">Loan</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
                <div class="box">
                        <div class="box-header with-border">
                            <div class="col-lg-12">
                                <p><a href="#">
                                <button class="btn bg-orange margin-r-5 btn-social"><i class="fa fa-plus"></i>Add Loan</button></a></p>
                            </div>
                                
                             <div class="col-lg-5"><br />
                                <h5 class="text-primary"><b>พบทั้งหมด 4 รายการ</b></h5>
                             </div>

                            <div class="col-lg-5">
                                <div class="input-group margin">
                                <input type="text" class="form-control">
                                <span class="input-group-btn">
                                  <button class="btn btn-info btn-flat" type="button"><i class="fa fa-search" data-toggle="tooltip" title="ค้นหา"></i></button>
                                </span>
                              </div><!-- /input-group -->
                           </div>
                           <div class="col-lg-2">
                                <div class="input-group margin">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collap1">
                                    <button type="button" class="btn btn-success btn-flat" data-toggle="tooltip" title="ค้นหาเพิ่มเติม">
                                    <i class="fa fa-magic"></i>
                                    </button></a>
                                    <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                        <button type="button" class="btn btn-success btn-flat dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-print"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="#"><i class="fa fa-file-pdf-o text-red"></i>PDF</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#"><i class="fa fa-file-excel-o text-green"></i>EXCEL</a></li>
                                        </ul>
                                    </div>
                                </div>
                              </div>
                            </div>
                            
                        <!-- /.box-header -->
                        <div class="clearfix"></div>
                       <div id="collap1" class="panel-collapse collapse">
                           
                        <div class="box-header with-border">
                            <div class="box-body">
                                <!-- form start -->
                                <form class="form-horizontal">
                                  <div class="box-body">
                                    <div class="form-group">
                                      <label for="Sector" class="col-sm-2 control-label">Project Title :</label>
                                      <div class="col-sm-4">
                                        <input class="form-control" id="Project" placeholder="">
                                      </div>

                                      
                                      <label for="Sector" class="col-sm-2 control-label">Start/End Date :</label>
                                      <div class="col-sm-4">
                                        <div class="input-group">
                                          <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                           </div>
                                         <input type="text" class="form-control pull-right" id="reservation">
                                        </div><!-- /.input group -->
                                      </div>
                                    </div>
                                   
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                      <label for="Sector" class="col-sm-2 control-label">Course (หลักสูตร) :</label>
                                      <div class="col-sm-4">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Select</option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                        </select>
                                      </div>
                                      
                                      <label for="Sector" class="col-sm-2 control-label">Recipient Country :</label>
                                      <div class="col-sm-4">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Select</option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                        </select>
                                      </div>
                                    </div>
                                  
                                    
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                      <label for="inputname" class="col-sm-2 control-label">Sector :</label>
                                      <div class="col-sm-4">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Select</option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                        </select>
                                      </div>

                                       <label for="inputname" class="col-sm-2 control-label">Sub Sector :</label>
                                      <div class="col-sm-4">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Select</option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                        </select>
                                      </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                      <label for="inputname" class="col-sm-2 control-label">Project Component :</label>
                                      <div class="col-sm-4">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Select</option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                        </select>
                                      </div>

                                       <label for="inputname" class="col-sm-2 control-label">Funding Agency :</label>
                                      <div class="col-sm-4">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Select</option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                        </select>
                                      </div>
                                    </div>

                                  </div><!-- /.box-body -->
                                 
                                </form>
                               
                                <!-- /.row -->
                            </div>
                            <!-- /.box-body -->
                           
                        </div>
                        </div>
                 <div class="box-body">
                  <table class="table table-bordered">
                    <tr class="bg-gray">
                      <th style="width: 20px" class="text-center">No. (ลำดับ)</th>
                      <th style="width: 30px" class="text-center">ID Loan</th>
                      <th style="width: 200px" class="text-center">Description (รายละเอียด)</th>
                      <th style="width: 80px" class="text-center">Tool (เครื่องมือ)</th>
                    </tr>
                    <tr>
                      <td class="text-center"><p>1</p></td>
                      <td class="text-center">201500243</td>
                      <td><p>โครงการปรับปรุงและก่อสร้างถนนจากบ้านฮวก (พะเยา) -เมืองคอน-เมืองเชียงฮ่อน-และเมืองคอบ-เมืองปากคอบ-บ้านก้อตื้น สปป.ลาว (มูลค่าเงินกู้ 191.19 ล้านบาทและ เงินให้เปล่า 47.03 ล้านบาท) </p>
                          <a class="text-blue"><b>Start/End Date :</b>	14 กันยายน 2557 - 31 30 กันยายน 2559</a>
                          <a class="text-yellow"><b>Allocated Budget :</b> 237,218,076.28 บาท</a>
                      </td>
                      <td class="text-center"><div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-navicon text-green"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                              <li><a href="frmDetailLoan1.aspx" target="_blank"><i class="fa fa-search text-blue"></i>  ดูรายละเอียด</a></li>
                              <li><a href="frmDetailLoan1.aspx" target="_blank"><i class="fa fa-pencil text-blue"></i>  แก้ไข</a></li>
                              <li class="divider"></li>
                              <li><a href="#"><i class="fa fa-remove text-red"></i>  ลบ</a></li>
                            </ul>
                          </div></td>
                    </tr>
                    <tr>
                      <td class="text-center"><p>2</p></td>
                      <td class="text-center">201400671</td>
                      <td><p>การศึกษาความเหมาะสมและออกแบบการก่อสร้างสายส่งไฟฟ้า 115 kV และสถานีไฟฟ้าช่วงน้ำทง- ห้วยทราย</p>
                          <a class="text-blue"><b>Start/End Date :</b>	10 มิถุนายน 2556 - 4 กุมภาพันธ์ 2557</a>
                          <a class="text-yellow"><b>Allocated Budget :</b> 15,000,000.00 บาท</a>
                      </td>
                      <td class="text-center"><div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-navicon text-green"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                              <li><a href="frmDetailLoan2.aspx" target="_blank"><i class="fa fa-search text-blue"></i>  ดูรายละเอียด</a></li>
                              <li><a href="frmDetailLoan2.aspx" target="_blank"><i class="fa fa-pencil text-blue"></i>  แก้ไข</a></li>
                              <li class="divider"></li>
                              <li><a href="#"><i class="fa fa-remove text-red"></i>  ลบ</a></li>
                            </ul>
                          </div></td>
                    </tr>
                    <tr>
                      <td class="text-center"><p>3</p></td>
                      <td class="text-center">201400665</td>
                      <td><p>โครงการปรับปรุงถนนในนครหลวงเวียงจันทน์สำหรับเป็นเจ้าภาพ การประชุมสุดยอดผู้น้ำเอเซีย-ยุโรป (ASEM Summit) ครั้งที่ 9</p>
                          <a class="text-blue"><b>Start/End Date :</b>	6 มิถุนายน 2555 - 31 มีนาคม 2556</a>
                          <a class="text-yellow"><b>Allocated Budget :</b> 190,700,000.00 บาท</a>
                      </td>
                      <td class="text-center"><div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-navicon text-green"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                              <li><a href="#"><i class="fa fa-search text-blue"></i>  ดูรายละเอียด</a></li>
                              <li><a href="#"><i class="fa fa-pencil text-blue"></i>  แก้ไข</a></li>
                              <li class="divider"></li>
                              <li><a href="#"><i class="fa fa-remove text-red"></i>  ลบ</a></li>
                            </ul>
                          </div></td>
                    </tr>
                    <tr>
                    <td class="text-center"><p>4</p></td>
                      <td class="text-center">20140063</td>
                      <td><p>โครงการปรับปรุงระบบระบายน้ำในนครหลวงเวียงจันทน์</p>
                          <a class="text-blue"><b>Start/End Date :</b>	5 ธันวาคม 2556 - 30 มิถุนายน 2557</a>
                          <a class="text-yellow"><b>Allocated Budget :</b> 95,400,000.00 บาท</a>
                      </td>
                      <td class="text-center"><div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-navicon text-green"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                              <li><a href="#"><i class="fa fa-search text-blue"></i>  ดูรายละเอียด</a></li>
                              <li><a href="#"><i class="fa fa-pencil text-blue"></i>  แก้ไข</a></li>
                              <li class="divider"></li>
                              <li><a href="#"><i class="fa fa-remove text-red"></i>  ลบ</a></li>
                            </ul>
                          </div></td>
                    </tr>
                     </table> 
                   <br /><br />

                    </div><!-- /.box-body -->
                 </div><!-- /.col -->
              </div>
	
              </div><!-- /.row -->
        </section></div>
</asp:Content>