﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="frmDetailFinanceProject.aspx.vb" Inherits="ODA_VBPrototype.frmDetailFinanceProject" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<title>Project | Finance</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
           <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
            
           <li class="active treeview">
              <a href="#">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                 <li class="active"><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i> Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li><a href="frmSubExpense.aspx"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="treeview">
              <a href="frmAllocatedBudget.aspx">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i> Budget</a></li>
                <li><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                    <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i> Sub Budget</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmReport.aspx">
                <i class="fa fa-file-text-o"></i> <span>Report</span></a>
            </li> 

            <li class="treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-sitemap"></i> <span>Organize Structure</span>
              </a>
             
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li class="active"><a href="frmSector.aspx"><i class="fa fa-file"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-list"></i> Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-list"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-list"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
   
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
           <a class="text-yellow">CODE ID 201400126 :</a><a class="text-blue"> โครงการพัฒนาห้องปฏิบัติการวิจัยโรคปลา ประจำปี 2555</a>
          </h4>
          <ol class="breadcrumb">
            <li><a href="frmDashborad.aspx"><i class="fa fa-calculator"></i>Finance</a></li>
            <li class="active">Detail Project</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                 <div class="box-header with-border">
                     
                           <div class="col-lg-2"> 
                                <div class="input-group margin">
                                <select class="form-control select2" style="width: 100%;">
                                  <option>Select Form</option>
                                  <option selected="selected">แบบฟอร์มที่1</option>
                                  <option>แบบฟอร์มที่2</option>
                                </select>
                              </div><!-- /input-group -->
                         </div>
                       <div class="col-lg-3"></div>
                           <div class="col-lg-3"> 
                             <div class="input-group margin">
                                <select class="form-control select2" style="width: 100%;">
                                  <option selected="selected">Select Month</option>
                                  <option>มกราคม</option>
                                  <option>กุมภาพันธ์</option>
                                  <option>มีนาคม</option>
                                  <option>เมษายน</option>
                                  <option>พฤษภาคม</option>
                                  <option>มิถุนายน</option>
                                  <option>กรกฎาคม</option>
                                  <option>สิงหาคม</option>
                                  <option>กันยายน</option>
                                  <option>ตุลาคม</option>
                                  <option>พฤศจิกายน</option>
                                  <option>ธันวาคม</option>
                                </select>
                              </div>   <!-- /input-group -->
                         </div>
                           <div class="col-lg-3"> 
                                <div class="input-group margin">
                                <select class="form-control select2" style="width: 100%;">
                                  <option selected="selected">Seclect Year</option>
                                  <option>2560</option>
                                  <option>2559</option>
                                  <option>2558</option>
                                  <option>2557</option>
                                  <option>2556</option>
                                  <option>2555</option>
                                  <option>2554</option>
                                  <option>2553</option>
                                  <option>2552</option>
                                  <option>2551</option>
                                  <option>2550</option>
                                  <option>2549</option>
                                </select>
                              </div><!-- /input-group -->
                         </div>
                          <div class="col-lg-1">
                                <div class="input-group margin pull-right">
                                    <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                        <button type="button" class="btn btn-success btn-flat dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-print"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="#"><i class="fa fa-file-pdf-o text-red"></i>PDF</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#"><i class="fa fa-file-excel-o text-green"></i>EXCEL</a></li>
                                        </ul>
                                    </div>
                                </div>
                              </div>
                  </div>

             
                    
                  <%--<table class="table table-bordered">
                      <tr class="bg-info">
                        <th><b>แผนงานที่ 1</b> แผนงานพัฒนาบุคลากรของวิทยาลัย</th>
                      </tr>
                      <tr class="bg-info">
                        <td><a data-toggle="collapse" data-parent="#accordion" href="#collap11">- 1.1 ทุนดูงานสำหรับผู้บริหาร 7 คน ณ ปทท.</a></td>
                      </tr>
                      <tr class="bg-info">
                        <td><a data-toggle="collapse" data-parent="#accordion" href="#collap12">- 1.2 ทุนศึกษา ป.ตรี 2 ทุน ป.โท 2 ทุน/ปี รวม 4 ทุน</a></td>
                      </tr>
                      <tr class="bg-info">
                        <td><a data-toggle="collapse" data-parent="#accordion" href="#collap13">- 1.3 ทุนฝึกอบรมหลักสูตรเฉพาะทางหลักสูตรสำหรับครูด้านศิลปดนตรี (ตัวโน้ต (ซอลแฟร์ กลอง แคน ระนาด ขิม ซอ ฆ้องวง ร้องเพลง ฟ้อน เปียโน) กีต้าร์ และเปียโน) จำนวน 15 คน / 2 เดือน</a></td>
                      </tr>
                      <tr class="bg-info">
                        <td><b>แผนงานที่ 2</b><a data-toggle="collapse" data-parent="#accordion" href="#collap2">
                             ส่ง ผชช. ไทย สาขาละ 2 คน ไปนิเทศน์การถ่ายทอดความรู้ ณ สปป.ลาว 2 ครั้ง/ปี</a>  </td>
                      </tr>
                      <tr class="bg-info">
                        <td><b>แผนงานที่ 3</b><a data-toggle="collapse" data-parent="#accordion" href="#collap3">
                             แผนงานสนับสนุนอุปกรณ์และสื่อการเรียนการสอนและคอมพิวเตอร์ 41 ชุด</a></td>
                      </tr>
                      <tr class="bg-info">
                       <th><b>แผนงานที่ 4</b> แผนงานติดตามและประเมินผล</th>
                      </tr>
                      <tr class="bg-info">
                        <td><a data-toggle="collapse" data-parent="#accordion" href="#collap41">4.1 การประชุม PMC ทุกปี ๆ ละ 1 ครั้ง</a></td>
                      </tr>
                      <tr class="bg-info">
                        <td><a data-toggle="collapse" data-parent="#accordion" href="#collap42">4.2 การติดตามประเมินผลโครงการ หลังจากสิ้นสุดระยะเวลา</a></td>
                      </tr>
                </table>--%><br />
              

                <div class="box-body">
                  <div style="overflow-x:auto;"> 
                    
                    <table class="table table-bordered">
                     <thead>
                      <tr class="bg-gray text-center">
                        <th class="text-center" colspan="56">Person</th>
                      </tr>
                      <tr class="bg-gray text-center">
                        <th class="text-center" colspan="3">ข้อมูลทั่วไป</th>
                        <th class="text-center" colspan="12">ค่าใช้จ่ายผู้รับทุน</th>
                        <th class="text-center" colspan="15">ค่าจัดหลักสูตร</th>
                        <th class="text-center" colspan="6">ค่าใช้จ่ายในการเดินทาง</th>
                        <th class="text-center" colspan="7">ค่าใช้จ่ายผู้เชี่ยวชาญ (ส่งไปต่างประเทศ)</th>
                        <th class="text-center" colspan="7">ค่าใช้จ่ายอาสาสมัคร (ส่งไปต่างประเทศ)</th>
                        <th class="text-center" colspan="3">ค่าก่อสร้างหรือซื้อวัสดุอุปกรณ์</th>
                        <th>ผลรวม</th>
                      </tr>
                      <tr class="bg-gray text-center">
                        <th class="text-center">No.</th>
                        <th style="width: 270px" class="text-center">Recipient Name</th>
                        <th class="text-center">Recipient Country</th>

                        <th class="text-center">ค่าเบี้ยเลี้ยง</th>
                        <th class="text-center">ค่าที่พัก</th>
                        <th class="text-center">ค่าพาหนะ </th>
                        <th class="text-center">เงินที่ได้รับเมื่อแรกถึง</th>
                        <th class="text-center">ค่าธรรมเนียมตรวจลงตรา / ขยายวีซ่า</th>
                        <th class="text-center">ค่าหนังสือ / อุปกรณ์</th>
                        <th class="text-center">ค่าธรรมเนียมการศึกษา</th>
                        <th class="text-center">ค่าเบี้ยประกันชีวิตและสุขภาพ</th>
                        <th class="text-center">ค่าทำวิทยานิพนธ์</th>
                        <th class="text-center">ค่าขนส่งสัมภาระ</th>
                        <th class="text-center">ค่าบริหารจัดการ</th>
                        <th class="text-center">ค่าใช้จ่ายอื่นๆ</th>

                        <th class="text-center">ค่าวิทยากร</th>
                        <th class="text-center">ค่าเลี้ยงรับรอง</th>
                        <th class="text-center">ค่าอาหารว่าง & เครื่องดื่ม</th>
                        <th class="text-center">ค่าเช่ารถ & ค่าน้ำมัน</th>
                        <th class="text-center">ค่าเบี้ยเลี้ยงของผู้รับทุน</th>
                        <th class="text-center">ค่าที่พักผู้รับทุน</th>
                        <th class="text-center">ค่าเบี้ยประกันชีวิตและสุขภาพ</th>
                        <th class="text-center">ค่าเช่าห้องประชุม</th>
                        <th class="text-center">ค่า VISA</th>
                        <th class="text-center">ค่าพาหนะ</th>
                        <th class="text-center">ค่าบัตรโดยสารเครื่องบิน</th>
                        <th class="text-center">ค่าวัสดุอุปกรณ์</th>
                        <th class="text-center">ค่าเบี้ยเลี้ยง / ที่พัก / พาหนะผู้ประสานงาน</th>
                        <th class="text-center">ค่าตกแต่งสถานที่</th>
                        <th class="text-center">ค่าใช้จ่ายอื่นๆ</th>

                        <th class="text-center">ค่าเบี้ยเลี้ยง</th>
                        <th class="text-center">ค่าที่พัก</th>
                        <th class="text-center">ค่าพาหนะ (ค่าแท็กซี่ , ค่าบัตรโดยสารเครื่องบิน)</th>
                        <th class="text-center">ค่าเลี้ยงรับรอง</th>
                        <th class="text-center">ค่าสมนาคุณสถานที่ดูงาน</th>
                        <th class="text-center">ค่าใช้จ่ายอื่นๆ </th>

                        <th class="text-center">ค่าเบี้ยเลี้ยง</th>
                        <th class="text-center">ค่าที่พัก</th>
                        <th class="text-center">ค่าพาหนะในประเทศ </th>
                        <th class="text-center">ค่าเตรียมการก่อนเดินทาง</th>
                        <th class="text-center">ค่าบัตรโดยสารเครื่องบิน</th>
                        <th class="text-center">ค่าขนส่งสัมภาระ</th>
                        <th class="text-center">ค่าพาหนะในต่างประเทศ</th>

                        <th class="text-center">ค่าเบี้ยเลี้ยง</th>
                        <th class="text-center">ค่าที่พัก</th>
                        <th class="text-center">ค่าพาหนะในประเทศ </th>
                        <th class="text-center">ค่าเตรียมการก่อนเดินทาง</th>
                        <th class="text-center">ค่าบัตรโดยสารเครื่องบิน</th>
                        <th class="text-center">ค่าขนส่งสัมภาระ</th>
                        <th class="text-center">ค่าพาหนะในต่างประเทศ</th>

                        <th class="text-center">ค่าก่อสร้าง</th>
                        <th class="text-center">ซื้อวัสดุอุปกรณ์</th>
                        <th class="text-center">อื่นๆ (ในระบบสามารถใส่ค่าได้เอง ไม่ติดฟิตมาจากมาสเตอร์)</th>

                        <th class="text-center">Total</th>

                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="text-center">1</td>
                        <td>Mr.Viengthone Thoummachan</td>
                        <td class="text-center">Laos</td>
                        <td>131,665.00</td>
                        <td>62,500.00</td>
                        <td>1,740.00</td>
                        <td>10,000.00</td>
                        <td>75,000.00</td>
                        <td>46,800.00</td>
                        <td>31,100.00</td>
                        <td>7,500.00</td>
                        <td>4,900.00</td>
                        <td>4,900.00</td>
                        <td>131,665.00</td>
                        <td>62,500.00</td>
                        <td>1,740.00</td>
                        <td>10,000.00</td>
                        <td>75,000.00</td>
                        <td>46,800.00</td>
                        <td>31,100.00</td>
                        <td>7,500.00</td>
                        <td>4,900.00</td>
                        <td>4,900.00</td>
                        <td>131,665.00</td>
                        <td>62,500.00</td>
                        <td>1,740.00</td>
                        <td>10,000.00</td>
                        <td>75,000.00</td>
                        <td>46,800.00</td>
                        <td>31,100.00</td>
                        <td>7,500.00</td>
                        <td>4,900.00</td>
                        <td>4,900.00</td>
                        <td>131,665.00</td>
                        <td>62,500.00</td>
                        <td>1,740.00</td>
                        <td>10,000.00</td>
                        <td>75,000.00</td>
                        <td>46,800.00</td>
                        <td>31,100.00</td>
                        <td>7,500.00</td>
                        <td>4,900.00</td>
                        <td>4,900.00</td>
                        <td>131,665.00</td>
                        <td>62,500.00</td>
                        <td>1,740.00</td>
                        <td>10,000.00</td>
                        <td>75,000.00</td>
                        <td>46,800.00</td>
                        <td>31,100.00</td>
                        <td>7,500.00</td>
                        <td>4,900.00</td>
                        <td>4,900.00</td>
                        
                      </tr>

                      <tr>
                        <td class="text-center">2</td>
                        <td>Mr.Khamphet Sengouly</td>
                        <td class="text-center">Laos</td>
                        <td>131,665.00</td>
                        <td>62,500.00</td>
                        <td>1,740.00</td>
                        <td>10,000.00</td>
                        <td>75,000.00</td>
                        <td>46,800.00</td>
                        <td>31,100.00</td>
                        <td>7,500.00</td>
                        <td>4,900.00</td>
                        <td>131,665.00</td>
                        <td>62,500.00</td>
                        <td>1,740.00</td>
                        <td>10,000.00</td>
                        <td>75,000.00</td>
                        <td>46,800.00</td>
                        <td>31,100.00</td>
                        <td>7,500.00</td>
                        <td>4,900.00</td>
                        <td>4,900.00</td>
                        <td>131,665.00</td>
                        <td>62,500.00</td>
                        <td>1,740.00</td>
                        <td>10,000.00</td>
                        <td>75,000.00</td>
                        <td>46,800.00</td>
                        <td>31,100.00</td>
                        <td>7,500.00</td>
                        <td>4,900.00</td>
                        <td>4,900.00</td>
                        <td>131,665.00</td>
                        <td>62,500.00</td>
                        <td>1,740.00</td>
                        <td>10,000.00</td>
                        <td>75,000.00</td>
                        <td>46,800.00</td>
                        <td>31,100.00</td>
                        <td>7,500.00</td>
                        <td>4,900.00</td>
                        <td>4,900.00</td>
                        <td>131,665.00</td>
                        <td>62,500.00</td>
                        <td>1,740.00</td>
                        <td>10,000.00</td>
                        <td>75,000.00</td>
                        <td>46,800.00</td>
                        <td>31,100.00</td>
                        <td>7,500.00</td>
                        <td>4,900.00</td>
                        <td>4,900.00</td>
                        
                      </tr>
                     
                      <tr>
                        <th class="text-right" colspan="52">Total</th>
                        <th class="text-center">0.00</th>
                      </tr>
                      
                    </tbody>
                  </table><br />
                </div>

                <div class="box-header with-border">
                     
                           <div class="col-lg-2"> 
                                <div class="input-group margin">
                                <select class="form-control select2" style="width: 100%;">
                                  <option>Select Form</option>
                                  <option>แบบฟอร์มที่1</option>
                                  <option selected="selected">แบบฟอร์มที่2</option>
                                </select>
                              </div><!-- /input-group -->
                         </div>
                       <div class="col-lg-3"></div>
                           <div class="col-lg-3"> 
                             <div class="input-group margin">
                                <select class="form-control select2" style="width: 100%;">
                                  <option selected="selected">Select Month</option>
                                  <option>มกราคม</option>
                                  <option>กุมภาพันธ์</option>
                                  <option>มีนาคม</option>
                                  <option>เมษายน</option>
                                  <option>พฤษภาคม</option>
                                  <option>มิถุนายน</option>
                                  <option>กรกฎาคม</option>
                                  <option>สิงหาคม</option>
                                  <option>กันยายน</option>
                                  <option>ตุลาคม</option>
                                  <option>พฤศจิกายน</option>
                                  <option>ธันวาคม</option>
                                </select>
                              </div>   <!-- /input-group -->
                         </div>
                           <div class="col-lg-3"> 
                                <div class="input-group margin">
                                <select class="form-control select2" style="width: 100%;">
                                  <option selected="selected">Seclect Year</option>
                                  <option>2560</option>
                                  <option>2559</option>
                                  <option>2558</option>
                                  <option>2557</option>
                                  <option>2556</option>
                                  <option>2555</option>
                                  <option>2554</option>
                                  <option>2553</option>
                                  <option>2552</option>
                                  <option>2551</option>
                                  <option>2550</option>
                                  <option>2549</option>
                                </select>
                              </div><!-- /input-group -->
                         </div>
                          <div class="col-lg-1">
                                <div class="input-group margin pull-right">
                                    <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                        <button type="button" class="btn btn-success btn-flat dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-print"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="#"><i class="fa fa-file-pdf-o text-red"></i>PDF</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#"><i class="fa fa-file-excel-o text-green"></i>EXCEL</a></li>
                                        </ul>
                                    </div>
                                </div>
                              </div>
                     
                  </div>
                <div style="overflow-x:auto;">
                  <table class="table table-bordered">
                    <thead>
                      <tr class="bg-gray text-center">
                        <th class="text-center" colspan="55">Country</th>
                      </tr>
                      <tr class="bg-gray text-center">
                        <th class="text-center" colspan="2">ข้อมูลทั่วไป</th>
                        <th class="text-center" colspan="12">ค่าใช้จ่ายผู้รับทุน</th>
                        <th class="text-center" colspan="15">ค่าจัดหลักสูตร</th>
                        <th class="text-center" colspan="6">ค่าใช้จ่ายในการเดินทาง</th>
                        <th class="text-center" colspan="7">ค่าใช้จ่ายผู้เชี่ยวชาญ (ส่งไปต่างประเทศ)</th>
                        <th class="text-center" colspan="7">ค่าใช้จ่ายอาสาสมัคร (ส่งไปต่างประเทศ)</th>
                        <th class="text-center" colspan="3">ค่าก่อสร้างหรือซื้อวัสดุอุปกรณ์</th>
                        <th>ผลรวม</th>
                      </tr>
                      <tr class="bg-gray text-center">
                        <th class="text-center">No.</th>
                        <th class="text-center">Recipient Country</th>

                        <th class="text-center">ค่าเบี้ยเลี้ยง</th>
                        <th class="text-center">ค่าที่พัก</th>
                        <th class="text-center">ค่าพาหนะ </th>
                        <th class="text-center">เงินที่ได้รับเมื่อแรกถึง</th>
                        <th class="text-center">ค่าธรรมเนียมตรวจลงตรา / ขยายวีซ่า</th>
                        <th class="text-center">ค่าหนังสือ / อุปกรณ์</th>
                        <th class="text-center">ค่าธรรมเนียมการศึกษา</th>
                        <th class="text-center">ค่าเบี้ยประกันชีวิตและสุขภาพ</th>
                        <th class="text-center">ค่าทำวิทยานิพนธ์</th>
                        <th class="text-center">ค่าขนส่งสัมภาระ</th>
                        <th class="text-center">ค่าบริหารจัดการ</th>
                        <th class="text-center">ค่าใช้จ่ายอื่นๆ</th>

                        <th class="text-center">ค่าวิทยากร</th>
                        <th class="text-center">ค่าเลี้ยงรับรอง</th>
                        <th class="text-center">ค่าอาหารว่าง & เครื่องดื่ม</th>
                        <th class="text-center">ค่าเช่ารถ & ค่าน้ำมัน</th>
                        <th class="text-center">ค่าเบี้ยเลี้ยงของผู้รับทุน</th>
                        <th class="text-center">ค่าที่พักผู้รับทุน</th>
                        <th class="text-center">ค่าเบี้ยประกันชีวิตและสุขภาพ</th>
                        <th class="text-center">ค่าเช่าห้องประชุม</th>
                        <th class="text-center">ค่า VISA</th>
                        <th class="text-center">ค่าพาหนะ</th>
                        <th class="text-center">ค่าบัตรโดยสารเครื่องบิน</th>
                        <th class="text-center">ค่าวัสดุอุปกรณ์</th>
                        <th class="text-center">ค่าเบี้ยเลี้ยง / ที่พัก / พาหนะผู้ประสานงาน</th>
                        <th class="text-center">ค่าตกแต่งสถานที่</th>
                        <th class="text-center">ค่าใช้จ่ายอื่นๆ</th>

                        <th class="text-center">ค่าเบี้ยเลี้ยง</th>
                        <th class="text-center">ค่าที่พัก</th>
                        <th class="text-center">ค่าพาหนะ (ค่าแท็กซี่ , ค่าบัตรโดยสารเครื่องบิน)</th>
                        <th class="text-center">ค่าเลี้ยงรับรอง</th>
                        <th class="text-center">ค่าสมนาคุณสถานที่ดูงาน</th>
                        <th class="text-center">ค่าใช้จ่ายอื่นๆ </th>

                        <th class="text-center">ค่าเบี้ยเลี้ยง</th>
                        <th class="text-center">ค่าที่พัก</th>
                        <th class="text-center">ค่าพาหนะในประเทศ </th>
                        <th class="text-center">ค่าเตรียมการก่อนเดินทาง</th>
                        <th class="text-center">ค่าบัตรโดยสารเครื่องบิน</th>
                        <th class="text-center">ค่าขนส่งสัมภาระ</th>
                        <th class="text-center">ค่าพาหนะในต่างประเทศ</th>

                        <th class="text-center">ค่าเบี้ยเลี้ยง</th>
                        <th class="text-center">ค่าที่พัก</th>
                        <th class="text-center">ค่าพาหนะในประเทศ </th>
                        <th class="text-center">ค่าเตรียมการก่อนเดินทาง</th>
                        <th class="text-center">ค่าบัตรโดยสารเครื่องบิน</th>
                        <th class="text-center">ค่าขนส่งสัมภาระ</th>
                        <th class="text-center">ค่าพาหนะในต่างประเทศ</th>

                        <th class="text-center">ค่าก่อสร้าง</th>
                        <th class="text-center">ซื้อวัสดุอุปกรณ์</th>
                        <th class="text-center">อื่นๆ (ในระบบสามารถใส่ค่าได้เอง ไม่ติดฟิตมาจากมาสเตอร์)</th>

                        <th class="text-center">Total</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="text-center">1</td>
                        <td class="text-center">Laos</td>
                        <td>131,665.00</td>
                        <td>62,500.00</td>
                        <td>1,740.00</td>
                        <td>10,000.00</td>
                        <td>75,000.00</td>
                        <td>46,800.00</td>
                        <td>31,100.00</td>
                        <td>7,500.00</td>
                        <td>4,900.00</td>
                        <td>4,900.00</td>
                        <td></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td class="text-center">2</td>
                        <td class="text-center">Laos</td>
                        <td>131,665.00</td>
                        <td>62,500.00</td>
                        <td>1,740.00</td>
                        <td>10,000.00</td>
                        <td>75,000.00</td>
                        <td>46,800.00</td>
                        <td>31,100.00</td>
                        <td>7,500.00</td>
                        <td>4,900.00</td>
                        <td>4,900.00</td>
                        <td></td>
                        <td></td>
                      </tr>
                    
                      <tr>
                        <th class="text-right" colspan="13">Total</th>
                        <th class="text-center">0.00</th>
                      </tr>
                      
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
               </div>

            </div><!-- /.col -->
            </div>
          </div><!-- /.row -->
        </section><!-- /.content -->
   </div>
   <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />
</asp:Content>
