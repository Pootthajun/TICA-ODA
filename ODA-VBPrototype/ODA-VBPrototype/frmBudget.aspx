﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="frmBudget.aspx.vb" Inherits="ODA_VBPrototype.frmBudget" %>
<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<title>Budget | Budget</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
            
           <li class="treeview">
              <a href="#">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                 <li><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i> Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li><a href="frmSubExpense.aspx"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="active treeview">
              <a href="frmAllocatedBudget.aspx">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active" ><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i> Budget</a></li>
                <li><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                    <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i> Sub Budget</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmReport.aspx">
                <i class="fa fa-file-text-o"></i> <span>Report</span></a>
            </li> 

            <li class="treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-sitemap"></i> <span>Organize Structure</span>
              </a>
             
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li class="active"><a href="frmSector.aspx"><i class="fa fa-file"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-list"></i> Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-list"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-list"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
   
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
           Budget (งบประมาณ)
          </h4>
          <ol class="breadcrumb">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i>Budget</a></li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                 <div class="box-header with-border">
                     <div class="box-body">
                      <!-- form start -->
                                <form class="form-horizontal">
                                  <div class="box-body">
                                    <div class="form-group">
                                      <label for="inputname" class="col-sm-2 control-label">ปีงบประมาณ :</label>
                                      <div class="col-sm-2">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">ทั้งหมด</option>
                                          <option>2560</option>
                                          <option>2559</option>
                                          <option>2558</option>
                                          <option>2557</option>
                                          <option>2556</option>
                                          <option>2555</option>
                                          <option>2554</option>
                                          <option>2553</option>
                                          <option>2552</option>
                                          <option>2551</option>
                                          <option>2550</option>
                                          <option>2549</option>
                                        </select>
                                      </div>
                                       <label for="inputname" class="col-sm-2 control-label">ครั้งที่ :</label>
                                      <div class="col-sm-2">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">ทั้งหมด</option>
                                          <option>1</option>
                                          <option>2</option>
                                          <option>3</option>
                                          <option>4</option>
                                          <option>5</option>
                                          <option>6</option>
                                          <option>7</option>
                                          <option>8</option>
                                          <option>9</option>
                                          <option>10</option>
                                        </select>
                                      </div>
                                      <div class="col-lg-3">
                                        <div class="input-group">
                                        <input type="text" class="form-control">
                                        <span class="input-group-btn">
                                          <button class="btn btn-info btn-flat" type="button" data-toggle="tooltip" title="ค้นหา"><i class="fa fa-search"></i></button>
                                        </span>
                                      </div><!-- /input-group -->
                                   </div>
                                   <div class="col-lg-1">
                                    <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                        <button type="button" class="btn btn-success btn-flat dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-print"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="#"><i class="fa fa-file-pdf-o text-red"></i>PDF</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#"><i class="fa fa-file-excel-o text-green"></i>EXCEL</a></li>
                                         </ul>
                                        </div>
                                      </div>
                                    </div>

                                  </div><!-- /.box-body -->
                                 
                                </form>                            
                              </div><!-- /.box-body -->
                            </div>
                            
                        <!-- /.box-header -->
                        <div class="clearfix"></div>
                       
                <div class="box-body">
                  <table class="table table-bordered">
                    <thead>
                      <tr class="bg-gray text-center">
                        <th class="text-center">ปีงบประมาณ</th>
                        <th class="text-center">ครั้งที่</th>
                        <th class="text-center">วันที่บันทึกงบประมาณ</th>
                        <th class="text-center">จำนวนเงินที่จ่าย</th>
                        <th class="text-center">เครื่องมือ</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="text-center">2558</td>
                        <td class="text-center">2</td>  
                        <td class="text-center">02/05/2558</td>
                        <td class="text-center">68,000.00</td> 
                        <td class="text-center"><div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-navicon text-green"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                              <li><a href="frmDetailBudget.aspx"><i class="fa fa-search text-blue"></i>  ดูรายละเอียด</a></li>
                              <li><a href="frmDetailBudget.aspx"><i class="fa fa-pencil text-blue"></i>  แก้ไข</a></li>
                              <li class="divider"></li>
                              <li><a href="#"><i class="fa fa-remove text-red"></i>  ลบ</a></li>
                            </ul>
                          </div></td>     
                      </tr>
                      <tr>
                        <td class="text-center">2558</td>
                        <td class="text-center">1</td>  
                        <td class="text-center">30/01/2558</td>
                        <td class="text-center">340,000.00</td>
                        <td class="text-center"><div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-navicon text-green"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                              <li><a href="frmDetailBudget.aspx"><i class="fa fa-search text-blue"></i>  ดูรายละเอียด</a></li>
                              <li><a href="frmDetailBudget.aspx"><i class="fa fa-pencil text-blue"></i>  แก้ไข</a></li>
                              <li class="divider"></li>
                              <li><a href="#"><i class="fa fa-remove text-red"></i>  ลบ</a></li>
                            </ul>
                          </div></td>  
                      </tr>
                      <tr>
                        <td class="text-center">2557</td>
                        <td class="text-center">3</td>  
                        <td class="text-center">013/09/2557</td>
                        <td class="text-center">508,000.00</td>
                        <td class="text-center"><div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-navicon text-green"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                              <li><a href="frmDetailBudget.aspx"><i class="fa fa-search text-blue"></i>  ดูรายละเอียด</a></li>
                              <li><a href="frmDetailBudget.aspx"><i class="fa fa-pencil text-blue"></i>  แก้ไข</a></li>
                              <li class="divider"></li>
                              <li><a href="#"><i class="fa fa-remove text-red"></i>  ลบ</a></li>
                            </ul>
                          </div></td>        
                      </tr>
                      <tr>
                        <td class="text-center">2557</td>
                        <td class="text-center">2</td>  
                        <td class="text-center">24/06/2557</td>
                        <td class="text-center">568,000.00</td>  
                        <td class="text-center"><div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-navicon text-green"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                              <li><a href="frmDetailBudget.aspx"><i class="fa fa-search text-blue"></i>  ดูรายละเอียด</a></li>
                              <li><a href="frmDetailBudget.aspx"><i class="fa fa-pencil text-blue"></i>  แก้ไข</a></li>
                              <li class="divider"></li>
                              <li><a href="#"><i class="fa fa-remove text-red"></i>  ลบ</a></li>
                            </ul>
                          </div></td>      
                      </tr>
                      <tr>
                        <td class="text-center">2557</td>
                        <td class="text-center">1</td>  
                        <td class="text-center">12/02/2557</td>
                        <td class="text-center">2,000,000.00</td>
                        <td class="text-center"><div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-navicon text-green"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                              <li><a href="frmDetailBudget.aspx"><i class="fa fa-search text-blue"></i>  ดูรายละเอียด</a></li>
                              <li><a href="frmDetailBudget.aspx"><i class="fa fa-pencil text-blue"></i>  แก้ไข</a></li>
                              <li class="divider"></li>
                              <li><a href="#"><i class="fa fa-remove text-red"></i>  ลบ</a></li>
                            </ul>
                          </div></td>  
                      </tr>
                      <tr>
                        <td class="text-center">2556</td>
                        <td class="text-center">1</td>  
                        <td class="text-center">02/11/2556</td>
                        <td class="text-center">1,000,000.00</td>
                        <td class="text-center"><div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-navicon text-green"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                              <li><a href="frmDetailBudget.aspx"><i class="fa fa-search text-blue"></i>  ดูรายละเอียด</a></li>
                              <li><a href="frmDetailBudget.aspx"><i class="fa fa-pencil text-blue"></i>  แก้ไข</a></li>
                              <li class="divider"></li>
                              <li><a href="#"><i class="fa fa-remove text-red"></i>  ลบ</a></li>
                            </ul>
                          </div></td>  
                      </tr>
                      
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
   </div>
   <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />
</asp:Content>

