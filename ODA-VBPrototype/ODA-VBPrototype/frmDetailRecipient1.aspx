﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="frmDetailRecipient1.aspx.vb" Inherits="ODA_VBPrototype.frmDetailRecipient1" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<title>History Recipient | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
           <li class="treeview">
              <a href="frnFinance.aspx">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li ><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i> Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li><a href="frmSubExpense.aspx"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="treeview">
              <a href="frmAllocatedBudget.aspx">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i> Budget</a></li>
                <li><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                    <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i> Sub Budget</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmReport.aspx">
                <i class="fa fa-file-text-o"></i> <span>Report</span></a>
            </li> 

            <li class="treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-sitemap"></i> <span>Organize Structure</span>
              </a>
             
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li><a href="frmSector.aspx"><i class="fa fa-file"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-list"></i> Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-list"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-list"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">

     <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
            Profile (ข้อมูลส่วนตัว)
          </h4>
          <ol class="breadcrumb">
            <li><a href="frmDashborad.aspx"><i class="fa fa-area-chart"></i> Dashborad</a></li>
            <li><a href="frmDetailProject.aspx">Project</a></li>
            <li class="active">Profile</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <div class="row">
            <div class="col-md-3">

              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="dist/img/user1-128x128.jpg" alt="User profile picture">
                  <h3 class="profile-username text-center">Viengthone Thoummachan</h3>
                  <p class="text-muted text-center"><b class="text-orange"> Student ID: 56199130400</b></p>

                  <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                      <b  data-toggle="tooltip" title="ประวัติส่วนตัว"><a href="#Per" data-toggle="tab">A. PERSONAL</a></b>
                    </li>
                    <li class="list-group-item">
                      <b  data-toggle="tooltip" title="ประวัติการศึกษา"><a href="#Edu" data-toggle="tab">B. EDUCATION RECORD</a></b>
                    </li>
                    <li class="list-group-item">
                      <b  data-toggle="tooltip" title="ประวัติการทำงาน"><a href="#Emp" data-toggle="tab">C. EMPLOYMENT RECORD</a></b>
                    </li>
                    <li class="list-group-item">
                      <b  data-toggle="tooltip" title="ประวัติการรับทุน"><a href="#Gov" data-toggle="tab">D. HISTORY</a></b>
                    </li>
                  </ul>

                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
            <div class="col-md-9">
              <div class="nav-tabs-custom">
                <div class="tab-content">
                  <div class="active tab-pane" id="Per">
                    <table class="table">
                        <tr class="bg-gray">
                        <th style="width: 150px" colspan="3">
                          <h5><b class="text-blue">A. PERSONAL HISTORY </b></h5></th>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Prefix :</b></th>
                          <td colspan="2">Mr.</td>
                        </tr>
                         <tr>
                          <th style="width: 250px"><b class="pull-right">Family Name (as shown in passport) :</b></th>
                          <td colspan="2">Thoummachan</td>
                        </tr>
                         <tr>
                          <th style="width: 250px"><b class="pull-right">Given Names :</b></th>
                          <td colspan="2">Viengthone Thoummachan</td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Sex :</b></th>
                          <td colspan="2">Male</td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Country and City Of Birth :</b></th>
                          <td>Laos</td>
                          <td></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Nationality :</b></th>
                          <td colspan="2">Laos</td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Date of Birth :</b></th>
                          <td colspan="2">1983-10-01</td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Age :</b></th>
                          <td colspan="2">33</td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Marital Status :</b></th>
                          <td colspan="2">Single</td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Region :</b></th>
                          <td colspan="2">-</td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Work Address :</b></th>
                          <td colspan="2">xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx</td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Telephone :</b></th>
                          <td>xxxxxxxxxx</td>
                          <td>xxxxxxxxxx</td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Fax :</b></th>
                          <td colspan="2">xxxxxxxxxx</td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">E-mail :</b></th>
                          <td colspan="2">xxxxxxxxxx@xxxxxx.co.th</td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Home Address :</b></th>
                          <td colspan="2">xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx</td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Telephone :</b></th>
                          <td>xxxxxxxxxx</td>
                          <td>xxxxxxxxxx</td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Fax :</b></th>
                          <td colspan="2">xxxxxxxxxx</td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">E-mail :</b></th>
                          <td colspan="2">xxxxxxxxxx@xxxxxx.co.th</td>
                        </tr>
                       <%-- <tr>
                          <th style="width: 250px"><b class="pull-right">Expiry Date of Passport:</b><br /><b class="pull-right"> (วันหมดอายุ) :</b></th>
                          <td>2023-04-17</td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Expiry Date of Insurance</b><br /><b class="pull-right">(วันหมดอายุประกันภัย) :</b></th>
                          <td>2015-05-31</td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Expiry Date of VISA (วันหมดอายุวีซ่า) :</b></th>
                          <td>2015-05-31</td>
                        </tr>--%>
                        <%--<tr>
                          <th style="width: 250px"><b class="pull-right">International Airport :</b><br /><b class="pull-right">City for departture</b><br /><b class="pull-right"> (เมืองสำหรับการเดินทาง) :</b></th>
                          <td>-</td>
                        </tr>--%>
                        
                        <%--<tr class="bg-gray">
                        <th style="width: 150px" colspan="2">
                          <h5><b class="text-blue">Address (ที่อยู่ปัจจุบัน) </b></h5></th>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Address (ที่อยู่) :</b></th>
                          <td>Sylanduare Village, Chanthabouly District, Vientiane Capital, Lao PDR</td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Telephone (เบอร์โทรศัพท์) :</b></th>
                          <td></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Fax (โทรสาร) :</b></th>
                          <td>-</td>
                        </tr>--%>
                    </table>
                  </div>
                  <div class="tab-pane" id="Edu">
                        <table class="table">
                        <tr class="bg-gray">
                        <th style="width: 150px" colspan="2">
                          <h5><b class="text-blue">B. EDUCATION RECORD </b></h5></th>
                            
                        </tr>
                       <%-- <tr>
                          <th style="width: 250px"><b class="pull-right">Status (สถานะ) :</b></th>
                          <td><!-- checkbox -->
                          <div class="form-group">
                            <label>
                              <input type="radio" name="r2" class="minimal-red" checked>
                                กำลังศึกษา
                            </label>
                            <label>
                              <input type="radio" name="r2" class="minimal-red">
                                จบการศึกษา
                            </label>
                            <label>
                              <input type="radio" name="r2" class="minimal-red">
                              ไม่จบการศึกษา
                            </label>
                          </div></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Course (หลักสูตร) :</b></th>
                          <td>-</td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Duration (ระยะเวลา) :</b></th>
                          <td>-</td>
                        </tr>--%>
                        <tr>
                          <td class="text-primary" colspan="2">
                                  <table class="table table-bordered"">
                                    <tr>
                                      <th style="width: 150px" class="bg-gray text-center"rowspan="2"><b>Major languages</b></th>
                                      <th style="width: 50px" class="bg-gray text-center" colspan="3"><b class="text-center">Read</b></th>
                                      <th style="width: 50px" class="bg-gray text-center" colspan="3"><b class="text-center">Write</b></th>
                                      <th style="width: 50px" class="bg-gray text-center" colspan="3"><b class="text-center">Speak</b></th>
                                     </tr>
                                    <tr>
                                      <th style="width: 50px" class="bg-gray"><b>Excellent</b></th>
                                      <th style="width: 50px" class="bg-gray"><b>Good</b></th>
                                      <th style="width: 50px" class="bg-gray"><b>Fair</b></th>
                                      <th style="width: 50px" class="bg-gray"><b>Excellent</b></th>
                                      <th style="width: 50px" class="bg-gray"><b>Good</b></th>
                                      <th style="width: 50px" class="bg-gray"><b>Fair</b></th>
                                      <th style="width: 50px" class="bg-gray"><b>Excellent</b></th>
                                      <th style="width: 50px" class="bg-gray"><b>Good</b></th>
                                      <th style="width: 50px" class="bg-gray"><b>Fair</b></th>
                                     </tr>
                                     <tr>
                                      <td>Thai</td>
                                      <td><label>
                                          <input type="radio" name="r1" class="minimal">
                                        </label></td>
                                      <td><label>
                                          <input type="radio" name="r1" class="minimal">
                                        </label></td>
                                      <td><label>
                                          <input type="radio" name="r1" class="minimal" checked>
                                        </label></td>
                                      <td><label>
                                          <input type="radio" name="r2" class="minimal">
                                        </label></td>
                                      <td><label>
                                          <input type="radio" name="r2" class="minimal">
                                        </label></td>
                                      <td><label>
                                          <input type="radio" name="r2" class="minimal" checked>
                                        </label></td>
                                      <td><label>
                                          <input type="radio" name="r3" class="minimal">
                                        </label></td>
                                      <td><label>
                                          <input type="radio" name="r3" class="minimal">
                                        </label></td>
                                      <td><label>
                                          <input type="radio" name="r3" class="minimal" checked>
                                        </label></td>
                                    </tr>
                                     <tr>
                                      <td>Chinese</td>
                                      <td><label>
                                          <input type="radio" name="r4" class="minimal">
                                        </label></td>
                                      <td><label>
                                          <input type="radio" name="r4" class="minimal" checked>
                                        </label></td>
                                      <td><label>
                                          <input type="radio" name="r4" class="minimal">
                                        </label></td>
                                      <td><label>
                                          <input type="radio" name="r5" class="minimal">
                                        </label></td>
                                      <td><label>
                                          <input type="radio" name="r5" class="minimal" checked>
                                        </label></td>
                                      <td><label>
                                          <input type="radio" name="r5" class="minimal">
                                        </label></td>
                                      <td><label>
                                          <input type="radio" name="r6" class="minimal">
                                        </label></td>
                                      <td><label>
                                          <input type="radio" name="r6" class="minimal" checked>
                                        </label></td>
                                      <td><label>
                                          <input type="radio" name="r6" class="minimal">
                                        </label></td>
                                    </tr>
                                     <%--<tr>
                                      <td></td>
                                      <td><label>
                                          <input type="radio" name="r7" class="minimal">
                                        </label></td>
                                      <td><label>
                                          <input type="radio" name="r7" class="minimal">
                                        </label></td>
                                      <td><label>
                                          <input type="radio" name="r7" class="minimal">
                                        </label></td>
                                      <td><label>
                                          <input type="radio" name="r8" class="minimal">
                                        </label></td>
                                      <td><label>
                                          <input type="radio" name="r8" class="minimal">
                                        </label></td>
                                      <td><label>
                                          <input type="radio" name="r8" class="minimal">
                                        </label></td>
                                      <td><label>
                                          <input type="radio" name="r9" class="minimal">
                                        </label></td>
                                      <td><label>
                                          <input type="radio" name="r9" class="minimal">
                                        </label></td>
                                      <td><label>
                                          <input type="radio" name="r9" class="minimal">
                                        </label></td>
                                    </tr>--%>
                            </table></td>
                        </tr>
                        <tr>
                                  <th style="width: 230px"><b class="pull-right">English Proficiency Test :</b></th>
                                  <td class="text-primary">
                                          <table class="table table-bordered">
                                            <tr>
                                              <th style="width: 150px" class="bg-gray"><b>TOEFL Score </b></th>
                                              <td>xxxxxxxx</td>
                                             </tr>
                                             <tr>
                                              <th style="width: 150px" class="bg-gray"><b>IELTs Score</b></th>
                                              <td>xxxxxxxx</td>
                                            </tr>
                                             <tr>
                                              <th style="width: 150px" class="bg-gray"><b>Other (specify)</b></th>
                                              <td>xxxxxxxx</td>
                                            </tr>
                                    </table></td>
                                </tr>
                        <tr>
                          <td class="text-primary" colspan="2">
                                  <table class="table table-bordered"">
                        
                                    <tr>
                                      <th style="width: 150px" class="bg-gray" rowspan="2"><b>Education Institution</b></th>
                                      <th style="width: 50px" class="bg-gray" rowspan="2"><b>City / Country</b></th>
                                      <th style="width: 50px" class="bg-gray" colspan="2"><b>Years Attended</b></th>
                                      <th style="width: 50px" class="bg-gray" rowspan="2"><b>Degrees, Diplomas and Certificates </b></th>
                                      <th style="width: 50px" class="bg-gray" rowspan="2"><b>Special fields of study</b></th>
                                     </tr>
                                    <tr>
                                      <th style="width: 50px" class="bg-gray"><b>From</b></th>
                                      <th style="width: 50px" class="bg-gray"><b>To</b></th>
                                     </tr>
                                     <tr>
                                      <td>xxxxxxxxxxxxxxxxxxxx</td>
                                      <td>xxxxxxxxxxxxxxxxxxxx</td>
                                      <td>xxxxxxxx</td>
                                      <td>xxxxxxxx</td>
                                      <td>xxxxxxxxxxxxxxxxxxxx</td>
                                      <td>xxxxxxxxxxxxxxxxxxxx</td>
                                    </tr>
                                     <tr>
                                      <td>xxxxxxxxxxxxxxxxxxxx</td>
                                      <td>xxxxxxxxxxxxxxxxxxxx</td>
                                      <td>xxxxxxxx</td>
                                      <td>xxxxxxxx</td>
                                      <td>xxxxxxxxxxxxxxxxxxxx</td>
                                      <td>xxxxxxxxxxxxxxxxxxxx</td>
                                    </tr>
                            </table></td>
                        </tr>
                      <%--  <tr>
                          <th style="width: 250px"><b class="pull-right">Agency (หน่วยงาน) :</b></th>
                          <td>-</td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Agency Address</b><br /><b class="pull-right">  (ที่ตั้งหน่วยงาน) :</b></th>
                          <td>-</td>
                        </tr>
                       --%> 
                       <%-- <tr>
                            <th style="width: 300px"><b class="pull-right">Have you ever been trained in Thailand? If yes, please specify title of the course, where and for how long? </b></th>
                            <td></td>
                        </tr>
                        <tr>
                             <th style="width: 300px"><b class="pull-right">For a candidate for a degree program, please give a list of relevant publications/researches (do not attach details) </b></th>
                             <td></td>
                         </tr>--%>
                        
                        </table>
                  </div>
                  <div class="tab-pane" id="Emp">
                     <table class="table">
                            <tr>
                              <th style="width: 200px"><b class="pull-right">Present or most recent post :</b></th>
                              <td>2556 <b>To</b> 2558
                              </td>
                            </tr>
                            <tr>
                              <th style="width: 200px"><b class="pull-right">Title of your post :</b></th>
                              <td>xxxxxxxx</td>
                            </tr>
                            <tr>
                              <th style="width: 200px"><b class="pull-right">Name of organisation :</b></th>
                              <td>xxxxxxxx</td>
                            </tr>
                            <tr>
                              <th style="width: 200px"><b class="pull-right">Type of organisation :</b></th>
                              <td>xxxxxxxx</td>
                            </tr>
                             <tr>
                              <th style="width: 200px"><b class="pull-right">Official Address :</b></th>
                              <td>xxxxxxxx</td>
                            </tr>
                             <tr>
                              <th style="width: 200px"><b class="pull-right">Desrciption :</b></th>
                              <td>xxxxxxxx</td>
                            </tr>
                           </table>
                           <table class="table">
                            <tr>
                              <th style="width: 200px"><b class="pull-right">Previous post : </b></th>
                              <td>xxxx<b> To</b>xxxx</td>
                            </tr>
                            <tr>
                              <th style="width: 200px"><b class="pull-right">Title of your post :</b></th>
                              <td>xxxxxxxx</td>
                            </tr>
                            <tr>
                              <th style="width: 200px"><b class="pull-right">Name of organisation :</b></th>
                              <td>xxxxxxxx</td>
                            </tr>
                            <tr>
                              <th style="width: 200px"><b class="pull-right">Type of organisation :</b></th>
                              <td>xxxxxxxx</td>
                            </tr>
                             <tr>
                              <th style="width: 200px"><b class="pull-right">Official Address :</b></th>
                              <td>xxxxxxxx</td>
                            </tr>
                             <tr>
                              <th style="width: 200px"><b class="pull-right">Desrciption :</b></th>
                              <td>xxxxxxxx</td>
                            </tr>
                           </table>
                  </div>
                  <div class="tab-pane" id="Gov">
                    <table class="table">
                                        <tr class="bg-gray">
                                          <th style="width: 100px">No.(ลำดับ)</th>
                                          <th>Course (หลักสูตร)</th>
                                        </tr>
                                        <tr>
                                          <td>1.</td>
                                          <td>Master of Arts</td>
                                        </tr>
                                        <tr>
                                          <td>2.</td>
                                          <td>Master of Science Program in Service Innovation</td>
                                        </tr>
                                        <tr>
                                          <td>3.</td>
                                          <td>Master of Engineering In Civil Engineering</td>
                                        </tr>
                      </table>
                  </div>
                </div>
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
  
    
  

<!----------------Page Advance---------------------->
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />

</asp:Content>
