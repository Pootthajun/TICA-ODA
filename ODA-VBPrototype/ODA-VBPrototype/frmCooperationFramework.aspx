﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="frmCooperationFramework.aspx.vb" Inherits="ODA_VBPrototype.frmCooperationFramework" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Cooperation Framework | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
           <li class="treeview">
              <a href="frnFinance.aspx">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li ><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i> Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li><a href="frmSubExpense.aspx"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="treeview">
              <a href="frmAllocatedBudget.aspx">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i> Budget</a></li>
                <li><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                    <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i> Sub Budget</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmReport.aspx">
                <i class="fa fa-file-text"></i> <span>Report</span></a>
            </li> 
            <li class="treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-sitemap"></i> <span>Organize Structure</span>
              </a>
             
            </li>
             <li class="active treeview">
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li class="active"><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li><a href="frmSector.aspx"><i class="fa fa-file"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-list"></i> Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-list"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-list"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
   
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
           Cooperation Framework
          </h4>
          <ol class="breadcrumb">
            <li><a href="frmDashborad.aspx"><i class="fa fa-area-chart"></i> Dashborad</a></li>
            <li class="active">Cooperation Framework</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <p><a href="frmEditCooperationFramework.aspx"><button class="btn bg-orange margin-r-5 btn-social"><i class="fa fa-plus"></i>Add Status</button></a></p>
                     <div class="col-lg-7">
                     <h4 class="text-primary">พบทั้งหมด 10 รายการ</h4>
                     </div>
                       <div class="input-group col-sm-5 pull-right">
                       <input type="text" class="form-control">
                       <span class="input-group-addon"><i class="fa fa-search"></i></span>
                     </div>
                     <div class="clearfix"></div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered">
                    <thead>
                      <tr class="bg-gray">
                        <th>Cooperation Framework (กรอบความร่วมมือ)</th>
                        <th style="width: 70px">Edit(แก้ไข)</th>
                        <th style="width: 50px">Delete(ลบ)</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>N/A</td>
                        <td><i class="fa fa-edit text-success"></i> Edit</td>
                        <td><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>AAF</td>
                        <td><i class="fa fa-edit text-success"></i> Edit</td>
                        <td><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ACMECS</td>
                        <td><i class="fa fa-edit text-success"></i> Edit</td>
                        <td><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>APEC</td>
                        <td><i class="fa fa-edit text-success"></i> Edit</td>
                        <td><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ASEAN</td>
                        <td><i class="fa fa-edit text-success"></i> Edit</td>
                        <td><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ASEM</td>
                        <td><i class="fa fa-edit text-success"></i> Edit</td>
                        <td><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>BIMSTEC</td>
                        <td><i class="fa fa-edit text-success"></i> Edit</td>
                        <td><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>GMS</td>
                        <td><i class="fa fa-edit text-success"></i> Edit</td>
                        <td><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>IMT-GT</td>
                        <td><i class="fa fa-edit text-success"></i> Edit</td>
                        <td><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>FEALAC</td>
                        <td><i class="fa fa-edit text-success"></i> Edit</td>
                        <td><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      
                    </tbody>
                   
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
   </div>
 
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />
</asp:Content>
